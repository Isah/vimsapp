package com.vims.vimsapp.usecase_tests;

import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.domain.patients.entity.Child;
import com.vims.vimsapp.domain.vaccinations.data.VaccinationsRepository;
import com.vims.vimsapp.domain.patients.interactor.vaccinations.GenerateChildSchedule;
import com.vims.vimsapp.domain.vaccinations.data.remote.HospitalScheduleService;
import com.vims.vimsapp.domain.vaccinations.entity.VaccinationCard;
import com.vims.vimsapp.utilities.DateCalendarConverter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class GenerateChildScheduleTest {



    @Mock
    UseCase.OutputBoundary<GenerateChildSchedule.ResponseValue> outputBoundary;

    /**
     * {@link ArgumentCaptor} is a powerful Mockito API to capture argument values and use them to
     * perform further actions or assertions on them.
     */
    @Captor
    private ArgumentCaptor<VaccinationsRepository.SaveChildVaccinationCardCallback> saveVaccinationCardCallbackArgumentCaptor;

    @Captor
    private ArgumentCaptor<VaccinationCard> vaccinationCardArgumentCaptor;




    GenerateChildSchedule generateChildSchedule;

    @Mock
    VaccinationsRepository vaccinationsRepository;


    HospitalScheduleService dummyHospitalScheduleService;


    @Before
    public void setUp(){

        MockitoAnnotations.initMocks(this);

        dummyHospitalScheduleService = new HospitalScheduleService();


        //put in a dummy hospital schedule
     //   generateChildSchedule = GenerateChildSchedule.getInstance(vaccinationsRepository, dummyHospitalScheduleService);



    }


    // The valid vaccination date is a date that is equal to today


    //Verifying Interactions
    @Test
    public void generatesScheduleIfCalendarDateExists() throws Exception {

        Child child = new Child();
        child.setFirstName("Toki");
        child.setLastName("Clare");
        child.setGender("Boy");

        //born feb 4th - after 6 weeks thurs sep 13 th
        GregorianCalendar birthCalendar = new GregorianCalendar();
        birthCalendar.set(2018, 0, 4); //jan 4th

        GregorianCalendar expectedCalAfter6weeksPlusHospitalDay = new GregorianCalendar();
        expectedCalAfter6weeksPlusHospitalDay.set(2018, 1, 15); // after 6wks date feb 10th //hospital date feb 15


        Date childBirth = DateCalendarConverter.calendarToDate(birthCalendar);


        child.setDateOfBirth(childBirth);

        //expected dte mon ,sept, 10



        GenerateChildSchedule.RequestValues requestValues = new GenerateChildSchedule.RequestValues(child);


        //1. When send message is called
        generateChildSchedule.execute(requestValues,outputBoundary);

        Mockito.verify(vaccinationsRepository).saveChildVaccinationCard(vaccinationCardArgumentCaptor.capture(), saveVaccinationCardCallbackArgumentCaptor.capture());


        //verify the right arguments have been passed
      //  assertThat(vaccinationCardArgumentCaptor.getValue().getVaccinationEvents().size(), is(2));


        String actualVaccineName = vaccinationCardArgumentCaptor.getValue().getVaccinationEvents().get(0).getVaccineName();
        Calendar actualHospitalCalendar6WeeksDates = DateCalendarConverter.dateToCalendar(DateCalendarConverter.stringToDateWithoutTime(vaccinationCardArgumentCaptor.getValue().getVaccinationEvents().get(0).getScheduledDate()));

        assertThat(actualVaccineName, is("Polio 1, DPT -HepB_Hib1"));
        assertThat(actualHospitalCalendar6WeeksDates.get(Calendar.MONTH), is(expectedCalAfter6weeksPlusHospitalDay.get(Calendar.MONTH)));
        assertThat(actualHospitalCalendar6WeeksDates.get(Calendar.DAY_OF_MONTH), is(expectedCalAfter6weeksPlusHospitalDay.get(Calendar.DAY_OF_MONTH)));


       // assertThat(child.getId(), is(GenerateChildSchedule.card.getCardId()));



        //i need to check the card


        //2. verify the right mtd executed
        // Mockito.verify(repository).sendMessageOverNetwork(Mockito.any(Message.class), mSendMessageCallbackCaptor.capture());

        // When msg is finally sent
        // trigger the  callback
        // mSendMessageCallbackCaptor.getValue().onMessageSent();



        //if theres any kind of callback capture the values and simulate the display



        //check that the scheduled date is the vaccination date calculated for that vaccine
        //check that the

    }




}
