package com.vims.vimsapp.usecase_tests;


import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.domain.vaccinations.data.VaccinationsDataSource;
import com.vims.vimsapp.domain.vaccinations.data.VaccinationsRepository;
import com.vims.vimsapp.domain.vaccinations.entity.VaccinationCard;
import com.vims.vimsapp.domain.patients.interactor.vaccinations.RecordChildVaccination;
import com.vims.vimsapp.domain.vaccinations.entity.VaccinationEvent;
import com.vims.vimsapp.domain.vaccinations.entity.VaccinationStatus;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.is;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@RunWith(MockitoJUnitRunner.class)
public class RecordChildVaccinationUseCaseTest {



    @Mock
    UseCase.OutputBoundary<RecordChildVaccination.ResponseValue> outputBoundary;

    /**
     * {@link ArgumentCaptor} is a powerful Mockito API to capture argument values and use them to
     * perform further actions or assertions on them.
     */
    @Captor
    private ArgumentCaptor<VaccinationsDataSource.SaveVaccinationCallback> saveVaccinationCallbackArgumentCaptor;

    @Captor
    private ArgumentCaptor<VaccinationEvent> vaccinationEventArgumentCaptor;




    RecordChildVaccination recordChildVaccination;

    @Mock
    VaccinationsRepository vaccinationsRepository;

    VaccinationEvent dummyVaccinationEvent;


    @Before
    public void setUp(){

        MockitoAnnotations.initMocks(this);

       recordChildVaccination = RecordChildVaccination.getInstance(vaccinationsRepository);





       //Dummy Data
        //Data





    }


    // The valid vaccination date is a date that is equal to today


    //Verifying Interactions
    @Test
    public void RecordVaccination_withValidDateOfVaccination_succeeds() throws Exception {
        //Need to mock the repository
        //Execute the use case and just capture its input callback parameters
        //Verify the repository methods were executed succesfully after the use case is called
        // e.g saveChild was executed



        //Data
        dummyVaccinationEvent = new VaccinationEvent();

        dummyVaccinationEvent.setVaccinationStatus(VaccinationStatus.NOT_DONE);

        dummyVaccinationEvent.setScheduledDate(new Date());
        dummyVaccinationEvent.setVaccineName("Hepititis B");

        VaccinationCard vc = new VaccinationCard();
        vc.setChildId("1");

        List<VaccinationEvent> vaccinationEvents = new ArrayList<>();
        vaccinationEvents.add(dummyVaccinationEvent);

        vc.setVaccinationEvents(vaccinationEvents);

       // vaccinationsRepository.mCachedVaccinationCards.put(vc.getCardId(), vc);

        Mockito.when(vaccinationsRepository.mCachedVaccinationCards.get(vc.getCardId())).thenReturn(vc);


        RecordChildVaccination.RequestValues requestValues = new RecordChildVaccination.RequestValues(dummyVaccinationEvent);

        //1. When send message is called
        recordChildVaccination.execute(requestValues,outputBoundary);

        Mockito.verify(vaccinationsRepository).saveVaccination(vaccinationEventArgumentCaptor.capture(), saveVaccinationCallbackArgumentCaptor.capture());

        //verify the right arguments have been passed
        assertThat(vaccinationEventArgumentCaptor.getValue(), is(dummyVaccinationEvent));



        VaccinationStatus actualStatus = vaccinationsRepository.mCachedVaccinationCards.get(vc.getCardId()).getVaccinationEvents().get(0).getVaccinationStatus();

        assertThat(VaccinationStatus.DONE, is(actualStatus));


    }





    //Verifying Interactions
    @Test
    public void RecordVaccination_withInValidDateOfVaccination_flags() throws Exception {
        //Need to mock the repository
        //Execute the use case and just capture its input callback parameters
        //Verify the repository methods were executed succesfully after the use case is called
        // e.g saveChild was executed


         Calendar stub_invalid_date = Calendar.getInstance();
         stub_invalid_date.set(2018,6,7);


        //Test Data
        dummyVaccinationEvent.setVaccinationDate(stub_invalid_date.getTime());
        dummyVaccinationEvent.setVaccinationStatus(VaccinationStatus.NOT_DONE);


        //1. When send message is called
        recordChildVaccination.execute(new RecordChildVaccination.RequestValues(dummyVaccinationEvent),outputBoundary);

        Mockito.verify(vaccinationsRepository, Mockito.never()).saveVaccination(vaccinationEventArgumentCaptor.capture(), saveVaccinationCallbackArgumentCaptor.capture());

        //verify the right arguments have been passed
       // assertThat(vaccinationEventArgumentCaptor.getValue(), is(dummyVaccinationEvent));



        //2. verify the right mtd executed
        // Mockito.verify(repository).sendMessageOverNetwork(Mockito.any(Message.class), mSendMessageCallbackCaptor.capture());

        // When msg is finally sent
        // trigger the  callback
        // mSendMessageCallbackCaptor.getValue().onMessageSent();



        //if theres any kind of callback capture the values and simulate the display


    }







    //Verifying Interactions
    @Test
    public void RecordVaccination_withInValidStatus_NotDue_flags() throws Exception {
        //Need to mock the repository
        //Execute the use case and just capture its input callback parameters
        //Verify the repository methods were executed succesfully after the use case is called
        // e.g saveChild was executed


        //Data
        dummyVaccinationEvent = new VaccinationEvent();

        //Data
        dummyVaccinationEvent.setVaccinationDate(new Date());
        dummyVaccinationEvent.setVaccinationStatus(VaccinationStatus.NOT_DONE);


        //1. When send message is called
        recordChildVaccination.execute(new RecordChildVaccination.RequestValues(dummyVaccinationEvent),outputBoundary);

        Mockito.verify(vaccinationsRepository, Mockito.never()).saveVaccination(vaccinationEventArgumentCaptor.capture(), saveVaccinationCallbackArgumentCaptor.capture());

        //verify the arguments were not even captured
        //ok no argument value even gets captured
       // assertEquals(null, vaccinationEventArgumentCaptor.getValue().getVaccinationStatus());


    }




    //Verifying Interactions
    @Test
    public void RecordVaccination_withInValidStatus_Done_flags() throws Exception {
        //Need to mock the repository
        //Execute the use case and just capture its input callback parameters
        //Verify the repository methods were executed succesfully after the use case is called
        // e.g saveChild was executed

        // Calendar invalid_date = Calendar.getInstance();
        // invalid_date.set(2018,6,7);

        // invalid_date.getTime();


        //Data
        dummyVaccinationEvent.setVaccinationDate(new Date());
        dummyVaccinationEvent.setVaccinationStatus(VaccinationStatus.DONE);


        //1. When send message is called
        recordChildVaccination.execute(new RecordChildVaccination.RequestValues(dummyVaccinationEvent),outputBoundary);

        Mockito.verify(vaccinationsRepository, Mockito.never()).saveVaccination(vaccinationEventArgumentCaptor.capture(), saveVaccinationCallbackArgumentCaptor.capture());

        //verify the arguments were not even captured
        //ok no argument value even gets captured
        // assertEquals(null, vaccinationEventArgumentCaptor.getValue().getVaccinationStatus());


    }



}
