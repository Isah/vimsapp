package com.vims.vimsapp.repository_tests;


import com.vims.vimsapp.domain.vaccinations.data.VaccinationsDataSource;
import com.vims.vimsapp.domain.vaccinations.data.VaccinationsRepository;
import com.vims.vimsapp.domain.vaccinations.data.local.VaccinationsLocalDataSource;
import com.vims.vimsapp.domain.vaccinations.data.remote.VaccinationsRemoteDataSource;
import com.vims.vimsapp.domain.vaccinations.entity.VaccinationCard;
import com.vims.vimsapp.domain.vaccinations.entity.VaccinationEvent;
import com.vims.vimsapp.domain.vaccinations.entity.VaccinationStatus;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.matchers.Null;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class VaccinationsRepositoryTest {


    @Mock
    VaccinationsLocalDataSource vaccinationsLocalDataSource;

    @Mock
    VaccinationsRemoteDataSource vaccinationsRemoteDataSource;




    @Mock
    VaccinationsDataSource.SaveVaccinationCallback saveVaccinationCallbackMock;



    @Captor
    ArgumentCaptor<VaccinationsDataSource.SaveVaccinationCallback> saveVaccinationCallbackArgumentCaptor;


    @Captor
    ArgumentCaptor<VaccinationsDataSource.SaveChildVaccinationCardCallback> saveCardCallbackCaptor;

    @Captor
    ArgumentCaptor<VaccinationsDataSource.LoadAllVaccinationCardsCallback> loadAllVaccinationCardsCallbackArgumentCaptor;




    @Mock
    VaccinationsDataSource.SaveChildVaccinationCardCallback saveChildVaccinationCardCallback;


    @Mock
    VaccinationsDataSource.LoadAllVaccinationCardsCallback loadAllVaccinationCardsCallback;



    @Captor
    private ArgumentCaptor<Date> dateArgumentCaptor;




    VaccinationsRepository vaccinationsRepository;





    @Before
   public void setUp(){


       MockitoAnnotations.initMocks(this);

       vaccinationsRepository =  VaccinationsRepository.getInstance(vaccinationsRemoteDataSource,vaccinationsLocalDataSource);


   }



    @Test
    public void savesVaccinationCardByLocalRepository(){


        VaccinationCard dummyCard = new VaccinationCard();
        dummyCard.setDateCreated(new Date());


        vaccinationsRepository.saveChildVaccinationCard(dummyCard, saveChildVaccinationCardCallback);

        Mockito.verify(vaccinationsLocalDataSource).saveChildVaccinationCard(dummyCard,saveChildVaccinationCardCallback);


        assertThat(vaccinationsRepository.mCachedVaccinationCards.size(), is(1));



    }




    @Test
    public void savesVaccinationCardByRemoteRepository(){


        VaccinationCard dummyCard = new VaccinationCard();
        dummyCard.setDateCreated(new Date());


        vaccinationsRepository.saveChildVaccinationCard(dummyCard, saveChildVaccinationCardCallback);

        Mockito.verify(vaccinationsRemoteDataSource).saveChildVaccinationCard(dummyCard,saveChildVaccinationCardCallback);


        assertThat(vaccinationsRepository.mCachedVaccinationCards.size(), is(1));



    }






    @Test
    public void updateCardCacheIfRetrievedCardsFromLocalRepository(){

        Date date = Calendar.getInstance().getTime();


        //1. when cards are requested by a date
        vaccinationsRepository.getAllChildVaccinationCardsByDate(date, loadAllVaccinationCardsCallback);

       //2. make sure the local repository get is executed
        Mockito.verify(vaccinationsLocalDataSource).getAllChildVaccinationCardsByDate(dateArgumentCaptor.capture(),loadAllVaccinationCardsCallbackArgumentCaptor.capture());


        //Create some dummy Cards
        List<VaccinationCard> vaccinationCards = new ArrayList<>();
        VaccinationCard vaccinationCard = new VaccinationCard();
        vaccinationCard.setDateCreated(date);
        vaccinationCard.setChildId("3");

        VaccinationCard vaccinationCard1 = new VaccinationCard();
        vaccinationCard1.setDateCreated(date);
        vaccinationCard1.setChildId("2");

        vaccinationCards.add(vaccinationCard);
        vaccinationCards.add(vaccinationCard1);


        //3. trigger the callback with some dummy data to check how data is handled
         loadAllVaccinationCardsCallbackArgumentCaptor.getValue().onCardsLoaded(vaccinationCards);


         //4. make sure the cache has been updated
        //becoz the cache is refreshed when data has been got
        assertThat(vaccinationsRepository.mCachedVaccinationCards.size(), is(2));






    }






    @Test
    public void recordsOrUpdatesExistingVaccinationEvent(){
        //the cache has to update

        VaccinationCard dummyCard = new VaccinationCard();
        dummyCard.setDateCreated(new Date());

        VaccinationEvent vEvent = new VaccinationEvent();
        vEvent.setChildId("1");
        vEvent.setVaccinationStatus(VaccinationStatus.NOT_DONE);
        vEvent.setChildName("tony");
        Calendar dateScheduled = Calendar.getInstance();
        dateScheduled.set(2018,8,5);
        vEvent.setScheduledDate(dateScheduled.getTime());
        vEvent.setCardId("2");

        List<VaccinationEvent> events = new ArrayList<>();
        events.add(vEvent);


        dummyCard.setVaccinationEvents(events);


        //when the cards are saved
        vaccinationsRepository.saveChildVaccinationCard(dummyCard,saveChildVaccinationCardCallback);


        //try updating the event
        vaccinationsRepository.saveVaccination(vEvent,saveVaccinationCallbackMock);

        Mockito.verify(vaccinationsLocalDataSource).saveVaccination(vEvent,saveVaccinationCallbackMock);

        //when the event is saved
        VaccinationCard vaccinationCard = vaccinationsRepository.mCachedVaccinationCards.entrySet().iterator().next().getValue();

        assertThat(vaccinationCard.getVaccinationEvents().get(0).getVaccinationStatus(), is(VaccinationStatus.DONE));

    }





}
