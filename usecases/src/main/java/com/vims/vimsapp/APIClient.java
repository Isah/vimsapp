package com.vims.vimsapp;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {


    //-- private static final String ENDPOINT = "http://10.42.0.1:8080/vim/webapi/";
    //-- private static final String ENDPOINT = "http://70.32.24.226:8080/vim/webapi/";
    //-- private static final String ENDPOINT = "http://10.0.2.2:8080/";

    private static final String ENDPOINT = "http://10.42.0.1:8183/";
    private static Retrofit retrofit;


    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {

            //to fake responses
                /*
                final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new FakeInterceptor())
                        .build();
                        */
            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(100, TimeUnit.SECONDS) //40
                    .readTimeout(200, TimeUnit.SECONDS)  //60
                    .writeTimeout(200, TimeUnit.SECONDS) //90
                    .build();


            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(ENDPOINT)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();



        }
        return retrofit;

    }



}
