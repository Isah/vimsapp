package com.vims.vimsapp.recordvaccinations;


import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationLocationState;
import com.vims.vimsapp.entities.vaccinations.VaccinationStatus;


public interface AddVaccinationDataSource {



    interface RecordVaccinationCallback {

        void onVaccinationSaved(VaccinationStatus vaccinationStatus);

        void onFailed(String error);
    }


    //SAVING-------------------  the guy that implements the callback gets the values

    void saveVaccination(VaccinationLocationState locationState, VaccinationEvent vaccinationEvent, RecordVaccinationCallback recordVaccinationCallback);



}
