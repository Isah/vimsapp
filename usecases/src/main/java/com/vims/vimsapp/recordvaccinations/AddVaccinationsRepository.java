package com.vims.vimsapp.recordvaccinations;

import com.vims.vimsapp.registerchild.AddChildDataSource;
import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationLocationState;

import java.util.Map;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class AddVaccinationsRepository implements AddVaccinationDataSource{


    private static AddVaccinationsRepository INSTANCE = null;
   // private final AddVaccinationDataSource mPatientsRemoteDataSource;
    private final AddVaccinationDataSource mPatientsLocalDataSource;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    public Map<String, Caretaker> mCachedCaretakers;

    public  Map<String, Child> mCachedChidren;


    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */
    private  boolean mCaretakerCacheIsDirty = false;
    private  boolean mChildrenCacheIsDirty = false;
    private  boolean mCachedVaccCardsCacheIsDirty = false;


    // Prevent direct instantiation.

    private AddVaccinationsRepository(AddVaccinationDataSource patientsLocalDataSource) {
      //  mPatientsRemoteDataSource = checkNotNull(patientsRemoteDataSource);
        mPatientsLocalDataSource = checkNotNull(patientsLocalDataSource);

    }



    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param patientsLocalDataSource  the device storage data source
     * @return the {@link AddChildDataSource} instance
     */
    public static AddVaccinationsRepository getInstance(AddVaccinationDataSource patientsLocalDataSource) {

        if (INSTANCE == null) {
            INSTANCE = new AddVaccinationsRepository(patientsLocalDataSource);
        }
        return INSTANCE;
    }


    public static void destroyInstance() {
        INSTANCE = null;
    }



    //SAVING------------------


    @Override
    public void saveVaccination(VaccinationLocationState locationState, VaccinationEvent vaccinationEvent, RecordVaccinationCallback recordVaccinationCallback) {
        mPatientsLocalDataSource.saveVaccination(locationState,vaccinationEvent,recordVaccinationCallback);
    }


}
