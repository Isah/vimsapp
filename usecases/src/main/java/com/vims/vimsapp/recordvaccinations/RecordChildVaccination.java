package com.vims.vimsapp.recordvaccinations;

import android.support.annotation.NonNull;

import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationLocationState;
import com.vims.vimsapp.entities.vaccinations.VaccinationStatus;


import java.util.Calendar;
import java.util.Date;

/**
 * Created by root on 5/26/18.
 */

public class RecordChildVaccination implements UseCase<RecordChildVaccination.RequestValues, RecordChildVaccination.ResponseValue>{


    private static RecordChildVaccination INSTANCE = null;


    private AddVaccinationsRepository patientsRepository;

    private RecordChildVaccination(AddVaccinationsRepository patientsRepository){

        this.patientsRepository = patientsRepository;

    }

    public static RecordChildVaccination getInstance(AddVaccinationsRepository patientsRepository) {

        if (INSTANCE == null) {
            INSTANCE = new RecordChildVaccination(patientsRepository);
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }



    @Override
    public void execute(RequestValues param,final OutputBoundary<ResponseValue> delivery) {


        final VaccinationEvent vaccinationEvent = param.getVaccinationEvent();
        VaccinationLocationState locationState = param.getVaccinationLocationState();



        if(!isStatusDONE(vaccinationEvent.getVaccinationStatus())) {


            patientsRepository.saveVaccination(locationState,vaccinationEvent, new AddVaccinationDataSource.RecordVaccinationCallback() {
                @Override
                public void onVaccinationSaved(VaccinationStatus vaccinationStatus) {

                    delivery.onSuccess(new ResponseValue(vaccinationEvent));

                }

                @Override
                public void onFailed(String error) {

                    delivery.onError(error);


                }
            });

        }
        else{


            delivery.onError("failed, status is done");



        }


    }


    private boolean isVaccinationDateValid(Date vaccination_date){

        Date calendarDate = Calendar.getInstance().getTime();
        int currentyear = calendarDate.getYear();
        int currentmonth = calendarDate.getMonth();
        int currentday = calendarDate.getDay();


        if(vaccination_date.getYear() == currentyear && vaccination_date.getMonth() == currentmonth && vaccination_date.getDay() == currentday){

            //vaccination should only take place if the date your vaccinating is that current date
            // that is if you set in another vaccination date in the phone not the current one
            return true;
        }
        else{

            return  false;
        }


    }



    private boolean isStatusDONE(VaccinationStatus status){
        boolean isStatusDONE;

       if(status == VaccinationStatus.DONE){

           isStatusDONE = true;
       }
       else{

           isStatusDONE =false;
       }

       return isStatusDONE;

    }



    //it implements such that its of a right type to be accepted in thhe execute method
    public static final class RequestValues implements UseCase.RequestValues {

        private final VaccinationEvent vaccinationEvent;
        private final VaccinationLocationState vaccinationLocationState;


        public RequestValues(VaccinationLocationState vaccinationLocationState, @NonNull VaccinationEvent vaccinationEvent) {
            this.vaccinationEvent = vaccinationEvent;
            this.vaccinationLocationState = vaccinationLocationState;

        }

        public VaccinationLocationState getVaccinationLocationState() {
            return vaccinationLocationState;
        }

        public VaccinationEvent getVaccinationEvent() {
            return vaccinationEvent;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private VaccinationEvent response;

        public ResponseValue(@NonNull VaccinationEvent savedResponseMsg) {
            this.response = savedResponseMsg;
        }

        public VaccinationEvent getmResponse() {
            return response;
        }
    }





}
