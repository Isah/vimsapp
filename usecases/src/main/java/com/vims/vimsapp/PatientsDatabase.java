package com.vims.vimsapp;


import android.app.Application;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.util.Log;

import com.vims.vimsapp.registerchild.local.CaretakerDao;
import com.vims.vimsapp.registerchild.local.ChildDao;
import com.vims.vimsapp.registerchild.local.PatientsSyncTimeDao;
import com.vims.vimsapp.registerchild.local.VaccinationEventDao;
import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.child.SyncUpdateTime;
import com.vims.vimsapp.entities.schedule.HospitalImmunisationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationCard;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.manageorders.VaccineOrder;
import com.vims.vimsapp.manageorders.VaccineOrderDao;
import com.vims.vimsapp.utilities.DateConverter;
import com.vims.vimsapp.utilities.ListTypeConverter;
import com.vims.vimsapp.utilities.StatusEnumConverter;
import com.vims.vimsapp.utilities.VaccinationCardConverter;

// List of the entry classes and associated TypeConverters
// List of the entry classes and associated TypeConverters MyTypeConverter.class,
@Database(entities = {Caretaker.class,Child.class,SyncUpdateTime.class,VaccinationCard.class, VaccinationEvent.class, HospitalImmunisationEvent.class, VaccineOrder.class}, version = 1, exportSchema = false)
@TypeConverters({DateConverter.class, StatusEnumConverter.class, ListTypeConverter.class, VaccinationCardConverter.class})
public abstract class PatientsDatabase extends RoomDatabase {


    private static final String LOG_TAG = PatientsDatabase.class.getSimpleName();
    private static final String DATABASE_NAME = "patients";

    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static PatientsDatabase sInstance;
    //.allowMainThreadQueries()
    public static PatientsDatabase getInstance(Application application) {
        Log.d(LOG_TAG, "Getting the database");
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = Room.databaseBuilder(application,
                        PatientsDatabase.class, PatientsDatabase.DATABASE_NAME)

                        .fallbackToDestructiveMigration()
                        .build();
                Log.d(LOG_TAG, "Made new database");
            }
        }
        return sInstance;
    }

    // The associated DAOs for the database
    public abstract CaretakerDao getCaretakerDao();
    public abstract ChildDao getChildDao();
    public abstract PatientsSyncTimeDao getPatientsSyncTimeDao();


    // The associated DAOs for the database
  //  public abstract VaccinationCardDao getVaccinationCardDao();
    public abstract VaccinationEventDao getVaccinationEventDao();
    public abstract VaccineOrderDao getVaccineOrderDao();






}
