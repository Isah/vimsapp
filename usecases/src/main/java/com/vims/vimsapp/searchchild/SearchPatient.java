package com.vims.vimsapp.searchchild;

import android.support.annotation.NonNull;

import com.vims.vimsapp.UseCase;

import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;

import java.util.List;

public class SearchPatient implements UseCase<SearchPatient.RequestValues,SearchPatient.ResponseValue>{


    private static SearchPatient INSTANCE = null;


    ViewChildRepository patientsRepository;

    private SearchPatient(ViewChildRepository patientsRepository){

        this.patientsRepository = patientsRepository;

    }

    public static SearchPatient getInstance(ViewChildRepository patientsRepository) {

        if (INSTANCE == null) {
            INSTANCE = new SearchPatient(patientsRepository);
        }
        return INSTANCE;
    }


    @Override
    public void execute(final RequestValues param,final OutputBoundary<ResponseValue> delivery) {

        final CharSequence searchText = param.getmSearchText();

        //ill do the search my self

        if(param.getmPatient() == Patient.CARETAKER){

            patientsRepository.searchCaretaker(searchText.toString(), new ViewChildDataSource.SearchCaretakerCallback() {
                @Override
                public void onSearchedCaretaker(List<Caretaker> caretakers) {

                    ResponseValue responseValue = new ResponseValue();
                    responseValue.setCaretakers(caretakers);

                    delivery.onSuccess(responseValue);

                }

                @Override
                public void onFailed() {
                    delivery.onError("No Caretaker Found");

                }
            });

        }else{


            patientsRepository
                    .searchChild(searchText.toString(), new ViewChildDataSource.SearchChildCallback() {
                @Override
                public void onSearchedChildren(List<Child> children) {

                    ResponseValue responseValue = new ResponseValue();
                    responseValue.setChildren(children);
                    delivery.onSuccess(responseValue);

                }

                @Override
                public void onFailed(String error) {

                    delivery.onError(error);

                }
            });



        }




    }

    public enum Patient{
        CHILD,CARETAKER
    }

    //it implements such that its of a right type to be accepted in thhe execute method
    public static final class RequestValues implements UseCase.RequestValues {

        private final CharSequence mSearchText;
        private final Patient mPatient;

        public RequestValues(@NonNull CharSequence mSearchText, Patient patient) {
            this.mSearchText = mSearchText;
            this.mPatient = patient;
        }

        public CharSequence getmSearchText() {
            return mSearchText;
        }

        public Patient getmPatient() {
            return mPatient;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private  List<Caretaker> caretakers;
        private  List<Child> children;

        public List<Child> getChildren() {
            return children;
        }

        public void setCaretakers(List<Caretaker> caretakers) {
            this.caretakers = caretakers;
        }

        public void setChildren(List<Child> children) {
            this.children = children;
        }

        public List<Caretaker> getCaretakers() {
            return caretakers;
        }
    }
















}
