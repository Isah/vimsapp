package com.vims.vimsapp.searchchild;

import android.app.Application;
import android.support.annotation.NonNull;
import android.util.Base64;

import com.vims.vimsapp.entities.child.Caretaker;

import com.vims.vimsapp.utilities.PreferenceHandler;

import java.io.UnsupportedEncodingException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewChildRemoteRetrofitDataSource implements ViewChildDataSource {


    private ViewChildApi mPatientsApi;


    private static volatile ViewChildRemoteRetrofitDataSource INSTANCE;
    private PreferenceHandler preferenceHandler;
    private Application application;

    // Prevent direct instantiation.
    private ViewChildRemoteRetrofitDataSource(@NonNull ViewChildApi patientsApi, Application application) {
        this.mPatientsApi = patientsApi;
        this.application = application;
        preferenceHandler = PreferenceHandler.getInstance(application);

    }

    public static ViewChildRemoteRetrofitDataSource getInstance(@NonNull ViewChildApi patientsApi, Application application) {
        if (INSTANCE == null) {
            synchronized (ViewChildRemoteRetrofitDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ViewChildRemoteRetrofitDataSource(patientsApi, application);
                }
            }
        }
        return INSTANCE;
    }



    private String API_USER_ID;
    private String API_PASSWORD;
    private String API_TOKEN;

    public  String getAuthToken() {

        //API_USER_NAME = "petr";
        API_USER_ID = preferenceHandler.getPref(PreferenceHandler.USER_ID);
        API_TOKEN = preferenceHandler.getPref(PreferenceHandler.USER_TOKEN);

        byte[] data = new byte[0];
        try {
            data = (API_USER_ID+ ":" + API_TOKEN).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }







    @Override
    public void getCaretakers(final @NonNull LoadCaretakersCallback callback) {

        String hospitalId = preferenceHandler.getPref(PreferenceHandler.USER_HOSPITAL_ID);
        //call object used to execute the request to the api
        //Call<List<Caretaker>> callAsync = mPatientsApi.getCaretakers(getAuthToken());
        Call<List<Caretaker>> callAsync = mPatientsApi.getVimsCaretakers(getAuthToken(),hospitalId);

        //background
        callAsync.enqueue(new Callback<List<Caretaker>>() {
            @Override
            public void onResponse(@NonNull Call<List<Caretaker>> call, @NonNull Response<List<Caretaker>> response) {


                if(response.isSuccessful()) {
                    callback.onCaretakersLoaded(response.body());
                }
                else if(response.code() == 401){
                    callback.onDataNotAvailable("UnAuthorized!");
                }
                else if(response.code() == 404){
                    callback.onDataNotAvailable("404");
                }
                else{
                    callback.onDataNotAvailable(response.code()+" "+response.message());
                }

            }

            @Override
            public void onFailure(@NonNull Call<List<Caretaker>> call, @NonNull Throwable throwable) {

                String msg ="not network";

                callback.onDataNotAvailable(msg);
            }
        });


    }


    @Override
    public void searchCaretaker(@NonNull String caretakerName, SearchCaretakerCallback searchCaretakerCallback) {


    }

    @Override
    public void getChildrenByCaretakerId(String caretakerId, LoadChildCallback callback) {

    }

    @Override
    public void getChildById(String childId, GetChildCallback callback) {

    }

    @Override
    public void getChildren(@NonNull LoadChildCallback callback) {

    }

    @Override
    public void getCaretaker(@NonNull String caretakerId, @NonNull GetCaretakerCallback callback) {

    }

    @Override
    public void getCaretakersToSync(@NonNull GetCaretakersToSyncCallback callback) {

    }

    @Override
    public void checkChildCardsSize(LoadChildCardsSizeCallback loadCardsSizeCheckCallback) {

    }

    @Override
    public void searchChild(@NonNull String searchText, SearchChildCallback searchChildrenCallback) {

    }

    @Override
    public void refreshEvents() {

    }



    @Override
    public void deleteAllCaretakers() {

    }

    @Override
    public void deleteCaretaker(@NonNull String caretakerId) {

    }

    @Override
    public void refreshCaretakers() {

    }

    @Override
    public void refreshChildren() {

    }
}
