package com.vims.vimsapp.searchchild;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.reports.VaccinationEventsPack;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class ViewChildRepository implements ViewChildDataSource {


    private static ViewChildRepository INSTANCE = null;
    private final ViewChildDataSource mPatientsRemoteDataSource;
    private final ViewChildDataSource mPatientsLocalDataSource;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    public Map<String, Caretaker> mCachedCaretakers;

    public  Map<String, Child> mCachedChidren;


    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */
    private  boolean mCaretakerCacheIsDirty = false;
    private  boolean mChildrenCacheIsDirty = false;
    private  boolean mCachedVaccCardsCacheIsDirty = false;


    // Prevent direct instantiation.

    private ViewChildRepository(ViewChildDataSource patientsRemoteDataSource, ViewChildDataSource patientsLocalDataSource) {
        mPatientsRemoteDataSource = checkNotNull(patientsRemoteDataSource);
        mPatientsLocalDataSource = checkNotNull(patientsLocalDataSource);

    }



    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param patientsRemoteDataSource the backend data source
     * @param patientsLocalDataSource  the device storage data source
     * @return the {@link ViewChildDataSource} instance
     */
    public static ViewChildRepository getInstance(ViewChildDataSource patientsRemoteDataSource, ViewChildDataSource patientsLocalDataSource) {

        if (INSTANCE == null) {
            INSTANCE = new ViewChildRepository(patientsRemoteDataSource, patientsLocalDataSource);
        }
        return INSTANCE;
    }


    public static void destroyInstance() {
        INSTANCE = null;
    }









    @Override
    public void getCaretakers(final @NonNull LoadCaretakersCallback callback) {

        checkNotNull(callback);


        /*
        // Respond immediately with cache if available and not dirty
        if (mCachedCaretakers != null && !mCaretakerCacheIsDirty) {
            callback.onCaretakersLoaded(new ArrayList<>(mCachedCaretakers.values()));
            return;
        }

        if (mCaretakerCacheIsDirty) {
            // If the cache is dirty we need to fetch new data from the network.
            getCaretakersFromRemoteDataSource(callback);

        } else {
            // Query the local storage if available. If not, query the network.
            mPatientsLocalDataSource.getCaretakers(new LoadCaretakersCallback() {
                @Override
                public void onCaretakersLoaded(List<Caretaker> tasks) {
                    refreshCaretakerCache(tasks);
                    callback.onCaretakersLoaded(new ArrayList<>(mCachedCaretakers.values()));
                }

                @Override
                public void onDataNotAvailable(String ms) {
                   callback.onDataNotAvailable(ms);
                }
            });
        }
        */

        mPatientsLocalDataSource.getCaretakers(new LoadCaretakersCallback() {
            @Override
            public void onCaretakersLoaded(List<Caretaker> tasks) {
                refreshCaretakerCache(tasks);
                callback.onCaretakersLoaded(tasks);
                //new ArrayList<>(mCachedCaretakers.values())
            }

            @Override
            public void onDataNotAvailable(String ms) {
                callback.onDataNotAvailable(ms);
            }
        });

    }


    @Override
    public void getCaretaker(final @NonNull String caretakerId,final  @NonNull GetCaretakerCallback callback) {

        checkNotNull(caretakerId);
        checkNotNull(callback);

        Caretaker cachedCaretaker = getCachedCaretakerWithId(caretakerId);

        // Respond immediately with cache if available
        if (cachedCaretaker != null) {
            callback.onCaretakerLoaded(cachedCaretaker);
            return;
        }

        // Load from server/persisted if needed.

        // Is the caretaker in the local data source? If not, query the network.
        mPatientsLocalDataSource.getCaretaker(caretakerId, new GetCaretakerCallback() {
            @Override
            public void onCaretakerLoaded(Caretaker caretaker) {
                // Do in memory cache update to keep the app UI up to date
                if (mCachedCaretakers == null) {
                    mCachedCaretakers = new LinkedHashMap<>();
                }
                mCachedCaretakers.put(caretaker.getCaretakerId(), caretaker);
                callback.onCaretakerLoaded(caretaker);
            }

            @Override
            public void onDataNotAvailable() {
                mPatientsRemoteDataSource.getCaretaker(caretakerId, new GetCaretakerCallback() {
                    @Override
                    public void onCaretakerLoaded(Caretaker caretaker) {
                        // Do in memory cache update to keep the app UI up to date
                        if (mCachedCaretakers == null) {
                            mCachedCaretakers = new LinkedHashMap<>();
                        }
                        mCachedCaretakers.put(caretaker.getCaretakerId(), caretaker);
                        callback.onCaretakerLoaded(caretaker);
                    }

                    @Override
                    public void onDataNotAvailable() {
                        callback.onDataNotAvailable();
                    }
                });
            }
        });


    }

    //used by getcaretaker
    @Nullable
    private Caretaker getCachedCaretakerWithId(@NonNull String id) {
        checkNotNull(id);
        if (mCachedCaretakers == null || mCachedCaretakers.isEmpty()) {
            return null;
        } else {
            return mCachedCaretakers.get(id);
        }
    }

    @Nullable
    private Child getCachedChildWithId(@NonNull String id) {
        checkNotNull(id);
        if (mCachedChidren == null || mCachedChidren.isEmpty()) {
            return null;
        } else {
            return mCachedChidren.get(id);
        }
    }



    @Override
    public void getChildById(final String childId,final GetChildCallback callback) {


        checkNotNull(childId);
        checkNotNull(callback);

        Child cachedChild = getCachedChildWithId(childId);

        /*
        // Respond immediately with cache if available
        if (cachedChild != null) {
            callback.onChildLoaded(cachedChild);
            return;
        }
        */

        // Load from server/persisted if needed.

        // Is the caretaker in the local data source? If not, query the network.
        mPatientsLocalDataSource.getChildById(childId, new GetChildCallback() {
            @Override
            public void onChildLoaded(Child child) {

                /*
                // Do in memory cache update to keep the app UI up to date
                if (mCachedChidren == null) {
                    mCachedChidren = new LinkedHashMap<>();
                }
                mCachedChidren.put(child.getId(), child);
                */

                callback.onChildLoaded(child);

            }

            @Override
            public void onDataNotAvailable() {


                mPatientsRemoteDataSource.getChildById(childId, new GetChildCallback() {
                    @Override
                    public void onChildLoaded(Child child) {

                        callback.onChildLoaded(child);
                    }

                    @Override
                    public void onDataNotAvailable() {

                        callback.onDataNotAvailable();

                    }
                });


            }
        });


    }



    @Override
    public void getChildrenByCaretakerId(final String caretakerId,final LoadChildCallback callback) {

        checkNotNull(callback);

        /*
        List<Child> children = new ArrayList<>();

        // Respond immediately with cache if available and not dirty
        if (mCachedChidren != null && !mChildrenCacheIsDirty) {
            for (Map.Entry<String, Child> map : mCachedChidren.entrySet()) {
                String key = map.getKey();
                Child child = map.getValue();

                if (child.getCaretakerId().equals(caretakerId)) {
                  //  children = map.getValue().getChildren();
                    children.add(child);

                }

            }

            callback.onChildrenLoaded(children);

            return;
        }
        */

        if (mCaretakerCacheIsDirty) {
            // If the cache is dirty we need to fetch new data from the network.
            //getTasksFromRemoteDataSource(callback);
        } else {
            // Query the local storage if available. If not, query the network.
            mPatientsLocalDataSource.getChildrenByCaretakerId(caretakerId, new LoadChildCallback() {
                @Override
                public void onChildrenLoaded(List<Child> children) {
                    refreshChildrenCache(children);
                    // callback.onChildrenLoaded(new ArrayList<>(mCachedChidren.values()));
                    callback.onChildrenLoaded(children);

                }

                @Override
                public void onDataNotAvailable() {

                }
            });
        }


    }


    @Override
    public void getChildren(final LoadChildCallback callback) {

        checkNotNull(callback);

        /*
        // Respond immediately with cache if available and not dirty
        if (mCachedChidren != null && !mChildrenCacheIsDirty) {
            callback.onChildrenLoaded(new ArrayList<>(mCachedChidren.values()));
            return;
        }
        */

        if (mChildrenCacheIsDirty) {
            // If the cache is dirty we need to fetch new data from the network.
            //getTasksFromRemoteDataSource(callback);
        } else {
            // Query the local storage if available. If not, query the network.
            mPatientsLocalDataSource.getChildren(new LoadChildCallback() {
                @Override
                public void onChildrenLoaded(List<Child> children) {
                    refreshChildrenCache(children);
                    // callback.onChildrenLoaded(new ArrayList<>(mCachedChidren.values()));
                    callback.onChildrenLoaded(children);

                }

                @Override
                public void onDataNotAvailable() {

                    callback.onDataNotAvailable();

                }
            });
        }


    }


    @Override
    public void getCaretakersToSync(@NonNull GetCaretakersToSyncCallback callback) {

        mPatientsLocalDataSource.getCaretakersToSync(callback);

    }

    //REFRESHING-------------------

    @Override
    public void refreshCaretakers() {
        mCaretakerCacheIsDirty = true;
    }

    @Override
    public void refreshChildren() {
        mChildrenCacheIsDirty = true;
    }




    private void refreshCaretakerCache(List<Caretaker> caretakers) {
        if (mCachedCaretakers == null) {
            mCachedCaretakers = new LinkedHashMap<>();
        }
        mCachedCaretakers.clear();
        for (Caretaker caretaker : caretakers) {
            mCachedCaretakers.put(caretaker.getCaretakerId(), caretaker);
        }
        mCaretakerCacheIsDirty = false;
    }

    private void refreshChildrenCache(List<Child> children) {
        if (mCachedChidren== null) {
            mCachedChidren = new LinkedHashMap<>();
        }
        mCachedChidren.clear();

        //mCachedChidren = children;

        for (Child child : children) {
            mCachedChidren.put(child.getCaretakerId(), child);
        }


        mChildrenCacheIsDirty = false;
    }


    private  final int HOME_SCREEN =0;
    private  final int STATS_SCREEN = 1;




    @Override
    public void checkChildCardsSize(LoadChildCardsSizeCallback loadCardsSizeCheckCallback) {

        mPatientsLocalDataSource.checkChildCardsSize(loadCardsSizeCheckCallback);

    }


    @Override
    public void searchCaretaker(@NonNull String caretakerName, SearchCaretakerCallback searchCaretakerCallback) {
        mPatientsLocalDataSource.searchCaretaker(caretakerName,searchCaretakerCallback);
    }

    @Override
    public void searchChild(@NonNull String searchText, SearchChildCallback searchChildrenCallback) {
        mPatientsLocalDataSource.searchChild(searchText,searchChildrenCallback);
    }

    @Override
    public void refreshEvents() {
       // mCachedEventsCacheIsDirty = true;
        //mCachedEventsPackCacheIsDirty = true;
    }

    @Override
    public void deleteAllCaretakers() {

    }

    @Override
    public void deleteCaretaker(@NonNull String caretakerId) {

    }







}
