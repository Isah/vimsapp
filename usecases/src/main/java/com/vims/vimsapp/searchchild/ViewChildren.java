package com.vims.vimsapp.searchchild;


import android.support.annotation.NonNull;
import android.util.Log;

import com.vims.vimsapp.UseCase;

import com.vims.vimsapp.entities.child.Child;

import java.util.List;

/**
 * Created by root on 6/2/18.
 */

public class ViewChildren implements UseCase<ViewChildren.RequestValues,ViewChildren.ResponseValue> {


    private ViewChildRepository mPatientsRepository;

    public ViewChildren(ViewChildRepository patientsRepository) {

        this.mPatientsRepository = patientsRepository;

    }


    @Override
    public void execute(RequestValues requestValues, OutputBoundary<ResponseValue> deliveryMechanism) {


        RequestType requestType = requestValues.getmRequestType();
        if (requestType == RequestType.CHILD){

            executeChildRequest(requestValues.getmChildId(), deliveryMechanism);

        }
        else{//children


            Log.d("Children request: ","children");


            executeChildrenRequest(deliveryMechanism);

        }



    }





    private void executeChildrenRequest(final OutputBoundary<ResponseValue> deliveryMechanism){

        mPatientsRepository.getChildren(new ViewChildDataSource.LoadChildCallback() {
            @Override
            public void onChildrenLoaded(List<Child> children) {




                ResponseValue responseValue = new ResponseValue();
                responseValue.setChildren(children);
                deliveryMechanism.onSuccess(responseValue);
            }

            @Override
            public void onDataNotAvailable() {

                deliveryMechanism.onError("No Children Available");

            }
        });



    }



    private void executeChildRequest(String childId,final OutputBoundary<ResponseValue> deliveryMechanism){


        mPatientsRepository.getChildById(childId, new ViewChildDataSource.GetChildCallback() {
            @Override
            public void onChildLoaded(Child child) {
                ResponseValue responseValue = new ResponseValue();
                responseValue.setChild(child);

                deliveryMechanism.onSuccess(responseValue);

            }

            @Override
            public void onDataNotAvailable() {
                deliveryMechanism.onError("No child found!");

            }
        });



    }










   public enum RequestType{
        CHILD,CHILDREN
   }


    //it implements such that its of a right type to be accepted in thhe execute method
    public static final class RequestValues implements UseCase.RequestValues {

        private final String mChildId;
        private final RequestType mRequestType;

        public RequestValues(@NonNull String childId, @NonNull  RequestType requestType) {
            mChildId = childId;
            mRequestType = requestType;

        }

        public String getmChildId() {
            return mChildId;
        }

        public RequestType getmRequestType() {
            return mRequestType;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private List<Child> children;
        private Child child;

        public ResponseValue() {
         //   this.children = children;
           // this.child = child;
        }

        public void setChildren(@NonNull List<Child> childrens) {

           // children = new ArrayList<>();

            /*
            for (Map.Entry<String, Child> childEntry : childrens.entrySet()) {
                String key = childEntry.getKey();
                Child child = childEntry.getValue();

                children.add(child);
            }
            */
            children = childrens;

        }

        public void setChild(@NonNull Child child) {
            this.child = child;
        }

        public List<Child>  getmChildren() {
            return children;
        }

        public Child getChild() {
            return child;
        }
    }



}
