package com.vims.vimsapp.searchchild;

import android.support.annotation.NonNull;

import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.reports.VaccinationEventsPack;
import com.vims.vimsapp.entities.schedule.ReadScheduleEntity;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.manageorders.Dosage;

import java.util.List;

public interface ViewChildDataSource {



    //Interface Callbacks for loading
    interface LoadCaretakersCallback {

        void onCaretakersLoaded(List<Caretaker> caretakers);

        void onDataNotAvailable(String erroe);
    }


    interface GetCaretakerCallback {

        void onCaretakerLoaded(Caretaker task);

        void onDataNotAvailable();
    }

    interface GetCaretakersToSyncCallback {

        void onCaretakersLoaded(List<Caretaker> caretakers);

        void onCaretakerNotAvailable();
    }






    interface LoadChildCallback {

        void onChildrenLoaded(List<Child> children);

        void onDataNotAvailable();
    }


    interface GetChildCallback {

        void onChildLoaded(Child child);

        void onDataNotAvailable();
    }

    interface SearchCaretakerCallback {

        void onSearchedCaretaker(List<Caretaker> caretakers);

        void onFailed();
    }
    interface SearchChildCallback {

        void onSearchedChildren(List<Child> caretakers);

        void onFailed(String error);
    }


    //GETTING------------------
    void getChildrenByCaretakerId(String caretakerId, LoadChildCallback callback);
    void getChildById(String childId, GetChildCallback callback);
    void getChildren(@NonNull LoadChildCallback callback);//loads children
    void getCaretakers(@NonNull LoadCaretakersCallback callback);
    void getCaretaker(@NonNull String caretakerId, @NonNull GetCaretakerCallback callback);
    void getCaretakersToSync(@NonNull GetCaretakersToSyncCallback callback);

    void searchCaretaker(@NonNull String caretakerName, SearchCaretakerCallback searchCaretakerCallback);
    void searchChild(@NonNull String searchText, SearchChildCallback searchChildrenCallback);






    //ADDITIONS

    interface LoadVaccinationEventsPackCallback {

        void onEventsPackLoaded(VaccinationEventsPack vaccinationEventsPack);

        void packUnAvailable();
    }

    interface LoadChildCardsSizeCallback {

        void onCardsSizeLoaded(int size);

        void onCardNotAvailable();
    }


    interface LoadAllVaccinationEventsCallback {

        void onVaccinationEventsLoaded(List<VaccinationEvent> vaccinationEvents);

        void onEventssNotAvailable();
    }

    interface LoadDosageUsageCallback {

        void onDosageLoaded(List<Dosage> dosages);

        void onFailed();
    }

    interface LoadReadScheduleEvents{

        void onEventsLoaded(ReadScheduleEntity readScheduleEntity);

        void onEventsNotAvailable(String error);
    }




    void checkChildCardsSize(LoadChildCardsSizeCallback loadCardsSizeCheckCallback);
    //void getVaccinationEventsPack(int whoWants, String vaccine, int yearOfEvents, LoadVaccinationEventsPackCallback packCallback);
    //void getVaccinationEvents(LoadAllVaccinationEventsCallback loadAllVaccinationEventsCallback);
    void refreshEvents();
    //void getDosageUsage(int month, LoadDosageUsageCallback usageCallback);
   // void getReadScheduleEvents(int month, LoadReadScheduleEvents eventsCallback);



    //DELETING-------------------
    void deleteAllCaretakers();

    void deleteCaretaker(@NonNull String caretakerId);



    //REFRESHING---------------
    void refreshCaretakers();
    void refreshChildren();




}
