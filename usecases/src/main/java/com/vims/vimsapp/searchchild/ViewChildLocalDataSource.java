package com.vims.vimsapp.searchchild;

import android.app.Application;
import android.support.annotation.NonNull;
import android.util.Log;

import com.vims.vimsapp.registerchild.local.CaretakerDao;
import com.vims.vimsapp.registerchild.local.ChildDao;
import com.vims.vimsapp.registerchild.local.PatientsSyncTimeDao;
import com.vims.vimsapp.registerchild.local.VaccinationEventDao;
import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.child.SyncUpdateTime;
import com.vims.vimsapp.utilities.AppExecutors;
import com.vims.vimsapp.utilities.PreferenceHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class ViewChildLocalDataSource implements ViewChildDataSource {


    private static volatile ViewChildLocalDataSource INSTANCE;

    private CaretakerDao mCaretakerDao;
    private ChildDao mChildDao;


    private VaccinationEventDao mvaccinationEventDao;

    private PatientsSyncTimeDao mPatientsSyncTimeDao;
    private AppExecutors mAppExecutors;

    private Application mContext;

    private PreferenceHandler preferenceHandler;

    private String USER_ID;
    private String USER_HOSPITAL_ID;




    private ViewChildLocalDataSource(@NonNull AppExecutors appExecutors, @NonNull CaretakerDao caretakerDao, ChildDao childDao, PatientsSyncTimeDao patientsSyncTimeDao, VaccinationEventDao vaccinationEventDao, Application context) {
        mAppExecutors = appExecutors;
        mCaretakerDao = caretakerDao;
        mChildDao = childDao;
        this.mPatientsSyncTimeDao = patientsSyncTimeDao;
        mvaccinationEventDao = vaccinationEventDao;
        this.mContext = context;
        preferenceHandler = PreferenceHandler.getInstance(mContext);


    }


    public static ViewChildLocalDataSource getInstance(@NonNull AppExecutors appExecutors, @NonNull CaretakerDao caretakerDao, ChildDao childDao, PatientsSyncTimeDao patientsSyncTimeDao, VaccinationEventDao vaccinationEventDao, Application context) {
        if (INSTANCE == null) {
            synchronized (ViewChildLocalDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ViewChildLocalDataSource(appExecutors, caretakerDao,childDao,patientsSyncTimeDao,vaccinationEventDao, context);
                }
            }
        }
        return INSTANCE;
    }




    @Override
    public void deleteAllCaretakers() {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                int rowsAffected = mCaretakerDao.deleteCaretakers();
                if(rowsAffected > 0) {
                    mChildDao.deleteChldren();
                }

            }
        };

        mAppExecutors.diskIO().execute(runnable);

    }




    @Override
    public void getCaretakers(final @NonNull LoadCaretakersCallback callback) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                // List<CaretakerWithChildren> cwc = mCaretakerDao.getCaretakerWithChhildren();

                final List<Caretaker> caretakers = mCaretakerDao.getCaretakersLtd(80);
                // ArrayList<Caretaker> caretakers1 = new ArrayList<>(caretakers);
                // final  List<Caretaker> cares = getCaretakersWithChildren(caretakers1);


                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (caretakers.isEmpty()) {
                            // This will be called if the table is new or just empty.
                            callback.onDataNotAvailable("no caretakers");
                        } else {


                            callback.onCaretakersLoaded(caretakers);
                        }
                    }
                });
            }
        };

        mAppExecutors.diskIO().execute(runnable);

    }

    @Override
    public void getCaretaker(final @NonNull String caretakerId,final  @NonNull GetCaretakerCallback callback) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final Caretaker caretaker = mCaretakerDao.getCaretakerById(caretakerId);

                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (caretaker != null) {
                            callback.onCaretakerLoaded(caretaker);
                        } else {
                            callback.onDataNotAvailable();
                        }
                    }
                });
            }
        };

        mAppExecutors.diskIO().execute(runnable);

    }

    private Map<String, Child> childMap(List<Child> children){

        Map<String, Child> map = new HashMap<>();
        for(Child child: children){
            map.put(child.getChildId(), child);
        }

        return map;

    }

    @Override
    public void getChildren(final @NonNull LoadChildCallback callback) {

        Runnable childrenRunnable = new Runnable() {
            @Override
            public void run() {

                List<Child> children =  mChildDao.getChildrenLtd(80);

                if(!children.isEmpty()){

                    callback.onChildrenLoaded(children);

                }
                else{

                    callback.onDataNotAvailable();

                }


            }
        };

        mAppExecutors.diskIO().execute(childrenRunnable);

    }

    @Override
    public void getChildById(final String childId,final GetChildCallback callback) {

        Runnable childRunnable = new Runnable() {
            @Override
            public void run() {

                final Child child =  mChildDao.getChildById(childId);

                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {

                        if(child != null){

                            callback.onChildLoaded(child);

                        }
                        else{

                            callback.onDataNotAvailable();

                        }


                    }
                });

            }
        };

        mAppExecutors.diskIO().execute(childRunnable);

    }

    @Override
    public void getChildrenByCaretakerId(final String caretakerId,final LoadChildCallback callback) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                List<Child> children = mChildDao.getChildrenByCaretakerId(caretakerId);

                if (children.isEmpty()) {
                    // This will be called if the table is new or just empty.
                    callback.onDataNotAvailable();
                } else {

                    callback.onChildrenLoaded(children);
                }

            }
        };

        mAppExecutors.diskIO().execute(runnable);

    }


    private boolean isCaretakerPresent(String id, List<Caretaker> caretakers){

        for(Caretaker caretaker: caretakers){
            if(caretaker.getCaretakerId().equals(id)){
                return true;
            }
        }
        return false;
    }

    private List<Caretaker> getUpdatedCaretakers( List<SyncUpdateTime> updateTimes){
        Log.d("SyncOut", "update times size: "+updateTimes.size());
        List<Caretaker> thecaretakers = mCaretakerDao.getCaretakers();
        ArrayList<Caretaker> mycaretakers = new ArrayList<>();

        for(SyncUpdateTime updateTime: updateTimes){
            for(Caretaker caretaker: thecaretakers){
                if(updateTime.getCaretakerId().equals(caretaker.getCaretakerId())) {
                    mycaretakers.add(caretaker);
                }
            }
        }

        for(Caretaker caretaker: mycaretakers) {
            Log.d("SyncOut", "update times caretaker size:: " + mycaretakers.size()+" a: "+caretaker.getFirstName());
        }

        return mycaretakers;
    }

    ArrayList<Caretaker> childrenCaretakers = new ArrayList<>();

    private List<Caretaker> getCaretakersWithChildren(List<Caretaker> caretakers){
        for(Caretaker caretaker: caretakers){

            List<Child> children =  mChildDao.getChildrenByCaretakerId(caretaker.getCaretakerId());
            caretaker.setChildren(children);
            childrenCaretakers.add(caretaker);
            for(Child child: children) {
                Log.d("SyncOut", "my kids: " + child.getFirstName());
            }
            //  caretaker.setChildren());
        }
        return childrenCaretakers;
    }


    @Override
    public void getCaretakersToSync(@NonNull final GetCaretakersToSyncCallback callback) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                int no = mCaretakerDao.getSize();
                if(no > 0){//1. There maybe some caretakers to sync

                    ArrayList<Caretaker> caretakerToSync = new ArrayList<>();


                    //2. Find if any of them was registered after sync time at begin reg > 0
                    long latest_sync_time = preferenceHandler.getLongPref("last_sync_time");
                    List<Caretaker> caretakers_after_sync = mCaretakerDao.getCaretakersToSync(latest_sync_time);

                    //4. combine them but no duplicates
                    if(!caretakers_after_sync.isEmpty()){
                        caretakerToSync.addAll(caretakers_after_sync);
                        for(Caretaker caretaker: caretakers_after_sync){

                            Log.d("SyncOut","caretakers after sync: "+caretaker.getFirstName());

                        }
                    }else{
                        Log.d("SyncOut","caretakers after sync: non after sync");
                    }


                    //3. find if any was updated after sync
                    int no_updates = mPatientsSyncTimeDao.getNoPatientsSyncTimes();
                    if(no_updates > 0){// means there were some updates
                        List<SyncUpdateTime> syncTimes = mPatientsSyncTimeDao.getUpdateTimesAfterLatestSync(latest_sync_time);
                        List<Caretaker> caretakers_updated = getUpdatedCaretakers(syncTimes);

                        if(!caretakers_updated.isEmpty()){

                            for(Caretaker caretaker: caretakers_updated){
                                boolean isPresent = isCaretakerPresent(caretaker.getCaretakerId(),caretakerToSync);
                                if(!isPresent) {
                                    caretakerToSync.add(caretaker);
                                }
                                Log.d("SyncOut","caretakers updated sync: "+caretaker.getFirstName());
                            }
                        }else{
                            Log.d("SyncOut","caretakers updated sync: non updated");
                        }

                    }

                    if(!caretakerToSync.isEmpty()){
                        callback.onCaretakersLoaded(getCaretakersWithChildren(caretakerToSync));
                        for(Caretaker caretaker: caretakerToSync){
                            Log.d("SyncOut","caretakers to sync: "+caretaker.getFirstName());
                        }
                    }else{
                        callback.onCaretakerNotAvailable();
                        Log.d("SyncOut","caretakers non available");
                    }


                }else{

                    callback.onCaretakerNotAvailable();

                }


            }
        };
        mAppExecutors.diskIO().execute(runnable);


    }



    @Override
    public void checkChildCardsSize(final LoadChildCardsSizeCallback loadCardsSizeCheckCallback) {
        checkNotNull(loadCardsSizeCheckCallback);

        Runnable cardRunnable = new Runnable() {
            @Override
            public void run() {

                // int size = mVaccinationCardDao.getChildVaccinationCardsSize();
                //  int size = mvaccinationEventDao.getNoOfEventsAvailable();
                int size = mChildDao.getNoChildrenSize();


                if (size > 0) {

                    loadCardsSizeCheckCallback.onCardsSizeLoaded(size);

                } else {

                    loadCardsSizeCheckCallback.onCardNotAvailable();

                }

            }

        };

        mAppExecutors.diskIO().execute(cardRunnable);

    }


    @Override
    public void searchCaretaker(final @NonNull String caretakerName,final SearchCaretakerCallback searchCaretakerCallback) {

        Runnable searchRunnable = new Runnable() {
            @Override
            public void run() {

                final List<Caretaker> caretakers = mCaretakerDao.searchCaretakers(caretakerName);

                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (caretakers.isEmpty()) {
                            // This will be called if the table is new or just empty.
                            searchCaretakerCallback.onFailed();
                        } else {
                            searchCaretakerCallback.onSearchedCaretaker(caretakers);
                        }
                    }
                });

            }
        };

        mAppExecutors.diskIO().execute(searchRunnable);

    }

    @Override
    public void searchChild(@NonNull final String searchText, final SearchChildCallback searchChildrenCallback) {

        Runnable searchRunnable = new Runnable() {
            @Override
            public void run() {

                final List<Child> children = mChildDao.searchChildren(searchText);

                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (children.isEmpty()) {
                            // This will be called if the table is new or just empty.
                            searchChildrenCallback.onFailed("Child Not Found");
                        } else {
                            searchChildrenCallback.onSearchedChildren(children);
                        }
                    }
                });

            }
        };

        mAppExecutors.diskIO().execute(searchRunnable);


    }

    @Override
    public void refreshEvents() {

    }

    @Override
    public void deleteCaretaker(@NonNull String caretakerId) {

    }

    @Override
    public void refreshCaretakers() {

    }

    @Override
    public void refreshChildren() {

    }
}
