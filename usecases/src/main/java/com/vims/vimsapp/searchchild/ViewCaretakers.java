package com.vims.vimsapp.searchchild;


import android.support.annotation.NonNull;
import android.util.Log;

import com.vims.vimsapp.UseCase;

import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;

import java.util.List;

/**
 * Created by root on 6/2/18.
 */

public class ViewCaretakers implements UseCase<ViewCaretakers.RequestValues,ViewCaretakers.ResponseValue> {


    private ViewChildRepository mPatientsRepository;

    public ViewCaretakers(ViewChildRepository patientsRepository) {

        this.mPatientsRepository = patientsRepository;

    }



    @Override
    public void execute(final RequestValues requestValues,final OutputBoundary<ResponseValue> deliveryMechanism) {


        boolean isSync = requestValues.ismIsSync();
        if(isSync){
            executeSyncCaretakers(deliveryMechanism);
        }else{
            executeCaretakers(deliveryMechanism);
        }

    }



    private void executeCaretakers(final OutputBoundary<ResponseValue> deliveryMechanism){

        //ArrayList<Caretaker> myCaretakers;
        mPatientsRepository.getCaretakers(new ViewChildDataSource.LoadCaretakersCallback() {
            @Override
            public void onCaretakersLoaded(List<Caretaker> caretakers) {
              //  myCaretakers = caretakers;

                for (final Caretaker caretaker:caretakers) {

                    for(Caretaker c: caretakers) {
                        Log.d("CaretakersKids1", "s: " + c.getCaretakerId());
                    }



                    mPatientsRepository.getChildrenByCaretakerId(caretaker.getCaretakerId(), new ViewChildDataSource.LoadChildCallback() {
                        @Override
                        public void onChildrenLoaded(List<Child> children) {
                            Log.d("CaretakersKids", "s: " + children.size());

                            caretaker.setChildren(children);


                        }

                        @Override
                        public void onDataNotAvailable() {

                        }
                    });



                }




                deliveryMechanism.onSuccess(new ResponseValue(caretakers));



            }

            @Override
            public void onDataNotAvailable(String e) {

                deliveryMechanism.onError("No Data: "+e);

            }
        });



    }

    private void executeSyncCaretakers(final OutputBoundary<ResponseValue> delivery){

        mPatientsRepository.getCaretakersToSync(new ViewChildDataSource.GetCaretakersToSyncCallback() {
            @Override
            public void onCaretakersLoaded(List<Caretaker> caretakers) {

                for(Caretaker caretaker: caretakers){
                    for(Child child: caretaker.getChildren()){
                        Log.d("SyncOut","children: now: "+child.getFirstName());
                    }
                }

               delivery.onSuccess(new ResponseValue(caretakers));

            }

            @Override
            public void onCaretakerNotAvailable() {
                delivery.onError("Data up to date!");

            }
        });




    }




    //it implements such that its of a right type to be accepted in thhe execute method
    public static final class RequestValues implements UseCase.RequestValues {

        private final boolean mIsSync;

        public RequestValues(@NonNull boolean isSync) {
            mIsSync = isSync;
        }

        public boolean ismIsSync() {
            return mIsSync;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private List<Caretaker> caretakerList;

        public ResponseValue(@NonNull List<Caretaker> caretakerList) {
            this.caretakerList = caretakerList;
        }

        public List<Caretaker>  getmCaretakers() {
            return caretakerList;
        }
    }



}
