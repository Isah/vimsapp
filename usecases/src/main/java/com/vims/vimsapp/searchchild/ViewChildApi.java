package com.vims.vimsapp.searchchild;


import com.vims.vimsapp.entities.child.Caretaker;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by root on 6/5/18.
 */

public interface ViewChildApi {
    //@FormUrlEncoded


    /** vims officail api **/

    @GET("vaccinationAPI/mobile/getAllHospitalCareTakers/{hospitalId}")
    Call<List<Caretaker>> getVimsCaretakers(@Header("Authorization") String authKey, @Path("hospitalId") String hospitalId);


    /** end of vims officail api **/

    @Multipart
    @POST("/upload")
    Call<ResponseBody> uploadImage(@Part MultipartBody.Part file, @Part("name") RequestBody requestBody);



}