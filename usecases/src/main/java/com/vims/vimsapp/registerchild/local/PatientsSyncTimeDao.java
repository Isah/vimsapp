package com.vims.vimsapp.registerchild.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.vims.vimsapp.entities.child.SyncUpdateTime;

import java.util.List;

/**
 * Created by root on 5/25/18.
 */

 //1. annotate your Dao
 @Dao
public interface PatientsSyncTimeDao {

  //2. Additionally, for bulkInsert, you want to use a OnConflictStrategy.REPLACE
  // so that when Sunshine re-downloads forecasts, old weatherEntry forecasts are replaced by new ones
  @Insert(onConflict = OnConflictStrategy.REPLACE)
  void Insert(SyncUpdateTime patientsSyncTime);

  @Update(onConflict = OnConflictStrategy.REPLACE)
  void Update(SyncUpdateTime patientsSyncTime);

  @Query("SELECT count(*) FROM sync_update")
  int getNoPatientsSyncTimes();

  @Query("SELECT * FROM sync_update WHERE caretakerId = :caretaker_id ")
  SyncUpdateTime getUpdateTime(String caretaker_id);



  //3. get weather data by unique date (:date  is a parameter)
  @Query("SELECT * FROM sync_update WHERE eventUpdateTime > :last_sync_time")
  List<SyncUpdateTime> getUpdateTimesAfterLatestSync(long last_sync_time);


  @Query("DELETE FROM sync_update")
   void deleteUpdates();



}

