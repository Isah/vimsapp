package com.vims.vimsapp.registerchild;

import android.app.Application;
import android.support.annotation.NonNull;

import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationLocationState;
import com.vims.vimsapp.entities.vaccinations.VaccinationStatus;
import com.vims.vimsapp.registerchild.local.CaretakerDao;
import com.vims.vimsapp.registerchild.local.ChildDao;
import com.vims.vimsapp.registerchild.local.PatientsSyncTimeDao;
import com.vims.vimsapp.registerchild.local.VaccinationEventDao;
import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.child.SyncUpdateTime;
import com.vims.vimsapp.utilities.AppExecutors;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.PreferenceHandler;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AddChildLocalDataSource implements AddChildDataSource{


    private static volatile AddChildLocalDataSource INSTANCE;

    private CaretakerDao mCaretakerDao;
    private ChildDao mChildDao;


    private VaccinationEventDao mvaccinationEventDao;

    private PatientsSyncTimeDao mPatientsSyncTimeDao;
    private AppExecutors mAppExecutors;

    private Application mContext;

    private PreferenceHandler preferenceHandler;

    private String USER_ID;
    private String USER_HOSPITAL_ID;




    private AddChildLocalDataSource(@NonNull AppExecutors appExecutors, @NonNull CaretakerDao caretakerDao, ChildDao childDao, PatientsSyncTimeDao patientsSyncTimeDao, VaccinationEventDao vaccinationEventDao, Application context) {
        mAppExecutors = appExecutors;
        mCaretakerDao = caretakerDao;
        mChildDao = childDao;
        mvaccinationEventDao = vaccinationEventDao;
        this.mContext = context;
        preferenceHandler = PreferenceHandler.getInstance(mContext);


    }


    public static AddChildLocalDataSource getInstance(@NonNull AppExecutors appExecutors, @NonNull CaretakerDao caretakerDao, ChildDao childDao, PatientsSyncTimeDao patientsSyncTimeDao,  VaccinationEventDao vaccinationEventDao, Application context) {
        if (INSTANCE == null) {
            synchronized (AddChildLocalDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new AddChildLocalDataSource(appExecutors, caretakerDao,childDao,patientsSyncTimeDao,vaccinationEventDao, context);
                }
            }
        }
        return INSTANCE;
    }




    //------------------SAVING --------------------------------------------------------------------------------------------

    @Override
    public void saveChild(final @NonNull Child child,final SaveChildCallback saveChildCallback) {


        Runnable saveRunnable = new Runnable() {
            @Override
            public void run() {

                try {
                    mChildDao.Insert(child);
                    //List<VaccinationEvent> events = child.getVaccinationEvents();

                    long[] save = mvaccinationEventDao.Insert(child.getVaccinationEvents());

                    // if (result.length > 0) {
                    saveChildCallback.onChildSaved(child);

                    //   }



                } catch (Exception e) {

                    saveChildCallback.onSaveFailed("error saving child: " + e.getCause());
                }

            }

        };
        mAppExecutors.diskIO().execute(saveRunnable);


    }


    @Override
    public void saveChildren(@NonNull final List<Child>  children, final SaveChildrenCallback saveChildrenCallback) {

        Runnable saveRunnable = new Runnable() {
            @Override
            public void run() {

                try {

                    mChildDao.InsertAll(children);

                    saveChildrenCallback.onChildrenSaved(children);


                } catch (Exception e) {

                    saveChildrenCallback.onSaveFailed("error saving child: " + e.getCause());
                }

            }

        };
        mAppExecutors.diskIO().execute(saveRunnable);




    }

    @Override
    public void saveCaretakers(@NonNull final List<Caretaker> caretakers,final SaveCaretakersCallback saveCaretakersCallback) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                mCaretakerDao.InsertAll(caretakers);

                for (Caretaker caretaker : caretakers) {

                    // if(!caretaker.getChildren().isEmpty()) {
                    mChildDao.InsertAll(caretaker.getChildren());

                    for (Child child : caretaker.getChildren()) {

                        //  if(!child.getVaccinationEvent().isEmpty()) {
                        mvaccinationEventDao.Insert(child.getVaccinationEvents());
                        //   }

                        //   }
                    }

                }

                saveCaretakersCallback.onCaretakersSaved("caretakers Insynced locally");

            }
        };
        mAppExecutors.diskIO().execute(runnable);

    }


    @Override
    public void saveCaretaker(final @NonNull Caretaker caretaker,final SaveCaretakerCallback saveCallback) {

        Runnable saveRunnable = new Runnable() {
            @Override
            public void run() {

                try {


                    mCaretakerDao.Insert(caretaker);


                    saveCallback.onCaretakerSaved(""+caretaker.getFirstName()+ " locally Saved successfully");
                }
                catch (Exception e){

                    saveCallback.onDataNotAvailable();

                }

            }
        };
        mAppExecutors.diskIO().execute(saveRunnable);
    }

    private VaccinationEvent getEvent(String eventId, List<VaccinationEvent> vaccinationEvents){

        for(VaccinationEvent vaccinationEvent: vaccinationEvents) {
            if(eventId.equals(vaccinationEvent.getEventId())){
                return vaccinationEvent;
            }
        }
        return null;
    }


/*
    @Override
    public void saveVaccination(final VaccinationLocationState locationState, final VaccinationEvent vaccinationEvent, final SaveVaccinationCallback saveVaccinationCallback) {

        Runnable saveRunnable = new Runnable() {
            @Override
            public void run() {


                USER_ID = preferenceHandler.getPref(PreferenceHandler.USER_ID);
                USER_HOSPITAL_ID = preferenceHandler.getPref(PreferenceHandler.USER_HOSPITAL_ID);


                Date d = new Date(vaccinationEvent.getScheduledDate());
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(d);
                int yearOfEvent = calendar.get(Calendar.YEAR);
                int monthOfEvent = calendar.get(Calendar.MONTH);

                vaccinationEvent.setVaccinationLocationState(locationState);
                String dateVaccinated = DateCalendarConverter.dateToStringWithoutTime(new Date());
                vaccinationEvent.setVaccinationDate(dateVaccinated);
                vaccinationEvent.setVaccinationStatus(VaccinationStatus.DONE);
                vaccinationEvent.setVaccinationStatusCode(1);
                vaccinationEvent.setYearOfEvent(yearOfEvent);

                vaccinationEvent.setUserId(USER_ID);
                vaccinationEvent.setHospitalId(USER_HOSPITAL_ID);

                final int noOfUpdatedRows = mvaccinationEventDao.Update(vaccinationEvent);

                Child child = mChildDao.getChildById(vaccinationEvent.getChildId());


                List<VaccinationEvent> vevents = child.getVaccinationEvents();
                VaccinationEvent myevent = getEvent(vaccinationEvent.getEventId(), vevents);

                if (myevent != null){

                    vevents.remove(myevent);
                    VaccinationEvent myeventremoved = getEvent(myevent.getEventId(), vevents);
                    if(myeventremoved==null){
                        //removed
                    }

                    vevents.add(vaccinationEvent);
                    VaccinationEvent myeventadded = getEvent(vaccinationEvent.getEventId(), vevents);
                    if(myeventadded!=null){

                    }

                    child.setVaccinationEvents(null);
                    child.setVaccinationEvents(vevents);

                }else{
                    //null
                }

                final int update = mChildDao.Update(child);

                //update the date when a vacinnation was taken as sync time

                int sync_times = mPatientsSyncTimeDao.getNoPatientsSyncTimes();
                if(sync_times > 0){
                    SyncUpdateTime syncUpdateTime = mPatientsSyncTimeDao.getUpdateTime(child.getCaretakerId());
                    if(syncUpdateTime!=null) {
                        syncUpdateTime.setEventUpdateTime(new Date().getTime());
                        mPatientsSyncTimeDao.Update(syncUpdateTime);

                    }else{
                        SyncUpdateTime patientsSyncTime = new SyncUpdateTime();
                        patientsSyncTime.setCaretakerId(child.getCaretakerId());
                        patientsSyncTime.setEventUpdateTime(new Date().getTime());
                        mPatientsSyncTimeDao.Insert(patientsSyncTime);
                    }

                }else{
                    SyncUpdateTime patientsSyncTime = new SyncUpdateTime();
                    patientsSyncTime.setCaretakerId(child.getCaretakerId());
                    patientsSyncTime.setEventUpdateTime(new Date().getTime());
                    mPatientsSyncTimeDao.Insert(patientsSyncTime);
                }

                if(update > 0){

                }

                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (update > 0) {
                            // This will be called if the table is new or just empty.
                            saveVaccinationCallback.onVaccinationSaved(VaccinationStatus.DONE);

                        } else {

                            saveVaccinationCallback.onFailed("error recording vaccination");
                        }
                    }
                });

            }
        };

        mAppExecutors.diskIO().execute(saveRunnable);


    }

  */

    @Override
    public void saveSync(@NonNull SyncUpdateTime patientsSyncTime) {
        mPatientsSyncTimeDao.Insert(patientsSyncTime);
    }

    @Override
    public void updateSync(@NonNull long sync_time) {

       // Log.d("Sync Last", "Date : "+new Date(sync_time));

        PreferenceHandler preferenceHandler =   PreferenceHandler.getInstance(mContext);
        preferenceHandler.putLongPref(PreferenceHandler.SYNC_TIME_KEY,sync_time);

    }


    @Override
    public void deleteAllCaretakers() {
        mCaretakerDao.deleteCaretakers();
    }
}
