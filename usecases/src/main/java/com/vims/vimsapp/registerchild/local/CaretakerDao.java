package com.vims.vimsapp.registerchild.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;


import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.CaretakerWithChildren;

import java.util.List;

/**
 * Created by root on 5/26/18.
 */

//1. annotate your Dao
@Dao
public interface CaretakerDao {

    //2. Additionally, for bulkInsert, you want to use a OnConflictStrategy.REPLACE
    // so that when Sunshine re-downloads forecasts, old weatherEntry forecasts are replaced by new ones
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void Insert(Caretaker caretaker);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void InsertAll(List<Caretaker> caretakers);


    //3. get weather data by unique date (:date  is a parameter)
    @Query("SELECT * FROM CARETAKER WHERE caretakerId = :id")
    Caretaker getCaretakerById(String id);



    @Query("SELECT * FROM CARETAKER WHERE firstName LIKE :searchFirstName OR lastName LIKE :searchFirstName")
    List<Caretaker> searchCaretakers(String searchFirstName);



    @Query("SELECT * FROM CARETAKER ORDER BY registrationDate DESC LIMIT :limit")
    List<Caretaker> getCaretakersLtd(int limit);

    @Query("SELECT * FROM CARETAKER ORDER BY registrationDate DESC")
    List<Caretaker> getCaretakers();



    //3. get weather data by unique date (:date  is a parameter)
    @Query("SELECT * FROM CARETAKER WHERE registrationDate > :last_sync_time")
    List<Caretaker> getCaretakersToSync(long last_sync_time);


    /*
    SELECT group, date, checks
    FROM table
    WHERE checks > 0
    GROUP BY group HAVING date = max(date)
    */


    @Query("SELECT *  FROM CARETAKER GROUP BY registrationDate HAVING registrationDate = max(registrationDate)")
    Caretaker getLatestCaretaker();


    //3. get weather data by unique date (:date  is a parameter)
    @Query("SELECT count(*) FROM CARETAKER")
    int getSize();

    @Query("SELECT * from caretaker")
    List<CaretakerWithChildren> getCaretakerWithChhildren();

    @Query("DELETE FROM caretaker")
    int deleteCaretakers();



}
