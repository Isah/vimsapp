package com.vims.vimsapp.registerchild;


import com.vims.vimsapp.entities.child.Caretaker;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by root on 6/5/18.
 */

public interface AddChildApi {
    //@FormUrlEncoded

    /** vims officail api **/


    @POST("vaccinationAPI/mobile/saveCaretaker")
    Call<Caretaker> addVimsCaretaker(@Header("Authorization") String authKey, @Body Caretaker caretakers);

    /** end of vims officail api **/

    @Multipart
    @POST("/upload")
    Call<ResponseBody> uploadImage(@Part MultipartBody.Part file, @Part("name") RequestBody requestBody);



}