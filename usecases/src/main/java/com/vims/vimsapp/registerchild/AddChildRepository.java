package com.vims.vimsapp.registerchild;

import android.support.annotation.NonNull;

import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.child.SyncUpdateTime;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class AddChildRepository implements AddChildDataSource{


    private static AddChildRepository INSTANCE = null;
    private final AddChildDataSource mPatientsRemoteDataSource;
    private final AddChildDataSource mPatientsLocalDataSource;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    public Map<String, Caretaker> mCachedCaretakers;

    public  Map<String, Child> mCachedChidren;


    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */
    private  boolean mCaretakerCacheIsDirty = false;
    private  boolean mChildrenCacheIsDirty = false;
    private  boolean mCachedVaccCardsCacheIsDirty = false;


    // Prevent direct instantiation.

    private AddChildRepository(AddChildDataSource patientsRemoteDataSource,  AddChildDataSource patientsLocalDataSource) {
        mPatientsRemoteDataSource = checkNotNull(patientsRemoteDataSource);
        mPatientsLocalDataSource = checkNotNull(patientsLocalDataSource);

    }



    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param patientsRemoteDataSource the backend data source
     * @param patientsLocalDataSource  the device storage data source
     * @return the {@link AddChildDataSource} instance
     */
    public static AddChildRepository getInstance(AddChildDataSource patientsRemoteDataSource, AddChildDataSource patientsLocalDataSource) {

        if (INSTANCE == null) {
            INSTANCE = new AddChildRepository(patientsRemoteDataSource, patientsLocalDataSource);
        }
        return INSTANCE;
    }


    public static void destroyInstance() {
        INSTANCE = null;
    }





    //SAVING------------------
    @Override
    public void saveCaretaker(@NonNull Caretaker caretaker, SaveCaretakerCallback saveCallback) {

        checkNotNull(caretaker);
//        mPatientsRemoteDataSource.saveCaretaker(caretaker, saveCallback);
        mPatientsLocalDataSource.saveCaretaker(caretaker, saveCallback);

        // Do in memory cache update to keep the app UI up to date
        if (mCachedCaretakers == null) {
            mCachedCaretakers = new LinkedHashMap<>();
        }

        // UUID.randomUUID();
        mCachedCaretakers.put(caretaker.getCaretakerId(), caretaker);

    }

    @Override
    public void saveCaretakers(@NonNull List<Caretaker> caretakers, SaveCaretakersCallback saveCaretakersCallback) {

        mPatientsLocalDataSource.saveCaretakers(caretakers,saveCaretakersCallback);
    }

    @Override
    public void saveChildren(@NonNull List<Child> children, SaveChildrenCallback saveChildrenCallback) {

    }
    @Override
    public void saveChild(@NonNull Child child, SaveChildCallback saveChildCallback) {

        checkNotNull(child);
        checkNotNull(child.getDateOfBirth());

        // mPatientsRemoteDataSource.saveChild(child);
        mPatientsLocalDataSource.saveChild(child, saveChildCallback);

        // Do in memory cache update to keep the app UI up to date
        if (mCachedChidren == null) {
            mCachedChidren = new LinkedHashMap<>();
        }
        mCachedChidren.put(child.getChildId(), child);
    }


    @Override
    public void saveSync(@NonNull SyncUpdateTime patientsSyncTime) {

    }

    @Override
    public void updateSync(@NonNull long sync_time) {

    }

    @Override
    public void deleteAllCaretakers() {

    }
}
