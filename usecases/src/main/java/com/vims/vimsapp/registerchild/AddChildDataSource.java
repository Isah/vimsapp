package com.vims.vimsapp.registerchild;

import android.support.annotation.NonNull;

import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.child.SyncUpdateTime;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationLocationState;

import java.util.List;

public interface AddChildDataSource {


    interface SaveChildCallback {

        void onChildSaved(Child child);

        void onSaveFailed(String errorMsg);
    }

    interface SaveChildrenCallback {

        void onChildrenSaved(List<Child> children);

        void onSaveFailed(String errorMsg);
    }

    // callbacks are like output boundaries
    interface SaveCaretakerCallback {

        void onCaretakerSaved(String onSavedMessage);

        void onDataNotAvailable();
    }

    // callbacks are like output boundaries
    interface SaveCaretakersCallback {

        void onCaretakersSaved(String onSavedMessage);

        void onFailed(String errorMessage);
    }


    interface SyncPatientsCallback {

        void onSynced(String msg);
        void onSyncFailed(String errorMsg);
    }





    //SAVING-------------------  the guy that implements the callback gets the values
    void saveCaretaker(@NonNull Caretaker caretaker, SaveCaretakerCallback saveCallback);

    void saveCaretakers(@NonNull List<Caretaker> caretakers, SaveCaretakersCallback saveCaretakersCallback);


    void saveChild(@NonNull Child child, SaveChildCallback saveChildCallback);

    void saveChildren(@NonNull List<Child> children, SaveChildrenCallback saveChildrenCallback);

   // void saveVaccination(VaccinationLocationState locationState, VaccinationEvent vaccinationEvent, SaveVaccinationCallback saveVaccinationCallback);

    void deleteAllCaretakers();

    void saveSync(@NonNull SyncUpdateTime patientsSyncTime);

    void updateSync(@NonNull long sync_time);





}
