package com.vims.vimsapp.registerchild;


import android.support.annotation.NonNull;

import com.vims.vimsapp.UseCase;

import com.vims.vimsapp.entities.child.Caretaker;

/**
 * Created by root on 5/24/18.
 */

public class RegisterCaretaker implements UseCase<RegisterCaretaker.RequestValues,RegisterCaretaker.ResponseValue> {

    private AddChildRepository mPatientsRepository;
    //private UniqueKeyGenertaor uniqueKeyGenerator;


    public RegisterCaretaker(AddChildRepository patientsRepository) {
       this.mPatientsRepository = patientsRepository;

    }


    @Override
    public void execute(final RequestValues param,final OutputBoundary<ResponseValue> deliveryMechanism) {

        //this is the guy that implements the interface savecaretaker callback
        // so he receives the updates


            Caretaker caretaker = param.getmCaretaker();


            mPatientsRepository.saveCaretaker(caretaker, new AddChildDataSource.SaveCaretakerCallback() {
                @Override
                public void onCaretakerSaved(String onSavedMessage) {

                    deliveryMechanism.onSuccess(new ResponseValue(onSavedMessage));


                }

                @Override
                public void onDataNotAvailable() {

                    deliveryMechanism.onError("");

                }
            });



        }







    //it implements such that its of a right type to be accepted in thhe execute method
    public static final class RequestValues implements UseCase.RequestValues {

        private final Caretaker mCaretaker;

        public RequestValues(@NonNull Caretaker caretaker) {
            mCaretaker = caretaker;
        }

        public Caretaker getmCaretaker() {
            return mCaretaker;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private String responseMessage;

        public ResponseValue(@NonNull String savedResponseMsg) {
            this.responseMessage = savedResponseMsg;
        }

        public String getmResponseMessage() {
            return responseMessage;
        }
    }





}





