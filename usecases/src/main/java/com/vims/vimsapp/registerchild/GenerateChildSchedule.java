package com.vims.vimsapp.registerchild;

import android.support.annotation.NonNull;
import android.util.Log;

import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.managehospitalschedule.ManageHospitalScheduleDataSource;
import com.vims.vimsapp.managehospitalschedule.ManageHospitalScheduleRepository;
import com.vims.vimsapp.entities.schedule.HospitalImmunisationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationCard;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationLocationState;
import com.vims.vimsapp.entities.vaccinations.VaccinationStatus;
import com.vims.vimsapp.entities.vaccinations.VaccineSpecification;
import com.vims.vimsapp.entities.schedule.HospitalSchedule;
import com.vims.vimsapp.utilities.AgeCalculator;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.viewcoverage.ReadVaccines;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.vims.vimsapp.utilities.DateCalendarConverter.calculateDifferenceInDates;

public class GenerateChildSchedule implements UseCase<GenerateChildSchedule.RequestValues, GenerateChildSchedule.ResponseValue>{


    private static GenerateChildSchedule INSTANCE = null;


    ManageHospitalScheduleRepository scheduleRepo;
    AddChildRepository patientsRepository;


    private GenerateChildSchedule(ManageHospitalScheduleRepository vaccinationsRepository, AddChildRepository patientsRepository){
        this.scheduleRepo = vaccinationsRepository;
        this.patientsRepository = patientsRepository;

        //  JodaTimeAndroid.init(this);
        //  with prefetch on background thread
        //  ApplicationStarter.initialize(this, true);
    }

    public static GenerateChildSchedule getInstance(ManageHospitalScheduleRepository vaccinationsRepository, AddChildRepository patientsRepository) {

        if (INSTANCE == null) {
            INSTANCE = new GenerateChildSchedule(vaccinationsRepository,patientsRepository);
        }
        return INSTANCE;
    }




    public static VaccinationCard card;

    @Override
    public void execute(RequestValues param,final OutputBoundary<ResponseValue> delivery) {
        //generate the card
        //needs to be checked
        generateVaccinationCard("Bushenyi",param.getChild(), scheduleRepo,delivery);


    }




    VaccinationCard vaccinationCard;

    Date childDateOfBirth;
    String childName;
    String childId;


    private void generateVaccinationCard(String hospitalName, final Child child, final ManageHospitalScheduleRepository vaccinationsRepository, final OutputBoundary<ResponseValue> delivery){


    vaccinationsRepository.getHospitalSchedule(false,hospitalName, new ManageHospitalScheduleDataSource.LoadHospitalScheduleCallback() {
        @Override
        public void onCardLoaded(HospitalSchedule hospitalSchedule) {

            //Get a hospital date
            //what is today
            //what is 6 weeks after today wc hd is close

            Date now = Calendar.getInstance().getTime();

          //  vaccinationCard = new VaccinationCard();
            //vaccinationCard.setDateCreated(DateCalendarConverter.dateToStringWithoutTime(now));
            //vaccinationCard.setChildId(child.getChildId());
            childName = child.getFirstName()+" "+child.getLastName();
            childId = child.getChildId();

            //these attributes can be used by methods outside this class

           // Map<String, VaccinationEvent> vaccinationEventMap = new HashMap<>();
            List<VaccinationEvent> vaccinationEvents = new ArrayList<>();
            childDateOfBirth =  DateCalendarConverter.stringToDateWithoutTime(child.getDateOfBirth());

            List<VaccineSpecification> vaccineSpecifications = ReadVaccines.getInstance().getVaccineSpecificationList();

            for(VaccineSpecification vspec: vaccineSpecifications) {
                int givenAfter;
                 //check for bcg or polio whether child is less than 14 days old to be given thsoe vaccines
                if (vspec.getVaccineName().equals(ReadVaccines.BCG_VACCINE) || vspec.getVaccineName().equals(ReadVaccines.OPV0_VACCINE)){
                    AgeCalculator.Age childAge = new AgeCalculator().calculateAge(childDateOfBirth);
                    int days = childAge.getDays();

                    long difference = DateCalendarConverter.calculateDifferenceInDates(new Date(),childDateOfBirth);

                    boolean isBeGive = isChildToBeGivenPolioOrBcg(difference, vspec.getDaysGivenAfter());
                    if(isBeGive){
                        givenAfter = 0;

                        VaccinationEvent afterAnoOfWeeksEvent =  createChildSchedule(givenAfter,vspec.getVaccineName(), VaccinationStatus.NOT_DONE,hospitalSchedule);
                        vaccinationEvents.add(afterAnoOfWeeksEvent);
                    }
                    //if its less then child will be given today
                }else{
                    givenAfter = vspec.getDaysGivenAfter();

                    VaccinationEvent afterAnoOfWeeksEvent =  createChildSchedule(givenAfter,vspec.getVaccineName(), VaccinationStatus.NOT_DONE,hospitalSchedule);
                    vaccinationEvents.add(afterAnoOfWeeksEvent);
                }

            }


           // vaccinationCard.setVaccinationEvents(vaccinationEvents);
           // Collections.sort(vaccinationEvents);
            child.setVaccinationEvents(vaccinationEvents);


            patientsRepository.saveChild(child, new AddChildDataSource.SaveChildCallback() {
                @Override
                public void onChildSaved(Child child) {
                    delivery.onSuccess(new ResponseValue(child));
                }

                @Override
                public void onSaveFailed(String errorMsg) {
                    delivery.onError(errorMsg);
                }
            });



        }


        @Override
        public void onCardNotAvailable(String error) {

            delivery.onError(error);

        }
    });

    }


    private boolean isChildToBeGivenPolioOrBcg(long childAgeInDays, int maxDaysGivenAt){

        if(childAgeInDays < maxDaysGivenAt){
           return true;
        }
        return false;
    }

    private boolean isPolioOrBcg(String vaccine){

        if (vaccine.equals(ReadVaccines.BCG_VACCINE) || vaccine.equals(ReadVaccines.OPV0_VACCINE)){

            return true;
        }

         return false;
    }

    //A.
    GregorianCalendar childCalendar = null;

    private VaccinationEvent createChildSchedule(int daysToGo, String vaccinesToGive, VaccinationStatus status, HospitalSchedule hospitalSchedule){

        if(vaccinesToGive.equals(ReadVaccines.MEASLES_VACCINE)){
            childCalendar = DateCalendarConverter.dateToCalendar(childDateOfBirth);
        }else{//we  use today as the date to count from for all other vaccines
            childCalendar = DateCalendarConverter.dateToCalendar(new Date());
        }

        //1. add days to calendar to get the next date of vaccination
        /*
        i need to get the visiting date wc is today
         */

      //  Log.i("UC Child Greg DateBirth", ": "+childCalendar.getTime());
        childCalendar.add(Calendar.DAY_OF_YEAR, daysToGo);
       // Log.i("VACCINE DATE", ": "+vaccinesToGive+" "+childCalendar.getTime());
        //2. search for next hospital schedule to accommodate the generated next date of vaccination
        // Date scheduledDate =   searchNextClosestHospitalDate(childCalendar.getTime(),hospitalSchedule);
        //we should prevent a situation where there are no candidate hospital dates

        Date nextHospitalDate;
        if(isPolioOrBcg(vaccinesToGive)){
            nextHospitalDate = new Date();
        }else {
            nextHospitalDate = searchNextClosestHospitalDate(childCalendar.getTime(),hospitalSchedule);
        }


        VaccinationEvent vaccinationEvent = new VaccinationEvent();
        if(nextHospitalDate!=null){
            Date convertedScheduledDate = DateCalendarConverter.dateWithTimeToDateWithoutTime(nextHospitalDate);
            Log.i("GCS", "Next Hospital Schedule: "+convertedScheduledDate);
            vaccinationEvent.setScheduledDate(convertedScheduledDate.getTime());

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(convertedScheduledDate);
            int yearOfEvent = calendar.get(Calendar.YEAR);
            int monthOfEvent = calendar.get(Calendar.MONTH);


            vaccinationEvent.setMonthOfEvent(monthOfEvent);
            vaccinationEvent.setYearOfEvent(yearOfEvent);

        }
        else{
          //  vaccinationEvent.setScheduledDate("N/A");
        }
        //3. create the event of vaccination after getting that date

        vaccinationEvent.setVaccinationStatusCode(0);
        vaccinationEvent.setVaccinationStatus(status);
        vaccinationEvent.setVaccineName(vaccinesToGive);
        vaccinationEvent.setChildName(childName);
        vaccinationEvent.setChildId(childId);
        vaccinationEvent.setVaccinationDate("");
        vaccinationEvent.setHospitalId("");
        vaccinationEvent.setUserId("");
        vaccinationEvent.setVaccinationLocationState(VaccinationLocationState.STATIC);




        return vaccinationEvent;

    }



    private Date formatHospitalWorkingDate(boolean isAarons, String hospital_date){

        Date formatedDate;

        if(isAarons) {

            String[] hospital_event_string = hospital_date.split("-");

            int y = Integer.parseInt(hospital_event_string[0]);
            int m = Integer.parseInt(hospital_event_string[1]);
            int d = Integer.parseInt(hospital_event_string[2]);


            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, y);
            calendar.set(Calendar.MONTH, --m);
            calendar.set(Calendar.DAY_OF_MONTH, d);
            formatedDate = DateCalendarConverter.dateWithTimeToDateWithoutTime(calendar.getTime());

        }else{

            formatedDate = DateCalendarConverter.stringToDateWithoutTime(hospital_date);

        }

        return formatedDate;

    }


    //B

    private Date searchNextClosestHospitalDate(@NonNull Date expectedChildVaccinationDate,@NonNull HospitalSchedule hospitalSchedule){

        Map<Long, Date> candidateHospitalDates;

        //1. given the hospital schedule events
        List<HospitalImmunisationEvent> hospitalEvents = hospitalSchedule.getWorkingDays();

        Log.d("GCS","Hospital schedule Size: "+hospitalSchedule.getWorkingDays().size());

        candidateHospitalDates = new LinkedHashMap<>(hospitalEvents.size());

        //2. for each event date
        for (HospitalImmunisationEvent hospitalEvent: hospitalEvents){

            /*
            String a = hospitalEvent.getEventDate();
            String[] s = a.split("/");
            int m = Integer.parseInt(s[0]);
            int d = Integer.parseInt(s[1]);
            int y = Integer.parseInt(s[2]);

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.MONTH,m);
            calendar.set(Calendar.DAY_OF_MONTH,d);
            calendar.set(Calendar.YEAR,y);
            Date hospitalWorkingDate =  calendar.getTime();
            */
            // Tue Feb 05 18:06:43 EAT 2019

           // Log.d("GCS","Hospital Event Date : "+hospitalEvent.getEventDate());

            boolean isAarons;

           // String hospital_event_sample = "2019-05-13";
            if(hospitalEvent.getEventDate().contains("-")){
            isAarons = true;
            }else{
             isAarons = false;
            }

            //Format
            Date hospitalWorkingDate = formatHospitalWorkingDate(isAarons,hospitalEvent.getEventDate());
            // Original Mine ///////////////
            // Date hospitalWorkingDate = DateCalendarConverter.stringToDateWithoutTime(hospitalEvent.getEventDate());
            ////////////////////////////////
            //Log.d("GCS","Exp Child Hospital schedule: "+expectedChildVaccinationDate);
            //Log.d("GCS","Work Date Hospital schedule: "+hospitalWorkingDate.getTime());

            //3. the child birth needs to be before the hospital date to set the schedule there
            if(expectedChildVaccinationDate.before(hospitalWorkingDate)) {

                //the calculator is found in DateCalendar Converter class
                long diffDays = calculateDifferenceInDates(hospitalWorkingDate, expectedChildVaccinationDate);

                //4. store all the upcoming dates, using diffdays as key to store which ones are closest or farthest
               candidateHospitalDates.put(diffDays, hospitalWorkingDate);


            }
        }



        //after getting the candidate dates calculate the closest thus smallest diff
        Date closestDate = null;


        if(!candidateHospitalDates.isEmpty()){
            closestDate = findClosestDateFromCandidates(candidateHospitalDates);
        }

        return  closestDate;


    }


    //Cderek@
    private Date findClosestDateFromCandidates(Map<Long, Date> candidateHospitalDates){

        //1 calculate the smallest key/ thus the closet

        Map.Entry<Long, Date> firstEntry = candidateHospitalDates.entrySet().iterator().next();

        long smallestKey = firstEntry.getKey();
        Date smallestKeyValue = firstEntry.getValue();

        for (Map.Entry<Long, Date> map : candidateHospitalDates.entrySet()) {
            long key = map.getKey();

            if (key < smallestKey) {
                smallestKey = key;
                smallestKeyValue = map.getValue();
            }

        }

        return smallestKeyValue;


    }


    //it implements such that its of a right type to be accepted in the execute method
    public static final class RequestValues implements UseCase.RequestValues {

          private final Child child;
        //private final VaccineSpec  vaccineSpec;
      //    private final HospitalSchedule hospitalSchedule;




        public RequestValues(@NonNull Child child) {
            this.child = child;
          //  this.hospitalSchedule = hospitalSchedule;
         //   this.vaccineSpecification = vaccineSpecification;

        }

        public Child getChild() {
           return child;
        }

        /*
        public HospitalSchedule getHospitalSchedule() {
            return hospitalSchedule;
        }
       */


    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private Child child;

        public ResponseValue(@NonNull Child child) {
            this.child = child;
        }

        public Child getRegisteredChild() {

            return child;
        }
    }








}
