package com.vims.vimsapp.registerchild;


import android.support.annotation.NonNull;

import com.vims.vimsapp.UseCase;

import com.vims.vimsapp.entities.child.Child;

/**
 * Created by root on 5/24/18.
 */

public class RegisterChild implements UseCase<RegisterChild.RequestValues, RegisterChild.ResponseValue>{

    private AddChildRepository mPatientsRepository;

    public RegisterChild(AddChildRepository patientsRepository) {
    this.mPatientsRepository = patientsRepository;

    }

    @Override
    public void execute(final RequestValues requestValues,final UseCase.OutputBoundary<ResponseValue> deliveryMechanism) {


                Child child = requestValues.getmChild();

                mPatientsRepository.saveChild(child, new AddChildDataSource.SaveChildCallback() {
                    @Override
                    public void onChildSaved(Child child) {

                        //from here save the vaccination events


                        deliveryMechanism.onSuccess(new ResponseValue(child));



                    }

                    @Override
                    public void onSaveFailed(String error) {

                        deliveryMechanism.onError(error);

                    }
                });


    }



    //it implements such that its of a right type to be accepted in thhe execute method
    public static final class RequestValues implements UseCase.RequestValues {

        private final Child mChild;

        public RequestValues(@NonNull Child child) {
            mChild = child;
        }

        public Child getmChild() {
            return mChild;
        }
    }


    public static final class ResponseValue implements UseCase.ResponseValue {


        private final Child mChild;

        public ResponseValue(@NonNull Child child) {
            mChild = child;
        }

        public Child getmChild() {
            return mChild;
        }
    }








}
