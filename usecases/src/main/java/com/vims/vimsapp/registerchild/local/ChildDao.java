package com.vims.vimsapp.registerchild.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;


import com.vims.vimsapp.entities.child.Child;

import java.util.List;

/**
 * Created by root on 5/25/18.
 */

 //1. annotate your Dao
 @Dao
public interface ChildDao {

  //2. Additionally, for bulkInsert, you want to use a OnConflictStrategy.REPLACE
  // so that when Sunshine re-downloads forecasts, old weatherEntry forecasts are replaced by new ones
  @Insert(onConflict = OnConflictStrategy.REPLACE)
  void Insert(Child child);

  @Update(onConflict = OnConflictStrategy.REPLACE)
  int Update(Child child);



  @Insert(onConflict = OnConflictStrategy.REPLACE)
  void InsertAll(List<Child> children);

    //3. get weather data by unique date (:date  is a parameter)
  @Query("SELECT * FROM child WHERE childId = :id")
  Child getChildById(String id);


  //3. get weather data by unique date (:date  is a parameter)
  @Query("SELECT * FROM child WHERE caretakerId = :caretakerId")
  List<Child> getChildrenByCaretakerId(String caretakerId);

  @Query("SELECT * FROM child")
  List<Child> getChildren();

  @Query("SELECT * FROM child ORDER BY registrationDate ASC LIMIT :limit")
  List<Child> getChildrenLtd(int limit);

  @Query("SELECT * FROM CHILD WHERE firstName LIKE :searchFirstName OR lastName LIKE :searchFirstName")
  List<Child> searchChildren(String searchFirstName);



  @Query("SELECT count(*) FROM child")
  int getNoChildrenSize();

  @Query("SELECT count(*) FROM child WHERE registrationDate LIKE :year")
  int getNoChildrenSizeInYear(int year);




  //3. get weather data by unique date (:date  is a parameter)
  @Query("SELECT count(*) FROM child WHERE caretakerId = :caretakerId")
  int getNoOfChildren(int caretakerId);

    @Query("DELETE FROM child")
    void deleteChldren();



}

