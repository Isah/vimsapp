package com.vims.vimsapp.registerchild;

import android.app.Application;
import android.support.annotation.NonNull;
import android.util.Base64;

import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.child.SyncUpdateTime;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationLocationState;
import com.vims.vimsapp.utilities.PreferenceHandler;

import java.io.UnsupportedEncodingException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddChildRemoteRetrofitDataSource implements AddChildDataSource {


    private AddChildApi mPatientsApi;


    private static volatile AddChildRemoteRetrofitDataSource INSTANCE;
    private PreferenceHandler preferenceHandler;
    private Application application;

    // Prevent direct instantiation.
    private AddChildRemoteRetrofitDataSource(@NonNull AddChildApi patientsApi, Application application) {
        this.mPatientsApi = patientsApi;
        this.application = application;
        preferenceHandler = PreferenceHandler.getInstance(application);

    }

    public static AddChildRemoteRetrofitDataSource getInstance(@NonNull AddChildApi patientsApi, Application application) {
        if (INSTANCE == null) {
            synchronized (AddChildRemoteRetrofitDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new AddChildRemoteRetrofitDataSource(patientsApi, application);
                }
            }
        }
        return INSTANCE;
    }



    private String API_USER_ID;
    private String API_PASSWORD;
    private String API_TOKEN;

    public  String getAuthToken() {

        //API_USER_NAME = "petr";
        API_USER_ID = preferenceHandler.getPref(PreferenceHandler.USER_ID);
        API_TOKEN = preferenceHandler.getPref(PreferenceHandler.USER_TOKEN);

        byte[] data = new byte[0];
        try {
            data = (API_USER_ID+ ":" + API_TOKEN).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }



    int codes = 0;
    int uploaded = 0;

    @Override
    public void saveCaretakers(@NonNull List<Caretaker> caretakers, final SaveCaretakersCallback saveCaretakersCallback) {



        final int size_caretakers = caretakers.size();


        for(Caretaker caretaker: caretakers) {

            Call<Caretaker> callSync = mPatientsApi.addVimsCaretaker(getAuthToken(), caretaker);
            //  Call<String> callSync = mPatientsApi.saveCaretakers(caretakers);

            callSync.enqueue(new Callback<Caretaker>() {
                @Override
                public void onResponse(@NonNull Call<Caretaker> call, @NonNull Response<Caretaker> response) {

                    uploaded++;

                    codes += response.code();

                  //  Log.d("ResponseCodes1", "code: " + response.code());

                    if (size_caretakers == uploaded) {//we ve exosted all caretakers

                        int codeEvaluationValue = codes / size_caretakers;
                        //  Log.d("ResponseCodes2", "code: "+codeEvaluationValue+" codesTotal: "+codes);
                        //means if you add p the codes and dive cartekaers it shld give you the original value to show traversal

                        boolean isSuccess =  response.code() == 201;


                        if (codeEvaluationValue == response.code() && isSuccess) {

                            saveCaretakersCallback.onCaretakersSaved("caretakers remotely saved ");
                        }
                    } else if (response.code() == 401) {

                        saveCaretakersCallback.onFailed("Un Authorized!");

                    } else {

                      //  Log.d("ResponseRetro", "" + response.errorBody());
                        saveCaretakersCallback.onFailed("failed! " + response.code() + " " + response.message());
                    }


                }

                @Override
                public void onFailure(@NonNull Call<Caretaker> call, @NonNull Throwable t) {

                    String msg = "no network!";

                    saveCaretakersCallback.onFailed(msg);

                }
            });

        }


    }



    @Override
    public void saveCaretaker(@NonNull Caretaker caretaker, SaveCaretakerCallback saveCallback) {

    }


    @Override
    public void saveChild(@NonNull Child child, SaveChildCallback saveChildCallback) {

    }

    @Override
    public void saveChildren(@NonNull List<Child> children, SaveChildrenCallback saveChildrenCallback) {

    }

    @Override
    public void saveSync(@NonNull SyncUpdateTime patientsSyncTime) {

    }

    @Override
    public void updateSync(@NonNull long sync_time) {

    }

    @Override
    public void deleteAllCaretakers() {

    }
}
