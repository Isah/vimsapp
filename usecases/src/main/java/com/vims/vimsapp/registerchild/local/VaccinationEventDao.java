package com.vims.vimsapp.registerchild.local;;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;


import com.vims.vimsapp.entities.schedule.HospitalImmunisationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;

import java.util.List;


@Dao
public interface VaccinationEventDao {


        //@Insert(onConflict = OnConflictStrategy.REPLACE)
      //  void Insert(VaccinationEvent vaccinationEvents);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        long[] Insert(List<VaccinationEvent> vaccinationEvents);


        @Insert(onConflict = OnConflictStrategy.REPLACE)
        long[] InsertHospitalEvents(List<HospitalImmunisationEvent> hospitalImmunisationEvents);

        //3. check whether there are any items
        @Query("SELECT count(*) FROM hospital_events")
        int getNoOfHospitalEvents();

        //@Query("SELECT * FROM vevent where vaccinationStatusCode = :status")
        @Query("SELECT * FROM vevent")
        List<VaccinationEvent> getVaccinationEvents();

        @Query("SELECT count(*) FROM vevent where  monthOfEvent = :month AND scheduledDate > :dateOfSchedule")
        int getNoUpcomingEvents(int month, long dateOfSchedule);


        //@Query("SELECT count(*) FROM vevent where vaccinationStatusCode = 0 AND vaccineName = :vaccine AND monthOfEvent = :month ")
        //int getNoUpcomingEventsOfMonth(String vaccine, int month);


        @Query("SELECT count(*) FROM vevent where vaccineName = :vaccineName AND yearOfEvent = :yearOfEvent")
        int getNoEventsOfVaccineOfYear(String vaccineName, int yearOfEvent);


        //UPCOMING
        @Query("SELECT * FROM vevent WHERE childName LIKE :searchTerm OR vaccineName LIKE :searchTerm")
        List<VaccinationEvent> searchEvent(String searchTerm);


        @Query("SELECT count(*) FROM vevent where vaccineName = :vaccine AND monthOfEvent = :month AND scheduledDate > :dateOfSchedule")
        int getNoUpcomingEventsOfVaccineInMonth(String vaccine, int month, long dateOfSchedule);


        @Query("SELECT count(*) FROM vevent where vaccineName = :vaccine AND monthOfEvent = :month")
        int getNoDueEventsOfVaccineInMonth(String vaccine, int month);


        @Query("SELECT * FROM vevent where scheduledDate > :date OR scheduledDate = :date AND vaccinationStatus = :status")
        List<VaccinationEvent> getUpcomingEvents(long date, String status);


        @Query("SELECT count(*) FROM vevent where scheduledDate = :date AND monthOfEvent = :month AND vaccinationStatus = :status")
        int getNoUpcomingEventsCurrentMonth(long date, int month, String status);



        @Query("SELECT * FROM vevent where  yearOfEvent = :year AND scheduledDate > :date OR scheduledDate = :date AND vaccinationStatus = :status")
        List<VaccinationEvent> getUpcomingEventsThisYear(int year, long date, String status);

        @Query("SELECT count(*) FROM vevent where yearOfEvent = :year AND  scheduledDate > :date AND vaccinationStatus = :status")
        int getNoUpcomingEventsThisYear(int year, long date, String status);



        @Query("SELECT count(*) FROM vevent where  scheduledDate > :date AND vaccinationStatus = :status")
        int getNoUpcomingEvents(long date, String status);


        @Query("SELECT * FROM vevent where  yearOfEvent = :year AND monthOfEvent = :month AND scheduledDate > :date  AND vaccinationStatus = :status")
        List<VaccinationEvent> getUpcomingEventsOfMonthOfYear(int year, int month, long date, String status);


        @Query("SELECT count(*) FROM vevent where  yearOfEvent = :year AND monthOfEvent = :month AND scheduledDate > :date  AND vaccinationStatus = :status")
        int getNoUpcomingEventsOfMonthOfYear(int year, int month, long date, String status);



        @Query("SELECT * FROM vevent where  yearOfEvent = :year AND monthOfEvent = :month AND scheduledDate > :date  AND vaccinationStatus = :status ORDER BY scheduledDate ASC LIMIT :limit")
        List<VaccinationEvent> getUpcomingEventsOfMonthOfYearLtd(int year, int month, long date, String status, int limit);



        @Query("SELECT * FROM vevent where  yearOfEvent = :year AND scheduledDate = :date  AND vaccinationStatus = :status")
        List<VaccinationEvent> getDueEventsOfMonthOfYear(int year, long date, String status);




        //MISSED
        @Query("SELECT count(*) FROM vevent where scheduledDate < :date AND vaccinationStatus = :status")
        int getNoMissedEvents(long date, String status);


        @Query("SELECT count(*) FROM vevent where vaccineName = :vaccineName AND yearOfEvent = :yearOfEvent AND scheduledDate < :date AND vaccinationStatus = :status")
        int getNoMissedEventsOfVaccineOfYear(String vaccineName, int yearOfEvent, long date, String status);


        @Query("SELECT count(*) FROM vevent where scheduledDate < :date AND monthOfEvent = :month AND vaccinationStatus = :status")
        int getNoMissedEventsCurrentMonth(long date, int month, String status);


        @Query("SELECT * FROM vevent where scheduledDate < :date AND vaccinationStatus = :status")
        List<VaccinationEvent> getMissedEvents(long date, String status);


        //DONE

        @Query("SELECT * FROM vevent where vaccinationStatusCode = 1 AND yearOfEvent = :yearOfEvent")
        List<VaccinationEvent> getDoneEventsOfYear(int yearOfEvent);


        @Query("SELECT * FROM vevent where vaccinationStatusCode = 1 AND yearOfEvent = :yearOfEvent AND vaccineName = :vaccine")
        List<VaccinationEvent> getDoneVaccinationEventsOfYear(String vaccine, int yearOfEvent);


        @Query("SELECT * FROM vevent where vaccinationStatusCode = 1 AND vaccineName = :vaccine")
        List<VaccinationEvent> getDoneVaccinationEvents(String vaccine);


        @Query("SELECT count(*) FROM vevent where vaccinationStatusCode = 1 AND vaccineName = :vaccine AND monthOfEvent = :month ")
        int getNoDoneVaccinationEventsOfCurrentMonth(String vaccine, int month);

        @Query("SELECT count(*) FROM vevent where vaccinationStatusCode = 1 AND vaccineName = :vaccine AND monthOfEvent = :month AND yearOfEvent = :year")
        int getNoDoneEventsOfYearOfMonthOfVaccine(String vaccine, int month, int year);

        @Query("SELECT count(*) FROM vevent where vaccinationStatusCode = 1 AND vaccineName = :vaccine AND yearOfEvent = :year")
        int getNoDoneEventsOfYearOfVaccine(String vaccine, int year);





        //NOT DONE


        @Query("SELECT * FROM vevent where vaccinationStatus = :status AND yearOfEvent = :yearOfEvent")
        List<VaccinationEvent> getNotDoneVaccinationEventsOfYear(String status, int yearOfEvent);


        @Query("SELECT * FROM vevent where userId = :userId AND vaccinationStatus = :status AND yearOfEvent = :yearOfEvent")
        List<VaccinationEvent> getAllDoneVaccinationEventsOfYear(String userId, String status, int yearOfEvent);


        @Query("SELECT count(*) FROM vevent where vaccineName = :vaccine AND monthOfEvent = :monthOfEvent AND yearOfEvent = :yearOfEvent")
        int getMonthTargetPopulation(String vaccine, int monthOfEvent, int yearOfEvent);


        //done,month,vaccine
        @Query("SELECT count(*) FROM vevent where vaccinationStatusCode = 1")
        int getDoneVaccinationsCount();

        @Query("SELECT count(*) FROM vevent where vaccinationStatusCode = 0")
        int getNoOfEventsAvailable();


        @Update(onConflict = OnConflictStrategy.REPLACE)
        int Update(VaccinationEvent vaccinationEvent);

        // @Query("SELECT * FROM vevent WHERE cardId = :childCardId")
        // List<VaccinationEvent> getChildVaccinationCardEvents(String childCardId);

        @Query("SELECT * FROM hospital_events")
        List<HospitalImmunisationEvent> getHospitalEvents();

        @Query("DELETE FROM hospital_events")
        void deleteHospitalEvents();

        @Query("DELETE FROM vevent")
        void deleteEvents();


}



