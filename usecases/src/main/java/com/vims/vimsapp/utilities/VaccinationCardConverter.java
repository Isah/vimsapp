package com.vims.vimsapp.utilities;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vims.vimsapp.entities.vaccinations.VaccinationCard;

import java.lang.reflect.Type;

public class VaccinationCardConverter {



    @TypeConverter
    public static VaccinationCard fromStringToVaccinationCard(String value) {

        Type listType = new TypeToken<VaccinationCard>() {}.getType();

        return new Gson().fromJson(value, listType);

    }

    @TypeConverter
    public static String fromVaccinationCardToString(VaccinationCard caretaker) {

        Gson gson = new Gson();

        String json = gson.toJson(caretaker);

        return json;

    }

}
