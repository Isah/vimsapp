package com.vims.vimsapp.utilities;

import android.arch.persistence.room.TypeConverter;


import com.vims.vimsapp.entities.vaccinations.VaccinationLocationState;
import com.vims.vimsapp.entities.vaccinations.VaccinationStatus;

public class StatusEnumConverter {



       private static VaccinationStatus vaccinationStatus;
       private static VaccinationLocationState vaccinationLocationState;
      // private static VaccineOrder.OrderStatus orderStatus;

       @TypeConverter
       public static VaccinationStatus toVaccinationStatus(String status) {

           if(status.equals("done")){

              vaccinationStatus = VaccinationStatus.DONE;

           }
           else if(status.equals("notdone")){

               vaccinationStatus = VaccinationStatus.NOT_DONE;
           }
           else if(status.equals("overdue")){

               vaccinationStatus = VaccinationStatus.FAILED;
           }

           return vaccinationStatus;

       }


        private  static String vstatus = "";

        @TypeConverter
        public static String toStringStatus(VaccinationStatus status) {

            if(status == VaccinationStatus.DONE){

                vstatus =  "done";
            }
            else if(status == VaccinationStatus.NOT_DONE){

                vstatus = "notdone";
            }
            else if(status == VaccinationStatus.FAILED){

                vstatus = "overdue";
            }


            return vstatus;

         }





    @TypeConverter
    public static VaccinationLocationState toVaccinationLocationState(String status) {

        if(status.equals("outreach")){

            vaccinationLocationState = VaccinationLocationState.OUTREACH;

        }
        else if(status.equals("static")){

            vaccinationLocationState = VaccinationLocationState.STATIC;

        }


        return vaccinationLocationState;

    }


    private  static String lstatus = "";

    @TypeConverter
    public static String toStringLocationState(VaccinationLocationState status) {

        if(status == VaccinationLocationState.OUTREACH){

            lstatus =  "outreach";
        }
        else if(status == VaccinationLocationState.STATIC){

            lstatus = "static";
        }


        return lstatus;

    }








   /*
    @TypeConverter
    public static VaccineOrder.OrderStatus toOrderStatus(String status) {

        if(status.equals("sent")){
            orderStatus = VaccineOrder.OrderStatus.SENT;
        }
        else if(status.equals("received")){
            orderStatus = VaccineOrder.OrderStatus.RECEIVED;
        }
        return orderStatus;

    }
    */


   /*
    private  static String ostatus = "";
    @TypeConverter
    public static String toStringOrderStatus(VaccineOrder.OrderStatus status) {

        if(status == VaccineOrder.OrderStatus.SENT){

            ostatus =  "sent";
        }
        else if(status == VaccineOrder.OrderStatus.RECEIVED){

            ostatus = "received";
        }

        return ostatus;

    }
    */




}
