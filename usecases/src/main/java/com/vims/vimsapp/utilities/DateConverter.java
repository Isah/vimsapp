package com.vims.vimsapp.utilities;

import android.arch.persistence.room.TypeConverter;

import com.vims.vimsapp.utilities.DateCalendarConverter;

import java.util.Date;

public class DateConverter {



        @TypeConverter
        public static Date toDate(Long timestamp) {

            return timestamp == null ? null : DateCalendarConverter.dateWithTimeToDateWithoutTime(new Date(timestamp));
        }

        @TypeConverter
        public static Long toTimestamp(Date date) {
            return date == null ? null : date.getTime();
        }



}
