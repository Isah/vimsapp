package com.vims.vimsapp.utilities;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.MonthDay;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class DateCalendarConverter {


    //Convert Date to Calendar
    public static GregorianCalendar dateToCalendar(Date date) {

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar;

    }

    //Convert Calendar to Date
    public static Date calendarToDate(GregorianCalendar calendar) {
        return calendar.getTime();
    }




    public static long calculateDifferenceInDates(Date firstDate, Date secondDate){

        long diffDays;

        //in milliseconds
        long diff = firstDate.getTime() - secondDate.getTime();

        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;

        diffDays = diff / (24 * 60 * 60 * 1000);

        return diffDays;


    }





    public static boolean isTimeAutomatic(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.Global.getInt(c.getContentResolver(), Settings.Global.AUTO_TIME, 0) == 1;
        } else {
            return android.provider.Settings.System.getInt(c.getContentResolver(), android.provider.Settings.System.AUTO_TIME, 0) == 1;
        }
    }

    public static boolean isTimeCurrentValid(){

       Date date = DateCalendarConverter.dateWithTimeToDateWithoutTime(new Date());

       Calendar calendar = Calendar.getInstance();
       calendar.set(Calendar.MONTH,0);
       calendar.set(Calendar.YEAR,2015);
       calendar.set(Calendar.DAY_OF_MONTH,1);

       Date date1 = DateCalendarConverter.dateToDateWithoutTimeAndDay(calendar.getTime());

       Log.d("DATECHECK","now date: "+date+" old date: "+date1);

       if(!date.equals(date1)){

           return true;
       }

     return false;
    }







    static  String dateString = null;
    public static String dateToString(Date date) {
       //  SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.US);
       SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.US);
       dateString = dateFormatter.format(date);
       return dateString;
    }



    public static String dateToStringWithoutTime(Date date) {
        String justDateNoTime;

        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMMM d, yyyy", Locale.US);
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

        //SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        justDateNoTime = dateFormatter.format(date);
        return justDateNoTime;
    }


    public static Date stringToDateWithoutTime(String date) {
        Date date1 = null;
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        //SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        try {
            date1 = dateFormatter.parse(date);
        } catch (ParseException e) {
        }
        return date1;
    }






    public static String dateToStringWithoutTimeAndDay(Date date) {
        String justDateNoTime;

        //  SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.US);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMMM, yyyy", Locale.US);
        justDateNoTime = dateFormatter.format(date);
        return justDateNoTime;
    }

    public static Date dateWithTimeToDateWithoutTime(Date date) {

        return stringToDateWithoutTime(dateToStringWithoutTime(date));
    }


    //yyyy-MM-dd, EEE, dd MMM yyyy HH:mm:ss zzz




    public static Date dateToDateWithoutTimeAndDay(Date date) {
       Date date1 = null;
        String notime = DateCalendarConverter.dateToStringWithoutTimeAndDay(date);
        //  SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.US);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMMM, yyyy", Locale.US);
        try {
            date1 = dateFormatter.parse(notime);
        } catch (ParseException e) {
        }
        return date1;
    }

    public static String dateToStringGetYear(Date date) {
        String justDateNoTime;

        //  SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.US);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy", Locale.US);


        justDateNoTime = dateFormatter.format(date);

        return justDateNoTime;
    }

    public static int dateToIntGetMonth(Date date) {

         Calendar calendar = Calendar.getInstance();
         calendar.setTime(date);
         int month = calendar.get(Calendar.MONTH);

        return month;
    }





    static String  dayOfWeekString = null;
    public static String dateToDayOfWeek(Date date) {

        Calendar  mycal =  DateCalendarConverter.dateToCalendar(date);
        int dayofweek = mycal.get(Calendar.DAY_OF_WEEK);
        if (dayofweek == 1){

            dayOfWeekString = "Sunday";

        }
        else if (dayofweek == 2){
            dayOfWeekString = "Monday";

        }
        else if(dayofweek == 3){

            dayOfWeekString = "Tuesday";
        }
        else if (dayofweek == 4){
            dayOfWeekString = "Wednesday";

        }
        else if (dayofweek == 5){

            dayOfWeekString = "Thursday";
        }
        else if(dayofweek == 6){
            dayOfWeekString = "Friday";

        }
        else if (dayofweek == 7){
            dayOfWeekString = "Saturday";

        }



        return dayOfWeekString;
    }


    private static final  String JAN = "January";
    private static final  String FEB = "February";
    private static final  String MAR = "March";
    private static final  String APR = "April";
    private static final  String MAY = "May";
    private static final  String JUN = "June";
    private static final  String JUL = "July";
    private static final  String AUG = "August";
    private static final  String SEP = "September";
    private static final  String OCT = "October";
    private static final  String NOV = "November";
    private static final  String DEC = "December";







    public static Map<Integer,String> monthHashMap(){

        Map<Integer,String> monthArray = new HashMap<>();
        monthArray.put(0,JAN);
        monthArray.put(1,FEB);
        monthArray.put(2,MAR);
        monthArray.put(3,APR);
        monthArray.put(4,MAY);
        monthArray.put(5,JUN);
        monthArray.put(6,JUL);
        monthArray.put(7,AUG);
        monthArray.put(8,SEP);
        monthArray.put(9,OCT);
        monthArray.put(10,NOV);
        monthArray.put(11,DEC);

        return monthArray;
    }



    static String monthInt ="";
    public static String monthIntToString(int month) {


        if (month == 0){

            monthInt = "January";

        }

       else if (month == 1){

            dayOfWeekString = "February";

        }
        else if (month == 2){
            dayOfWeekString = "March";

        }
        else if(month == 3){

            dayOfWeekString = "April";
        }
        else if (month == 4){
            dayOfWeekString = "May";

        }
        else if (month == 5){

            dayOfWeekString = "June";
        }
        else if(month == 6){
            dayOfWeekString = "July";

        }
        else if (month == 7){
            dayOfWeekString = "August";

        }

        else if (month == 8){
            dayOfWeekString = "September";

        }

        else if (month == 9){
            dayOfWeekString = "October";

        }
        else if (month == 10){
            dayOfWeekString = "November";

        }
        else if (month == 11){
            dayOfWeekString = "December";

        }


        return dayOfWeekString;
    }


    public static int monthStringToInt(String month) {
        int monthInt = 0;

        if (month.equals(JAN)){ monthInt = 0; }

        else if (month.equals(FEB)){ monthInt = 1; }
        else if (month.equals(MAR)){ monthInt = 2; }
        else if(month.equals(APR)){ monthInt = 3; }
        else if (month.equals(MAY)){ monthInt = 4; }
        else if (month.equals(JUN)){ monthInt = 5; }
        else if(month.equals(JUL)){ monthInt = 6; }
        else if (month.equals(AUG)){ monthInt = 7; }
        else if (month.equals(SEP)){ monthInt = 8; }
        else if (month.equals(OCT)){ monthInt = 9; }
        else if (month.equals(NOV)){ monthInt = 10; }
        else if (month.equals(DEC)){ monthInt = 11; }


        return monthInt;
    }



    public static MyDateMonth dateToMyMonth(int month) {

        months = new ArrayList<>();

        MyDateMonth jan = new MyDateMonth(0,"Jan");
        MyDateMonth feb = new MyDateMonth(1,"Feb");
        MyDateMonth mar = new MyDateMonth(2,"March");
        MyDateMonth apr = new MyDateMonth(3,"April");
        MyDateMonth may = new MyDateMonth(4,"May");
        MyDateMonth jun = new MyDateMonth(5,"June");
        MyDateMonth jul = new MyDateMonth(6,"July");
        MyDateMonth aug = new MyDateMonth(7,"August");
        MyDateMonth sep = new MyDateMonth(8,"Sept");
        MyDateMonth ot = new MyDateMonth(9,"Oct");
        MyDateMonth nov = new MyDateMonth(10,"Nov");
        MyDateMonth dec = new MyDateMonth(11,"Dec");

        months.add(jan);
        months.add(feb);
        months.add(mar);
        months.add(apr);
        months.add(may);
        months.add(jun);
        months.add(jul);
        months.add(aug);
        months.add(sep);
        months.add(ot);
        months.add(nov);
        months.add(dec);

        MyDateMonth myMonthReturn = null;

        for(MyDateMonth myMonth: months){
            if(myMonth.getMonthNo() == month){

                myMonthReturn = myMonth;

            }
        }


        return myMonthReturn;
    }


    private static List<MyDateMonth> months;

    public static String dateToMonth(String date) {

        months = new ArrayList<>();

        MyDateMonth jan = new MyDateMonth(0,"January");
        MyDateMonth feb = new MyDateMonth(1,"February");
        MyDateMonth mar = new MyDateMonth(2,"March");
        MyDateMonth apr = new MyDateMonth(3,"April");
        MyDateMonth may = new MyDateMonth(4,"May");
        MyDateMonth jun = new MyDateMonth(5,"June");
        MyDateMonth jul = new MyDateMonth(6,"July");
        MyDateMonth aug = new MyDateMonth(7,"August");
        MyDateMonth sep = new MyDateMonth(8,"September");
        MyDateMonth ot = new MyDateMonth(9,"October");
        MyDateMonth nov = new MyDateMonth(10,"November");
        MyDateMonth dec = new MyDateMonth(11,"December");

        months.add(jan);
        months.add(feb);
        months.add(mar);
        months.add(apr);
        months.add(may);
        months.add(jun);
        months.add(jul);
        months.add(aug);
        months.add(sep);
        months.add(ot);
        months.add(nov);
        months.add(dec);

        Calendar  mycal =  DateCalendarConverter.dateToCalendar(DateCalendarConverter.stringToDateWithoutTime(date));

        int month = mycal.get(Calendar.DAY_OF_WEEK);

        String monthString = null;

        for(MyDateMonth myMonth: months){

            if(myMonth.getMonthNo() == month){

                monthString = myMonth.getMonthName();
            }
        }

        return monthString;
    }






    public static String periodAge = "";

    public static float ageCalculator(Date dateOfBirth){
        float age;

        Calendar today = Calendar.getInstance();
        Calendar birthDate = dateToCalendar(dateOfBirth);

        birthDate.setTime(dateOfBirth);

        age = today.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);

        Log.d("DCCAGE","Age Calc: "+monthAge);

        Log.d("DateCalendarConverter: ","Age "+age);


        if(age == 0){
            periodAge = "Month";

           age = getAgeMonths(today,birthDate);

           // age = ((  (float)  monthAge / (float) 12));

        }
        else {
            periodAge = "Year";

        }

        return age;


    }


    private static int getAgeYear(Calendar today,  Calendar birthDate) {

        int yearAge;

        if (birthDate.after(today)) {
            throw new IllegalArgumentException("Can't be born in the future");
        }

        yearAge = today.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);


        Log.d("DateCalendarConverter: ","Date Today "+today.getTime());
        Log.d("DateCalendarConverter: ","Date Birth "+birthDate.getTime());
        Log.d("DateCalendarConverter: ","Year Today "+today.get(Calendar.YEAR));
        Log.d("DateCalendarConverter: ","Year Birth "+birthDate.get(Calendar.YEAR));


        // If birth date is greater than todays date (after 2 days adjustment of leap year) then decrement age one year
        if ( (birthDate.get(Calendar.DAY_OF_YEAR) - today.get(Calendar.DAY_OF_YEAR) > 3) ||
                (birthDate.get(Calendar.MONTH) > today.get(Calendar.MONTH ))){
            yearAge--;

            // If birth date and todays date are of same month and birth day of month is greater than todays day of month then decrement age
        }else if ((birthDate.get(Calendar.MONTH) == today.get(Calendar.MONTH )) &&
                (birthDate.get(Calendar.DAY_OF_MONTH) > today.get(Calendar.DAY_OF_MONTH ))){
            yearAge--;
        }

        return yearAge;
    }


    private static int monthAge = 0;
    private static int getAgeMonths(Calendar today,  Calendar birthDate) {

        if (birthDate.after(today)) {
            throw new IllegalArgumentException("Can't be born in the future");
        }

        monthAge = today.get(Calendar.MONTH) - birthDate.get(Calendar.MONTH);


        return monthAge;
    }






}
