package com.vims.vimsapp.utilities;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class PreferenceHandler {


    private Context mContext;

    public static final String USER_ID= "user_id";
    public static final String USER_HOSPITAL_ID="user_hospital_id";
    public static final String USER_HOSPITAL_NAME="user_hospital_name";
    public static final String USER_HOSPITAL_ADDRESS="user_hospital_address";
    public static final String USER_NAME="user_name";
    public static final String USER_PASSWORD="user_password";
    public static final String USER_TOKEN="user_token";
    public static final String USER_EMAIL="user_email";

    public static final String DATE_ACCURATE_TIME_KEY ="accurate_time";

    public static final String SYNC_TIME_KEY ="last_sync_time";

    //saves the order month of the spinner so as to colect the events of that month for order
    public static final String ORDER_MONTH_KEY ="order_month";
    public static final String ORDER_DAY_KEY ="order_day";


    //saves the next date of vaccination as along
    public static final String SCHEDULE_DATE_NOTIFY_KEY_LONG ="notify_hospital_schedule_date";
    public static final String ALARM_STATE_KEY ="alarm_state";


    //check whether the schedule was loaded in order to authorize reg child
    public static final String SCHEDULE_LOADED_INT ="hospital_schedule_loaded";

    public static final String HOSPITAL_SCHEDULE_CREATION_DATE="hospital_date_created";


    //CHECKS WHETHER the vaccine was recorded or not so as to refresh a view or not during onResume
    public static final String IS_VACCINE_EVENT_RECORDED ="is_vaccine_event_recorded";

    //CHECKS WHETHER the vaccine was recorded or not so as to refresh a view or not during onResume
    public static final String IS_CHILD_REGISTERED_INT ="is_child_registered";


    //CHECKS WHETHER the vaccine was recorded or not so as to refresh a view or not during onResume
    public static final String IS_CARETAKER_REGISTERED_INT ="is_caretaker_registered";




    private static PreferenceHandler INSTANCE = null;

    // Prevent direct instantiation.
    private PreferenceHandler(Context context) {
        this.mContext = context;
    }

    public static PreferenceHandler getInstance(Context context) {

        if (INSTANCE == null) {
            INSTANCE = new PreferenceHandler(context);
        }
        return INSTANCE;
    }






    public SharedPreferences getSharedPreferencesInstance(){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        return prefs;
    }


    public void putPref(String key, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getPref(String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return preferences.getString(key, null);
    }


    public void putLongPref(String key, long value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(key,value);
        editor.apply();
    }

    public long getLongPref(String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return preferences.getLong(key, 0);
    }



    public void putBoolPref(String key, boolean value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key,value);
        editor.apply();
    }

    public boolean getBoolPref(String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return preferences.getBoolean(key, false);
    }




    //  AT BIRTH OPV0,BCG,OPV1,Hib1,PCV1,Rota1,OPV2,Hib2,PCV2,Rota2,OPV3
    //  Hib3,PCV3,Rota3,IPV,Measles


    public void initPrefStat() {
        putPrefStat("OPV0",0);
        putPrefStat("BCG",0);
        putPrefStat("OPV1",0);
        putPrefStat("Hib1",0);
        putPrefStat("PCV1",0);
        putPrefStat("Rota1",0);
        putPrefStat("OPV2",0);
        putPrefStat("Hib2",0);
        putPrefStat("PCV2",0);
        putPrefStat("Rota2",0);
        putPrefStat("OPV3",0);
        putPrefStat("Hib3",0);
        putPrefStat("PCV3",0);
        putPrefStat("Rota3",0);
        putPrefStat("IPV",0);
        putPrefStat("Measles",0);
    }

    public void putPrefStat(String key, int value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.apply();
    }





    public Boolean isUserRegistered(String key) {


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        String value = preferences.getString(key, null);

        boolean b = Boolean.parseBoolean(value);
        return b;

    }


    public void setCoverage(String vaccineName, int month_value){

        setIntValue(vaccineName,month_value);

    }




    public void setIntValue(String key, int value) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = prefs.edit();


        editor.putInt(key, value);


        editor.apply();
    }

    public int getIntValue(String key) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        int value = preferences.getInt(key, 0);


        return value;

    }


}