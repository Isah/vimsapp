package com.vims.vimsapp.utilities;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vims.vimsapp.manageorders.VaccinePlan;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.viewcoverage.CoverageTimeLine;
import com.vims.vimsapp.viewcoverage.VaccineReport;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;


public class ListTypeConverter {

        static  Gson gson = new Gson();

        @TypeConverter
        public static List<VaccinationEvent> stringToSomeObjectList(String data) {
            if (data == null) {
                return Collections.emptyList();
            }

            Type listType = new TypeToken<List<VaccinationEvent>>() {}.getType();

            return gson.fromJson(data, listType);
        }

        @TypeConverter
        public static String someObjectListToString(List<VaccinationEvent> someObjects) {
            return gson.toJson(someObjects);
        }


    @TypeConverter
    public static List<VaccineReport> stringToSomeReportList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<VaccineReport>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someReportListToString(List<VaccineReport> someObjects) {
        return gson.toJson(someObjects);
    }



    @TypeConverter
    public static List<CoverageTimeLine> stringToTimeLineList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<CoverageTimeLine>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someTimeLineListToString(List<CoverageTimeLine> someObjects) {
        return gson.toJson(someObjects);
    }


    @TypeConverter
    public static List<VaccinePlan> stringToPlanineList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<VaccinePlan>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String somePlanLineListToString(List<VaccinePlan> someObjects) {
        return gson.toJson(someObjects);
    }



    @TypeConverter

    public static VaccinePlan fromStringToPlan(String value) {

        Type listType = new TypeToken<VaccinePlan>() {}.getType();

        return new Gson().fromJson(value, listType);

    }

    @TypeConverter

    public static String fromPlanToString(VaccinePlan caretaker) {

        Gson gson = new Gson();

        String json = gson.toJson(caretaker);

        return json;

    }




}
