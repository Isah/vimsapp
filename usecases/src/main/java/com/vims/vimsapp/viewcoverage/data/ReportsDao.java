package com.vims.vimsapp.viewcoverage.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.vims.vimsapp.viewcoverage.CoverageReport;

/**
 * Created by root on 5/25/18.
 */

 //1. annotate your Dao
 @Dao
public interface ReportsDao {

  //2. Additionally, for bulkInsert, you want to use a OnConflictStrategy.REPLACE
  // so that when Sunshine re-downloads forecasts, old weatherEntry forecasts are replaced by new ones
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void InsertReport(CoverageReport coverageReport);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int UpdateReport(CoverageReport coverageReport);


    //3. get weather data by unique date (:date  is a parameter)
    @Query("SELECT * FROM coverage_report")
    CoverageReport getCoverageReport();

    @Query("SELECT count(*) FROM coverage_report")
    int getNoCoverageReports();


    @Query("DELETE FROM coverage_report")
    int deleteCoverageReport();



}

