package com.vims.vimsapp.viewcoverage.data;

/*
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.vims.vimsapp.domain.patients.entity.vaccinated.Caretaker;
import com.vims.vimsapp.domain.patients.entity.vaccinated.Child;
import com.vims.vimsapp.domain.patients.entity.vaccinated.SyncUpdateTime;
import com.vims.vimsapp.domain.patients.entity.vaccinations.MonthTargetPopulation;
import com.vims.vimsapp.domain.patients.entity.vaccinations.VaccinationEvent;
import com.vims.vimsapp.domain.patients.entity.vaccinations.VaccinationEventsPack;
import com.vims.vimsapp.domain.patients.entity.vaccinations.VaccinationLocationState;
import com.vims.vimsapp.domain.patients.entity.vaccinations.VaccinationStatus;
import com.vims.vimsapp.domain.reports.CoverageReport;

import com.vims.vimsapp.domain.reports.CoverageTimeLine;
import com.vims.vimsapp.domain.reports.VaccineReportsCard;
import com.vims.vimsapp.utilities.AppExecutors;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.PreferenceHandler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;



public class ReportsLocalDataSource implements ReportsDataSource{


    private static volatile ReportsLocalDataSource INSTANCE;

    private ReportsDao mReportsDao;
    private ReportsVaccineCardDao mReportsVaccineCardDao;
    private AppExecutors mAppExecutors;



    //caretakerDao = PatientsDatabase.getInstance(context).getCaretakerDao();
    //childDao = PatientsDatabase.getInstance(context).getChildDao();

    // Prevent direct instantiation.
    private ReportsLocalDataSource(@NonNull AppExecutors appExecutors, @NonNull ReportsDao reportsDao, ReportsVaccineCardDao reportsVaccineCardDao) {
        mAppExecutors = appExecutors;
        mReportsDao = reportsDao;
        mReportsVaccineCardDao = reportsVaccineCardDao;
    }


    public static ReportsLocalDataSource getInstance(@NonNull AppExecutors appExecutors, @NonNull ReportsDao reportsDao, ReportsVaccineCardDao reportsVaccineCardDao) {
        if (INSTANCE == null) {
            synchronized (ReportsLocalDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ReportsLocalDataSource(appExecutors, reportsDao,reportsVaccineCardDao);
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public void getCoverageReport(boolean isRefresh, final String vaccine, int year, final GetReportsCallback reportsCallback) {


        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                 final CoverageReport coverageReport = mReportsDao.getCoverageReport();
                 VaccineReportsCard reportsCard = mReportsVaccineCardDao.getVaccineReportsCard(vaccine);
                 List<CoverageTimeLine> timeLines = mReportsVaccineCardDao.getCoverageTimeLines(vaccine);
                reportsCard.setCoverageTimeLines(timeLines);

                coverageReport.setVaccineReportsCard(reportsCard);

                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (coverageReport != null) {
                            reportsCallback.onReportsLoaded(coverageReport);
                        } else {
                            reportsCallback.onFailed("no report");
                        }
                    }
                });
            }
        };

        mAppExecutors.diskIO().execute(runnable);




    }

    @Override
    public void saveCoverageReport(final CoverageReport coverageReport, final SaveReportsCallback reportsCallback) {

        Runnable saveRunnable = new Runnable() {
            @Override
            public void run() {

                try {

                    mReportsDao.InsertReport(coverageReport);
                    mReportsVaccineCardDao.InsertCards(coverageReport.getVaccineReportsCards());

                    for(VaccineReportsCard card: coverageReport.getVaccineReportsCards()){

                      List<CoverageTimeLine> coverageTimeLines =  card.getCoverageTimeLines();
                      mReportsVaccineCardDao.InsertTimeLines(coverageTimeLines);

                      for(CoverageTimeLine coverageTimeLine: coverageTimeLines){
                          Log.d("ReportsApi", "coverage: vac: "+coverageTimeLine.getVaccine()+" cov: "+coverageTimeLine.getCoverage()+" yr: "+coverageTimeLine.getYear());
                      }

                    }

                    reportsCallback.onReportsSaved(coverageReport);

                } catch (Exception e) {

                    reportsCallback.onFailed(e.getLocalizedMessage());
                    Log.d("ReportsApi", "error: "+e.getMessage());

                }

            }

        };
        mAppExecutors.diskIO().execute(saveRunnable);


    }



    @Override
    public void refreshReports() {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                int rowsAffected1 = mReportsDao.deleteCoverageReport();
                int rowsAffected2 = mReportsVaccineCardDao.deleteCards();
                int rowsAffected3 = mReportsVaccineCardDao.deleteTimeLines();

            }
        };

        mAppExecutors.diskIO().execute(runnable);


    }

}

*/