package com.vims.vimsapp.viewcoverage.data;

import android.support.annotation.NonNull;

import com.vims.vimsapp.viewcoverage.CoverageReport;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 6/24/18.
 */

public class ReportsRemoteDataSource implements ReportsDataSource {

    private com.vims.vimsapp.domain.reports.data.ReportsApi mPatientsApi;


    private static volatile ReportsRemoteDataSource INSTANCE;

    // Prevent direct instantiation.
    private ReportsRemoteDataSource(@NonNull com.vims.vimsapp.domain.reports.data.ReportsApi patientsApi) {

         this.mPatientsApi = patientsApi;

    }

    public static ReportsRemoteDataSource getInstance(@NonNull com.vims.vimsapp.domain.reports.data.ReportsApi patientsApi) {
        if (INSTANCE == null) {
            synchronized (ReportsRemoteDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ReportsRemoteDataSource(patientsApi);
                }
            }
        }
        return INSTANCE;
    }


    @Override
    public void getCoverageReport(boolean isNetOn,String vaccine, int yearDate, final GetReportsCallback reportsCallback) {

        //Call<Object>  executes the request to the Api

        Call<CoverageReport> callSync = mPatientsApi.getCoverageReport(yearDate);

        callSync.enqueue(new Callback<CoverageReport>() {
            @Override
            public void onResponse(Call<CoverageReport> call, Response<CoverageReport> response) {
                reportsCallback.onReportsLoaded(response.body());
            }

            @Override
            public void onFailure(Call<CoverageReport> call, Throwable t) {

                reportsCallback.onFailed(t.getMessage());
            }
        });

    }

    @Override
    public void saveCoverageReport(CoverageReport coverageReport, SaveReportsCallback reportsCallback) {

        //Call<Object>  executes the request to the Api


    }

    @Override
    public void refreshReports() {

    }
}
