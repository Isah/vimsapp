package com.vims.vimsapp.viewcoverage;

import java.util.Date;

public class DashBoardHeader{

    private Date nextHospitalDate;
    private int upcomingVaccinations;
    private int missedVaccinations;
    private int upcomingVaccinationsCurrentMonth;
    private int missedVaccinationsCurrentMonth;
    private int doneVaccinations;
    private int totalChildrenImmunised;

    public int getUpcomingVaccinationsCurrentMonth() {
        return upcomingVaccinationsCurrentMonth;
    }

    public void setUpcomingVaccinationsCurrentMonth(int upcomingVaccinationsCurrentMonth) {
        this.upcomingVaccinationsCurrentMonth = upcomingVaccinationsCurrentMonth;
    }

    public int getMissedVaccinationsCurrentMonth() {
        return missedVaccinationsCurrentMonth;
    }

    public void setMissedVaccinationsCurrentMonth(int missedVaccinationsCurrentMonth) {
        this.missedVaccinationsCurrentMonth = missedVaccinationsCurrentMonth;
    }

    public int getDoneVaccinations() {
        return doneVaccinations;
    }

    public void setDoneVaccinations(int doneVaccinations) {
        this.doneVaccinations = doneVaccinations;
    }

    public int getTotalChildrenImmunised() {
        return totalChildrenImmunised;
    }

    public void setTotalChildrenImmunised(int totalChildrenImmunised) {
        this.totalChildrenImmunised = totalChildrenImmunised;
    }

    public Date getNextHospitalDate() {
        return nextHospitalDate;
    }

    public void setNextHospitalDate(Date nextHospitalDate) {
        this.nextHospitalDate = nextHospitalDate;
    }

    public int getUpcomingVaccinations() {
        return upcomingVaccinations;
    }

    public void setUpcomingVaccinations(int upcomingVaccinations) {
        this.upcomingVaccinations = upcomingVaccinations;
    }

    public int getMissedVaccinations() {
        return missedVaccinations;
    }

    public void setMissedVaccinations(int missedVaccinations) {
        this.missedVaccinations = missedVaccinations;
    }
}

