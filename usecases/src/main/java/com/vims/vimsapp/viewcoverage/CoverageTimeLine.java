package com.vims.vimsapp.viewcoverage;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.UUID;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "vaccine_report_timeline",indices = {@Index(value = {"reportId"})}, foreignKeys = @ForeignKey(entity = CoverageReport.class,
        parentColumns = "reportId",
        childColumns = "reportId",
        onDelete = CASCADE))
public class CoverageTimeLine {

      @PrimaryKey //(autoGenerate = true)
      @NonNull
      private String timelineId;
      private String vaccine;
      private String reportId;
      private int coverage;
      private int year;
      private int targetPopulation;

      @Ignore
      public CoverageTimeLine() {
          setTimelineId(UUID.randomUUID().toString());
      }


    public CoverageTimeLine(@NonNull String timelineId, String vaccine, String reportId, int coverage, int year) {
        this.timelineId = timelineId;
        this.vaccine = vaccine;
        this.reportId = reportId;
        this.coverage = coverage;
        this.year = year;
    }

    public int getTargetPopulation() {
        return targetPopulation;
    }

    public void setTargetPopulation(int targetPopulation) {
        this.targetPopulation = targetPopulation;
    }

    public String getTimelineId() {
        return timelineId;
    }

    public void setTimelineId(String timelineId) {
        this.timelineId = timelineId;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getVaccine() {
        return vaccine;
    }

    public void setVaccine(String vaccine) {
        this.vaccine = vaccine;
    }

    public int getCoverage() {
        return coverage;
    }

    public void setCoverage(int coverage) {
        this.coverage = coverage;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
