package com.vims.vimsapp.viewcoverage.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.vims.vimsapp.viewcoverage.CoverageTimeLine;
import com.vims.vimsapp.viewcoverage.VaccineReportsCard;

import java.util.List;

/**
 * Created by root on 5/25/18.
 */

 //1. annotate your Dao
 @Dao
public interface ReportsVaccineCardDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void InsertCards(List<VaccineReportsCard> vaccineReportsCards);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int UpdateCards(List<VaccineReportsCard> vaccineReportsCard);

    @Query("SELECT * FROM vaccine_report_card")
    List<VaccineReportsCard> getVaccineReportsCards();

    @Query("SELECT * FROM vaccine_report_card WHERE vaccineName = :vaccine")
    VaccineReportsCard getVaccineReportsCard(String vaccine);



    @Query("DELETE FROM vaccine_report_card")
    int deleteCards();


    //timelines

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void InsertTimeLines(List<CoverageTimeLine> coverageTimeLines);

    @Query("SELECT * FROM vaccine_report_timeline WHERE vaccine = :vaccine")
    List<CoverageTimeLine> getCoverageTimeLines(String vaccine);


    @Query("DELETE FROM vaccine_report_timeline")
    int deleteTimeLines();




}

