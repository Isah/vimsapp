package com.vims.vimsapp.domain.reports.data;



import com.vims.vimsapp.viewcoverage.CoverageReport;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by root on 6/5/18.
 */

public interface ReportsApi {
        //@FormUrlEncoded

        @GET("api/reports/{year}")
        Call<CoverageReport> getCoverageReport(@Path("year") int year);


        /*
        @GET("api/caretakers/{hospitalId}")
        Call<List<Caretaker>> getCaretakers(@Path("hospitalId") String hospitalId);

        @POST("api/addcaretaker/")
        Call<Caretaker> addCaretaker(@Body Caretaker caretaker);
        */




}
