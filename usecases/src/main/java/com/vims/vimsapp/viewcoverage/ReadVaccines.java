package com.vims.vimsapp.viewcoverage;


import com.vims.vimsapp.entities.vaccinations.VaccineSpecification;

import java.util.ArrayList;
import java.util.List;

public class ReadVaccines {



    private static volatile ReadVaccines INSTANCE;
    public static final String MEASLES_VACCINE = "Measles";
    public static final String OPV0_VACCINE = "OPV0";
    public static final String BCG_VACCINE = "BCG";


    // Prevent direct instantiation.
    private ReadVaccines() {

    }


    public static ReadVaccines getInstance() {
        if (INSTANCE == null) {
            synchronized (ReadVaccines.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ReadVaccines();
                }
            }
        }
        return INSTANCE;
    }





        final String rotaMode = "Orally slow administration on inner Aspect of the check";
        final String rotaSite = "Mouth";

    public List<VaccineSpecification> getVaccineSpecificationList(){
        final String polioDisease = "Polio";
        final String polioMode = "Oral";
        final String polioSite = "Mouth";

        final String pcvDisease = "Pneumococcus";
        final String pcvMode = "IM";
        final String pcvSite = "Rt. Thigh outer upper aspect";

        final String hipbDisease = "Haemophilus influenzae type b";
        final String hipbMode = "IM";
        final String hipbSite = "Lt thigh upper outer aspect";

        final String rotaDisease = "Rota Virus";
        final String rotaMode = "Orally slow administration on inner Aspect of the check";
        final String rotaSite = "Mouth";


        ArrayList<VaccineSpecification> vaccines = new ArrayList<>();


        //AT BIRTH
        VaccineSpecification vpecOpv0 = new VaccineSpecification();
        vpecOpv0.setVaccineName("OPV0");
        vpecOpv0.setProtectsAgainst(polioDisease);
        vpecOpv0.setModeOfAdministration(polioMode);
        vpecOpv0.setSiteOfAdministration(polioSite);
        vpecOpv0.setDaysGivenAfter(14);

        VaccineSpecification vspecBcg = new VaccineSpecification();
        vspecBcg.setVaccineName("BCG");
        vspecBcg.setProtectsAgainst("BCG Virus");
        vspecBcg.setModeOfAdministration("Intra dermal");
        vspecBcg.setSiteOfAdministration("Rt. Upper arm");
        vspecBcg.setDaysGivenAfter(14);


        //AFTER 6 WEEKS
        VaccineSpecification vpecOpv1 = new VaccineSpecification();
        vpecOpv1.setVaccineName("OPV1");
        vpecOpv1.setProtectsAgainst(polioDisease);
        vpecOpv1.setModeOfAdministration(polioMode);
        vpecOpv1.setSiteOfAdministration(polioSite);
        vpecOpv1.setDaysGivenAfter(42);

        VaccineSpecification vspecHib1 = new VaccineSpecification();
        vspecHib1.setVaccineName("Hib1");
        vspecHib1.setProtectsAgainst(hipbDisease);
        vspecHib1.setModeOfAdministration(hipbMode);
        vspecHib1.setSiteOfAdministration(hipbSite);
        vspecHib1.setDaysGivenAfter(42);

        VaccineSpecification vspecPcv1 = new VaccineSpecification();
        vspecPcv1.setVaccineName("PCV1");
        vspecPcv1.setProtectsAgainst(pcvDisease);
        vspecPcv1.setModeOfAdministration(pcvMode);
        vspecPcv1.setSiteOfAdministration(pcvSite);
        vspecPcv1.setDaysGivenAfter(42);

        VaccineSpecification vspecRota1 = new VaccineSpecification();
        vspecRota1.setVaccineName("Rota1");
        vspecRota1.setProtectsAgainst(rotaDisease);
        vspecRota1.setModeOfAdministration(rotaMode);
        vspecRota1.setSiteOfAdministration(rotaSite);
        vspecRota1.setDaysGivenAfter(42);



        //AFTER 10 WEEKS
        VaccineSpecification vpecOpv2 = new VaccineSpecification();
        vpecOpv2.setVaccineName("OPV2");
        vpecOpv2.setProtectsAgainst(polioDisease);
        vpecOpv2.setModeOfAdministration(polioMode);
        vpecOpv2.setSiteOfAdministration(polioSite);
        vpecOpv2.setDaysGivenAfter(72);

        VaccineSpecification vspecHib2 = new VaccineSpecification();
        vspecHib2.setVaccineName("Hib2");
        vspecHib2.setProtectsAgainst(hipbDisease);
        vspecHib2.setModeOfAdministration(hipbMode);
        vspecHib2.setSiteOfAdministration(hipbSite);
        vspecHib2.setDaysGivenAfter(72);

        VaccineSpecification vspecPcv2 = new VaccineSpecification();
        vspecPcv2.setVaccineName("PCV2");
        vspecPcv2.setProtectsAgainst(pcvDisease);
        vspecPcv2.setModeOfAdministration(pcvMode);
        vspecPcv2.setSiteOfAdministration(pcvSite);
        vspecPcv2.setDaysGivenAfter(72);

        VaccineSpecification vspecRota2 = new VaccineSpecification();
        vspecRota2.setVaccineName("Rota2");
        vspecRota2.setProtectsAgainst(rotaDisease);
        vspecRota2.setModeOfAdministration(rotaMode);
        vspecRota2.setSiteOfAdministration(rotaSite);
        vspecRota2.setDaysGivenAfter(72);



        //AFTER 14 WEEKS
        VaccineSpecification vpecOpv3 = new VaccineSpecification();
        vpecOpv3.setVaccineName("OPV3");
        vpecOpv3.setProtectsAgainst(polioDisease);
        vpecOpv3.setModeOfAdministration(polioMode);
        vpecOpv3.setSiteOfAdministration(polioSite);
        vpecOpv3.setDaysGivenAfter(98);

        VaccineSpecification vspecHib3 = new VaccineSpecification();
        vspecHib3.setVaccineName("Hib3");
        vspecHib3.setProtectsAgainst(hipbDisease);
        vspecHib3.setModeOfAdministration(hipbMode);
        vspecHib3.setSiteOfAdministration(hipbSite);
        vspecHib3.setDaysGivenAfter(98);

        VaccineSpecification vspecPcv3 = new VaccineSpecification();
        vspecPcv3.setVaccineName("PCV3");
        vspecPcv3.setProtectsAgainst(pcvDisease);
        vspecPcv3.setModeOfAdministration(pcvMode);
        vspecPcv3.setSiteOfAdministration(pcvSite);
        vspecPcv3.setDaysGivenAfter(98);

        VaccineSpecification vspecRota3 = new VaccineSpecification();
        vspecRota3.setVaccineName("Rota3");
        vspecRota3.setProtectsAgainst(rotaDisease);
        vspecRota3.setModeOfAdministration(rotaMode);
        vspecRota3.setSiteOfAdministration(rotaSite);
        vspecRota3.setDaysGivenAfter(98);

        VaccineSpecification vspecIpv = new VaccineSpecification();
        vspecIpv.setVaccineName("IPV");
        vspecIpv.setProtectsAgainst("Polio");
        vspecIpv.setModeOfAdministration("IM");
        vspecIpv.setSiteOfAdministration("Rt. Thigh outer upper aspect");
        vspecIpv.setDaysGivenAfter(98);



        //AFTER 9 MONTHS
        VaccineSpecification vspecMeasles = new VaccineSpecification();
        vspecMeasles.setVaccineName("Measles");
        vspecMeasles.setProtectsAgainst("Measles");
        vspecMeasles.setModeOfAdministration("SC");
        vspecMeasles.setSiteOfAdministration("Lt Upper Arm");
        vspecMeasles.setDaysGivenAfter(252);


        vaccines.add(vpecOpv0);
        vaccines.add(vspecBcg);

        vaccines.add(vpecOpv1);
        vaccines.add(vspecHib1);
        vaccines.add(vspecPcv1);
        vaccines.add(vspecRota1);

        vaccines.add(vpecOpv2);
        vaccines.add(vspecHib2);
        vaccines.add(vspecPcv2);
        vaccines.add(vspecRota2);

        vaccines.add(vpecOpv3);
        vaccines.add(vspecHib3);
        vaccines.add(vspecPcv3);
        vaccines.add(vspecRota3);

        vaccines.add(vspecIpv);
        vaccines.add(vspecMeasles);




        return vaccines;

    }


}
