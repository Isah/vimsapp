package com.vims.vimsapp.viewcoverage.data;

import android.util.Log;

import com.vims.vimsapp.viewcoverage.CoverageReport;
import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;

import java.util.Map;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

/**
 * Created by root on 5/26/18.
 */


/**
 * Concrete implementation to load tasks from the data sources into a cache.
 * <p>
 * For simplicity, this implements a dumb synchronisation between locally persisted data and data
 * obtained from the server, by using the remote data source only if the local database doesn't
 * exist or is empty.
 */
 public class ReportsRepository implements ReportsDataSource{


    private static ReportsRepository INSTANCE = null;
    private final ReportsDataSource mReportsRemoteDataSource;
    private final ReportsDataSource mReportsLocalDataSource;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    public Map<String, Caretaker> mCachedCaretakers;

    public  Map<String, Child> mCachedChidren;


    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */
    private  boolean mCaretakerCacheIsDirty = false;
    private  boolean mChildrenCacheIsDirty = false;
    private  boolean mCachedVaccCardsCacheIsDirty = false;


        // Prevent direct instantiation.

    private ReportsRepository(ReportsDataSource patientsRemoteDataSource, ReportsDataSource patientsLocalDataSource) {
           mReportsRemoteDataSource = checkNotNull(patientsRemoteDataSource);
           mReportsLocalDataSource = checkNotNull(patientsLocalDataSource);

        }



    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param patientsRemoteDataSource the backend data source
     * @param patientsLocalDataSource  the device storage data source
     * @return the {@link ReportsRepository} instance
     */
    public static ReportsRepository getInstance(ReportsDataSource patientsRemoteDataSource, ReportsDataSource patientsLocalDataSource) {

        if (INSTANCE == null) {
            INSTANCE = new ReportsRepository(patientsRemoteDataSource, patientsLocalDataSource);
        }
        return INSTANCE;
    }



    public static void destroyInstance() {
        INSTANCE = null;
    }



    @Override
    public void getCoverageReport(boolean isRefresh, final String vaccine, final int yearDate, final GetReportsCallback reportsCallback) {

        checkNotNull(reportsCallback);

        if(isRefresh){
            mReportsRemoteDataSource.getCoverageReport(isRefresh,vaccine, yearDate,new GetReportsCallback() {
                @Override
                public void onReportsLoaded(CoverageReport coverageReport) {
                   //reportsCallback.onReportsLoaded(coverageReport);
                   Log.d("ReportsApi", "loaded : cov: "+coverageReport.getVaccineReportsCards().get(0).getCoveragePercentage()+" v:"+coverageReport.getVaccineReportsCards().get(0).getVaccineName());

                   mReportsLocalDataSource.refreshReports();

                   mReportsLocalDataSource.saveCoverageReport(coverageReport, new SaveReportsCallback() {
                      @Override
                      public void onReportsSaved(CoverageReport coverageReport) {


                          mReportsLocalDataSource.getCoverageReport(true,vaccine,yearDate, new GetReportsCallback() {
                              @Override
                              public void onReportsLoaded(CoverageReport coverageReport) {
                                  reportsCallback.onReportsLoaded(coverageReport);
                              }

                              @Override
                              public void onFailed(String msg) {
                                  reportsCallback.onFailed(msg);

                              }
                          });


                      }

                      @Override
                      public void onFailed(String msg) {

                      }
                  });

                }

                @Override
                public void onFailed(String msg) {
                 reportsCallback.onFailed(msg);
                }
            });


        }else {

         mReportsLocalDataSource.getCoverageReport(isRefresh,vaccine, yearDate,new GetReportsCallback() {
             @Override
             public void onReportsLoaded(CoverageReport coverageReport) {
                 reportsCallback.onReportsLoaded(coverageReport);
             }

             @Override
             public void onFailed(String msg) {
                 reportsCallback.onFailed(msg);
             }
         });


        }





    }

    @Override
    public void saveCoverageReport(CoverageReport coverageReport, SaveReportsCallback reportsCallback) {
         reportsCallback.onReportsSaved(coverageReport);
    }



    @Override
    public void refreshReports() {

    }
}
