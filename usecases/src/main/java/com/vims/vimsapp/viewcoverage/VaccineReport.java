package com.vims.vimsapp.viewcoverage;

import android.support.annotation.NonNull;


/**
 * Created by root on 5/24/18.
 */

public class VaccineReport implements Comparable<VaccineReport>{

    private String vaccineName;
    private int noOfImmunizedChildren;
    private int noOfPlannedImmunisations;
    private int noOfDelayedImmunisations;
    private int coveragePercentage;
    private int monthTarget;


    private float[] coverageImmunisedDelayed;
    private int xAxisValue;


    public int getMonthTarget() {
        return monthTarget;
    }

    public void setMonthTarget(int monthTarget) {
        this.monthTarget = monthTarget;
    }

    public int getxAxisValue() {
        return xAxisValue;
    }

    public void setxAxisValue(int xAxisValue) {
        this.xAxisValue = xAxisValue;
    }

    public float[] getCoverageImmunisedDelayed() {
        return coverageImmunisedDelayed;
    }

    public void setCoverageImmunisedDelayed(float[] coverageImmunisedDelayed) {
        this.coverageImmunisedDelayed = coverageImmunisedDelayed;
    }

    public String getVaccineName() {
        return vaccineName;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    public int getNoOfImmunizedChildren() {
        return noOfImmunizedChildren;
    }

    public void setNoOfImmunizedChildren(int noOfImmunizedChildren) {
        this.noOfImmunizedChildren = noOfImmunizedChildren;
    }

    public int getNoOfPlannedImmunisations() {
        return noOfPlannedImmunisations;
    }

    public void setNoOfPlannedImmunisations(int noOfPlannedImmunisations) {
        this.noOfPlannedImmunisations = noOfPlannedImmunisations;
    }

    public int getNoOfDelayedImmunisations() {
        return noOfDelayedImmunisations;
    }

    public void setNoOfDelayedImmunisations(int noOfDelayedImmunisations) {
        this.noOfDelayedImmunisations = noOfDelayedImmunisations;
    }

    public int getCoveragePercentage() {
        return coveragePercentage;
    }

    public void setCoveragePercentage(int coveragePercentage) {
        this.coveragePercentage = coveragePercentage;
    }



    @Override
    public int compareTo(@NonNull VaccineReport comparestu) {

        //65, 10

        int dateCmp = this.coveragePercentage - comparestu.getCoveragePercentage();

        if(dateCmp > 1){

            return -1;

        }else {

            return  1;

        }

    }

}
