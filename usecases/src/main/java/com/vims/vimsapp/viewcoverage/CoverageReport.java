package com.vims.vimsapp.viewcoverage;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


import java.util.List;
import java.util.UUID;

/**
 * Created by root on 5/24/18.
 */

@Entity(tableName = "coverage_report", indices = {@Index(value = {"reportId"}, unique = true)})
public class CoverageReport {

    @PrimaryKey //(autoGenerate = true)
    @NonNull
    private String reportId;

    private int coveragePercentage;
    private int totalImmunized;
    private int totalMissedImmunisation;
    @Ignore
    private VaccineReportsCard vaccineReportsCard;//monthly

    @Ignore
    private List<VaccineReportsCard> vaccineReportsCards;//all vacciness



    private int yearOfReport;
    private String nextHospitalDate;
    private int upcomingVaccinations;
    private int missedVaccinations;
    private int doneVaccinations;


    private int noUpcomingEventsOfCurrentMonth;
    private int noMissedEventsOfCurrentMonth;


    public CoverageReport() {
        setReportId(UUID.randomUUID().toString());
    }

    @NonNull
    public String getReportId() {
        return reportId;
    }

    public void setReportId(@NonNull String reportId) {
        this.reportId = reportId;
    }

    public List<VaccineReportsCard> getVaccineReportsCards() {
        return vaccineReportsCards;
    }

    public void setVaccineReportsCards(List<VaccineReportsCard> vaccineReportsCards) {
        this.vaccineReportsCards = vaccineReportsCards;
    }

    public int getTotalMissedImmunisation() {
        return totalMissedImmunisation;
    }

    public void setTotalMissedImmunisation(int totalMissedImmunisation) {
        this.totalMissedImmunisation = totalMissedImmunisation;
    }

    public int getYearOfReport() {
        return yearOfReport;
    }

    public void setYearOfReport(int yearOfReport) {
        this.yearOfReport = yearOfReport;
    }

    public int getNoUpcomingEventsOfCurrentMonth() {
        return noUpcomingEventsOfCurrentMonth;
    }

    public void setNoUpcomingEventsOfCurrentMonth(int noUpcomingEventsOfCurrentMonth) {
        this.noUpcomingEventsOfCurrentMonth = noUpcomingEventsOfCurrentMonth;
    }

    public int getNoMissedEventsOfCurrentMonth() {
        return noMissedEventsOfCurrentMonth;
    }

    public void setNoMissedEventsOfCurrentMonth(int noMissedEventsOfCurrentMonth) {
        this.noMissedEventsOfCurrentMonth = noMissedEventsOfCurrentMonth;
    }

    /*
    public List<VaccineReportsCard> getVaccineReportsCards() {
        return vaccineReportsCards;
    }

    public void setVaccineReportsCards(List<VaccineReportsCard> vaccineReportsCards) {
        this.vaccineReportsCards = vaccineReportsCards;
    }
    */


    public VaccineReportsCard getVaccineReportsCard() {
        return vaccineReportsCard;
    }

    public void setVaccineReportsCard(VaccineReportsCard vaccineReportsCard) {
        this.vaccineReportsCard = vaccineReportsCard;
    }

    public int getCoveragePercentage() {
        return coveragePercentage;
    }

    public void setCoveragePercentage(int coveragePercentage) {
        this.coveragePercentage = coveragePercentage;
    }

   /*
    public int getDropOutPercentage() {
        return dropOutPercentage;
    }

    public void setDropOutPercentage(int dropOutPercentage) {
        this.dropOutPercentage = dropOutPercentage;
    }
    */


    public int getMissedVaccinations() {
        return missedVaccinations;
    }

    public void setMissedVaccinations(int missedVaccinations) {
        this.missedVaccinations = missedVaccinations;
    }

    public int getUpcomingVaccinations() {
        return upcomingVaccinations;
    }

    public void setUpcomingVaccinations(int upcomingVaccinations) {
        this.upcomingVaccinations = upcomingVaccinations;
    }

    public int getTotalImmunized() {
        return totalImmunized;
    }

    public void setTotalImmunized(int totalImmunized) {
        this.totalImmunized = totalImmunized;
    }

    public String getNextHospitalDate() {
        return nextHospitalDate;
    }

    public void setNextHospitalDate(String nextHospitalDate) {
        this.nextHospitalDate = nextHospitalDate;
    }

    public int getDoneVaccinations() {
        return doneVaccinations;
    }

    public void setDoneVaccinations(int doneVaccinations) {
        this.doneVaccinations = doneVaccinations;
    }

    /*
    public List<VaccineReport> getVaccineReportList() {
        return vaccineReportList;
    }

    public void setVaccineReportList(List<VaccineReport> vaccineReportList) {
        this.vaccineReportList = vaccineReportList;
    }
    */
}
