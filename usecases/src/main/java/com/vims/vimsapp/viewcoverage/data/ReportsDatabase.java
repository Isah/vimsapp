package com.vims.vimsapp.viewcoverage.data;

import android.app.Application;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.util.Log;


import com.vims.vimsapp.viewcoverage.CoverageReport;
import com.vims.vimsapp.viewcoverage.CoverageTimeLine;
import com.vims.vimsapp.viewcoverage.VaccineReportsCard;
import com.vims.vimsapp.utilities.DateConverter;
import com.vims.vimsapp.utilities.ListTypeConverter;
import com.vims.vimsapp.utilities.StatusEnumConverter;
import com.vims.vimsapp.utilities.VaccinationCardConverter;

/**
 * Created by root on 6/3/18.
 */


// List of the entry classes and associated TypeConverters
// List of the entry classes and associated TypeConverters MyTypeConverter.class,
@Database(entities = {CoverageReport.class, VaccineReportsCard.class, CoverageTimeLine.class}, version = 1, exportSchema = false)
@TypeConverters({DateConverter.class, StatusEnumConverter.class, ListTypeConverter.class, VaccinationCardConverter.class})
public abstract class ReportsDatabase extends RoomDatabase{


    private static final String LOG_TAG = ReportsDatabase.class.getSimpleName();
    private static final String DATABASE_NAME = "reports";

    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static ReportsDatabase sInstance;
    //.allowMainThreadQueries()
    public static ReportsDatabase getInstance(Application application) {
        Log.d(LOG_TAG, "Getting the database");
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = Room.databaseBuilder(application,
                        ReportsDatabase.class, ReportsDatabase.DATABASE_NAME)

                        .fallbackToDestructiveMigration()
                        .build();
                Log.d(LOG_TAG, "Made new database");
            }
        }
        return sInstance;
    }

    // The associated DAOs for the database
    public abstract ReportsDao getReportsDao();



}
