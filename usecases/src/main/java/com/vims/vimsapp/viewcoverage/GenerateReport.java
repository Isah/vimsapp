package com.vims.vimsapp.viewcoverage;

import android.support.annotation.NonNull;
import android.util.Log;

import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.searchvaccinations.GenerateVaccinationStats;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.entities.reports.MonthTargetPopulation;
import com.vims.vimsapp.entities.vaccinations.VaccinationCard;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.reports.VaccinationEventsPack;
import com.vims.vimsapp.entities.vaccinations.VaccinationStatus;

import static com.vims.vimsapp.utilities.DateCalendarConverter.calculateDifferenceInDates;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class GenerateReport implements UseCase<GenerateReport.RequestValues,GenerateReport.ResponseValue>{


   // private VaccinationsLocalDataSource vaccinationsLocalDataSource;
    //private PatientsRepository vaccinationsRepository;
    private GenerateVaccinationStats generateVaccinationStats;
   // private ReportsRepository reportsRepository;



    private final int NO_MONTHS = 12;
    private final int NO_VACCINES = 16;


    public GenerateReport(GenerateVaccinationStats generateVaccinationStats) {
       // this.vaccinationsLocalDataSource = vaccinationsLocalDataSource;
        this.generateVaccinationStats = generateVaccinationStats;
      //  this.reportsRepository = reportsRepository;
    }


    @Override
    public void execute(RequestValues param, OutputBoundary<ResponseValue> delivery) {

      ReportType reportType =  param.getReportType();
      String monthYear = param.getYearOrMonthValue(); //January, 2018
      String vaccineType = param.getTypeOfVaccine();
      int year = param.getYear();
      boolean isRefresh = param.isRefresh();


        Log.d("GeneralReport","yearPeriod: "+monthYear);


      boolean isDataChanged = param.isDataChanged();

      if(reportType == ReportType.API){

          //coverage for each individual vaccine monthly - at home screen
         // generateMonthlyReport(monthYear,delivery);
          generateReportFromAPI(isRefresh,vaccineType,year,delivery);

      }
      else if(reportType == ReportType.VACCINE_COVERAGE_YEARLY){

          //coverage for each vaccine throught the year
          //feed in year period
          //generateYearlyReportForACertainVaccine(monthYear,delivery);

      }

      else if(reportType == ReportType.VACCINE_SPECIFIC_HOME){

          //coverage under stats specific
          //feed in year period
          Log.d("YearlyReportCard",""+monthYear);//January, 2019

          generateMonthlyVaccineSpecificReportHome(isDataChanged,vaccineType,year,delivery);

      }

      else if(reportType == ReportType.VACCINE_SPECIFIC_STATS){

          //coverage under stats specific
          //feed in year period
          Log.d("YearlyReportCard",""+monthYear);//January, 2019

          generateMonthlyVaccineSpecificReportStats(isDataChanged,vaccineType,year,delivery);

      }

      //YEARLY
      else{//yearly
          //coverage each month on a yearyly scale - global
          //monthyear for report is given as a new Date
         // String yearOfReport =  DateCalendarConverterForView.dateToStringGetYear(new Date());


         //s generateAnnaulReport(isDataChanged,monthYear,delivery);

      }

    }




    int vaccinexAxis = 1;

    private List<VaccinationEvent>
    getVaccinationEventsForVaccine(String vaccineName, List<VaccinationEvent> vaccinationEvents){

        ArrayList<VaccinationEvent> eventList = new ArrayList<>();

        for(VaccinationEvent vaccinationEvent: vaccinationEvents){

            if(vaccinationEvent.getVaccineName().equals(vaccineName)){

                eventList.add(vaccinationEvent);

            }

        }

        return eventList;
    }

    //HOME SCREEN /////////////////////////////////////////////////


    /*
    private int getTotalUpcomingVaccinations(List<VaccinationEvent> vaccinationEvents){ //all

        int total=0;

       // int pendingEvents = vaccinationEvents.size();
       // int totalExpectedEvents = TARGET_POPULATION * 16; //16 is the no of possible events for 1 child
       // int pending = totalExpectedEvents - doneEvents;

        for (VaccinationEvent mapEvent : vaccinationEvents) {

            if(mapEvent.getVaccinationStatus() == VaccinationStatus.NOT_DONE) {
                Date dateScheduled = new Date(mapEvent.getScheduledDate());
                boolean isEventOverDue = dateScheduled.after(DateCalendarConverter.dateWithTimeToDateWithoutTime(new Date()));

                if (isEventOverDue) {
                    //  overDueEvents.add(event);
                    total++;
                }
            }
        }

        return total;

    }

    private int getTotalMissedVaccinations(List<VaccinationEvent> vaccinationEvents){

        int total=0;
        for (VaccinationEvent mapEvent : vaccinationEvents) {

            if(mapEvent.getVaccinationStatus() == VaccinationStatus.NOT_DONE) {

            Date dateScheduled = new Date(mapEvent.getScheduledDate());
            boolean isEventOverDue =  dateScheduled.before(DateCalendarConverter.dateWithTimeToDateWithoutTime(new Date()));


                if(isEventOverDue) {
                    //  overDueEvents.add(event);
                    total++;
                }
            }

        }

        return total;

    }

   */


    ////////  FINDING NEXT SCHEDULED DATE ////////////////////////////////////////////////

    Date found;
    private DashBoardHeader generateDashBoard(List<VaccinationEvent> upcomingEvents,  int done, int upcoming, int missed,  int upcomingCurrentMonth, int missedCurrentMonth){
        //upcimgn events help to calc the next vac date

        Map<Long, Date> candidateHospitalDates;

        candidateHospitalDates = new LinkedHashMap<>(upcomingEvents.size());
        ////////// from today   /////////////

        //2. for each event date
        for (VaccinationEvent vEvent: upcomingEvents){


                Date scheduledDate = new Date(vEvent.getScheduledDate());

                //3. if the date is after today then its one of our candidates
                //
               // if(scheduledDate.after(new Date())  && vEvent.getVaccinationStatus() == VaccinationStatus.NOT_DONE) {
                    //Log.d("GeneralReportNext","NextDates: "+vEvent.getScheduledDate()+"status: "+vEvent.getVaccinationStatus()+" child: "+vEvent.getChildName());
                    //the calculator is found in DateCalendar Converter class
                    long diffDays = calculateDifferenceInDates(scheduledDate, new Date());
                    //4. store all the upcoming dates, using diffdays as key to store which ones are closest or farthest
                    candidateHospitalDates.put(diffDays, scheduledDate);



        }



       // int missed  = getTotalMissedVaccinations(vaccinationEvents);
      //  int upcoming = getTotalUpcomingVaccinations(vaccinationEvents);
        //after getting the candidate dates calculate the closest thus smallest diff


        DashBoardHeader dashBoardHeader = new DashBoardHeader();

        if(!candidateHospitalDates.isEmpty()){
            found =  findClosestDateFromCandidates(candidateHospitalDates);
            dashBoardHeader.setNextHospitalDate(found);
        }else{

            dashBoardHeader.setNextHospitalDate(found);
        }

        dashBoardHeader.setMissedVaccinations(missed);
        dashBoardHeader.setUpcomingVaccinations(upcoming);
        dashBoardHeader.setDoneVaccinations(done);

        dashBoardHeader.setUpcomingVaccinationsCurrentMonth(upcomingCurrentMonth);
        dashBoardHeader.setMissedVaccinationsCurrentMonth(missedCurrentMonth);



        return dashBoardHeader;

    }

    private Date findClosestDateFromCandidates(Map<Long, Date> candidateHospitalDates){

        //1 calculate the smallest key/ thus the closet

        Map.Entry<Long, Date> firstEntry = candidateHospitalDates.entrySet().iterator().next();

        long smallestKey = firstEntry.getKey();
        Date smallestKeyValue = firstEntry.getValue();

        for (Map.Entry<Long, Date> map : candidateHospitalDates.entrySet()) {
            long key = map.getKey();

            Log.d("CLoseDate",""+key+" date: "+map.getValue());

            if(key == smallestKey){
                smallestKey = key;
                smallestKeyValue = map.getValue();
            }else {
                if (key < smallestKey) {
                    smallestKey = key;
                    smallestKeyValue = map.getValue();
                }
            }

            Log.d("CLoseDate","found: "+key+" date: "+map.getValue());


        }

        return smallestKeyValue;

    }


    /*
    private void findNextHospitalDate(final String monthDate, final OutputBoundary<ResponseValue> delivery){

        int status = 0;

        vaccinationsRepository.getVaccinationEvents(status, new VaccinationsDataSource.LoadAllVaccinationEventsCallback() {
            @Override
            public void onVaccinationEventsLoaded(List<VaccinationEvent> vaccinationEvents) {

                Map<Long, Date> candidateHospitalDates;

                candidateHospitalDates = new LinkedHashMap<>(vaccinationEvents.size());
                //from today

                //2. for each event date
                for (VaccinationEvent mapEvent: vaccinationEvents){

                    Date scheduledDate = DateCalendarConverter.stringToDateWithoutTime(mapEvent.getScheduledDate());

                    //3. if the date is after today then its one of our candidates
                    if(scheduledDate.after(new Date()) && mapEvent.getVaccinationStatus() == VaccinationStatus.NOT_DONE) {

                       // Log.d("GeneralReportNext","NextDates: "+vEvent.getScheduledDate()+"status: "+vEvent.getVaccinationStatus()+" child: "+vEvent.getChildName());
                        //the calculator is found in DateCalendar Converter class
                        long diffDays = calculateDifferenceInDates(scheduledDate, new Date());

                        //4. store all the upcoming dates, using diffdays as key to store which ones are closest or farthest
                        candidateHospitalDates.put(diffDays, scheduledDate);


                    }
                }


                int missed  = getTotalMissedVaccinations(vaccinationEvents);
                int upcoming = getTotalUpcomingVaccinations(vaccinationEvents);
                //after getting the candidate dates calculate the closest thus smallest diff
                Date found =  findClosestDateFromCandidates(candidateHospitalDates);


                DashBoardHeader dashBoardHeader = new DashBoardHeader();
                dashBoardHeader.setNextHospitalDate(found);
                dashBoardHeader.setMissedVaccinations(missed);
                dashBoardHeader.setUpcomingVaccinations(upcoming);

                ResponseValue responseValue = new ResponseValue(null);
                delivery.onSuccess(responseValue);

            }

            @Override
            public void onEventssNotAvailable() {
                delivery.onError("Events N/A");
            }
        });

    }
     */




    //////////// END OF NEXT SCHEDULED DATE FUNCTIONS /////////////////////////////////////////











    ////////////////// STATS / REPORTS SCREEN //////////////////////////////////////////////////////////////

    /*
    private void generateAnnaulReport(boolean isDataChanged, final String yearDate, final OutputBoundary<ResponseValue> delivery){


        int status = 1;  //done events

        vaccinationsRepository.getVaccinationEvents(status, new VaccinationsDataSource.LoadAllVaccinationEventsCallback() {
            @Override
            public void onVaccinationEventsLoaded(List<VaccinationEvent> vaccinationEvents) {
//                Log.d("SizeDataGR", "Cache "+vaccinationEvents.get(0).getVaccineName());
                List<VaccineReport> vaccineReports = new ArrayList<>();

                //convert the provided year date to one without time then get the year of the date
                //the year is used to search only events of that specific year
                Date dateNoTime = DateCalendarConverter.stringToDateWithoutTime(yearDate);  // January 27, 2019
              //  String yearPeriod =  DateCalendarConverter.dateToStringGetYear(dateNoTime); //2019
                int yearPeriod = 2018;


                //THE COVERAGE
                int coverageTotal = 0;
                int immunisedTotal = 0;
                int  inplanTotal = 0;



                for(int month=0; month<12; month++){
                    Log.d("MonthIndex","mi: "+month);
                    //VaccineReport monthReport = getVaccineReport_Month(month,yearPeriod,vaccinationEvents);
                    //vaccineReports.add(monthReport);

                    //coverageTotal += monthReport.getCoveragePercentage();
                    //immunisedTotal += monthReport.getNoOfImmunizedChildren();
                }


                CoverageReport coverageReport1 = new CoverageReport();
                coverageReport1.setCoverageReportName("Report 1");

                int coverage =(int) (((double) coverageTotal / (double) NO_MONTHS));
               // int dropout = dropOutRate(yearPeriod, vaccinationEvents);

              //  int missed  = getTotalMissedVaccinations(vaccinationEvents);
              //  int upcoming = getTotalUpcomingVaccinations(vaccinationEvents);

                DashBoardHeader dashBoardHeader = generateDashBoard(vaccinationEvents,0);
               // Log.d("GRNextDatess","dates: "+dashBoardHeader.getNextHospitalDate());

                coverageReport1.setDashBoardHeader(dashBoardHeader);
                //Log.d("GRNextDateDH","dh: "+dashBoardHeader.getNextHospitalDate());

                coverageReport1.setCoveragePercentage(coverage);
                //coverageReport.setNextImmunisationDate(nextDateOfSchedule);
                coverageReport1.setTotalImmunized(immunisedTotal);
                //coverageReport1.setUpcomingVaccinations(upcoming);
                //coverageReport1.setMissedVaccinations(missed);
                //  coverageReport.setNextImmunisationDate(nextDate);
                coverageReport1.setTotalVaccinations(TARGET_POPULATION);
               //  coverageReport1.setDropOutPercentage(dropout);
                coverageReport1.setVaccineReportList(vaccineReports);


                ResponseValue responseValue = new ResponseValue(coverageReport1);
                delivery.onSuccess(responseValue);

            }

            @Override
            public void onEventssNotAvailable() {

                delivery.onError("error no reports");

            }
        });


    }
     */

    private void getNoChildrenFullyImmunised(List<VaccinationCard> cards){
        for(VaccinationCard card: cards) {
            /////////////
            for(VaccinationEvent v: card.getVaccinationEvents()){

                  if(v.getVaccinationStatus() == VaccinationStatus.NOT_DONE) {

                  break;
                  }

            }


        }

    }


    /** Gets the Vaccine Report for a single month
     *  Calculates the total immunised children for each vaccine for given month
     *  calculates the coverage for each vaccine for given month
     *  calculates the mean coverage of the vaccines for given month
     * */
    /*
    private VaccineReport getVaccineReport_Month(int month, int year,  List<VaccinationEvent>   vaccinationEvents){
        VaccineReport vaccineReport = new VaccineReport();
        //January, 2019
        //InPlan means total target population
        //getInPlanVaccinationsForAMonth(month,year,vaccinationEvents);
        int immunisedTotal = 0;
        int pendingTotal = 0;
        int coverageTotal = 0;

        Collections.sort(vaccinationEvents);
        for(VaccineSpecification vspec: ReadVaccines.getInstance().getVaccineSpecificationList()){
            String vaccine = vspec.getVaccineName();
            //  int immunised = getChildrenImmunisedForVaccine_Month(true,vaccine,month,vaccinationEvents);
            // int pending = getChildrenPendingForVaccine_Month(vaccine,month,year,vaccinationEvents);//16S
            // method 2 to get pending
            // int pending = vaccinationEvents.size() - immunised;
            // int immunised = hash_getChildrenImmunisedForVaccine_Month(true,vaccine,month,year,vaccinationEvents);
            int coverage = (int) (((double) immunisedTotal / (double) TARGET_POPULATION * 100));
            coverageTotal += coverage;
            // immunisedTotal += immunised;
            // pendingTotal +=pending;
        }

        int coverageMean = (int) (((double) coverageTotal / (double) NO_VACCINES));
       // Log.d("GeneralReportImmunisedX","Immunised Total: "+immunisedTotal+"/"+TARGET_POPULATION+" Coverage"+coverageMean+" month"+month);
        vaccineReport.setCoveragePercentage(coverageMean);
        vaccineReport.setNoOfDelayedImmunisations(pendingTotal);
        vaccineReport.setNoOfImmunizedChildren(immunisedTotal);
        vaccineReport.setNoOfPlannedImmunisations(TARGET_POPULATION);
        //vaccineReport.setDropOutPercentage(dropout);
        //vaccineReport.setVaccineName(month);
        vaccineReport.setxAxisValue(++month);
        //this is to help creating the graph
        float[] a = new float[2];
        a[0] = immunisedTotal;
        a[1] = pendingTotal;
        vaccineReport.setCoverageImmunisedDelayed(a);
        return vaccineReport;
    }
    */


    private int getMonthInEvent(VaccinationEvent event){
        Date dateScheduled =new Date(event.getScheduledDate());
        Calendar midScheduleCalendar = Calendar.getInstance();
        midScheduleCalendar.setTime(dateScheduled);
        return midScheduleCalendar.get(Calendar.MONTH);
    }

    private int getYearInEvent(VaccinationEvent event){
        Date dateScheduled = new Date(event.getScheduledDate());
        Calendar midScheduleCalendar = Calendar.getInstance();
        midScheduleCalendar.setTime(dateScheduled);
        return midScheduleCalendar.get(Calendar.YEAR);
    }

    private int getChildrenImmunisedForVaccine_Month(boolean filterMonth,String vaccine, boolean useYearDate, int year, int month, List<VaccinationEvent> vaccinationEvents){


        //calculates based on the fact that no child can be vaccinated

        long startTime = System.currentTimeMillis();

        int childrenSize = 0;


        //done,month,vaccine
        for(VaccinationEvent vaccinationEvent: vaccinationEvents) {

          //  if (vaccinationEvent.getVaccinationStatus() == VaccinationStatus.DONE){

            int midScheduleMonth = getMonthInEvent(vaccinationEvent);
            int yearOfSchedule = getYearInEvent(vaccinationEvent);

            //The home graph is unpredictable it may require year progress or monthly progress
            if (filterMonth) {
                //&& yearOfSchedule == year
                boolean condition1;
                if(!useYearDate){
                    condition1 = (month == midScheduleMonth);
                }
                else{
                    condition1 = (month == midScheduleMonth && yearOfSchedule == year);
                }



                if (condition1) {
                   // if (vaccine.equals(vaccinationEvent.getVaccineName())) {

                            // countList.add(vaccinationEvent);
                            childrenSize++;

                   // }
                }




            } else {

                  //  if (vaccine.equals(vaccinationEvent.getVaccineName())) {
                        //countList.add(vaccinationEvent);
                        childrenSize++;
                  //  }


            }

        //  }

        }


        long endTime = System.currentTimeMillis();
        long ellapsedTime = startTime - endTime;


        return  childrenSize;

    }



    public int runBinarySearchIteratively(int[] sortedArray, int key, int low, int high) {
        int index = Integer.MAX_VALUE;

        while (low <= high) {
            int mid = (low + high) / 2;

            if (sortedArray[mid] < key) {
                low = mid + 1;
            } else if (sortedArray[mid] > key) {
                high = mid - 1;
            } else if (sortedArray[mid] == key) {
                index = mid;
                break;
            }
        }
        return index;
    }



    /* if given jan month, check vac event is in same month from the scheduled mid date
    *  then check its status
    * */

    public List<VaccinationEvent> runBinarySearch(VaccinationStatus vaccinationStatus, List<VaccinationEvent> vaccinationEvents, int keySearchMonth, String year) {
        int index = Integer.MAX_VALUE;
        ArrayList<VaccinationEvent> events = new ArrayList<>();

        int low = 0;//vaccinationEvents.indexOf(vevent);
        int high = vaccinationEvents.size()-1;

        for(VaccinationEvent vaccinationEvent: vaccinationEvents){



        }

        Log.d("GeneralReportLOW ","LOW "+low+" high"+high);


        //0 - 15
        while (low <= high) {
            int mid = (low + high) / 2; //143

            //int dropout = (int) (((double) pending / (double) inPlan * 100));

            Log.d("GeneralReportMID2","MID "+mid+" high"+high);

            VaccinationEvent vevent = vaccinationEvents.get(mid);

            Log.d("GeneralReportMID2","MID DATE"+mid+vevent.getScheduledDate());

            //int midIndex = vaccinationEvents.indexOf(vevent);

            //January, 2019
            Date midDateScheduled = new Date(vevent.getScheduledDate());

            //mid month
            Calendar midScheduleCalendar = Calendar.getInstance();
            midScheduleCalendar.setTime(midDateScheduled);
            int midScheduleMonth = midScheduleCalendar.get(Calendar.MONTH);



            Log.d("GeneralReportMonthss","keyMonthSearch: "+keySearchMonth+" MidScheduleMonth: "+midScheduleMonth);
            Log.d("GeneralReportStatus","given status "+vaccinationStatus+" sch status: "+vevent.getVaccinationStatus());

            if (midScheduleMonth < keySearchMonth) {
                low = mid + 1;

            } else if (midScheduleMonth > keySearchMonth) {

                high = mid - 1;

            } else if (midScheduleMonth == keySearchMonth) {

                Log.d("GeneralReportFound","EqualDates:"+vevent.getScheduledDate());


               // if(vevent.getVaccinationStatus() == vaccinationStatus) {
                    index = mid;

                    VaccinationEvent  event = vaccinationEvents.get(index);
                    events.add(event);
                    vaccinationEvents.remove(index);

                    Log.d("GeneralReportFound","FOUND:"+event.getScheduledDate());


               // }
              //  break;
            }


            Log.d("GeneralReport","anaylis: low:"+low+" high"+high);

        }

        return events;

    }



    int yearlyxAxis = 1;

    //YEARLY REPORT SHOWS ALL 12 MONTHS
    private int getInPlanVaccinationsForAMonth(int month, String year, List<VaccinationEvent> vaccinationEvents){

        ArrayList<VaccinationEvent> countList = new ArrayList<>();

        Collections.sort(countList);

        long startTime = System.currentTimeMillis();

        VaccinationEvent vevent = vaccinationEvents.get(0);
        int low = vaccinationEvents.indexOf(vevent);
        int high = vaccinationEvents.size()-1;

        int sizeInplan = runBinarySearch(null,vaccinationEvents,month,year).size();


        for(VaccinationEvent vaccinationEvent: vaccinationEvents){
            Date dateScheduled = new Date(vaccinationEvent.getScheduledDate());
            String vDate = DateCalendarConverter.dateToStringWithoutTimeAndDay(dateScheduled);
            // if(vaccinationEvent.getVaccineName().equals(vaccineName) && searchDate.equals(vDate)){

            Calendar midScheduleCalendar = Calendar.getInstance();
            midScheduleCalendar.setTime(dateScheduled);
            int midScheduleMonth = midScheduleCalendar.get(Calendar.MONTH);

            if(month == midScheduleMonth && vaccinationEvent.getVaccinationStatus() == VaccinationStatus.DONE){
                countList.add(vaccinationEvent);
            }
        }




        long endTime = System.currentTimeMillis();
        long ellapsedTime = startTime - endTime;

        Log.d("GeneralReportTime","EllapsedTimeNew: "+ellapsedTime);


        return countList.size();//countList.size()
    }




    //old one
    /*
    private void generateYearlyReportForACertainVaccine(final String yearPeriod,final OutputBoundary<ResponseValue> delivery){

        vaccinationsRepository.getAllChildVaccinationCardsByDate(new Date(), new VaccinationsDataSource.LoadAllVaccinationCardsCallback() {
            @Override
            public void onCardsLoaded(List<VaccinationCard> vaccinationCards) {

                ArrayList<VaccinationEvent> vaccinationEvents = new ArrayList<>();
                List<VaccineReport> vaccineReports = new ArrayList<>();
                ArrayList<VaccineReportsCard> vaccineReportsCards = new ArrayList<>();//for each vaccine per month yearly

                for(VaccinationCard vaccinationCard: vaccinationCards){
                    vaccinationEvents.addAll(vaccinationCard.getVaccinationEvents());
                }

                String[] year = yearPeriod.split(" ");


                List<VaccineSpecification> vaccineSpecifications = ReadVaccines.getInstance().getVaccineSpecificationList();

                for(VaccineSpecification vspec: vaccineSpecifications){

                    VaccineReportsCard vaccineReportsCardVaccine =  getVaccineReportCard_Vaccine_Month(vspec.getVaccineName(),yearPeriod,vaccinationEvents);
                    vaccineReportsCards.add(vaccineReportsCardVaccine);

                }


                CoverageReport coverageReport = new CoverageReport();
                coverageReport.setCoverageReportName("Report 1");

                int yearlyStats[] = getCoverageStatistics(vaccineReports);
                int coverage = yearlyStats[0];
                int immunized = yearlyStats[1];
                int inplan = yearlyStats[2];


                coverageReport.setCoveragePercentage(coverage);
                coverageReport.setTotalImmunized(immunized);
                coverageReport.setTotalVaccinations(inplan);
                coverageReport.setVaccineReportsCards(vaccineReportsCards);


                ResponseValue responseValue = new ResponseValue(coverageReport);
                delivery.onSuccess(responseValue);


            }

            @Override
            public void onCardsNotAvailable() {

                delivery.onError("No Reports Available");
            }
        });

    }
    */



    /*
   private int getOverallVaccineCoverage(int yearPeriod, List<VaccinationEvent> vaccinationEvents){
       //THE COVERAGE
       int coverageTotal = 0;
       for(int month=0; month<12; month++){
           VaccineReport monthReport = getVaccineReport_Month(month,yearPeriod,vaccinationEvents);
           coverageTotal += monthReport.getCoveragePercentage();
       }
       int coverage;
       coverage = (int) (((double) coverageTotal / (double) NO_MONTHS));
       return coverage;
   }
   */







    /*
    private List<CoverageTimeLine> getCoverageTimeLines(String vaccineName,List<VaccinationEvent> vaccinationEvents, MonthTargetPopulation monthTargetPopulation) {
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        int nextYear = currentYear +1;
        int bottomYear = currentYear - 4;
        List<CoverageTimeLine> coverageTimeLines = new ArrayList<>();
        //Get coverage for all the past years
        for(int year=bottomYear; year<nextYear; year++){
            CoverageTimeLine coverageTimeLine = new CoverageTimeLine();
            VaccineReportsCard vaccineReportsCard = getVaccineReportCard_Vaccine_Month(vaccineName,monthTargetPopulation,);
            coverageTimeLine.setCoverage(vaccineReportsCard.getCoveragePercentage());
            coverageTimeLine.setVaccine(vaccineName);
            coverageTimeLine.setYear(year);
            Log.d("TimeLines x:","c: "+coverageTimeLine.getCoverage()+" y:"+coverageTimeLine.getYear());
            coverageTimeLines.add(coverageTimeLine);
        }
        return  coverageTimeLines;
    }
    */




    private int getOverallCoverage(List<CoverageTimeLine> coverageTimeLines, int year){

        int coverage = 0;

        for(CoverageTimeLine cov: coverageTimeLines){

            if(cov.getYear() == year){
              coverage = cov.getCoverage();
            }

        }

   return coverage;

    }

    // used for deciding whether to use the cache or not, home screen doesnt use the cache
    // probably it will have its own cache implementation

    public   final int HOME_SCREEN = 0;
    public   final int STATS_SCREEN = 1;

    private void generateMonthlyVaccineSpecificReportHome(boolean isDataChanged, final String vaccineName, final int yearDate,final OutputBoundary<ResponseValue> delivery){

          GenerateVaccinationStats.RequestValues requestValues = new GenerateVaccinationStats.RequestValues(vaccineName,HOME_SCREEN,yearDate);
          requestValues.setDataChanged(isDataChanged);

           generateVaccinationStats.execute(requestValues, new OutputBoundary<GenerateVaccinationStats.ResponseValue>() {
               @Override
               public void onSuccess(GenerateVaccinationStats.ResponseValue result) {

                   VaccinationEventsPack eventsPack = result.getVaccinationStats();

                   /*
                   final List<VaccinationEvent> vaccinationEventsDoneOfYear = eventsPack.getDoneEventsOfYear();
                   final List<VaccinationEvent> vaccinationEventsNotDone = eventsPack.getNotDone();
                   final List<VaccinationEvent> vaccinationEvents = eventsPack.getAllEvents();
                   */

                   List<VaccinationEvent> eventsDoneOfYearAllVaccines = eventsPack.getDoneAllVaccinesEventsOfYear();
                   List<VaccinationEvent> eventsUpcoming = eventsPack.getUpComingEventsOfyear();


                   int upcoming = eventsPack.getNoUpcomingEventsOfyear();
                   int missed = eventsPack.getNoMissedEventsOfyear();

                   int upcomingCurrentMonth = eventsPack.getNoUpcomingEventsOfCurrentMonth();
                   int missedCurrentMonth = eventsPack.getNoMissedEventsOfCurrentMonth();

                   if(eventsUpcoming!=null &&  !eventsUpcoming.isEmpty() ) {

                       // List<CoverageTimeLine> coverageTimeLines = getCoverageTimeLines(vaccineName,vaccinationEventsDone);
                       // VaccineReportsCard vaccineReportsCard = getVaccineReportCard_Vaccine_Month(vaccineName,false,yearDate,vaccinationEventsDoneOfYear,monthTarget);
                       DashBoardHeader dashBoardHeader = generateDashBoard(eventsUpcoming, eventsDoneOfYearAllVaccines.size(), upcoming, missed, upcomingCurrentMonth, missedCurrentMonth);
                       // int overall = getOverallVaccineCoverage(yearDate,vaccinationEvents);
                       CoverageReport coverageReport = new CoverageReport();
                       coverageReport.setUpcomingVaccinations(dashBoardHeader.getUpcomingVaccinations());
                       coverageReport.setMissedVaccinations(dashBoardHeader.getMissedVaccinations());
                       coverageReport.setNoUpcomingEventsOfCurrentMonth(dashBoardHeader.getUpcomingVaccinationsCurrentMonth());
                       coverageReport.setNoMissedEventsOfCurrentMonth(dashBoardHeader.getMissedVaccinationsCurrentMonth());
                       coverageReport.setDoneVaccinations(dashBoardHeader.getDoneVaccinations());
                       coverageReport.setNextHospitalDate(DateCalendarConverter.dateToStringWithoutTime(dashBoardHeader.getNextHospitalDate()));
                       coverageReport.setYearOfReport(yearDate);

                       ResponseValue responseValue = new ResponseValue(coverageReport);
                       delivery.onSuccess(responseValue);
                   }else{


                       delivery.onError("No Upcoming Events This year!");
                   }

               }

               @Override
               public void onError(String errorMessage) {
                   delivery.onError("No Reports Data E1");

               }
           });


    }


    private void generateMonthlyVaccineSpecificReportStats(boolean isDataChanged, final String vaccineName, final int yearDate,final OutputBoundary<ResponseValue> delivery){

        GenerateVaccinationStats.RequestValues requestValues = new GenerateVaccinationStats.RequestValues(vaccineName,STATS_SCREEN,yearDate);


        generateVaccinationStats.execute(requestValues, new OutputBoundary<GenerateVaccinationStats.ResponseValue>() {
            @Override
            public void onSuccess(GenerateVaccinationStats.ResponseValue result) {

                VaccinationEventsPack eventsPack = result.getVaccinationStats();


                List<CoverageTimeLine> coverageTimeLines = eventsPack.getCoverageTimeLines();
                final Map<Integer,Integer> childrenImmunised = eventsPack.getImmunisedCHildren();
                MonthTargetPopulation monthTargetPopulation = eventsPack.getMonthTargetPopulation();


                VaccineReportsCard vaccineReportsCard1 = getVaccineReportCard_Vaccine_Month(vaccineName,monthTargetPopulation,childrenImmunised);
                vaccineReportsCard1.setCoverageTimeLines(coverageTimeLines);


                //Set the coverage
                //These are reports for a vaccine
                //THE COVERAGE
                int overall = getOverallCoverage(coverageTimeLines,yearDate);


                //int missed = getTotalMissedVaccinations(vaccinationEvents);
                //Date nextDateOfSchedule = findNextHospitalDate(vaccinationEvents);
                //int missed  = getTotalMissedVaccinations(vaccinationEvents);
                //int upcoming = getTotalUpcomingVaccinations(vaccinationEvents);

                //DashBoardHeader dashBoardHeader = generateDashBoard(vaccinationEvents);
                //int overall = getOverallVaccineCoverage(yearDate,vaccinationEvents);
                CoverageReport coverageReport = new CoverageReport();
                //coverageReport.setNextImmunisationDate(nextDateOfSchedule);

                coverageReport.setCoveragePercentage(overall);
                coverageReport.setVaccineReportsCard(vaccineReportsCard1);
                coverageReport.setTotalMissedImmunisation(eventsPack.getNoChildrenMissedImmunisation());

                ResponseValue responseValue = new ResponseValue(coverageReport);
                delivery.onSuccess(responseValue);

            }

            @Override
            public void onError(String errorMessage) {
                delivery.onError("No Reports Data E1");

            }
        });


    }


    private void generateReportFromAPI(boolean isRefresh, final String vaccineName, final int yearDate,final OutputBoundary<ResponseValue> delivery) {

        //coverageReport
        /*
        reportsRepository.getCoverageReport(isRefresh, vaccineName, yearDate, new ReportsDataSource.GetReportsCallback() {
            @Override
            public void onReportsLoaded(CoverageReport coverageReport) {


             delivery.onSuccess(new ResponseValue(coverageReport));

            }

            @Override
            public void onFailed(String msg) {

                delivery.onError(msg);

            }
        });

        */

    }


    ////////// DROPOUT ANALYSIS //////////////////////////////////////////////////////////////
    /*
    private int dropOutRate(String year, List<VaccinationEvent> vaccinationEvents){
        int dropoutRate;
        //OPV0,OPV1,OPV2,OPV3,BCG,Hib1,Hib2,Hib3,PCV1,PCV2,PCV3,Rota1,Rota2,Rota3,IPV,Measles
        int opv0Immunised = getImmunisedRateForASpecificVaccine("OPV0",year,vaccinationEvents);
        int opv3Immunised = getImmunisedRateForASpecificVaccine("OPV3",year,vaccinationEvents);

        int hib1Immunised = getImmunisedRateForASpecificVaccine("Hib1",year,vaccinationEvents);
        int hib3Immunised = getImmunisedRateForASpecificVaccine("Hib3",year,vaccinationEvents);

        int pcv1Immunised = getImmunisedRateForASpecificVaccine("PCV1",year,vaccinationEvents);
        int pcv3Immunised = getImmunisedRateForASpecificVaccine("PCV3",year,vaccinationEvents);

        int rota1Immunised = getImmunisedRateForASpecificVaccine("Rota1",year,vaccinationEvents);
        int rota3Immunised = getImmunisedRateForASpecificVaccine("Rota3",year,vaccinationEvents);

        int ipvImmunised = getImmunisedRateForASpecificVaccine("IPV",year,vaccinationEvents);
        int measlesImmunised = getImmunisedRateForASpecificVaccine("Measles",year,vaccinationEvents);
        int bcgImmunised = getImmunisedRateForASpecificVaccine("BCG",year,vaccinationEvents);

        int ipvDropout = 0; //ipvDropOut - ipvDropOut;
        int bcgDropout = 0; //bcgDropOut - bcgDropOut;
        int measlesDropout = 0; //measlesDropOut - measlesDropOut;

        int totalDropout = opvDropOut + hibDropOut + pcvDropOut + rotaDropOut;
        int totalImmunised = opv0Immunised+opv3Immunised+hib1Immunised+hib3Immunised+pcv1Immunised+pcv3Immunised+rota1Immunised+rota3Immunised;


        dropoutRate = (int) (((double) totalDropout / (double) totalImmunised * 100));

        Log.d("ReportDropOut", "drop "+dropoutRate);


        return dropoutRate;
    }
    */


    private int getImmunisedRateForASpecificVaccine(String vaccine, int year, List<VaccinationEvent> vaccinationEvents){

        /*
        int dropoutAllyear = 0;

        VaccineReportsCard vaccineReportsCard = getVaccineReportCard_Vaccine_Month(vaccine,false,year,vaccinationEvents);

        for(VaccineReport vaccineReport: vaccineReportsCard.getVaccineReports()){
         dropoutAllyear += vaccineReport.getNoOfImmunizedChildren();
        }

        return dropoutAllyear;
        */
        return 0;
    }

    ////////////////END OF DROPOUT ANALYSIS ///////////////////////////////////





    /////////////////////    REAL REPORT CALCULATION METHODS //////////////////////////////////

     private int getTarget(MonthTargetPopulation monthTargetPopulation, int month){
         int target = 0;
         if(month==0){
             target =  monthTargetPopulation.getJan();
         }
         else if(month==1){
             target =  monthTargetPopulation.getFeb();
         }
         else if(month==2){
             target =  monthTargetPopulation.getMar();
         }
         else if(month==3){
             target =  monthTargetPopulation.getApr();
         }else if(month==4){
             target =  monthTargetPopulation.getMay();
         }
         else if(month==5){
             target =  monthTargetPopulation.getJun();
         }
         else if(month==6){
             target =  monthTargetPopulation.getJul();
         }
         else if(month==7){
             target =  monthTargetPopulation.getAug();
         }
         else if(month==8){
             target =  monthTargetPopulation.getSep();
         }
         else if(month==9){
             target =  monthTargetPopulation.getOct();
         }
         else if(month==10){
             target =  monthTargetPopulation.getNov();
         }
         else if(month==11){
             target =  monthTargetPopulation.getDec();
         }
        return target;
     }


     private VaccineReportsCard getVaccineReportCard_Vaccine_Month(String vaccine, MonthTargetPopulation monthTargetPopulation, Map<Integer,Integer> childrenImmunised){

        List<VaccineReport> vaccineReports = new ArrayList<>();

        int xaxis = 1;
        int immunized = 0;
        int coverageTotal = 0;
        int targetTotal = 0;



           for(int month=0; month<12; month++){
                int target = getTarget(monthTargetPopulation,month);
                targetTotal += target;

               int immunisedInMonth = childrenImmunised.get(month);

                VaccineReport monthReport = getVaccineReport_Vaccine_Month(vaccine,xaxis,target,immunisedInMonth);
                vaccineReports.add(monthReport);
                xaxis++;
                immunized += immunisedInMonth;//monthReport.getNoOfImmunizedChildren();
                coverageTotal += monthReport.getCoveragePercentage();

               // Get Difference in months to calculate the rise or drop in coverage
               // Get target this month

               int thisMonth = DateCalendarConverter.dateToIntGetMonth(new Date());
               if(thisMonth == month){
                 //  coverageMonth1 = monthReport.getCoveragePercentage();
                   //get the report of previous month =
                   int indexOfPrevMonth = 0;
                   if(thisMonth!=0){
                     indexOfPrevMonth =   thisMonth - 1;
                   }
                  // VaccineReport vReport = vaccineReports.get(indexOfPrevMonth);
                  // coverageMonth2 = vReport.getCoveragePercentage();
               }

           }

         //  int riseOrDrop = coverageMonth1 - coverageMonth2;


            VaccineReportsCard reportsCard = new VaccineReportsCard();
            reportsCard.setVaccineName(vaccine);
            reportsCard.setVaccineReports(vaccineReports);


            int coverage = (int) (((double) coverageTotal / (double) NO_MONTHS));

           // int vaccineCoverage = (int) (((double) immunized / (double) TARGET_POPULATION) * 100);


            reportsCard.setNoImmunized(immunized);
            reportsCard.setNoInplan(targetTotal);
            reportsCard.setCoveragePercentage(coverage);

            return reportsCard;


        }

    private VaccineReport getVaccineReport_Vaccine_Month(String vaccine,int xAxisValue, int monthTargetPopulation, int immunised){

        VaccineReport vaccineReport = new VaccineReport();

        // InPlan means total target population
        // int inPlan = getInPlanVaccinationOfVaccineForMonth(true,vaccine,monthDate,vaccinationEvents);
        // int immunized = getNoOfVaccinationsAdministeredOfVaccineForAMonth(true,vaccine,monthDate,vaccinationEvents);
        // int missed = getNoDelayedVaccinationOfVaccineForAMonth(true,vacine,monthDate,vaccinationEvents);

        //you cannot be vaccinated by e.g BCG in a month twice so each event/dose means a child

        //int immunised = getChildrenImmunisedForVaccine_Month(filterMonth,vaccine,useYearDate,year,month,eventsDoneOfYr);

        //int immunised = 0;

        int coverage = (int) (((double) immunised / (double) monthTargetPopulation * 100));
        int pending = monthTargetPopulation - immunised;

        vaccineReport.setCoveragePercentage(coverage);
        vaccineReport.setNoOfDelayedImmunisations(pending);
        vaccineReport.setNoOfImmunizedChildren(immunised);
        vaccineReport.setNoOfPlannedImmunisations(monthTargetPopulation);
        vaccineReport.setVaccineName(vaccine);
        vaccineReport.setMonthTarget(monthTargetPopulation);
        vaccineReport.setxAxisValue(xAxisValue);

        //This is to help creating the graph
        float[] a = new float[2];
        a[0] = immunised;
        a[1] = pending;
        vaccineReport.setCoverageImmunisedDelayed(a);
        return vaccineReport;

    }

    /////////////////////    REAL REPORT CALCULATION METHODS //////////////////////////////////






    public enum ReportType{
        API,YEARLY,VACCINE_COVERAGE_YEARLY,VACCINE_SPECIFIC_HOME,VACCINE_SPECIFIC_STATS,NEX_DATE
    }


    //it implements such that its of a right type to be accepted in the execute method
    public static final class RequestValues implements UseCase.RequestValues {

        private final ReportType reportType;
        private final String yearOrMonthValue;
        private String typeOfVaccine;
        private boolean isDataChanged;
        private boolean isRefresh;
        private int year;



        public RequestValues(ReportType reportType, String yearOrMonthValue) {
            this.reportType = reportType;
            this.yearOrMonthValue = yearOrMonthValue;
        }


        public boolean isRefresh() {
            return isRefresh;
        }

        public void setRefresh(boolean refresh) {
            isRefresh = refresh;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public boolean isDataChanged() {
            return isDataChanged;
        }

        public void setDataChanged(boolean dataChanged) {
            isDataChanged = dataChanged;
        }

        public ReportType getReportType() {
            return reportType;
        }

        public String getYearOrMonthValue() {
            return yearOrMonthValue;
        }

        public String getTypeOfVaccine() {
            return typeOfVaccine;
        }

        public void setTypeOfVaccine(String typeOfVaccine) {
            this.typeOfVaccine = typeOfVaccine;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private CoverageReport coverageReport;

        public ResponseValue(@NonNull CoverageReport report) {
           this.coverageReport = report;
        }

        public CoverageReport getCoverageReport() {
            return coverageReport;
        }
    }





}
