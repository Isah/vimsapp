package com.vims.vimsapp.viewcoverage.data;

/**
 * Created by root on 6/24/18.
 */


import com.vims.vimsapp.viewcoverage.CoverageReport;

/**
 * Main entry point for accessing tasks data.
 * <p>
 * For simplicity, only getTasks() and getTask() have callbacks. Consider adding callbacks to other
 * methods to inform the user of network/database errors or successful operations.
 * For example, when a new task is created, it's synchronously stored in cache but usually every
 * operation on database or network should be executed in a different thread.
 */

public interface ReportsDataSource {


    //Reports


    ////// Reports
    interface GetReportsCallback {

        void onReportsLoaded(CoverageReport coverageReport);

        void onFailed(String msg);
    }

    interface SaveReportsCallback {

        void onReportsSaved(CoverageReport coverageReport);

        void onFailed(String msg);
    }



    void  getCoverageReport(boolean isNetOn, String vaccine, int yearDate, GetReportsCallback reportsCallback);
    void  saveCoverageReport(CoverageReport coverageReport,SaveReportsCallback reportsCallback);

    void  refreshReports();

}
