package com.vims.vimsapp.viewcoverage;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


import java.util.List;
import java.util.UUID;

import static android.arch.persistence.room.ForeignKey.CASCADE;


@Entity(tableName = "vaccine_report_card",indices = {@Index(value = {"reportId"})}, foreignKeys = @ForeignKey(entity = CoverageReport.class,
        parentColumns = "reportId",
        childColumns = "reportId",
        onDelete = CASCADE))
public class VaccineReportsCard implements Comparable<VaccineReportsCard>{


    @PrimaryKey //(autoGenerate = true)
    @NonNull
    private String reportCardId;
    private String reportId;

    private String vaccineName;
    private int coveragePercentage;

  //  private float[] coverageImmunisedDelayed;
  //  private int xAxisValue;

    private int noImmunized;
    private int noInplan;

    private List<VaccineReport> vaccineReports;
    private List<CoverageTimeLine> coverageTimeLines;




    @Ignore
    public VaccineReportsCard() {
        setReportCardId(UUID.randomUUID().toString());
    }


    public VaccineReportsCard(@NonNull String reportCardId, String reportId, String vaccineName, int coveragePercentage, int noImmunized, int noInplan, List<VaccineReport> vaccineReports, List<CoverageTimeLine> coverageTimeLines) {
        this.reportCardId = reportCardId;
        this.reportId = reportId;
        this.vaccineName = vaccineName;
        this.coveragePercentage = coveragePercentage;
        // this.coverageImmunisedDelayed = coverageImmunisedDelayed;
        // this.xAxisValue = xAxisValue;
        this.noImmunized = noImmunized;
        this.noInplan = noInplan;
        this.vaccineReports = vaccineReports;
        this.coverageTimeLines = coverageTimeLines;
    }


    public List<CoverageTimeLine> getCoverageTimeLines() {
        return coverageTimeLines;
    }

    public void setCoverageTimeLines(List<CoverageTimeLine> coverageTimeLines) {
        this.coverageTimeLines = coverageTimeLines;
    }

    @NonNull
    public String getReportCardId() {
        return reportCardId;
    }

    public void setReportCardId(@NonNull String reportCardId) {
        this.reportCardId = reportCardId;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public int getNoInplan() {
        return noInplan;
    }

    public void setNoInplan(int noInplan) {
        this.noInplan = noInplan;
    }

    public int getNoImmunized() {
        return noImmunized;
    }

    public void setNoImmunized(int noImmunized) {
        this.noImmunized = noImmunized;
    }

    public int getCoveragePercentage() {
        return coveragePercentage;
    }

    public void setCoveragePercentage(int coveragePercentage) {
        this.coveragePercentage = coveragePercentage;
    }


    /*
    public float[] getCoverageImmunisedDelayed() {
        return coverageImmunisedDelayed;
    }

    public void setCoverageImmunisedDelayed(float[] coverageImmunisedDelayed) {
        this.coverageImmunisedDelayed = coverageImmunisedDelayed;
    }

    public int getxAxisValue() {
        return xAxisValue;
    }

    public void setxAxisValue(int xAxisValue) {
        this.xAxisValue = xAxisValue;
    }
    */

    public String getVaccineName() {
        return vaccineName;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    public List<VaccineReport> getVaccineReports() {
        return vaccineReports;
    }

    public void setVaccineReports(List<VaccineReport> vaccineReports) {
        this.vaccineReports = vaccineReports;
    }


    @Override
    public int compareTo(@NonNull VaccineReportsCard comparestu) {

        int sd  = this.noImmunized;
        int cd = comparestu.noImmunized;

        //  int a = sd - cd;

        //   int dateCmp = this.scheduledDate.compareTo(comparestu.getScheduledDate());
        //  Log.i("==Albums==", "dateComp: " + dateCmp);

        return comparestu.noImmunized - this.noImmunized;


    }

}

