package com.vims.vimsapp.viewcoverage.data;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ReportsApiClient {


    //private static final String ENDPOINT = "https://api.github.com/";
   // private static final String ENDPOINT = "http://10.42.0.1:8080/vim/webapi/";

    private static final String ENDPOINT = "http://10.0.2.2:8080/";

    private static Retrofit retrofit;


    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {

            //to fake responses
                /*
                final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new FakeInterceptor())
                        .build();
                        */

            retrofit = new Retrofit.Builder()
                    .baseUrl(ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            // .client(okHttpClient)

        }
        return retrofit;

    }


}
