package com.vims.vimsapp.managehospitalschedule;



import android.util.Log;

import com.vims.vimsapp.entities.schedule.HospitalImmunisationEvent;
import com.vims.vimsapp.entities.schedule.WeekRepeat;
import com.vims.vimsapp.entities.schedule.WeekSchedule;
import com.vims.vimsapp.entities.schedule.HospitalSchedule;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.PreferenceHandler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

public class HospitalScheduleService {


    private static final GregorianCalendar getSecondMonday(int year, int month) {

        GregorianCalendar cal = new GregorianCalendar();
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);
        cal.setFirstDayOfWeek(Calendar.SUNDAY);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal.set(Calendar.WEEK_OF_MONTH,2);
        return cal;
    }

    private static final GregorianCalendar getWednesdays(int year, int week) {

        GregorianCalendar cal = new GregorianCalendar();
        cal.set(Calendar.YEAR, year);
        cal.setFirstDayOfWeek(Calendar.SUNDAY);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
        cal.set(Calendar.WEEK_OF_MONTH,week);
        return cal;
    }

    private static final GregorianCalendar getDays(int year,int dayOfweek, int repeatNoOfDayOfWeek, int month) {

        GregorianCalendar cal = new GregorianCalendar();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.setFirstDayOfWeek(Calendar.SUNDAY);
        cal.set(Calendar.DAY_OF_WEEK, dayOfweek);  //monday

        if(repeatNoOfDayOfWeek == 5){ //last week of month
            int maxDayInWeekNumber = cal.getActualMaximum(Calendar.DAY_OF_WEEK_IN_MONTH);//e.g get the max monday
            cal.set(Calendar.DAY_OF_WEEK_IN_MONTH,maxDayInWeekNumber);
            // int maxWeekNumber = cal.getActualMaximum(Calendar.WEEK_OF_MONTH);//e.g get the max monday
            // cal.set(Calendar.WEEK_OF_MONTH,maxWeekNumber);

        }else{
            //if repeat is 2/3
            cal.set(Calendar.DAY_OF_WEEK_IN_MONTH,repeatNoOfDayOfWeek); //e.g like 1st monday

        }


        return cal;
    }





    public static HospitalSchedule getDummyCreatedHospitalSchedule(String hospitalName, List<WeekSchedule> weekScheduleList){

        ArrayList<GregorianCalendar> gregorianCalendars = new ArrayList<>();

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int maxYearLimit = currentYear + 5;

        for(int i = currentYear;i< maxYearLimit; i++){

            for(int m = 0;m< 12; m++){
                //There may be many gregorian calendars
                //so check the list and add
                for(WeekSchedule weekSchedule: weekScheduleList){

                    for(WeekRepeat weekRepeat: weekSchedule.getWeekRepeats()) {
                        //get a certain day of week not just 2nd monday
                        int  repeatNoOfDayOfWeek = weekRepeat.getRepeatDay();

                        GregorianCalendar day = getDays(i, weekSchedule.getDayOfWeek(), repeatNoOfDayOfWeek, m);
                        gregorianCalendars.add(day);

                        //b4 global junior - theres a blue nile school
                    }

                }
            }

            }



        ArrayList<HospitalImmunisationEvent> hospitalEvents = new ArrayList<>();


        for(GregorianCalendar gc: gregorianCalendars){

            HospitalImmunisationEvent hospitalEvent = new HospitalImmunisationEvent();
            hospitalEvent.setHospitalId(UUID.randomUUID().toString());
            hospitalEvent.setEventId(UUID.randomUUID().toString());
            hospitalEvent.setEventTitle(hospitalName);
            hospitalEvent.setEventDate(DateCalendarConverter.dateToStringWithoutTime(gc.getTime()));
            hospitalEvents.add(hospitalEvent);

            Log.d("Schedule Date: ",hospitalEvent.getEventDate());

        }


        HospitalSchedule hospitalSchedule = new HospitalSchedule();
        hospitalSchedule.setWorkingDays(hospitalEvents);


        return hospitalSchedule;

    }


    public   HospitalSchedule getDummyHospitalSchedule(){

        ArrayList<GregorianCalendar> gregorianCalendars = new ArrayList<>();

        for(int i = 2018;i< 2022; i++){

            for(int w = 1;w< 6; w++){

                GregorianCalendar wednesday =  getWednesdays(i,w);
                gregorianCalendars.add(wednesday);

            }

            for(int m = 0;m< 12; m++){

                //There may be many gregorian calendars
                //so check the list and add
                GregorianCalendar monday = getSecondMonday(i,m);
                //get a certain day of week not just 2nd monday

                gregorianCalendars.add(monday);

            }

        }



        ArrayList<HospitalImmunisationEvent> hospitalEvents = new ArrayList<>();

        for(GregorianCalendar gc: gregorianCalendars){

            HospitalImmunisationEvent hospitalEvent = new  HospitalImmunisationEvent();
            hospitalEvent.setHospitalId(UUID.randomUUID().toString());
            hospitalEvent.setEventId(UUID.randomUUID().toString());
            hospitalEvent.setEventDate(DateCalendarConverter.dateToStringWithoutTime(gc.getTime()));
            hospitalEvents.add(hospitalEvent);

            Log.d("Schedule Date: ",hospitalEvent.getEventDate());

        }


        HospitalSchedule hospitalSchedule = new HospitalSchedule();
        hospitalSchedule.setWorkingDays(hospitalEvents);


       return hospitalSchedule;


    }



}
