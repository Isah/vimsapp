package com.vims.vimsapp.managehospitalschedule;

import com.vims.vimsapp.entities.schedule.Hospital;
import com.vims.vimsapp.entities.schedule.HospitalImmunisationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationCard;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.manageorders.VaccineOrder;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by root on 6/5/18.
 */

public interface ManageHospitalApi {


        @GET("api/vaccinations/schedule/{hosp}")
        Call<List<HospitalImmunisationEvent>> getRemoteHospitalSchedule(@Path("hospitalId") String hosp);



        /** vims officail api **/

        @GET("scheduleAPI/mobile/getDefaultEvents/{hospitalId}")
        Call<List<HospitalImmunisationEvent>> getVimsHospitalSchedule(@Header("Authorization") String authKey, @Path("hospitalId") String hosp);

        @GET("accountsAPI/mobile/getHospital/{hospitalId}")
        Call<Hospital> getVimsHospital(@Header("Authorization") String authKey, @Path("hospitalId") String hospitalId);

        /** end of vims officail api **/




}
