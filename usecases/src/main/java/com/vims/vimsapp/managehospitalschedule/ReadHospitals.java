package com.vims.vimsapp.managehospitalschedule;

import com.vims.vimsapp.entities.schedule.Hospital;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ReadHospitals {


    /*
    Kyeibanga HC II
    Beverly HC II
    KAKANJU HC III
    KISIIZI HC II
    */


    static List<Hospital> hospitals;

    public ReadHospitals() {

    }

    public static List<Hospital> getHardCodedHospitals(){

        hospitals = new ArrayList<>();

        Hospital hospital1 = new Hospital();
        hospital1.setDistrict("SCHEEMA");
        hospital1.setHospitalName("KYEIBANGA HC II");
        hospital1.setHospitalId(UUID.randomUUID().toString());


        Hospital hospital2 = new Hospital();
        hospital2.setDistrict("BUHWEJU");
        hospital2.setHospitalName("BEVERLY HC II");
        hospital2.setHospitalId(UUID.randomUUID().toString());


        Hospital hospital3 = new Hospital();
        hospital3.setDistrict("BUSHENYI");
        hospital3.setHospitalName("KAKANJU HC III");
        hospital3.setHospitalId(UUID.randomUUID().toString());


        Hospital hospital4 = new Hospital();
        hospital4.setDistrict("MITOOMA");
        hospital4.setHospitalName("KISIIZI HC II");
        hospital4.setHospitalId(UUID.randomUUID().toString());



        hospitals.add(hospital1);
        hospitals.add(hospital2);
        hospitals.add(hospital3);
        hospitals.add(hospital4);

        return hospitals;


    }


}
