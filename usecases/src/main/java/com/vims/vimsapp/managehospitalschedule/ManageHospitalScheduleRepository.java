package com.vims.vimsapp.managehospitalschedule;

import android.support.annotation.NonNull;
import android.util.Log;

import com.vims.vimsapp.entities.reports.VaccinationEventsPack;
import com.vims.vimsapp.entities.schedule.HospitalImmunisationEvent;
import com.vims.vimsapp.entities.schedule.HospitalSchedule;
import com.vims.vimsapp.entities.vaccinations.VaccinationCard;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationLocationState;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

/**
 * Created by root on 5/26/18.
 */


/**
 * Concrete implementation to load tasks from the data sources into a cache.
 * <p>
 * For simplicity, this implements a dumb synchronisation between locally persisted data and data
 * obtained from the server, by using the remote data source only if the local database doesn't
 * exist or is empty.
 */
 public class ManageHospitalScheduleRepository implements ManageHospitalScheduleDataSource{


    private static ManageHospitalScheduleRepository INSTANCE = null;
    private final ManageHospitalScheduleDataSource mVaccinationsRemoteDataSource;
    private final ManageHospitalScheduleDataSource mVaccinationsLocalDataSource;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */


    public  Map<String, VaccinationCard> mCachedVaccinationCards;

    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */

    private  boolean mCachedVaccCardsCacheIsDirty = false;


        // Prevent direct instantiation.
    private ManageHospitalScheduleRepository(ManageHospitalScheduleDataSource vacsRemoteDataSource, ManageHospitalScheduleDataSource vacsLocalDataSource) {
           mVaccinationsRemoteDataSource = checkNotNull(vacsRemoteDataSource);
           mVaccinationsLocalDataSource = checkNotNull(vacsLocalDataSource);

        }



    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param vacsRemoteDataSource the backend data source
     * @param vacsLocalDataSource  the device storage data source
     * @return the {@link ManageHospitalScheduleRepository} instance
     */
    public static ManageHospitalScheduleRepository getInstance(ManageHospitalScheduleDataSource vacsRemoteDataSource, ManageHospitalScheduleDataSource vacsLocalDataSource) {

        if (INSTANCE == null) {
            INSTANCE = new ManageHospitalScheduleRepository(vacsRemoteDataSource, vacsLocalDataSource);
        }
        return INSTANCE;
    }


    public static void destroyInstance() {
        INSTANCE = null;
    }





    @Override
    public void saveHospitalSchedule(HospitalSchedule hospitalSchedule, SaveHospitalScheduleCallback saveHospitalScheduleCallback) {
        //NOT FOR IMPLEMENTATION/USAGE

         mVaccinationsLocalDataSource.saveHospitalSchedule(hospitalSchedule,saveHospitalScheduleCallback);

    }




    @Override
    public void getHospitalSchedule(final boolean isLoadOnline, final String hospitalName, final LoadHospitalScheduleCallback callback) {
        //check available if not - online
        //if available do you need update

        //if there is no data in local database get online
        mVaccinationsLocalDataSource.checkHospitalScheduleAvailable(new LoadHospitalScheduleAvailabilityCallback() {
            @Override
            public void onHospitalScheduleAvailable(int size) {

                if(isLoadOnline){
                    //if local data is available but need online update

                    Log.d("VRepo From","Local Data Availbale but need online");
                    getScheduleFromRemoteDataSource(hospitalName,callback);


                }else {  //normally used by child schedule , and when theres no data

                    Log.d("VRepo From","Schedule From Local");
                    mVaccinationsLocalDataSource.getHospitalSchedule(false,hospitalName, new LoadHospitalScheduleCallback() {
                        @Override
                        public void onCardLoaded(HospitalSchedule hospitalSchedule) {

                            callback.onCardLoaded(hospitalSchedule);

                        }

                        @Override
                        public void onCardNotAvailable(String error) {

                            callback.onCardNotAvailable(error);

                        }
                    });



                }

            }

            @Override
            public void onHospitalScheduleUnAvailable() {

                Log.d("VRepo From","Schedule From Remote");
                // If the cache is dirty we need to fetch new data from the network save it locally.
                getScheduleFromRemoteDataSource(hospitalName,callback);


            }
        });


        }







    //0752 15 65 37




    //REMOTE////////////////////////////////////////////////////////////////////////////////

    private void getScheduleFromRemoteDataSource(String hospitalname , final LoadHospitalScheduleCallback loadHospitalScheduleCallback){

        mVaccinationsRemoteDataSource.getHospitalSchedule(true,hospitalname, new LoadHospitalScheduleCallback() {
            @Override
            public void onCardLoaded(HospitalSchedule hospitalSchedule) {

                Log.d("VRepoFromRemote","Remote Data UnAvailable");

                //1. refresh local data source
                refreshLocalDataSourceHospitalSchedule(hospitalSchedule.getWorkingDays());

                //2. load the events
                loadHospitalScheduleCallback.onCardLoaded(hospitalSchedule);

            }

            @Override
            public void onCardNotAvailable(String error) {

                Log.d("VRepoFromRemote","Remote Data UnAvailable");

                loadHospitalScheduleCallback.onCardNotAvailable(error);

            }
        });


    }



    @Override
    public void getRegisteredHospital(String hospitalId, isHospitalExistCallback existCallback) {



        mVaccinationsRemoteDataSource.getRegisteredHospital(hospitalId,existCallback);


    }

    private void refreshLocalDataSourceHospitalSchedule(List<HospitalImmunisationEvent> hospitalImmunisationEvents) {
        mVaccinationsLocalDataSource.deleteHospitalEvents();


         HospitalSchedule hospitalSchedule = new HospitalSchedule();
         hospitalSchedule.setWorkingDays(hospitalImmunisationEvents);

         mVaccinationsLocalDataSource.saveHospitalSchedule(hospitalSchedule, new SaveHospitalScheduleCallback() {
             @Override
             public void onHospitalScheduleSaved(HospitalSchedule hospitalSchedule) {

             }

             @Override
             public void onFailed(String error) {

             }
         });

        //no callback provide coz we dont need to know that they were refreshed local db




    }


    @Override
    public void checkHospitalScheduleAvailable(LoadHospitalScheduleAvailabilityCallback loadHospitalScheduleAvailabilityCallback) {

    }

    @Override
    public void deleteHospitalEvents() {
        mVaccinationsLocalDataSource.deleteHospitalEvents();

    }
}
