package com.vims.vimsapp.managehospitalschedule;


import com.vims.vimsapp.entities.reports.VaccinationEventsPack;
import com.vims.vimsapp.entities.schedule.Hospital;
import com.vims.vimsapp.entities.schedule.HospitalSchedule;
import com.vims.vimsapp.entities.schedule.ReadScheduleEntity;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.manageorders.Dosage;

import java.util.List;


public interface ManageHospitalScheduleDataSource {



    interface isHospitalExistCallback {

        void onAvailable(Hospital hospital);
        void onNotAvailable(String error);
    }
    // callbacks are like output boundaries
    interface SaveHospitalScheduleCallback {

        void onHospitalScheduleSaved(HospitalSchedule hospitalSchedule);

        void onFailed(String error);
    }

    interface LoadHospitalScheduleAvailabilityCallback {

        void onHospitalScheduleAvailable(int size);

        void onHospitalScheduleUnAvailable();
    }

    interface LoadHospitalScheduleCallback {

        void onCardLoaded(HospitalSchedule hospitalSchedule);

        void onCardNotAvailable(String error);
    }





    void saveHospitalSchedule(HospitalSchedule hospitalSchedule, SaveHospitalScheduleCallback saveHospitalScheduleCallback);
    void checkHospitalScheduleAvailable(LoadHospitalScheduleAvailabilityCallback loadHospitalScheduleAvailabilityCallback) ;
    void getHospitalSchedule(boolean isLoadOnline, String hospitalName, LoadHospitalScheduleCallback callback);
    void getRegisteredHospital(String hospitalId, isHospitalExistCallback existCallback);
    void deleteHospitalEvents();







}
