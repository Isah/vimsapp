package com.vims.vimsapp.managehospitalschedule;

import android.support.annotation.NonNull;
import android.util.Log;

import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.entities.schedule.HospitalImmunisationEvent;
import com.vims.vimsapp.entities.schedule.WeekSchedule;
import com.vims.vimsapp.entities.schedule.HospitalSchedule;

import java.util.List;

public class GenerateHospitalSchedule implements UseCase<GenerateHospitalSchedule.RequestValues,GenerateHospitalSchedule.ResponseValue>{

    private static GenerateHospitalSchedule INSTANCE = null;

    ManageHospitalScheduleRepository hospitalScheduleRepository;

    private GenerateHospitalSchedule(ManageHospitalScheduleRepository vaccinationsRepository){

        this.hospitalScheduleRepository = vaccinationsRepository;

    }

    public static GenerateHospitalSchedule getInstance(ManageHospitalScheduleRepository vaccinationsRepository) {

        if (INSTANCE == null) {
            INSTANCE = new GenerateHospitalSchedule(vaccinationsRepository);
        }
        return INSTANCE;
    }



    public enum RequestType{
        LOAD_SCHEDULE,CREATE_SCHEDULE
    }

    @Override
    public void execute(RequestValues param, final OutputBoundary<ResponseValue> delivery) {


        Log.d("ScheduleLoad","IS: "+param.isLoadOnline());

        if(param.getScheduleRequestType() == RequestType.CREATE_SCHEDULE){
            //means schedule creation request
            Log.d("ScheduleLoad","IS OFFLINE: "+param.isLoadOnline());
           createHospitalSchedule(param.getHospitalName(),param.getWeekSchedules(),delivery);

        }else{ //load schedule

            Log.d("ScheduleLoad","IS ONLINE: "+param.isLoadOnline());
          loadHospitalSchedule(param,delivery);

        }


    }


    private void createHospitalSchedule(final String hospitalName, List<WeekSchedule> weekSchedules, final OutputBoundary<ResponseValue> delivery){

        HospitalSchedule hospitalSchedule =  HospitalScheduleService.getDummyCreatedHospitalSchedule(hospitalName,weekSchedules);
        hospitalScheduleRepository.saveHospitalSchedule(hospitalSchedule, new ManageHospitalScheduleDataSource.SaveHospitalScheduleCallback() {
            @Override
            public void onHospitalScheduleSaved(HospitalSchedule hospitalSchedule) {

                String resp = hospitalName+" Schedule has been created";

                ResponseValue responseValue = new ResponseValue(resp);
                delivery.onSuccess(responseValue);

            }

            @Override
            public void onFailed(String error) {
                delivery.onError(error);
            }
        });


    }

    private void loadHospitalSchedule(final RequestValues params, final OutputBoundary<ResponseValue> delivery){

        hospitalScheduleRepository.getHospitalSchedule(params.isLoadOnline(),params.getHospitalName(), new ManageHospitalScheduleDataSource.LoadHospitalScheduleCallback() {
            @Override
            public void onCardLoaded(HospitalSchedule hospitalSchedule) {

                String resp = params.getHospitalName()+" Schedule has been created";

                ResponseValue responseValue = new ResponseValue(resp);
                delivery.onSuccess(responseValue);

            }

            @Override
            public void onCardNotAvailable(String error) {

                delivery.onError(error);

            }
        });


    }




    /*It implements such that its of a right type to be accepted in
    the execute method */
    public static final class RequestValues implements UseCase.RequestValues {

        private String hospitalName;
        private boolean isLoadOnline;
        private RequestType scheduleRequestType;

        private List<WeekSchedule> weekSchedules;

        public RequestValues(boolean isLoadOnline,String hospitalName, RequestType requestType) {
            this.hospitalName = hospitalName;
            this.isLoadOnline = isLoadOnline;
            this.scheduleRequestType = requestType;
        }


        public List<WeekSchedule> getWeekSchedules() {
            return weekSchedules;
        }

        public void setWeekSchedules(List<WeekSchedule> weekSchedules) {
            this.weekSchedules = weekSchedules;
        }

        public boolean isLoadOnline() {
            return isLoadOnline;
        }

        public String getHospitalName() {
            return hospitalName;
        }

        public RequestType getScheduleRequestType() {
            return scheduleRequestType;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private String response;

        public ResponseValue(@NonNull String response) {
            this.response = response;
        }

        public String getResponse() {
            return response;
        }
    }


}
