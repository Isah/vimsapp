package com.vims.vimsapp.managehospitalschedule;

import android.app.Application;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import com.vims.vimsapp.entities.schedule.Hospital;
import com.vims.vimsapp.entities.schedule.HospitalImmunisationEvent;
import com.vims.vimsapp.entities.schedule.HospitalSchedule;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationLocationState;
import com.vims.vimsapp.utilities.PreferenceHandler;

import java.io.UnsupportedEncodingException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageHospitalRemoteDataSource implements ManageHospitalScheduleDataSource {

    private ManageHospitalApi vaccinationsApi;


    private static volatile ManageHospitalRemoteDataSource INSTANCE;


    private PreferenceHandler preferenceHandler;

    // Prevent direct instantiation.
    private ManageHospitalRemoteDataSource(@NonNull ManageHospitalApi vaccinationsApi, Application application) {
        this.vaccinationsApi = vaccinationsApi;
        preferenceHandler = PreferenceHandler.getInstance(application);
    }

    public static ManageHospitalRemoteDataSource getInstance(@NonNull ManageHospitalApi vaccinationsApi, Application application) {
        if (INSTANCE == null) {
            synchronized (ManageHospitalRemoteDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ManageHospitalRemoteDataSource(vaccinationsApi,application);
                }
            }
        }
        return INSTANCE;
    }



    private String API_USER_ID;
    private String API_PASSWORD;
    private String API_TOKEN;

    public  String getAuthToken() {

        //API_USER_ID = "petr"; //used for testing

        API_USER_ID = preferenceHandler.getPref(PreferenceHandler.USER_ID);
        API_TOKEN = preferenceHandler.getPref(PreferenceHandler.USER_TOKEN);

        byte[] data = new byte[0];
        try {
            data = (API_USER_ID + ":" + API_TOKEN).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }



    @Override
    public void deleteHospitalEvents() {

    }




    @Override
    public void checkHospitalScheduleAvailable(LoadHospitalScheduleAvailabilityCallback loadHospitalScheduleAvailabilityCallback) {
        //NOT FOR IMPLEMENTATION
    }

    @Override
    public void saveHospitalSchedule(HospitalSchedule hospitalSchedule, SaveHospitalScheduleCallback saveHospitalScheduleCallback) {
        //NOT FOR IMPLEMENTATION
    }




    @Override
    public void getHospitalSchedule(boolean isLoadOnline, String hospitalName,final LoadHospitalScheduleCallback callback) {
        //load online is always true

        /*
        HospitalScheduleService hospitalScheduleService = new HospitalScheduleService();
        hospitalScheduleService.getDummyHospitalSchedule();
        //callback.onCardLoaded(hospitalScheduleService.getDummyHospitalSchedule());
        callback.onCardNotAvailable();
        */

        String hospitalId =  preferenceHandler.getPref(PreferenceHandler.USER_HOSPITAL_ID);

        Call<List<HospitalImmunisationEvent>> callSyncEvents = vaccinationsApi.getVimsHospitalSchedule(getAuthToken(),hospitalId);

        callSyncEvents.enqueue(new Callback<List<HospitalImmunisationEvent>>() {
            @Override
            public void onResponse(@NonNull Call<List<HospitalImmunisationEvent>> call, @NonNull Response<List<HospitalImmunisationEvent>> response) {

                if(response.isSuccessful()) {
                    Log.d("RemoteScheduleSource", "Data In" + response.body().size());

                    HospitalSchedule hospitalSchedule = new HospitalSchedule();
                    hospitalSchedule.setWorkingDays(response.body());
                    callback.onCardLoaded(hospitalSchedule);
                }
                else if(response.code() == 401){

                    callback.onCardNotAvailable("UnAuthorized!");
                }else if(response.code() == 404){

                    callback.onCardNotAvailable("Hospital schedule not available!");
                }else{

                    callback.onCardNotAvailable("connecting to server! "+response.code());

                }


            }

            @Override
            public void onFailure(Call<List<HospitalImmunisationEvent>> call, Throwable t) {

   /** DUMMY SCHEDULE **/

                HospitalScheduleService hospitalScheduleService = new HospitalScheduleService();
                hospitalScheduleService.getDummyHospitalSchedule();
                callback.onCardLoaded(hospitalScheduleService.getDummyHospitalSchedule());




                 String msg = "Can't connect to internet!";
                 callback.onCardNotAvailable(msg);

            }
        });



    }




    private Hospital getDummyHospital(){


        Hospital hospital = new Hospital();//Kyeibanga HC II
        hospital.setHospitalId("6936");
        hospital.setHospitalName("Kyeibanga HC II");
        hospital.setVimServerId("5ceda2846c44052e9bb59d0f");//vims hospital id
        //hospital.setHospitalLocation("kitagata subcounty,sheema south,sheema");

        return hospital;
    }

    @Override
    public void getRegisteredHospital(final String hospitalId, final isHospitalExistCallback existCallback) {


        Call<Hospital> callSyncEvents = vaccinationsApi.getVimsHospital(getAuthToken(),hospitalId);
        callSyncEvents.enqueue(new Callback<Hospital>() {
            @Override
            public void onResponse(@NonNull Call<Hospital> call, @NonNull Response<Hospital> response) {

                if (response.isSuccessful()) {

                 //   Log.d("ResponseCode", "CODE: "+response.code()+" BODY: "+response.body().getAddress());
                    existCallback.onAvailable(response.body());

                }else if(response.code() == 401){

                    existCallback.onNotAvailable("!UnAuthorized");
                }else if(response.code() == 404){


                    existCallback.onNotAvailable("Hospital with ID "+hospitalId+" not registered with us, Please Contact Admin!");
                }else{
                    existCallback.onNotAvailable("!"+response.code());

                }


            }

            @Override
            public void onFailure(@NonNull Call<Hospital> call, @NonNull Throwable t) {

             /** DUMMY HOSPITAL **/

                  existCallback.onAvailable(getDummyHospital());

                  String msg = "Can't connect to internet!";
                  existCallback.onNotAvailable(msg);
            }
        });


    }
}
