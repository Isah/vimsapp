package com.vims.vimsapp.managehospitalschedule;

import android.support.annotation.NonNull;

import com.vims.vimsapp.entities.reports.AnnualCoverage;
import com.vims.vimsapp.entities.reports.MonthTargetPopulation;
import com.vims.vimsapp.entities.schedule.HospitalImmunisationEvent;
import com.vims.vimsapp.entities.schedule.HospitalSchedule;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationLocationState;
import com.vims.vimsapp.entities.vaccinations.VaccinationStatus;
import com.vims.vimsapp.manageorders.VaccineOrderDao;
import com.vims.vimsapp.registerchild.local.VaccinationEventDao;
import com.vims.vimsapp.utilities.AppExecutors;
import com.vims.vimsapp.utilities.DateCalendarConverter;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class ManageHospitalScheduleLocalDataSource implements ManageHospitalScheduleDataSource {



    private static volatile ManageHospitalScheduleLocalDataSource INSTANCE;

    private VaccinationEventDao mvaccinationEventDao;
    private VaccineOrderDao mVaccineOrderDao;
    private AppExecutors mAppExecutors;



    //caretakerDao = PatientsDatabase.getInstance(context).getCaretakerDao();
    //childDao = PatientsDatabase.getInstance(context).getChildDao();

    // Prevent direct instantiation.
    private ManageHospitalScheduleLocalDataSource(@NonNull AppExecutors appExecutors,VaccinationEventDao vaccinationEventDao) {
        mAppExecutors = appExecutors;
        mvaccinationEventDao = vaccinationEventDao;


    }


    public static ManageHospitalScheduleLocalDataSource getInstance(@NonNull AppExecutors appExecutors, VaccinationEventDao vaccinationEventDao) {
        if (INSTANCE == null) {
            synchronized (ManageHospitalScheduleLocalDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ManageHospitalScheduleLocalDataSource(appExecutors,vaccinationEventDao);
                }
            }
        }
        return INSTANCE;
    }



    @Override
    public void checkHospitalScheduleAvailable(final LoadHospitalScheduleAvailabilityCallback loadHospitalScheduleAvailabilityCallback) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                int noOfHospitalEvents =   mvaccinationEventDao.getNoOfHospitalEvents();

                if(noOfHospitalEvents>0){

                loadHospitalScheduleAvailabilityCallback.onHospitalScheduleAvailable(noOfHospitalEvents);

                }else {

                loadHospitalScheduleAvailabilityCallback.onHospitalScheduleUnAvailable();

                }

            }
        };


        mAppExecutors.diskIO().execute(runnable);

    }

    @Override
    public void getHospitalSchedule(boolean isLoadOnline, String hospitalName, final LoadHospitalScheduleCallback callback) {

        //isload online is always false at this stage
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {

              final List<HospitalImmunisationEvent> hospitalImmunisationEvents =  mvaccinationEventDao.getHospitalEvents();

              mAppExecutors.mainThread().execute(new Runnable() {
                  @Override
                  public void run() {

                      if(hospitalImmunisationEvents!=null){

                          HospitalSchedule hospitalSchedule = new HospitalSchedule();
                          hospitalSchedule.setWorkingDays(hospitalImmunisationEvents);

                          callback.onCardLoaded(hospitalSchedule);

                      }else{

                          callback.onCardNotAvailable("Hospital Schedule UnAvailable");

                      }


                  }
              });


            }
        };

        mAppExecutors.diskIO().execute(runnable);


    }


    @Override
    public void saveHospitalSchedule(final HospitalSchedule hospitalSchedule, final SaveHospitalScheduleCallback saveHospitalScheduleCallback) {

       Runnable runnable = new Runnable() {
           @Override
           public void run() {

               long[] save = mvaccinationEventDao.InsertHospitalEvents(hospitalSchedule.getWorkingDays());

               if (save.length > 0) {

                   saveHospitalScheduleCallback.onHospitalScheduleSaved(hospitalSchedule);

               } else {

                   saveHospitalScheduleCallback.onFailed("Error Saving Schedule");

               }

           }
       };


       mAppExecutors.diskIO().execute(runnable);

    }

    @Override
    public void getRegisteredHospital(String hospitalId, isHospitalExistCallback existCallback) {

    }




    @Override
    public void deleteHospitalEvents() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                mvaccinationEventDao.deleteHospitalEvents();

            }
        };

        mAppExecutors.diskIO().execute(runnable);

    }


}
