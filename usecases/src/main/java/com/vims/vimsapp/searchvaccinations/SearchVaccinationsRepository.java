package com.vims.vimsapp.searchvaccinations;

import android.support.annotation.NonNull;

import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.reports.VaccinationEventsPack;
import com.vims.vimsapp.recordvaccinations.AddVaccinationDataSource;
import com.vims.vimsapp.registerchild.AddChildDataSource;

import java.util.Map;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class SearchVaccinationsRepository implements SearchVaccinationsDataSource {


    private static SearchVaccinationsRepository INSTANCE = null;
    //private final AddVaccinationDataSource mPatientsRemoteDataSource;
    private final SearchVaccinationsDataSource mPatientsLocalDataSource;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    public Map<String, Caretaker> mCachedCaretakers;

    public  Map<String, Child> mCachedChidren;


    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */
    private  boolean mCaretakerCacheIsDirty = false;
    private  boolean mChildrenCacheIsDirty = false;
    private  boolean mCachedVaccCardsCacheIsDirty = false;


    // Prevent direct instantiation.

    private SearchVaccinationsRepository(SearchVaccinationsDataSource patientsLocalDataSource) {
      //  mPatientsRemoteDataSource = checkNotNull(patientsRemoteDataSource);
        mPatientsLocalDataSource = checkNotNull(patientsLocalDataSource);

    }



    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param patientsLocalDataSource  the device storage data source
     * @return the {@link AddChildDataSource} instance
     */
    public static SearchVaccinationsRepository getInstance(SearchVaccinationsDataSource patientsLocalDataSource) {

        if (INSTANCE == null) {
            INSTANCE = new SearchVaccinationsRepository(patientsLocalDataSource);
        }
        return INSTANCE;
    }


    public static void destroyInstance() {
        INSTANCE = null;
    }




    private  final int HOME_SCREEN =0;
    private  final int STATS_SCREEN = 1;


    @Override
    public void getDosageUsage(int month, LoadDosageUsageCallback usageCallback) {

        mPatientsLocalDataSource.getDosageUsage(month,usageCallback);

    }

    @Override
    public void checkChildCardsSize(LoadChildCardsSizeCallback loadCardsSizeCheckCallback) {

        mPatientsLocalDataSource.checkChildCardsSize(loadCardsSizeCheckCallback);

    }

    @Override
    public void getVaccinationEventsPack(int whoWants,String vaccine, int yearOfEvent, final LoadVaccinationEventsPackCallback packCallback) {
        checkNotNull(packCallback);
        // Respond immediately with cache if available and not dirty
        if(whoWants == HOME_SCREEN){

            // mPatientsLocalDataSource.getVaccinationEventsPack(whoWants,vaccine,yearOfEvent,);
            mPatientsLocalDataSource.getVaccinationEventsPack(whoWants,vaccine, yearOfEvent, new LoadVaccinationEventsPackCallback() {
                @Override
                public void onEventsPackLoaded(VaccinationEventsPack vaccinationEventsPack) {

                    //  refreshVaccinationsEventsPackCache(vaccinationEventsPack);
                    packCallback.onEventsPackLoaded(vaccinationEventsPack);
                }

                @Override
                public void packUnAvailable() {
                    packCallback.packUnAvailable();

                }
            });


        }else{


            if (mCachedPackEntry != null && !mCachedEventsPackCacheIsDirty) {
                // ArrayList<VaccinationEventsPack> ve = new ArrayList<>(mCachedVaccinationEventsPack.values());
                //Log.d("SizeDataX", "Data Refresh Cache: "+ ve.get(0).getVaccineName());
                packCallback.onEventsPackLoaded(mCachedPackEntry);

            }

            else{

                mPatientsLocalDataSource.getVaccinationEventsPack(whoWants,vaccine, yearOfEvent, new LoadVaccinationEventsPackCallback() {
                    @Override
                    public void onEventsPackLoaded(VaccinationEventsPack vaccinationEventsPack) {

                        refreshVaccinationsEventsPackCache(vaccinationEventsPack);
                        packCallback.onEventsPackLoaded(vaccinationEventsPack);
                    }

                    @Override
                    public void packUnAvailable() {
                        packCallback.packUnAvailable();

                    }
                });
            }

        }

    }


    private VaccinationEventsPack mCachedPackEntry;//= candidateHospitalDates.entrySet().iterator().next();
    private  boolean mCachedEventsPackCacheIsDirty = false;

    private void refreshVaccinationsEventsPackCache(@NonNull VaccinationEventsPack vaccinationEventsPack) {
        if (mCachedPackEntry == null) {
            mCachedPackEntry = new VaccinationEventsPack();
        }
        mCachedPackEntry = vaccinationEventsPack;

        mCachedEventsPackCacheIsDirty = false;
    }




    @Override
    public void getReadScheduleEvents(int eventype,int month,int year, LoadReadScheduleEvents eventsCallback) {
        mPatientsLocalDataSource.getReadScheduleEvents(eventype,month,year,eventsCallback);
    }

    @Override
    public void getVaccinationEvents(LoadAllVaccinationEventsCallback loadAllVaccinationEventsCallback) {
        mPatientsLocalDataSource.getVaccinationEvents(loadAllVaccinationEventsCallback);
    }


    @Override
    public void searchVaccinationEvents(String searchTerm, SearchEventCallback searchEventCallback) {

    }

    @Override
    public void refreshEvents() {
       // mCachedEventsCacheIsDirty = true;
        mCachedEventsPackCacheIsDirty = true;
    }
}
