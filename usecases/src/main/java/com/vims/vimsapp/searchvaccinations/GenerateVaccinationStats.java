package com.vims.vimsapp.searchvaccinations;

import android.support.annotation.NonNull;

import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.entities.reports.VaccinationEventsPack;

public class GenerateVaccinationStats implements UseCase<GenerateVaccinationStats.RequestValues,GenerateVaccinationStats.ResponseValue>{



    private SearchVaccinationsRepository vaccinationsRepository;


    public GenerateVaccinationStats(SearchVaccinationsRepository  vaccinationsRepository) {
        this.vaccinationsRepository = vaccinationsRepository;
    }


    public   final int HOME_SCREEN = 0;
    public   final int STATS_SCREEN = 1;


    int yearInit = 0;
    String myVaccine = "vaccine";
    @Override
    public void execute(RequestValues param, final OutputBoundary<ResponseValue> delivery) {


        int screen = param.getScreen();
        int year = param.getYearDate();
        String vaccineName = param.getVaccineName();


        if(screen == STATS_SCREEN){

          boolean isDataChanged = param.isDataChanged();

         if(isDataChanged) {
            vaccinationsRepository.refreshEvents();
          }else if(yearInit != year){
            yearInit = year;
            vaccinationsRepository.refreshEvents();
          }
          else if(!myVaccine.equals(vaccineName)){
            vaccinationsRepository.refreshEvents();
          }


        }



        vaccinationsRepository.getVaccinationEventsPack(screen, vaccineName, year, new SearchVaccinationsDataSource.LoadVaccinationEventsPackCallback() {
            @Override
            public void onEventsPackLoaded(VaccinationEventsPack vaccinationEventsPack) {
                delivery.onSuccess(new ResponseValue(vaccinationEventsPack));


            }

            @Override
            public void packUnAvailable() {
                delivery.onError("No Stats");

            }
        });






    }

    //it implements such that its of a right type to be accepted in the execute method
    public static final class RequestValues implements UseCase.RequestValues {


        private final String vaccineName;
        private final int screen;
        private final int yearDate;
        private boolean isDataChanged;

        public RequestValues(String vaccineName, int screen, int yearDate) {
            this.vaccineName = vaccineName;
            this.screen = screen;
            this.yearDate = yearDate;

        }

        public void setDataChanged(boolean dataChanged) {
            isDataChanged = dataChanged;
        }

        public boolean isDataChanged() {
            return isDataChanged;
        }

        public String getVaccineName() {
            return vaccineName;
        }

        public int getScreen() {
            return screen;
        }

        public int getYearDate() {
            return yearDate;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private VaccinationEventsPack vaccinationEventsPack;

        public ResponseValue(@NonNull VaccinationEventsPack report) {
            this.vaccinationEventsPack = report;
        }

        public VaccinationEventsPack getVaccinationStats() {
            return vaccinationEventsPack;
        }
    }



}
