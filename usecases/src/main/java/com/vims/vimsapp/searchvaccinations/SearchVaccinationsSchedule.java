package com.vims.vimsapp.searchvaccinations;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.vims.vimsapp.UseCase;

import com.vims.vimsapp.entities.schedule.ReadScheduleEntity;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.manageorders.Dosage;
import com.vims.vimsapp.manageorders.VaccinePlan;
import com.vims.vimsapp.utilities.DateCalendarConverter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SearchVaccinationsSchedule implements UseCase<SearchVaccinationsSchedule.RequestValues, SearchVaccinationsSchedule.ResponseValue>{


    private static SearchVaccinationsSchedule INSTANCE = null;


    SearchVaccinationsRepository vaccinationsRepository;

    private SearchVaccinationsSchedule(SearchVaccinationsRepository vaccinationsRepository){

        this.vaccinationsRepository = vaccinationsRepository;

    }

    public static SearchVaccinationsSchedule getInstance(SearchVaccinationsRepository vaccinationsRepository) {

        if (INSTANCE == null) {
            INSTANCE = new SearchVaccinationsSchedule(vaccinationsRepository);
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }



    //for testing purposes

    public ResponseValue responseValue;

    @Override
    public void execute(RequestValues param, OutputBoundary<ResponseValue> delivery) {

      //  vaccinationsRepository.refreshCards();
        //i think the refresh happens when theres a net conn

           if(param.getVaccinationsRequestType() == VaccinationsRequestType.DOSAGE){

               generateDosage(param,delivery);

           }else {

               generateApproriateVaccinations(param, delivery);
           }



    }



    /*
    private void getEventsTHisMonth(){

        if(!upComingEvents.isEmpty()){

            // Collections.sort(upComingEvents);
            //get the first date of not done vacs
            // dateToday = DateCalendarConverter.dateWithTimeToDateWithoutTime(new Date());

            //convert the strings to date for comparison
            dateToday = stringToDateWithoutTime(upComingEvents.get(0).getScheduledDate());


            //1. get vacs for this month not done
            for(VaccinationEvent event: upComingEvents){

                Date dateScheduled = DateCalendarConverter.stringToDateWithoutTime(event.getScheduledDate());

                if(dateScheduled.equals(dateToday)){
                    upComingEventsNext.add(event);
                }
            }

            //2. get the vacs for this month all

            for(VaccinationEvent event: card.getVaccinationEvents()){
                if(event.getScheduledDate().equals(dateToday)){
                    totalEventsNext.add(event);
                }
            }


        }

    }


     if(dateToday==null){
                        delivery.onError("No Upcoming Events");

                    }else {
                        ResponseValue responseValue = new ResponseValue(upComingEvents,upComingEvents.size(),upComingEvents.size());
                        responseValue.setNextVaccinationDate(DateCalendarConverter.dateToStringWithoutTime(dateToday));
                        delivery.onSuccess(responseValue);


                    }

    */



    private void generateDosage(final RequestValues param,final OutputBoundary<ResponseValue> delivery){

        final int orderMonth = param.getOrderMonth();

        vaccinationsRepository.getDosageUsage(orderMonth,new SearchVaccinationsDataSource.LoadDosageUsageCallback() {
            @Override
            public void onDosageLoaded(List<Dosage> dosages) {


                ResponseValue responseValue = new ResponseValue(null);
                responseValue.setDosageList(dosages);

                delivery.onSuccess(responseValue);


            }

            @Override
            public void onFailed() {

                delivery.onError("Couldn't load doses");


            }
        });

    }


    public static int UPCOMING = 0;
    public static int MISSED = 1;
    public static int DONE = 2;

    private void  generateApproriateVaccinations(final RequestValues param,final OutputBoundary<ResponseValue> delivery) {

        final int givenMonth = DateCalendarConverter.monthStringToInt(param.getMonth());
        final int givenYear = param.getYear();

        if (param.vaccinationsRequestType == VaccinationsRequestType.DUE) {

            vaccinationsRepository.getReadScheduleEvents(UPCOMING, givenMonth, givenYear, new SearchVaccinationsDataSource.LoadReadScheduleEvents() {
                @Override
                public void onEventsLoaded(ReadScheduleEntity readScheduleEntity) {


                    //Are we in same month or month given is diff
                    //If diff load upcoming events
                    int thisMonth = DateCalendarConverter.dateToIntGetMonth(new Date());
                    int thisYear = Calendar.getInstance().get(Calendar.YEAR);
                    int nextYear = thisYear + 1;


                    if (givenMonth == thisMonth && givenYear == thisYear) {
                        // return due events
                        if (readScheduleEntity.getDueEvents() != null && !readScheduleEntity.getDueEvents().isEmpty()) {

                            ResponseValue responseValue = new ResponseValue(readScheduleEntity.getDueEvents());

                            delivery.onSuccess(responseValue);
                        } else {
                            delivery.onError("No Events Due");
                        }

                    } else if (givenMonth > thisMonth && givenYear == thisYear) {
                        // return upcoming events
                        if (readScheduleEntity.getUpcomingEventsOfMonth() != null && !readScheduleEntity.getUpcomingEventsOfMonth().isEmpty()) {
                            ResponseValue responseValue = new ResponseValue(readScheduleEntity.getUpcomingEventsOfMonth());

                            Log.d("SearchVaccinations", readScheduleEntity.getUpcomingEventsOfMonth().get(0).getVaccineName());

                            delivery.onSuccess(responseValue);
                        } else {
                            delivery.onError("No Upcoming Events");
                        }

                    } else if (givenMonth == thisMonth && givenYear == nextYear) {
                        // return upcoming events
                        if (readScheduleEntity.getUpcomingEventsOfMonth() != null && !readScheduleEntity.getUpcomingEventsOfMonth().isEmpty()) {
                            ResponseValue responseValue = new ResponseValue(readScheduleEntity.getUpcomingEventsOfMonth());

                            Log.d("SearchVaccinations", "yy: " + readScheduleEntity.getUpcomingEventsOfMonth().get(0).getVaccineName());

                            delivery.onSuccess(responseValue);
                        } else {
                            delivery.onError("No Upcoming Events");
                        }
                    } else if (givenMonth > thisMonth && givenYear == nextYear) {
                        // return upcoming events
                        if (!readScheduleEntity.getUpcomingEventsOfMonth().isEmpty() && readScheduleEntity.getUpcomingEventsOfMonth() != null) {
                            ResponseValue responseValue = new ResponseValue(readScheduleEntity.getUpcomingEventsOfMonth());

                            Log.d("SearchVaccinations", "yyy: " + readScheduleEntity.getUpcomingEventsOfMonth().get(0).getVaccineName());

                            delivery.onSuccess(responseValue);
                        } else {
                            delivery.onError("No Upcoming Events");
                        }
                    }
                    else if (givenMonth < thisMonth && givenYear == nextYear) {
                        // return upcoming events
                        if (!readScheduleEntity.getUpcomingEventsOfMonth().isEmpty() && readScheduleEntity.getUpcomingEventsOfMonth() != null) {
                            ResponseValue responseValue = new ResponseValue(readScheduleEntity.getUpcomingEventsOfMonth());

                            Log.d("SearchVaccinations", "yyy: " + readScheduleEntity.getUpcomingEventsOfMonth().get(0).getVaccineName());

                            delivery.onSuccess(responseValue);
                        } else {
                            delivery.onError("No Upcoming Events");
                        }
                    }



                }


                @Override
                public void onEventsNotAvailable(String error) {

                    delivery.onError("No events");


                }
            });
        }

         else if (param.vaccinationsRequestType == VaccinationsRequestType.DONE) {

            vaccinationsRepository.getReadScheduleEvents(DONE, givenMonth, givenYear, new SearchVaccinationsDataSource.LoadReadScheduleEvents() {
                @Override
                public void onEventsLoaded(ReadScheduleEntity readScheduleEntity) {

                    if (!readScheduleEntity.getDoneEvents().isEmpty() || readScheduleEntity.getDoneEvents() != null) {
                        ResponseValue responseValue = new ResponseValue(readScheduleEntity.getDoneEvents());

                        delivery.onSuccess(responseValue);
                    } else {
                        delivery.onError("No Missed Events");
                    }


                }

                @Override
                public void onEventsNotAvailable(String error) {

                }
            });


        } else if (param.vaccinationsRequestType == VaccinationsRequestType.DATE) {


            ArrayList<VaccinationEvent> dateEvents = new ArrayList<>();


            ResponseValue responseValue = new ResponseValue(dateEvents);
            delivery.onSuccess(responseValue);


        } else if (param.vaccinationsRequestType == VaccinationsRequestType.OVERDUE) {

            vaccinationsRepository.getReadScheduleEvents(MISSED, givenMonth, givenYear, new SearchVaccinationsDataSource.LoadReadScheduleEvents() {
                @Override
                public void onEventsLoaded(ReadScheduleEntity readScheduleEntity) {


                    if (!readScheduleEntity.getMissedEvents().isEmpty() && readScheduleEntity.getMissedEvents() != null) {
                        ResponseValue responseValue = new ResponseValue(readScheduleEntity.getMissedEvents());
                        delivery.onSuccess(responseValue);
                    } else {

                        delivery.onError("There are no Missed Events");
                    }

                }

                @Override
                public void onEventsNotAvailable(String error) {

                }
            });
        }





    }




    public enum VaccinationsRequestType{

        DUE,DONE,OVERDUE,CHILD,DATE,TOTAL,DOSAGE

    }

    //it implements such that its of a right type to be accepted in the execute method
    public static final class RequestValues implements UseCase.RequestValues {

        private final VaccinationsRequestType vaccinationsRequestType;
        private Date date;
        private String month;
        private int year;

        private int orderMonth;

        //private final VaccineSpec  vaccineSpec;
        //    private final HospitalSchedule hospitalSchedule;


        public RequestValues(VaccinationsRequestType vaccinationsRequestType) {
            this.vaccinationsRequestType = vaccinationsRequestType;

        }


        public VaccinationsRequestType getVaccinationsRequestType() {
            return vaccinationsRequestType;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public int getOrderMonth() {
            return orderMonth;
        }

        public void setOrderMonth(int orderMonth) {
            this.orderMonth = orderMonth;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private List<VaccinationEvent> vaccinationEvents;
        private List<Dosage> dosageList;


        public ResponseValue(@NonNull List<VaccinationEvent> vaccinationEvents) {
            this.vaccinationEvents = vaccinationEvents;

        }


        public void setDosageList(List<Dosage> dosageList) {
            this.dosageList = dosageList;
        }

        public List<Dosage> getDosageList() {
            return dosageList;
        }


        public List<VaccinationEvent> getVaccinationEvents() {
            return vaccinationEvents;
        }



    }










}
