package com.vims.vimsapp.searchvaccinations;

import android.app.Application;
import android.support.annotation.NonNull;
import android.util.Log;

import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.child.SyncUpdateTime;
import com.vims.vimsapp.entities.reports.MonthTargetPopulation;
import com.vims.vimsapp.entities.reports.VaccinationEventsPack;
import com.vims.vimsapp.entities.schedule.ReadScheduleEntity;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationLocationState;
import com.vims.vimsapp.entities.vaccinations.VaccinationStatus;
import com.vims.vimsapp.entities.vaccinations.VaccineSpecification;
import com.vims.vimsapp.manageorders.Dosage;
import com.vims.vimsapp.recordvaccinations.AddVaccinationDataSource;
import com.vims.vimsapp.registerchild.local.ChildDao;
import com.vims.vimsapp.registerchild.local.PatientsSyncTimeDao;
import com.vims.vimsapp.registerchild.local.VaccinationEventDao;
import com.vims.vimsapp.utilities.AppExecutors;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.viewcoverage.CoverageTimeLine;
import com.vims.vimsapp.viewcoverage.ReadVaccines;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class SearchVaccinationsLocalDataSource implements SearchVaccinationsDataSource{


    private static volatile SearchVaccinationsLocalDataSource INSTANCE;

    //private CaretakerDao mCaretakerDao;
    private ChildDao mChildDao;


    private VaccinationEventDao mvaccinationEventDao;

    private PatientsSyncTimeDao mPatientsSyncTimeDao;
    private AppExecutors mAppExecutors;

    private Application mContext;

    private PreferenceHandler preferenceHandler;

    private String USER_ID;
    private String USER_HOSPITAL_ID;


    private SearchVaccinationsLocalDataSource(@NonNull AppExecutors appExecutors, ChildDao childDao, PatientsSyncTimeDao patientsSyncTimeDao, VaccinationEventDao vaccinationEventDao, Application context) {
        mAppExecutors = appExecutors;
        mChildDao = childDao;
        mvaccinationEventDao = vaccinationEventDao;
        mPatientsSyncTimeDao = patientsSyncTimeDao;
        this.mContext = context;
        preferenceHandler = PreferenceHandler.getInstance(mContext);


    }


    public static SearchVaccinationsLocalDataSource getInstance(@NonNull AppExecutors appExecutors, ChildDao childDao, PatientsSyncTimeDao patientsSyncTimeDao, VaccinationEventDao vaccinationEventDao, Application context) {
        if (INSTANCE == null) {
            synchronized (SearchVaccinationsLocalDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new SearchVaccinationsLocalDataSource(appExecutors,childDao,patientsSyncTimeDao,vaccinationEventDao, context);
                }
            }
        }
        return INSTANCE;
    }




    //------------------SEARCH --------------------------------------------------------------------------------------------



    private MonthTargetPopulation getMonthTargetPopulation(String vaccine,int yearOfEvent){
        int monthTarget;
        MonthTargetPopulation monthTargetPopulation = new MonthTargetPopulation();

        for(int month=0; month<12; month++) {
            if (month == 0) {
                monthTarget = mvaccinationEventDao.getMonthTargetPopulation(vaccine,month,yearOfEvent);
                monthTargetPopulation.setJan(monthTarget);
            } else if (month == 1) {
                monthTarget = mvaccinationEventDao.getMonthTargetPopulation(vaccine,month,yearOfEvent);
                monthTargetPopulation.setFeb(monthTarget);
            } else if (month == 2) {
                monthTarget = mvaccinationEventDao.getMonthTargetPopulation(vaccine,month,yearOfEvent);
                monthTargetPopulation.setMar(monthTarget);
            } else if (month == 3) {
                monthTarget = mvaccinationEventDao.getMonthTargetPopulation(vaccine,month,yearOfEvent);
                monthTargetPopulation.setApr(monthTarget);
            } else if (month == 4) {
                monthTarget = mvaccinationEventDao.getMonthTargetPopulation(vaccine,month,yearOfEvent);
                monthTargetPopulation.setMay(monthTarget);
            } else if (month == 5) {
                monthTarget = mvaccinationEventDao.getMonthTargetPopulation(vaccine,month,yearOfEvent);
                monthTargetPopulation.setJun(monthTarget);
            } else if (month == 6) {
                monthTarget = mvaccinationEventDao.getMonthTargetPopulation(vaccine,month,yearOfEvent);
                monthTargetPopulation.setJul(monthTarget);
            } else if (month == 7) {
                monthTarget = mvaccinationEventDao.getMonthTargetPopulation(vaccine,month,yearOfEvent);
                monthTargetPopulation.setAug(monthTarget);
            } else if (month == 8) {
                monthTarget = mvaccinationEventDao.getMonthTargetPopulation(vaccine,month,yearOfEvent);
                monthTargetPopulation.setSep(monthTarget);
            } else if (month == 9) {
                monthTarget = mvaccinationEventDao.getMonthTargetPopulation(vaccine,month,yearOfEvent);
                monthTargetPopulation.setOct(monthTarget);
            } else if (month == 10) {
                monthTarget = mvaccinationEventDao.getMonthTargetPopulation(vaccine,month,yearOfEvent);
                monthTargetPopulation.setNov(monthTarget);
            } else  {
                monthTarget = mvaccinationEventDao.getMonthTargetPopulation(vaccine,month,yearOfEvent);
                monthTargetPopulation.setDec(monthTarget);
            }
        }
        return monthTargetPopulation;

    }

    @Override
    public void getDosageUsage(final int month, final LoadDosageUsageCallback usageCallback) {


        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                //used in creating vaccine order

                //1. Dosage list to be given next month
                //2. Type in the balance this month
                //2. keep the used this month
                //3. calculate required this month


                ArrayList<Dosage> dosageUseds = new ArrayList<>();

                // Calendar calendar = Calendar.getInstance();
                // int month = calendar.get(Calendar.MONTH);

                List<VaccineSpecification> specs = ReadVaccines.getInstance().getVaccineSpecificationList();
                for(VaccineSpecification spec: specs){

                    //check if thres an upcoming event for the vaccine in given month

                    long dateToday = DateCalendarConverter.dateWithTimeToDateWithoutTime(new Date()).getTime();



                    int ordermonth = month;//preferenceHandler.getIntValue(PreferenceHandler.ORDER_MONTH_KEY);//may



                    int doneEventsMonth = mvaccinationEventDao.getNoDoneVaccinationEventsOfCurrentMonth(spec.getVaccineName(),ordermonth);


                    // int upcomingEventsOfThisMonth = mvaccinationEventDao.getNoUpcomingEventsOfVaccineInMonth(spec.getVaccineName(),thisMonth,dateToday);
                    int dueEventsOfThisMonth = mvaccinationEventDao.getNoDueEventsOfVaccineInMonth(spec.getVaccineName(),ordermonth);


                    //  int upcomingEventsOfNextMonth = mvaccinationEventDao.getNoUpcomingEventsOfVaccineInMonth(spec.getVaccineName(),nextMonth,dateToday);


                    if(dueEventsOfThisMonth > 0) {//means event is of that month

                        Dosage dosageUsed = new Dosage();
                        dosageUsed.setVaccine(spec.getVaccineName());
                        dosageUsed.setDosageUsedThisMonth(doneEventsMonth);
                        dosageUsed.setDosageNeededNextMonth(dueEventsOfThisMonth);
                        dosageUseds.add(dosageUsed);
                    }



                }

                final ArrayList<Dosage> dosageUsedsList = new ArrayList<>(dosageUseds);

                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {

                        if(!dosageUsedsList.isEmpty()){

                            usageCallback.onDosageLoaded(dosageUsedsList);

                        }else{

                            usageCallback.onFailed();

                        }


                    }
                });


            }
        };
        mAppExecutors.diskIO().execute(runnable);


    }


    private List<CoverageTimeLine> getMyCoverageTimeLines(String vaccineName) {
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        int nextYear = currentYear +1;
        int bottomYear = currentYear - 4;
        List<CoverageTimeLine> coverageTimeLines = new ArrayList<>();
        //Get coverage for all the past years
        for(int year=bottomYear; year<nextYear; year++){
            CoverageTimeLine coverageTimeLine = new CoverageTimeLine();
            //VaccineReportsCard vaccineReportsCard = getVaccineReportCard_Vaccine_Month(vaccineName,true,year,vaccinationEvents,monthTargetPopulation);


            int doneChildren =  mvaccinationEventDao.getNoDoneEventsOfYearOfVaccine(vaccineName,year);


            // int noChildren = mChildDao.getNoChildrenSize();
            //This only caters for children vaccinated in current year
            int noChildren = mvaccinationEventDao.getNoEventsOfVaccineOfYear(vaccineName,year);

            int coverage = (int) (((double) doneChildren / (double) noChildren) * 100);


            coverageTimeLine.setCoverage(coverage);
            coverageTimeLine.setVaccine(vaccineName);
            coverageTimeLine.setYear(year);
            coverageTimeLine.setTimelineId(UUID.randomUUID().toString());
            coverageTimeLine.setReportId(UUID.randomUUID().toString());
            coverageTimeLine.setTargetPopulation(noChildren);


            coverageTimeLines.add(coverageTimeLine);
        }
        return  coverageTimeLines;
    }


    private Map<Integer,Integer> getImmunisedChildrenFor12Months(String vaccine, int year){

        Map<Integer,Integer> immunisation = new HashMap<>();

        for(int m= 0;m < 12;m++ ) {

            int noImmunised = mvaccinationEventDao.getNoDoneEventsOfYearOfMonthOfVaccine(vaccine,m,year);
            immunisation.put(m,noImmunised);

        }

        return immunisation;
    }


    @Override
    public void getVaccinationEventsPack(int whoWants,final String vaccine,final int yearOfEvent,final LoadVaccinationEventsPackCallback packCallback) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {


                long dateToday = DateCalendarConverter.dateWithTimeToDateWithoutTime(new Date()).getTime();
                String userId = preferenceHandler.getPref(PreferenceHandler.USER_ID);

                final List<VaccinationEvent> myEventsDoneAllYear = mvaccinationEventDao.getAllDoneVaccinationEventsOfYear(userId,"done",yearOfEvent);
                final List<VaccinationEvent> vaccinationEvents = mvaccinationEventDao.getVaccinationEvents();
                final List<VaccinationEvent> vaccinationEventsDoneOfVaccine = mvaccinationEventDao.getDoneVaccinationEvents(vaccine);
                final List<VaccinationEvent> vaccinationEventsDoneOfYear = mvaccinationEventDao.getDoneVaccinationEventsOfYear(vaccine,yearOfEvent);
                final List<VaccinationEvent> vaccinationEventsNotDone = mvaccinationEventDao.getNotDoneVaccinationEventsOfYear("notdone",yearOfEvent);
                //final List<VaccinationEvent> vaccinationEventsUpcoming = mvaccinationEventDao.getUpcomingEventsThisYear(yearOfEvent,dateToday,"notdone");
                final List<VaccinationEvent> vaccinationEventsUpcoming = mvaccinationEventDao.getUpcomingEvents(dateToday,"notdone");


                //final List<VaccinationEvent> vaccinationEventsUpcoming = mvaccinationEventDao.getUpcomingEvents(dateToday,"notdone");

                final int noUpcomingEvents = mvaccinationEventDao.getNoUpcomingEvents(dateToday,"notdone");//yearOfEvent,
                //final int noUpcomingEvents = mvaccinationEventDao.getNoUpcomingEventsThisYear(yearOfEvent,dateToday,"notdone");//yearOfEvent,
                final int noMissedEvents = mvaccinationEventDao.getNoMissedEvents(dateToday,"notdone");

                int tMonth = DateCalendarConverter.dateToIntGetMonth(new Date());
                //Log.d("CurrentMonth","m: "+noUpcomingEvents+" year: "+yearOfEvent+" date: "+new Date(dateToday));
                final int noUpcomingEventsCurrentMonth = mvaccinationEventDao.getNoUpcomingEventsCurrentMonth(dateToday,tMonth,"notdone");
                final int noMissedEventsCurrentMonth = mvaccinationEventDao.getNoMissedEventsCurrentMonth(dateToday,tMonth,"notdone");
                //Log.d("CurrentMonth","u: "+noUpcomingEventsCurrentMonth);

                final Map<Integer,Integer> childrenImmunised = getImmunisedChildrenFor12Months(vaccine,yearOfEvent);
                final MonthTargetPopulation monthTargetPopulation = getMonthTargetPopulation(vaccine,yearOfEvent);
                final List<CoverageTimeLine> coverageTimeLines = getMyCoverageTimeLines(vaccine);

                //the no children who have missed a particular vaccine immunisation
                final int noChildrenMissed =  mvaccinationEventDao.getNoMissedEventsOfVaccineOfYear(vaccine,yearOfEvent,dateToday,"notdone");


                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (vaccinationEvents.isEmpty()) {
                            // This will be called if the table is new or just empty.
                            packCallback.packUnAvailable();
                        } else {

                            VaccinationEventsPack eventsPack = new VaccinationEventsPack();
                            eventsPack.setDoneEvents(vaccinationEventsDoneOfVaccine);//certain vaccine
                            eventsPack.setDoneAllVaccinesEventsOfYear(myEventsDoneAllYear);
                            eventsPack.setDoneEventsOfYear(vaccinationEventsDoneOfYear);

                            eventsPack.setNotDone(vaccinationEventsNotDone);
                            eventsPack.setAllEvents(vaccinationEvents);
                            eventsPack.setMonthTargetPopulation(monthTargetPopulation);


                            eventsPack.setUpComingEventsOfyear(vaccinationEventsUpcoming);
                            eventsPack.setNoUpcomingEventsOfyear(noUpcomingEvents);
                            eventsPack.setNoMissedEventsOfyear(noMissedEvents);

                            eventsPack.setNoMissedEventsOfCurrentMonth(noMissedEventsCurrentMonth);
                            eventsPack.setNoUpcomingEventsOfCurrentMonth(noUpcomingEventsCurrentMonth);

                            eventsPack.setImmunisedCHildren(childrenImmunised);
                            eventsPack.setCoverageTimeLines(coverageTimeLines);

                            eventsPack.setNoChildrenMissedImmunisation(noChildrenMissed);

                            // eventsPack.setAnnualCoverage(annualCoverage);
                            // eventsPack.setAnnualCoveragesTimeLines(annualCoveragesTImeLines);

                            packCallback.onEventsPackLoaded(eventsPack);
                        }
                    }
                });


            }
        };

        mAppExecutors.networkIO().execute(runnable);

    }


    @Override
    public void getReadScheduleEvents(final int eventtype, final int month,final int yr, final LoadReadScheduleEvents eventsCallback) {


        Runnable runnable = new Runnable() {
            @Override
            public void run() {

               // Calendar calendar = Calendar.getInstance();
                //int yr = calendar.get(Calendar.YEAR);

                long dateToday = DateCalendarConverter.dateWithTimeToDateWithoutTime(new Date()).getTime();

                final List<VaccinationEvent> eventsDone = mvaccinationEventDao.getDoneEventsOfYear(yr);
                final List<VaccinationEvent> eventsMissed = mvaccinationEventDao.getMissedEvents(dateToday,"notdone");
                //final List<VaccinationEvent> eventsUpcoming = mvaccinationEventDao.getUpcomingEventsOfMonthOfYear(yr,month,dateToday,"notdone");
                final List<VaccinationEvent> eventsUpcoming = mvaccinationEventDao.getUpcomingEventsOfMonthOfYearLtd(yr,month,dateToday,"notdone",10);
                final int upcomigEvents =mvaccinationEventDao.getNoUpcomingEventsOfMonthOfYear(yr,month,dateToday,"notdone");
                final List<VaccinationEvent> eventsDue = mvaccinationEventDao.getDueEventsOfMonthOfYear(yr,dateToday,"notdone");
                // final List<VaccinationEvent> eventsDue = mvaccinationEventDao.getDueEventsOfMonthOfYearLtd(dateToday,"notdone",2);

                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {

                        ReadScheduleEntity readScheduleEntity = new ReadScheduleEntity();

                        if(eventtype==SearchVaccinationsSchedule.UPCOMING){

                            readScheduleEntity.setUpcomingEventsOfMonth(eventsUpcoming);
                            readScheduleEntity.setDueEvents(eventsDue);
                            readScheduleEntity.setNoUpcomingEvents(upcomigEvents);

                        }else if(eventtype==SearchVaccinationsSchedule.MISSED){

                            readScheduleEntity.setMissedEvents(eventsMissed);

                        }

                        eventsCallback.onEventsLoaded(readScheduleEntity);

                    }
                });


            }
        };
        mAppExecutors.diskIO().execute(runnable);
    }




    @Override
    public void getVaccinationEvents(final LoadAllVaccinationEventsCallback loadAllVaccinationEventsCallback) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                //final List<VaccinationEvent> vaccinationEventsDone =  mvaccinationEventDao.getDoneVaccinationEvents("");
                //final List<VaccinationEvent> vaccinationEventsNotDone =  mvaccinationEventDao.getNotDoneVaccinationEvents();
                final List<VaccinationEvent> vaccinationEvents =  mvaccinationEventDao.getVaccinationEvents();
                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (vaccinationEvents.isEmpty()) {
                            // This will be called if the table is new or just empty.

                            loadAllVaccinationEventsCallback.onEventssNotAvailable();
                        } else {
                            loadAllVaccinationEventsCallback.onVaccinationEventsLoaded(vaccinationEvents);
                        }
                    }
                });
            }
        };


        mAppExecutors.networkIO().execute(runnable);
    }


    @Override
    public void searchVaccinationEvents(final String searchTerm,final SearchEventCallback searchEventCallback) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                final List<VaccinationEvent> events = mvaccinationEventDao.searchEvent(searchTerm);

                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {

                        if(!events.isEmpty()){

                            searchEventCallback.onSearchEvents(events);

                        }else{

                            searchEventCallback.onFailed("Even Not Found!");

                        }



                    }
                });


            }
        };

        mAppExecutors.diskIO().execute(runnable);



    }


    @Override
    public void checkChildCardsSize(final LoadChildCardsSizeCallback loadCardsSizeCheckCallback) {
        checkNotNull(loadCardsSizeCheckCallback);

        Runnable cardRunnable = new Runnable() {
            @Override
            public void run() {

                // int size = mVaccinationCardDao.getChildVaccinationCardsSize();
                //  int size = mvaccinationEventDao.getNoOfEventsAvailable();
                int size = mChildDao.getNoChildrenSize();


                if (size > 0) {

                    loadCardsSizeCheckCallback.onCardsSizeLoaded(size);

                } else {

                    loadCardsSizeCheckCallback.onCardNotAvailable();

                }

            }

        };

        mAppExecutors.diskIO().execute(cardRunnable);

    }


    @Override
    public void refreshEvents() {

    }
}
