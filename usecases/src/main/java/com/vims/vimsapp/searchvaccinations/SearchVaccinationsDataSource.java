package com.vims.vimsapp.searchvaccinations;


import com.vims.vimsapp.entities.reports.VaccinationEventsPack;
import com.vims.vimsapp.entities.schedule.Hospital;
import com.vims.vimsapp.entities.schedule.HospitalSchedule;
import com.vims.vimsapp.entities.schedule.ReadScheduleEntity;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.manageorders.Dosage;

import java.util.List;


public interface SearchVaccinationsDataSource {




    interface LoadVaccinationEventsPackCallback {

        void onEventsPackLoaded(VaccinationEventsPack vaccinationEventsPack);

        void packUnAvailable();
    }

    interface LoadChildCardsSizeCallback {

        void onCardsSizeLoaded(int size);

        void onCardNotAvailable();
    }


    interface LoadAllVaccinationEventsCallback {

        void onVaccinationEventsLoaded(List<VaccinationEvent> vaccinationEvents);

        void onEventssNotAvailable();
    }

    interface LoadDosageUsageCallback {

        void onDosageLoaded(List<Dosage> dosages);

        void onFailed();
    }

    interface LoadReadScheduleEvents{

        void onEventsLoaded(ReadScheduleEntity readScheduleEntity);

        void onEventsNotAvailable(String error);
    }


    interface SearchEventCallback {

        void onSearchEvents(List<VaccinationEvent> events);

        void onFailed(String error);
    }







    //SAVING-------------------  the guy that implements the callback gets the values



    void searchVaccinationEvents(String searchTerm, SearchEventCallback searchEventCallback);
    void checkChildCardsSize(LoadChildCardsSizeCallback loadCardsSizeCheckCallback);
    void getVaccinationEventsPack(int whoWants, String vaccine, int yearOfEvents, LoadVaccinationEventsPackCallback packCallback);
    void getVaccinationEvents(LoadAllVaccinationEventsCallback loadAllVaccinationEventsCallback);
    void refreshEvents();
    void getDosageUsage(int month, LoadDosageUsageCallback usageCallback);
    void getReadScheduleEvents(int eventtype,int month,int year, LoadReadScheduleEvents eventsCallback);






}
