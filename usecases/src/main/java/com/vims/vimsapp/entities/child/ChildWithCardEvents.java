package com.vims.vimsapp.entities.child;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;

import java.util.List;

/**
 * Created by root on 6/24/18.
 */

public class ChildWithCardEvents {

        @Embedded
        public Child child;

        @Relation(parentColumn = "childId", entityColumn = "childId")
        public List<VaccinationEvent> vaccinationEvents;


}
