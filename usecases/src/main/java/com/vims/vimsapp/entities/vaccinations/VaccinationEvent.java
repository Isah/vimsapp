package com.vims.vimsapp.entities.vaccinations;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.vims.vimsapp.entities.child.Child;

import java.util.Date;
import java.util.UUID;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by root on 5/25/18.
 */


@Entity(tableName = "vevent",indices = {@Index(value = {"childId"})}, foreignKeys = @ForeignKey(entity = Child.class,
        parentColumns = "childId",
        childColumns = "childId",
        onDelete = CASCADE))


public class VaccinationEvent implements Comparable<VaccinationEvent> {

    @PrimaryKey // (autoGenerate = true)
    @NonNull
    private String eventId;

    //@SerializedName("childId")
    //@Expose
    private String childId;

    //@SerializedName("childName")
    //@Expose
    private String childName;

    //@SerializedName("scheduledDate")
    //@Expose
    private long scheduledDate;

    //@SerializedName("vaccinationDate")
   // @Expose
    private String vaccinationDate;

    //@SerializedName("vaccinationStatus")
    //@Expose
    private VaccinationStatus vaccinationStatus;

    //@SerializedName("vaccinationLocationState")
    //@Expose
    private VaccinationLocationState vaccinationLocationState;
    private int vaccinationStatusCode;

    //@SerializedName("vaccineName")
    //@Expose
    private String vaccineName;
    private String hospitalId;
    private int yearOfEvent;
    private int monthOfEvent;
    private String userId;



    @Ignore
    public VaccinationEvent() {
        setEventId(UUID.randomUUID().toString());
    }

    public VaccinationEvent(@NonNull String eventId, String childId, String childName, long scheduledDate, String vaccinationDate, VaccinationStatus vaccinationStatus,String vaccineName,int vaccinationStatusCode,VaccinationLocationState vaccinationLocationState, String hospitalId, int yearOfEvent, int monthOfEvent, String userId) {
        this.eventId = eventId;
        this.childId = childId;
        this.childName = childName;
        this.scheduledDate = scheduledDate;
        this.vaccinationDate = vaccinationDate;
        this.vaccinationStatusCode = vaccinationStatusCode;
        this.vaccinationStatus = vaccinationStatus;
        this.vaccineName = vaccineName;
        this.vaccinationLocationState = vaccinationLocationState;
        this.hospitalId = hospitalId;
        this.yearOfEvent = yearOfEvent;
        this.monthOfEvent = monthOfEvent;
        this.userId = userId;

    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public VaccinationLocationState getVaccinationLocationState() {
        return vaccinationLocationState;
    }

    public void setVaccinationLocationState(VaccinationLocationState vaccinationLocationState) {
        this.vaccinationLocationState = vaccinationLocationState;
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    @NonNull
    public String getEventId() {
        return eventId;
    }

    public void setEventId(@NonNull String eventId) {
        this.eventId = eventId;
    }

    public long getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(long scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public String getVaccinationDate() {
        return vaccinationDate;
    }

    public void setVaccinationDate(String vaccinationDate) {
        this.vaccinationDate = vaccinationDate;
    }

    public int getVaccinationStatusCode() {
        return vaccinationStatusCode;
    }

    public void setVaccinationStatusCode(int vaccinationStatusCode) {
        this.vaccinationStatusCode = vaccinationStatusCode;
    }

    public VaccinationStatus getVaccinationStatus() {
        return vaccinationStatus;
    }

    public void setVaccinationStatus(VaccinationStatus vaccinationStatus) {
        this.vaccinationStatus = vaccinationStatus;
    }

    public int getMonthOfEvent() {
        return monthOfEvent;
    }

    public void setMonthOfEvent(int monthOfEvent) {
        this.monthOfEvent = monthOfEvent;
    }

    public int getYearOfEvent() {
        return yearOfEvent;
    }

    public void setYearOfEvent(int yearOfEvent) {
        this.yearOfEvent = yearOfEvent;
    }

    public String getVaccineName() {
        return vaccineName;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    @Override
    public int compareTo(@NonNull VaccinationEvent comparestu) {


        Date sd = new Date(this.scheduledDate);
        Date cd = new Date(comparestu.getScheduledDate());

     //   int dateCmp = this.scheduledDate.compareTo(comparestu.getScheduledDate());
      //  Log.i("==Albums==", "dateComp: " + dateCmp);

        return sd.compareTo(cd);


    }
}
