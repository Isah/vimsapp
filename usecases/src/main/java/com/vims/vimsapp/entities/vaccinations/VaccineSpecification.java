package com.vims.vimsapp.entities.vaccinations;

import java.util.Calendar;
import java.util.Date;

public class VaccineSpecification {


    private String vaccineName;
    private int daysGivenAfter;
    private String howGiven;
    private String protectsAgainst;
    private String modeOfAdministration;
    private String siteOfAdministration;





    public String getModeOfAdministration() {
        return modeOfAdministration;
    }

    public void setModeOfAdministration(String modeOfAdministration) {
        this.modeOfAdministration = modeOfAdministration;
    }

    public String getSiteOfAdministration() {
        return siteOfAdministration;
    }

    public void setSiteOfAdministration(String siteOfAdministration) {
        this.siteOfAdministration = siteOfAdministration;
    }

    public int getDaysGivenAfter() {
        return daysGivenAfter;
    }

    public void setDaysGivenAfter(int daysGivenAfter) {
        this.daysGivenAfter = daysGivenAfter;
    }


    public String getProtectsAgainst() {
        return protectsAgainst;
    }

    public void setProtectsAgainst(String protectsAgainst) {
        this.protectsAgainst = protectsAgainst;
    }

    public String getVaccineName() {
        return vaccineName;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }


}
