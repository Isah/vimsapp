package com.vims.vimsapp.entities.child;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


import java.util.List;
import java.util.UUID;

/**
 * Created by root on 5/24/18.
 */
//1. convert data model to a Room Entity
//2. date key is a unique key
@Entity(tableName = "caretaker", indices = {@Index(value = {"phoneNo"}, unique = true)})
public class Caretaker implements Comparable<Caretaker>{

    //Retrofit The @SerializedName annotation is needed for Gson to map the JSON keys to Java object fields.
    //2. To auto generate primary key in table

    @PrimaryKey // (autoGenerate = true)
    @NonNull
    private String caretakerId;

    private String hospitalId;

    //@SerializedName("firstName")
    //@Expose
    private String firstName;

    //@SerializedName("lastName")
    //@Expose
    private String lastName;

    //@SerializedName("birthdate")
    //@Expose
    private String birthDate;

    //@SerializedName("genderRole")
    //@Expose
    private String genderRole;

   // @SerializedName("district")
   // @Expose
    private String district;

   // @SerializedName("village")
   // @Expose
    private String village;

   // @SerializedName("parish")
   // @Expose
    private String parish;

   // @SerializedName("phoneno1")
   // @Expose
    private String phoneNo;

    private String profileImagePath;

    private Long registrationDate;


    @Ignore
    private List<Child> children;


    @Ignore
    public Caretaker() {

        setCaretakerId(UUID.randomUUID().toString());
    }


    @Ignore
    public Caretaker(@NonNull String caretakerId, String hospitalId, String firstName, String lastName, String birthDate, String genderRole, String district, String village, String parish, String phoneNo, String profileImagePath, Long registrationDate, List<Child> children) {
        this.caretakerId = caretakerId;
        this.hospitalId = hospitalId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.genderRole = genderRole;
        this.district = district;
        this.village = village;
        this.parish = parish;
        this.phoneNo = phoneNo;
        this.profileImagePath = profileImagePath;
        this.registrationDate = registrationDate;
        this.children = children;
    }




    public Caretaker(@NonNull String caretakerId, String hospitalId, String firstName, String lastName, String birthDate, String genderRole, String district, String village, String parish, String phoneNo,  String profileImagePath, Long registrationDate) {
        this.caretakerId = caretakerId;
        this.hospitalId = hospitalId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.genderRole = genderRole;
        this.district = district;
        this.village = village;
        this.parish = parish;
        this.phoneNo = phoneNo;
        this.profileImagePath = profileImagePath;
        this.registrationDate = registrationDate;
    }

    /*
    @Ignore
    public Caretaker(@NonNull String caretakerId, String hospitalId, String firstName, String lastName, String birthDate, String genderRole, String district, String village, String parish, String phoneNo,  String profileImagePath, List<Child> children, Long registrationDate) {
        this.caretakerId = caretakerId;
        this.hospitalId = hospitalId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.genderRole = genderRole;
        this.district = district;
        this.village = village;
        this.parish = parish;
        this.phoneNo = phoneNo;
        this.profileImagePath = profileImagePath;
        this.children = children;
        this.registrationDate = registrationDate;
    }
    */



    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getProfileImagePath() {
        return profileImagePath;
    }

    public void setProfileImagePath(String profileImagePath) {
        this.profileImagePath = profileImagePath;
    }

    public String getGenderRole() {
        return genderRole;
    }

    public void setGenderRole(String genderRole) {
        this.genderRole = genderRole;
    }


    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getParish() {
        return parish;
    }

    public void setParish(String parish) {
        this.parish = parish;
    }


    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }


    @NonNull
    public String getCaretakerId() {
        return caretakerId;
    }

    public void setCaretakerId(@NonNull String caretakerId) {
        this.caretakerId = caretakerId;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }


    public Long getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Long registrationDate) {
        this.registrationDate = registrationDate;
    }


    @Override
    public int compareTo(@NonNull Caretaker comparestu) {


       // Date sd = DateCalendarConverter.stringToDateWithoutTime(this.registrationDate);
       // Date cd = DateCalendarConverter.stringToDateWithoutTime(comparestu.getRegistrationDate();

        Long sd = this.registrationDate;
        Long cd = comparestu.getRegistrationDate();


        //   int dateCmp = this.scheduledDate.compareTo(comparestu.getScheduledDate());
        //  Log.i("==Albums==", "dateComp: " + dateCmp);

        return cd.compareTo(sd);


    }


}
