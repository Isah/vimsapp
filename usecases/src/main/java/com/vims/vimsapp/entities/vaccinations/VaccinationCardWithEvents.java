package com.vims.vimsapp.entities.vaccinations;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import java.util.List;

/**
 * Created by root on 6/24/18.
 */

public class VaccinationCardWithEvents {

        @Embedded
        public VaccinationCard card;

        @Relation(parentColumn = "cardId", entityColumn = "cardId")
        public List<VaccinationEvent> vaccinationEvents;

}
