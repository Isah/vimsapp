package com.vims.vimsapp.entities.child;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by root on 6/24/18.
 */

@Entity(tableName = "sync_update", indices = {@Index(value = {"caretakerId"}, unique = true)})

public class SyncUpdateTime {

    @PrimaryKey
    @NonNull
    private String caretakerId;
    private long eventUpdateTime;

    public String getCaretakerId() {
        return caretakerId;
    }

    public void setCaretakerId(String caretakerId) {
        this.caretakerId = caretakerId;
    }

    public long getEventUpdateTime() {
        return eventUpdateTime;
    }

    public void setEventUpdateTime(long eventUpdateTime) {
        this.eventUpdateTime = eventUpdateTime;
    }


}
