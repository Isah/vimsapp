package com.vims.vimsapp.entities.child;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;

import java.util.List;
import java.util.UUID;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by root on 5/24/18.
 */

//@Entity(tableName = "child", indices = {@Index(value = {"age"}, unique = true)})

@Entity(tableName = "child",indices = {@Index(value = {"caretakerId"})}, foreignKeys = @ForeignKey(entity = Caretaker.class,
        parentColumns = "caretakerId",
        childColumns = "caretakerId",
        onDelete = CASCADE))

public class Child {

    @PrimaryKey //(autoGenerate = true)
    @NonNull
    private String childId;
    private String hospitalId;
    private String caretakerId;
    private String firstName;
    private String lastName;
    private String age;
    private float weight;
    private String dateOfBirth;
    private String gender;
    private String registrationDate;


    private List<VaccinationEvent> vaccinationEvents;
    //Map<String, VaccinationEvent> vaccinationEvents;


    @Ignore
    public Child() {
        setChildId(UUID.randomUUID().toString());
    }



    public Child(@NonNull String childId, String caretakerId, String firstName, String lastName, String age,float weight, String dateOfBirth, String gender, String registrationDate, List<VaccinationEvent> vaccinationEvents) {
        this.childId = childId;
        this.caretakerId = caretakerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.weight = weight;
        this.age = age;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.registrationDate = registrationDate;
        this.vaccinationEvents = vaccinationEvents;
    }


    public List<VaccinationEvent> getVaccinationEvents() {
        return vaccinationEvents;
    }

    public void setVaccinationEvents(List<VaccinationEvent> vaccinationEvents) {
        this.vaccinationEvents = vaccinationEvents;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @NonNull
    public String getChildId() {
        return childId;
    }

    public void setChildId(@NonNull String childId) {
        this.childId = childId;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCaretakerId() {
        return caretakerId;
    }

    public void setCaretakerId(String caretakerId) {
        this.caretakerId = caretakerId;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }
}

