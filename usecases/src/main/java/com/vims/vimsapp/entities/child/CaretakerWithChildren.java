package com.vims.vimsapp.entities.child;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;

import java.util.List;

/**
 * Created by root on 6/24/18.
 */

public class CaretakerWithChildren {

        @Embedded
        public Caretaker caretaker;

        @Relation(parentColumn = "caretakerId", entityColumn = "caretakerId")
        public List<Child> children;

    public Caretaker getCaretaker() {
        return caretaker;
    }

    public List<Child> getChildren() {
        return children;
    }

}
