package com.vims.vimsapp.entities.schedule;

import java.util.Date;

public class Hospital {


    /* "id": "6936",
              "facility": "Kyeibanga HC II",
              "address": "",
              "sector": "govt",
              "category": "HC II",
              "license": "",
              "contact": "",
              "phone": "",
              "email": "",
              "qualification": "",
              "location": "kitagata subcounty,sheema south,sheema",
              "created_at": "2019-05-12 12:00:00"
              */

    private String hospitalId;//
    private String hospitalName;//
    private String address;//
    private String defaultSchedule;//
    private Date date;//
    private String userName;//
    private String district;//
    private String vimServerId;//
    private boolean deleted;//

    public Hospital() {
    }

    public Hospital(String hospitalId, String hospitalName, String address, String defaultSchedule, Date date, String userName, String district, String vimServerId, boolean deleted) {
        this.hospitalId = hospitalId;
        this.hospitalName = hospitalName;
        this.address = address;
        this.defaultSchedule = defaultSchedule;
        this.date = date;
        this.userName = userName;
        this.district = district;
        this.vimServerId = vimServerId;
        this.deleted = deleted;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDefaultSchedule() {
        return defaultSchedule;
    }

    public void setDefaultSchedule(String defaultSchedule) {
        this.defaultSchedule = defaultSchedule;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getVimServerId() {
        return vimServerId;
    }

    public void setVimServerId(String vimServerId) {
        this.vimServerId = vimServerId;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
