package com.vims.vimsapp.entities.reports;

public class VaccinationStatistic {

    private int totalNoOfVaccinations;
    private int totalNoOfUpcomingVaccinations;
    private int totalNoOfFinishedVaccinations;
    private int totalVaccinationPercentage;

    private String titleMonth;


    public String getTitleMonth() {
        return titleMonth;
    }

    public void setTitleMonth(String titleMonth) {
        this.titleMonth = titleMonth;
    }

    public int getTotalVaccinationPercentage() {
        return totalVaccinationPercentage;
    }

    public void setTotalVaccinationPercentage(int totalVaccinationPercentage) {
        this.totalVaccinationPercentage = totalVaccinationPercentage;
    }

    public int getTotalNoOfVaccinations() {
        return totalNoOfVaccinations;
    }

    public void setTotalNoOfVaccinations(int totalNoOfVaccinations) {
        this.totalNoOfVaccinations = totalNoOfVaccinations;
    }

    public int getTotalNoOfUpcomingVaccinations() {
        return totalNoOfUpcomingVaccinations;
    }

    public void setTotalNoOfUpcomingVaccinations(int totalNoOfUpcomingVaccinations) {
        this.totalNoOfUpcomingVaccinations = totalNoOfUpcomingVaccinations;
    }

    public int getTotalNoOfFinishedVaccinations() {
        return totalNoOfFinishedVaccinations;
    }

    public void setTotalNoOfFinishedVaccinations(int totalNoOfFinishedVaccinations) {
        this.totalNoOfFinishedVaccinations = totalNoOfFinishedVaccinations;
    }
}
