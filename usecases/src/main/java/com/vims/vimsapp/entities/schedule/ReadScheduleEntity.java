package com.vims.vimsapp.entities.schedule;

import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;

import java.util.List;

public class ReadScheduleEntity {


    private List<VaccinationEvent> upcomingEventsOfMonth;
    private List<VaccinationEvent> missedEvents;
    private List<VaccinationEvent> doneEvents;
    private List<VaccinationEvent> dueEvents;
    private int noUpcomingEvents;



    public int getNoUpcomingEvents() {
        return noUpcomingEvents;
    }

    public void setNoUpcomingEvents(int noUpcomingEvents) {
        this.noUpcomingEvents = noUpcomingEvents;
    }

    public List<VaccinationEvent> getDueEvents() {
        return dueEvents;
    }

    public void setDueEvents(List<VaccinationEvent> dueEvents) {
        this.dueEvents = dueEvents;
    }

    public List<VaccinationEvent> getUpcomingEventsOfMonth() {
        return upcomingEventsOfMonth;
    }

    public void setUpcomingEventsOfMonth(List<VaccinationEvent> upcomingEventsOfMonth) {
        this.upcomingEventsOfMonth = upcomingEventsOfMonth;
    }

    public List<VaccinationEvent> getMissedEvents() {
        return missedEvents;
    }

    public void setMissedEvents(List<VaccinationEvent> missedEvents) {
        this.missedEvents = missedEvents;
    }

    public List<VaccinationEvent> getDoneEvents() {
        return doneEvents;
    }

    public void setDoneEvents(List<VaccinationEvent> doneEvents) {
        this.doneEvents = doneEvents;
    }
}
