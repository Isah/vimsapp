package com.vims.vimsapp.entities.reports;

import com.vims.vimsapp.viewcoverage.CoverageTimeLine;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;

import java.util.List;
import java.util.Map;

public class VaccinationEventsPack {


        private int noUpcomingEventsOfyear;
        private int noMissedEventsOfyear;

        private int noUpcomingEventsOfCurrentMonth;
        private int noMissedEventsOfCurrentMonth;

        private Map<Integer,Integer> immunisedCHildren;
        private int noChildrenMissedImmunisation;

        private List<CoverageTimeLine> coverageTimeLines;


        private List<VaccinationEvent> doneEvents;
        private List<VaccinationEvent> upComingEventsOfyear;
        private List<VaccinationEvent> doneEventsOfYear;
        private List<VaccinationEvent> doneAllVaccinesEventsOfYear;
        private List<VaccinationEvent> notDone;
        private List<VaccinationEvent> allEvents;
        private AnnualCoverage annualCoverage;
        private List<AnnualCoverage> annualCoveragesTimeLines;
        private MonthTargetPopulation monthTargetPopulation;




    public int getNoChildrenMissedImmunisation() {
        return noChildrenMissedImmunisation;
    }

    public void setNoChildrenMissedImmunisation(int noChildrenMissedImmunisation) {
        this.noChildrenMissedImmunisation = noChildrenMissedImmunisation;
    }

    public int getNoUpcomingEventsOfCurrentMonth() {
        return noUpcomingEventsOfCurrentMonth;
    }

    public void setNoUpcomingEventsOfCurrentMonth(int noUpcomingEventsOfCurrentMonth) {
        this.noUpcomingEventsOfCurrentMonth = noUpcomingEventsOfCurrentMonth;
    }

    public int getNoMissedEventsOfCurrentMonth() {
        return noMissedEventsOfCurrentMonth;
    }

    public void setNoMissedEventsOfCurrentMonth(int noMissedEventsOfCurrentMonth) {
        this.noMissedEventsOfCurrentMonth = noMissedEventsOfCurrentMonth;
    }

    public List<CoverageTimeLine> getCoverageTimeLines() {
        return coverageTimeLines;
    }

    public void setCoverageTimeLines(List<CoverageTimeLine> coverageTimeLines) {
        this.coverageTimeLines = coverageTimeLines;
    }

    public Map<Integer, Integer> getImmunisedCHildren() {
        return immunisedCHildren;
    }

    public void setImmunisedCHildren(Map<Integer, Integer> immunisedCHildren) {
        this.immunisedCHildren = immunisedCHildren;
    }

    public int getNoUpcomingEventsOfyear() {
        return noUpcomingEventsOfyear;
    }

    public void setNoUpcomingEventsOfyear(int noUpcomingEventsOfyear) {
        this.noUpcomingEventsOfyear = noUpcomingEventsOfyear;
    }

    public int getNoMissedEventsOfyear() {
        return noMissedEventsOfyear;
    }

    public void setNoMissedEventsOfyear(int noMissedEventsOfyear) {
        this.noMissedEventsOfyear = noMissedEventsOfyear;
    }

    public List<VaccinationEvent> getUpComingEventsOfyear() {
        return upComingEventsOfyear;
    }

    public void setUpComingEventsOfyear(List<VaccinationEvent> upComingEventsOfyear) {
        this.upComingEventsOfyear = upComingEventsOfyear;
    }

    public List<VaccinationEvent> getDoneAllVaccinesEventsOfYear() {
        return doneAllVaccinesEventsOfYear;
    }

    public void setDoneAllVaccinesEventsOfYear(List<VaccinationEvent> doneAllVaccinesEventsOfYear) {
        this.doneAllVaccinesEventsOfYear = doneAllVaccinesEventsOfYear;
    }

    public List<AnnualCoverage> getAnnualCoveragesTimeLines() {
        return annualCoveragesTimeLines;
    }

    public void setAnnualCoveragesTimeLines(List<AnnualCoverage> annualCoveragesTimeLines) {
        this.annualCoveragesTimeLines = annualCoveragesTimeLines;
    }

    public AnnualCoverage getAnnualCoverage() {
        return annualCoverage;
    }

    public void setAnnualCoverage(AnnualCoverage annualCoverage) {
        this.annualCoverage = annualCoverage;
    }

    public List<VaccinationEvent> getDoneEventsOfYear() {
        return doneEventsOfYear;
    }

    public void setDoneEventsOfYear(List<VaccinationEvent> doneEventsOfYear) {
        this.doneEventsOfYear = doneEventsOfYear;
    }

    public List<VaccinationEvent> getDoneEvents() {
            return doneEvents;
        }

        public void setDoneEvents(List<VaccinationEvent> doneEvents) {
            this.doneEvents = doneEvents;
        }

        public List<VaccinationEvent> getNotDone() {
            return notDone;
        }

        public void setNotDone(List<VaccinationEvent> notDone) {
            this.notDone = notDone;
        }

    public List<VaccinationEvent> getAllEvents() {
        return allEvents;
    }

    public void setAllEvents(List<VaccinationEvent> allEvents) {
        this.allEvents = allEvents;
    }


    public MonthTargetPopulation getMonthTargetPopulation() {
        return monthTargetPopulation;
    }

    public void setMonthTargetPopulation(MonthTargetPopulation monthTargetPopulation) {
        this.monthTargetPopulation = monthTargetPopulation;
    }
}
