package com.vims.vimsapp.entities.vaccinations;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.List;
import java.util.UUID;

@Entity(tableName = "vcard", indices = {@Index(value = {"dateCreated"}, unique = true)})
public class VaccinationCard {


    @PrimaryKey // (autoGenerate = true)
    //@Expose
    @NonNull
    private String cardId;

   // @SerializedName("childId")
   // @Expose
    private String childId;

    //@SerializedName("dateCreated")
    //@Expose
    private String dateCreated;


   // @SerializedName("vaccinationEvents")
   // @Expose
    @Ignore
    private List<VaccinationEvent> vaccinationEvents;


    @Ignore
    public VaccinationCard() {
        setCardId(UUID.randomUUID().toString());
    }

    public VaccinationCard(@NonNull String cardId, String childId, String dateCreated) {
        this.cardId = cardId;
        this.childId = childId;
        this.dateCreated = dateCreated;

    }


    public String getCardId() {
        return cardId;
    }

    private void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }


    public List<VaccinationEvent> getVaccinationEvents() {
        return vaccinationEvents;
    }

    public void setVaccinationEvents(List<VaccinationEvent> vaccinationEvents) {
        this.vaccinationEvents = vaccinationEvents;
    }
}
