package com.vims.vimsapp.entities.vaccinations;

public enum VaccinationStatus {

        DONE,
        FAILED,
        NOT_DONE,
        DUE
}
