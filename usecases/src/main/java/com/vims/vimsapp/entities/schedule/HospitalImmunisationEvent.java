package com.vims.vimsapp.entities.schedule;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;


@Entity(tableName = "hospital_events",indices = {@Index(value = {"eventId"})})
public class HospitalImmunisationEvent {

    @NonNull
    @PrimaryKey // (autoGenerate = true)
    private String eventId;
    private String hospitalId;
    private String eventTitle;
    private String description;
    private String username;
    private String eventDate;



    @Ignore
    public HospitalImmunisationEvent() {
    }

    public HospitalImmunisationEvent(@NonNull String eventId ,  String hospitalId, String description, String username, String eventDate, String eventTitle) {
        this.hospitalId = hospitalId;
        this.description = description;
        this.username = username;
        this.eventDate = eventDate;
        this.eventTitle = eventTitle;
        this.eventId = eventId;
    }


    @NonNull
    public String getEventId() {
        return eventId;
    }

    public void setEventId(@NonNull String eventId) {
        this.eventId = eventId;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

}
