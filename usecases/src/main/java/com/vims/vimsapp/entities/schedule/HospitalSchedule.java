package com.vims.vimsapp.entities.schedule;


import java.util.List;

public class HospitalSchedule {

    HospitalImmunisationEvent event;
    List<HospitalImmunisationEvent> workingDays;


    public HospitalImmunisationEvent getEvent() {
        return event;
    }

    public void setEvent(HospitalImmunisationEvent event) {
        this.event = event;
    }


    public void setWorkingDays(List<HospitalImmunisationEvent> workingDays) {
        this.workingDays = workingDays;
    }

    public List<HospitalImmunisationEvent> getWorkingDays() {
        return workingDays;
    }




}
