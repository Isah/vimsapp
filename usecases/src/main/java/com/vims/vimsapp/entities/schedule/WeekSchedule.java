package com.vims.vimsapp.entities.schedule;

import java.util.ArrayList;
import java.util.List;

public class WeekSchedule {

    private  int weekOfMonth;
    private int dayOfWeek;
    private boolean isSelected;
    private String dayOfWeekName;

    private List<WeekRepeat> weekRepeats;


    public List<WeekRepeat> getWeekRepeats() {

        return weekRepeats;
    }

    public void setWeekRepeats(List<WeekRepeat> weekRepeats) {
        this.weekRepeats = weekRepeats;
    }

    public String getDayOfWeekName() {
        return dayOfWeekName;
    }

    public void setDayOfWeekName(String dayOfWeekName) {
        this.dayOfWeekName = dayOfWeekName;
    }

    public WeekSchedule() {
        this.isSelected = false;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public int getWeekOfMonth() {
        return weekOfMonth;
    }

    public void setWeekOfMonth(int week) {
        this.weekOfMonth = week;
    }










}
