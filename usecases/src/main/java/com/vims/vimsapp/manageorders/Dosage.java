package com.vims.vimsapp.manageorders;

public class Dosage {

    private String vaccine;
    private int dosageNeededNextMonth;
    private int dosageUsedThisMonth;


    public String getVaccine() {
        return vaccine;
    }

    public void setVaccine(String vaccine) {
        this.vaccine = vaccine;
    }

    public int getDosageNeededNextMonth() {
        return dosageNeededNextMonth;
    }

    public void setDosageNeededNextMonth(int dosageNeededNextMonth) {
        this.dosageNeededNextMonth = dosageNeededNextMonth;
    }

    public int getDosageUsedThisMonth() {
        return dosageUsedThisMonth;
    }

    public void setDosageUsedThisMonth(int dosageUsedThisMonth) {
        this.dosageUsedThisMonth = dosageUsedThisMonth;
    }
}
