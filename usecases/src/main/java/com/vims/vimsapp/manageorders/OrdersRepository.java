package com.vims.vimsapp.manageorders;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class OrdersRepository implements OrdersDataSource {


    private OrdersDataSource mOrdersRemoteDataSource;
    private OrdersDataSource mOrdersLocalDataSource;

    private static OrdersRepository INSTANCE = null;
    // Prevent direct instantiation.
    private OrdersRepository(OrdersDataSource ordersRemoteDataSource, OrdersDataSource ordersLocalDataSource) {
        mOrdersRemoteDataSource = checkNotNull(ordersRemoteDataSource);
        mOrdersLocalDataSource = checkNotNull(ordersLocalDataSource);

    }



    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param ordersRemoteDataSource the backend data source
     * @param ordersLocalDataSource  the device storage data source
     * @return the {@link OrdersRepository} instance
     */
    public static OrdersRepository getInstance(OrdersDataSource ordersRemoteDataSource, OrdersDataSource ordersLocalDataSource) {

        if (INSTANCE == null) {
            INSTANCE = new OrdersRepository(ordersRemoteDataSource, ordersLocalDataSource);
        }
        return INSTANCE;
    }



    @Override
    public void getAllOrders(LoadAllOrdersCallback ordersCallback) {
        mOrdersLocalDataSource.getAllOrders(ordersCallback);
    }

    @Override
    public void getOrder(String dateOfRequest, LoadOrdersCallback ordersCallback) {

        mOrdersLocalDataSource.getOrder(dateOfRequest,ordersCallback);

    }

    @Override
    public void isOrderAvailable(String dateOfRequest, isOrderAvailableCallback orderCallback) {

       mOrdersLocalDataSource.isOrderAvailable(dateOfRequest,orderCallback);

    }

    @Override
    public void saveVaccineOrder(final VaccineOrder vaccineOrder, final SaveOrderCallback saveOrderCallback) {

        mOrdersRemoteDataSource.saveVaccineOrder(vaccineOrder, new SaveOrderCallback() {
            @Override
            public void onOrderSaved(VaccineOrder vaccineOrder) {

                mOrdersLocalDataSource.saveVaccineOrder(vaccineOrder, new SaveOrderCallback() {
                    @Override
                    public void onOrderSaved(VaccineOrder vaccineOrder) {


                        saveOrderCallback.onOrderSaved(vaccineOrder);

                    }

                    @Override
                    public void onFailed(String error) {

                        saveOrderCallback.onFailed(error);

                    }
                });


            }

            @Override
            public void onFailed(String error) {

                mOrdersLocalDataSource.saveVaccineOrder(vaccineOrder, new SaveOrderCallback() {
                    @Override
                    public void onOrderSaved(VaccineOrder vaccineOrder) {


                        saveOrderCallback.onOrderSaved(vaccineOrder);

                    }

                    @Override
                    public void onFailed(String error) {

                        saveOrderCallback.onFailed(error);

                    }
                });

             //   saveOrderCallback.onFailed(error);


            }
        });



    }







}
