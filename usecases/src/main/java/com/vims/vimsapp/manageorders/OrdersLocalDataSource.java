package com.vims.vimsapp.manageorders;

import android.support.annotation.NonNull;
import android.util.Log;

import com.vims.vimsapp.utilities.AppExecutors;

import java.util.List;

public class OrdersLocalDataSource implements OrdersDataSource {



    private static volatile OrdersLocalDataSource INSTANCE;



    private VaccineOrderDao mVaccineOrderDao;
    private AppExecutors mAppExecutors;



    //caretakerDao = PatientsDatabase.getInstance(context).getCaretakerDao();
    //childDao = PatientsDatabase.getInstance(context).getChildDao();

    // Prevent direct instantiation.
    private OrdersLocalDataSource(@NonNull AppExecutors appExecutors, VaccineOrderDao  vaccineOrderDao) {
        mAppExecutors = appExecutors;
        mVaccineOrderDao = vaccineOrderDao;


    }


    public static OrdersLocalDataSource getInstance(@NonNull AppExecutors appExecutors,VaccineOrderDao  vaccineOrderDao) {
        if (INSTANCE == null) {
            synchronized (OrdersLocalDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new OrdersLocalDataSource(appExecutors,vaccineOrderDao);
                }
            }
        }
        return INSTANCE;
    }





    @Override
    public void getAllOrders(final LoadAllOrdersCallback ordersCallback) {


        Runnable runnable = new Runnable() {
            @Override
            public void run() {


                final List<VaccineOrder> vaccineOrders = mVaccineOrderDao.getAllOrdersLtd(5);

                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {


                        if(!vaccineOrders.isEmpty()) {

                            ordersCallback.onOrdersLoaded(vaccineOrders);
                        }else {
                            Log.d("OrderMonth","theres no order");
                            ordersCallback.onNotAvailable("not available");
                        }

                    }
                });



            }
        };

        mAppExecutors.diskIO().execute(runnable);


    }

    @Override
    public void getOrder(final String dateOfRequest, final LoadOrdersCallback ordersCallback) {

        //we've changed to get an order by id



        Runnable runnable = new Runnable() {
            @Override
            public void run() {


                final VaccineOrder vaccineOrder = mVaccineOrderDao.getOrderById(dateOfRequest);

                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {


                        if(vaccineOrder!=null) {
                            Log.d("OrderMonth","theres an order");

                            ordersCallback.onOrdersLoaded(vaccineOrder);
                        }else {
                            Log.d("OrderMonth","theres no order");
                            ordersCallback.onNotAvailable("not available");
                        }

                    }
                });



            }
        };

        mAppExecutors.diskIO().execute(runnable);




    }

    @Override
    public void isOrderAvailable(final String dateOfRequest,final isOrderAvailableCallback orderCallback) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                final int orderCount  =  mVaccineOrderDao.isOrderAvailableCount(dateOfRequest);

                Log.d("OrderRequest","Date: "+dateOfRequest+" count"+orderCount);

                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (orderCount > 0) {

                            orderCallback.onOrderAvailable();

                        } else {


                            orderCallback.onNotAvailable("No orders found");

                        }
                    }
                });

            }
        };
        mAppExecutors.diskIO().execute(runnable);




    }

    @Override
    public void saveVaccineOrder(final VaccineOrder vaccineOrder, final SaveOrderCallback saveOrderCallback) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                //  vaccineOrder.setOrderStatus(VaccineOrder.OrderStatus.SENT);

                final long id = mVaccineOrderDao.Insert(vaccineOrder);

                //returning data on mainThread
                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (id > 0) {

                            saveOrderCallback.onOrderSaved(vaccineOrder);


                        } else {

                            saveOrderCallback.onFailed("Error Saving Order locally");

                        }
                    }
                });

            }
        };

        mAppExecutors.diskIO().execute(runnable);

    }






}
