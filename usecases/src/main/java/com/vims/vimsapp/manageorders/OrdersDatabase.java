package com.vims.vimsapp.manageorders;

import android.app.Application;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.util.Log;

import com.vims.vimsapp.utilities.DateConverter;
import com.vims.vimsapp.utilities.ListTypeConverter;
import com.vims.vimsapp.utilities.StatusEnumConverter;

/**
 * Created by root on 6/3/18.
 */


// List of the entry classes and associated TypeConverters
// List of the entry classes and associated TypeConverters
@Database(entities = {VaccineOrder.class}, version = 1, exportSchema = false)
@TypeConverters({DateConverter.class, StatusEnumConverter.class, ListTypeConverter.class})
public abstract class OrdersDatabase extends RoomDatabase{


    private static final String LOG_TAG = OrdersDatabase.class.getSimpleName();
    private static final String DATABASE_NAME = "orders";

    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static OrdersDatabase sInstance;
    //.allowMainThreadQueries()
    public static OrdersDatabase getInstance(Application application) {
        Log.d(LOG_TAG, "Getting the database");
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = Room.databaseBuilder(application,
                        OrdersDatabase.class, OrdersDatabase.DATABASE_NAME)

                        .fallbackToDestructiveMigration()
                        .build();
                Log.d(LOG_TAG, "Made new database");
            }
        }
        return sInstance;
    }



    // The associated DAOs for the database

    public abstract VaccineOrderDao getVaccineOrderDao();






}
