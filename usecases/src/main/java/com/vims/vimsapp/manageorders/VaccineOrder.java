package com.vims.vimsapp.manageorders;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.List;
import java.util.UUID;

@Entity(tableName = "vaccine_order")
public class VaccineOrder {

    @PrimaryKey
    @NonNull
    private String orderId;
    private String hospitalId;
    private String hospitalName;
    private String userName;
    private String district;
    private boolean status;
    private String orderDate;

    private List<VaccinePlan> vaccinePlans;


    @Ignore
    public VaccineOrder() {
        setOrderId(UUID.randomUUID().toString());
    }


    public VaccineOrder(@NonNull String orderId, String hospitalId, String hospitalName, String userName, String district, boolean status, String orderDate, List<VaccinePlan> vaccinePlans) {
        this.orderId = orderId;
        this.hospitalId = hospitalId;
        this.hospitalName = hospitalName;
        this.userName = userName;
        this.district = district;
        this.status = status;
        this.orderDate = orderDate;
        this.vaccinePlans = vaccinePlans;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getOrderId() {
        return orderId;

    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }


    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }


    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public List<VaccinePlan> getVaccinePlans() {
        return vaccinePlans;
    }

    public void setVaccinePlans(List<VaccinePlan> vaccinePlans) {
        this.vaccinePlans = vaccinePlans;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


}
