package com.vims.vimsapp.manageorders;

import android.support.annotation.NonNull;
import android.view.View;

import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsSchedule;
import com.vims.vimsapp.utilities.DateCalendarConverter;

import java.util.Calendar;
import java.util.List;

public class ViewOrders implements UseCase<ViewOrders.RequestValues,ViewOrders.ResponseValue>{




    private static ViewOrders INSTANCE = null;


    OrdersRepository vaccinationsRepository;
    SearchVaccinationsSchedule searchVaccinationsSchedule;

    private ViewOrders(OrdersRepository vaccinationsRepository,SearchVaccinationsSchedule searchVaccinationsSchedule){
        this.vaccinationsRepository = vaccinationsRepository;
        this.searchVaccinationsSchedule = searchVaccinationsSchedule;
    }

    public static ViewOrders getInstance(OrdersRepository vaccinationsRepository,SearchVaccinationsSchedule searchVaccinationsSchedule) {

        if (INSTANCE == null) {
            INSTANCE = new ViewOrders(vaccinationsRepository,searchVaccinationsSchedule);
        }
        return INSTANCE;
    }


    @Override
    public void execute(final RequestValues param, final OutputBoundary<ResponseValue> delivery) {


        boolean isShowList = param.isListOfOrders();

        if(isShowList){


            vaccinationsRepository.getAllOrders(new OrdersDataSource.LoadAllOrdersCallback() {
                @Override
                public void onOrdersLoaded(List<VaccineOrder> vaccineOrders) {

                    delivery.onSuccess(new ResponseValue(vaccineOrders));


                }

                @Override
                public void onNotAvailable(String error) {

                   delivery.onError(error);


                }
            });



        }else {
            /**
             * 1.  view a submitted order with an order id
             * 2.  to submit an order (get a list of doses to form an order)
             * **/


            boolean isSubmitOrder = param.isSubmitOrder();

            if(!isSubmitOrder) { // To view a submited order
                vaccinationsRepository.getOrder(param.getOrderId(), new OrdersDataSource.LoadOrdersCallback() {
                    @Override
                    public void onOrdersLoaded(VaccineOrder vaccineOrder) {
                        ResponseValue responseValue = new ResponseValue(null);
                        responseValue.setVaccineOrder(vaccineOrder);

                        delivery.onSuccess(responseValue);
                    }

                    @Override
                    public void onNotAvailable(String error) {
                        delivery.onError(error);
                    }
                });
            }else { //To submit order

                int ordermonth = param.getOrderMonth();
                generateDosesforSubmission(ordermonth,delivery);


            }



        }


    }




    private void generateDosesforSubmission(final int ordermonth, final OutputBoundary<ResponseValue> delivery){

        SearchVaccinationsSchedule.RequestValues requestValues = new SearchVaccinationsSchedule.RequestValues(SearchVaccinationsSchedule.VaccinationsRequestType.DOSAGE);
        requestValues.setOrderMonth(ordermonth);

        searchVaccinationsSchedule.execute(requestValues, new OutputBoundary<SearchVaccinationsSchedule.ResponseValue>() {
            @Override
            public void onSuccess(SearchVaccinationsSchedule.ResponseValue result) {


                String month = DateCalendarConverter.monthIntToString(ordermonth);
                Calendar calendar = Calendar.getInstance();
                int yr = calendar.get(Calendar.YEAR);
                //int yr = calendar.get(Calendar.YEAR);
                String msg = ""+month+", "+yr;
                String ordering = "Order List . "+msg;


                ResponseValue responseValue = new ResponseValue(null);
                responseValue.setDosageList(result.getDosageList());
                responseValue.setDosageDate(ordering);


                Calendar cal = Calendar.getInstance();
                int lastDayOfMonth = cal.getActualMaximum(Calendar.DATE);
                int dayOfmonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                int daysTogo = lastDayOfMonth - dayOfmonth;


                responseValue.setExpiryDaysForVaccination(daysTogo);

                delivery.onSuccess(responseValue);

            }

            @Override
            public void onError(String errorMessage) {

            }
        });


    }

    public static final class RequestValues implements UseCase.RequestValues {

        private final  boolean isListOfOrders;
        private String orderId;
        private int orderMonth;
        private boolean isSubmitOrder;




        public RequestValues(@NonNull boolean isListOfOrders) {

            this.isListOfOrders = isListOfOrders;
        }


        public boolean isSubmitOrder() {
            return isSubmitOrder;
        }

        public void setSubmitOrder(boolean submitOrder) {
            isSubmitOrder = submitOrder;
        }

        public int getOrderMonth() {
            return orderMonth;
        }

        public void setOrderMonth(int orderMonth) {
            this.orderMonth = orderMonth;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public boolean isListOfOrders() {
            return isListOfOrders;
        }
    }


    public static final class ResponseValue implements UseCase.ResponseValue {

        private List<VaccineOrder> vaccineOrders;
        private VaccineOrder vaccineOrder;

        private List<Dosage> dosageList;
        private String dosageDate;
        private int expiryDaysForVaccination;


        public ResponseValue(@NonNull List<VaccineOrder> vaccineOrders) {
            this.vaccineOrders = vaccineOrders;
        }


        public List<Dosage> getDosageList() {
            return dosageList;
        }

        public void setDosageList(List<Dosage> dosageList) {
            this.dosageList = dosageList;
        }

        public int getExpiryDaysForVaccination() {
            return expiryDaysForVaccination;
        }

        public void setExpiryDaysForVaccination(int expiryDateForVaccination) {
            this.expiryDaysForVaccination = expiryDateForVaccination;
        }

        public String getDosageDate() {
            return dosageDate;
        }

        public void setDosageDate(String dosageDate) {
            this.dosageDate = dosageDate;
        }

        public void setVaccineOrder(VaccineOrder vaccineOrder) {
            this.vaccineOrder = vaccineOrder;
        }

        public List<VaccineOrder> getVaccineOrders() {
            return vaccineOrders;
        }

        public VaccineOrder getVaccineOrder() {
            return vaccineOrder;
        }
    }


}
