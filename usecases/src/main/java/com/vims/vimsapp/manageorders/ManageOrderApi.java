package com.vims.vimsapp.manageorders;

import com.vims.vimsapp.entities.schedule.Hospital;
import com.vims.vimsapp.entities.schedule.HospitalImmunisationEvent;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by root on 6/5/18.
 */

public interface ManageOrderApi {



        /** vims officail api **/

        @POST("inventoryAPI/mobile/makeVaccineOrder")
        Call<VaccineOrder> orderVaccine(@Header("Authorization") String authKey, @Body VaccineOrder vaccineOrder);

        /** end of vims officail api **/




}
