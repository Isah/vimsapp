package com.vims.vimsapp.manageorders;

/**
 * Created by root on 6/24/18.
 */


import java.util.List;

/**
 * Main entry point for accessing tasks data.
 * <p>
 * For simplicity, only getTasks() and getTask() have callbacks. Consider adding callbacks to other
 * methods to inform the user of network/database errors or successful operations.
 * For example, when a new task is created, it's synchronously stored in cache but usually every
 * operation on database or network should be executed in a different thread.
 */

interface OrdersDataSource {


    interface SaveOrderCallback {

        void onOrderSaved(VaccineOrder vaccineOrder);

        void onFailed(String error);
    }



    //SAVING-------------------  the guy that implements the callback gets the values
   // void saveChildVaccinationCard(VaccinationCard vaccinationCard, SaveChildVaccinationCardCallback saveChildVaccinationCardCallback);


    void saveVaccineOrder(VaccineOrder vaccineOrder, SaveOrderCallback saveOrderCallback);



    interface isOrderAvailableCallback {

        void onOrderAvailable();
        void onNotAvailable(String error);
    }

    void isOrderAvailable(String dateOfRequest, isOrderAvailableCallback orderCallback);



    interface LoadOrdersCallback {

        void onOrdersLoaded(VaccineOrder vaccineOrder);
        void onNotAvailable(String error);
    }

    void getOrder(String dateOfRequest, LoadOrdersCallback ordersCallback);


    interface LoadAllOrdersCallback {

        void onOrdersLoaded(List<VaccineOrder> vaccineOrders);
        void onNotAvailable(String error);
    }

    void getAllOrders(LoadAllOrdersCallback ordersCallback);





}
