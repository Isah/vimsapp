package com.vims.vimsapp.manageorders;

import android.app.Application;
import android.support.annotation.NonNull;
import android.util.Base64;

import com.vims.vimsapp.utilities.PreferenceHandler;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersRemoteDataSource implements OrdersDataSource{


    private ManageOrderApi vaccinationsApi;


    private static volatile OrdersRemoteDataSource INSTANCE;


    private PreferenceHandler preferenceHandler;

    // Prevent direct instantiation.
    private OrdersRemoteDataSource(@NonNull ManageOrderApi vaccinationsApi, Application application) {
        this.vaccinationsApi = vaccinationsApi;
        preferenceHandler = PreferenceHandler.getInstance(application);
    }

    public static OrdersRemoteDataSource getInstance(@NonNull ManageOrderApi vaccinationsApi, Application application) {
        if (INSTANCE == null) {
            synchronized (OrdersRemoteDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new OrdersRemoteDataSource(vaccinationsApi,application);
                }
            }
        }
        return INSTANCE;
    }



    private String API_USER_ID;
    private String API_PASSWORD;
    private String API_TOKEN;

    public  String getAuthToken() {

        //API_USER_ID = "petr"; //used for testing

        API_USER_ID = preferenceHandler.getPref(PreferenceHandler.USER_ID);
        API_TOKEN = preferenceHandler.getPref(PreferenceHandler.USER_TOKEN);

        byte[] data = new byte[0];
        try {
            data = (API_USER_ID + ":" + API_TOKEN).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }



    @Override
    public void getAllOrders(LoadAllOrdersCallback ordersCallback) {

    }

    @Override
    public void getOrder(String dateOfRequest, LoadOrdersCallback ordersCallback) {

    }

    @Override
    public void isOrderAvailable(String dateOfRequest, isOrderAvailableCallback orderCallback) {

    }

    @Override
    public void saveVaccineOrder(final VaccineOrder vaccineOrder, final SaveOrderCallback saveOrderCallback) {


        Call<VaccineOrder> callSync = vaccinationsApi.orderVaccine(getAuthToken(),vaccineOrder);

        callSync.enqueue(new Callback<VaccineOrder>() {
            @Override
            public void onResponse(@NonNull Call<VaccineOrder> call, @NonNull Response<VaccineOrder> response) {

                if(response.isSuccessful()) {
                    saveOrderCallback.onOrderSaved(vaccineOrder);
                    //preferenceHandler.putPref(PreferenceHandler.ORDER_REQUESTED_DATE,vaccineOrder.getOrderDate());
                }
                else if(response.code() == 401){

                    saveOrderCallback.onFailed("UnAuthorized, Check Login Details!");
                }
                else{

                    saveOrderCallback.onFailed("server! "+response.code()+ " "+response.message());
                }


            }

            @Override
            public void onFailure(@NonNull Call<VaccineOrder> call, @NonNull Throwable t) {

                String msg = "Can't connect to internet!";
                saveOrderCallback.onFailed(msg);


            }
        });




    }



}
