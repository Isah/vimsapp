package com.vims.vimsapp.manageorders;

import java.util.UUID;

public class VaccinePlan {


    private String planId;
    private String vaccineName;
    private int quantityAtHand;
    private int quantityRequired;
    private int quantityUsed;


    public VaccinePlan() {
        setPlanId(UUID.randomUUID().toString());
    }

    public VaccinePlan(String planId, String vaccineName, int quantityAtHand, int quantityRequired, int quantityUsed) {
        this.vaccineName = vaccineName;
        this.quantityAtHand = quantityAtHand;
        this.quantityRequired = quantityRequired;
        this.quantityUsed = quantityUsed;
        this.planId = planId;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public int getQuantityUsed() {
        return quantityUsed;
    }

    public void setQuantityUsed(int quantityUsed) {
        this.quantityUsed = quantityUsed;
    }

    public String getVaccineName() {
        return vaccineName;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    public int getQuantityAtHand() {
        return quantityAtHand;
    }

    public void setQuantityAtHand(int quantityAtHand) {
        this.quantityAtHand = quantityAtHand;
    }

    public int getQuantityRequired() {
        return quantityRequired;
    }

    public void setQuantityRequired(int quantityRequired) {
        this.quantityRequired = quantityRequired;
    }
}
