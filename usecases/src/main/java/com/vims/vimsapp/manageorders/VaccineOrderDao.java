package com.vims.vimsapp.manageorders;;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;


import java.util.List;


//1. annotate your Dao
@Dao
public interface VaccineOrderDao {



        @Insert(onConflict = OnConflictStrategy.REPLACE)
        long Insert(VaccineOrder vaccineOrder);


        @Query("SELECT * FROM vaccine_order")
        VaccineOrder getAllOrders();

        @Query("SELECT * FROM vaccine_order ORDER BY orderDate DESC limit :limit ")
        List<VaccineOrder> getAllOrdersLtd(int limit);



        @Query("SELECT * FROM vaccine_order WHERE orderId = :orderId")
        VaccineOrder getOrderById(String orderId);


        @Query("SELECT * FROM vaccine_order WHERE orderDate = :orderDate")
        VaccineOrder getOrderByDate(String orderDate);


        @Query("SELECT count(*) FROM vaccine_order WHERE orderDate = :orderDate")
        int isOrderAvailableCount(String orderDate);



        @Query("DELETE FROM vaccine_order")
        void deleteOrders();


}


