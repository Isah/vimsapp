package com.vims.vimsapp.manageorders;

import android.support.annotation.NonNull;

import com.vims.vimsapp.UseCase;

import com.vims.vimsapp.utilities.DateCalendarConverter;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RequestOrder implements UseCase<RequestOrder.RequestValues, RequestOrder.ResponseValue>{


    private static RequestOrder INSTANCE = null;


    OrdersRepository ordersRepository;

    private RequestOrder(OrdersRepository ordersRepository){
        this.ordersRepository = ordersRepository;
    }

    public static RequestOrder getInstance(OrdersRepository vaccinationsRepository) {

        if (INSTANCE == null) {
            INSTANCE = new RequestOrder(vaccinationsRepository);
        }
        return INSTANCE;
    }


    @Override
    public void execute(RequestValues param, final OutputBoundary<ResponseValue> delivery) {


           String hospitalId = param.getHospitalId();

           VaccineOrder vaccineOrder = new VaccineOrder();
           vaccineOrder.setUserName(param.getUserName());
           vaccineOrder.setHospitalName(param.getHospitalName());
           vaccineOrder.setDistrict(param.getHospitalAddress());
           vaccineOrder.setVaccinePlans(param.getVaccinePlanList());
           vaccineOrder.setHospitalId(hospitalId);
           vaccineOrder.setOrderDate(DateCalendarConverter.dateToStringWithoutTime(new Date()));

          // Log.d("OrderDate","in: "+vaccineOrder.getOrderDate());


        String errorFuture = "Order can't be made this month";
        String errorEndOfMonth = "Orders can only be submitted in the last week of month";


        int monthOfOrderGiven = param.getMonthOfOrderGiven();



        if(isOrderFuture(monthOfOrderGiven)){

            delivery.onError(errorFuture);

        }
        else if(!isOrderInLastDaysOfMonth()){

            delivery.onError(errorEndOfMonth);

        }
        else {


            sendOrderIfNeverSent(vaccineOrder,delivery);

        }


    }



    private void sendOrderIfNeverSent(final VaccineOrder vaccineOrder, final OutputBoundary<ResponseValue> delivery){

        ordersRepository.isOrderAvailable(vaccineOrder.getOrderDate(), new OrdersDataSource.isOrderAvailableCallback() {
            @Override
            public void onOrderAvailable() {

                String errorManyOrdersAMonth = "Order already placed this month!";
                delivery.onError(errorManyOrdersAMonth);

            }

            @Override
            public void onNotAvailable(String error) {


                ordersRepository.saveVaccineOrder(vaccineOrder,new OrdersDataSource.SaveOrderCallback() {
                    @Override
                    public void onOrderSaved(VaccineOrder vaccineOrder) {

                        ResponseValue responseValue = new ResponseValue(vaccineOrder);
                        delivery.onSuccess(responseValue);

                    }

                    @Override
                    public void onFailed(String error) {
                        delivery.onError(error);

                    }
                });


            }
        });

    }


    private boolean isOrderFuture(int monthOfOrderGiven){
        //month of order given = cal time wc starts from 0
        Calendar calendar = Calendar.getInstance();
        int monthOfYear = calendar.get(Calendar.MONTH);
        int monthOrderDiff = monthOfOrderGiven - monthOfYear;

        boolean isFuture = monthOrderDiff != 1;
        //boolean isFuture = monthOrderDiff > 0;
       // Log.d("OrderDifference","diff: "+monthOrderDiff+" m1:"+monthOfOrderGiven+" m2: "+monthOfYear);

        if(isFuture){

            return true;
        }

        return false;

    }



    private boolean isOrderInLastDaysOfMonth(){
        Calendar calendar = Calendar.getInstance();
        int lastDayOfMonth = calendar.getActualMaximum(Calendar.DATE);


        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        //int monthOfYear = calendar.get(Calendar.MONTH);
        int diff = lastDayOfMonth - dayOfMonth;

        boolean isOk = true;//diff < 7;

        if(isOk){

            return true;
        }

        return false;

    }

    public static final class RequestValues implements UseCase.RequestValues {

        private final String hospitalId;
        private final String userName;
        private final List<VaccinePlan> vaccinePlanList;
        private final int monthOfOrderGiven;

        private String hospitalName;
        private String hospitalAddress;


        public RequestValues(@NonNull String hospitalId, String userName,List<VaccinePlan> vaccinePlanList, int monthOfOrderGiven) {

            this.hospitalId = hospitalId;
            this.vaccinePlanList = vaccinePlanList;
            this.userName = userName;
            this.monthOfOrderGiven = monthOfOrderGiven;
        }

        public void setHospitalName(String hospitalName) {
            this.hospitalName = hospitalName;
        }

        public void setHospitalAddress(String hospitalAddress) {
            this.hospitalAddress = hospitalAddress;
        }

        public String getHospitalName() {
            return hospitalName;
        }

        public String getHospitalAddress() {
            return hospitalAddress;
        }

        public int getMonthOfOrderGiven() {
            return monthOfOrderGiven;
        }

        public String getUserName() {
            return userName;
        }

        public List<VaccinePlan> getVaccinePlanList() {
            return vaccinePlanList;
        }

        public String getHospitalId() {
            return hospitalId;
        }
    }


    public static final class ResponseValue implements UseCase.ResponseValue {

        private VaccineOrder vaccineOrder;

        public ResponseValue(@NonNull VaccineOrder vaccineOrder) {
            this.vaccineOrder = vaccineOrder;
        }

        public VaccineOrder getVaccineOrder() {
            return vaccineOrder;
        }
    }









}
