package com.vims.vimsapp;


import android.app.Application;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.util.Log;

import com.vims.vimsapp.entities.schedule.HospitalImmunisationEvent;
import com.vims.vimsapp.manageorders.VaccineOrder;
import com.vims.vimsapp.utilities.DateConverter;
import com.vims.vimsapp.utilities.ListTypeConverter;
import com.vims.vimsapp.utilities.StatusEnumConverter;


@Database(entities = { HospitalImmunisationEvent.class, VaccineOrder.class}, version = 1, exportSchema = false)
@TypeConverters({DateConverter.class,StatusEnumConverter.class, ListTypeConverter.class})
public abstract class VaccinationsDatabase extends RoomDatabase{

    private static final String LOG_TAG = VaccinationsDatabase.class.getSimpleName();
    private static final String DATABASE_NAME = "vaccinations";


    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static VaccinationsDatabase sInstance;

    public static VaccinationsDatabase getInstance(Application application) {
        Log.d(LOG_TAG, "Getting the database");
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = Room.databaseBuilder(application,
                        VaccinationsDatabase.class, VaccinationsDatabase.DATABASE_NAME)
                        .fallbackToDestructiveMigration()
                        .build();
                Log.d(LOG_TAG, "Made new vaccinations database");
            }
        }
        return sInstance;
    }








}
