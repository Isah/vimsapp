package com.vims.vimsapp.retrofit_tests;


import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Base64;
import android.util.Log;

import com.vims.vimsapp.domain.patients.data.remote.vaccinated.PatientsApi;
import com.vims.vimsapp.domain.patients.data.remote.vaccinated.PatientsApiClient;
import com.vims.vimsapp.domain.patients.data.remote.vaccinations.VaccinationsApi;
import com.vims.vimsapp.domain.patients.data.remote.vaccinations.VaccinationsApiClient;
import com.vims.vimsapp.manageorders.VaccineOrder;
import com.vims.vimsapp.manageorders.VaccinePlan;
import com.vims.vimsapp.domain.patients.entity.vaccinated.Caretaker;
import com.vims.vimsapp.domain.patients.entity.vaccinated.Child;
import com.vims.vimsapp.domain.patients.entity.vaccinations.Hospital;
import com.vims.vimsapp.domain.patients.entity.vaccinations.HospitalImmunisationEvent;
import com.vims.vimsapp.domain.patients.entity.vaccinations.VaccinationCard;
import com.vims.vimsapp.domain.patients.entity.vaccinations.VaccinationEvent;
import com.vims.vimsapp.domain.patients.entity.vaccinations.VaccinationLocationState;
import com.vims.vimsapp.domain.patients.entity.vaccinations.VaccinationStatus;
import com.vims.vimsapp.utilities.DateCalendarConverter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


@RunWith(AndroidJUnit4.class)
public class RetrofitApiTests {


    VaccinationsApi vaccinationsApi;
    PatientsApi patientsApi;



    @Before
    public void createDb(){

        Context context = InstrumentationRegistry.getTargetContext();

        vaccinationsApi = VaccinationsApiClient.getRetrofitInstance().create(VaccinationsApi.class);
        patientsApi = PatientsApiClient.getRetrofitInstance().create(PatientsApi.class);


    }


    String res = "Api";
    boolean isApiConnected = false;


    Call<VaccinationEvent> callSyncEvents;

    @Test
    public void sendsEventsToApi(){

        Log.d("CARDRetrofitSend","API now!");
        final VaccinationCard vaccinationCard = new VaccinationCard();
        vaccinationCard.setDateCreated(DateCalendarConverter.dateToStringWithoutTime(new Date()));
        vaccinationCard.setChildId(UUID.randomUUID().toString());

        VaccinationEvent vaccinationEvent = new VaccinationEvent();
        String dateVacc = DateCalendarConverter.dateToStringWithoutTime(new Date());
        vaccinationEvent.setHospitalId("xxxxxxxxxxxxxxxxxxx");
        vaccinationEvent.setVaccinationLocationState(VaccinationLocationState.OUTREACH);
        vaccinationEvent.setVaccinationDate(dateVacc);
        vaccinationEvent.setVaccinationStatus(VaccinationStatus.DONE);
        vaccinationEvent.setVaccineName("BCG");
        vaccinationEvent.setChildName("Hellen");

        ///////////////////////////////////////////////////////////////
        VaccinationEvent vaccinationEvent1 = new VaccinationEvent();
        String dateVac = DateCalendarConverter.dateToStringWithoutTime(new Date());
        vaccinationEvent1.setHospitalId("xxxxxxxxyyyyyxxxxxxx");
        vaccinationEvent1.setVaccinationLocationState(VaccinationLocationState.STATIC);
        vaccinationEvent1.setVaccinationDate(dateVac);
        vaccinationEvent1.setVaccinationStatus(VaccinationStatus.DONE);
        vaccinationEvent1.setVaccineName("OPV1");
        vaccinationEvent1.setChildName("Martha");

        ArrayList<VaccinationEvent> vaccinationEvents = new ArrayList<>();
        vaccinationEvents.add(vaccinationEvent);
        vaccinationEvents.add(vaccinationEvent1);
        vaccinationCard.setVaccinationEvents(vaccinationEvents);



        for(VaccinationEvent vevent: vaccinationEvents) {

           // callSyncEvents = vaccinationsApi.addEvent(vevent);

            callSyncEvents.enqueue(new Callback<VaccinationEvent>() {
                @Override
                public void onResponse(@NonNull Call<VaccinationEvent> call, @NonNull Response<VaccinationEvent> response) {

                    Log.d("EventApi: ", "error: " + response.isSuccessful());

                    int statuscode = response.code();
                    VaccinationEvent event = response.body();
                    if (event != null) {
                        Log.d("EventApi: ", event.getVaccineName());
                    }

                    isApiConnected = true;
                    assertEquals(201, statuscode);

                }

                @Override
                public void onFailure(@NonNull Call<VaccinationEvent> call, @NonNull Throwable t) {

                    Log.d("CARDRetrofitFail", t.getMessage());
                    res = t.getMessage();
                    isApiConnected = false;
                    assertEquals("response executed error", t.getMessage());

                }
            });

        }
    }


    @Test
    public void getEventsFromApi(){
        //vaccinationsApi.saveChildVaccinationCard(vaccinationCard);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                 Call<List<VaccinationEvent>> callSync = null;// = vaccinationsApi.getEvents();

                 callSync.enqueue(new Callback<List<VaccinationEvent>>() {

                  @Override
                  public void onResponse(Call<List<VaccinationEvent>> call, Response<List<VaccinationEvent>> response) {
                  Log.d("CARDRetrofit","API Responded!");

                  assertEquals("ofcourse", response.body().get(0).getChildName());

                 }

                 @Override
                public void onFailure(Call<List<VaccinationEvent>> call, Throwable t) {
                 Log.d("CARDRetrofit","API Response Failure Messages!"+t.getCause());
                  }
                });

            }
        };


        new MainThreadExecutor().execute(runnable);

        // assertTrue(callSync.isExecuted());
        // assertTrue(callSync.isCanceled());

    }




    @Test
    public void getHospital(){

       Call<Hospital> hospitalCall =  vaccinationsApi.getVimsHospital(getAuthToken(),"6142");

       hospitalCall.enqueue(new Callback<Hospital>() {
           @Override
           public void onResponse(Call<Hospital> call, Response<Hospital> response) {


               //Log.d("Hospital",response.body().getAddress());
               assertEquals(200,response.code());


           }

           @Override
           public void onFailure(Call<Hospital> call, Throwable t) {

           }
       });




    }


    @Test
    public void makeOrder(){

        VaccinePlan vaccinePlan = new VaccinePlan();
        vaccinePlan.setVaccineName("BCG");
        vaccinePlan.setQuantityUsed(4);
        vaccinePlan.setQuantityAtHand(5);
        vaccinePlan.setQuantityRequired(3);

        List<VaccinePlan> plans = new ArrayList<>();
        plans.add(vaccinePlan);

        VaccineOrder vaccineOrder = new VaccineOrder("123456", "123", "kye", "kale", "mbale", true, "23/45/2018", plans);
        //vaccineOrder.setHospitalId("8888");
        //vaccineOrder.setStatus(true);
        //vaccineOrder.setOrderDate("23/45/2018");
        //vaccineOrder.setDistrict("denmark");
        //vaccineOrder.setHospitalName("france");
        // vaccineOrder.setHospitalId("hos");
        // vaccineOrder.setUserName("petr");
        // vaccineOrder.setOrderId("426272282");
        // vaccineOrder.setVaccinePlans(plans);

        Call<VaccineOrder> callSync = vaccinationsApi.orderVaccine(getAuthToken(),vaccineOrder);

        Log.d("RetroPlan: ","start");

        callSync.enqueue(new Callback<VaccineOrder>() {
            @Override
            public void onResponse(Call<VaccineOrder> call, Response<VaccineOrder> response) {

                Log.d("RetroPlan: ",response.message());
                assertEquals(200,response.code());

            }

            @Override
            public void onFailure(Call<VaccineOrder> call, Throwable t) {
                Log.d("RetroPlan: ",t.getMessage());

            }
        });


    }


    Call<Caretaker> callSync;
    @Test
    public void syncsCaretakerToApi() {

        Log.d("CARDRetrofitSend", "API now!");

        List<Caretaker> caretakers = new ArrayList<>();

        Caretaker caretaker = new Caretaker();
        caretaker.setCaretakerId(UUID.randomUUID().toString());
        caretaker.setRegistrationDate(DateCalendarConverter.dateWithTimeToDateWithoutTime(new Date()).getTime());
        caretaker.setHospitalId("5c2521ea6c4405162f987b04");
        caretaker.setFirstName("Joshua");
        caretaker.setLastName("Gareth");
        caretaker.setPhoneNo("adgggga");

        caretaker.setBirthDate("10/7/2018");
        caretaker.setGenderRole("afffffase");
        caretaker.setVillage("afffffaaa");
        caretaker.setParish("dfggggd");
        caretaker.setDistrict("sffggg");
        caretaker.setProfileImagePath("sggffff");

        Child child1 = new Child();
        child1.setFirstName("lucy");
        child1.setLastName("martha");
        child1.setDateOfBirth(DateCalendarConverter.dateToStringWithoutTime(new Date()));
        child1.setWeight(4);
        child1.setAge("45");
        child1.setCaretakerId("dgsg434gssdsdd");
        child1.setChildId("3323ksjsns");
        child1.setGender("boy");

        VaccinationEvent vaccinationEvent = new VaccinationEvent();
        String dateVacc = DateCalendarConverter.dateToStringWithoutTime(new Date());
        vaccinationEvent.setVaccinationDate(dateVacc);
        vaccinationEvent.setVaccinationStatus(VaccinationStatus.DONE);
        vaccinationEvent.setVaccineName("OPV3");

        ArrayList<VaccinationEvent> vaccinationEvents = new ArrayList<>();
        vaccinationEvents.add(vaccinationEvent);

        ArrayList<Child> children = new ArrayList<>();
        children.add(child1);

        caretaker.setChildren(children);




        VaccinationEvent vaccinationEventb = new VaccinationEvent();
        String dateV = DateCalendarConverter.dateToStringWithoutTime(new Date());

        vaccinationEventb.setVaccinationDate(dateV);
        vaccinationEventb.setVaccinationStatus(VaccinationStatus.DONE);
        vaccinationEventb.setVaccineName("OPV2");

        ArrayList<VaccinationEvent> vaccinationEventsb = new ArrayList<>();
        vaccinationEventsb.add(vaccinationEvent);

        caretakers.add(caretaker);


        for(Caretaker ctr: caretakers) {

          //  callSync = patientsApi.addCaretaker(ctr);

            callSync.enqueue(new Callback<Caretaker>() {
                @Override
                public void onResponse(@NonNull Call<Caretaker> call,@NonNull  Response<Caretaker> response) {

                    Log.d("CARDRetrofitSend", "api: "+response.message());

                    isApiConnected = true;
                    assertEquals(201, response.code());

                    Log.d("CARDRetrofitSend", "API Tested!" + response.body());


                }

                @Override
                public void onFailure(@NonNull Call<Caretaker> call, @NonNull Throwable t) {

                    Log.d("CARDRetrofitFail", t.getMessage());
                    res = t.getMessage();
                    isApiConnected = false;

                    assertEquals("response executed error", t.getMessage());


                }
            });


        }





    }


    @Test
    public void getCaretakers(){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Call<List<Caretaker>> callSync = patientsApi.getCaretakers("5c2521ea6c4405162f987b05");
                //Call<List<Caretaker>> callSync = patientsApi.getCaretakers();

                Log.d("CARETAKERS","started");
                callSync.enqueue(new Callback<List<Caretaker>>() {
                    @Override
                    public void onResponse(@NonNull  Call<List<Caretaker>> call, @NonNull  Response<List<Caretaker>> response) {
                        List<Caretaker> caretakers = response.body();
                        if(null != caretakers) {
                            Log.d("CARETAKERS N",caretakers.get(0).getFirstName() +""+caretakers.get(0).getLastName());
                        }
                        else {
                            Log.d("CARETAKERS","nothing picked");
                            assertEquals(260, response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Caretaker>> call, Throwable t) {

                        assertEquals("caretakers unavailable", t.getMessage());
                        Log.d("CARETAKERS","error");

                    }
                });

            }
        };
        new MainThreadExecutor().execute(runnable);
    }




    /*
     List<HospitalImmunisationEvent> hospitalImmunisationEvents = response.body();

                       if(hospitalImmunisationEvents!= null) {
                           for (HospitalImmunisationEvent h : hospitalImmunisationEvents) {
                               Log.d("HospitalEvent", "Date: " + h.getEventDate());
                           }
                       }
     */


    public  String getAuthToken() {

        //API_USER_ID = "petr"; //used for testing

        byte[] data = new byte[0];
        try {
            data = ("petr" + ":" + "12345678910").getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }



    @Test
    public void getImmunisationApi(){
               // Call<List<HospitalImmunisationEvent>> callSync = vaccinationsApi.getHospitalSchedule("5c25221f6c4405162f987b05");
               // Call<List<HospitalImmunisationEvent>> callSync = vaccinationsApi.getRemoteHospitalSchedule("5c25221f6c4405162f987b05");
               Call<List<HospitalImmunisationEvent>> callSync = vaccinationsApi.getVimsHospitalSchedule(getAuthToken(),"5ceda2846c44052e9bb59d0f");

               callSync.enqueue(new Callback<List<HospitalImmunisationEvent>>() {
                   @Override
                   public void onResponse(Call<List<HospitalImmunisationEvent>> call, Response<List<HospitalImmunisationEvent>> response) {

                       if(response.body()!=null){

                           List<HospitalImmunisationEvent> events = response.body();
                           if(events!=null) {
                               HospitalImmunisationEvent event = events.get(0);
                               Log.d("RemoteSchedule", "API Responded ResponseEvent! "+event.getEventDate()+" size: "+events.size());
                           }

                       }

                       int response_code = response.code();
                       Log.d("RemoteSchedule", "API Responded ResponseCode! "+response_code);
                       assertEquals(200, response_code);

                   }

                   @Override
                   public void onFailure(Call<List<HospitalImmunisationEvent>> call, Throwable t) {

                       Log.d("RemoteSchedule","API HoppitalEvents Response Failure Messages!"+t.getCause());
                       assertEquals("RemoteSchedule HospitalEvents Error", t.getLocalizedMessage());

                   }
               });



                // new MainThreadExecutor().execute(runnable);
                // assertTrue(callSync.isExecuted());
                // assertTrue(callSync.isCanceled());

    }





    public class MainThreadExecutor implements Executor {

        private Handler mainThreadHandler = new Handler(Looper.getMainLooper());


        @Override
        public void execute(@NonNull Runnable runnable) {

            mainThreadHandler.postDelayed(runnable, 5000);
        }
    }


}
