package com.vims.vimsapp.local_repository_tests;


import com.vims.vimsapp.domain.patients.data.PatientsDataSource;
import com.vims.vimsapp.domain.patients.data.local.vaccinated.CaretakerDao;
import com.vims.vimsapp.domain.patients.data.local.vaccinated.ChildDao;
import com.vims.vimsapp.domain.patients.data.local.vaccinated.PatientsLocalDataSource;
import com.vims.vimsapp.domain.patients.data.local.vaccinated.PatientsSyncTimeDao;
import com.vims.vimsapp.domain.patients.data.remote.vaccinated.PatientsRemoteDataSource;

import com.vims.vimsapp.domain.patients.entity.vaccinated.Child;
import com.vims.vimsapp.utilities.AppExecutors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

@RunWith(org.mockito.runners.MockitoJUnitRunner.class)
public class PatientsLocalRepositoryTests {




    PatientsLocalDataSource patientsLocalDataSource;

    PatientsRemoteDataSource patientsRemoteDataSource;

    @Mock
    CaretakerDao caretakerDao;

    @Mock
    ChildDao childDao;


    @Mock
    PatientsSyncTimeDao patientsSyncTimeDao;


    @Mock
    PatientsDataSource.SaveChildCallback saveChildCallback;

    AppExecutors executors;


    @Before
    public void setUp(){

        MockitoAnnotations.initMocks(this);
        executors = new AppExecutors();
       // patientsLocalDataSource = PatientsLocalDataSource.getInstance(executors,caretakerDao,childDao,patientsSyncTimeDao);


    }



    @Test
    public void saveChild_savesChild_toRepo() {
        // Given a stub task with title and description
       Child child = new Child();

       child.setFirstName("Augusto");
       child.setLastName("Sahi");
       child.setGender("boy");


        patientsLocalDataSource.saveChild(child,saveChildCallback);

        // Then the service API and persistent repository are called and the cache is updated

        Mockito.verify(childDao).Insert(child);


    }




}
