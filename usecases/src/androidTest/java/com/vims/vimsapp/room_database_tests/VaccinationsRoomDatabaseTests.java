package com.vims.vimsapp.room_database_tests;


import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;


import com.vims.vimsapp.domain.patients.data.VaccinationsDatabase;
import com.vims.vimsapp.domain.patients.data.local.vaccinations.VaccinationCardDao;
import com.vims.vimsapp.domain.patients.data.local.vaccinations.VaccinationEventDao;
import com.vims.vimsapp.domain.patients.entity.vaccinations.VaccinationCard;
import com.vims.vimsapp.domain.patients.entity.vaccinations.VaccinationEvent;
import com.vims.vimsapp.domain.patients.entity.vaccinations.VaccinationStatus;
import com.vims.vimsapp.utilities.DateCalendarConverter;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class VaccinationsRoomDatabaseTests {



    VaccinationCardDao vaccinationCardDao;
    VaccinationEventDao vaccinationEventDao;

    VaccinationsDatabase vaccinationsDatabase;


    @Before
    public void createDb(){

        Context context = InstrumentationRegistry.getTargetContext();

        vaccinationsDatabase = Room.inMemoryDatabaseBuilder(context, VaccinationsDatabase.class).build();

       // vaccinationCardDao = vaccinationsDatabase.getVaccinationCardDao();
       //  vaccinationEventDao = vaccinationsDatabase.getVaccinationEventDao();


    }

    @After
    public void closeDb() throws IOException {

        vaccinationsDatabase.close();


    }




    @Test
    public void saveCard_shouldReturn_card() throws Exception{

        VaccinationCard vaccinationCard = new VaccinationCard();

        String expectedChildId = UUID.randomUUID().toString();
        Date expectedDate = Calendar.getInstance().getTime();

        vaccinationCard.setChildId(expectedChildId);
        vaccinationCard.setDateCreated(DateCalendarConverter.dateToStringWithoutTime(expectedDate));

        vaccinationCardDao.Insert(vaccinationCard);

        VaccinationCard dbVaccinationCard  = vaccinationCardDao.getChildVaccinationCard(expectedChildId);

        List<VaccinationCard> dbVaccinationCards  = vaccinationCardDao.GetAllChildVaccinationCardsAll();



        String actualChildId =  dbVaccinationCard.getChildId();
       // Date actualDate =  DateCalendarConverter.stringToDateWithoutTime();


        assertThat(expectedChildId, equalTo(actualChildId));
        assertThat(DateCalendarConverter.dateToStringWithoutTime(expectedDate), equalTo(dbVaccinationCard.getDateCreated()));

        assertThat(dbVaccinationCards.size(), is(1));


    }



    @Test
    public void verify_savingCard_returnsTheCardsSavedEvents() throws Exception{

        VaccinationCard vaccinationCard = new VaccinationCard();

        String expectedChildId = UUID.randomUUID().toString();
        Date expectedDate = Calendar.getInstance().getTime();

        vaccinationCard.setChildId(expectedChildId);
        vaccinationCard.setDateCreated((DateCalendarConverter.dateToStringWithoutTime(expectedDate)));

        vaccinationCardDao.Insert(vaccinationCard);


        VaccinationEvent expectedVaccinationEvent = new VaccinationEvent();
        expectedVaccinationEvent.setVaccineName("Polio 0");

        String dateVacc = DateCalendarConverter.dateToStringWithoutTime(new Date());



       // expectedVaccinationEvent.setScheduledDate(dateVacc);
        expectedVaccinationEvent.setVaccinationStatus(VaccinationStatus.DONE);

        List<VaccinationEvent> events = new ArrayList<>();
        events.add(expectedVaccinationEvent);


         vaccinationEventDao.Insert(events);


        // List<VaccinationEvent> actualtEventsFromDb  = vaccinationEventDao.getChildVaccinationCardEvents(vaccinationCard.getCardId());

        // String actualCardIdReturned = actualtEventsFromDb .get(0).getCardId();
         //String actualVaccineNameReturned = actualtEventsFromDb .get(0).getVaccineName();
        // VaccinationStatus  actualVaccineStatusReturned = actualtEventsFromDb .get(0).getVaccinationStatus();


         //assertThat(vaccinationCard.getCardId(), equalTo(actualCardIdReturned));
         //assertThat(expectedVaccinationEvent.getVaccineName(), equalTo(actualVaccineNameReturned));
         //assertThat(VaccinationStatus.DONE, equalTo(actualVaccineStatusReturned));



    }






    @Test
    public void updatesSavedVaccinationEventFromEventsDao() throws Exception{


        VaccinationCard vaccinationCard = new VaccinationCard();

        String expectedChildId = UUID.randomUUID().toString();
        Date expectedDate = Calendar.getInstance().getTime();

        vaccinationCard.setChildId(expectedChildId);
        vaccinationCard.setDateCreated(DateCalendarConverter.dateToStringWithoutTime(expectedDate));

        vaccinationCardDao.Insert(vaccinationCard);


        VaccinationEvent expectedVaccinationEvent = new VaccinationEvent();
        expectedVaccinationEvent.setVaccineName("Polio 0");

        Calendar sechedule_calendar = Calendar.getInstance();
        sechedule_calendar.set(2018,8, 10);



      //  expectedVaccinationEvent.setScheduledDate(new Date(sechedule_calendar.getTime()));
        expectedVaccinationEvent.setVaccinationStatus(VaccinationStatus.NOT_DONE);


        List<VaccinationEvent> events = new ArrayList<>();
        events.add(expectedVaccinationEvent);


        //when event has been inserted
        vaccinationEventDao.Insert(events);



        //try updating the existing event
        expectedVaccinationEvent.setVaccinationStatus(VaccinationStatus.DONE);
        Calendar dateOfVaccination = Calendar.getInstance();
        dateOfVaccination.set(2018,8,15);

        expectedVaccinationEvent.setVaccinationDate(DateCalendarConverter.dateToStringWithoutTime(dateOfVaccination.getTime()));

        int noOfUpdatedRows = vaccinationEventDao.Update(expectedVaccinationEvent);

        //check that event is updated
       // List<VaccinationEvent> actualEvents = vaccinationEventDao.getChildVaccinationCardEvents(vaccinationCard.getCardId());

       // VaccinationStatus actualStatus = actualEvents.get(0).getVaccinationStatus();

       // Date actualVaccinationDate  = DateCalendarConverter.stringToDateWithoutTime(actualEvents.get(0).getVaccinationDate());

       // assertEquals(actualStatus, VaccinationStatus.DONE);
       // assertThat(actualVaccinationDate,is(DateCalendarConverter.dateWithTimeToDateWithoutTime(dateOfVaccination.getTime())));


        assertThat(1,is(noOfUpdatedRows));

    }









}
