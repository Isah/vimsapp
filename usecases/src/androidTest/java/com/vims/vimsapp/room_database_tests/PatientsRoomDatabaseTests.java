package com.vims.vimsapp.room_database_tests;


import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.vims.vimsapp.config.PatientsDatabase;

import com.vims.vimsapp.domain.patients.data.local.vaccinated.CaretakerDao;
import com.vims.vimsapp.domain.patients.data.local.vaccinated.ChildDao;
import com.vims.vimsapp.domain.patients.entity.vaccinated.Caretaker;
import com.vims.vimsapp.domain.patients.entity.vaccinated.Child;
import com.vims.vimsapp.utilities.DateCalendarConverter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.List;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class PatientsRoomDatabaseTests {


    private ChildDao childDao;
    private CaretakerDao caretakerDao;
    private PatientsDatabase patientsDatabase;



    //you should create an in memory version of
    // your db to make the tests mor hermetic

    @Before
    public void createDb(){

        Context context = InstrumentationRegistry.getTargetContext();

        patientsDatabase = Room.inMemoryDatabaseBuilder(context, PatientsDatabase.class).build();

        childDao = patientsDatabase.getChildDao();
        caretakerDao = patientsDatabase.getCaretakerDao();


    }


    @After
    public void closeDb() throws IOException {

        patientsDatabase.close();


    }


    @Test
    public void savesChildIfChildIsSaved() throws Exception{


        Caretaker caretaker = new Caretaker();
        caretaker.setFirstName("Mr peter");

        caretakerDao.Insert(caretaker);


        Child child = new Child();

        child.setFirstName("Augusto");
        child.setLastName("Sahi");
        child.setGender("boy");



        int day = 15;
        int month = 7;
        int year = 2018;

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(year,month,day);

        String datewithouttime = DateCalendarConverter.dateToStringWithoutTime(gregorianCalendar.getTime());


        child.setDateOfBirth(datewithouttime);


        child.setCaretakerId(caretaker.getCaretakerId());

        childDao.Insert(child);

        List<Child> children = childDao.getChildrenByCaretakerId(caretaker.getCaretakerId());

        assertThat(children.get(0).getFirstName(), equalTo(child.getFirstName()));

        assertThat(children.get(0).getDateOfBirth(), equalTo(child.getDateOfBirth()));



    }










}
