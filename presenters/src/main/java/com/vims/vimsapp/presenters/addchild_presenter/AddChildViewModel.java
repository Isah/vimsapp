package com.vims.vimsapp.presenters.addchild_presenter;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

/**
 * Created by root on 5/31/18.
 */

public class AddChildViewModel extends ViewModel {


    /*
    If those buttons should be grayed out, the
    Presenter will set an appropriate boolean
    flag in the View model.
    */

    //1. Intro Model
    //private MutableLiveData<CharSequence> mIntroTitle;
    //private MutableLiveData<CharSequence> mIntroSubtitle;
    private MutableLiveData<String> mIntroValidationFailMessage;
    private MutableLiveData<Boolean> mIsScheduleAvailable;



    //3. Success Model
    private MutableLiveData<String> mSuccessDescTitle;
    private MutableLiveData<String> mSuccessDescSubtitle;
    private MutableLiveData<Boolean> mIsShowColorsOfDescTitleSubtitle;
    private MutableLiveData<Boolean> mIsShowProgressBar;
    private MutableLiveData<Boolean> mIsShowChildDetailGrid;
    private MutableLiveData<Boolean> mIsShowChildSuccessWrapper;
    private MutableLiveData<Boolean> mIsShowSuccessAnimation;
    private MutableLiveData<Boolean> mIsShowTitleDashLine;
    private MutableLiveData<Boolean> mIsMarkCaretakerAsRegistered;
    private MutableLiveData<String> mFailMessage;


    //1. Switch off the old presentation
    //present the visibility of the progress loading to off
    //present the visibility of the grid that holds the child detail info to off
    //present the visibility of the dashline of title off

    //2. switch on the new presentation
    //present the name and name label
    //present the colors of the name and label
    //present the wrapping of the success message
    //present the view visibility for success info on
    //present the animation of the tickcross to success
    //present the marking of current caretaker as registered


    public MutableLiveData<Boolean> getmIsShowProgressBar() {
        if(this.mIsShowProgressBar == null){
            this.mIsShowProgressBar = new MutableLiveData<>();
        }
        return mIsShowProgressBar;
    }

    public void setmIsShowProgressBar(boolean mIsShowProgressBar) {
        if(this.mIsShowProgressBar == null){
            this.mIsShowProgressBar = new MutableLiveData<>();
        }
        this.mIsShowProgressBar.postValue(mIsShowProgressBar);
    }


    public MutableLiveData<Boolean> getmIsShowColorsOfDescTitleSubtitle() {
        if(this.mIsShowColorsOfDescTitleSubtitle == null){
            this.mIsShowColorsOfDescTitleSubtitle = new MutableLiveData<>();
        }
        return mIsShowColorsOfDescTitleSubtitle;
    }

    public void setmIsShowColorsOfDescTitleSubtitle(boolean mIsShowColorsOfDescTitleSubtitle) {
        if(this.mIsShowColorsOfDescTitleSubtitle == null){
            this.mIsShowColorsOfDescTitleSubtitle = new MutableLiveData<>();
        }
        this.mIsShowColorsOfDescTitleSubtitle.postValue(mIsShowColorsOfDescTitleSubtitle);
    }

    public MutableLiveData<Boolean> getmIsShowChildDetailGrid() {
        if(this.mIsShowChildDetailGrid == null){
            this.mIsShowChildDetailGrid = new MutableLiveData<>();
        }
        return mIsShowChildDetailGrid;
    }

    public void setmIsShowChildDetailGrid(boolean mIsShowChildDetailGrid) {
        if(this.mIsShowChildDetailGrid == null){
            this.mIsShowChildDetailGrid = new MutableLiveData<>();
        }
        this.mIsShowChildDetailGrid.postValue(mIsShowChildDetailGrid);
    }

    public MutableLiveData<Boolean> getmIsShowChildSuccessWrapper() {
        if(this.mIsShowChildSuccessWrapper == null){
            this.mIsShowChildSuccessWrapper = new MutableLiveData<>();
        }
        return mIsShowChildSuccessWrapper;
    }

    public void setmIsShowChildSuccessWrapper(boolean mIsShowChildSuccessWrapper) {
        if(this.mIsShowChildSuccessWrapper == null){
            this.mIsShowChildSuccessWrapper = new MutableLiveData<>();
        }
        this.mIsShowChildSuccessWrapper.postValue(mIsShowChildSuccessWrapper);
    }

    public MutableLiveData<Boolean> getmIsShowSuccessAnimation() {
        if(this.mIsShowSuccessAnimation == null){
            this.mIsShowSuccessAnimation = new MutableLiveData<>();
        }
        return mIsShowSuccessAnimation;
    }

    public void setmIsShowSuccessAnimation(boolean mIsShowSuccessAnimation) {
        if(this.mIsShowSuccessAnimation == null){
            this.mIsShowSuccessAnimation = new MutableLiveData<>();
        }
        this.mIsShowSuccessAnimation.postValue(mIsShowSuccessAnimation);
    }

    public MutableLiveData<Boolean> getmIsShowTitleDashLine() {
        if(this.mIsShowTitleDashLine == null){
            this.mIsShowTitleDashLine = new MutableLiveData<>();
        }
        return mIsShowTitleDashLine;
    }

    public void setmIsShowTitleDashLine(boolean mIsShowTitleDashLine) {
        if(this.mIsShowTitleDashLine == null){
            this.mIsShowTitleDashLine = new MutableLiveData<>();
        }
        this.mIsShowTitleDashLine.postValue(mIsShowTitleDashLine);
    }

    public LiveData<Boolean> getmIsMarkCaretakerAsRegistered() {
        if(this.mIsMarkCaretakerAsRegistered == null){
            this.mIsMarkCaretakerAsRegistered = new MutableLiveData<>();
        }
        return mIsMarkCaretakerAsRegistered;
    }

    public void setmIsMarkCaretakerAsRegistered(boolean mIsMarkCaretakerAsRegistered) {
        if(this.mIsMarkCaretakerAsRegistered == null){
            this.mIsMarkCaretakerAsRegistered = new MutableLiveData<>();
        }
        this.mIsMarkCaretakerAsRegistered.postValue(mIsMarkCaretakerAsRegistered);
    }












    public MutableLiveData<String> getmIntroValidationFailMessage() {
        if(this.mIntroValidationFailMessage == null){
            this.mIntroValidationFailMessage = new MutableLiveData<>();
        }
        return mIntroValidationFailMessage;
    }

    public void setmIntroValidationFailMessage(String mIntroValidationFailMessage) {
        if(this.mIntroValidationFailMessage == null){
            this.mIntroValidationFailMessage = new MutableLiveData<>();
        }
        this.mIntroValidationFailMessage.postValue(mIntroValidationFailMessage);
    }





    public MutableLiveData<Boolean> getmIsScheduleAvailable() {
        return mIsScheduleAvailable;
    }

    public void setmIsScheduleAvailable(boolean mIsScheduleAvailable) {
        if(this.mIsScheduleAvailable == null){
            this.mIsScheduleAvailable = new MutableLiveData<>();
        }
        this.mIsScheduleAvailable.postValue(mIsScheduleAvailable);
    }





    public MutableLiveData<String> getmFailMessage() {
        if(this.mFailMessage == null){
            this.mFailMessage = new MutableLiveData<>();
        }
        return mFailMessage;
    }

    public void setmFailMessage(String mFailMessage) {
        if(this.mFailMessage == null){
            this.mFailMessage = new MutableLiveData<>();
        }
        this.mFailMessage.postValue(mFailMessage);
    }

    public MutableLiveData<String> getmSuccessDescTitle() {
        if(this.mSuccessDescTitle == null){
            this.mSuccessDescTitle = new MutableLiveData<>();
        }
        return mSuccessDescTitle;
    }

    public void setmSuccessDescTitle(String mSuccessDescTitle) {
        if(this.mSuccessDescTitle == null){
            this.mSuccessDescTitle = new MutableLiveData<>();
        }
        this.mSuccessDescTitle.postValue(mSuccessDescTitle);
    }



    public MutableLiveData<String> getmSuccessDescSubtitle() {
        if(this.mSuccessDescSubtitle == null){
            this.mSuccessDescSubtitle = new MutableLiveData<>();
        }
        return mSuccessDescSubtitle;
    }
    public void setmSuccessDescSubtitle(String mSuccessDescSubtitle) {
        if(this.mSuccessDescSubtitle == null){
            this.mSuccessDescSubtitle = new MutableLiveData<>();
        }
        this.mSuccessDescSubtitle.postValue(mSuccessDescSubtitle);
    }

}
