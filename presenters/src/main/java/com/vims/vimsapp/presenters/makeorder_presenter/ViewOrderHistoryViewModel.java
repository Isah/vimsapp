package com.vims.vimsapp.presenters.makeorder_presenter;


import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

/**
 * Created by root on 5/31/18.
 */

public class ViewOrderHistoryViewModel extends ViewModel {

    /*
    If those buttons should be grayed out, the
    Presenter will set an appropriate boolean
    flag in the View model.
    */



    //3. History Model
    private MutableLiveData<String> mPastOrderTitle;
    private MutableLiveData<List> mPastOrderItemList;

    private MutableLiveData<Boolean> mIsShowPastOrderListView;
    private MutableLiveData<Boolean> mIsShowPastOrderErrorView;
    private MutableLiveData<Boolean> mIsShowPastOrderProgress;


    private MutableLiveData<String> mOrderResponseMessage;


    //1. Switch off the old presentation
    //present the visibility of the progress loading to off
    //present the visibility of the grid that holds the child detail info to off
    //present the visibility of the dashline of title off

    //2. switch on the new presentation
    //present the name and name label
    //present the colors of the name and label
    //present the wrapping of the success message
    //present the view visibility for success info on
    //present the animation of the tickcross to success
    //present the marking of current caretaker as registered



    public MutableLiveData<String> getmOrderResponseMessage() {
        if(mOrderResponseMessage == null){
            mOrderResponseMessage = new MutableLiveData<>();
        }
        return mOrderResponseMessage;
    }

    public void setmOrderResponseMessage(String orderResponseMessage) {
        this.mOrderResponseMessage.postValue(orderResponseMessage);
    }




    /**  Past Orders**/

    public MutableLiveData<String> getmPastOrderTitle() {
        if(mPastOrderTitle == null){
            mPastOrderTitle = new MutableLiveData<>();
        }
        return mPastOrderTitle;
    }

    public void setmPastOrderTitle(String mPastOrderTitle) {
        this.mPastOrderTitle.postValue(mPastOrderTitle);
    }

    public MutableLiveData<List> getmPastOrderItemList() {
        if(mPastOrderItemList == null){
            mPastOrderItemList = new MutableLiveData<>();
        }
        return mPastOrderItemList;
    }

    public void setmPastOrderItemList(List mPastOrderItemList) {
        this.mPastOrderItemList.postValue(mPastOrderItemList);
    }

    public MutableLiveData<Boolean> getmIsShowPastOrderListView() {
        if(mIsShowPastOrderListView == null){
            mIsShowPastOrderListView = new MutableLiveData<>();
        }
        return mIsShowPastOrderListView;
    }

    public void setmIsShowPastOrderListView(boolean mIsShowPastOrderListView) {
        this.mIsShowPastOrderListView.postValue(mIsShowPastOrderListView);
    }

    public MutableLiveData<Boolean> getmIsShowPastOrderErrorView() {
        if(mIsShowPastOrderErrorView == null){
            mIsShowPastOrderErrorView = new MutableLiveData<>();
        }
        return mIsShowPastOrderErrorView;
    }

    public void setmIsShowPastOrderErrorView(boolean mIsShowPastOrderErrorView) {
        this.mIsShowPastOrderErrorView.postValue(mIsShowPastOrderErrorView);
    }

    public MutableLiveData<Boolean> getmIsShowPastOrderProgress() {
        if(mIsShowPastOrderProgress == null){
            mIsShowPastOrderProgress = new MutableLiveData<>();
        }
        return mIsShowPastOrderProgress;
    }

    public void setmIsShowPastOrderProgress(boolean mIsShowPastOrderProgress) {
        this.mIsShowPastOrderProgress.postValue(mIsShowPastOrderProgress);
    }
}
