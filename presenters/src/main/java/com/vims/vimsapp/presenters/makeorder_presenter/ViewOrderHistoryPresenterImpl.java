package com.vims.vimsapp.presenters.makeorder_presenter;


import com.vims.vimsapp.manage_order_controller.ManageOrderPresenter;
import com.vims.vimsapp.manage_order_controller.ViewOrderHistoryPresenter;
import com.vims.vimsapp.manageorders.RequestOrder;
import com.vims.vimsapp.manageorders.ViewOrders;

import java.util.Calendar;

public class ViewOrderHistoryPresenterImpl implements ViewOrderHistoryPresenter {

     /**
      *
     The presenter knows about the response model and the view model
     the view model is a data structure
     the response model is also a data structure

    1. user clicks to make order
       system opens up make order view
       user selects doses as required
       user makes order
       system checks order and if order is not in future and is in last week of month
       else it sends validity message
       so system sends order request
       system return response of order whether sent or not
       TEST
       pass in a fake ResponseModel
       look at the view model
       if the values have been prepared well
       and also if the right methods executed
       *
      **/


    //AddChildPreviewScreen addChildPreviewScreen;
    ViewOrderHistoryViewModel viewModel;

    //The preview screen is a fragment
    //But because we have a view model the view will register for events through the viewmodel

    public ViewOrderHistoryPresenterImpl(ViewOrderHistoryViewModel viewModel) {
        this.viewModel = viewModel;
    }




    @Override
    public void showAllOrders(ViewOrders.ResponseValue responseValue) {

    }

    @Override
    public void showRecentOrders(ViewOrders.ResponseValue responseValue) {
        viewModel.setmPastOrderTitle("History");
        viewModel.setmPastOrderItemList(responseValue.getVaccineOrders());

        viewModel.setmIsShowPastOrderErrorView(false);
        viewModel.setmIsShowPastOrderListView(true);//recycler
        viewModel.setmIsShowPastOrderProgress(false);

    }
}

