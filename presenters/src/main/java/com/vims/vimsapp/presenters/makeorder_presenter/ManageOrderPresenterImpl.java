package com.vims.vimsapp.presenters.makeorder_presenter;


import android.util.Log;

import com.vims.vimsapp.addchild_controller.AddChildPresenter;
import com.vims.vimsapp.manage_order_controller.ManageOrderPresenter;
import com.vims.vimsapp.manageorders.Dosage;
import com.vims.vimsapp.manageorders.RequestOrder;
import com.vims.vimsapp.manageorders.ViewOrders;
import com.vims.vimsapp.presenters.addchild_presenter.AddChildViewModel;
import com.vims.vimsapp.registerchild.GenerateChildSchedule;

import java.util.ArrayList;
import java.util.Calendar;

public class ManageOrderPresenterImpl implements ManageOrderPresenter {

     /**
      *
     The presenter knows about the response model and the view model
     the view model is a data structure
     the response model is also a data structure

    1. user clicks to make order
       system opens up make order view
       user selects doses as required
       user makes order
       system checks order and if order is not in future and is in last week of month
       else it sends validity message
       so system sends order request
       system return response of order whether sent or not
       TEST
       pass in a fake ResponseModel
       look at the view model
       if the values have been prepared well
       and also if the right methods executed
       *
      **/


    //AddChildPreviewScreen addChildPreviewScreen;
    ManageOrderViewModel viewModel;
    ManageOrderView manageOrderView;

    //The preview screen is a fragment
    //But because we have a view model the view will register for events through the viewmodel

    public ManageOrderPresenterImpl(ManageOrderView manageOrderView) {
        viewModel = new ManageOrderViewModel();
        this.manageOrderView = manageOrderView;
    }

    @Override
    public void presentOrderFailure(String error) {

        viewModel.setmOrderResponseMessage(error);
    }

    @Override
    public void presentOrderSuccess(RequestOrder.ResponseValue responseValue) {

        //The Presenter gets the result from the use case
        //Then it organises it into a string for the view

        String orderSuccessMessage = "Order "+responseValue.getVaccineOrder().getOrderId()+" sent successfully";
        viewModel.setmOrderResponseMessage(orderSuccessMessage);


    }

    @Override
    public void presentOrderValidationFailure(String error) {

        viewModel.setmOrderResponseMessage(error);

    }

    @Override
    public void previewSentOrder(ViewOrders.ResponseValue responseValue) {


        String[] orderSplit = responseValue.getVaccineOrder().getOrderId().split("-");

        String firstString = orderSplit[1];
        String lastString = orderSplit[4];

        char orderid_char1 = lastString.charAt(0);

        //db7b91e0-14b5-4a87-8312-1b695457bf97
        //bd0f5a79-7908-49b7-b081-c5af2165755f

        //THE BINDING
        String orderId = firstString+" - "+orderid_char1;
        String orderTitle = "Order #" + orderId;



        viewModel.setmSentOrderTitle(responseValue.getDosageDate());
        viewModel.setmSentOrderSubtitle(orderTitle);
        viewModel.setmSentOrderItemList(responseValue.getVaccineOrders());
        viewModel.setmSentOrderDateMessage(responseValue.getDosageDate());

        viewModel.setmIsShowOrderExpiryDate(false);
        viewModel.setmIsShowOrderSubmitted(true);
        viewModel.setmIsShowOrderSummary(true);


    }

    @Override
    public void previewNewOrder(ViewOrders.ResponseValue responseValue) {

        int daysToGoToVaccination = responseValue.getExpiryDaysForVaccination();

        String timeText;

        if(daysToGoToVaccination <= 7){//supposed to be 7 to indicae last 7 days

            timeText = "Ordering Exp in "+daysToGoToVaccination+" days";
            viewModel.setmIsOrderExpiring(true);


        }else{
            Calendar cal = Calendar.getInstance();
            int lastDayOfMonth = cal.getActualMaximum(Calendar.DATE);
            int dayOfmonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

            int daysOfOrderPeriod = lastDayOfMonth - 7;
            int daysToOrderPeriod = daysOfOrderPeriod - dayOfmonth;


            timeText = "Submit Order in "+daysToOrderPeriod+" days";
            viewModel.setmIsOrderExpiring(false);

        }



        viewModel.setmNewOrderTitle(responseValue.getDosageDate());
        viewModel.setmNewOrderSubtitle(timeText);
        viewModel.setmNewOrderItemList(responseValue.getDosageList());
        viewModel.setmIsShowOrderExpiryDate(true);
        viewModel.setmIsShowOrderSubmitted(false);
        viewModel.setmIsShowOrderSummary(false);


        manageOrderView.showNewOrder(viewModel);


    }




}

