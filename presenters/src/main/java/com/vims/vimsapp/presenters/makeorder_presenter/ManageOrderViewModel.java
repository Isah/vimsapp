package com.vims.vimsapp.presenters.makeorder_presenter;


import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;
import android.util.Log;


import com.vims.vimsapp.manageorders.Dosage;

import java.util.List;

import static android.arch.lifecycle.Transformations.map;

/**
 * Created by root on 5/31/18.
 */

public class ManageOrderViewModel extends ViewModel {


    /*
    If those buttons should be grayed out, the
    Presenter will set an appropriate boolean
    flag in the View model.
    */

    //1. NewOrder Model
    private String mNewOrderTitle;
    private String mNewOrderSubtitle;
    private List<Dosage> mNewOrderItemList;



    //2. SentOrder Model
    private String mSentOrderTitle;
    private String mSentOrderSubtitle;
    private String mSentOrderDateMessage;
    private List mSentOrderItemList;


    private Boolean mIsShowOrderSubmitted;
    private Boolean mIsShowOrderSummary;
    private Boolean mIsShowOrderExpiryDate;
    private Boolean mIsOrderExpiring;







    private String mOrderResponseMessage;

    public ManageOrderViewModel() {

    }

//1. Switch off the old presentation
    //present the visibility of the progress loading to off
    //present the visibility of the grid that holds the child detail info to off
    //present the visibility of the dashline of title off

    //2. switch on the new presentation
    //present the name and name label
    //present the colors of the name and label
    //present the wrapping of the success message
    //present the view visibility for success info on
    //present the animation of the tickcross to success
    //present the marking of current caretaker as registered


    /** New Order Preview**/


    public String getmNewOrderTitle() {
        return mNewOrderTitle;
    }

    public void setmNewOrderTitle(String mNewOrderTitle) {
        this.mNewOrderTitle = mNewOrderTitle;
    }

    public String getmNewOrderSubtitle() {
        return mNewOrderSubtitle;
    }

    public void setmNewOrderSubtitle(String mNewOrderSubtitle) {
        this.mNewOrderSubtitle = mNewOrderSubtitle;
    }

    public List<Dosage> getmNewOrderItemList() {
        return mNewOrderItemList;
    }

    public void setmNewOrderItemList(List<Dosage> mNewOrderItemList) {
        this.mNewOrderItemList = mNewOrderItemList;
    }

    public String getmSentOrderTitle() {
        return mSentOrderTitle;
    }

    public void setmSentOrderTitle(String mSentOrderTitle) {
        this.mSentOrderTitle = mSentOrderTitle;
    }

    public String getmSentOrderSubtitle() {
        return mSentOrderSubtitle;
    }

    public void setmSentOrderSubtitle(String mSentOrderSubtitle) {
        this.mSentOrderSubtitle = mSentOrderSubtitle;
    }

    public String getmSentOrderDateMessage() {
        return mSentOrderDateMessage;
    }

    public void setmSentOrderDateMessage(String mSentOrderDateMessage) {
        this.mSentOrderDateMessage = mSentOrderDateMessage;
    }

    public List getmSentOrderItemList() {
        return mSentOrderItemList;
    }

    public void setmSentOrderItemList(List mSentOrderItemList) {
        this.mSentOrderItemList = mSentOrderItemList;
    }

    public Boolean getmIsShowOrderSubmitted() {
        return mIsShowOrderSubmitted;
    }

    public void setmIsShowOrderSubmitted(Boolean mIsShowOrderSubmitted) {
        this.mIsShowOrderSubmitted = mIsShowOrderSubmitted;
    }

    public Boolean getmIsShowOrderSummary() {
        return mIsShowOrderSummary;
    }

    public void setmIsShowOrderSummary(Boolean mIsShowOrderSummary) {
        this.mIsShowOrderSummary = mIsShowOrderSummary;
    }

    public Boolean getmIsShowOrderExpiryDate() {
        return mIsShowOrderExpiryDate;
    }

    public void setmIsShowOrderExpiryDate(Boolean mIsShowOrderExpiryDate) {
        this.mIsShowOrderExpiryDate = mIsShowOrderExpiryDate;
    }

    public Boolean getmIsOrderExpiring() {
        return mIsOrderExpiring;
    }

    public void setmIsOrderExpiring(Boolean mIsOrderExpiring) {
        this.mIsOrderExpiring = mIsOrderExpiring;
    }

    public String getmOrderResponseMessage() {
        return mOrderResponseMessage;
    }

    public void setmOrderResponseMessage(String mOrderResponseMessage) {
        this.mOrderResponseMessage = mOrderResponseMessage;
    }
}
