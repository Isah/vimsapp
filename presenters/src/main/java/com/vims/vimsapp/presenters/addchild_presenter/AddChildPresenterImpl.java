package com.vims.vimsapp.presenters.addchild_presenter;


import com.vims.vimsapp.addchild_controller.AddChildPresenter;
import com.vims.vimsapp.registerchild.GenerateChildSchedule;

public class AddChildPresenterImpl implements AddChildPresenter {

     /*
     The presenter knows about the response model and the view model
     the view model is a data structure
     the response model is also a data structure

    1. user enters details
       system validates details and takes user forward
       user selects to enter caretaker info
       system presents caretaker entry form
       user enters caretaker details
       system validates details and takes user forward
       users previews their details
       user presses to register child

       TEST
       pass in a fake ResponseModel
       look at the view model
       if the values have been prepared well
       and also if the right methods executed

     */


    //AddChildPreviewScreen addChildPreviewScreen;
    AddChildViewModel viewModel;

    //The preview screen is a fragment
    //But because we have a view model the view will register for events through the viewmodel

    public AddChildPresenterImpl(AddChildViewModel viewModel) {
        this.viewModel = viewModel;
    }


    @Override
    public void presentValidationError(String error) {

       viewModel.setmFailMessage(error);

    }

    @Override
    public void presentRegistrationSuccess(GenerateChildSchedule.ResponseValue responseValue) {


        //1. Switch off the old presentation
        //present the visibility of the progress loading to off
        //present the visibility of the grid that holds the child detail info to off
        //present the visibility of the dashline of title off

        viewModel.setmIsShowProgressBar(false);
        viewModel.setmIsShowChildDetailGrid(false);
        viewModel.setmIsShowTitleDashLine(false);


        //2. switch on the new presentation
        //present the name and name label
        //present the colors of the name and label
        //present the wrapping of the success message
        //present the view visibility for success info on
        //present the animation of the tickcross to success
        //present the marking of current caretaker as registered

        String successDesc = ""+responseValue.getRegisteredChild().getFirstName()+" "+responseValue.getRegisteredChild().getLastName()+" registered successfully";
        String successDescSubtitle = "You can now view and vaccinate this child";

        viewModel.setmSuccessDescTitle(successDescSubtitle);
        viewModel.setmSuccessDescSubtitle(successDesc);
        viewModel.setmIsShowColorsOfDescTitleSubtitle(true);
        viewModel.setmIsShowChildSuccessWrapper(true);
        viewModel.setmIsShowSuccessAnimation(true);
        viewModel.setmIsMarkCaretakerAsRegistered(true);



    }



}

