package com.vims.vimsapp.repository_tests;


import com.vims.vimsapp.domain.patients.data.PatientsDataSource;
import com.vims.vimsapp.domain.patients.data.PatientsRepository;
import com.vims.vimsapp.domain.patients.data.local.PatientsLocalDataSource;
import com.vims.vimsapp.domain.patients.data.remote.vaccinated.PatientsRemoteDataSource;
import com.vims.vimsapp.domain.patients.entity.Child;
import com.vims.vimsapp.domain.vaccinations.entity.VaccinationCard;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class PatientsRepositoryTest {




    PatientsRepository patientsRepository;

    @Mock
    PatientsLocalDataSource patientsLocalDataSource;

    @Mock
    PatientsRemoteDataSource patientsRemoteDataSource;


    @Mock
    PatientsDataSource.SaveChildCallback saveChildCallback;



    @Before
    public void setUp(){

        MockitoAnnotations.initMocks(this);

        patientsRepository = PatientsRepository.getInstance(patientsRemoteDataSource,patientsLocalDataSource);



    }



    @Test
    public void saveChild_savesChild_toRepo() {
        // Given a stub task with title and description
         Child child = new Child();

         child.setFirstName("Augusto");
         child.setLastName("Sahi");
         child.setGender("boy");



        // When a task is saved to the tasks repository
        patientsRepository.saveChild(child, saveChildCallback);

        // Then the service API and persistent repository are called and the cache is updated

        Mockito.verify(patientsLocalDataSource).saveChild(child,saveChildCallback);

        assertThat(patientsRepository.mCachedChidren.size(), is(1));
    }








}
