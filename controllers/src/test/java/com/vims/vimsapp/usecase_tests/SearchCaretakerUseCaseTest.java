package com.vims.vimsapp.usecase_tests;


import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.domain.patients.data.PatientsRepository;
import com.vims.vimsapp.domain.patients.data.local.PatientsLocalDataSource;
import com.vims.vimsapp.domain.patients.interactor.vaccinated.SearchPatient;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SearchCaretakerUseCaseTest {


    @Mock
    UseCase.OutputBoundary<SearchPatient.ResponseValue> outputBoundary;

    /**
     * {@link ArgumentCaptor} is a powerful Mockito API to capture argument values and use them to
     * perform further actions or assertions on them.
     */
    @Captor
    private ArgumentCaptor<PatientsLocalDataSource.LoadCaretakersCallback> searchCallbackArgumentCaptor;



    SearchPatient searchCaretaker;

    @Mock
    PatientsRepository patientsRepository;



    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        searchCaretaker = SearchPatient.getInstance(patientsRepository);


        //Dummy Data
        //Valid Data
        //caretaker names dont exceed 50 chars


    }



    @Test
    public void search_careteker_valid_data_executes(){

        //this is where the usecase are executed
        //1. when search caretaker is called

        //Data
        CharSequence charSequence = "Jayson borrows";

        SearchPatient.RequestValues requestValues = new SearchPatient.RequestValues(charSequence);

        searchCaretaker.execute(requestValues, outputBoundary);
        Mockito.verify(patientsRepository).getCaretakers(searchCallbackArgumentCaptor.capture());




    }





}

