package com.vims.vimsapp.usecase_tests;


import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.domain.vaccinations.data.VaccinationsDataSource;
import com.vims.vimsapp.domain.vaccinations.data.VaccinationsRepository;
import com.vims.vimsapp.domain.vaccinations.entity.VaccinationCard;
import com.vims.vimsapp.domain.patients.interactor.vaccinations.ReadChildSchedule;
import com.vims.vimsapp.utilities.DateCalendarConverter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ReadChildScheduleUseCaseTest {


    @Mock
    UseCase.OutputBoundary<ReadChildSchedule.ResponseValue> outputBoundary;

    /**
     * {@link ArgumentCaptor} is a powerful Mockito API to capture argument values and use them to
     * perform further actions or assertions on them.
     */
    @Captor
    private ArgumentCaptor<VaccinationsDataSource.LoadAllVaccinationCardsCallback> readscheduleCallbackArgumentCaptor;


    @Captor
    private ArgumentCaptor<Date> dateArgumentCaptor;



    ReadChildSchedule readChildSchedule;

    @Mock
    VaccinationsRepository vaccinationsRepository;



    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        readChildSchedule = ReadChildSchedule.getInstance(vaccinationsRepository);


        //Dummy Data
        //Valid Data
        //caretaker names dont exceed 50 chars
    }



    @Test
    public void getsTheVaccinationCardsWhenRequestedByDate(){

        //this is where the usecase are executed
        //1. when search caretaker is called

        //Data
        Date dummyDate = Calendar.getInstance().getTime();


        ReadChildSchedule.VaccinationsRequestType requestType = ReadChildSchedule.VaccinationsRequestType.CHILD;
        ReadChildSchedule.RequestValues requestValues = new ReadChildSchedule.RequestValues(requestType);

        readChildSchedule.execute(requestValues, outputBoundary);


        Mockito.verify(vaccinationsRepository).getAllChildVaccinationCardsByDate(dateArgumentCaptor.capture(),readscheduleCallbackArgumentCaptor.capture());


        //Create some dummy Response Values
        List<VaccinationCard> vaccinationCards = new ArrayList<>();
        VaccinationCard vaccinationCard = new VaccinationCard();
        vaccinationCard.setDateCreated(dummyDate);
        vaccinationCards.add(vaccinationCard);


        //trigger the callback
        readscheduleCallbackArgumentCaptor.getValue().onCardsLoaded(vaccinationCards);

        //check the Responsevalue look at it open it

        assertThat(dummyDate, is(DateCalendarConverter.stringToDateWithoutTime(readChildSchedule.responseValue.getVaccinationEvents().get(0).getScheduledDate())));

        //assertEquals(dummyDate,);







    }





}

