package com.vims.vimsapp.usecase_tests;


import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.domain.patients.data.PatientsRepository;
import com.vims.vimsapp.domain.patients.data.local.PatientsLocalDataSource;
import com.vims.vimsapp.domain.patients.entity.Child;
import com.vims.vimsapp.domain.patients.interactor.vaccinated.RegisterChild;
import com.vims.vimsapp.domain.patients.interactor.vaccinations.GenerateChildSchedule;
import com.vims.vimsapp.utilities.DateCalendarConverterForView;

import org.junit.Before;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class RegisterChildUseCaseTest {


    @Mock
    UseCase.OutputBoundary<RegisterChild.ResponseValue> outputBoundary;

    /**
     * {@link ArgumentCaptor} is a powerful Mockito API to capture argument values and use them to
     * perform further actions or assertions on them.
     */
    @Captor
    private ArgumentCaptor<PatientsLocalDataSource.SaveChildCallback> searchCallbackArgumentCaptor;

    @Captor
    private ArgumentCaptor<Child> childArgumentCaptor;



    @Mock
    PatientsLocalDataSource.SaveChildCallback saveChildCallbackMock;


    RegisterChild registerChild;

    @Mock
    PatientsRepository patientsRepository;

    @Mock
    GenerateChildSchedule generateChildSchedule;



    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        registerChild = new RegisterChild(patientsRepository);


        //Dummy Data
        //Valid Data
        //caretaker names dont exceed 50 chars


    }



    @Test
    public void search_careteker_valid_data_executes(){

        //this is where the usecase are executed
        //1. when search caretaker is called


        Child child = new Child();
        child.setFirstName("logo");
        child.setLastName("bod");

        String dateofBirth = "15,7,2018";

        child.setDateOfBirth(DateCalendarConverterForView.stringToDateWithoutTime(dateofBirth));

        RegisterChild.RequestValues requestValues = new RegisterChild.RequestValues(child);

        registerChild.execute(requestValues, outputBoundary);
        Mockito.verify(patientsRepository).saveChild(childArgumentCaptor.capture(),searchCallbackArgumentCaptor.capture());


        Child mChild = childArgumentCaptor.getValue();


        assertThat(mChild.getDateOfBirth(), is(child.getDateOfBirth()));


    }





}

