package com.vims.vimsapp.local_repository_tests;




import com.vims.vimsapp.domain.patients.data.VaccinationsDataSource;
import com.vims.vimsapp.domain.patients.data.local.vaccinations.VaccinationCardDao;
import com.vims.vimsapp.domain.patients.data.local.vaccinations.VaccinationEventDao;
import com.vims.vimsapp.domain.patients.data.local.vaccinations.VaccinationsLocalDataSource;
import com.vims.vimsapp.domain.patients.data.local.vaccinations.VaccineOrderDao;
import com.vims.vimsapp.domain.patients.entity.vaccinations.VaccinationCard;
import com.vims.vimsapp.domain.patients.entity.vaccinations.VaccinationEvent;
import com.vims.vimsapp.domain.patients.entity.vaccinations.VaccinationStatus;
import com.vims.vimsapp.utilities.AppExecutors;
import com.vims.vimsapp.utilities.DateCalendarConverter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@RunWith(org.mockito.runners.MockitoJUnitRunner.class)
public class VaccinationLocalRepositoryTest {


    @Mock
    VaccinationCardDao vaccinationCardDao;

    @Mock
    VaccinationEventDao vaccinationEventDao;

    @Mock
    VaccineOrderDao annualCoverageDao;

   // @Mock
   // VaccinationsDataSource.SaveChildVaccinationCardCallback saveChildVaccinationCardCallback;

    @Mock
    VaccinationsDataSource.SaveVaccinationCallback saveVaccinationCallbackMock;



   // @Mock
   // VaccinationsDataSource.LoadAllVaccinationCardsCallback loadAllVaccinationCardsCallback;



   // @Captor
   // ArgumentCaptor<VaccinationsDataSource.SaveChildVaccinationCardCallback> cardCallbackArgumentCaptor;


    VaccinationsLocalDataSource vaccinationsLocalDataSource;



    AppExecutors appExecutors;


    @Before
    public void setUp(){


        appExecutors = new AppExecutors();
        vaccinationsLocalDataSource = VaccinationsLocalDataSource.getInstance(appExecutors,vaccinationCardDao,vaccinationEventDao,annualCoverageDao);


    }


    @Test
    public void savesChildCardByCardDao(){


        VaccinationCard vaccinationCard = new VaccinationCard();
        vaccinationCard.setDateCreated(DateCalendarConverter.dateToStringWithoutTime(new Date()));
        vaccinationCard.setChildId(UUID.randomUUID().toString());


        VaccinationEvent vaccinationEvent = new VaccinationEvent();

        String dateVacc = DateCalendarConverter.dateToStringWithoutTime(new Date());


        vaccinationEvent.setVaccinationDate(dateVacc);
        vaccinationEvent.setVaccinationStatus(VaccinationStatus.DONE);
        vaccinationEvent.setVaccineName("Polio 0");

        ArrayList<VaccinationEvent> vaccinationEvents = new ArrayList<>();
        vaccinationEvents.add(vaccinationEvent);
        vaccinationCard.setVaccinationEvents(vaccinationEvents);


       // vaccinationsLocalDataSource.saveChildVaccinationCard(vaccinationCard, saveChildVaccinationCardCallback);


        Mockito.verify(vaccinationCardDao).Insert(vaccinationCard);
        Mockito.verify(vaccinationEventDao).Insert(vaccinationEvents);



    }



    @Test
    public void getsAllChildCardsByCardDao(){
        Date date = Calendar.getInstance().getTime();
      //  vaccinationsLocalDataSource.getAllChildVaccinationCardsByDate(date,loadAllVaccinationCardsCallback);
        //Mockito.verify(vaccinationCardDao).GetAllChildVaccinationCards(date);

    }



    @Test
    public void updatesVaccinationEventByCardDao(){


        VaccinationEvent vaccinationEvent = new VaccinationEvent();

        //vaccinationsLocalDataSource.saveVaccination(vaccinationEvent,saveVaccinationCallbackMock);

        Mockito.verify(vaccinationEventDao).Update(vaccinationEvent);



    }







}
