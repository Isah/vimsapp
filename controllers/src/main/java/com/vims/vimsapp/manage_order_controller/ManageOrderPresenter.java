package com.vims.vimsapp.manage_order_controller;

import com.vims.vimsapp.manageorders.RequestOrder;
import com.vims.vimsapp.manageorders.ViewOrders;

public interface ManageOrderPresenter {

    //To repackage the output data into viewable form

    void presentOrderFailure(String error);
    void presentOrderSuccess(RequestOrder.ResponseValue responseValue);
    void presentOrderValidationFailure(String error);
    void previewSentOrder(ViewOrders.ResponseValue responseValue);
    void previewNewOrder(ViewOrders.ResponseValue responseValue);

    /*
    1. user enters details
       system validates details and takes user forward
       user selects to enter caretaker info
       system presents caretaker entry form
       user enters caretaker details
       system validates details and takes user forward
       users previews their details
       user presses to register child
     */



}
