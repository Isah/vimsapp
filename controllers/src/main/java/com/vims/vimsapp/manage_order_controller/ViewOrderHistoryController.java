package com.vims.vimsapp.manage_order_controller;


import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.manageorders.RequestOrder;
import com.vims.vimsapp.manageorders.VaccinePlan;
import com.vims.vimsapp.manageorders.ViewOrders;

import java.util.List;

public class ViewOrderHistoryController {

    /**
     * Information Hiding
     * The Controller shouldn't know too much about the usecase
     * it only knows the Request and ResponseModel but not the usecase
     * so we use the InputBoundary
     *
     * The Controller is the delivery mechanism
     */


    ViewOrderHistoryPresenter viewOrderHistoryPresenter;
    UseCase<ViewOrders.RequestValues,ViewOrders.ResponseValue> viewOrderUseCaseInputBoundary;


    private static ViewOrderHistoryController INSTANCE = null;

    private ViewOrderHistoryController(ViewOrderHistoryPresenter viewOrderHistoryPresenter,
                                       UseCase<ViewOrders.RequestValues,ViewOrders.ResponseValue> viewOrderUseCaseInputBoundary) {


        this.viewOrderHistoryPresenter = viewOrderHistoryPresenter;
        this.viewOrderUseCaseInputBoundary = viewOrderUseCaseInputBoundary;

    }




    public static ViewOrderHistoryController getInstance(ViewOrderHistoryPresenter viewOrderHistoryPresenter,
                                                         UseCase<ViewOrders.RequestValues,ViewOrders.ResponseValue> viewOrderUseCaseInputBoundary) {

        if (INSTANCE == null) {
            INSTANCE = new ViewOrderHistoryController(viewOrderHistoryPresenter,viewOrderUseCaseInputBoundary);
        }
        return INSTANCE;
    }



    public void viewRecentOrders(){
        ViewOrders.RequestValues requestValues = new ViewOrders.RequestValues(true);

        viewOrderUseCaseInputBoundary.execute(requestValues, new UseCase.OutputBoundary<ViewOrders.ResponseValue>() {
            @Override
            public void onSuccess(ViewOrders.ResponseValue result) {

                viewOrderHistoryPresenter.showRecentOrders(result);

            }

            @Override
            public void onError(String errorMessage) {

            }
        });
    }



    public void viewAllPastOrders(){
        ViewOrders.RequestValues requestValues = new ViewOrders.RequestValues(true);

        viewOrderUseCaseInputBoundary.execute(requestValues, new UseCase.OutputBoundary<ViewOrders.ResponseValue>() {
            @Override
            public void onSuccess(ViewOrders.ResponseValue result) {

                viewOrderHistoryPresenter.showRecentOrders(result);

            }

            @Override
            public void onError(String errorMessage) {

            }
        });
    }



}
