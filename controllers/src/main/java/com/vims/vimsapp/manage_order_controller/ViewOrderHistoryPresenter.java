package com.vims.vimsapp.manage_order_controller;

import com.vims.vimsapp.manageorders.RequestOrder;
import com.vims.vimsapp.manageorders.ViewOrders;

public interface ViewOrderHistoryPresenter {

    //To repackage the output data into viewable form

    void showAllOrders(ViewOrders.ResponseValue responseValue);
    void showRecentOrders(ViewOrders.ResponseValue responseValue);

    /*
    1. user enters details
       system validates details and takes user forward
       user selects to enter caretaker info
       system presents caretaker entry form
       user enters caretaker details
       system validates details and takes user forward
       users previews their details
       user presses to register child
     */



}
