package com.vims.vimsapp.manage_order_controller;


import android.util.Log;

import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.addchild_controller.AddChildPresenter;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.manageorders.RequestOrder;
import com.vims.vimsapp.manageorders.VaccinePlan;
import com.vims.vimsapp.manageorders.ViewOrders;
import com.vims.vimsapp.registerchild.GenerateChildSchedule;
import com.vims.vimsapp.utilities.AgeCalculator;
import com.vims.vimsapp.utilities.DateCalendarConverter;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ManageOrderController {

    /**
     * Information Hiding
     * The Controller shouldn't know too much about the usecase
     * it only knows the Request and ResponseModel but not the usecase
     * so we use the InputBoundary
     *
     * The Controller is the delivery mechanism
     */



    ManageOrderPresenter manageOrderPresenter;
    UseCase<RequestOrder.RequestValues,RequestOrder.ResponseValue> requestOrderUseCaseInputBoundary;
    UseCase<ViewOrders.RequestValues,ViewOrders.ResponseValue> viewOrderUseCaseInputBoundary;


    private static ManageOrderController INSTANCE = null;

    private ManageOrderController(ManageOrderPresenter manageOrderPresenter,
                                 UseCase<RequestOrder.RequestValues,RequestOrder.ResponseValue> requestOrderUseCaseInputBoundary,
                                 UseCase<ViewOrders.RequestValues,ViewOrders.ResponseValue> viewOrderUseCaseInputBoundary) {

        this.manageOrderPresenter = manageOrderPresenter;
        this.requestOrderUseCaseInputBoundary = requestOrderUseCaseInputBoundary;
        this.viewOrderUseCaseInputBoundary = viewOrderUseCaseInputBoundary;

    }




    public static ManageOrderController getInstance(ManageOrderPresenter manageOrderPresenter,
                                                    UseCase<RequestOrder.RequestValues,RequestOrder.ResponseValue> requestOrderUseCaseInputBoundary,
                                                    UseCase<ViewOrders.RequestValues,ViewOrders.ResponseValue> viewOrderUseCaseInputBoundary) {

        if (INSTANCE == null) {
            INSTANCE = new ManageOrderController(manageOrderPresenter,requestOrderUseCaseInputBoundary,viewOrderUseCaseInputBoundary);
        }
        return INSTANCE;
    }









    public void makeOrderSelection(int orderMonth){

        Log.d("OrderSelection","order: "+orderMonth);

        final ViewOrders.RequestValues requestValues = new ViewOrders.RequestValues(false);
        requestValues.setSubmitOrder(true);
        requestValues.setOrderMonth(orderMonth);

        viewOrderUseCaseInputBoundary.execute(requestValues, new UseCase.OutputBoundary<ViewOrders.ResponseValue>() {
            @Override
            public void onSuccess(ViewOrders.ResponseValue result) {
                Log.d("OrderSelection","order success: ");

                manageOrderPresenter.previewNewOrder(result);

            }

            @Override
            public void onError(String errorMessage) {
                Log.d("OrderSelection","order error: "+errorMessage);


            }
        });


    }

    public void submitOrder(List<VaccinePlan> plans,
                            int monthOfOrder,
                            String username,
                            String hospitalId,
                            String hospitalName,
                            String hospitalAddress
                             ){

        RequestOrder.RequestValues requestValues = new RequestOrder.RequestValues(hospitalId,username,plans,monthOfOrder);
        requestValues.setHospitalName(hospitalName);
        requestValues.setHospitalAddress(hospitalAddress);


        requestOrderUseCaseInputBoundary.execute(requestValues, new UseCase.OutputBoundary<RequestOrder.ResponseValue>() {
            @Override
            public void onSuccess(RequestOrder.ResponseValue result) {

                manageOrderPresenter.presentOrderSuccess(result);

            }

            @Override
            public void onError(String errorMessage) {

                manageOrderPresenter.presentOrderFailure(errorMessage);
            }
        });

       }


    public void viewSentOrder(String orderId){


        ViewOrders.RequestValues requestValues = new ViewOrders.RequestValues(false);
        requestValues.setSubmitOrder(true);
        requestValues.setOrderId(orderId);

        viewOrderUseCaseInputBoundary.execute(requestValues, new UseCase.OutputBoundary<ViewOrders.ResponseValue>() {
            @Override
            public void onSuccess(ViewOrders.ResponseValue result) {

             manageOrderPresenter.previewSentOrder(result);

            }

            @Override
            public void onError(String errorMessage) {

            }
        });



    }




}
