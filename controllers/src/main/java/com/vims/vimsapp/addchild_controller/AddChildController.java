package com.vims.vimsapp.addchild_controller;


import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.utilities.AgeCalculator;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.registerchild.GenerateChildSchedule;

import java.util.Calendar;
import java.util.Date;

public class AddChildController {

    /**
     * Information Hiding
     * The Controller shouldn't know too much about the usecase
     * it only knows the Request and ResponseModel but not the usecase
     * so we use the InputBoundary
     *
     * The Controller is the delivery mechanism
     */

    AddChildPresenter registerChildPresenter;
    UseCase<GenerateChildSchedule.RequestValues,GenerateChildSchedule.ResponseValue> useCaseInputBoundary;

    public AddChildController(AddChildPresenter registerChildPresenter, UseCase<GenerateChildSchedule.RequestValues,GenerateChildSchedule.ResponseValue> useCaseInputBoundary) {
        this.registerChildPresenter = registerChildPresenter;
        this.useCaseInputBoundary = useCaseInputBoundary;
    }

    public void registerChild(String firstName,
                              String lastName,
                              String gender,
                              String weight1,
                              String weight2,
                              String dateOfBirth,
                              String caretakerId,
                              String hospitalId){


           //1. Prepare Data for the use case
           Child child = new Child();


           child.setFirstName(firstName);
           child.setLastName(lastName);

           //10 21, 2018

           String vals[] = dateOfBirth.split("/");//month//day//year

           int month = Integer.parseInt(vals[0]);
           int day = Integer.parseInt(vals[1]);
           int year = Integer.parseInt(vals[2]);

           Calendar calendar = Calendar.getInstance();
           calendar.set(year,month,day);

           child.setDateOfBirth(DateCalendarConverter.dateToStringWithoutTime((calendar.getTime())));


           AgeCalculator.Age myAge = new AgeCalculator().calculateAge(DateCalendarConverter.stringToDateWithoutTime(child.getDateOfBirth()));
           String stringAge;
           if(myAge.getYears()==0 && myAge.getMonths() == 0){

               int days = myAge.getDays();

               stringAge = "" + days+" days";
           }else {

               stringAge = "" + Integer.toString(myAge.getYears()) + "-" + Integer.toString(myAge.getMonths());

           }

           child.setAge(stringAge);
           child.setHospitalId(hospitalId);//preferenceHandler.getPref(PreferenceHandler.USER_HOSPITAL_ID

           child.setGender(gender);

           String w1 = weight1;//getmBirthWeight().getValue().toString();
           String w2 = weight2;//getmBirthWeightFloat().getValue().toString();

           String w = ""+w1+"."+w2;
           float myweight = Float.parseFloat(w);
           child.setWeight(myweight);
           child.setCaretakerId(caretakerId);
           child.setRegistrationDate(DateCalendarConverter.dateToStringWithoutTime(new Date()));



           GenerateChildSchedule.RequestValues requestValues = new GenerateChildSchedule.RequestValues(child);


           //2. Execute the usecase
           registerChild(requestValues);

       }





    private void registerChild(GenerateChildSchedule.RequestValues requestValues){

                                                    //this is the delivery mechanism
        useCaseInputBoundary.execute(requestValues, new UseCase.OutputBoundary<GenerateChildSchedule.ResponseValue>() {
            @Override
            public void onSuccess(GenerateChildSchedule.ResponseValue result) {

                //call the presenter to present the success info
                registerChildPresenter.presentRegistrationSuccess(result);

            }

            @Override
            public void onError(String e) {
                //call the presenter to present the error info
                registerChildPresenter.presentValidationError(e);

            }
        });


    }





}
