package com.vims.vimsapp.addchild_controller;

import com.vims.vimsapp.registerchild.GenerateChildSchedule;

public interface AddChildPresenter {

    //To repackage the output data into viewable form

    void presentValidationError(String error);
    void presentRegistrationSuccess(GenerateChildSchedule.ResponseValue responseValue);


    /*
    1. user enters details
       system validates details and takes user forward
       user selects to enter caretaker info
       system presents caretaker entry form
       user enters caretaker details
       system validates details and takes user forward
       users previews their details
       user presses to register child
     */



}
