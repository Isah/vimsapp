# Add project specific ProGuard rules here.

-keepclassmembers class android.support.design.internal.BottomNavigationMenuView {
    boolean mShiftingMode;
}
-keepclassmembers class android.support.design.internal.BottomNavigationItemView {
    int mShiftAmount;
}

-ignorewarnings

# Rules for keeping the app from crashing when dealing with app target for menu items
-keep class android.support.v7.widget.Toolbar { *** mMenuView; }
-keep class android.support.v7.widget.ActionMenuView { *** mPresenter; }
-keep class android.support.v7.widget.ActionMenuPresenter { *** mOverflowButton; }

# By default, the flags in this file are appended to flags specified
# in D:\Projects\Android\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
