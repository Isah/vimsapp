package com.vims.vimsapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.vims.vimsapp.view.users.LoginActivity;
import com.vims.vimsapp.view.users.SQLiteHandler;
import com.vims.vimsapp.view.users.SessionManager;

import java.util.HashMap;

import vimsapp.R;

public class AccountActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        db = new SQLiteHandler(getApplicationContext());



        NavigationView navigationView = findViewById(R.id.nav_view);
        //  navigationView.setPadding(0,140,0,0);
        // CoordinatorLayout.LayoutParams layoutParams = ( CoordinatorLayout.LayoutParams) navigationView.getLayoutParams();
        // layoutParams.setMargins(0,144,0,0);
        // navigationView.setLayoutParams(layoutParams);
        navigationView.setNavigationItemSelectedListener(new MyVerticalBottomViewItemsListner());


        setUpHeaderContent(navigationView);


        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

    }


    private SQLiteHandler db;
    private SessionManager session;


    private void logoutUser() {
        session.setLogin(false);

        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void setUpHeaderContent(NavigationView navigationView){

        View headerView = navigationView.getHeaderView(0);
        TextView txtName = headerView.findViewById(R.id.profile_name);
        TextView txtEmail = headerView.findViewById(R.id.profile_email);

        //Fetching user details from sqlite
        HashMap<String, String> user = db.getUserDetails();

        String name = user.get("name");
        String email = user.get("email");

        // Displaying the user details on the screen
        txtName.setText(name);
        txtEmail.setText(email);

    }

    private class MyVerticalBottomViewItemsListner implements NavigationView.OnNavigationItemSelectedListener{

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            item.setChecked(true);

            selectNavItem(item);

            return true;

        }
    }


    private void selectNavItem(MenuItem item){
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            // Handle the camera action
        }
        else if (id == R.id.nav_settings) {
            Intent intent = new Intent(getApplicationContext(), ActivitySettings.class);
            startActivity(intent);

        } else if (id == R.id.nav_help) {

        } else if (id == R.id.nav_logout) {
            logoutUser();
        }

    }





}
