package com.vims.vimsapp;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.view.MyFonts;

import vimsapp.R;


public class ActivitySettings extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
       // toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_close));
        toolbar.setTitle("Account");
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_clear_black_24dp));
        setSupportActionBar(toolbar);


        CollapsingToolbarLayout collapsing  = findViewById(R.id.collapsing_toolbar);
        collapsing.setTitleEnabled(false);


        displayProfile();

        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null){

            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        if(itemId == android.R.id.home){

            NavUtils.navigateUpFromSameTask(this);
        }


        return super.onOptionsItemSelected(item);


    }


    private void displayProfile(){

      TextView tvName = findViewById(R.id.profile_name);
      TextView tvEmail =  findViewById(R.id.profile_email);

      tvName.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
        tvEmail.setTypeface(MyFonts.getTypeFace(getApplicationContext()));

      PreferenceHandler preferenceHandler = PreferenceHandler.getInstance(getApplicationContext());
      String userName = preferenceHandler.getPref(PreferenceHandler.USER_NAME);
      String userEmail = preferenceHandler.getPref(PreferenceHandler.USER_EMAIL);

      tvName.setText(userName);
      tvEmail.setText(userEmail);

    }




}
