package com.vims.vimsapp.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import com.vims.vimsapp.MainActivity;
import com.vims.vimsapp.viewcoverage.CoverageReport;
import com.vims.vimsapp.utilities.DateCalendarConverter;

import java.util.Date;

import vimsapp.R;

/**
 * Implementation of App Widget functionality.
 */
public class IWidgetProvider extends AppWidgetProvider {

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, CoverageReport coverageReport,long daystogo,  int appWidgetId) {

        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.iwidget_provider);

        /**
         *Remote views are linked to a pending intent that will launch once the view is clecked
         * Pending Intent is a wrapper around an intent to allow other other apps to have access and run
         * that intent in other applications
         */
        Intent intent = new Intent(context,MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,0,intent,0);
        views.setOnClickPendingIntent(R.id.widget_layout_linear,pendingIntent);



        //STATISTICS
        // original String nextdate =  DateCalendarConverter.dateToStringWithoutTime(coverageReport.getNextImmunisationDate());
        String nextdate = DateCalendarConverter.dateToStringWithoutTime(new Date());
        String days = "In "+Long.toString(daystogo)+" days";
        String coverage = coverageReport.getCoveragePercentage()+"%";



        views.setTextViewText(R.id.appwidget_text_value_date, nextdate);
        views.setTextViewText(R.id.appwidget_text_value_days, days);
        views.setTextViewText(R.id.appwidget_text_value_coverage, coverage);



        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }


    /** called from the service to run the update using appwidget manager */
    public static void updateImmunisationWidget(Context context, AppWidgetManager appWidgetManager, CoverageReport coverageReport, long daystogo, int[] appwidgetids){

      for(int appwidgetid: appwidgetids) {
          updateAppWidget(context, appWidgetManager, coverageReport,daystogo,appwidgetid);
      }

    }



    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        /** 1. There may be multiple widgets active (for same app), so update all of them
        // this method is called when a new widget has been created as well as every update interval
          */

        // for (int appWidgetId : appWidgetIds) {
        //    updateAppWidget(context, appWidgetManager, appWidgetId);
        // }

        Log.d("WidgetService","WidgetService started");


        //when widget created or on interval start the service that will get the next date
     //   ImmunisationScheduleService.startImmunisationService(context);

        /** 2. the service will run a query and when done will call
         *    updateImmunisationWidget()
         * */

    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

