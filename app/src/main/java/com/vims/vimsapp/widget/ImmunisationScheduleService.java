package com.vims.vimsapp.widget;

import android.app.IntentService;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.vims.vimsapp.UseCase;

import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.viewcoverage.CoverageReport;
import com.vims.vimsapp.viewcoverage.GenerateReport;
import com.vims.vimsapp.utilities.AppExecutors;
import com.vims.vimsapp.utilities.DateCalendarConverter;

import java.util.Date;


public class ImmunisationScheduleService extends IntentService {


    AppExecutors appExecutors = new AppExecutors();

    public ImmunisationScheduleService() {
        super("Immunisation");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        if(intent!=null) {

            Log.d("WidgetService","Intent Started");


            //do background work here
            String action = intent.getAction();

                if(action_next_date.equals(action)){

                   handleAction();

                }


        }

    }

    public static final String action_next_date = "ACTION_GET_NEXT_DATE";

    //called from the widget provider on everal interval to chack next date
    public static void startImmunisationService(Context context){
       Intent intent = new Intent(context,ImmunisationScheduleService.class);
       intent.setAction(action_next_date);
       context.startService(intent);


    }



    GenerateReport generateReport;


    public void handleAction() {


        Runnable loginRunnable = new Runnable() {
            @Override
            public void run() {

                generateReport = Injection.provideGenerateReport(getApplication());

                GenerateReport.ReportType reportType = GenerateReport.ReportType.YEARLY;
                String yearOfReport = DateCalendarConverter.dateToStringWithoutTime(new Date());

                GenerateReport.RequestValues requestValues = new GenerateReport.RequestValues(reportType,yearOfReport);
                requestValues.setDataChanged(true);

                generateReport.execute(requestValues, new UseCase.OutputBoundary<GenerateReport.ResponseValue>() {
                    @Override
                    public void onSuccess(GenerateReport.ResponseValue result) {

                        CoverageReport coverageReport = result.getCoverageReport();

                       // Log.d("WidgetScheduleService","date: "+coverageReport.getNextImmunisationDate());

                       // long daysToGo =  DateCalendarConverter.calculateDifferenceInDates(coverageReport.getNextImmunisationDate(),new Date());

                        long daysToGo =  0;
                        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getApplication());
                        int[] ids = appWidgetManager.getAppWidgetIds(new ComponentName(getApplication(), IWidgetProvider.class));

                        IWidgetProvider.updateImmunisationWidget(getApplication(),appWidgetManager,coverageReport,daysToGo,ids);


                    }

                    @Override
                    public void onError(String errorMessage) {

                    }
                });



            }
        };


        appExecutors.diskIO().execute(loginRunnable);


    }




}
