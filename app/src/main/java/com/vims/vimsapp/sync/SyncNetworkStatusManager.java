package com.vims.vimsapp.sync;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;

public class SyncNetworkStatusManager {

    private static SyncNetworkStatusManager INSTANCE = null;


    Context context;

    private SyncNetworkStatusManager(Context context){
        this.context = context;
    }

    public static SyncNetworkStatusManager getInstance(Context context) {

        if (INSTANCE == null) {
            INSTANCE = new SyncNetworkStatusManager(context);
        }
        return INSTANCE;
    }


    public  boolean isNetworkConnected() {
        boolean isWifiConn = false;
        boolean isMobileConn = false;

        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        for (Network network : connMgr.getAllNetworks()) {
            NetworkInfo networkInfo = connMgr.getNetworkInfo(network);
            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                isWifiConn |= networkInfo.isConnected();
            }
            if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                isMobileConn |= networkInfo.isConnected();
            }
        }

        return isWifiConn;
    }

    public boolean isWifiOnline() {
        boolean isWifiConn = false;
        boolean isMobileConn = false;

        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
            isWifiConn |= networkInfo.isConnected();
        }


        return isWifiConn;
    }

    public boolean isMobileOnline() {

        boolean isMobileConn = false;

        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
            isMobileConn |= networkInfo.isConnected();
        }

        return isMobileConn;
    }




}
