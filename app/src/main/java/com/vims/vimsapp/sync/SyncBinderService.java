package com.vims.vimsapp.sync;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.vims.vimsapp.UseCase;



import com.vims.vimsapp.entities.child.Caretaker;

import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.registerchild.AddChildDataSource;
import com.vims.vimsapp.registerchild.AddChildLocalDataSource;
import com.vims.vimsapp.registerchild.AddChildRemoteRetrofitDataSource;
import com.vims.vimsapp.registerchild.AddChildRepository;
import com.vims.vimsapp.registerchild.RegisterCaretaker;
import com.vims.vimsapp.searchchild.ViewCaretakers;
import com.vims.vimsapp.searchchild.ViewChildRemoteRetrofitDataSource;
import com.vims.vimsapp.utilities.AppExecutors;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.injection.ModuleRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;


public class SyncBinderService extends Service{


    private AppExecutors appExecutors;

    //save remote,local    get remote

    AddChildRepository addChildRepository;
    AddChildRemoteRetrofitDataSource addChildRemoteRetrofitDataSource;
    AddChildLocalDataSource addChildLocalDataSource;

    ViewChildRemoteRetrofitDataSource viewChildRepository;

    RegisterCaretaker registerCaretaker;


    PreferenceHandler preferenceHandler;



    public SyncBinderService() {
    }

    public SyncBinderService(AppExecutors appExecutors,
                             AddChildRepository addChildRepository,
                             AddChildRemoteRetrofitDataSource patientsRemoteDataSource,
                             AddChildLocalDataSource patientsLocalDataSource,
                             ViewChildRemoteRetrofitDataSource viewChildRepository
    ) {

        this.appExecutors = appExecutors;
        this.addChildRemoteRetrofitDataSource = patientsRemoteDataSource;
        this.addChildLocalDataSource = patientsLocalDataSource;
        this.viewChildRepository = viewChildRepository;
    }

    //1. WHat the Client will use
    //1. Binder given to clients to bind to services
    private final IBinder mBinder = new LocalBinder();


    /**2
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public SyncBinderService getMySyncService() {
            // Return this instance of LocalService so clients can call public methods
            return SyncBinderService.this;
        }
    }

    //2. How to implement binding
    // To provide binding for a service i need to implement onBind()
    // returns a IBinder that provides a programming interface that clients
    // can use to interact with the service

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        if(intent != null){


            /*
            passWord = intent.getStringExtra("pwd");
            userName = intent.getStringExtra("usr");
            HOST = intent.getStringExtra("host");
            HOST_ADDRESS = intent.getStringExtra("host_address");

            Log.d("SERVICE CONN BOUND: ", "time: "+new Date());
            */

        }
        return mBinder;
    }





    @Override
    public void onCreate() {
        super.onCreate();
       // started = true;



        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                Log.d("SERVICE CONN EST: ", "time: "+new Date());

                notifyConnectionEstablished();


            }
        };
        handler.postDelayed(runnable,5000);

        appExecutors = new AppExecutors();
        appExecutors.diskIO().execute(runnable);


    }


    @Override
    public void onDestroy() {
        super.onDestroy();
      //  stopped = true;
    }



    //1. push plain caretakers
    //2. push plain kids
    //3. push events
    //4. get all caretakers from servers
    //5. clean the local database
    //6. update the local database


    // methods to be used by clients


    /** caretaker sync mobile 1 = 22nd jan
        if i add a caretaker ive changed my local sync date
        so i should compare the latest caretaker registration date time stamp  and
        the caretaker sync date time stamp
     * if the caretaker reg timestamp is less get the caretakers whose reg is less than the sync time stamp
     *
     * 2.
     *
     * @param patientsCallback
     */

    private final int SYNC_MODE_1_CARETAKER = 1;
    private final int SYNC_MODE_2_CHILD = 2;
    private final int SYNC_MODE_3_EVENT = 3;






    public void syncData(boolean isFronLogin, final AddChildDataSource.SyncPatientsCallback patientsCallback){

            addChildRemoteRetrofitDataSource = ModuleRepository.provideAddChildRemoteRetrofitRepository(getApplication());
            addChildLocalDataSource = ModuleRepository.provideAddChildLocalDataSource(getApplication());

            addChildRepository = ModuleRepository.provideAddChildRepository(getApplication());
            viewChildRepository = ModuleRepository.provideViewChildRemoteRetrofitRepository(getApplication());
            registerCaretaker = Injection.provideRegisterCaretakerUseCase(getApplication());

            preferenceHandler = PreferenceHandler.getInstance(getApplicationContext());

            if(isFronLogin){

                sync4PatientsDataIn(patientsCallback);

            }else {
                sync1Caretakers(patientsCallback);
            }

        //Need to know when to get the data

    }


    private void sync1Caretakers(final AddChildDataSource.SyncPatientsCallback syncPatientsCallback){
        //2. Loop all the caretakers
        //3. On each caretaker save caretaker remotely

        ViewCaretakers viewCaretakers = Injection.provideViewCaretakersUseCase(getApplication());
        ViewCaretakers.RequestValues requestValues = new ViewCaretakers.RequestValues(true);
        viewCaretakers.execute(requestValues, new UseCase.OutputBoundary<ViewCaretakers.ResponseValue>() {
            @Override
            public void onSuccess(ViewCaretakers.ResponseValue result) {

                List<Caretaker> mycaretakers = result.getmCaretakers();

                //if we have caretakers to update
                if(!mycaretakers.isEmpty()){
                    //now save the full caretakers
                    addChildRemoteRetrofitDataSource.saveCaretakers(result.getmCaretakers(), new AddChildDataSource.SaveCaretakersCallback() {
                        @Override
                        public void onCaretakersSaved(String onSavedMessage) {

                            addChildLocalDataSource.updateSync(new Date().getTime());
                            //syncPatientsCallback.onSynced("sync 1 done!"+onSavedMessage);

                            sync4PatientsDataIn(syncPatientsCallback);
                        }

                        @Override
                        public void onFailed(String error) {


                            syncPatientsCallback.onSyncFailed(""+error);


                        }
                    });

                }



            }

            @Override
            public void onError(String errorMessage) {
                //data is upto date
                //syncPatientsCallback.onSyncFailed(""+errorMessage);

                //no caretakers available for syncing
                //maybe there are some for bringing in
                sync4PatientsDataIn(syncPatientsCallback);

            }
        });


    }


    /*
    private void sync2Children(List<Child> children,List<VaccinationEvent> vevents_for_sync , PatientsDataSource.SyncPatientsCallback patientsCallback) {

                  Log.d("SyncedCaretakers","size children: "+children.size());
                    //for each child get a vaccination card



                    addChildRemoteRetrofitDataSource.saveChildren(children, new PatientsDataSource.SaveChildrenCallback() {
                        @Override
                        public void onChildrenSaved(List<Child> child) {

                            patientsCallback.onSynced(" sync 2 done");

                            //3.1 as events have been prepared
                            //3.2 push the events
                            sync3Events(vevents_for_sync,patientsCallback);


                        }

                        @Override
                        public void onSaveFailed(String errorMsg) {

                            patientsCallback.onSyncFailed("sync 2 failed");

                        }
                    });


    }
    private void sync3Events(List<VaccinationEvent> vaccinationEvents, PatientsDataSource.SyncPatientsCallback patientsCallback){


      vaccinationsRemoteDataSource.saveVaccinationEvents(vaccinationEvents, new VaccinationsDataSource.SaveChildVaccinationEventCallback() {
          @Override
          public void onChildVaccinationEventsSaved(List<VaccinationEvent> vaccinationEvents) {


              patientsCallback.onSynced("sync 3 done");


              //4. sync all the data from the server to local database
              sync4PatientsDataIn(patientsCallback);

          }

          @Override
          public void onFailed(String error) {

              patientsCallback.onSyncFailed("sync 3 failed");

          }
      });



    }

    */


    private boolean isDataAccurate(List<Caretaker> caretakers){

        for (Caretaker caretaker : caretakers) {

            if(!caretaker.getChildren().isEmpty() && caretaker.getChildren()!=null) {

                for (Child child : caretaker.getChildren()) {

                    if(child.getVaccinationEvents()!= null && !child.getVaccinationEvents().isEmpty()) {
                        //return true;
                    }else {
                        return false;
                    }

                }
            }

        }

        return true;

    }


    int noOfCaretakersWithChildren;
    int sizeOfCaretakersWithChildren;
    private void sync4PatientsDataIn(final AddChildDataSource.SyncPatientsCallback syncPatientsCallback){
        noOfCaretakersWithChildren =0;
        //5. get all the caretakers betweeen the last 2 years
        //6. wipe out all the current caretakers and load the new ones

        viewChildRepository.getCaretakers(new ViewChildRemoteRetrofitDataSource.LoadCaretakersCallback() {
            @Override
            public void onCaretakersLoaded(final List<Caretaker> caretakers) {

                ArrayList<Caretaker> caretakersIn = new ArrayList<>(new HashSet<>(caretakers));


                if(isDataAccurate(caretakersIn)) {

                    clearLocalDatabase();

                    //after getting all the caretakers thats when we clear all the local database
                    addChildRepository.saveCaretakers(caretakersIn, new AddChildDataSource.SaveCaretakersCallback() {
                        @Override
                        public void onCaretakersSaved(String onSavedMessage) {

                            //Log.d("RemoteCaretakers:","Received Now Inside: "+onSavedMessage);

                            //To mark current caretaker reg to use when deciding to refresh list/not
                            preferenceHandler.setIntValue(PreferenceHandler.IS_CARETAKER_REGISTERED_INT, 1);

                            addChildLocalDataSource.updateSync(new Date().getTime());
                            syncPatientsCallback.onSynced(" Data Synced successfully!");


                        }

                        @Override
                        public void onFailed(String errorMessage) {
                            syncPatientsCallback.onSyncFailed(errorMessage);

                        }
                    });

                }else{ //Data is InAccurate

                    syncPatientsCallback.onSyncFailed("Data is corrupted!");

                }


            }

            @Override
            public void onDataNotAvailable(String msg) {

                syncPatientsCallback.onSyncFailed(msg);


            }
        });





    }


    private void clearLocalDatabase(){
    addChildLocalDataSource.deleteAllCaretakers();
    }





    //Callback to check whether connection/binded is established to do other work
    public interface OnConnectionEstablishedListener {
        void onConnectionEstablished();
    }

    private OnConnectionEstablishedListener mListener;

    private boolean mIsConnectionEstablished = false;


    private void notifyConnectionEstablished() {

       mIsConnectionEstablished = true;

        if(mListener != null) {
            mListener.onConnectionEstablished();
        }
    }


    //the one that impl this interface will get the call back
    public void setOnConnectionEstablishedListener(OnConnectionEstablishedListener listener) {

        mListener = listener;

        // Already connected to server. Notify immediately.
        if(mIsConnectionEstablished) {
            mListener.onConnectionEstablished();
        }
    }






}
