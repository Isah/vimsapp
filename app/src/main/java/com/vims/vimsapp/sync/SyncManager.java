package com.vims.vimsapp.sync;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import com.vims.vimsapp.registerchild.AddChildDataSource;

public class SyncManager implements SyncBinderService.OnConnectionEstablishedListener{


    private SyncBinderService binderService;
    boolean mBound = false;

    public SyncManager(Context context) {
        if(!mBound) {
            bindtoLocalService(context);
        }

    }


    private void bindtoLocalService(Context context){

        //  myContext =  preferenceHandler.getActivityContext();
        // Bind to LocalService
        Intent intent = new Intent(context, SyncBinderService.class);

      //  intent.putExtra("usr",user.getUsername());
       // intent.putExtra("pwd",user.getPassword());
       // intent.putExtra("host",HOST);
       // intent.putExtra("host_address",HOST_ADDRESS);

       // Log.d("Bound Service Before", ""+myContext.getPackageName());

        // application.startService(intent);
        context.bindService(intent, new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

                //LocalBinder binder = (LocalBinder) iBinder;
                binderService = ((SyncBinderService.LocalBinder) iBinder).getMySyncService();

               // Log.d("Bound Service Conn", ""+ binderService.started);

                //binderService = (MyXMPPBinderService) binder.getService();
                if(binderService != null){
                    Log.d("service-bind", "Service is bonded successfully!");
                    mBound = true;
                    binderService.setOnConnectionEstablishedListener(SyncManager.this);
                    //do whatever you want to do after successful binding
                }else {

                    Log.d("service-bind", "Service is not bonded !");

                }


            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {

            }
        }, Context.BIND_AUTO_CREATE);

        Log.d("Bound Service After", ""+context.getPackageName());


    }


    protected boolean isServiceConnectionEstablished = false;

    @Override
    public void onConnectionEstablished() {
     //after the connection has been established
      isServiceConnectionEstablished = true;

        Log.d("Service Connection", "connection Established Now");

        /*
        binderService.syncPatientsOut(new PatientsDataSource.SyncPatientsCallback() {
            @Override
            public void onSynced(String msg) {

            }

            @Override
            public void onSyncFailed(String errorMsg) {

            }
        });
         */
    }


    public void syncData(boolean isSyncFromLogin, final AddChildDataSource.SyncPatientsCallback syncPatientsCallback){

     if(isServiceConnectionEstablished){



         binderService.syncData(isSyncFromLogin,syncPatientsCallback);

        // Log.d("Service Connection", "syncPatients Called");


     }else{

       //  Log.d("Service Connection", "connection Not Established To Send");

     }

    }







}
