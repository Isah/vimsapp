package com.vims.vimsapp.view.patients.addchild;



import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.CalendarView;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.DrawableManager;
import com.vims.vimsapp.utilities.MyDateMonth;
import com.vims.vimsapp.utilities.calendar.CurrentDayDecorator;
import com.vims.vimsapp.utilities.calendar.EventsDecorator;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.Date;

import vimsapp.R;
import vimsapp.databinding.PatientsChildAddDateofbirthFragmentBinding;


///**
// * A simple {@link Fragment} subclass.
// * Use the {@link AddCaretakerNameFragment#newInstance} factory method to
// * create an instance of this fragment.
// */
public class AddChildBirthDateFragment extends Fragment implements BlockingStep{
    // TODO: Rename parameter arguments, choose names that match
    //  the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    AddChildActivityWizardViewModel addChildActivityWizardViewModel;
    AddChildActivityWizardViewModelFactory viewModelFactory;

    // binding name with view
    //PatientsChildAddDateofbirthFragmentBinding dateofbirthFragmentBinding;
    PatientsChildAddDateofbirthFragmentBinding dateofbirthFragmentBinding;




    public AddChildBirthDateFragment() {
        //   Required empty public constructor
    }



    public static AddChildBirthDateFragment newInstance(String param1, String param2) {
        AddChildBirthDateFragment fragment = new AddChildBirthDateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        setHasOptionsMenu(true); //to enable or display actions
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem itemSearch = menu.findItem(R.id.action_search);
        // itemSearch.setVisible(false);

        MenuItem itemSettings = menu.findItem(R.id.action_settings);
        // itemSettings.setVisible(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        dateofbirthFragmentBinding  = DataBindingUtil.inflate(inflater, R.layout.patients_child_add_dateofbirth_fragment, container, false);


        //setSupportActionBar(toolbar);
        //itemView =  inflater.inflate(R.layout.patients_activity_add_caretaker, container, false);

        viewModelFactory = new AddChildActivityWizardViewModelFactory(Injection.provideRegisterChildUseCase(getActivity().getApplication()), Injection.provideGenerateChildScheduleUseCase(getActivity().getApplication()));

        addChildActivityWizardViewModel =  ViewModelProviders.of(getActivity(), viewModelFactory).get(AddChildActivityWizardViewModel.class);
        //setUpActionBar();


        //OnDateClickedEvent();
        //mydatelistener();

        //lastNameTextChanged();
        //birthPlaceTextChanged();
        //caretakerNameTextChanged();
        //genderNameTextChanged();
        //birthDateTextChanged();
        observeGender();
        observeFName();
        observeGenderErrors();
        observeBornInFutureError();

        myOnDatePicker();
        observeBirthDay();


        //set variables in Binding
        return dateofbirthFragmentBinding.getRoot();
    }

    Toolbar toolbar;

    /*

    private void setUpActionBar(){

        toolbar = registerformBinding.toolbar;
        toolbar.setTitle("Caretaker Form");

         //((AppCompatActivity)getActivity()).setSupportActionBar(toolbar); //we replace our actionbar with our toolbar
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();

            }
        });
    }
    */




    public void addDecoratorsToCalendar(DayViewDecorator dayViewDecorator){


        dateofbirthFragmentBinding.calendarChildBirth.addDecorators(
                dayViewDecorator
        );


    }



    MaterialCalendarView calendarView;
    private void myOnDatePicker(){
        calendarView = dateofbirthFragmentBinding.calendarChildBirth;
        calendarView.setSelected(true);


        Calendar cal = Calendar.getInstance();
        int yearMax = cal.get(Calendar.YEAR);
        int monthMax = cal.get(Calendar.MONTH);
        int dayMax = cal.get(Calendar.DAY_OF_MONTH);

        int yearMin = yearMax - 2;
        int monthMin = monthMax - 1;
        int dayMin = dayMax - 1;



        calendarView.state().edit()
                .setFirstDayOfWeek(Calendar.MONDAY)
                .setMinimumDate(CalendarDay.from(yearMin, monthMin, dayMin))
                .setMaximumDate(CalendarDay.from(yearMax, monthMax, dayMax))
                .setCalendarDisplayMode(CalendarMode.MONTHS)
                .commit();

        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                Log.d("TIME_CHECK","date selected");

                Log.d("TIME_CHECK","date selected: "+date.getDate());
                //Wed May 01 00:00:00 GMT+03:00 2019
                // Get a string of the selected date ////////////////////////////////////////////
                String mcv_date =  DateCalendarConverter.dateToStringWithoutTime(date.getDate());

                Log.d("TIME_CHECK","mcv date selected: "+mcv_date);
                //Wednesday, May 1, 2019

                Date dateItem = DateCalendarConverter.dateWithTimeToDateWithoutTime(date.getDate());

                //Date dateItem = DateCalendarConverter.stringToDateWithoutTime(mcv_date);

                Log.d("TIME_CHECK","Item"+dateItem);

                //addDecoratorsToCalendar(new CurrentDayDecorator(getActivity(),dateItem));
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(dateItem);

                calendarView.setSelected(true);
                //calendarView.setDate(calendar.getTime().getTime());
                Date myDate =   calendar.getTime();
                if(myDate.after(new Date())){
                    addChildActivityWizardViewModel.setmBirthPlaceError("Cant be born in future");
                }else{
                    //  Toast.makeText(getContext(),date,Toast.LENGTH_LONG).show();
                    //for the viewmodel
                    //String dateViewModel = ""+month+"/"+dayOfMonth+"/"+year;

                    int month = calendar.get(Calendar.MONTH);
                    int day = calendar.get(Calendar.DAY_OF_MONTH)+1;

                    String dateViewModel = ""+month+"/"+day+"/"+calendar.get(Calendar.YEAR);
                    addChildActivityWizardViewModel.setMdateOfBirth(dateViewModel);
                }

            }
        });


    }

   private void observeBirthDay(){

        Observer<CharSequence> birthDayObserver =  new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence s) {
                if(s!=null) {
                    String dateString = s.toString();

                    String[] dates = dateString.split("/");
                    int month = Integer.parseInt(dates[0]);
                    int year = Integer.parseInt(dates[2]);
                    int dayOfMonth = Integer.parseInt(dates[1]);

                    //String showDate = ""+dateString+" m:"+month+" d:"+dayOfMonth+" y:"+year;
                    //Toast.makeText(getContext(),showDate+" "+month,Toast.LENGTH_LONG).show();

                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, month);
                    calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);

                   // calendarView.setDateSelected(calendar.getTime(),true);

                    String dateSelected = DateCalendarConverter.dateToStringWithoutTime(calendar.getTime());
                    dateofbirthFragmentBinding.tvPreviewChildBirthdate.setText(dateSelected);
                }


            }
        };

        addChildActivityWizardViewModel.getMdateOfBirth().observe(this,birthDayObserver);



   }



    DatePickerDialog.OnDateSetListener dateSetListener;

    private void mydatelistener(){

       dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {


                Calendar calendar = Calendar.getInstance();
                calendar.set(year,monthOfYear,dayOfMonth);

                //convert to a date without time
                Date convertedBirthDate = DateCalendarConverter.dateWithTimeToDateWithoutTime(calendar.getTime());

                if(convertedBirthDate.after(new Date())){

                    addChildActivityWizardViewModel.setmBirthPlaceError("Cant be born in future");

                }else{


                    //for the viewmodel
                    String dateViewModel = ""+monthOfYear+"/"+dayOfMonth+"/"+year;

                    //for presentation
                    //MMMM d, yyyy
                    MyDateMonth month = DateCalendarConverter.dateToMyMonth(monthOfYear);
                    String dateFinal = ""+month.getMonthName()+" "+dayOfMonth+", "+year;


                    dateofbirthFragmentBinding.tvPreviewChildBirthdate.setText(dateFinal);
                    addChildActivityWizardViewModel.setMdateOfBirth(dateViewModel);



                }

            }
        };



    }

    public void OnDateClickedEvent(){


        dateofbirthFragmentBinding.tvPreviewChildBirthdate.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(getContext(),R.drawable.ic_menu_calendar_black_24dp), null, null, null);

         dateofbirthFragmentBinding.tvPreviewChildBirthdate.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

                 Calendar now = Calendar.getInstance();

                 DatePickerDialog dpd = DatePickerDialog.newInstance(
                         dateSetListener,
                         now.get(Calendar.YEAR),
                         now.get(Calendar.MONTH),
                         now.get(Calendar.DAY_OF_MONTH)
                 );
                 dpd.show(getActivity().getFragmentManager(), "");


             }
         });


    }






    public void buildPickerDialog(){


        final NumberPicker numberPicker = new NumberPicker(getActivity());
        numberPicker.setMinValue(20);
        numberPicker.setMaxValue(60);


        NumberPicker.OnValueChangeListener valueChangeListener = new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

            }
        };


        NumberPicker.Formatter formatter = new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return String.format("%02d kg", value);
            }
        };


        numberPicker.setFormatter(formatter);
        numberPicker.setOnValueChangedListener(valueChangeListener);


        new AlertDialog.Builder(getContext())
                .setTitle("Child Weight")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {



                    }

                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {



                    }
                })
                .setView(numberPicker)
                .show();



    }

    CharSequence genderIdentity = "";

    public void observeGender(){


        Observer<CharSequence> genderObserver = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence charSequence) {

                if(charSequence != null)
                if(charSequence.equals("boy")){

                    genderIdentity = "his";

                }else{

                    genderIdentity = "her";


                }


            }
        };

        addChildActivityWizardViewModel.getmGender().observe(this, genderObserver);

    }



    private void observeFName(){

        Observer<CharSequence> nameObserver = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence charSequence) {


                String text = "Baby "+charSequence+" birth day?";


                dateofbirthFragmentBinding.tvWizardDatebirth.setTypeface(MyFonts.getTypeFace(getContext()));

                dateofbirthFragmentBinding.tvWizardDatebirth.setText(text);


            }
        };

        addChildActivityWizardViewModel.getFirstName().observe(this, nameObserver);

    }


    public void observeBornInFutureError(){

        Observer<String> nameObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String bornInFuture) {

                Snackbar snackbar = Snackbar.make(dateofbirthFragmentBinding.cardView,""+bornInFuture,Snackbar.LENGTH_LONG);
                snackbar.show();

            }
        };

        addChildActivityWizardViewModel.getmBirthPlaceError().observe(this, nameObserver);

    }



   /*

    public void birthDateTextChanged(){
        profileFragmentBinding.tvDateofbirthChild.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() != 0){
                    addChildActivityWizardViewModel.setMdateOfBirth(charSequence);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }


    public void caretakerNameTextChanged(){
        profileFragmentBinding.tvCaretakerName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() != 0){
                    addChildActivityWizardViewModel.setmCaretakerName(charSequence);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }


    public void birthPlaceTextChanged(){
        profileFragmentBinding.tvBirthPlace.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() != 0){
                    addChildActivityWizardViewModel.setmBirthPlace(charSequence);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }
     */




    public void observeGenderErrors(){

        Observer<String> fnameErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();

            }};

       // addChildActivityWizardViewModel.getmGender().observe(this,fnameErrorObserver);


    }









    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {

        if(addChildActivityWizardViewModel.checkBirth()) {
            callback.goToNextStep();
        }

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
       callback.goToPrevStep();
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}
