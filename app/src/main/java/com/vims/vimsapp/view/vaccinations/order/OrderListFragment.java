package com.vims.vimsapp.view.vaccinations.order;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import com.vims.vimsapp.UseCase;

import com.vims.vimsapp.manageorders.Dosage;
import com.vims.vimsapp.manageorders.VaccineOrder;
import com.vims.vimsapp.manageorders.ViewOrders;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsDataSource;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsRepository;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.utilities.alarm.MyAlarmManager;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.injection.ModuleRepository;
import com.vims.vimsapp.view.patients.list.ItemOffsetDecoration;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import vimsapp.R;
import vimsapp.databinding.VaccinationsOrderListFragmentBinding;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;
import static com.vims.vimsapp.utilities.alarm.UIAlert.showSnackBarView;


/**
 * A simple {@link Fragment} subclass.
 */
public class OrderListFragment extends Fragment {



    VaccinationsOrderListFragmentBinding orderOverviewActivityBinding;


    public OrderListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        orderOverviewActivityBinding  = DataBindingUtil.inflate(inflater, R.layout.vaccinations_order_list_fragment, container, false);

        Toolbar toolbar = orderOverviewActivityBinding.toolbar;
        SpannableStringBuilder str = new SpannableStringBuilder("ORDERS");
        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 6, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        toolbar.setTitle(str);
        toolbar.setTitleMarginStart(getResources().getInteger(R.integer.fragment_title_margin));


        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar); //we replace our actionbar with our toolbar
        android.support.v7.app.ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if(actionBar!=null) {
            //  actionBar.setTitle("Reports");
        }




        orderOverviewActivityBinding.cardViewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isOrderInLastWeek(31)){

                    int thisMonth =  Calendar.getInstance().get(Calendar.MONTH);
                    int orderMonth = thisMonth+1; //order given for next month
                    Log.d("OrderDiffx","diff:"+orderMonth);

                    //int orderMonth = thisMonth+2; //order given for next month

                    //Log.d("OrderMonth","next month: "+thisMonth);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date());

                    int order_day = cal.get(Calendar.DAY_OF_MONTH);
                   // loadCreateOrder(orderMonth,order_day,"",true);
                    //check whether there is activity next month
                    isDosageAvailable(orderMonth,order_day);

                }else{

                    String subtitle = "Orders are only submitted in the last week of month";

                    Snackbar snackbar = Snackbar.make(orderOverviewActivityBinding.cardViewOrder, ""+subtitle,  Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    showSnackBarView(snackbar);
                }


            }
        });


        setUpRecyclerView();
        loadPastOrders();

        displayNextOrderDate();
       // displayExpDateMsg();

       // showNotificationIfOrderIsDue();

        fonts();

        return orderOverviewActivityBinding.getRoot();

    }


    private void fonts(){

        orderOverviewActivityBinding.tvOrderTitle.setTypeface(MyFonts.getTypeFace(getContext()));
        orderOverviewActivityBinding.tvOrderSubtitle.setTypeface(MyFonts.getTypeFace(getContext()));
        orderOverviewActivityBinding.tvOrderTitleOverview.setTypeface(MyFonts.getTypeFace(getContext()));
        orderOverviewActivityBinding.tvOrderTime.setTypeface(MyFonts.getTypeFace(getContext()));



    }


    MyAlarmManager myAlarmManager;
    private void showNotificationIfOrderIsDue(){
        myAlarmManager = MyAlarmManager.getInstance(getContext());

        Calendar calendar = Calendar.getInstance();
        int currentMonth = calendar.get(Calendar.MONTH);

        int lastDayOfMonth = calendar.getActualMaximum(Calendar.DATE);
        int firstDayOfOrdering = lastDayOfMonth - 10;

            PreferenceHandler pref = PreferenceHandler.getInstance(getContext());
            int isOrderNotifiedThisMonth = pref.getIntValue("order_notify_month");

            if(isOrderNotifiedThisMonth != currentMonth){//order wasn't notified

                Calendar calOrder = Calendar.getInstance();
                calOrder.set(Calendar.DAY_OF_MONTH,firstDayOfOrdering);

                Date orderDate = DateCalendarConverter.dateWithTimeToDateWithoutTime(calOrder.getTime());

               // Log.d("MonthOrder","time: "+orderDate);

                myAlarmManager.startAlarm(orderDate,MyAlarmManager.ALARM_TYPE_ORDER);

            }else{
               myAlarmManager.stopAlarm();
            }



    }




    private boolean isOrderInLastWeek(int lastWeekPeriod){

       Calendar cal = Calendar.getInstance();
       int lastDayOfMonth = cal.getActualMaximum(Calendar.DATE);
       int dayOfmonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
       int daysTogo = lastDayOfMonth - dayOfmonth;

       if(daysTogo <= lastWeekPeriod) {
         return true;
       }
        return  false;
       }

    private void displayNextOrderDate(){

        String orderDateNow = DateCalendarConverter.dateToStringWithoutTime(new Date());
        String[] dates = orderDateNow.split(" ");
        String m = dates[0];
        String y = dates[2];


        //Get for next month coz orders are made for
        int month  = DateCalendarConverter.monthStringToInt(m);
        ++month;
        String nextMonth = DateCalendarConverter.monthIntToString(month);
        String finalNextDate = ""+nextMonth+", "+y;


        orderOverviewActivityBinding.tvOrderSubtitle.setText(finalNextDate);
    }


    private void loadCreateOrder(int month,int order_day, String orderId, boolean isSubmit){

        String orderID;

        if(isSubmit){
            orderID = "1";
        }else {
            orderID = orderId;
        }

        Intent intent = new Intent(getActivity(),MakeOrderActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("order_month",month);
        bundle.putInt("order_day",order_day);
        bundle.putString("order_id",orderID);
        bundle.putBoolean("isSubmit", isSubmit);
        intent.putExtras(bundle);
        startActivity(intent);


            /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            //shared Element Transitions
            //ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, p1, p2, p3);

            String trans = getResources().getString(R.string.transitionNameOrder);
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), imageView, trans);
            startActivity(intent, options.toBundle());

            //Exit Transitions
            //Bundle mbundle = ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle();
            //startActivity(intent, mbundle);

        } else {
            startActivity(intent);
        }
        */
    }

    private void loadPastOrders(){

       orderOverviewActivityBinding.progressbarOrders.setVisibility(View.VISIBLE);

       ViewOrders viewOrders = Injection.provideViewOrders(getActivity().getApplication());
        ViewOrders.RequestValues requestValues = new ViewOrders.RequestValues(true);

        viewOrders.execute(requestValues, new UseCase.OutputBoundary<ViewOrders.ResponseValue>() {
            @Override
            public void onSuccess(ViewOrders.ResponseValue result) {
              //  runRecyclerViewAnimator();

                orderOverviewActivityBinding.progressbarOrders.setVisibility(View.GONE);
                orderOverviewActivityBinding.recyclerViewOrderOverview.setVisibility(View.VISIBLE);
                orderOverviewActivityBinding.layoutErrorOrders.setVisibility(View.GONE);

                updateInfo(result.getVaccineOrders());

            }

            @Override
            public void onError(String errorMessage) {

                orderOverviewActivityBinding.progressbarOrders.setVisibility(View.GONE);
                orderOverviewActivityBinding.recyclerViewOrderOverview.setVisibility(View.GONE);
                orderOverviewActivityBinding.layoutErrorOrders.setVisibility(View.VISIBLE);


            }
        });

    }




    private void runRecyclerViewAnimator(){

        Interpolator overshoot  = new OvershootInterpolator(1f);
        Interpolator accelerateDecelerate  = new AccelerateDecelerateInterpolator();
        Interpolator bounce  = new BounceInterpolator();

        SlideInUpAnimator animator = new SlideInUpAnimator(accelerateDecelerate);
        orderOverviewActivityBinding.recyclerViewOrderOverview.setItemAnimator(animator);
        orderOverviewActivityBinding.recyclerViewOrderOverview.getItemAnimator().setAddDuration(getResources().getInteger(R.integer.duration_recycler_view));

    }
    public void setUpRecyclerView(){


        //setup the recycler view basic info
        runRecyclerViewAnimator();
        //setting animation on scrolling

        DividerItemDecoration decoration = new DividerItemDecoration(getContext(), VERTICAL);
        //decoration.setDrawable(getResources().getDrawable(R.drawable.shape_borderline_ultra_thin_darktheme));
        // detailChildrenBinding.recyclerviewContent.recyclerview.addItemDecoration(decoration);



        orderOverviewActivityBinding.recyclerViewOrderOverview.setHasFixedSize(true); //enhance recycler view scroll
        orderOverviewActivityBinding.recyclerViewOrderOverview.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset_large);
        orderOverviewActivityBinding.recyclerViewOrderOverview.addItemDecoration(itemDecoration);

        RecyclerView.LayoutManager mLayoutManager ;


        boolean isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);
        boolean isTablet = getResources().getBoolean(R.bool.tablet);

        mLayoutManager = new LinearLayoutManager(getContext());
        orderOverviewActivityBinding.recyclerViewOrderOverview.setLayoutManager(mLayoutManager);


    }

    AdapterOrder adapterOrder;
    public void updateInfo(List<VaccineOrder> orders){
        adapterOrder = new AdapterOrder(orders, getContext());
        adapterOrder.setHasStableIds(true);
        adapterOrder.notifyItemInserted(0);
        adapterOrder.setClickListener(new OrderListner());
        orderOverviewActivityBinding.recyclerViewOrderOverview.setAdapter(adapterOrder);
    }



    private class OrderListner implements AdapterOrder.OnVaccinationCLickLIstner {

        @Override
        public void onOrderClicked(View view, int position, VaccineOrder vaccineOrder) {

               // Toast.makeText(getContext(), "Add clicked", Toast.LENGTH_SHORT).show();

                /*
                Date orderDate = DateCalendarConverter.stringToDateWithoutTime(vaccineOrder.getOrderDate());
                Calendar cal = Calendar.getInstance();
                cal.setTime(orderDate);

                int thisMonth =  DateCalendarConverter.dateToIntGetMonth(orderDate);
                int orderDay = cal.get(Calendar.DAY_OF_MONTH);

                int orderMonth = thisMonth; //this month

                String orderId = vaccineOrder.getOrderId();

                loadCreateOrder(orderMonth,orderDay,orderId,false);
                */

                Toast.makeText(getContext(), "Order clicked", Toast.LENGTH_SHORT).show();


                Intent intent = new Intent(getContext(), ViewOrderActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("order_id", vaccineOrder.getOrderId());
                intent.putExtras(bundle);
                startActivity(intent);


        }
    }


    private void isDosageAvailable(int month, int order_day){
      SearchVaccinationsRepository repository = ModuleRepository.provideSearchVaccinationRepository(getActivity().getApplication());
       repository.getDosageUsage(month, new SearchVaccinationsDataSource.LoadDosageUsageCallback() {
           @Override
           public void onDosageLoaded(List<Dosage> dosages) {

               loadCreateOrder(month,order_day,null,true);

           }

           @Override
           public void onFailed() {

               String m =  DateCalendarConverter.monthIntToString(month);
               String subtitle = "Sorry there is no vaccination activity in "+m +" ";
               Snackbar snackbar = Snackbar.make(orderOverviewActivityBinding.cardViewOrder, ""+subtitle,  Snackbar.LENGTH_LONG);
               snackbar.setDuration(4000);
               snackbar.show();
               showSnackBarView(snackbar);

           }
       });

    }


    @Override
    public void onResume() {
        super.onResume();

        loadPastOrders();

    }
}



