package com.vims.vimsapp.view.patients.addchild;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;

import vimsapp.databinding.PatientsChildAddGenderFragmentBinding;

import vimsapp.R;


///**
// * A simple {@link Fragment} subclass.
// * Use the {@link AddCaretakerNameFragment#newInstance} factory method to
// * create an instance of this fragment.
// */
public class AddChildGenderFragment extends Fragment implements BlockingStep {
    // TODO: Rename parameter arguments, choose names that match
    //  the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    AddChildActivityWizardViewModel addChildActivityWizardViewModel;
    AddChildActivityWizardViewModelFactory viewModelFactory;

    // binding name with view
   // PatientsChildAddGenderFragmentBinding patientsAddGenderFragmentBinding;
    PatientsChildAddGenderFragmentBinding patientsAddGenderFragmentBinding;




    public AddChildGenderFragment() {
        //   Required empty public constructo
    }



    public static AddChildGenderFragment newInstance(String param1, String param2) {
        AddChildGenderFragment fragment = new AddChildGenderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        setHasOptionsMenu(true); //to enable or display actions
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem itemSearch = menu.findItem(R.id.action_search);
        // itemSearch.setVisible(false);

        MenuItem itemSettings = menu.findItem(R.id.action_settings);
        // itemSettings.setVisible(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        patientsAddGenderFragmentBinding  = DataBindingUtil.inflate(inflater, R.layout.patients_child_add_gender_fragment, container, false);


        //setSupportActionBar(toolbar);
        //itemView =  inflater.inflate(R.layout.patients_activity_add_caretaker, container, false);

        viewModelFactory = new AddChildActivityWizardViewModelFactory(Injection.provideRegisterChildUseCase(getActivity().getApplication()), Injection.provideGenerateChildScheduleUseCase(getActivity().getApplication()));

        addChildActivityWizardViewModel =  ViewModelProviders.of(getActivity(), viewModelFactory).get(AddChildActivityWizardViewModel.class);
        //setUpActionBar();

        patientsAddGenderFragmentBinding.tvWizardDatebirth.setTypeface(MyFonts.getTypeFace(getContext()));


       // onRadioButtonClicked();
        onButtonClicked();

        //lastNameTextChanged();
        //birthPlaceTextChanged();
        //caretakerNameTextChanged();
        //genderNameTextChanged();
        //birthDateTextChanged();
        observeGenderErrors();


        //set variables in Binding
        return patientsAddGenderFragmentBinding.getRoot();
    }

    Toolbar toolbar;

    /*

    private void setUpActionBar(){

        toolbar = registerformBinding.toolbar;
        toolbar.setTitle("Caretaker Form");

         //((AppCompatActivity)getActivity()).setSupportActionBar(toolbar); //we replace our actionbar with our toolbar
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();

            }
        });
    }
    */


    /*
    public void onRadioButtonClicked() {


        //default settings before click listners
      //  patientsAddGenderFragmentBinding.radioGenderMale.setChecked(true);


        patientsAddGenderFragmentBinding.radioGenderFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean checked =  patientsAddGenderFragmentBinding.radioGenderFemale.isChecked();
                if(checked){
                    addChildActivityWizardViewModel.setmGender(patientsAddGenderFragmentBinding.radioGenderFemale.getText());
                    patientsAddGenderFragmentBinding.radioGenderFemale.setBackground(getResources().getDrawable(R.drawable.shape_background_color_primary));
                   // patientsAddGenderFragmentBinding.radioGenderMale.setBackground(getResources().getDrawable(R.drawable.shape_button_fill));
                }

            }
        });



        patientsAddGenderFragmentBinding.radioGenderMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean checked =  patientsAddGenderFragmentBinding.radioGenderMale.isChecked();
                if(checked){
                    addChildActivityWizardViewModel.setmGender(patientsAddGenderFragmentBinding.radioGenderMale.getText());
                    patientsAddGenderFragmentBinding.radioGenderMale.setBackground(getResources().getDrawable(R.drawable.shape_background_color_primary));
                  //  patientsAddGenderFragmentBinding.radioGenderFemale.setBackground(getResources().getDrawable(R.drawable.shape_button_fill));

                }

            }
        });






    }
   */

    public void onButtonClicked(){


        patientsAddGenderFragmentBinding.btnPreviewGenderBoy.setCompoundDrawablesWithIntrinsicBounds(null,getResources().getDrawable(R.drawable.g_new_baby_boy_select), null,null);
        patientsAddGenderFragmentBinding.btnPreviewGenderGirl.setCompoundDrawablesWithIntrinsicBounds(null,getResources().getDrawable(R.drawable.g_new_baby_girl_select), null,null);
        patientsAddGenderFragmentBinding.btnPreviewGenderGirl.setSelected(true);

        addChildActivityWizardViewModel.setmGender("girl");


        patientsAddGenderFragmentBinding.btnPreviewGenderBoy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                patientsAddGenderFragmentBinding.btnPreviewGenderBoy.setSelected(true);
                patientsAddGenderFragmentBinding.btnPreviewGenderGirl.setSelected(false);

                addChildActivityWizardViewModel.setmGender("boy");

                patientsAddGenderFragmentBinding.btnPreviewGenderBoy.setCompoundDrawablesWithIntrinsicBounds(null,getResources().getDrawable(R.drawable.g_new_baby_boy_select), null,null);

            }
        });


        patientsAddGenderFragmentBinding.btnPreviewGenderGirl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                patientsAddGenderFragmentBinding.btnPreviewGenderBoy.setSelected(false);
                patientsAddGenderFragmentBinding.btnPreviewGenderGirl.setSelected(true);


                addChildActivityWizardViewModel.setmGender("girl");

                patientsAddGenderFragmentBinding.btnPreviewGenderGirl.setCompoundDrawablesWithIntrinsicBounds(null,getResources().getDrawable(R.drawable.g_new_baby_girl_select), null,null);


            }
        });




    }





   /*

    public void birthDateTextChanged(){
        profileFragmentBinding.tvDateofbirthChild.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() != 0){
                    addChildActivityWizardViewModel.setMdateOfBirth(charSequence);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }


    public void caretakerNameTextChanged(){
        profileFragmentBinding.tvCaretakerName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() != 0){
                    addChildActivityWizardViewModel.setmCaretakerName(charSequence);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }


    public void birthPlaceTextChanged(){
        profileFragmentBinding.tvBirthPlace.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() != 0){
                    addChildActivityWizardViewModel.setmBirthPlace(charSequence);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }
     */




    public void observeGenderErrors(){

        Observer<String> fnameErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();

            }};

       // addChildActivityWizardViewModel.getmGender().observe(this,fnameErrorObserver);


    }









    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {

        if(addChildActivityWizardViewModel.checkGender()) {
            callback.goToNextStep();
        }

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        getActivity().finish();
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}
