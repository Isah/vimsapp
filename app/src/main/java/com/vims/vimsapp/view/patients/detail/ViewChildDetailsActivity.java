package com.vims.vimsapp.view.patients.detail;

import android.Manifest;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.itextpdf.text.DocumentException;
import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationStatus;
import com.vims.vimsapp.searchchild.ViewChildren;
import com.vims.vimsapp.utilities.AgeCalculator;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.MainThreadExecutor;
import com.vims.vimsapp.utilities.MyRecyclerViewManager;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.utilities.feature_discovery.FeatureDiscovery;
import com.vims.vimsapp.utilities.pdf.MyPdfCreator;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.vaccinations.CardHeader;
import com.vims.vimsapp.view.vaccinations.schedule.AdapterVaccinationsChildDetail;
import com.vims.vimsapp.view.vaccinations.schedule.ComfirmVaccinationActivity;


import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import vimsapp.BuildConfig;
import vimsapp.R;
import vimsapp.databinding.PatientsChildDetailFragmentBinding;


import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.vims.vimsapp.utilities.animations.AnimatorManager.animateScrollForView;
import static com.vims.vimsapp.utilities.ScreenManager.setScreenOrientation;
import static java.lang.Math.round;


public class ViewChildDetailsActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener{


    ImageView mProfileImage;
    private boolean mIsAvatarShown = true;
    private int mMaxScrollSize;
    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;


    ViewCaretakerDetailsActivityViewModel detailsActivityViewModel;
    PatientsChildDetailFragmentBinding detailBinding;

    int layout1 = R.layout.patients_child_detail_fragment;
    //int layout2 = R.layout.patients_fragment_caretaker_detail_tablet;


    public String cardCreationDate  = "N/A";

    Bundle b;
    String childId;
    Child child;
    int sizeDoneEvents;


    PreferenceHandler preferenceHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

         detailBinding = DataBindingUtil.setContentView(this, layout1);
         detailsActivityViewModel =  ViewModelProviders.of(this).get(ViewCaretakerDetailsActivityViewModel.class);

         //set initial value to show that at beginning nothing is yet recorded
         preferenceHandler = PreferenceHandler.getInstance(getApplicationContext());


        setScreenOrientation(this);

         b = getIntent().getExtras();
         if(b!=null) {

            childId = b.getString("child_id");
            sizeDoneEvents = b.getInt("child_done_vacs");
           // String childString = b.getString("child");
            //child = MyDetailsTypeConverter.fromStringToChild(childString);
            //setChildDetails(child);

            loadChild(childId);

            setUpDynamicAppBar();

            //float age = DateCalendarConverter.ageCalculator(DateCalendarConverter.stringToDateWithoutTime(child.getDateOfBirth()));
            //Fonts
            detailBinding.firstnameProfileTv.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
            detailBinding.tvAgeChildDetail.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
            detailBinding.tvWeightChildDetail.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
            detailBinding.tvGenderChildDetail.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
            detailBinding.tvVaccinationsChildDetail.setTypeface(MyFonts.getTypeFace(getApplicationContext()));

            detailBinding.textView3.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
            detailBinding.textView4.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
            detailBinding.textView5.setTypeface(MyFonts.getTypeFace(getApplicationContext()));

            //Animator for recycler view
            Interpolator overshoot  = new OvershootInterpolator(1f);
            Interpolator accelerateDecelerate  = new AccelerateDecelerateInterpolator();
            Interpolator bounce  = new BounceInterpolator();

            SlideInUpAnimator animator = new SlideInUpAnimator(accelerateDecelerate);

            MyRecyclerViewManager.recyclerViewSetUp(getApplicationContext(),detailBinding.childDetailInfo.recyclerViewChildDetailSchedule,animator);

//            loadChild(childId);

            //103.9
            String primaryText = "Generate Child Immunisation Card";
            String secondaryText = "Tap the icon to generate an Immunisation card in pdf";
            String preferenceText = "pdf_first_time";
            View targetView = findViewById(R.id.fabPdfReport);
            Drawable drawableIcon = AppCompatResources.getDrawable(getApplicationContext(),R.drawable.md_file_document);


             //tapTarget(ViewChildDetailsActivity.this,detailBinding.actionGeneratePdf,preferenceText,primaryText,secondaryText);

             //Feature Discovery---------------------------------
             //----------FeatureDiscovery.tapTargetKeepSafe(ViewChildDetailsActivity.this,targetView,drawableIcon,preferenceText,primaryText, secondaryText);


          //   AnimatorManager.detailEnterTransition(this);




        }

    }




    int pStatus = 0;
    String percentage = "";

    private void startCircularProgress(int progress) {

        Handler handler = new Handler();

        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (pStatus < progress) {
                    pStatus += 1;
                    percentage = Integer.toString(progress)+"%";

                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub

                             detailBinding.circularProgressbarDashboard.setProgress(pStatus);
                            //scheduleHomeBinding.tvDashboardDonePercentage.setText(percentage);

                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        // Just to display the progress slowly
                        Thread.sleep(50); //thread will take approx 3 seconds to finish
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }




    private void setScheduleLoadingIndicator(boolean isLoading){

       ProgressBar progressBar = detailBinding.contentChildrenInfo.progressBarSchedule;

       if(isLoading) {
           progressBar.setVisibility(View.VISIBLE);
       }else{
           progressBar.setVisibility(View.INVISIBLE);
       }

    }



    //ANIMATE VIEW TO INSTRUCT THAT THERE IS MORE CONTENT SCROLLABLE
    @Override
    public void onEnterAnimationComplete() {
        super.onEnterAnimationComplete();

        boolean isTab = getResources().getBoolean(R.bool.tablet);
        if(isTab) {
           animateScrollForView(detailBinding.childDetailInfo.recyclerViewChildDetailSchedule);
        }

    }


    private void showChildBackgroudImage() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
         detailBinding.materialupProfileBackdrop.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }


    private void bitmapFactory(){


          /*
            Bitmap myBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_doctor_male);
            if (myBitmap != null && !myBitmap.isRecycled()) {

                //do processing on background thread
                Palette.from(myBitmap).clearFilters().generate(new Palette.PaletteAsyncListener() {
                    @Override
                    public void onGenerated(Palette palette) {


                        Palette.Swatch vibrantSwatch = palette.getLightMutedSwatch();

                        if(vibrantSwatch != null){

                            int backgroundColor = vibrantSwatch.getRgb();

                           // detailBinding.materialupProfileBackdrop.setBackgroundColor(backgroundColor);
                            detailBinding.materialupProfileImage.setBorderColor(backgroundColor);

                        }

                    }
                });

            }
            */

    }


    private void actionOnClickLoadPdf(Child child, String pdfname){
        ImageView fab = detailBinding.contentChildrenInfo.buttonAddChild;

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    permissonPdfWrapper(child,pdfname);


                }catch (FileNotFoundException fileNotFound){

                }
                catch (DocumentException documentException) {

                }
            }
        });




    }





    //CREATING PDFS///////////////////////////////////////////////////////

    final private int REQUEST_CODE_ASK_PERMISSIONS = 111;

    MyPdfCreator myPdfCreator;


    private void loadPdf(Child child, String pdfname){

        try{

            String hospitalName = preferenceHandler.getPref(PreferenceHandler.USER_HOSPITAL_NAME);
            myPdfCreator = new MyPdfCreator(getBaseContext());

            myPdfCreator.createPdf(child, pdfname,hospitalName);
            previewPdf();


        }catch (FileNotFoundException f){


        }catch (DocumentException d){


        }

    }

    private void permissonPdfWrapper(Child child, String pdfname) throws FileNotFoundException,DocumentException{


            int hasWriteStoragePermission = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

            boolean isWriteStoragePermissionGranted = hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED;

            if (isWriteStoragePermissionGranted) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_CONTACTS)) {
                        if(getApplicationContext() != null) {

                            showAllowAccessToStorage();


                            return;
                        }
                    }

                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_CODE_ASK_PERMISSIONS);

                }


                return;
            }else {

                loadPdf(child,pdfname);

            }
        }


    private void showAllowAccessToStorage(){

            showMessageOKCancel("You need to allow access to Storage",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);

                            }
                        }
                    });

        }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ViewChildDetailsActivity.this, R.style.Theme_AppCompat_Light_Dialog_Alert);
        if(builder.getContext() != null){
         builder.setMessage(message);
         builder.setPositiveButton("Ok",okListener);
         builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialog, int which) {

             }
         });
         builder.create();
         builder.show();
        }

        }


    private void grantAllUriPermissions(Context context, Intent intent, Uri uri) {
        List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
    }

    private void previewPdf() {
        PackageManager packageManager = getPackageManager();
        Intent testIntent = new Intent(Intent.ACTION_VIEW);


        /** solves error 1
         * This file could not be accessed Check the location or the network and try again. Android 6 Marshmallow
         *
         * **/
        testIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        testIntent.setType("application/pdf");
        List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
        if (list.size() > 0) {
            Intent intent = new Intent();
           // file:///storage/emulated/0/Documents/healthcardRichardOweon2019.pdf exposed beyond app through Intent.getData()
            intent.setAction(Intent.ACTION_VIEW);
          //  Uri uri = Uri.fromFile(myPdfCreator.getPdfFile());

            Uri uri = FileProvider.getUriForFile(ViewChildDetailsActivity.this, BuildConfig.APPLICATION_ID + ".provider",
                    myPdfCreator.getPdfFile());


            /** solves error 1 **/
            grantAllUriPermissions(getApplicationContext(),testIntent,uri);


            intent.setDataAndType(uri, "application/pdf");

            startActivity(intent);
        }else{
            Toast.makeText(getApplicationContext(),"Download a PDF Viewer to see the generated PDF", Toast.LENGTH_SHORT).show();
        }
    }


    private static final String TAG = "PdfCreatorActivity";


    private long getMonths(long days){

        long months = days / 30;

        return months;

    }


    //////////END OF PDF //////////////////////////////////////////////////////
    private String getChildAge(Child child){

        AgeCalculator.Age myAge = new AgeCalculator().calculateAge(DateCalendarConverter.stringToDateWithoutTime(child.getDateOfBirth()));
        String stringAge;

        long daysAge = DateCalendarConverter.calculateDifferenceInDates(new Date(),DateCalendarConverter.stringToDateWithoutTime(child.getDateOfBirth()));

        Log.d("DiffAge","age: "+daysAge);

        if(myAge.getYears()==0 && daysAge < 32){

            String days_string = "days";

            if(daysAge==1){
                days_string = "day";
            }

            stringAge = "" + daysAge+" "+days_string;
        }
        else if(myAge.getYears()==0 && daysAge > 31){

            String months_string = "months";

            long months = getMonths(daysAge);
            if(months==1){
                months_string = "month";
            }

            stringAge = "" + months+" "+months_string;
        }
        else {

            stringAge = "" + Integer.toString(myAge.getYears()) + "." + getMonths(myAge.getDays())+" yrs";

        }
        return stringAge;
    }



    private int getDoneEvents(List<VaccinationEvent> vaccinationEvents){

        int vacs = 0;
        for(VaccinationEvent vaccinationEvent: vaccinationEvents) {
            if(vaccinationEvent.getVaccinationStatus() == VaccinationStatus.DONE){
                vacs++;
            }
        }

        return vacs;

    }


    private void setChildDetails(Child child){

        String weight = Float.toString(child.getWeight());
        //AgeCalculator.Age myAge = new AgeCalculator().calculateAge(DateCalendarConverter.stringToDateWithoutTime(child.getDateOfBirth()));
        String stringAge = getChildAge(child);
        String genderBirth = child.getDateOfBirth();
        String full_name = child.getFirstName()+" "+child.getLastName();


        //DETAIL PROFILE INFO
        detailBinding.tvAgeChildDetail.setText(stringAge);
        detailBinding.tvWeightChildDetail.setText(weight);
        detailBinding.tvGenderChildDetail.setText(genderBirth);
        detailBinding.firstnameProfileTv.setText(full_name);

        int doneEvents = getDoneEvents(child.getVaccinationEvents());
        int totalEvents = child.getVaccinationEvents().size();

        String stats = ""+doneEvents+"/"+totalEvents;
        detailBinding.tvVaccinationsChildDetail.setText(stats);

        int coverage = (int) (((double) doneEvents / (double) totalEvents * 100));

        startCircularProgress(coverage);


        //ACTIONS ONCLICK
        String year = DateCalendarConverter.dateToStringGetYear(new Date());
        String pdfname = "health_card_"+child.getFirstName()+""+child.getLastName()+""+year;
        actionOnClickLoadPdf(child,pdfname);

      //  updateInfo(child.getVaccinationEvents());



        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                updateInfo(child.getVaccinationEvents());

            }
        };

        new MainThreadExecutor(700).execute(runnable);




    }


    AdapterVaccinationsChildDetail adapterVaccinationsChildDetail;

    private void updateInfo(List<VaccinationEvent> vaccinationEvents){

        CardHeader cardHeader = new CardHeader();
        //cardHeader.setTitle("Immunisation Card");
        //cardHeader.setSubTitle(cardCreationDate);
        Collections.sort(vaccinationEvents);

        adapterVaccinationsChildDetail = new AdapterVaccinationsChildDetail(cardHeader,vaccinationEvents,getApplication(), Injection.provideRecordVaccinationUseCase(getApplication()));
        adapterVaccinationsChildDetail.setClickListner(new ChildDetailRecordVaccinationListener());

        detailBinding.childDetailInfo.recyclerViewChildDetailSchedule.setAdapter(adapterVaccinationsChildDetail);

        int prevSize = vaccinationEvents.size();
        adapterVaccinationsChildDetail.notifyItemRangeInserted(prevSize, vaccinationEvents.size() -prevSize);

        setScheduleLoadingIndicator(false);


    }



    private void loadChild(String childId){

        setScheduleLoadingIndicator(true);

        ViewChildren viewChildren = Injection.provideReadChildrenUseCase(getApplication());

        ViewChildren.RequestValues requestValues = new ViewChildren.RequestValues(childId, ViewChildren.RequestType.CHILD);
        viewChildren.execute(requestValues, new UseCase.OutputBoundary<ViewChildren.ResponseValue>() {
            @Override
            public void onSuccess(ViewChildren.ResponseValue result) {


                setChildDetails(result.getChild());

            }

            @Override
            public void onError(String errorMessage) {

            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();

        if(adapterVaccinationsChildDetail!=null) {

            int isRecorded = preferenceHandler.getIntValue(PreferenceHandler.IS_VACCINE_EVENT_RECORDED);
            if(isRecorded == 1){
                loadChild(childId);
                preferenceHandler.setIntValue(PreferenceHandler.IS_VACCINE_EVENT_RECORDED,0);
            }

        }

    }

    public void setUpDynamicAppBar(){



       // String full_name = child.getFirstName()+" "+child.getLastName();
        AppBarLayout appbarLayout = detailBinding.appbarChildDetail;

        Toolbar toolbar = detailBinding.materialupToolbar;
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null) {
            //  getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        final CollapsingToolbarLayout collapsingToolbarLayout = detailBinding.mainCollapsing;
        //collapsingToolbarLayout.setTitleEnabled(false);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout.setCollapsedTitleGravity(Gravity.BOTTOM);
        collapsingToolbarLayout.setTitleEnabled(false);
        collapsingToolbarLayout.setTitle("");
      //  detailBinding.firstnameProfileTv.setText(full_name);




        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                 onBackPressed();
               // finish();
            }
        });


        boolean isTab = getResources().getBoolean(R.bool.tablet);
        if(!isTab){
            appbarLayout.addOnOffsetChangedListener(this);
            mMaxScrollSize = appbarLayout.getTotalScrollRange();


        }

    }

    private class ChildDetailRecordVaccinationListener implements AdapterVaccinationsChildDetail.OnRecordVaccinationClicked{

        @Override
        public void onRecordVaccination(View view, int position, VaccinationEvent vaccinationEvent) {

              executeVaccination(vaccinationEvent, position,view);


        }
    }

    private void executeVaccination(VaccinationEvent vaccinationEvent, int pos, View view){
        //View sharedView = detailBinding.c;
        Bundle bundle = new Bundle();
        bundle.putString("vevent",MyDetailsTypeConverter.fromVaccinationEventToString(vaccinationEvent));
        bundle.putInt("position",pos);

        Intent intent = new Intent(this,ComfirmVaccinationActivity.class);
        intent.putExtras(bundle);

        String transitionName  = getResources().getString(R.string.transitionNameEvent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            //shared Element Transitions
            // ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, p1, p2, p3);
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, view, transitionName);
            startActivity(intent, options.toBundle());
            //Exit Transitions
            //Bundle bsundle = ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle();
            //context.startActivity(intent, bundle);

        } else {
            startActivity(intent);
        }

        //AnimatorManager.makeSceneTransition(this,view,transitionName,intent);
        //startActivity(intent);

    }





    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

        mProfileImage = detailBinding.babyGender;
       // FloatingActionButton fab = detailBinding.contentChildrenInfo.fabPdfReport;


        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int percentage = (Math.abs(verticalOffset)) * 100 / mMaxScrollSize;

        if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
            mIsAvatarShown = false;

            mProfileImage.animate()
                    .scaleY(0).scaleX(0)
                    .setDuration(50)
                    .start();

            /*
            fab.animate()
                    .scaleY(0).scaleX(0)
                    .setDuration(50)
                    .start();
                    */

        }

        if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
            mIsAvatarShown = true;

            mProfileImage.animate()
                    .scaleY(1).scaleX(1)
                    .start();

            /*
            fab.animate()
                    .scaleY(1).scaleX(1)
                    .start();
                    */



        }






    }

    private static final int REQUEST_CODE = 2;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                Bundle bundle =   data.getExtras();
                if(bundle!=null) {
                    String child_string =  bundle.getString("child_saved");
                    //Child child = (Child) MyDetailsTypeConverter.fromStringTo(child_string);
                 /*
                 initCaretaker();
                 if(detailsActivityViewModel.getCaretaker().getValue()!=null) {
                     Toast.makeText(this, child_string + " isBack" + " " + detailsActivityViewModel.getCaretaker().getValue().getLastName(), Toast.LENGTH_LONG).show();

                     adapterChildren.addAll(detailsActivityViewModel.getCaretaker().getValue().getChildren());
                     adapterChildren.notifyDataSetChanged();
                     childlistview.setAdapter(adapterChildren);
                 }
                */
                }



            }
        }

    }






}
