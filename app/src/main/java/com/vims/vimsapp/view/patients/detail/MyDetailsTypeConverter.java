package com.vims.vimsapp.view.patients.detail;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.viewcoverage.CoverageReport;

import java.lang.reflect.Type;

/**
 * Created by root on 6/9/18.
 */

public class MyDetailsTypeConverter {

    @TypeConverter

    public static Caretaker fromStringToCaretaker(String value) {

        Type listType = new TypeToken<Caretaker>() {}.getType();

        return new Gson().fromJson(value, listType);

    }

    @TypeConverter

    public static String fromCatakerToString(Caretaker caretaker) {

        Gson gson = new Gson();

        String json = gson.toJson(caretaker);

        return json;

    }


    @TypeConverter
    public static Child fromStringToChild(String value) {

        Type listType = new TypeToken<Child>() {}.getType();

        return new Gson().fromJson(value, listType);

    }

    @TypeConverter
    public static String fromChildToString(Child child) {

        Gson gson = new Gson();

        return gson.toJson(child);

    }









    @TypeConverter
    public static Object fromStringTo(String value) {

        return value;
    }

    @TypeConverter
    public static String toString(Object data) {
        return (String) data;
    }



    @TypeConverter

    public static VaccinationEvent fromStringToVaccinationEvent(String value) {

        Type listType = new TypeToken<VaccinationEvent>() {}.getType();

        return new Gson().fromJson(value, listType);

    }

    @TypeConverter

    public static String fromVaccinationEventToString(VaccinationEvent vaccinationEvent) {

        Gson gson = new Gson();

        String json = gson.toJson(vaccinationEvent);

        return json;

    }




    @TypeConverter
    public static CoverageReport fromStringToCoverageReport(String value) {

        Type listType = new TypeToken<CoverageReport>() {}.getType();

        return new Gson().fromJson(value, listType);

    }

    @TypeConverter
    public static String fromCoverageReportToString(CoverageReport coverageReport) {

        Gson gson = new Gson();

        String json = gson.toJson(coverageReport);

        return json;

    }












}

