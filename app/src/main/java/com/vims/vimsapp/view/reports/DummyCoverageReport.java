package com.vims.vimsapp.view.reports;

public class DummyCoverageReport {

    private float[] noOfvaccinationsY;
    private int monthX;

    public float[] getNoVaccinations() {
        return noOfvaccinationsY;
    }

    public void setNoVaccications(float[] vaccinatedChildrenY) {
        this.noOfvaccinationsY = vaccinatedChildrenY;
    }

    public int getMonthX() {
        return monthX;
    }

    public void setMonthX(int monthX) {
        this.monthX = monthX;
    }
}
