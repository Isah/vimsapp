package com.vims.vimsapp.view.vaccinations.order;

import android.content.Context;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vims.vimsapp.manageorders.VaccineOrder;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.view.MyFonts;

import java.util.List;
import java.util.Map;

import vimsapp.R;

public class AdapterOrder extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    //the adapter needs its viewmodel
    private static volatile AdapterOrder INSTANCE;

    private List<VaccineOrder> vaccinePlans;


    private Context context;

    int sizeOfItems;

    public Map<String, VaccinationEvent> mappedEvents;
    boolean isAnOrder;


    public AdapterOrder(List<VaccineOrder>  vaccinePlans, Context context) {
        this.context = context;

        /*
        String id = UUID.randomUUID().toString();
        for(VaccinationEvent vaccinationEvent: vaccinationEvents) {
            mappedEvents.put(id, vaccinationEvent);
        }
        */

        this.vaccinePlans = vaccinePlans;

        //1. Animations bug
       // setHasStableIds(true); //in constructor
        sizeOfItems = vaccinePlans.size();


        setHasStableIds(true);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }



    public static void clearInstance() {
        INSTANCE = null;
    }





    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

      if(viewType == TYPE_ITEM){

          View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vaccinations_order_overview_card, parent, false);

          return new MyViewHolder(v);


        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");


    }

    //(2) implements the click listner
    public class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {
        private TextView tvOrderTitle;
        private TextView tvOrderSubtitle;
        private TextView tvOrderTime;
        private ImageView ivOrder;



        public MyViewHolder(View view) {
            super(view);
            //(3) Bind the click listener
            view.setOnClickListener(this);


        }


        public void setData(VaccineOrder vaccineOrder, int position){

            //place data from the object onto the view

            //THE VIEW
            tvOrderTitle = itemView.findViewById(R.id.tv_order_title);
            tvOrderSubtitle = itemView.findViewById(R.id.tv_order_subtitle);
            tvOrderTime = itemView.findViewById(R.id.tv_order_time);
            ivOrder = itemView.findViewById(R.id.ivOrder);

            //THE TYPEFACE
            tvOrderTitle.setTypeface(MyFonts.getTypeFace(context));
            tvOrderSubtitle.setTypeface(MyFonts.getTypeFace(context));
            tvOrderTime.setTypeface(MyFonts.getTypeFace(context));
            //tvDosageUsed.setTypeface(MyFonts.getTypeFace(context));



           // char orderid_last_char = vaccineOrder.getOrderId().charAt(vaccineOrder.getOrderId().length()-1);

            String[] orderSplit = vaccineOrder.getOrderId().split("-");
            String lastString = orderSplit[4];
            String firstString = orderSplit[1];

            char orderid_char1 = lastString.charAt(0);

            //db7b91e0-14b5-4a87-8312-1b695457bf97
            //bd0f5a79-7908-49b7-b081-c5af2165755f

          //  Log.d("OrderID"," ID: "+vaccineOrder.getOrderId());


            //THE BINDING
            String orderId = firstString+" - "+orderid_char1;
            String orderTitle = "Order #"+orderId;
            String orderSubtitle = "Viles: "+vaccineOrder.getVaccinePlans().size();
            String orderTime = "D. "+vaccineOrder.getOrderDate();

            tvOrderTitle.setText(orderTitle);
            tvOrderSubtitle.setText(orderSubtitle);
            tvOrderTime.setText(orderTime);
            tvOrderTime.setTextColor(context.getResources().getColor(R.color.colorPrimary));


        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition(); // gets item position

          //  myclickListener.onAddClicked(v,position,getVaccineOrder(position)); // call the onClick in the OnCardItemClickListener Interface i created
            myclickListener.onOrderClicked(v,position,getVaccineOrder(position));
            // VaccinationEvent vaccinationEvent = vaccinationEvents.get(position);

        }

    }


    int mExpandedPosition = -1;
    int previousExpandedPosition = -1;
    int pStatus = 0;
    String percentage = "";


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder general_holder, int position) {


      if(general_holder instanceof AdapterOrder.MyViewHolder ) {

            final AdapterOrder.MyViewHolder holder = (AdapterOrder.MyViewHolder) general_holder;
            final VaccineOrder vaccinePlan = getVaccineOrder(position);


            holder.setData(vaccinePlan,position);



        }


    }



    @Override
    public int getItemCount() {

        return vaccinePlans.size();
    }

    /*

    //2. Animations bug
    @Override
    public long getItemId(int position) {
        return vaccinationEvents.get(position+1).hashCode(); //Any unique id
    }
    */


    public VaccineOrder getVaccineOrder(int position)
    {
        return vaccinePlans.get(position);
    }

    public void upDateVaccinePlan(int position, VaccineOrder vaccinePlan)
    {
        vaccinePlans.set(position,vaccinePlan);

    }

    public List<VaccineOrder> getVaccinePlans()
    {
        return vaccinePlans;
    }


    //    need to override this method
    @Override
    public int getItemViewType(int position) {
      /*
        if(isPositionHeader(position))
            return TYPE_HEADER;
            */
        return TYPE_ITEM;
    }

    /*
    @Override
    public long getItemId(int position) {
        return position;
    }

    */
    private boolean isPositionHeader(int position)
    {
        return position == 0;
    }




    //(1) Declare Interface
    protected OnVaccinationCLickLIstner myclickListener;  //our interace
    // private WebView webView;


    //interfaces
    public void setClickListener(OnVaccinationCLickLIstner onCardItemClickListener) {
        this.myclickListener = onCardItemClickListener;
    }


    public interface OnVaccinationCLickLIstner {

      //  void onAddClicked(View view,int position, VaccineOrder vaccineOrder);
        void onOrderClicked(View view,int position, VaccineOrder vaccineOrder);

    }














}