package com.vims.vimsapp.view.patients.addcaretaker;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.graphics.Bitmap;
import android.text.TextUtils;

/*
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
*/
import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.registerchild.RegisterCaretaker;
import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.view.patients.detail.MyDetailsTypeConverter;

import java.util.Date;

/**
 * Created by root on 5/31/18.
 */

public class CaretakerActivityWizardViewModel extends ViewModel {

    private MutableLiveData<CharSequence> firstName;
    private MutableLiveData<CharSequence> lastName;
    private MutableLiveData<String> lBirthDate;
    private MutableLiveData<CharSequence> lPhone1;
    private MutableLiveData<CharSequence> lPhone2;

    private MutableLiveData<CharSequence> lrole;
    private MutableLiveData<CharSequence> lParish;
    private MutableLiveData<CharSequence> lDistrict;
    private MutableLiveData<CharSequence> lVillage;



    private MutableLiveData<CharSequence> fullName;
    private MutableLiveData<CharSequence> fullAddress;
    private MutableLiveData<CharSequence> fullPhone;


    private MutableLiveData<String> profileImagePath;



    private MutableLiveData<String> saveCaretakerResponse;

    private MutableLiveData<String> mlastNameError;
    private MutableLiveData<String> mfirstNameError;
    private MutableLiveData<String> mPhoneError;

    private MutableLiveData<String> mParishError;
    private MutableLiveData<String> mDistrictError;
    private MutableLiveData<String> mVillageError;


    private MutableLiveData<Boolean> mIsNextButtonEnabled;

    private RegisterCaretaker registerCaretaker;


    public CaretakerActivityWizardViewModel(RegisterCaretaker registerCaretaker) {


        this.registerCaretaker = registerCaretaker;

        mlastNameError = new MutableLiveData<>();
        mfirstNameError = new MutableLiveData<>();
        mDistrictError = new MutableLiveData<>();
        mParishError = new MutableLiveData<>();
        mVillageError = new MutableLiveData<>();
        mPhoneError = new MutableLiveData<>();
        saveCaretakerResponse = new MutableLiveData<>();

        firstName = new MutableLiveData<>();
        lastName = new MutableLiveData<>();
        lBirthDate = new MutableLiveData<>();
        lPhone1 = new MutableLiveData<>();
        lPhone2 = new MutableLiveData<>();
        lParish = new MutableLiveData<>();
        lVillage = new MutableLiveData<>();
        lDistrict = new MutableLiveData<>();
        lrole = new MutableLiveData<>();

        mIsNextButtonEnabled = new MutableLiveData<>();


    }

    public MutableLiveData<String> getmPhoneError() {
        return mPhoneError;
    }

    public void setmPhoneError(String PhoneError) {
        mPhoneError.postValue(PhoneError);
    }


    public MutableLiveData<String> getmParishError() {
        return mParishError;
    }

    public void setmParishError(String parishError) {
        mParishError.postValue(parishError);
    }

    public MutableLiveData<String> getmDistrictError() {
        return mDistrictError;
    }

    public void setmDistrictError(String districtError) {
        mDistrictError.postValue(districtError);
    }

    public MutableLiveData<String> getmVillageError() {
        return mVillageError;
    }

    public void setmVillageError(String villageError) {
        mVillageError.postValue(villageError);
    }

    public LiveData<String> getMlastNameError() {
        if(mlastNameError == null){
            mlastNameError = new MutableLiveData<>();
        }
        return mlastNameError;
    }

    private void setMlastNameError(String lastNameError) {
        mlastNameError.postValue(lastNameError);
    }

    public LiveData<String> getMfirstNameError() {
        if(mfirstNameError == null){
            mfirstNameError = new MutableLiveData<>();
        }
        return mfirstNameError;

    }

    private void setMfirstNameError(String firstNameError) {
        mfirstNameError.postValue(firstNameError);
    }


    private MutableLiveData<String> mBirthDateError;
    public LiveData<String> getMBirthDateError() {
        if(mBirthDateError == null){
            mBirthDateError = new MutableLiveData<>();
        }
        return mBirthDateError;
    }

    private void setMBirthDateError(String lastNameError) {
        mBirthDateError.postValue(lastNameError);
    }



    public LiveData<String> getResponse() {
        if(saveCaretakerResponse == null){
            saveCaretakerResponse = new MutableLiveData<>();
        }
        return saveCaretakerResponse;
    }

    public void setResponse(String res) {
        saveCaretakerResponse.postValue(res);
    }


    public LiveData<CharSequence> getFirstName() {
        if(firstName == null){
            firstName = new MutableLiveData<>();
        }
        return firstName;
    }

    public void setFirstName(CharSequence fName) {
        firstName.postValue(fName);
    }

    public LiveData<CharSequence> getLastName() {
        if(lastName == null){
            lastName = new MutableLiveData<>();
        }
        return lastName;
    }

    public void setLastName(CharSequence lName) {
        lastName.postValue(lName);
    }


    public MutableLiveData<String> getlBirthDate() {
        return lBirthDate;
    }

    public void setlBirthDate(String age) {
        lBirthDate.postValue(age);
    }

    public MutableLiveData<CharSequence> getlPhone1() {
        return lPhone1;
    }

    public void setlPhone1(CharSequence phone1) {
        lPhone1.postValue(phone1);
    }

    public MutableLiveData<CharSequence> getlPhone2() {
        return lPhone2;
    }

    public void setlPhone2(CharSequence phone2) {
        lPhone2.postValue(phone2);
    }

    public MutableLiveData<CharSequence> getlRole() {
        return lrole;
    }

    public void setlRole(CharSequence healthcentre) {
        lrole.postValue(healthcentre);
    }

    public MutableLiveData<CharSequence> getlParish() {
        return lParish;
    }

    public void setlParish(CharSequence parish) {
        lParish.postValue(parish);
    }

    public MutableLiveData<CharSequence> getlDistrict() {
        return lDistrict;
    }

    public void setlDistrict(CharSequence district) {
        lDistrict.postValue(district);
    }

    public MutableLiveData<CharSequence> getlVillage() {
        return lVillage;
    }

    public void setlVillage(CharSequence village) {
       lVillage.postValue(village);
    }


    public MutableLiveData<Boolean> getmIsNextButtonEnabled() {
        return mIsNextButtonEnabled;
    }

    public void setmIsNextButtonEnabled(Boolean isNextButtonEnabled) {
        this.mIsNextButtonEnabled.postValue(isNextButtonEnabled);
    }

    //combined names
    public MutableLiveData<CharSequence> getFullName(){

        if(fullName == null){

            fullName = new MutableLiveData<>();
        }

        return fullName;

    }


    public void setFullName(){


        if(fullName == null){

            fullName = new MutableLiveData<>();
        }

        if(getFirstName().getValue()!=null & getLastName().getValue()!=null) {

            String fn = getFirstName().getValue() + " " + getLastName().getValue();

            fullName.postValue(fn);
        }




    }

    public LiveData<CharSequence> getFullAddress(){
        if(fullAddress == null){
            fullAddress = new MutableLiveData<>();
        }
        return fullAddress;
    }


    public void setFullAddress(){
        if(lDistrict.getValue()!=null && lParish.getValue()!=null){

           // String fn = lDistrict.getValue()+", "+lParish.getValue()+", "+lVillage.getValue();
            String fn = lDistrict.getValue()+","+lParish.getValue();
            if(fullAddress == null){
                fullAddress = new MutableLiveData<>();
            }
            fullAddress.postValue(fn);
        }
    }


    public MutableLiveData<String> getProfileImagePath() {

        return profileImagePath;
    }

    public void setProfileImagePath(String sprofileImagePath) {
        if(profileImagePath ==null){
            profileImagePath = new MutableLiveData<>();
        }
        this.profileImagePath.postValue(sprofileImagePath);
    }


    private MutableLiveData<Bitmap> profileImageBitmap;
    public MutableLiveData<Bitmap> getProfileImageBitmap() {
        if(profileImagePath ==null){
            profileImagePath = new MutableLiveData<>();
        }
        return profileImageBitmap;
    }

    public void setProfileImageBitmap(Bitmap sprofileImageBitmap) {
        if(profileImageBitmap == null){
            profileImageBitmap = new MutableLiveData<>();
        }
        this.profileImageBitmap.postValue(sprofileImageBitmap);
    }



    private  boolean isValid = false;
    private Caretaker caretaker;

    public boolean checkCaretakerBasicinfo(){


        String errorMessage = "cannot be empty!";
        String errorMessage2 = "field must be more than 3 chars!";
        String errorMessage3 = "cannot be born in the future!!";



        if(TextUtils.isEmpty(getFirstName().getValue())){

         isValid = false;
         setMfirstNameError("FirstName "+errorMessage);

     }
     else if(TextUtils.isEmpty(getLastName().getValue())){

         isValid = false;
         setMlastNameError("LastName "+errorMessage);


     }


     /*

     else if(TextUtils.isEmpty(getlBirthDate().getValue())){


         isValid = false;
         setMBirthDateError("birth date "+errorMessage);


     }
     */

     /*
     else if(getProfileImagePath() == null || TextUtils.isEmpty(getProfileImagePath().toString())){

         isValid = false;
         //mlastNameError = null;
         setMlastNameError("Pic needed "+errorMessage);


     }
     */


     else {
         isValid = true;


     }

     return isValid;

    }



    private MutableLiveData<Boolean> isAddress1Valid;
    public MutableLiveData<Boolean> getIsAddress1Valid() {

        return isAddress1Valid;
    }
    public void setIsAddress1Valid(boolean isaddressValid) {
        if(isAddress1Valid == null){
            isAddress1Valid = new MutableLiveData<>();
        }
        this.isAddress1Valid.postValue(isaddressValid);
    }





    private void setAddressObjects(){
        if(isAddressDValid == null){
          isAddressDValid = new MutableLiveData<>();
        }
        if(isAddress1Valid == null){
            isAddress1Valid = new MutableLiveData<>();
        }
        if(isAddressParValid == null){
            isAddressParValid = new MutableLiveData<>();
        }
        if(isAddressVValid == null){
            isAddressVValid = new MutableLiveData<>();
        }
    }

    public boolean checkAddressValid(){
      setAddressObjects();

        if(isAddressDValid.getValue() != null && isAddress1Valid.getValue() != null && isAddressParValid.getValue() != null ){

            if(isAddressDValid.getValue() && isAddress1Valid.getValue() && isAddressParValid.getValue()){
                 return true;
            }
        }
        return false;
    }


    private MutableLiveData<Boolean> isAddressDValid;
    public MutableLiveData<Boolean> getIsAddressDValid() {

        return isAddressDValid;
    }
    public void setIsAddressDValid(boolean isaddressValid) {
        if(isAddressDValid == null){
            isAddressDValid = new MutableLiveData<>();
        }
        this.isAddressDValid.postValue(isaddressValid);
    }


    private MutableLiveData<Boolean> isAddressParValid;
    public MutableLiveData<Boolean> getIsAddressParValid() {

        return isAddressParValid;
    }
    public void setIsAddressParValid(boolean isaddressValid) {
        if(isAddressParValid == null){
            isAddressParValid = new MutableLiveData<>();
        }
        this.isAddressParValid.postValue(isaddressValid);
    }


    private MutableLiveData<Boolean> isAddressVValid;
    public MutableLiveData<Boolean> getIsAddressVValid() {

        return isAddressVValid;
    }
    public void setIsAddressVValid(boolean isaddressValid) {
        if(isAddressVValid == null){
            isAddressVValid = new MutableLiveData<>();
        }
        this.isAddressVValid.postValue(isaddressValid);
    }




    public boolean checkAddressDistrict(){

        if(TextUtils.isEmpty(getlDistrict().getValue())){

            setMfirstNameError("District "+errorMessage);
            return  false;

        }
        return true;

    }
    public boolean checkAddressParish(){
        if(TextUtils.isEmpty(getlParish().getValue())){

            setmParishError("Parish "+errorMessage);
            return false;
        }
        return true;
    }
    public boolean checkAddressVillage(){
        if(TextUtils.isEmpty(getlVillage().getValue())){

            setmVillageError("Village "+errorMessage);
            return false;

        }
        return true;

    }

    String errorMessage = "cannot be empty!";
    String errorMessageP = "Invalid Phone Number!";

    public boolean checkAddressPhone(){
      boolean isValid = false;

        if(getlPhone1().getValue()!=null){

            if(isPhoneNumberValid(getlPhone1().getValue(),"+256")){
                isValid = true;
            }else {

                setmPhoneError(""+errorMessageP+" ");

            }

        }

        return isValid;

    }



    private boolean isPhoneNumberValid(CharSequence phoneNumber, String countryCode)
    {

        if(phoneNumber.toString().startsWith("07") && phoneNumber.length() == 10){

            return true;
        }

             /*
            //NOTE: This should probably be a member variable.
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

            try {
                Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber.toString(), countryCode);
                return phoneUtil.isValidNumber(numberProto);
            } catch (NumberParseException e) {
                System.err.println("NumberParseException was thrown: " + e.toString());
            }
            */


        return false;
    }



    public void addCaretaker(String hospitalId){
             caretaker = new Caretaker();

             if(getFirstName().getValue()!=null
                && getLastName().getValue()!=null
                && getlPhone1().getValue()!=null

                && getlParish().getValue()!=null
                && getlDistrict().getValue()!=null
                && getlRole().getValue() !=null
              //  && getProfileImageBitmap().getValue() !=null
                 //    && getlVillage().getValue()!=null
                     ) {

            //get the hospital id
            //5c2521ea6c4405162f987b04

            caretaker.setHospitalId(hospitalId);
            caretaker.setFirstName(getFirstName().getValue().toString());
            caretaker.setLastName(getLastName().getValue().toString());
            caretaker.setPhoneNo(getlPhone1().getValue().toString());

            //caretaker.setBirthDate(getlBirthDate().getValue().toString());
            caretaker.setBirthDate("N/A");
            caretaker.setGenderRole(getlRole().getValue().toString());


           // caretaker.setVillage(getlVillage().getValue().toString());
            caretaker.setParish(getlParish().getValue().toString());
            caretaker.setDistrict(getlDistrict().getValue().toString());

            caretaker.setProfileImagePath(getProfileImagePath().getValue());

            caretaker.setRegistrationDate(new Date().getTime());

            //registerCaretaker = new RegisterCaretaker(null,this.getApplication());
            registerCaretaker.execute(new RegisterCaretaker.RequestValues(caretaker), new UseCase.OutputBoundary<RegisterCaretaker.ResponseValue>() {
                @Override
                public void onSuccess(RegisterCaretaker.ResponseValue result) {


                    String myCaretaker =  MyDetailsTypeConverter.fromCatakerToString(caretaker);
                    saveCaretakerResponse.postValue(myCaretaker);


                }

                @Override
                public void onError(String e) {

                    saveCaretakerResponse.postValue(e);

                }
            });


             }



    }














}
