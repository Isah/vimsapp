package com.vims.vimsapp.view.vaccinations.order;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.manage_order_controller.ManageOrderController;
import com.vims.vimsapp.manageorders.Dosage;
import com.vims.vimsapp.manageorders.RequestOrder;
import com.vims.vimsapp.manageorders.VaccineOrder;
import com.vims.vimsapp.manageorders.VaccinePlan;
import com.vims.vimsapp.manageorders.ViewOrders;
import com.vims.vimsapp.presenters.makeorder_presenter.ManageOrderView;
import com.vims.vimsapp.presenters.makeorder_presenter.ManageOrderViewModel;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.MainThreadExecutor;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.utilities.ScreenManager;
import com.vims.vimsapp.utilities.alarm.UIAlert;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.injection.ModuleRepository;
import com.vims.vimsapp.view.patients.list.ItemOffsetDecoration;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import vimsapp.R;
import vimsapp.databinding.VaccinationsOrderActivityBinding;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;


public class MakeOrderActivity extends AppCompatActivity implements ManageOrderView{


    VaccinationsOrderActivityBinding activityBinding;
    ManageOrderController manageOrderController;

    boolean isSubmitOrder = false;
    int order_month_global_var;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityBinding = DataBindingUtil.setContentView(this, R.layout.vaccinations_order_activity);

        setUpToolbar();
        ScreenManager.setScreenOrientation(this);
        setUpRecyclerView();
        bundle = getIntent().getExtras();
        if(bundle!=null) {
            int orderMonth = bundle.getInt("order_month");
            order_month_global_var = orderMonth;


            //  runMakeSelection();
            makeNewOrderSelection(orderMonth);
        }




    }


    private void setUpToolbar(){
        activityBinding.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        activityBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish();
                onBackPressed();

            }
        });

        setSupportActionBar(activityBinding.toolbar);
    }

    private void runMakeSelection(int orderMonth){

            //global_order_month = orderMonth;
            //int orderDay = bundle.getInt("order_day");
            //orderId = bundle.getString("order_id");
            //isSubmitOrder = bundle.getBoolean("isSubmit");
            manageOrderController = Injection.provideManageOrderController(this,getApplication());
            manageOrderController.makeOrderSelection(orderMonth);

        // Toast.makeText(getApplicationContext(),"Make Selection: ",Toast.LENGTH_LONG).show();


    }

    RecyclerView.LayoutManager mLayoutManager;

    public void setUpRecyclerView(){
        boolean isTablet = getResources().getBoolean(R.bool.tablet);
        boolean isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);

        // DividerItemDecoration decoration = new DividerItemDecoration(getApplicationContext(), VERTICAL);
        DividerItemDecoration decoration = new DividerItemDecoration(getApplicationContext(), VERTICAL);
        // decoration.setDrawable(getResources().getDrawable(R.drawable.shape_borderline_ultra_thin_darktheme));
        activityBinding.recyclerview.addItemDecoration(decoration);

        //setup the recycler view basic info
        SlideInUpAnimator animator = new SlideInUpAnimator(new OvershootInterpolator(1f));
        activityBinding.recyclerview.setItemAnimator(animator);
        activityBinding.recyclerview.getItemAnimator().setAddDuration(500);
        //setting animation on scrolling

        // scheduleHomeBinding.recyclerview.setHasFixedSize(true); //enhance recycler view scroll
        // scheduleHomeBinding.recyclerview.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getApplicationContext(), R.dimen.item_offset_small);
        activityBinding.recyclerview.addItemDecoration(itemDecoration);
        //  RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL, false);



        if(isTablet){

            //if(!isPotrait){
            //mLayoutManager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL, false);
            mLayoutManager  = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);

            // }


        }else {
            // if(!isPotrait){
            mLayoutManager  = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);

            // }

        }

        activityBinding.recyclerview.setLayoutManager(mLayoutManager);


    }


    AdapterOrderPlans adapterOrders;

    public void updateUpcomingEvents(List<VaccinePlan> vaccinePlanList, int total, boolean isAnOrder){

        adapterOrders = new AdapterOrderPlans(vaccinePlanList,getApplication(),isAnOrder);
        adapterOrders.setClickListener(new OrderListner());


        //adapterVaccinationEvents.setHasStableIds(true);ooncomingEvents.notifyItemInserted(0);
        int prevSize = vaccinePlanList.size();
        adapterOrders.notifyItemRangeInserted(prevSize, vaccinePlanList.size() -prevSize);

        activityBinding.recyclerview.setNestedScrollingEnabled(false);

        activityBinding.recyclerview.postDelayed(new Runnable() {
            @Override
            public void run() {
                activityBinding.recyclerview.setAdapter(adapterOrders);

                //   adapterUpcomingEvents.getFilter().filter("January");

            }
        },100);

        String totalQuantity = "Req. "+Integer.toString(total);
        //tvQuantityRequired.setText(totalQuantity);

    }


    @Override
    public void showNewOrder(ManageOrderViewModel manageOrderViewModel) {
        String title = "October Month";
       // Toast.makeText(getApplicationContext(),"Title: "+manageOrderViewModel.getmNewOrderTitle()+" orders: "+manageOrderViewModel.getmNewOrderItemList().size(),Toast.LENGTH_LONG).show();


        activityBinding.tvOrderDate.setText(title);
        activityBinding.tvDateTiming.setText(manageOrderViewModel.getmNewOrderSubtitle());


        ////////////// behavior //////////////////////////
        if(manageOrderViewModel.getmIsShowOrderSubmitted()){
            activityBinding.tvOrderSubmitted.setVisibility(View.VISIBLE);
        }else {
            activityBinding.tvOrderSubmitted.setVisibility(View.GONE);
        }


        //is expiring
        if(manageOrderViewModel.getmIsOrderExpiring()){

            activityBinding.tvDateTiming.setTextColor(getResources().getColor(R.color.colorAccent));

        }else {

            //tvDateTiming.setTextColor(getResources().getColor(R.color.colorAccent));

        }





        ArrayList<VaccinePlan> vaccinePlans = new ArrayList<>();
        int total = 0;


        if(manageOrderViewModel.getmNewOrderItemList()!=null){


            for (Dosage dosage : manageOrderViewModel.getmNewOrderItemList()) {
                VaccinePlan vaccinePlan = new VaccinePlan();
                vaccinePlan.setVaccineName(dosage.getVaccine());
                vaccinePlan.setQuantityRequired(dosage.getDosageNeededNextMonth());
                vaccinePlan.setQuantityAtHand(0);
                vaccinePlan.setQuantityUsed(dosage.getDosageUsedThisMonth());
                vaccinePlans.add(vaccinePlan);
                total += dosage.getDosageNeededNextMonth();
                updateUpcomingEvents(vaccinePlans, total, true);
            }
        }



    }






    ////////////////////// HARDCODED NO SEPERATION BETWEEN VIEW AND DOMAIN/////////////////////////////


    ////////////////////// NEW ORDER /////////////

    private void makeNewOrderSelection(int order_month){

        ViewOrders viewOrders = Injection.provideViewOrders(getApplication());

        ViewOrders.RequestValues requestValues = new ViewOrders.RequestValues(false);
        requestValues.setOrderMonth(order_month);
        requestValues.setSubmitOrder(true);

        viewOrders.execute(requestValues, new UseCase.OutputBoundary<ViewOrders.ResponseValue>() {
            @Override
            public void onSuccess(ViewOrders.ResponseValue result) {


                String date = result.getDosageDate();
                int days =result.getExpiryDaysForVaccination();

                activityBinding.tvOrderDate.setText(date);
                setExpiryDays(days);

                ArrayList<VaccinePlan> vaccinePlans = new ArrayList<>();
                int total = 0;

                if(result.getDosageList()!=null){
                    for (Dosage dosage : result.getDosageList()) {
                        VaccinePlan vaccinePlan = new VaccinePlan();
                        vaccinePlan.setVaccineName(dosage.getVaccine());
                        vaccinePlan.setQuantityRequired(dosage.getDosageNeededNextMonth());
                        vaccinePlan.setQuantityAtHand(0);
                        vaccinePlan.setQuantityUsed(dosage.getDosageUsedThisMonth());
                        vaccinePlans.add(vaccinePlan);
                        total += dosage.getDosageNeededNextMonth();
                        updateUpcomingEvents(vaccinePlans, total, true);
                    }
                }


            }

            @Override
            public void onError(String errorMessage) {

            }
        });

    }

    private void setExpiryDays(int daysToGoToVaccination){

        String timeText;

        if(daysToGoToVaccination <= 7){//supposed to be 7 to indicae last 7 days

            timeText = "Ordering Exp in "+daysToGoToVaccination+" days";
            activityBinding.tvDateTiming.setText(timeText);


        }else{
            Calendar cal = Calendar.getInstance();
            int lastDayOfMonth = cal.getActualMaximum(Calendar.DATE);
            int dayOfmonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

            int daysOfOrderPeriod = lastDayOfMonth - 7;
            int daysToOrderPeriod = daysOfOrderPeriod - dayOfmonth;

            timeText = "Submit Order in "+daysToOrderPeriod+" days";
            activityBinding.tvDateTiming.setText(timeText);



        }
    }

    ///////////////////  ORDER SELECTION LISTENER /////////////////

    private class OrderListner implements AdapterOrderPlans.OnVaccinationCLickLIstner{
        @Override
        public void onAddClicked(TextView qtyAtHand, TextView qtyRequired, int position, VaccinePlan vaccinePlan) {


            String textValue = qtyAtHand.getText().toString();
            int qtyAtHandInt = Integer.parseInt(textValue) + 1;

            String qtyRequiredString = qtyRequired.getText().toString();
            int qtyRequiredInt = Integer.parseInt(qtyRequiredString);

            //you can increment only if the qtyReq is > 0 coz it cant go to -ves
            if(qtyRequiredInt > 0) {

                String newTextValue = Integer.toString(qtyAtHandInt);
                qtyAtHand.setText(newTextValue);

                int required = qtyRequiredInt -1;
                String newRequired = Integer.toString(required);
                qtyRequired.setText(newRequired);

                vaccinePlan.setQuantityAtHand(qtyAtHandInt);
                vaccinePlan.setQuantityRequired(required);

                if (adapterOrders != null) {
                    adapterOrders.upDateVaccinePlan(position, vaccinePlan);
                }
            }


        }

        @Override
        public void onMinusClicked(TextView qtyAtHand,TextView qtyRequired, int position, VaccinePlan vaccinePlan) {

            String textValue = qtyAtHand.getText().toString();
            int qtyAtHandInt = Integer.parseInt(textValue) - 1;


            String qtyRequiredString = qtyRequired.getText().toString();
            int qtyRequiredInt = Integer.parseInt(qtyRequiredString);


            if(qtyAtHandInt >= 0){
                String newTextValue = Integer.toString(qtyAtHandInt);
                qtyAtHand.setText(newTextValue);



                int required = qtyRequiredInt + 1;
                String newRequired = Integer.toString(required);
                qtyRequired.setText(newRequired);


                vaccinePlan.setQuantityAtHand(qtyAtHandInt);
                vaccinePlan.setQuantityRequired(required);


            }

            if(adapterOrders != null){
                adapterOrders.upDateVaccinePlan(position,vaccinePlan);
            }


        }
    }

    /////////////////// MAKE ORDER ///////////////////////////
    private void makeOrder(List<VaccinePlan> plans, int monthOfOrder){

        RequestOrder requestOrder = RequestOrder.getInstance(ModuleRepository.provideOrdersRepository(getApplication()));

        String username = PreferenceHandler.getInstance(getApplicationContext()).getPref(PreferenceHandler.USER_NAME);
        String hospitalId = PreferenceHandler.getInstance(getApplicationContext()).getPref(PreferenceHandler.USER_HOSPITAL_ID);
        String hospitalName = PreferenceHandler.getInstance(getApplicationContext()).getPref(PreferenceHandler.USER_HOSPITAL_NAME);
        String hospitalAddress = PreferenceHandler.getInstance(getApplicationContext()).getPref(PreferenceHandler.USER_HOSPITAL_ADDRESS);

        VaccineOrder vaccineOrder = new VaccineOrder();
        vaccineOrder.setHospitalName(hospitalName);
        vaccineOrder.setOrderDate(DateCalendarConverter.dateToStringWithoutTime(new Date()));
        vaccineOrder.setUserName(username);
        vaccineOrder.setOrderId(UUID.randomUUID().toString());
        vaccineOrder.setHospitalId(hospitalId);
        vaccineOrder.setVaccinePlans(plans);

        makeOrderUseMail(vaccineOrder,monthOfOrder);


        /*
        RequestOrder.RequestValues requestValues = new RequestOrder.RequestValues(hospitalId,username,plans,monthOfOrder);
        requestValues.setHospitalName(hospitalName);
        requestValues.setHospitalAddress(hospitalAddress);


        requestOrder.execute(requestValues, new UseCase.OutputBoundary<RequestOrder.ResponseValue>() {
            @Override
            public void onSuccess(RequestOrder.ResponseValue result) {
                //Save the order month, such that when
                //  Toast.makeText(getApplicationContext(),""+msg, Toast.LENGTH_LONG).show();
                String title = "Order";
                String subtitle = "Your order has been submitted on "+result.getVaccineOrder().getOrderDate();
                //  showAlert(title,subtitle,ViewOrderActivity.this,UIAlert.AlertType.SUCCESS);
                Snackbar.make(activityBinding.recyclerview, ""+subtitle, Snackbar.LENGTH_LONG)
                        .setDuration(5000)
                        .show();

                finish();


            }

            @Override
            public void onError(String errorMessage) {

                String subtitle = ""+errorMessage;

                if(errorMessage.startsWith("Order")) {
                    //showAlert(title, subtitle, ViewOrderActivity.this, UIAlert.AlertType.INFO);

                    Snackbar snackbar = Snackbar.make(activityBinding.recyclerview, subtitle, Snackbar.LENGTH_LONG);
                    snackbar.setDuration(6000);
                    UIAlert.showSnackBarView(snackbar);

                    //  UIAlert.showSnackBarView(snackbar);

                }else{
                    //showAlert(errorTitle, subtitle, ViewOrderActivity.this, UIAlert.AlertType.ERROR);
                    Snackbar snackbar = Snackbar.make(activityBinding.recyclerview, subtitle, Snackbar.LENGTH_LONG);
                    snackbar.setDuration(6000);
                    snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
                    snackbar.setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            makeOrder(plans,monthOfOrder);

                        }
                    }).show();


                }


            }
        });
        */


    }

    private void makeOrderUseMail(VaccineOrder vaccineOrder, int monthOfOrder) {

        String hospitalName = vaccineOrder.getHospitalName();
        String username = vaccineOrder.getUserName();
        String orderDate = vaccineOrder.getOrderDate();
        String hospitalId = vaccineOrder.getHospitalId();

        StringBuilder builder = new StringBuilder();

        String subject = "Vaccine Order: "+hospitalName;

       // builder.append(username);
       // builder.append(hospitalName);

        for(VaccinePlan plan: vaccineOrder.getVaccinePlans()){

            String info = " -  Vaccine: "+plan.getVaccineName()+", AtHand: "+plan.getQuantityAtHand()+", Req: "+plan.getQuantityRequired()+"";
            builder.append(info);
        }
        composeEmail(null,subject,builder.toString());
    }

    public void composeEmail(String[] addresses, String subject, String body) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_TEXT,body);
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }




        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu_home; this adds items to the action bar if it is present.

      getMenuInflater().inflate(R.menu.menu_order, menu);


        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {

            case R.id.action_order:
                List<VaccinePlan> vaccinePlans;

                if(adapterOrders != null && !adapterOrders.getVaccinePlans().isEmpty()){
                    vaccinePlans =  adapterOrders.getVaccinePlans();

                    for(VaccinePlan plan: vaccinePlans){

                        Log.d("VaccineOrder","Vaccine: "+plan.getVaccineName()+" QH: "+plan.getQuantityAtHand()+" QR"+plan.getQuantityRequired());

                    }



                    makeOrder(vaccinePlans,order_month_global_var);
                    //  finish();

                }

                return true;

        }

        return super.onOptionsItemSelected(item);
    }






}
