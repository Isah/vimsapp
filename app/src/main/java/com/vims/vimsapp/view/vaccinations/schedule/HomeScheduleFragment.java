package com.vims.vimsapp.view.vaccinations.schedule;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.elconfidencial.bubbleshowcase.BubbleShowCase;
import com.elconfidencial.bubbleshowcase.BubbleShowCaseBuilder;
import com.elconfidencial.bubbleshowcase.BubbleShowCaseListener;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import com.tapadoo.alerter.Alerter;
import com.vims.vimsapp.UseCase;

import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccineSpecification;
import com.vims.vimsapp.registerchild.AddChildDataSource;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsSchedule;
import com.vims.vimsapp.view.injection.ModuleRepository;
import com.vims.vimsapp.view.patients.detail.MyDetailsTypeConverter;
import com.vims.vimsapp.view.vaccinations.CardHeader;
import com.vims.vimsapp.viewcoverage.CoverageReport;
import com.vims.vimsapp.viewcoverage.DashBoardHeader;
import com.vims.vimsapp.viewcoverage.GenerateReport;
import com.vims.vimsapp.viewcoverage.VaccineReport;

import com.vims.vimsapp.viewcoverage.ReadVaccines;
import com.vims.vimsapp.sync.SyncManager;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.MainThreadExecutor;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.utilities.ThemeManager;
import com.vims.vimsapp.utilities.alarm.MyAlarmManager;
import com.vims.vimsapp.utilities.alarm.Notification;
import com.vims.vimsapp.utilities.feature_discovery.FeatureDiscovery;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.patients.addcaretaker.AddCaretakerWelcomeActivity;
import com.vims.vimsapp.view.patients.list.ItemOffsetDecoration;
import com.vims.vimsapp.view.reports.FragmentReportsViewModel;
import com.vims.vimsapp.view.vaccinations.viewmodel.ViewScheduleUpcomingFragmentViewModel;
import com.vims.vimsapp.view.vaccinations.viewmodel.ViewScheduleUpcomingFragmentViewModelFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInDownAnimator;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import vimsapp.R;
import vimsapp.databinding.VaccinationFragmentScheduleHomeBinding;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;
import static com.vims.vimsapp.utilities.animations.AnimatorManager.animateTextView;
import static com.vims.vimsapp.utilities.ThemeManager.bar_color1;
import static com.vims.vimsapp.utilities.ThemeManager.bar_color2;
import static com.vims.vimsapp.utilities.ThemeManager.bar_labels_color;
import static com.vims.vimsapp.utilities.ThemeManager.linechart_dataset_color;
import static com.vims.vimsapp.utilities.ThemeManager.xaxis_grid_color;
import static com.vims.vimsapp.utilities.ThemeManager.xaxis_label_text_color;
import static com.vims.vimsapp.utilities.ThemeManager.xaxis_value_text_color;
import static com.vims.vimsapp.utilities.ThemeManager.yaxis1_grid_Color;
import static com.vims.vimsapp.utilities.ThemeManager.yaxis1_scale_text_Color;
import static com.vims.vimsapp.utilities.ThemeManager.yaxis2_grid_Color;
import static com.vims.vimsapp.utilities.ThemeManager.yaxis2_scale_text_Color;
import static com.vims.vimsapp.utilities.alarm.UIAlert.showSnackBarView;
import static com.vims.vimsapp.view.injection.ModuleRepository.provideSyncManager;


public class HomeScheduleFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mScheduleType;

    public HomeScheduleFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static HomeScheduleFragment newInstance(String param1) {
        HomeScheduleFragment fragment = new HomeScheduleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);

        fragment.setArguments(args);
        return fragment;
    }



    PreferenceHandler preferenceHandler;



    static final String  ALARM_STATE = "alarmSate";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            mScheduleType = getArguments().getString(ARG_PARAM1);

            DEFAULT_THEME_ID = "R.style.AppTheme_Dark";
            LIGHT_THEME_ID = "R.style.AppTheme_Light";

            ThemeManager.setContext(getContext());
            ThemeManager.setUpGraphColorTheme(DEFAULT_THEME_ID);
            preferenceHandler = PreferenceHandler.getInstance(getContext());
        }


    }



    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putBoolean(ALARM_STATE, preferenceHandler.getBoolPref(PreferenceHandler.ALARM_STATE_KEY));
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }



    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if(savedInstanceState != null) {
            boolean isChecked = savedInstanceState.getBoolean(ALARM_STATE);
            alarmToggle.setChecked(true);
        }




    }

    /**
     * Instead of restoring the state during onCreate() you may choose to implement onRestoreInstanceState(),
     * @param savedInstanceState
     */





   /*
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
       // inflater.inflate(R.menu.main, menu);
        // MenuItem item = menu.findItem(R.id.action_search);
        // item.setVisible(false);
        Runnable  runnable = new Runnable() {
            @Override
            public void run() {
                tapTarget();
            }
        };
        new MainThreadExecutor(1000).execute(runnable);


    }

    public void tapTarget(){

        //TAP TARGET
        //when to show it
        preferenceHandler = new PreferenceHandler(getContext());
        int firsttime = preferenceHandler.getIntValue("tap_target_first_time");
        if(firsttime != 1) {
            // preferenceHandler.setIntValue("tap_target_first_time", 1);

            new MaterialTapTargetPrompt.Builder(getActivity())
                    .setTarget(getActivity().findViewById(R.id.action_hospital_schedule))
                    .setPrimaryText("Load the Hospital schedule")
                    .setSecondaryText("Tap the menu icon to get access to the hospital schedule")
                    .setAnimationInterpolator(new FastOutSlowInInterpolator())
                    .setBackgroundColour(getResources().getColor(R.color.colorPrimary))
                    .setOnHidePromptListener(new MaterialTapTargetPrompt.OnHidePromptListener() {
                        @Override
                        public void onHidePrompt(MotionEvent event, boolean tappedTarget) {
                            //TODO: Store in SharedPrefs so you don't show this prompt again.
                        }

                        @Override
                        public void onHidePromptComplete() {
                        }
                    })
                    .show();

        }

    }

    */

    VaccinationFragmentScheduleHomeBinding scheduleHomeBinding;
    ViewScheduleUpcomingFragmentViewModel viewScheduleFragmentViewModel;
    ViewScheduleUpcomingFragmentViewModelFactory scheduleUpcomingFragmentViewModelFactory;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        boolean isTab = getResources().getBoolean(R.bool.tablet);
        if(!isTab){


        }




    }


    //SWIPE AND TOUCH EVENTS
    SwipeRefreshLayout swipeRefreshLayout;
    EventState eventState;

    String DEFAULT_THEME_ID;
    String LIGHT_THEME_ID;

    String theme_id;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        theme_id = preferenceHandler.getPref(getResources().getString(R.string.pref_key_theme));

        // Inflate the layout for this fragment
        scheduleHomeBinding = DataBindingUtil.inflate(inflater, R.layout.vaccination_fragment_schedule_home, container, false);

        scheduleUpcomingFragmentViewModelFactory = new ViewScheduleUpcomingFragmentViewModelFactory(Injection.provideReadChildSchedule(getActivity().getApplication()), Injection.provideRecordVaccinationUseCase(getActivity().getApplication()), Injection.provideGenerateReport(getActivity().getApplication()), ModuleRepository.provideSearchVaccinationRepository(getActivity().getApplication()), Injection.provideHospitalSchedule(getActivity().getApplication()), getActivity().getApplication() );
        viewScheduleFragmentViewModel = ViewModelProviders.of(this, scheduleUpcomingFragmentViewModelFactory).get(ViewScheduleUpcomingFragmentViewModel.class);

        final CollapsingToolbarLayout collapsingToolbarLayout = scheduleHomeBinding.collapsingToolbar;
        collapsingToolbarLayout.setTitleEnabled(false);





        setUpToolBar();

             setUpRecyclerView();
            setupDrawables();

            fonts();

           setUpFonts();


        eventState = new EventState(getContext());
            eventState.putPref("recorded", "0");

            //loadRefreshAndScrollListners();


            /*


            observeGraphReports();
            */

            loadEventsReports();
            observeErrorNoData();
            observeProgress();
            observeGraphReports();
            observeUpcomingSchedule();


            //btnSchedule();
            //observeHospitalSchedule();

            setUpToggleScheduleAlarm();

            observeIsNotifyScheduleDate();

            scheduleHomeBinding.registerNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getContext(), AddCaretakerWelcomeActivity.class);
                    startActivity(intent);

                }
            });


            //first login load data caretakers from server
        int homeFirstTime = preferenceHandler.getIntValue("home_first_time");
        if(homeFirstTime!=1){ //1 means is old time user
            preferenceHandler.setIntValue("home_first_time",1);
            //syncDataPackets();
        }



        return scheduleHomeBinding.getRoot();

    }

    @Override
    public void setAllowEnterTransitionOverlap(boolean allow) {
        super.setAllowEnterTransitionOverlap(allow);
    }



    TextView tvUpcoming;
    TextView tvMissed;//missed
    TextView tvVaccinationsComplete;
    TextView tvDaysTogo;

    private void fonts(){
        //Revised
        tvUpcoming = scheduleHomeBinding.tvUpcomingEvents;
        tvMissed = scheduleHomeBinding.tvMissedEvents;//missed

     //   scheduleHomeBinding.tvUpcomingEventsLabel.setTypeface(MyFonts.getTypeFace(getContext()));
        scheduleHomeBinding.tvMissedEventsLabel.setTypeface(MyFonts.getTypeFace(getContext()));

        tvVaccinationsComplete = scheduleHomeBinding.tvVaccinationCompleted;
        tvDaysTogo = scheduleHomeBinding.tvDashboardDaysToGo;

        //color
       // tvUpcoming.setTextColor(getResources().getColor(R.color.colorAccent));
       // tvMissed.setTextColor(getResources().getColor(R.color.errorColor));

        //FONTS
        tvDaysTogo.setTypeface(MyFonts.getTypeFace(getContext()));
        tvUpcoming.setTypeface(MyFonts.getTypeFace(getContext()));
        tvMissed.setTypeface(MyFonts.getTypeFace(getContext()));
        tvVaccinationsComplete.setTypeface(MyFonts.getTypeFace(getContext()));
        scheduleHomeBinding.textViewHospital.setTypeface(MyFonts.getTypeFace(getContext()));
        scheduleHomeBinding.tvDashboardUpcomingVacccinationsLabel.setTypeface(MyFonts.getTypeFace(getContext()));
        scheduleHomeBinding.tvDashboardUpcomingVacccinationsLabelMonth.setTypeface(MyFonts.getTypeFace(getContext()));


    }


    //------------------- NOTIFICATIONS -------------------------------------------------------/


    Notification notification;
    private void showNotification(String nextDateOfVaccination){

        String today = DateCalendarConverter.dateToStringWithoutTime(new Date());
        if(today.equals(nextDateOfVaccination)){

            String title = "Immunisation Schedule";
            String content = "Vaccination of children is scheduled for today";
           // notification = new Notification(getContext(),title,content,R.drawable.vimslogo_small);
           // notification.showNotification();

        }


    }



    MyAlarmManager myAlarmManager;
    Switch alarmToggle;
    private void setUpToggleScheduleAlarm(){

        myAlarmManager = MyAlarmManager.getInstance(getContext());

        alarmToggle = scheduleHomeBinding.switch1;

        alarmToggle.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                        //compound button is the button the user taps

                        String toastMessage;
                        if(isChecked){

                            boolean isAlarm = preferenceHandler.getBoolPref(PreferenceHandler.ALARM_STATE_KEY);

                            if(!isAlarm) {
                                viewScheduleFragmentViewModel.setIsNotifyScheduledDate(true);
                                preferenceHandler.putBoolPref(PreferenceHandler.ALARM_STATE_KEY, true);
                            }


                        } else {
                            //Set the toast message for the "off" case.

                            viewScheduleFragmentViewModel.setIsNotifyScheduledDate(false);
                            preferenceHandler.putBoolPref(PreferenceHandler.ALARM_STATE_KEY,false);

                        }

                    }
                });
    }


    private void saveNextDateOfNotification(long date){

        preferenceHandler.putLongPref(PreferenceHandler.SCHEDULE_DATE_NOTIFY_KEY_LONG,date);

    }

    private void observeIsNotifyScheduleDate(){
        Observer<Boolean> isNotifyObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isNotify) {

               String toastMessage = "Immunisation Schedule Alarm";


                if(isNotify!= null){

                    if(isNotify){


                        Toast.makeText(getContext(),toastMessage+" On!",Toast.LENGTH_LONG).show();


                        alarmToggle.setChecked(true);
                       long date =  preferenceHandler.getLongPref(PreferenceHandler.SCHEDULE_DATE_NOTIFY_KEY_LONG);

                       myAlarmManager.startAlarm(new Date(date),MyAlarmManager.ALARM_TYPE_IMMUNISE);

                    }else{

                        Toast.makeText(getContext(),toastMessage+" Off",Toast.LENGTH_LONG).show();


                        alarmToggle.setChecked(false);
                        myAlarmManager.stopAlarm();
                       // Notification notification = new Notification(getContext(),"","",0);
                       // notification.cancelNotifications();


                    }


                }


            }
        };

        viewScheduleFragmentViewModel.getIsNotifyScheduledDate().observe(getActivity(),isNotifyObserver);


    }



    //ANIMATE VIEW TO INSTRUCT THAT THERE IS MORE CONTENT SCROLLABLE
     /*
    @Override
    public void onEnterAnimationComplete() {
        super.onEnterAnimationComplete();

        final int startScrollPos = getResources().getDimensionPixelOffset(R.dimen.init_scroll_up_distance);
       Animator animator =  ObjectAnimator.ofInt(detailBinding.childDetailInfo.recyclerViewHolder,"scrollY",startScrollPos).setDuration(300);
       animator.start();

    }
    */


    //-------------------SEARCH BAR  -------------------------------------------------------/
    private void setUpSearchBar(){

        /*

        // Search bar
        searchBar = findViewById(R.id.searchBar);
        //enable searchbar callbacks
        //searchBar.setOnSearchActionListener(this);
        //restore last queries from disk
        lastSearches = searchBar.getLastSuggestions();
        searchBar.setLastSuggestions(lastSearches);
        //Inflate menu and setup OnMenuItemClickListener
        searchBar.inflateMenu(R.menu.main);
        searchBar.setSearchIcon(R.drawable.ic_search_black_24dp);
        searchBar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        searchBar.setCardViewElevation(10);
        searchBar.setDividerColor(R.color.colorAccent);
        //searchBar.setPlaceHolderColor(getResources().getColor(R.color.light_blue));

        //  searchBar.setNavButtonEnabled(true);



        // SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        ComponentName cn = new ComponentName(this, SearchCaretakerActivity.class);



        searchBar.setOnSearchActionListener(new SimpleOnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {
                String s = enabled ? "enabled" : "disabled";
                Toast.makeText(MainActivity.this, "Search " + s, Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                //  startSearch(text.toString(), true, null, true);
                //   setSearchableInfo(searchManager.getSearchableInfo(cn));





            }

            @Override
            public void onButtonClicked(int buttonCode) {
                switch (buttonCode) {
                    case MaterialSearchBar.BUTTON_NAVIGATION:
                        drawer.openDrawer(Gravity.LEFT);
                        break;
                    case MaterialSearchBar.BUTTON_SPEECH:
                        Toast.makeText(MainActivity.this, "Speech ", Toast.LENGTH_SHORT).show();

                        break;
                    case MaterialSearchBar.BUTTON_BACK:
                        searchBar.disableSearch();
                        break;
                }
            }
        });


        searchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("LOG_TAG", getClass().getSimpleName() + " text changed " + searchBar.getText());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }

        });

*/
    }

    private void setUpToolBar(){

        Toolbar toolbar = scheduleHomeBinding.toolbar;

        //sett bold
        SpannableStringBuilder str = new SpannableStringBuilder("VIMS");
        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        toolbar.setTitle(str);
       // toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);

        //toolbar.setTitleTextColor(getResources().getColor(R.color.textColorPrimary));
        toolbar.setTitleMarginStart(getResources().getInteger(R.integer.fragment_title_margin));
       // toolbar.setNavigationIcon(getResources().ge);

        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar); //we replace our actionbar with our toolbar
        if(actionBar!=null) {
          //  actionBar.setHomeButtonEnabled(true);
           // actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


    }

    private void setupDrawables(){

          //scheduleHomeBinding.ivTarget.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_menu_schedule_blue_24dp),null,null,null);
        // scheduleHomeBinding.errorMsgTv.setTypeface(MyFonts.getTypeFace(getContext()));
        // scheduleHomeBinding.errorMsgTvTitle.setTypeface(MyFonts.getBoldTypeFace(getContext()));
        // scheduleHomeBinding.tvErrorRegister.setTypeface(MyFonts.getTypeFace(getContext()));


            Drawable drawableIcon = AppCompatResources.getDrawable(getContext(),R.drawable.ic_menu_schedule_black_24dp);
         //   DrawableManager.setUpDrawableStart(scheduleHomeBinding.ivTarget,drawableIcon);




    }

    private void setUpFonts(){
       // scheduleHomeBinding.tvTitle.setTypeface(MyFonts.getBoldTypeFace(getContext()));
       //scheduleHomeBinding.tvSubtitleOverviewUpcomingVacs.setTypeface(MyFonts.getBoldTypeFace(getContext()));
       // scheduleHomeBinding.tvTitleOverviewUpcomingVacs.setTypeface(MyFonts.getTypeFace(getContext()));

        scheduleHomeBinding.tvTitleOverviewGraph.setTypeface(MyFonts.getTypeFace(getContext()));
        scheduleHomeBinding.tvTitleOverviewVaccine.setTypeface(MyFonts.getTypeFace(getContext()));


        //Error View
        scheduleHomeBinding.welcomeTitle.setTypeface(MyFonts.getTypeFace(getContext()));
        scheduleHomeBinding.vaccineDescriptionHowGiven.setTypeface(MyFonts.getTypeFace(getContext()));

       //Header
        scheduleHomeBinding.tvVaccinationDay.setTypeface(MyFonts.getTypeFace(getContext()));
       // scheduleHomeBinding.tvVaccinationMonth.setTypeface(MyFonts.getBoldTypeFace(getContext()));//btn


        
        //FONTS
        // tvVaccinationsUpcoming.setTypeface(MyFonts.getBoldTypeFace(getContext()));
        // tvDosagePercentage2.setTypeface(MyFonts.getBoldTypeFace(getContext()));
        // tvVaccinationsUpcoming.setTypeface(MyFonts.getBoldTypeFace(getContext()));
        // tvVaccinationsMissed.setTypeface(MyFonts.getBoldTypeFace(getContext()));

    }




    //-------------------SCHEDULE -------------------------------------------------------/


    private void btnSchedule(){

        scheduleHomeBinding.ivTarget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setUpScheduleDialog();

            }
        });

    }

    private void syncDataPackets(){

        scheduleHomeBinding.progressbarHome.setVisibility(View.VISIBLE);

        SyncManager syncManager = provideSyncManager(getContext());

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                syncManager.syncData(true,new AddChildDataSource.SyncPatientsCallback() {
                    @Override
                    public void onSynced(String msg) {

                        Runnable run = new Runnable() {
                            @Override
                            public void run() {

                                scheduleHomeBinding.progressbarHome.setVisibility(View.INVISIBLE);

                             //   loadEventsReports();

                            }
                        };

                        new MainThreadExecutor(1000).execute(run);

                    }

                    @Override
                    public void onSyncFailed(String errorMsg) {

                        Runnable run = new Runnable() {
                            @Override
                            public void run() {


                                scheduleHomeBinding.progressbarHome.setVisibility(View.INVISIBLE);

                                Snackbar snackbar = Snackbar.make(scheduleHomeBinding.view, errorMsg, Snackbar.LENGTH_LONG);
                                    snackbar.setDuration(4000);
                                    snackbar.show();
                                    showSnackBarView(snackbar);

                            }
                        };

                        new MainThreadExecutor(1000).execute(run);

                    }
                });

            }
        };

        new MainThreadExecutor(6000).execute(runnable);

    }

    private void setUpScheduleDialog(){

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.Theme_AppCompat_Light_Dialog_Alert);

            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int year2 = year + 2;

            String cater = "from "+year+"-"+year2;

            String dialogTitle = "Load New Hospital Schedule ?";
            String dialogDesc =  "You are about to replace the existing hospital schedule with the latest "+ cater;

            builder.setTitle(dialogTitle);
            builder.setMessage(dialogDesc);
            builder.setPositiveButton("Comfirm", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {


                    preferenceHandler = PreferenceHandler.getInstance(getContext());
                    String hid = preferenceHandler.getPref(PreferenceHandler.USER_HOSPITAL_ID);
                    viewScheduleFragmentViewModel.loadHospitalSchedule(true,hid);


                }

            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {


                }
            });

            AlertDialog dialog = builder.create();


            //if using api >= 19 use TypeToast not type_sytem_alert

            //  dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);

            dialog.show();



    }

    private void showAlert(String Title, String content, Activity activity, int colorInt) {
        Alerter.create(activity)
                .setTitle(Title)
                .setText(content)
                .setBackgroundColorRes(colorInt) // or setBackgroundColorInt(Color.CYAN)
                .show();
    }

    private void observeHospitalSchedule(){

        Observer<String> stringObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                if(s!=null){
                 //   Toast.makeText(getContext(), s, Toast.LENGTH_LONG).show();

                 //   FancyToast.makeText(getContext(),""+s,FancyToast.LENGTH_LONG, FancyToast.SUCCESS,false).show();

                    String title = "Success";
                    String content = "Hospital "+s;

                    showAlert(title,content,getActivity(),R.color.colorPrimary);

                    /*
                    Snackbar.make(progressBar, "Hospital Schedule "+s, Snackbar.LENGTH_LONG)
                            .setDuration(5000)
                            .setAction("Action", null).show();

                    progressBar.setVisibility(View.INVISIBLE);
                    */

                }else{

                }

            }
        };

        viewScheduleFragmentViewModel.getHospitalScheduleLoadFeedback().observe(this,stringObserver);

    }

    private void discoverNewSchedule(){

        View targetView =  scheduleHomeBinding.ivTarget;
        String primaryText = "Load New Hospital Schedule";
        String secondaryText = "Tap the menu icon and load a new hospital schedule when available";

      //  BubbleShowCase bubbleShowCase = new BubbleShowCase(new BubbleShowCaseBuilder(getActivity()));
        BubbleShowCaseBuilder builder = new BubbleShowCaseBuilder(getActivity());
        builder.title(primaryText);
        builder.targetView(targetView);
        builder .description(secondaryText); //More detailed description
        builder.arrowPosition(BubbleShowCase.ArrowPosition.TOP); //You can force the position of the arrow to change the location of the bubble.
        builder.backgroundColor(getResources().getColor(R.color.colorPrimary)); //Bubble background color
        builder.textColor(Color.WHITE); //Bubble Text color
        builder.titleTextSize(17); //Title text size in SP (default value 16sp)
        builder.descriptionTextSize(15); //Subtitle text size in SP (default value 14sp)

        builder .showOnce("BUBBLE_SHOW_CASE_ID") ;//Id to show only once the BubbleShowCase

        builder.listener(new BubbleShowCaseListener() {
            @Override
            public void onTargetClick(BubbleShowCase bubbleShowCase) {

                bubbleShowCase.finishSequence();
            }

            @Override
            public void onCloseActionImageClick(BubbleShowCase bubbleShowCase) {
                bubbleShowCase.finishSequence();
            }

            @Override
            public void onBackgroundDimClick(BubbleShowCase bubbleShowCase) {

                bubbleShowCase.finishSequence();
            }

            @Override
            public void onBubbleClick(BubbleShowCase bubbleShowCase) {

                bubbleShowCase.finishSequence();
            }
        });

        builder.show();



    }

    private void tapTargetSchedule(){


        String primaryText = "Load New Hospital Schedule";
        String secondaryText = "Tap the menu icon and load a new hospital schedule if available";
        String preferenceText = "hospital_schedule_first_time";

        View targetView =  scheduleHomeBinding.ivTarget;


        Drawable drawableIcon = AppCompatResources.getDrawable(getContext(),R.drawable.ic_schedule_black_16dp);

            //tapTarget(ViewChildDetailsActivity.this,detailBinding.actionGeneratePdf,preferenceText,primaryText,secondaryText);
            FeatureDiscovery.tapTargetKeepSafe(getActivity(), targetView, drawableIcon, preferenceText, primaryText, secondaryText);

    }

    /////////////////////////END OF SCHEDULE ///////////////////////////////////////////










    int pStatus = 0;
    String percentage = "";

    private void startCircularProgress(int progress) {

        Handler handler = new Handler();

        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (pStatus < progress) {
                    pStatus += 1;
                    percentage = Integer.toString(progress)+"%";

                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub

                           // scheduleHomeBinding.circularProgressbar.setProgress(pStatus);
                            //scheduleHomeBinding.tvDashboardDonePercentage.setText(percentage);

                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        // Just to display the progress slowly
                        Thread.sleep(50); //thread will take approx 3 seconds to finish
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }


    private void observeUpcomingSchedule(){

        Observer<List<VaccinationEvent>> vcardsObserver = new Observer<List<VaccinationEvent>>() {
            @Override
            public void onChanged(@Nullable List<VaccinationEvent> vaccinationEvents) {



                if(vaccinationEvents != null) {


                    Collections.sort(vaccinationEvents);

                    List<VaccinationEvent>  unDuplicatesList = new ArrayList<>(new LinkedHashSet<>(vaccinationEvents));

                    String itemsSize = "No. doses: "+unDuplicatesList.size()+"";
                    // tvMonthPlan.setText(itemsSize);

                    if(!vaccinationEvents.isEmpty()) {
                        scheduleHomeBinding.profile.tvDosesNo.setText(itemsSize);
                        updateUpcomingEvents(unDuplicatesList);
                        Log.d("SearchSchedule","?size: "+vaccinationEvents.size());

                    }else{
                        // setUpSpinner();
                    }

                }
            }
        };

        viewScheduleFragmentViewModel.upcomingVaccinations().observe(this, vcardsObserver);

    }


    AdapterUpcomingEvents adapterUpcomingEvents;

    public void updateUpcomingEvents(List<VaccinationEvent> vaccinationEvents){



        AdapterUpcomingEvents.clearInstance();

        //VaccinationStatistic vstat =  viewScheduleFragmentViewModel.getStatistic(vaccinationEvents.size());
        /// String cardTitle = "vaccinations:"+vstat.getTotalNoOfFinishedVaccinations()+"/"+vstat.getTotalNoOfVaccinations();
        String date =  DateCalendarConverter.dateToStringWithoutTimeAndDay(new Date());
        String card = "";
        CardHeader cardHeader = new CardHeader();
        cardHeader.setTitle(date);
        cardHeader.setSubTitle(card);
        //cardHeader.setImmunisationCoveragePercentage(vstat.getTotalVaccinationPercentage());

        adapterUpcomingEvents = AdapterUpcomingEvents.getINSTANCE(cardHeader,vaccinationEvents, getActivity().getApplication(), Injection.provideRecordVaccinationUseCase(getActivity().getApplication()));
        adapterUpcomingEvents.setActivity(getActivity());
        adapterUpcomingEvents.setClickListener(new AdapterVaccinationEventListner());
        //adapterUpcomingEvents.setSpinnerClickListner(new AdapterSpinnerListner());

        if (adapterUpcomingEvents.getItemCount()<1) {
            Log.d("FUS","count < 1 resumed");
        }


        //adapterVaccinationEvents.setHasStableIds(true);ooncomingEvents.notifyItemInserted(0);
        int prevSize = vaccinationEvents.size();
        adapterUpcomingEvents.notifyItemRangeInserted(prevSize, vaccinationEvents.size() -prevSize);

        scheduleHomeBinding.profile.recyclerview.setNestedScrollingEnabled(false);

        scheduleHomeBinding.profile.recyclerview.postDelayed(new Runnable() {
            @Override
            public void run() {
                scheduleHomeBinding.profile.recyclerview.setAdapter(adapterUpcomingEvents);

                //adapterUpcomingEvents.getFilter().filter("January");

            }
        },100);


    }


    public class AdapterVaccinationEventListner implements AdapterUpcomingEvents.OnVaccinationCLickLIstner {

        @Override
        public void onVaccinationClicked(View view, int position, VaccinationEvent vaccinationEvent) {

            Bundle bundle = new Bundle();
            bundle.putString("vevent", MyDetailsTypeConverter.fromVaccinationEventToString(vaccinationEvent));
            bundle.putInt("position",position);


            String transitionName  = getResources().getString(R.string.transitionNameEvent);
            Intent intent = new Intent(getContext(),ComfirmVaccinationActivity.class);
            intent.putExtras(bundle);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                //shared Element Transitions
                // ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, p1, p2, p3);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), view, transitionName);
                startActivity(intent, options.toBundle());
                //Exit Transitions
                //Bundle bundle = ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle();
                //context.startActivity(intent, bundle);

            } else {
                startActivity(intent);
            }

            // toggleBottomSheet(vaccinationEvent);


        }
    }



    private void setUpHomeScreenData(DashBoardHeader dashBoardHeader){

        //set up screen
        String username = preferenceHandler.getPref(PreferenceHandler.USER_NAME);
        //scheduleHomeBinding.profile.basicInfo.setText(username);

        Date nextImmunisationDate =  dashBoardHeader.getNextHospitalDate();
        saveNextDateOfNotification(nextImmunisationDate.getTime());

        String nextDate = DateCalendarConverter.dateToStringWithoutTime(nextImmunisationDate);
        String[] dateArray = nextDate.split(" ");

        String m = dateArray[0];
        String d = dateArray[1];
        String y = dateArray[2];

        if (m.length() > 3) {
            m = m.substring(0, 3);
        }


        String dayOfWeek = DateCalendarConverter.dateToDayOfWeek(nextImmunisationDate);
        String splitDate = d+" "+m;


        scheduleHomeBinding.tvDay.setText(dayOfWeek);
        scheduleHomeBinding.tvMonthlyPlanDate.setText(splitDate);
        String dateOfVaccination = DateCalendarConverter.dateToStringWithoutTime(nextImmunisationDate);
        //showNotification(DateCalendarConverter.dateToStringWithoutTime(nextImmunisationDate));


        String[] dateofvac = dateOfVaccination.split(" ");
        String vm = dateofvac[0];
        String vd = dateofvac[1];
        String vy = dateofvac[2];

        if (vm.length() > 3) {
            vm = vm.substring(0, 3);
        }

        //get the month to be used as input in spinner
        Calendar monthCalendar = Calendar.getInstance();
        monthCalendar.setTime(nextImmunisationDate);

        scheduledMonth = monthCalendar.get(Calendar.MONTH);
        int scheduledYear = monthCalendar.get(Calendar.YEAR);

        String vdate = vd+" "+vm+" "+vy;
        scheduleHomeBinding.tvVaccinationDay.setText(vdate);

        String hospitalName = preferenceHandler.getPref(PreferenceHandler.USER_HOSPITAL_NAME);
        String hospital = hospitalName.replace("Hospital","H.");

        String titleHospital;
        int firsttime = preferenceHandler.getIntValue("welcome");
        if(firsttime != 1) {
            titleHospital = "Welcome, "+hospital;
            preferenceHandler.setIntValue("welcome",1);
        }else{
            titleHospital = ""+hospital+"- Outreach Vaccination";
        }


        scheduleHomeBinding.textViewHospital.setText(hospital);
        scheduleHomeBinding.tvVaccinate.setText(titleHospital);


        Bundle bundle = new Bundle();
        //ConstraintLayout cardView1 = scheduleHomeBinding.cardView1;
        //Load the recycler view of upcoming events
        String month = DateCalendarConverter.monthIntToString(scheduledMonth);

        Log.d("NullEvent", ""+m+" "+scheduledYear);


        Spinner eventTypeSpinner = scheduleHomeBinding.spinnerEventType;
        String item = eventTypeSpinner.getSelectedItem().toString();

        if(item.equals("Upcoming")){
            viewScheduleFragmentViewModel.loadUpcomingVaccinationEvents(dateArray[0],scheduledYear, SearchVaccinationsSchedule.VaccinationsRequestType.DUE);
        }else if(item.equals("Missed")){
            viewScheduleFragmentViewModel.loadUpcomingVaccinationEvents(dateArray[0],scheduledYear, SearchVaccinationsSchedule.VaccinationsRequestType.OVERDUE);
        }

        eventTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getSelectedItem().toString();

                if(item.equals("Upcoming")){
                    viewScheduleFragmentViewModel.loadUpcomingVaccinationEvents(dateArray[0],scheduledYear, SearchVaccinationsSchedule.VaccinationsRequestType.DUE);
                }else if(item.equals("Missed")){
                    viewScheduleFragmentViewModel.loadUpcomingVaccinationEvents(dateArray[0],scheduledYear, SearchVaccinationsSchedule.VaccinationsRequestType.OVERDUE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        scheduleHomeBinding.cardViewDosageDue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//Due

                bundle.putInt("nextScheduledMonth",monthCalendar.get(Calendar.MONTH));
                bundle.putInt("nextScheduledYear",monthCalendar.get(Calendar.YEAR));
                Intent intent = new Intent(getContext(), UpcomingSchedulesActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });
        scheduleHomeBinding.cardViewDosageMissed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), MissedScheduleActivity.class);
                startActivity(intent);

            }
        });


        ////  2nd Header  /////

        setUpDashBoard(dashBoardHeader);

    }

    public static final String pendingTitle = "Pending";
    public static final String missedTile = "Missed";


    private void setUpDashBoard(DashBoardHeader dashBoard){

        DashBoardSchedule dashBoardScheduleUpcoming = new DashBoardSchedule();
        dashBoardScheduleUpcoming.setMonthOfVaccination("January,2018");



        int upcoming = dashBoard.getUpcomingVaccinations();
        int missed  = dashBoard.getMissedVaccinations();
        int upcomingCurrentMonth = dashBoard.getUpcomingVaccinationsCurrentMonth();
        int missedCurrentMonth  = dashBoard.getMissedVaccinationsCurrentMonth();
        int done = dashBoard.getDoneVaccinations();
        int totalImmunisedChildren = dashBoard.getTotalChildrenImmunised();

        int totalDoses = upcoming+missed+done;


        String missedString = Integer.toString(missed)+" doses ";
        String upcomingString  = Integer.toString(upcoming)+" doses";
        String missedCurrentMonthString = Integer.toString(missedCurrentMonth)+"/";
        String upcomingCurrentMonthString  = Integer.toString(upcomingCurrentMonth)+" doses";
        String totalString  = ""+totalDoses+"";
        String doneString  = ""+Integer.toString(done)+" used doses of "+totalDoses;


        //int coverage = (int) (((double) totalImmunisedChildren / (double) TARGET_POPULATION * 100));
       // int totalVaccinations = (upcoming+missed+totalImmunisedChildren) * 16;

       // int percentageUpcoming = (int) (((double) upcoming / (double) totalVaccinations * 100));
       // int percentageMissed = (int) (((double) missed / (double) totalVaccinations * 100));
        int percentageDone = (int) (((double) done / (double) totalDoses * 100));


        String month  = "In "+DateCalendarConverter.monthIntToString(DateCalendarConverter.dateToIntGetMonth(new Date()))+":";
        String doses = "doses .";
      //   TextView labelUpcoming = scheduleHomeBinding.tvUpcomingEventsLabel;
      //  TextView labelMissed = scheduleHomeBinding.tvMissedEventsLabel;
      //  labelUpcoming.setText(doses);
      //  labelMissed.setText(doses);


        tvUpcoming.setText(upcomingCurrentMonthString);
       // tvMissed.setText(missedCurrentMonthString);
        tvVaccinationsComplete.setText(doneString);
        //scheduleHomeBinding.tvVaccinationCoverage.setText(totalString);
        scheduleHomeBinding.tvVaccinationCoverage.setTypeface(MyFonts.getTypeFace(getContext()));
        //total
        scheduleHomeBinding.tvHealth.setText(upcomingString);
        scheduleHomeBinding.tvHealthMonth.setText(missedString);

        if(missed>0){
          //  scheduleHomeBinding.tvHealthMonth.setTextColor(getResources().getColor(R.color.errorColor));
        }

        scheduleHomeBinding.tvHealthMonth.setTypeface(MyFonts.getTypeFace(getContext()));
        scheduleHomeBinding.tvHealth.setTypeface(MyFonts.getTypeFace(getContext()));


        ProgressBar progressBarUpcoming = scheduleHomeBinding.circularProgressbarDashboard;
        ProgressBar progressBarDone = scheduleHomeBinding.circularProgressbarDone;
        startCircularProgress(percentageDone, progressBarDone);



        progressBarUpcoming.setProgressDrawable(ThemeManager.getProgressBarDrawable(ThemeManager.VACCINATION_UPCOMING));



        String daysToGo = "";

        long days = DateCalendarConverter.calculateDifferenceInDates(dashBoard.getNextHospitalDate(),new Date());
        if(days == 0){
            daysToGo = "Due Today";
            tvDaysTogo.setText(daysToGo);
            scheduleHomeBinding.tvDaysLabel.setText("");
            tvDaysTogo.setTextColor(getContext().getResources().getColor(R.color.colorAccent));
        }else{
            daysToGo = days+"";
            int daysInt = Integer.parseInt(daysToGo);
            tvDaysTogo.setText(daysToGo);
            animateTextView(0,daysInt,tvDaysTogo);
        }


        // animateCountDown(tvDaysTogo,daysInt);
        //AnimatorManager.countDownStart(DateCalendarConverter.dateToStringWithoutTime(dashBoard.getNextHospitalDate()),tvDaysTogo);

        //tvVaccinationsUpcoming.setText(upcomingString);
        //tvVaccinationsMissed.setText(missedString);
        //tvChildrenImmunised.setText(immunisedChldrenString);//upcoming

    }


    private void setUpCircularProgressDrawable(int vaccinationType, ProgressBar progressBar){

        progressBar.setProgressDrawable(ThemeManager.getProgressBarDrawable(vaccinationType));


    }
    int pStatus1;
    private void startCircularProgress(int progress, ProgressBar progressBar) {
        pStatus1 = 0;
       // Log.d("ARAltProgress","I-Progress"+progress);
        // Log.d("ARAltProgress","I-Progress"+pStatus);
        Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (pStatus1 < progress) {
                    pStatus1 += 1;
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            progressBar.setProgress(pStatus1);
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        // Just to display the progress slowly
                        Thread.sleep(70); //thread will take approx 3 seconds to finish
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }





    int scheduledMonth = 0;

    private void setUpDataToGraph(){
        //plot a graph of coverage percentage with months
        GenerateReport generateReport = Injection.provideGenerateReport(getActivity().getApplication());

        String searchDate = DateCalendarConverter.dateToStringWithoutTimeAndDay(new Date());
        GenerateReport.ReportType reportType = GenerateReport.ReportType.VACCINE_SPECIFIC_HOME;

        GenerateReport.RequestValues requestValues = new GenerateReport.RequestValues(reportType,searchDate);

        generateReport.execute(requestValues, new UseCase.OutputBoundary<GenerateReport.ResponseValue>() {
            @Override
            public void onSuccess(GenerateReport.ResponseValue result) {

                scheduleHomeBinding.chart.setVisibility(View.VISIBLE);
                scheduleHomeBinding.cardViewHomeWelcome.setVisibility(View.INVISIBLE);
                scheduleHomeBinding.cardViewGraph.setVisibility(View.VISIBLE);
                scheduleHomeBinding.cardViewDosageDue.setVisibility(View.VISIBLE);
                scheduleHomeBinding.cardViewDosageMissed.setVisibility(View.VISIBLE);
                //to cntroll tap target showing
                scheduleHomeBinding.overview.setVisibility(View.VISIBLE);
                /*
                scheduleHomeBinding.tvTitleOverview.setVisibility(View.VISIBLE);
                scheduleHomeBinding.tvSubtitleOverview.setVisibility(View.VISIBLE);
                scheduleHomeBinding.relativeLayout.setVisibility(View.VISIBLE);
                */

                CoverageReport coverageReport = result.getCoverageReport();
              //  List<VaccineReport> vaccineReports =  coverageReport.getVaccineReportList();




                //1. Create Data Entry Objects
                List<BarEntry> entries = new ArrayList<>();

                /*
                for (VaccineReport data : vaccineReports) {
                    // turn your data into Entry objects
                    entries.add(new BarEntry(data.getxAxisValue(), data.getCoverageImmunisedDelayed()));
                }
                */


                //2. Labels for the objects
                ArrayList<String> xAxisLbels = new ArrayList<>();


                List<VaccineSpecification> vaccineSpecifications = ReadVaccines.getInstance().getVaccineSpecificationList();

                for(VaccineSpecification vspec: vaccineSpecifications){

                    xAxisLbels.add(vspec.getVaccineName());

                }




                //DataSet objects hold data which belongs together, and allow individual styling of that data.
                BarDataSet dataSet = new BarDataSet(entries,"Children");
                dataSet.setStackLabels(new String[]{"Immunised", "Not Immunised"});

                //change color of stack labels thus the bar lables
                scheduleHomeBinding.chart.getLegend().setTextColor(bar_labels_color);



                // colors for each bar
                int barColors[] = new int[2];
                barColors[0] = bar_color1;
                barColors[1] = bar_color2;

                //colors for the percentage values of each bar
                dataSet.setColors(barColors);
                dataSet.setValueTextColor(xaxis_value_text_color);


                //ad labels horizontally
                //YAXIS
                YAxis yAxis1 = scheduleHomeBinding.chart.getAxisLeft();
                YAxis yAxis2 = scheduleHomeBinding.chart.getAxisRight();
                //Axis horizontal line
                yAxis1.setDrawAxisLine(false);
                yAxis2.setDrawAxisLine(false);
                yAxis1.setDrawGridLines(false);
                //yAxis2.setDrawGridLines(false);
                yAxis1.setGridColor(yaxis1_grid_Color);
                yAxis2.setGridColor(yaxis2_grid_Color);

                //text color for the y axis scale values
                yAxis1.setTextColor(yaxis1_scale_text_Color); //same color as bar to hide the values
                yAxis2.setTextColor(yaxis2_scale_text_Color);



                //XAXIS
                XAxis xAxis = scheduleHomeBinding.chart.getXAxis();
                xAxis.setGranularity(1f);
                //set scale value text color
                xAxis.setTextColor(xaxis_label_text_color);
                xAxis.setCenterAxisLabels(false);
                //xAxis.setAvoidFirstLastClipping(true);
                xAxis.setDrawGridLines(false);
                xAxis.setLabelCount(16);
                //  xAxis.setDrawLimitLinesBehindData(false);


                xAxis.setAxisMinimum(dataSet.getXMin()-.5f);
                xAxis.setAxisMaximum(dataSet.getXMax()+.5f);



                //position of the labels
                xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                //label angle
                xAxis.setLabelRotationAngle(0); //this angle is also good -35
                //set space between bars
                xAxis.setSpaceMin(13f);
                xAxis.setSpaceMax(13f);
                xAxis.setXOffset(5);//xaxis margin from the view



                //hide the grid color
                xAxis.setGridColor(xaxis_grid_color);
                xAxis.setTextSize(12);
                xAxis.setTypeface(MyFonts.getTypeFace(getContext()));
                //xAxis.setLabelRotationAngle(-90);
                xAxis.setValueFormatter(new IAxisValueFormatter() {
                    @Override
                    public String getFormattedValue(float value, AxisBase axis) {

                        if(value >= 0){
                            if (value <= xAxisLbels.size() - 1) {
                                return xAxisLbels.get((int) value);
                            }
                            return "";
                        }
                        return "";
                    }
                });



                float barWidth = 0.7f; // x4 dataset

                //ArrayList<IBarDataSet> dataSets = new ArrayList<>();
                //DataSets.add(dataSet);
                //This object holds all data that is represented by a Chart
                //LineData lineData = new LineData(dataSet);
                BarData barData = new BarData(dataSet);
                barData.setBarWidth(barWidth); // set custom bar width
                barData.setHighlightEnabled(false);
                barData.setDrawValues(true);


                scheduleHomeBinding.chart.setFitBars(true); // make the x-axis fit exactly all bars
                scheduleHomeBinding.chart.setData(barData);
                //vhHeader.barChart.setMaxVisibleValueCount(12);

                scheduleHomeBinding.chart.setHighlightFullBarEnabled(false);
                scheduleHomeBinding.chart.getDescription().setEnabled(false);
                scheduleHomeBinding.chart.animateY(1500);
                scheduleHomeBinding.chart.setFitsSystemWindows(true);
                scheduleHomeBinding.chart.setNoDataText("No children to vaccinate, go register some children!");
                scheduleHomeBinding.chart.invalidate(); // refresh

              //  startCircularProgress(percent);
               // scheduleHomeBinding.tvTitleOverviewUpcomingVacs.setTypeface(MyFonts.getTypeFace(getContext()));


            }

            @Override
            public void onError(String errorMessage) {

                //setUpGraph(entries);
                scheduleHomeBinding.chart.setVisibility(View.INVISIBLE);
                scheduleHomeBinding.cardViewHomeWelcome.setVisibility(View.VISIBLE);
                scheduleHomeBinding.cardViewGraph.setVisibility(View.INVISIBLE);
                scheduleHomeBinding.cardViewDosageDue.setVisibility(View.INVISIBLE);
                scheduleHomeBinding.cardViewDosageMissed.setVisibility(View.INVISIBLE);
               // scheduleHomeBinding.overview.setVisibility(View.INVISIBLE);

               // scheduleHomeBinding.tvTitleOverview.setVisibility(View.INVISIBLE);
               // scheduleHomeBinding.tvSubtitleOverview.setVisibility(View.INVISIBLE);


                String percentage ="";
                String nextDate ="";
                String dosageTotal ="";


            }
        });



    }




    /*
    public void loadRefreshAndScrollListners(){

        scheduleHomeBinding.recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (adapterScheduleHome.getItemCount()<1) {
                    scheduleHomeBinding.swiperefresh.setEnabled(true);

                }else{
                    scheduleHomeBinding.swiperefresh.setEnabled(false);

                }


            }
        });


        scheduleHomeBinding.swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                scheduleHomeBinding.swiperefresh.setEnabled(true);

              //  viewScheduleFragmentViewModel.removeEventsObserver(vcardsObserver);
                //load items here
               // viewScheduleFragmentViewModel.loadUpcomingVaccinationEvents();
             //   observeUpcomingSchedule();

                scheduleHomeBinding.swiperefresh.setRefreshing(false);

            }
        });
    }
   */

    @Override
    public void onResume() {
        super.onResume();

            boolean isChecked = preferenceHandler.getBoolPref(PreferenceHandler.ALARM_STATE_KEY);
            alarmToggle.setChecked(isChecked);


        if(getActivity().getApplication()!=null) {
            if (adapterUpcomingEvents != null) {

                String vaccinationRecorded = eventState.getPref("recorded");
                if (vaccinationRecorded.equals("1")) {

                    eventState.putPref("recorded", "0");

                    String index = eventState.getPref("recordedIndex");
                    int indexRecorded = Integer.parseInt(index);

                    adapterUpcomingEvents.removeItem(indexRecorded);
                    adapterUpcomingEvents.notifyItemRemoved(indexRecorded);
                    // scheduleHomeBinding.recyclerview.setAdapter(adapterUpcomingEvents);
                    //   viewScheduleFragmentViewModel.loadUpcomingVaccinationEvents(month);



                } else if (vaccinationRecorded.equals("0")) {

                    //scheduleHomeBinding.recyclerview.setAdapter(adapterUpcomingEvents);
                    //scheduleHomeBinding.recyclerview.setAdapter(adapterUpcomingEvents);
                    //adapterUpcomingEvents.notifyDataSetChanged();

                    // viewScheduleFragmentViewModel.loadUpcomingVaccinationEvents(month);
                }
            }

        }


    }









    RecyclerView.LayoutManager mLayoutManager;

    int recycler_view_drawable = 0;


    /*
    public void ececyclerView(){
        if(theme_id == DEFAULT_THEME_ID) {
          recycler_view_drawable =  R.drawable.divider_horizontal;
        }else{
            recycler_view_drawable =  R.drawable.divider_horizontal_light;
        }


        boolean isTablet = getResources().getBoolean(R.bool.tablet);
        boolean isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);


        DividerItemDecoration decoration = new DividerItemDecoration(getContext(), VERTICAL);
       // Drawable verticalDivider = ContextCompat.getDrawable(getActivity(), recycler_view_drawable);
       // decoration.setDrawable(verticalDivider);
       // scheduleHomeBinding.recyclerview.addItemDecoration(decoration);


        //Animator for recycler view
        Interpolator overshoot  = new OvershootInterpolator(1f);
        Interpolator accelerateDecelerate  = new AccelerateDecelerateInterpolator();
        Interpolator bounce  = new BounceInterpolator();

        //setup the recycler view basic info
        SlideInLeftAnimator animator = new SlideInLeftAnimator(accelerateDecelerate);
        SlideInDownAnimator downAnimator = new SlideInDownAnimator(bounce);
       // scheduleHomeBinding.recyclerview.setItemAnimator(downAnimator);
       // scheduleHomeBinding.recyclerview.getItemAnimator().setAddDuration(5);
        //setting animation on scrolling

       // scheduleHomeBinding.recyclerview.setHasFixedSize(true); //enhance recycler view scroll
       // scheduleHomeBinding.recyclerview.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset_small_xtra);
      //  scheduleHomeBinding.recyclerview.addItemDecoration(itemDecoration);
        //  RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL, false);


        if(isTablet){

            if(isPotrait){

              mLayoutManager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
           // mLayoutManager  = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);


           // }//else {//potrait

              //  mLayoutManager  = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

            }

        }else {
            // if(!isPotrait){
           // mLayoutManager  = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

            mLayoutManager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
            // }

        }

       // scheduleHomeBinding.recyclerview.setLayoutManager(mLayoutManager);


    }
    */

    DividerItemDecoration decoration;
    public void setUpRecyclerView(){
        boolean isTablet = getResources().getBoolean(R.bool.tablet);
        boolean isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);

        decoration = new DividerItemDecoration(getContext(), VERTICAL);
        //  vaccinationScheduleBinding.recyclerview.addItemDecoration(decoration);

        //setup the recycler view basic info
        SlideInUpAnimator animator = new SlideInUpAnimator(new OvershootInterpolator(1f));
        scheduleHomeBinding.profile.recyclerview.setItemAnimator(animator);
        scheduleHomeBinding.profile.recyclerview.getItemAnimator().setAddDuration(500);
        //setting animation on scrolling

        // scheduleHomeBinding.recyclerview.setHasFixedSize(true); //enhance recycler view scroll
        // scheduleHomeBinding.recyclerview.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset_schedule);
        scheduleHomeBinding.profile.recyclerview.addItemDecoration(itemDecoration);
        //  RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL, false);



        if(isTablet){

            //if(!isPotrait){
            //mLayoutManager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL, false);
            mLayoutManager  = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

            // }

        }else {
            // if(!isPotrait){
            mLayoutManager  = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

            // }

        }

        scheduleHomeBinding.profile.recyclerview.setLayoutManager(mLayoutManager);


    }


    ArrayList<DashBoardSchedule> dashBoards = new ArrayList<>();

    ///////////LINE GRAPH //////////////////////////




    private void loadEventsReports(){

       viewScheduleFragmentViewModel.loadReports("OPV0");

    }




    ///// NOT IN USE ////////////////////////////

    private void observeGraphReports(){

        Observer<CoverageReport> coverageReportObserver = new Observer<CoverageReport>() {
            @Override
            public void onChanged(@Nullable CoverageReport coverageReport) {


                if(coverageReport!=null) {

                    /*
                    scheduleHomeBinding.cardViewTitle.setVisibility(View.VISIBLE);
                    scheduleHomeBinding.cardViewGraph.setVisibility(View.VISIBLE);
                    scheduleHomeBinding.cardViewDosageDue.setVisibility(View.VISIBLE);
                    scheduleHomeBinding.cardViewDosageMissed.setVisibility(View.VISIBLE);
                    scheduleHomeBinding.cardViewHomeWelcome.setVisibility(View.INVISIBLE);
                    scheduleHomeBinding.progressbarHome.setVisibility(View.INVISIBLE);
                    */


                    DashBoardHeader dashBoardHeader = new DashBoardHeader();
                    dashBoardHeader.setDoneVaccinations(coverageReport.getDoneVaccinations());
                    dashBoardHeader.setMissedVaccinations(coverageReport.getMissedVaccinations());
                    dashBoardHeader.setUpcomingVaccinationsCurrentMonth(coverageReport.getNoUpcomingEventsOfCurrentMonth());
                    dashBoardHeader.setMissedVaccinationsCurrentMonth(coverageReport.getNoMissedEventsOfCurrentMonth());
                    dashBoardHeader.setNextHospitalDate(DateCalendarConverter.stringToDateWithoutTime(coverageReport.getNextHospitalDate()));
                    dashBoardHeader.setUpcomingVaccinations(coverageReport.getUpcomingVaccinations());
                    //dashBoardHeader.setTotalChildrenImmunised();

                    setUpHomeScreenData(dashBoardHeader);

                   //  setUpLineChart(vaccineReports);
                    //////coverage //////////////////////////////////////////////////

                    int percent = coverageReport.getCoveragePercentage();

                    String percentOverall = Integer.toString(coverageReport.getCoveragePercentage()) + "%";
                    String vaccine_percentage = "" + percent + "%";
                   // String riseOrDropString = riseOrDrop+"%";

                    scheduleHomeBinding.tvTitleOverviewGraph.setText(percentOverall);
                    scheduleHomeBinding.tvTitleOverviewVaccine.setText(vaccine_percentage);

                }


            }
        };

        //declare viewmodel observation
        viewScheduleFragmentViewModel.getCoverageReportMutableLiveData().observe(this,coverageReportObserver);


    }
    private void observeEllapsedTime(){

        Observer<String> ellapsedObs = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                Toast.makeText(getContext(),"time: "+s+" secs", Toast.LENGTH_LONG).show();;

            }
        };
        //decalare observer
        viewScheduleFragmentViewModel.getEllapsedTime().observe(this,ellapsedObs);


    }


    private void observeProgress(){

        Observer<FragmentReportsViewModel.ReportProgress> progressObserver = new Observer<FragmentReportsViewModel.ReportProgress>() {
            @Override
            public void onChanged(@Nullable FragmentReportsViewModel.ReportProgress reportProgress) {

                if(reportProgress == FragmentReportsViewModel.ReportProgress.IN_PROGRESS){

                  //  scheduleHomeBinding.cardViewGraph.setVisibility(View.VISIBLE);
                   // scheduleHomeBinding.cardViewDosageDue.setVisibility(View.VISIBLE);
                  //  scheduleHomeBinding.cardViewDosageMissed.setVisibility(View.VISIBLE);
                  //  scheduleHomeBinding.cardViewHomeWelcome.setVisibility(View.INVISIBLE);


                    scheduleHomeBinding.progressbarHome.setVisibility(View.VISIBLE);

                }else if(reportProgress == FragmentReportsViewModel.ReportProgress.LOAD_SUCCESS){
                    scheduleHomeBinding.progressbarHome.setVisibility(View.INVISIBLE);
                    scheduleHomeBinding.cardViewHomeWelcome.setVisibility(View.INVISIBLE);

                }
                else if(reportProgress == FragmentReportsViewModel.ReportProgress.LOAD_FAIL){//failed

                    //setLineErrors();
                    scheduleHomeBinding.progressbarHome.setVisibility(View.INVISIBLE);

                  //  scheduleHomeBinding.cardViewTitle.setVisibility(View.INVISIBLE);
                  //  scheduleHomeBinding.cardViewGraph.setVisibility(View.INVISIBLE);
                   // scheduleHomeBinding.cardViewDosageDue.setVisibility(View.INVISIBLE);
                   // scheduleHomeBinding.cardViewDosageMissed.setVisibility(View.INVISIBLE);
                    scheduleHomeBinding.cardViewHomeWelcome.setVisibility(View.VISIBLE);

                }

            }
        };

        viewScheduleFragmentViewModel.getProgressMutableLiveDataLC().observe(this,progressObserver);

    }



    private void observeErrorNoData(){

        Observer<String> ellapsedObs = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {


                scheduleHomeBinding.progressbarHome.setVisibility(View.INVISIBLE);
                scheduleHomeBinding.cardViewTitle.setVisibility(View.INVISIBLE);
                scheduleHomeBinding.cardViewGraph.setVisibility(View.INVISIBLE);
              //  scheduleHomeBinding.cardViewDosageDue.setVisibility(View.INVISIBLE);
              //  scheduleHomeBinding.cardViewDosageMissed.setVisibility(View.INVISIBLE);

                scheduleHomeBinding.cardViewHomeWelcome.setVisibility(View.VISIBLE);


            }
        };
        //decalare observer
        viewScheduleFragmentViewModel.getError().observe(this,ellapsedObs);


    }








}
