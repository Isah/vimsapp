package com.vims.vimsapp.view.vaccinations.schedule;

public class DashBoardSchedule {


    private String monthOfVaccination;
    private String noOfVaccinations;
    private String dosagePercentage;
    private String typeOfVaccinationCategory;


    public String getDosagePercentage() {

        return dosagePercentage;
    }

    public void setDosagePercentage(String dosagePercentage) {
        this.dosagePercentage = dosagePercentage;
    }

    public String getTypeOfVaccinationCategory() {
        return typeOfVaccinationCategory;
    }

    public void setTypeOfVaccinationCategory(String typeOfVaccinationCategory) {
        this.typeOfVaccinationCategory = typeOfVaccinationCategory;
    }

    public String getMonthOfVaccination() {
        return monthOfVaccination;
    }

    public void setMonthOfVaccination(String monthOfVaccination) {
        this.monthOfVaccination = monthOfVaccination;
    }

    public String getNoOfVaccinations() {
        return noOfVaccinations;
    }

    public void setNoOfVaccinations(String noOfVaccinations) {
        this.noOfVaccinations = noOfVaccinations;
    }
}
