package com.vims.vimsapp.view.vaccinations.schedule;

import android.app.ActivityOptions;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.OvershootInterpolator;


import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.injection.ModuleRepository;
import com.vims.vimsapp.view.patients.detail.MyDetailsTypeConverter;
import com.vims.vimsapp.view.patients.list.ItemOffsetDecoration;
import com.vims.vimsapp.view.vaccinations.CardHeader;
import com.vims.vimsapp.view.vaccinations.viewmodel.ViewScheduleUpcomingFragmentViewModel;
import com.vims.vimsapp.view.vaccinations.viewmodel.ViewScheduleUpcomingFragmentViewModelFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import vimsapp.R;

import vimsapp.databinding.VaccinationFragmentScheduleDoneMissedBinding;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MissedScheduleActivity.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class MissedScheduleActivity extends AppCompatActivity {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    VaccinationFragmentScheduleDoneMissedBinding vaccinationScheduleBinding;

    ViewScheduleUpcomingFragmentViewModel viewScheduleFragmentViewModel;
    ViewScheduleUpcomingFragmentViewModelFactory scheduleUpcomingFragmentViewModelFactory;



    public MissedScheduleActivity() {
        // Required empty public constructor
    }



    // TODO: Rename and change types and number of parameters


    EventState eventState;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


       // vaccinationHomeBinding = DataBindingUtil.inflate(inflater, R.layout.vaccination_fragment_home, container, false);

        vaccinationScheduleBinding = DataBindingUtil.setContentView(this,R.layout.vaccination_fragment_schedule_done_missed);


       Toolbar toolbar = vaccinationScheduleBinding.toolbar;
       toolbar.setTitle("Overdue");
       setSupportActionBar(toolbar); //we replace our actionbar with our toolbar

        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
          //  actionBar.setTitle("Overdue");
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        scheduleUpcomingFragmentViewModelFactory = new ViewScheduleUpcomingFragmentViewModelFactory(Injection.provideReadChildSchedule(getApplication()), Injection.provideRecordVaccinationUseCase(getApplication()), Injection.provideGenerateReport(getApplication()), ModuleRepository.provideSearchVaccinationRepository(getApplication()),Injection.provideHospitalSchedule(getApplication()), getApplication() );
        viewScheduleFragmentViewModel = ViewModelProviders.of(this, scheduleUpcomingFragmentViewModelFactory).get(ViewScheduleUpcomingFragmentViewModel.class);


        setUpRecyclerView();
        //vaccinationScheduleBinding.setViewModel(viewScheduleFragmentViewModel);

        viewScheduleFragmentViewModel.loadMissedVaccinationEvents();

     //   vaccinationScheduleBinding.spinnerUpcomingEvents.setVisibility(View.INVISIBLE);




        observeDoneSchedule();
        observeProgressBar();
        observeMissedError();
        //  observeRecordedVaccinaton();

        setupDrawables();

        //item sinner sets the spinner text color
        /*
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.months_array, R.layout.item_spinner);
        adapter.setDropDownViewResource(R.layout.item_spinner_dropdown);
        vaccinationScheduleBinding.monthSpinner.setAdapter(adapter);
        */

        // setupViewPager(savedInstanceState);
        // setupTabIcons();


      //  spinnerListner();


        eventState = new EventState(getApplicationContext());


    }










    private void setupDrawables(){

        vaccinationScheduleBinding.tvErrorRegister.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_group_add_cs_16dp),null,null,null);
        vaccinationScheduleBinding.errorMsgTv.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
        vaccinationScheduleBinding.errorMsgTvTitle.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
        vaccinationScheduleBinding.tvErrorRegister.setTypeface(MyFonts.getTypeFace(getApplicationContext()));

    }



    @Override
    public void onResume() {
        super.onResume();

        if(getApplication()!=null) {
            if (adapterMissedEvents != null) {

                String vaccinationRecorded = eventState.getPref("recordedMissed");
                if (!TextUtils.isEmpty(vaccinationRecorded) && vaccinationRecorded.equals("1")) {

                    String index = eventState.getPref("recordedIndexMissed");
                    int indexRecorded = Integer.parseInt(index);



                    Log.d("MissedScheduleActivityR",""+indexRecorded);


                    eventState.putPref("recordedMissed", "0");//clear the index


                    adapterMissedEvents.removeItem(indexRecorded);
                    adapterMissedEvents.notifyItemRemoved(indexRecorded);
                    // scheduleHomeBinding.recyclerview.setAdapter(adapterUpcomingEvents);
                   // viewScheduleFragmentViewModel.loadMissedVaccinationEvents();



                }
            }

        }


    }




    RecyclerView.LayoutManager mLayoutManager;
    DividerItemDecoration decoration;
    public void setUpRecyclerView(){
        boolean isTablet = getResources().getBoolean(R.bool.tablet);
        boolean isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);


        // decoration = new DividerItemDecoration(getContext(), VERTICAL);
        // scheduleHomeBinding.recyclerview.addItemDecoration(decoration);
 

        //setup the recycler view basic info
        SlideInUpAnimator animator = new SlideInUpAnimator(new OvershootInterpolator(1f));
        vaccinationScheduleBinding.recyclerview.setItemAnimator(animator);
        vaccinationScheduleBinding.recyclerview.getItemAnimator().setAddDuration(500);
        //setting animation on scrolling

        // scheduleHomeBinding.recyclerview.setHasFixedSize(true); //enhance recycler view scroll
        // scheduleHomeBinding.recyclerview.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getApplicationContext(), R.dimen.item_offset_large);
        vaccinationScheduleBinding.recyclerview.addItemDecoration(itemDecoration);
        //  RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL, false);



        if(isTablet){

         //   if(!isPotrait){

                //    mLayoutManager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL, false);
                mLayoutManager  = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);

           // }


        }else {
            // if(!isPotrait){
            mLayoutManager  = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);

            // }

        }

        vaccinationScheduleBinding.recyclerview.setLayoutManager(mLayoutManager);


    }




    public class AdapterVaccinationEventListner implements AdapterMissedEvents.OnVaccinationCLickLIstner {

        @Override
        public void onVaccinationClicked(View view, int position, VaccinationEvent vaccinationEvent) {


            Bundle bundle = new Bundle();
            bundle.putString("vevent", MyDetailsTypeConverter.fromVaccinationEventToString(vaccinationEvent));
            bundle.putInt("position",position);

            Log.d("MissedScheduleActivityC",""+position);

            bundle.putString("state","missed");


            String transitionName  = getResources().getString(R.string.transitionNameEvent);
            Intent intent = new Intent(getApplicationContext(),ComfirmVaccinationActivity.class);
            intent.putExtras(bundle);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                //shared Element Transitions
                // ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, p1, p2, p3);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MissedScheduleActivity.this, view, transitionName);
                startActivity(intent, options.toBundle());
                //Exit Transitions
                //Bundle bundle = ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle();
                //context.startActivity(intent, bundle);

            } else {
                startActivity(intent);
            }


        }
    }


    private void setMonthOnDetailBackDrop(List<VaccinationEvent> vaccinationEvents){
        /*
        VaccinationEvent vaccinationEvent1 = vaccinationEvents.get(0);
        VaccinationEvent vaccinationEvent2 = vaccinationEvents.get(vaccinationEvents.size()-1);

        Date date1 = DateCalendarConverter.stringToDateWithoutTime(vaccinationEvent1.getScheduledDate());
        Date date2 = DateCalendarConverter.stringToDateWithoutTime(vaccinationEvent2.getScheduledDate());

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date1);
        int m1 =  calendar1.get(Calendar.MONTH);
        String m1s = DateCalendarConverter.monthIntToString(m1);
        String y1 = Integer.toString(calendar1.get(Calendar.YEAR));


        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(date2);
        int m2 = calendar1.get(Calendar.MONTH);
        String m2s = DateCalendarConverter.monthIntToString(m2);
        String y2 = Integer.toString(calendar1.get(Calendar.YEAR));


        String dateRange = "from "+m1s+" "+y1;
        final CollapsingToolbarLayout collapsingToolbarLayout = vaccinationScheduleBinding.collapsingToolbar;
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setTitleEnabled(false);

        //collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout.setTitle(dateRange);



       // vaccinationScheduleBinding.missedVaccinationsBtn.setVisibility(View.INVISIBLE);
       // vaccinationScheduleBinding.materialupTitleContainer.setVisibility(View.GONE);



          */
    }


    AdapterMissedEvents adapterMissedEvents;

    public void updateDoneEvents(List<VaccinationEvent> duplicateVaccinationEvents){

        if(adapterMissedEvents !=null){
            adapterMissedEvents = null;
        }

        List<VaccinationEvent>  unDuplicatesList = new ArrayList<>(new LinkedHashSet<>(duplicateVaccinationEvents));


        setMonthOnDetailBackDrop(unDuplicatesList);



        //VaccinationStatistic vstat =  viewScheduleFragmentViewModel.getStatistic(vaccinationEvents.size());
        /// String cardTitle = "vaccinations:"+vstat.getTotalNoOfFinishedVaccinations()+"/"+vstat.getTotalNoOfVaccinations();
        String date =  DateCalendarConverter.dateToStringWithoutTimeAndDay(new Date());

        String card = "";
        String cardSubTitle = date;

        CardHeader cardHeader = new CardHeader();
        cardHeader.setTitle(date);
        cardHeader.setSubTitle(card);
        //cardHeader.setImmunisationCoveragePercentage(vstat.getTotalVaccinationPercentage());

        adapterMissedEvents = AdapterMissedEvents.getINSTANCE(unDuplicatesList, getApplication(), Injection.provideRecordVaccinationUseCase(getApplication()));
        adapterMissedEvents.setActivity(getParent());
        adapterMissedEvents.setClickListener(new AdapterVaccinationEventListner());

 
        //adapterVaccinationEvents.setHasStableIds(true);ooncomingEvents.notifyItemInserted(0);
        int prevSize = unDuplicatesList.size();
        adapterMissedEvents.notifyItemRangeInserted(prevSize, unDuplicatesList.size() -prevSize);

        vaccinationScheduleBinding.recyclerview.setNestedScrollingEnabled(false);

        vaccinationScheduleBinding.recyclerview.postDelayed(new Runnable() {
            @Override
            public void run() {
                vaccinationScheduleBinding.recyclerview.setAdapter(adapterMissedEvents);

            }
        },100);


    }




    ArrayList<VaccinationEvent> monthVaccinationEvents = new ArrayList<>();

    private void observeDoneSchedule(){

        Observer<List<VaccinationEvent>> vcardsObserver = new Observer<List<VaccinationEvent>>() {
            @Override
            public void onChanged(@Nullable List<VaccinationEvent> vaccinationEvents) {

                if(vaccinationEvents != null) {

                    vaccinationScheduleBinding.progressbarUpcomingEvents.setVisibility(View.INVISIBLE);
                    vaccinationScheduleBinding.recyclerview.setVisibility(View.VISIBLE);
                    vaccinationScheduleBinding.layoutError.setVisibility(View.INVISIBLE);


                    if(vaccinationEvents.size()>0) {
                        Collections.sort(vaccinationEvents);

                        updateDoneEvents(vaccinationEvents);
                    }


                }else{


                }

            }
        };

        viewScheduleFragmentViewModel.getmMissedVaccinationEvents().observe(this, vcardsObserver);


    }






/*
    private void spinnerListner(){

        vaccinationScheduleBinding.monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                String monthSelected = parent.getSelectedItem().toString();

                // August 2, 2018

                ArrayList<VaccinationEvent> monthEvents = new ArrayList<>();

                for(VaccinationEvent vaccinationEvent: monthVaccinationEvents){

                    String mdy = vaccinationEvent.getScheduledDate();
                    String[] spliter = mdy.split(" ");

                    String eventMonth = spliter[0];

                    Log.d("SPINNER","EventMonth "+eventMonth+" Selected"+monthSelected);

                    if(monthSelected.equals(eventMonth)){

                     monthEvents.add(vaccinationEvent);

                    }

                }


                updateDoneEvents(monthEvents);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    */


    private void observeMissedError(){
        Observer<String> missedObs = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

             //   Toast.makeText(getApplicationContext(),""+s,Toast.LENGTH_LONG).show();
                vaccinationScheduleBinding.layoutError.setVisibility(View.VISIBLE);


            }
        };

        viewScheduleFragmentViewModel.getMissedError().observe(this,missedObs);


    }



    public void observeProgressBar(){

        Observer<ViewScheduleUpcomingFragmentViewModel.ProgressStatus> progressObserver = new Observer<ViewScheduleUpcomingFragmentViewModel.ProgressStatus>() {
            @Override
            public void onChanged(@Nullable ViewScheduleUpcomingFragmentViewModel.ProgressStatus progressStatus) {

                if(progressStatus!=null) {
                    if (progressStatus == ViewScheduleUpcomingFragmentViewModel.ProgressStatus.LOADING) {

                        vaccinationScheduleBinding.progressbarUpcomingEvents.setVisibility(View.VISIBLE);



                    }else if(progressStatus == ViewScheduleUpcomingFragmentViewModel.ProgressStatus.FAILED){

                        vaccinationScheduleBinding.progressbarUpcomingEvents.setVisibility(View.INVISIBLE);
                    //    vaccinationScheduleBinding.layoutError.setVisibility(View.VISIBLE);




                    }else if(progressStatus == ViewScheduleUpcomingFragmentViewModel.ProgressStatus.SUCCESS){

                        vaccinationScheduleBinding.progressbarUpcomingEvents.setVisibility(View.INVISIBLE);
                     //   vaccinationScheduleBinding.layoutError.setVisibility(View.INVISIBLE);



                    }
                }

            }
        };

        viewScheduleFragmentViewModel.getMissedLoadProgress().observe(this,progressObserver);

    }








    ///these icons go on tabs
    private int[] tabIcons = {
        //    R.drawable.ic_battery_full_black_24dp,
         //   R.drawable.ic_battery_20_black_24dp,
          //  R.drawable.ic_battery_alert_black_24dp
    };



    /*
    //setup tab icons
    private void setupTabIcons() {

        TabLayout.Tab tab1 = vaccinationScheduleBinding.tabsCaretaker.getTabAt(0);
        if(tab1!=null) {
            tab1.setIcon(tabIcons[0]);
        }

        TabLayout.Tab tab2 = vaccinationScheduleBinding.tabsCaretaker.getTabAt(1);
        if(tab2!=null) {
            tab2.setIcon(tabIcons[1]);
        }

        TabLayout.Tab tab3 = vaccinationScheduleBinding.tabsCaretaker.getTabAt(2);
        if(tab3!=null) {
            tab3.setIcon(tabIcons[2]);
        }


    }
     */

    /*
    //set up the fragments on the viewpager
    private void setupViewPager(Bundle bundle) {


        //setup the viewpager--- it contains the fragments to be swiped
        ViewPager viewPager =  vaccinationScheduleBinding.viewpagerHome;
        //assign viewpager to tablayout
        vaccinationScheduleBinding.tabsCaretaker.setupWithViewPager(viewPager);



        AdapterViewPager adapter = new AdapterViewPager(getSupportFragmentManager());//child


       // adapter.addFragment(FragmentUpcomingSchedule.newInstance("due"), "Overview");
        adapter.addFragment(FragmentDoneSchedule.newInstance("done"), "Completed");
        adapter.addFragment(FragmentMissedSchedule.newInstance("overdue"), "Overdue");



        viewPager.setAdapter(adapter);



    }

      */

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    /**
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    **/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
