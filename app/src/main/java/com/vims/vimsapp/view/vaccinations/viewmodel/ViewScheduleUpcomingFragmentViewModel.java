package com.vims.vimsapp.view.vaccinations.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;
import android.util.Log;

import com.vims.vimsapp.UseCase;

import com.vims.vimsapp.entities.vaccinations.VaccinationCard;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationLocationState;
import com.vims.vimsapp.entities.reports.VaccinationStatistic;
import com.vims.vimsapp.managehospitalschedule.GenerateHospitalSchedule;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsDataSource;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsRepository;
import com.vims.vimsapp.viewcoverage.CoverageReport;
import com.vims.vimsapp.viewcoverage.GenerateReport;

import com.vims.vimsapp.searchvaccinations.SearchVaccinationsSchedule;
import com.vims.vimsapp.recordvaccinations.RecordChildVaccination;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.view.reports.FragmentReportsViewModel;
import com.vims.vimsapp.utilities.PreferenceHandler;

import java.util.Date;
import java.util.List;

/**
 * Created by root on 5/30/18.
 */

public class ViewScheduleUpcomingFragmentViewModel extends ViewModel{


     private  MutableLiveData<List<VaccinationEvent>>  mCalendarDayVaccinationEvents;
     private  MutableLiveData<List<VaccinationCard>>  mVaccinationCardsAll;
     private  MutableLiveData<String>  errorMessage;



     private SearchVaccinationsSchedule readChildSchedule;
     private  RecordChildVaccination recordChildVaccination;
     private  GenerateReport generateReport;
     private SearchVaccinationsRepository vaccinationsRepository;
     private Application mApplication;
     private PreferenceHandler preferenceHandler;



    public ViewScheduleUpcomingFragmentViewModel(SearchVaccinationsSchedule readChildSchedule, RecordChildVaccination recordChildVaccination, GenerateReport generateReport, SearchVaccinationsRepository vaccinationsRepository, GenerateHospitalSchedule generateHospitalSchedule, Application application) {
        this.readChildSchedule = readChildSchedule;
        this.recordChildVaccination = recordChildVaccination;
        this.generateReport = generateReport;
        this.vaccinationsRepository = vaccinationsRepository;
        this.generateHospitalSchedule = generateHospitalSchedule;
        this.mApplication = application;

        mCalendarDayVaccinationEvents = new MutableLiveData<>();
        mVaccinationCardsAll = new MutableLiveData<>();

        errorMessage = new MutableLiveData<>();

        preferenceHandler = PreferenceHandler.getInstance(application);
      //  loadUpcomingVaccinationEvents();

    }



    public LiveData<String> getErrorMessage() {
        return errorMessage;
    }
    public void setErrorMessage(String msg){
        if(errorMessage==null){
            errorMessage = new MutableLiveData<>();
        }

         errorMessage.postValue(msg);
    }



    public LiveData<List<VaccinationEvent>> getCalendarDayVaccinationEvents() {
        if(mCalendarDayVaccinationEvents == null){
            mCalendarDayVaccinationEvents = new MutableLiveData<>();

        }
        return mCalendarDayVaccinationEvents;
    }







    private MutableLiveData<VaccinationStatistic> totalVaccinationsStatistic;
    private MutableLiveData<String> mVaccinationPercentage;






    public MutableLiveData<VaccinationStatistic> getTotalVaccinationsStatistic() {
        if(totalVaccinationsStatistic == null){
            totalVaccinationsStatistic = new MutableLiveData<>();
        }



        return totalVaccinationsStatistic;
    }

    public void setTotalVaccinationsStatistic(VaccinationStatistic vaccinationsStatistic) {

        this.totalVaccinationsStatistic.postValue(vaccinationsStatistic);

        }


    ///////// Total Vaccinations ////////////////////////////////////

    private MutableLiveData<Integer> mTotalVaccinations;

    private MutableLiveData<Integer> getTotalVaccinations() {


        return mTotalVaccinations;
    }

    private void setTotalVaccinations(int totalVaccinations) {
        if(mTotalVaccinations==null){

            mTotalVaccinations = new MutableLiveData<>();
        }
        Log.d("SUVM", "TotalVacs Set: "+totalVaccinations);
        this.mTotalVaccinations.postValue(totalVaccinations);
    }

    //////////////////////////////////////////////////////////




    int finishedVaccinations =0;









    ///Vaccinations /////////////////////////////////////////////


    public MutableLiveData<String> getRecordedVaccinationError() {
        return recordedVaccinationError;
    }

    private MutableLiveData<Integer> recordedVaccinationIndex;
    private MutableLiveData<String> recordedVaccinationError;

    private MutableLiveData<String> recordResponse;

    public MutableLiveData<String> getRecordResponse() {
        if(recordResponse == null){
            recordResponse = new MutableLiveData<>();

        }
        return recordResponse;
    }

    public void setRecordResponse(String record_response) {

        this.recordResponse.postValue(record_response);
    }

    public void recordChildVaccination(VaccinationLocationState locationState, VaccinationEvent vaccinationEvent){

        RecordChildVaccination.RequestValues requestValues = new RecordChildVaccination.RequestValues(locationState,vaccinationEvent);

        recordChildVaccination.execute(requestValues, new UseCase.OutputBoundary<RecordChildVaccination.ResponseValue>() {
            @Override
            public void onSuccess(RecordChildVaccination.ResponseValue result) {

                setRecordResponse("recorded dosage of "+result.getmResponse().getVaccineName() +" to "+result.getmResponse().getChildName());
            // recordedVaccinationIndex.postValue(position);

                Log.d("UpcomingD",result.getmResponse().getChildName());


            }

            @Override
            public void onError(String errorMessage) {

                if(recordedVaccinationError == null){

                    recordedVaccinationError = new MutableLiveData<>();
                }

                recordedVaccinationError.postValue(errorMessage);

            }
        });


    }





    private MutableLiveData<Boolean> isNotifyScheduledDate;

    public LiveData<Boolean> getIsNotifyScheduledDate() {
        if(isNotifyScheduledDate == null){
            isNotifyScheduledDate = new MutableLiveData<>();
        }
        return isNotifyScheduledDate;
    }

    public void setIsNotifyScheduledDate(boolean isNotify) {
        this.isNotifyScheduledDate.postValue(isNotify);
    }







    private MediatorLiveData<List<VaccinationEvent>> mediatorLiveData;

    public MediatorLiveData<List<VaccinationEvent>> upcomingVaccinations() {
        if(upComingVaccinations == null){
            upComingVaccinations = new MutableLiveData<>();
        }else {

            upComingVaccinations = new MutableLiveData<>();
        }

        if(mediatorLiveData ==null){
            mediatorLiveData=new MediatorLiveData<>();
        }else {
            upComingVaccinations = new MutableLiveData<>();

        }

        mediatorLiveData.addSource(upComingVaccinations, new Observer<List<VaccinationEvent>>() {
            @Override
            public void onChanged(@Nullable List<VaccinationEvent> vaccinationEventList) {
                mediatorLiveData.setValue(vaccinationEventList);
            }
        });
        return mediatorLiveData;

    }



    private MutableLiveData<List<VaccinationEvent>> upComingVaccinations;

    //this will take Big O(N)
    public void loadUpcomingVaccinationEvents(String month, int year, SearchVaccinationsSchedule.VaccinationsRequestType vaccinationsRequestType){

        if(upComingVaccinations == null){
            upComingVaccinations = new MutableLiveData<>();
        }
        setLoadProgress(ProgressStatus.LOADING);

        SearchVaccinationsSchedule.RequestValues requestValues = new SearchVaccinationsSchedule.RequestValues(vaccinationsRequestType);
        requestValues.setMonth(month);
        requestValues.setYear(year);
        //RequestValues.set

        readChildSchedule.execute(requestValues, new UseCase.OutputBoundary<SearchVaccinationsSchedule.ResponseValue>() {
            @Override
            public void onSuccess(SearchVaccinationsSchedule.ResponseValue result) {

                Log.d("SearchSchedule","?: "+month);

                setLoadProgress(ProgressStatus.SUCCESS);
                upComingVaccinations.postValue(result.getVaccinationEvents());

            }

            @Override
            public void onError(String theErrorMessage) {

                setErrorMessage(theErrorMessage);
                //errorMessage.postValue(theErrorMessage);//
                setLoadProgress(ProgressStatus.FAILED);


            }
        });




    }






    private MutableLiveData<List<VaccinationEvent>> mDoneVaccinationEvents;

    public MutableLiveData<List<VaccinationEvent>> getmDoneVaccinationEvents() {
        return mDoneVaccinationEvents;
    }

    public void loadDoneVaccinationEvents(){
        if(mDoneVaccinationEvents == null){
            mDoneVaccinationEvents = new MutableLiveData<>();
        }

      //  mDoneVaccinationEvents.postValue(null);
       // setLoadProgress(ProgressStatus.LOADING);

        SearchVaccinationsSchedule.RequestValues requestValues = new SearchVaccinationsSchedule.RequestValues(SearchVaccinationsSchedule.VaccinationsRequestType.DONE);

        readChildSchedule.execute(requestValues, new UseCase.OutputBoundary<SearchVaccinationsSchedule.ResponseValue>() {
            @Override
            public void onSuccess(SearchVaccinationsSchedule.ResponseValue result) {


                setLoadProgress(ProgressStatus.SUCCESS);

                mDoneVaccinationEvents.postValue(result.getVaccinationEvents());

            }

            @Override
            public void onError(String theErrorMessage) {

                errorMessage.postValue(theErrorMessage);
                setLoadProgress(ProgressStatus.FAILED);


            }
        });




    }







    //////////////////MISSED ///////////////////////////////////////////////

    private MutableLiveData<String> errorMissed;
    public LiveData<String> getMissedError() {
        if(errorMissed==null){
            errorMissed = new MutableLiveData<>();
        }
        return errorMissed;
    }

    public void setMissedError(String error) {
        if(errorMissed==null){
            errorMissed = new MutableLiveData<>();
        }
        this.errorMissed.postValue(error);
    }



    private MutableLiveData<List<VaccinationEvent>> mMissedVaccinationEvents;

    public MutableLiveData<List<VaccinationEvent>> getmMissedVaccinationEvents() {
        return mMissedVaccinationEvents;
    }

    public void loadMissedVaccinationEvents(){
        if(mMissedVaccinationEvents == null){
            mMissedVaccinationEvents = new MutableLiveData<>();
        }

        //  mDoneVaccinationEvents.postValue(null);
         setMissedLoadProgress(ProgressStatus.LOADING);

        SearchVaccinationsSchedule.RequestValues requestValues = new SearchVaccinationsSchedule.RequestValues(SearchVaccinationsSchedule.VaccinationsRequestType.OVERDUE);
        int m = DateCalendarConverter.dateToIntGetMonth(new Date());
        String month =DateCalendarConverter.monthIntToString(m);
        requestValues.setMonth(month);


        readChildSchedule.execute(requestValues, new UseCase.OutputBoundary<SearchVaccinationsSchedule.ResponseValue>() {
            @Override
            public void onSuccess(SearchVaccinationsSchedule.ResponseValue result) {

              //  setTotalVaccinations(result.getTotalVaccinations());
                //setNextVaccinationDate(result.getNextVaccinationDate());
                  setMissedLoadProgress(ProgressStatus.SUCCESS);

                Log.d("DateTodayEvents", "date size: "+result.getVaccinationEvents().size());

                mMissedVaccinationEvents.postValue(result.getVaccinationEvents());

            }

            @Override
            public void onError(String theErrorMessage) {

                Log.d("DateTodayEvents", "date size: 0 "+theErrorMessage);


                //errorMessage.postValue(theErrorMessage);
                setMissedLoadProgress(ProgressStatus.FAILED);
                setMissedError(theErrorMessage);


            }
        });




    }





    private MutableLiveData<ProgressStatus> mIsMissedStartProgress;

    public MutableLiveData<ProgressStatus> getMissedLoadProgress() {
        if(mIsMissedStartProgress == null){
            mIsMissedStartProgress = new MutableLiveData<>();
        }

        return mIsMissedStartProgress;
    }

    public void setMissedLoadProgress(ProgressStatus isStartProgress) {
        if(mIsMissedStartProgress == null){
            mIsMissedStartProgress = new MutableLiveData<>();
        }
        this.mIsMissedStartProgress.postValue(isStartProgress);
    }







    public enum ProgressStatus{
        LOADING,SUCCESS,FAILED

    }








    private MutableLiveData<ProgressStatus> mIsStartProgress;

    public MutableLiveData<ProgressStatus> getLoadProgress() {
        if(mIsStartProgress == null){
            mIsStartProgress = new MutableLiveData<>();
        }

        return mIsStartProgress;
    }

    public void setLoadProgress(ProgressStatus isStartProgress) {
        if(mIsStartProgress == null){
            mIsStartProgress = new MutableLiveData<>();
        }
        this.mIsStartProgress.postValue(isStartProgress);
    }







    ///LINE CHART REPORT /////////////

    private MutableLiveData<Boolean> isDataChanged;
    private MutableLiveData<CoverageReport> coverageReportMutableLiveData;

    public LiveData<CoverageReport> getCoverageReportMutableLiveData() {
        if(coverageReportMutableLiveData==null){
            coverageReportMutableLiveData = new MutableLiveData<>();
        }
        return coverageReportMutableLiveData;
    }

    public void setCoverageReportMutableLiveData(CoverageReport coverageReport) {
        coverageReportMutableLiveData.postValue(coverageReport);
    }

    private MutableLiveData<String> errorMutableLC;
    public LiveData<String> getError() {
        if(errorMutableLC==null){
            errorMutableLC = new MutableLiveData<>();
        }
        return errorMutableLC;
    }

    public void setError(String error) {
        if(errorMutableLC==null){
            errorMutableLC = new MutableLiveData<>();
        }
        this.errorMutableLC.postValue(error);
    }



    private Integer childrenCardSizeCompare;




    private int childrenCardSize = 0;

    public void loadReports(String vaccine){
        if(coverageReportMutableLiveData==null){
            coverageReportMutableLiveData = new MutableLiveData<>();
            childrenCardSizeCompare = 0;
        }
        if(isDataChanged==null){
            isDataChanged = new MutableLiveData<>();
        }

        Log.d("CardSize","Start");

        vaccinationsRepository.checkChildCardsSize(new SearchVaccinationsDataSource.LoadChildCardsSizeCallback() {
            @Override
            public void onCardsSizeLoaded(int size) {

                childrenCardSize = size; //5

                int x = preferenceHandler.getIntValue("size");

                Log.d("CardSize","Size: "+childrenCardSize+" CompareSize: "+x);

                if (childrenCardSize > x) { //5 > 4

                    //means data has changed
                    preferenceHandler.setIntValue("size",size);

                    generateReports(true, vaccine);

                } else {//less or equal

                    generateReports(false, vaccine);

                }

            }



            @Override
            public void onCardNotAvailable() {

                setError("Cards UnAvailable");

            }

        });


    }



    private MutableLiveData<String> mEllapsedTime;

    public LiveData<String> getEllapsedTime() {
        if(mEllapsedTime == null){
            mEllapsedTime = new MutableLiveData<>();
        }
        return mEllapsedTime;
    }
    private void setEllapsedTime(String ellapsedTime) {
        if(mEllapsedTime == null){
            mEllapsedTime = new MutableLiveData<>();
        }
        mEllapsedTime.postValue(ellapsedTime);
    }




    private MutableLiveData<FragmentReportsViewModel.ReportProgress> progressMutableLiveData;

    public LiveData<FragmentReportsViewModel.ReportProgress> getProgressMutableLiveDataLC() {
        if(progressMutableLiveData == null){
            progressMutableLiveData = new MutableLiveData<>();
        }
        return progressMutableLiveData;
    }

    public void setProgressMutableLiveDataLC(FragmentReportsViewModel.ReportProgress progress) {
        if(progressMutableLiveData == null){
            progressMutableLiveData = new MutableLiveData<>();
        }
        this.progressMutableLiveData.postValue(progress);
    }


    private void generateReports(boolean isDataChanged, String vaccineName){
        long startTime = System.currentTimeMillis();

        setProgressMutableLiveDataLC(FragmentReportsViewModel.ReportProgress.IN_PROGRESS);

        int yearNow = Integer.parseInt(DateCalendarConverter.dateToStringGetYear(new Date()));
        String searchDate = DateCalendarConverter.dateToStringWithoutTimeAndDay(new Date());
        GenerateReport.ReportType reportType = GenerateReport.ReportType.VACCINE_SPECIFIC_HOME;

        GenerateReport.RequestValues requestValues = new GenerateReport.RequestValues(reportType,searchDate);
        requestValues.setTypeOfVaccine(vaccineName);
        requestValues.setYear(yearNow);


        generateReport.execute(requestValues, new UseCase.OutputBoundary<GenerateReport.ResponseValue>() {
            @Override
            public void onSuccess(GenerateReport.ResponseValue result) {

                long endTime = System.currentTimeMillis();
                long ellapsedTime = endTime - startTime;

                setEllapsedTime(Long.toString(ellapsedTime));

                if(!isDataChanged) {
                  //  Log.d("GRNextDateCh", "total: " + result.getCoverageReport().getNextImmunisationDate());
                }

                CoverageReport coverageReport = result.getCoverageReport();
                setCoverageReportMutableLiveData(coverageReport);
                setProgressMutableLiveDataLC(FragmentReportsViewModel.ReportProgress.LOAD_SUCCESS);

            }

            @Override
            public void onError(String errorMessage) {

                setProgressMutableLiveDataLC(FragmentReportsViewModel.ReportProgress.LOAD_FAIL);

            }
        });


    }






    /////////////////// SCHEDULE ////////////////////////////////////////



    private MutableLiveData<String> hospitalScheduleLoadFeedback;
    private GenerateHospitalSchedule generateHospitalSchedule;


    public MutableLiveData<String> getHospitalScheduleLoadFeedback() {
        if(hospitalScheduleLoadFeedback==null){
            hospitalScheduleLoadFeedback = new MutableLiveData<>();
        }

        return hospitalScheduleLoadFeedback;
    }

    public void loadHospitalSchedule(boolean isLoadOnline, String hospitalName){

        if(hospitalScheduleLoadFeedback==null){
            hospitalScheduleLoadFeedback = new MutableLiveData<>();
        }

        GenerateHospitalSchedule.RequestType requestType;

        if(!isLoadOnline){//offline request you create a new schedule
            requestType = GenerateHospitalSchedule.RequestType.CREATE_SCHEDULE;
        }else{
            requestType = GenerateHospitalSchedule.RequestType.LOAD_SCHEDULE;
        }


        GenerateHospitalSchedule.RequestValues requestValues = new GenerateHospitalSchedule.RequestValues(isLoadOnline,hospitalName,requestType);

        generateHospitalSchedule.execute(requestValues, new UseCase.OutputBoundary<GenerateHospitalSchedule.ResponseValue>() {
            @Override
            public void onSuccess(GenerateHospitalSchedule.ResponseValue result) {

                hospitalScheduleLoadFeedback.postValue("Successfully created! ");

            }

            @Override
            public void onError(String errorMessage) {

                hospitalScheduleLoadFeedback.postValue(errorMessage);

            }
        });



    }






}



