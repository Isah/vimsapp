package com.vims.vimsapp.view.reports;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v13.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.itextpdf.text.DocumentException;
import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.reports.MonthTargetPopulation;
import com.vims.vimsapp.entities.reports.VaccinationEventsPack;
import com.vims.vimsapp.entities.vaccinations.VaccineSpecification;
import com.vims.vimsapp.searchvaccinations.GenerateVaccinationStats;
import com.vims.vimsapp.utilities.MainThreadExecutor;
import com.vims.vimsapp.utilities.pdf.MyPdfCreator;
import com.vims.vimsapp.utilities.pdf.MyReportPdfCreator;
import com.vims.vimsapp.view.injection.ModuleRepository;
import com.vims.vimsapp.view.patients.detail.MyDetailsTypeConverter;
import com.vims.vimsapp.view.patients.detail.ViewChildDetailsActivity;
import com.vims.vimsapp.viewcoverage.CoverageReport;
import com.vims.vimsapp.viewcoverage.CoverageTimeLine;
import com.vims.vimsapp.viewcoverage.GenerateReport;
import com.vims.vimsapp.viewcoverage.ReadVaccines;
import com.vims.vimsapp.viewcoverage.VaccineReport;
import com.vims.vimsapp.viewcoverage.VaccineReportsCard;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.ThemeManager;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.utilities.PreferenceHandler;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import vimsapp.BuildConfig;
import vimsapp.R;
import vimsapp.databinding.ReportsFragmentBinding;


import static com.vims.vimsapp.utilities.ThemeManager.bar_color1;
import static com.vims.vimsapp.utilities.ThemeManager.bar_color2;
import static com.vims.vimsapp.utilities.ThemeManager.drawale_gradient;
import static com.vims.vimsapp.utilities.ThemeManager.xaxis_grid_color;
import static com.vims.vimsapp.utilities.ThemeManager.xaxis_label_text_color;
import static com.vims.vimsapp.utilities.ThemeManager.xaxis_value_text_color;
import static com.vims.vimsapp.utilities.ThemeManager.yaxis1_grid_Color;
import static com.vims.vimsapp.utilities.ThemeManager.yaxis1_scale_text_Color;
import static com.vims.vimsapp.utilities.ThemeManager.yaxis2_grid_Color;
import static com.vims.vimsapp.utilities.ThemeManager.yaxis2_scale_text_Color;
import static com.vims.vimsapp.utilities.animations.AnimatorManager.animateTextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentReportsGlobalCoverage.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentReportsGlobalCoverage#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentReportsGlobalCoverage extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    ReportsFragmentBinding reportsFragmentBinding;



    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentReportsGlobalCoverage() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentReports.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentReportsGlobalCoverage newInstance(String param1, String param2) {
        FragmentReportsGlobalCoverage fragment = new FragmentReportsGlobalCoverage();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    FragmentReportsViewModelFactory fragmentReportsViewModelFactory;
    FragmentReportsViewModel fragmentReportsViewModel;



    //SWIPE AND TOUCH EVENTS
    SwipeRefreshLayout swipeRefreshLayout;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        reportsFragmentBinding = DataBindingUtil.inflate(inflater,R.layout.reports_fragment,container,false);

        fragmentReportsViewModelFactory = new FragmentReportsViewModelFactory(Injection.provideGenerateReport(getActivity().getApplication()), ModuleRepository.provideViewChildRepository(getActivity().getApplication()),getActivity().getApplication());
        fragmentReportsViewModel=  ViewModelProviders.of(this, fragmentReportsViewModelFactory).get(FragmentReportsViewModel.class);


        ThemeManager.setUpGraphColorTheme(ThemeManager.LIGHT_THEME_ID_BLUE);


        //reportsFragmentBinding.headerGraph.tvtitleEvents.setTypeface(MyFonts.getBoldTypeFace(getContext()));
        //reportsFragmentBinding.headerGraph.tvSubtitleEvents.setTypeface(MyFonts.getTypeFace(getContext()));


        //((AppCompatActivity)getActivity()).getSupportActionBar().setLogo(R.drawable.notice_48);
        final CollapsingToolbarLayout collapsingToolbarLayout = reportsFragmentBinding.collapsingToolbar;
        collapsingToolbarLayout.setTitleEnabled(false);


        Toolbar toolbar = reportsFragmentBinding.toolbar;
        SpannableStringBuilder str = new SpannableStringBuilder("STATS");
        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        toolbar.setTitle(str);

       // boolean isTab = getResources().getBoolean(R.bool.tablet);
          toolbar.setTitleMarginStart(getResources().getInteger(R.integer.fragment_title_margin));


        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar); //we replace our actionbar with our toolbar
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if(actionBar!=null) {
          //  actionBar.setTitle("Reports");
        }



        //item sinner sets the spinner text color
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getContext(), R.array.graph_array, R.layout.item_spinner);
        adapter.setDropDownViewResource(R.layout.item_spinner_dropdown);
       // reportsFragmentBinding.graphSpinner.setAdapter(adapter);

        reportsFragmentBinding.graphSpinnerVaccine.setBackground(getResources().getDrawable(ThemeManager.getSpinnerDrawable()));

        fonts();



        observeGraphReports();
        observeEllapsedTime();
        observeProgress();
        observeErrors();

        setupDrawables();

        loadRefresh();

        setLegendTextSize();

        shouldRefresh = false;
        setUpYearSpinner(shouldRefresh);
        setupGraphSlidePager();


        loadReportPdf();

        reportsFragmentBinding.sizeOfChildren.setTypeface(MyFonts.getTypeFace(getContext()));
       reportsFragmentBinding.tvGraphTitleVaccine.setTypeface(MyFonts.getTypeFace(getContext()));
       reportsFragmentBinding.tvGraphTitle.setTypeface(MyFonts.getTypeFace(getContext()));
        return reportsFragmentBinding.getRoot();

    }

    @Override
    public void onResume() {
        super.onResume();
       // observeGraphReports();
        //reportsFragmentBinding.swiperefresh.setRefreshing(true);

    }


    boolean shouldRefresh = false;

    public void loadRefresh() {
        boolean isTablet = getResources().getBoolean(R.bool.tablet);

        reportsFragmentBinding.swiperefresh.setEnabled(false);


        if (!isTablet){
          //  reportsFragmentBinding.nestedScroll.setNestedScrollingEnabled(false);
          }

        /*

        reportsFragmentBinding.nestedScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {



                if (v.isInTouchMode()) {
                    reportsFragmentBinding.swiperefresh.setEnabled(true);

                }else{
                    reportsFragmentBinding.swiperefresh.setEnabled(false);

                }




            }
        });
        */


        reportsFragmentBinding.swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
               // shouldRefresh = true;
               // setUpYearSpinner(shouldRefresh);

            }
        });
    }










    private void setupDrawables(){

   //        reportsFragmentBinding.reportsNoDataViewTitle.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_show_chart_black_24dp),null,null,null);

    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /**
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }




    private void setUpCircularProgressDrawableCoverage(int coverage){

        int  DEFAULT_THEME_ID = R.style.AppTheme_Dark;
        int  LIGHT_THEME_ID = R.style.AppTheme_Light;

        int high_coverage = 80;
        int low_coverage = 40;

        PreferenceHandler preferenceHandler = PreferenceHandler.getInstance(getContext());
        int  theme_id = preferenceHandler.getIntValue("THEME_ID");


        if(theme_id == DEFAULT_THEME_ID){

            if(coverage > high_coverage){

                reportsFragmentBinding.headerStatsGlobal.circularProgressbar.setProgressDrawable(getContext().getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_dark_high));

            }

            else if(coverage < low_coverage){

                reportsFragmentBinding.headerStatsGlobal.circularProgressbar.setProgressDrawable(getContext().getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_dark_low));

            }

            else {
                reportsFragmentBinding.headerStatsGlobal.circularProgressbar.setProgressDrawable(getContext().getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_dark_meduim));

            }


        }else{

            if(coverage > high_coverage){

                reportsFragmentBinding.headerStatsGlobal.circularProgressbar.setProgressDrawable(getContext().getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_light_high));

            }

            else if(coverage < low_coverage){

                reportsFragmentBinding.headerStatsGlobal.circularProgressbar.setProgressDrawable(getContext().getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_light_low));

            }

            else {
                reportsFragmentBinding.headerStatsGlobal.circularProgressbar.setProgressDrawable(getContext().getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_light_meduim));

            }



        }








    }

    private void setUpCircularProgressDrawableDropout(int dropout){


        int  DEFAULT_THEME_ID = R.style.AppTheme_Dark;
        int  LIGHT_THEME_ID = R.style.AppTheme_Light;

        int high_dropout = 70;
        int low_dropout = 20;

        PreferenceHandler preferenceHandler = PreferenceHandler.getInstance(getContext());
        int  theme_id = preferenceHandler.getIntValue("THEME_ID");


        if(theme_id == DEFAULT_THEME_ID){


             if(dropout > high_dropout){

                reportsFragmentBinding.headerStatsGlobal.circularProgressbarDropOut.setProgressDrawable(getContext().getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_dark_low));

            }

            else if(dropout < low_dropout){

                 reportsFragmentBinding.headerStatsGlobal.circularProgressbarDropOut.setProgressDrawable(getContext().getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_dark_high));

             }
            else {

                 reportsFragmentBinding.headerStatsGlobal.circularProgressbarDropOut.setProgressDrawable(getContext().getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_dark_meduim));



             }


        }else{


           if(dropout > high_dropout){

                reportsFragmentBinding.headerStatsGlobal.circularProgressbarDropOut.setProgressDrawable(getContext().getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_light_low));

            }

            else if(dropout < low_dropout){

                reportsFragmentBinding.headerStatsGlobal.circularProgressbarDropOut.setProgressDrawable(getContext().getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_light_high));

            }
            else {
                reportsFragmentBinding.headerStatsGlobal.circularProgressbarDropOut.setProgressDrawable(getContext().getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_light_meduim));

            }



        }








    }

    int pStatus = 0;
    String percentage = "";
    private void setUpCircularProgress1(ProgressBar progressBar, TextView tvProgress, int progress) {

        Handler handler = new Handler();
        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (pStatus < progress) {
                    pStatus += 1;
                    percentage = Integer.toString(progress)+"%";

                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            progressBar.setProgress(pStatus);
                            tvProgress.setText(percentage);

                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        // Just to display the progress slowly
                        Thread.sleep(50); //thread will take approx 3 seconds to finish
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }


    int pStatus2 = 0;
    String percentage2 = "";
    private void setUpCircularProgress2(ProgressBar progressBar, TextView tvProgress, int progress) {

        Handler handler = new Handler();
        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (pStatus2 < progress) {
                    pStatus2 += 1;
                    percentage2 = Integer.toString(progress)+"%";

                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            progressBar.setProgress(pStatus2);
                            tvProgress.setText(percentage2);

                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        // Just to display the progress slowly
                        Thread.sleep(50); //thread will take approx 3 seconds to finish
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }



    ProgressDialog progressDialog;

    public void setProgressDialog(){

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        new Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed





                        // onLoginFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);

    }










    @Override
    public void onPrepareOptionsMenu(Menu menu) {
       MenuItem itemSearch = menu.findItem(R.id.action_hospital_schedule);
       // itemSearch.setVisible(false);
      //  item.setVisible(false);

    }











    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

      //  inflater.inflate(R.menu.menu_home, menu);

        MenuItem item = menu.findItem(R.id.action_hospital_schedule);
        item.setVisible(false);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        /*
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                //supportFinishAfterTransition();

                return true;

            case R.id.action_search:
                return true;
        }
        */

        return super.onOptionsItemSelected(item);
    }



    ////////////////////////////////////////////////////////////////////////////////////////////

    private void loadReportPdf(){

        ArrayList<CoverageReport> coverageReports = new ArrayList<>();


        ImageButton button = reportsFragmentBinding.btnReport;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coverageReports.clear();


               for(VaccineSpecification specification: ReadVaccines.getInstance().getVaccineSpecificationList()){

                   String searchDate = DateCalendarConverter.dateToStringWithoutTimeAndDay(new Date());
                   //searchDate used only to get months
                   GenerateReport.ReportType reportType = GenerateReport.ReportType.VACCINE_SPECIFIC_STATS;
                   int year = Calendar.getInstance().get(Calendar.YEAR);

                   GenerateReport.RequestValues requestValues = new GenerateReport.RequestValues(reportType,searchDate);
                   requestValues.setTypeOfVaccine(specification.getVaccineName());
                   requestValues.setYear(year);
                   requestValues.setDataChanged(true);
                   requestValues.setRefresh(true);

                   GenerateReport generateReport = Injection.provideGenerateReport(getActivity().getApplication());

                   generateReport.execute(requestValues, new UseCase.OutputBoundary<GenerateReport.ResponseValue>() {
                       @Override
                       public void onSuccess(GenerateReport.ResponseValue result) {

                           CoverageReport coverageReport = result.getCoverageReport();
                           coverageReports.add(coverageReport);


                       }

                       @Override
                       public void onError(String errorMessage) {


                       }
                   });

               }




                try {

                    permissonPdfWrapper(coverageReports,"report_vims_"+ UUID.randomUUID().toString().substring(0,5));


                }catch (FileNotFoundException fileNotFound){

                }
                catch (DocumentException documentException) {

                }

            }
        });




    }


    //CREATING PDFS///////////////////////////////////////////////////////

    final private int REQUEST_CODE_ASK_PERMISSIONS = 111;

    MyReportPdfCreator myPdfCreator;

    private void permissonPdfWrapper(List<CoverageReport> reportsCards, String pdfname) throws FileNotFoundException,DocumentException{


        int hasWriteStoragePermission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        boolean isWriteStoragePermissionGranted = hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED;

        if (isWriteStoragePermissionGranted) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_CONTACTS)) {
                    if(getContext() != null) {

                        showAllowAccessToStorage();


                        return;
                    }
                }

                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_CODE_ASK_PERMISSIONS);

            }


            return;
        }else {

            new MainThreadExecutor(2000).execute(new Runnable() {
                @Override
                public void run() {
                    loadPdf(reportsCards,pdfname);

                }
            });


        }
    }


    private void loadPdf(List<CoverageReport> reportsCards, String pdfname){

        try{

            PreferenceHandler preferenceHandler = PreferenceHandler.getInstance(getContext());

            String hospitalName = preferenceHandler.getPref(PreferenceHandler.USER_HOSPITAL_NAME);
            myPdfCreator = new MyReportPdfCreator(getContext());

            myPdfCreator.createPdf(reportsCards, pdfname,hospitalName);
            previewPdf();


        }catch (FileNotFoundException f){


        }catch (DocumentException d){


        }

    }



    private void showAllowAccessToStorage(){

        showMessageOKCancel("You need to allow access to Storage",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);

                        }
                    }
                });

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.Theme_AppCompat_Light_Dialog_Alert);
        if(builder.getContext() != null){
            builder.setMessage(message);
            builder.setPositiveButton("Ok",okListener);
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.create();
            builder.show();
        }

    }


    private void grantAllUriPermissions(Context context, Intent intent, Uri uri) {
        List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
    }

    private void previewPdf() {
        PackageManager packageManager = getActivity().getPackageManager();
        Intent testIntent = new Intent(Intent.ACTION_VIEW);


        /** solves error 1
         * This file could not be accessed Check the location or the network and try again. Android 6 Marshmallow
         *
         * **/
        testIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        testIntent.setType("application/pdf");
        List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
        if (list.size() > 0) {
            Intent intent = new Intent();
            // file:///storage/emulated/0/Documents/healthcardRichardOweon2019.pdf exposed beyond app through Intent.getData()
            intent.setAction(Intent.ACTION_VIEW);
            //  Uri uri = Uri.fromFile(myPdfCreator.getPdfFile());

            Uri uri = FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider",
                    myPdfCreator.getPdfFile());


            /** solves error 1 **/
            grantAllUriPermissions(getContext(),testIntent,uri);


            intent.setDataAndType(uri, "application/pdf");

            startActivity(intent);
        }else{
            Toast.makeText(getContext(),"Download a PDF Viewer to see the generated PDF", Toast.LENGTH_SHORT).show();
        }
    }


    private static final String TAG = "PdfCreatorActivity";


    //////////////////////////////////////////////////////////////////////////////////////
















    private void setUpYearSpinner(boolean isRefresh){

        Date today = new Date();
        String year =  DateCalendarConverter.dateToStringGetYear(today);
        int year1 = Integer.parseInt(year);
        int year2 = year1 - 1;
        int year3 = year2 - 1;
        int year4 = year3 - 1;

        String[] yearArray = new String[4];
        yearArray[0] = Integer.toString(year1);
        yearArray[1] = Integer.toString(year2);
        yearArray[2] = Integer.toString(year3);
        yearArray[3] = Integer.toString(year4);

        Spinner yearSpinner = reportsFragmentBinding.graphYearSpinner;

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),R.layout.item_spinner_blue, yearArray);
        adapter.setDropDownViewResource(R.layout.item_spinner_dropdown_blue);
        yearSpinner.setAdapter(adapter);

        yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getSelectedItem().toString();
                int yearSelected = Integer.parseInt(item);
                setUpVaccineSpinner(isRefresh,yearSelected);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void setUpVaccineSpinner(boolean isRefresh, int year){

        Spinner graphSpinner = reportsFragmentBinding.graphSpinnerVaccine;

        fragmentReportsViewModel.loadReports(isRefresh,"OPV0",year);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(getContext(), R.array.vaccine_array, R.layout.item_spinner_blue);
        adapter.setDropDownViewResource(R.layout.item_spinner_dropdown_blue);
        graphSpinner.setAdapter(adapter);

        graphSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getSelectedItem().toString();

                if (item.equals("OPV0")) {

                    fragmentReportsViewModel.loadReports(isRefresh,"OPV0",year);

                }if (item.equals("BCG")) {

                    fragmentReportsViewModel.loadReports(isRefresh,"BCG",year);

                }if (item.equals("OPV1")) {

                    fragmentReportsViewModel.loadReports(isRefresh,"OPV1",year);
                    
                }if (item.equals("Hib1")) {

                    fragmentReportsViewModel.loadReports(isRefresh,"Hib1",year);

                }if (item.equals("PCV1")) {

                    fragmentReportsViewModel.loadReports(isRefresh,"PCV1",year);

                }if (item.equals("Rota1")) {

                    fragmentReportsViewModel.loadReports(isRefresh,"Rota1",year);

                }if (item.equals("OPV2")) {

                    fragmentReportsViewModel.loadReports(isRefresh,"OPV2",year);

                }if (item.equals("Hib2")) {

                    fragmentReportsViewModel.loadReports(isRefresh,"Hib2",year);


                }if (item.equals("PCV2")) {

                    fragmentReportsViewModel.loadReports(isRefresh,"PCV2",year);

                }if (item.equals("Rota2")) {

                    fragmentReportsViewModel.loadReports(isRefresh,"Rota2",year);

                }if (item.equals("OPV3")) {

                    fragmentReportsViewModel.loadReports(isRefresh,"OPV3",year);

                }if (item.equals("Hib3")) {

                    fragmentReportsViewModel.loadReports(isRefresh,"Hib3",year);

                }if (item.equals("PCV3")) {

                    fragmentReportsViewModel.loadReports(isRefresh,"PCV3",year);

                }if (item.equals("Rota3")) {

                    fragmentReportsViewModel.loadReports(isRefresh,"Rota3",year);

                }if (item.equals("IPV")) {

                    fragmentReportsViewModel.loadReports(isRefresh,"IPV",year);

                }if (item.equals("Measles")) {

                    fragmentReportsViewModel.loadReports(isRefresh,"Measles",year);

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void observeErrors(){

        Observer<String> error =new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

               // Toast.makeText(getContext(),""+s,Toast.LENGTH_LONG).show();



                //reportsFragmentBinding.progressbarReports.setVisibility(View.INVISIBLE);
                //reportsFragmentBinding.errorViewReports.setVisibility(View.VISIBLE);
                //reportsFragmentBinding.headerGraph.reportsHeader.setVisibility(View.INVISIBLE);
               //////// reportsFragmentBinding.cardViewHomeWelcome.setVisibility(View.INVISIBLE);
                //reportsFragmentBinding.progressbarReports.setVisibility(View.INVISIBLE);
               //////// reportsFragmentBinding.cardViewGraphContainerVaccine.setVisibility(View.INVISIBLE);
              //  reportsFragmentBinding.errorViewReports.setVisibility(View.VISIBLE);
                reportsFragmentBinding.progressbarReports.setVisibility(View.INVISIBLE);


            }
        };

        fragmentReportsViewModel.getError().observe(this,error);


    }

    private void observeProgress(){

        Observer<FragmentReportsViewModel.ReportProgress> progressObserver = new Observer<FragmentReportsViewModel.ReportProgress>() {
            @Override
            public void onChanged(@Nullable FragmentReportsViewModel.ReportProgress reportProgress) {

                if(reportProgress == FragmentReportsViewModel.ReportProgress.IN_PROGRESS){
                    // reportsFragmentBinding.headerGraph.reportsHeader.setVisibility(View.INVISIBLE);
                    // reportsFragmentBinding.cardViewHomeWelcome.setVisibility(View.VISIBLE);
                    // reportsFragmentBinding.errorViewReports.setVisibility(View.INVISIBLE);
                   // reportsFragmentBinding.cardViewHomeWelcome.setVisibility(View.INVISIBLE);
                    //reportsFragmentBinding.cardViewGraphContainerVaccine.setVisibility(View.INVISIBLE);
                    reportsFragmentBinding.progressbarReports.setVisibility(View.VISIBLE);

                    reportsFragmentBinding.swiperefresh.setRefreshing(false);


                }else if(reportProgress == FragmentReportsViewModel.ReportProgress.LOAD_SUCCESS){

                    //reportsFragmentBinding.headerGraph.reportsHeader.setVisibility(View.VISIBLE);
                   // reportsFragmentBinding.cardViewHomeWelcome.setVisibility(View.VISIBLE);
                    reportsFragmentBinding.progressbarReports.setVisibility(View.INVISIBLE);
                   // reportsFragmentBinding.errorViewReports.setVisibility(View.INVISIBLE);
                    //reportsFragmentBinding.mainConstraintLayoutReports.setVisibility(View.VISIBLE);
                   // reportsFragmentBinding.cardViewGraphContainerVaccine.setVisibility(View.VISIBLE);
                    reportsFragmentBinding.swiperefresh.setRefreshing(false);

                }
                else {//failed
                    //  reportsFragmentBinding.headerGraph.reportsHeader.setVisibility(View.INVISIBLE);
                   // reportsFragmentBinding.cardViewHomeWelcome.setVisibility(View.INVISIBLE);
                    //reportsFragmentBinding.progressbarReports.setVisibility(View.INVISIBLE);
                   // reportsFragmentBinding.cardViewGraphContainerVaccine.setVisibility(View.INVISIBLE);
                   // reportsFragmentBinding.errorViewReports.setVisibility(View.VISIBLE);
                    reportsFragmentBinding.progressbarReports.setVisibility(View.INVISIBLE);
                    reportsFragmentBinding.swiperefresh.setRefreshing(false);
                }

            }
        };

        fragmentReportsViewModel.getProgressMutableLiveData().observe(this,progressObserver);

    }

    private void fonts(){

        reportsFragmentBinding.tvStatsVaccinatedValue.setTypeface(MyFonts.getTypeFace(getContext()));
        reportsFragmentBinding.tvStatsNotVaccinatedValue.setTypeface(MyFonts.getTypeFace(getContext()));
        reportsFragmentBinding.tvStatsNotVaccinatedLabelSubtitle.setTypeface(MyFonts.getTypeFace(getContext()));
        reportsFragmentBinding.tvStatsNotVaccinatedLabelTitle.setTypeface(MyFonts.getTypeFace(getContext()));
        reportsFragmentBinding.tvStatsVaccinatedLabelSubtitle.setTypeface(MyFonts.getTypeFace(getContext()));
        reportsFragmentBinding.tvStatsVaccinatedLabelTitle.setTypeface(MyFonts.getTypeFace(getContext()));
        reportsFragmentBinding.graphSubtitle.setTypeface(MyFonts.getTypeFace(getContext()));
        reportsFragmentBinding.graphSubtitlePercent.setTypeface(MyFonts.getTypeFace(getContext()));
        reportsFragmentBinding.graphPercentageTitle.setTypeface(MyFonts.getTypeFace(getContext()));

        reportsFragmentBinding.tvStatsVaccine.setTypeface(MyFonts.getTypeFace(getContext()));

    }

    private void observeGraphReports(){

        Observer<CoverageReport> coverageReportObserver = new Observer<CoverageReport>() {
            @Override
            public void onChanged(@NonNull CoverageReport coverageReport) {


                VaccineReportsCard card =  coverageReport.getVaccineReportsCard();
                List<VaccineReport> vaccineReports = card.getVaccineReports();

                int immunized = card.getNoImmunized();
                int notImmunised = coverageReport.getTotalMissedImmunisation();

                String vaccine = "- "+card.getVaccineName();


                int coverage = coverageReport.getCoveragePercentage();

                //int percentageUpcoming = (int) (((double) upcoming / (double) totalVaccinations * 100));
               // int percentageMissed = (int) (((double) missed / (double) totalVaccinations * 100));
               // int percentageDone = (int) (((double) done / (double) totalVaccinations * 100));


                String immunizedString = ""+immunized;
                String notImmunizedString = ""+notImmunised;
                String totalChildren = ""+card.getNoInplan();

                String coverageConverted = Integer.toString(coverage) + "%";
               // reportsFragmentBinding.tv.setText(coverageConverted);tvTargetPopulation

                reportsFragmentBinding.tvTargetPopulation.setText(totalChildren);
                reportsFragmentBinding.graphSubtitle.setText(coverageConverted);
                reportsFragmentBinding.tvStatsVaccinatedValue.setText(immunizedString);
                reportsFragmentBinding.tvStatsNotVaccinatedValue.setText(notImmunizedString);
                reportsFragmentBinding.tvStatsVaccine.setText(vaccine);


                animateTextView(0,coverage,reportsFragmentBinding.graphSubtitle);

                if (getContext() != null) {
                    setUpBarChart(vaccineReports);
                    setUpLineChart(vaccineReports);
                    setUpLineChartTimeLine(coverageReport.getVaccineReportsCard().getCoverageTimeLines());
                }

            }


        };

        //declare view model observation
        fragmentReportsViewModel.getCoverageReportMutableLiveData().observe(this,coverageReportObserver);

    }

    private void observeEllapsedTime(){

        Observer<String> ellapsedObs = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

              //    Toast.makeText(getContext(),"time1: "+s+" secs", Toast.LENGTH_LONG).show();;

            }
        };
        //decalare observer
        fragmentReportsViewModel.getEllapsedTime().observe(this,ellapsedObs);


    }


    int legendTextSize;
    private void setLegendTextSize(){
        boolean isTab = getResources().getBoolean(R.bool.tablet);
        if(isTab){
            legendTextSize = 14;
        }else{
            legendTextSize = 8;
        }
    }

    private void setUpBarChart(List<VaccineReport> vaccineReports){
      //  setLegendTextSize();

        BarChart barChart = reportsFragmentBinding.chartVaccine;


        List<BarEntry> entries = new ArrayList<>();

        for (VaccineReport data : vaccineReports) {
            // turn your data into Entry objects
            entries.add(new BarEntry(data.getxAxisValue(), data.getCoverageImmunisedDelayed()));
        }

        //DataSet objects hold data which belongs together, and allow individual styling of that data.
        BarDataSet dataSet = new BarDataSet(entries,"Children");
        dataSet.setStackLabels(new String[]{"Immunised", "Not Immunised"});



        //change color of dataset stack labels
        barChart.getLegend().setTextColor(xaxis_label_text_color);
        barChart.getLegend().setTextSize(legendTextSize);



        // add entries to dataset
        int barColors[] = new int[2];
        barColors[0] = bar_color1;//cpt0
        barColors[1] = bar_color2;//cpd

        dataSet.setColors(barColors);
        dataSet.setValueTextColor(xaxis_value_text_color); // styling, value colorss



        //ad labels horizontally
        ArrayList<String> labels = new ArrayList<>();

        labels.add("Jan");
        labels.add("Feb");
        labels.add("Mar");
        labels.add("Apr");
        labels.add("May");
        labels.add("Jun");
        labels.add("Jul");
        labels.add("Aug");
        labels.add("Sep");
        labels.add("Oct");
        labels.add("Nov");
        labels.add("Dec");



        YAxis yAxis1 = barChart.getAxisLeft();
        YAxis yAxis2 = barChart.getAxisRight();


        yAxis1.setDrawAxisLine(false);
        yAxis2.setDrawAxisLine(false);

        yAxis1.setDrawGridLines(false);
        // yAxis2.setDrawGridLines(false);


        yAxis1.setTextColor(yaxis1_scale_text_Color);
        yAxis2.setTextColor(yaxis2_scale_text_Color);

        yAxis1.setGridColor(yaxis1_grid_Color);
        yAxis2.setGridColor(yaxis2_grid_Color);



        XAxis xAxis = barChart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setTextColor(xaxis_label_text_color);
        xAxis.setCenterAxisLabels(true);
        xAxis.setDrawGridLines(false);


        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGridColor(xaxis_grid_color);
        xAxis.setTextSize(14);

        //xAxis.setLabelRotationAngle(-90);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                if(value >= 0){
                    if (value <= labels.size() - 1) {
                        return labels.get((int) value);
                    }
                    return "";

                }
                return "";

            }
        });


        // ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        // dataSets.add(dataSet);




        //This object holds all data that is represented by a Chart
        //LineData lineData = new LineData(dataSet);
        BarData barData = new BarData(dataSet);
        barData.setBarWidth(0.7f); // set custom bar width

        barData.setDrawValues(true);


        barChart.setFitBars(true); // make the x-axis fit exactly all bars
        barChart.setData(barData);
        barChart.setHighlightFullBarEnabled(false);
        barChart.getDescription().setEnabled(false);
        barChart.animateY(1500);
        barChart.invalidate(); // refresh
        barChart.setNoDataText("Loading..");
        barChart.setNoDataTextColor(getResources().getColor(R.color.white));
        barChart.setNoDataTextTypeface(MyFonts.getTypeFace(getContext()));
       // barChart.setNoDataTextTypeface(MyFonts.getBoldTypeFace(getContext()));

        // reportsFragmentBinding.linechart.setVisibility(View.INVISIBLE);
       // barChart.setVisibility(View.VISIBLE);


    }

    private void setUpLineChart(List<VaccineReport> vaccineReports){

        List<Entry> entries = new ArrayList<>();

        for (VaccineReport data : vaccineReports) {
            // turn your data into Entry objects
            float xvalue =  Float.parseFloat(Integer.toString(data.getxAxisValue()));

            entries.add(new Entry(xvalue, data.getCoveragePercentage() ));
        }


        LineChart lineChart = reportsFragmentBinding.linechart;

        LineDataSet dataSet = new LineDataSet(entries, "Immunised %");
        //set curve
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);

        dataSet.setColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryTint3));
        dataSet.setValueTextColor(ContextCompat.getColor(getContext(), R.color.white));

        dataSet.setLineWidth(3f);

        dataSet.setValueTextSize(9f);


        //To remove the cricle from the graph
        dataSet.setDrawCircles(false);
       // dataSet.setCircleRadius(8f);
        dataSet.setDrawCircleHole(false);
        dataSet.setCircleRadius(3f);



        //to enable the cubic density : if 1 then it will be sharp curve
        dataSet.setCubicIntensity(0.1f);

        //to fill the smooth line  curve
        dataSet.setDrawFilled(true);
        //set the transparency
        dataSet.setFillAlpha(80);


        dataSet.setFillColor(getResources().getColor(R.color.colorPrimaryTint0));
        //GRADIENT
        //set the gradiant then the above draw fill color will be replace
        Drawable drawable = ContextCompat.getDrawable(getContext(), drawale_gradient);
        dataSet.setFillDrawable(drawable);




        //dataSet.enableDashedLine(10f, 5f, 0f);
        /***********************************************************************/
        // Controlling X axis
        XAxis xAxis = lineChart.getXAxis();
       // XAxis xAxis = reportsFragmentBinding.linechart_timeline.getXAxis();
        // Set the xAxis position to bottom. Default is top
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        //Customizing x axis value
        final String[] months = new String[]{"Jan", "Feb", "Mar", "Apr","May","June","Jul","Aug","Sep","Oct","Nov","Dec"};

        IAxisValueFormatter formatter = new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return months[(int) value-1];
            }
        };

        xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
        xAxis.setValueFormatter(formatter);
        //do not draw the xaxis lines that move up
        xAxis.setDrawGridLines(false);
        //color text
        xAxis.setTextColor(xaxis_label_text_color);
        xAxis.setGridColor(xaxis_grid_color);

        /** Controlling right side of y axis **/
        YAxis yAxisRight = lineChart.getAxisRight();
        yAxisRight.setGridColor(yaxis1_grid_Color);
        yAxisRight.setEnabled(false);

        //***
        // Controlling left side of y axis
        YAxis yAxisLeft = lineChart.getAxisLeft();
        yAxisLeft.setGranularity(1f);
        yAxisLeft.setAxisMaximum(100f);
        yAxisLeft.setAxisMinimum(0f);
        // yAxisLeft.setEnabled(false);
        //color of the text values on the left y axis
        yAxisLeft.setTextColor(yaxis1_scale_text_Color);
        //color of the y axis lines
        yAxisLeft.setGridColor(yaxis2_grid_Color);


        //GRID LINES
        yAxisLeft.setDrawAxisLine(false);
        yAxisLeft.setDrawAxisLine(false);

        yAxisRight.setDrawGridLines(false);
        // yAxis2.setDrawGridLines(false);



        // Setting Data
        LineData data = new LineData(dataSet);
        lineChart.setData(data);
        lineChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);

        //Legend
        //color of the bar desc text
        Legend legend = lineChart.getLegend();
        //lineChart.getLegend().setTextColor(R.color.colorPrimaryTint3);
        legend.setTextSize(legendTextSize);
        legend.setTextColor(R.color.colorPrimaryTint3);
        //set legend disable or enable to hide {the left down corner name of graph}
        legend.setEnabled(true);



        Description description = new Description();
        description.setText("");
        //description.setTextColor(R.color.colorPrimaryTint3);

        reportsFragmentBinding.linechart.setDescription(description);
        reportsFragmentBinding.linechart.setNoDataText("");

        //refresh
        reportsFragmentBinding.linechart.invalidate();
        //reportsFragmentBinding.chart.setVisibility(View.INVISIBLE);

        /*********************************************************************/




    }

    private void setUpLineChartTimeLine(List<CoverageTimeLine> coverageTimeLines){

        LineChart lineChart = reportsFragmentBinding.linechartTimeline;
       // LineChart lineChart = reportsFragmentBinding.linechart1;

        List<Entry> entries = new ArrayList<>();

        for (CoverageTimeLine data : coverageTimeLines) {
            // turn your data into Entry objects
            float xvalue =  Float.parseFloat(Integer.toString(data.getYear()));


            entries.add(new Entry(xvalue, data.getCoverage()));
        }


        LineDataSet dataSet = new LineDataSet(entries, "Immunised %");
        //set curve
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);

        dataSet.setColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryTint3));
        dataSet.setValueTextColor(ContextCompat.getColor(getContext(), R.color.white));
        dataSet.setFillColor(getResources().getColor(R.color.colorPrimaryTint2));
        dataSet.setLineWidth(3f);

        dataSet.setValueTextSize(9f);


        //To remove the cricle from the graph
        dataSet.setDrawCircles(false);
        //dataSet.setCircleRadius(8f);
        dataSet.setDrawCircleHole(false);
        dataSet.setCircleRadius(3f);



        //to enable the cubic density : if 1 then it will be sharp curve
        dataSet.setCubicIntensity(0.1f);

        //to fill the below of smooth line in graph
        dataSet.setDrawFilled(true);
        //set the transparency
        dataSet.setFillAlpha(80);


        //GRADIENT
        //set the gradiant then the above draw fill color will be replace
        Drawable drawable = ContextCompat.getDrawable(getContext(), drawale_gradient);
        dataSet.setFillDrawable(drawable);






        //dataSet.enableDashedLine(10f, 5f, 0f);
        /***********************************************************************/
        // Controlling X axis
        XAxis xAxis = lineChart.getXAxis();
        // Set the xAxis position to bottom. Default is top
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);


        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        int lastYear = currentYear - 1;
        int lastYearBy1 = currentYear -2;
        int lastYearBy2 = currentYear - 3;
        int lastYearBy3 = currentYear - 4;



        //Customizing x axis value
        final String[] months = new String[]{Integer.toString(lastYearBy3), Integer.toString(lastYearBy2), Integer.toString(lastYearBy1), Integer.toString(lastYear),Integer.toString(currentYear)};

        IAxisValueFormatter formatter = new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return months[(int) value-lastYearBy3];
            }
        };

        xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
        xAxis.setValueFormatter(formatter);
        //do not draw the xaxis lines that move up
        xAxis.setDrawGridLines(false);
        //color text
        xAxis.setTextColor(xaxis_label_text_color);
        xAxis.setGridColor(xaxis_grid_color);

        /** Controlling right side of y axis **/
        YAxis yAxisRight = lineChart.getAxisRight();
        yAxisRight.setGridColor(yaxis1_grid_Color);
        yAxisRight.setEnabled(false);

        //***
        // Controlling left side of y axis
        YAxis yAxisLeft = lineChart.getAxisLeft();
        yAxisLeft.setGranularity(1f);
        yAxisLeft.setAxisMaximum(100f);
        yAxisLeft.setAxisMinimum(0f);
        // yAxisLeft.setEnabled(false);
        //color of the text values on the left y axis
        yAxisLeft.setTextColor(yaxis1_scale_text_Color);
        //color of the y axis lines
        yAxisLeft.setGridColor(yaxis2_grid_Color);


        //GRID LINES
        yAxisLeft.setDrawAxisLine(false);
        yAxisLeft.setDrawAxisLine(false);

        yAxisRight.setDrawGridLines(false);
        // yAxis2.setDrawGridLines(false);




        // Setting Data
        LineData data = new LineData(dataSet);
        lineChart.setData(data);
        lineChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);

        //Legend
        //color of the bar desc text
        Legend legend = lineChart.getLegend();
        lineChart.getLegend().setTextColor(R.color.colorPrimaryTint0);
        legend.setTextSize(legendTextSize);
        //set legend disable or enable to hide {the left down corner name of graph}
        legend.setEnabled(true);



        Description description = new Description();
        description.setText("");

        lineChart.setDescription(description);
        lineChart.setNoDataText("");

        //refresh
        lineChart.invalidate();

        lineChart.setVisibility(View.VISIBLE);
        // reportsFragmentBinding.chart.setVisibility(View.INVISIBLE);

        /*********************************************************************/




    }










    private int mShortAnimationDuration;


    private void crossfadeBarToLine() {

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        reportsFragmentBinding.chartVaccine.setAlpha(0f);
        reportsFragmentBinding.chartVaccine.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        reportsFragmentBinding.chartVaccine.animate()
                .alpha(1f)
                .setDuration(mShortAnimationDuration)
                .setListener(null);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        reportsFragmentBinding.linechart.animate()
                .alpha(0f)
                .setDuration(mShortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        reportsFragmentBinding.linechart.setVisibility(View.GONE);
                    }
                });
    }

    private void crossfadeLineToBar() {

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        reportsFragmentBinding.linechart.setAlpha(0f);
        reportsFragmentBinding.linechart.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        reportsFragmentBinding.linechart.animate()
                .alpha(1f)
                .setDuration(mShortAnimationDuration)
                .setListener(null);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        reportsFragmentBinding.chartVaccine.animate()
                .alpha(0f)
                .setDuration(mShortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        reportsFragmentBinding.chartVaccine.setVisibility(View.GONE);
                    }
                });
    }



    private void btnMonthChartsSelect(){

        // Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);


        reportsFragmentBinding.btnMonthBarChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reportsFragmentBinding.btnMonthBarChart.setCardBackgroundColor(getResources().getColor(R.color.colorPrimaryTint3));
                reportsFragmentBinding.btnMonthLineChart.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));

                crossfadeBarToLine();

            }
        });

        reportsFragmentBinding.btnMonthLineChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reportsFragmentBinding.btnMonthBarChart.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
                reportsFragmentBinding.btnMonthLineChart.setCardBackgroundColor(getResources().getColor(R.color.colorPrimaryTint3));

                crossfadeLineToBar();


            }
        });



    }




    class WizardPagerAdapter extends PagerAdapter {

        public Object instantiateItem(View collection, int position) {

            int resId = 0;
            switch (position) {

                case 0:
                    resId = R.id.month_chart;


                    break;

                case 1:
                    resId = R.id.linechart_timeline;


                    break;
            }
            return getActivity().findViewById(resId);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == ((View) arg1);
        }
    }

    private void setupGraphSlidePager(){

        //set up selection of month charts on btn click
        btnMonthChartsSelect();

        //reportsFragmentBinding.imageViewStep1.setBackground(getResources().getDrawable(R.drawable.ic_brightness_1_select));
        //reportsFragmentBinding.imageViewStep2.setBackground(getResources().getDrawable(R.drawable.ic_brightness_2_select));

        reportsFragmentBinding.imageViewStep1.setSelected(true);
        reportsFragmentBinding.imageViewStep2.setSelected(false);

        //reportsFragmentBinding.imageViewStep1.setPaintFlags(reportsFragmentBinding.imageViewStep1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        // reportsFragmentBinding.btnMonthBarChart.setSelected(true);

        //Set the ViewPager adapter
        WizardPagerAdapter viewPagerAdapter = new WizardPagerAdapter();
        ViewPager pager = reportsFragmentBinding.pager;
        pager.setAdapter(viewPagerAdapter);


        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0) {

                    // Toast.makeText(getContext(),"slide1",Toast.LENGTH_LONG).show();

                    reportsFragmentBinding.imageViewStep1.setSelected(true);
                    reportsFragmentBinding.imageViewStep2.setSelected(false);
                    //reportsFragmentBinding.imageViewStep3.setSelected(false);
                    //reportsFragmentBinding.imageViewStep1.setBackground(getResources().getDrawable(R.drawable.ic_brightness_1_select));

                    reportsFragmentBinding.btnMonthBarChart.setVisibility(View.VISIBLE);
                    reportsFragmentBinding.btnMonthLineChart.setVisibility(View.VISIBLE);



                }
                else {

                  //  Toast.makeText(getContext(),"slide2",Toast.LENGTH_LONG).show();

                    reportsFragmentBinding.imageViewStep1.setSelected(false);
                    reportsFragmentBinding.imageViewStep2.setSelected(true);
                   // reportsFragmentBinding.imageViewStep3.setSelected(true);
                    //reportsFragmentBinding.imageViewStep2.setBackground(getResources().getDrawable(R.drawable.ic_brightness_2_select));

                    reportsFragmentBinding.btnMonthBarChart.setVisibility(View.INVISIBLE);
                    reportsFragmentBinding.btnMonthLineChart.setVisibility(View.INVISIBLE);


                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }














    ////OLD BOYS ASSOCIATION


    private void chartSampleSpecs(){


        /*

             lineChartDownFill = view.findViewById(R.id.lineChartDownFill);
             lineChartDownFill.setTouchEnabled(false);
             lineChartDownFill.setDragEnabled(true);
             lineChartDownFill.setScaleEnabled(true);
             lineChartDownFill.setPinchZoom(false);
             lineChartDownFill.setDrawGridBackground(false);
             lineChartDownFill.setMaxHighlightDistance(200);
             lineChartDownFill.setViewPortOffsets(0, 0, 0, 0);
             lineChartDownFillWithData();

       //////////////////////////////////////////////////////////



             Description description = new Description();
             description.setText("Days Data");

             lineChartDownFill.setDescription(description);


             ArrayList<Entry> entryArrayList = new ArrayList<>();
             entryArrayList.add(new Entry(0, 60f, "1"));
             entryArrayList.add(new Entry(1, 55f, "2"));
             entryArrayList.add(new Entry(2, 60f, "3"));
             entryArrayList.add(new Entry(3, 40f, "4"));
             entryArrayList.add(new Entry(4, 45f, "5"));
             entryArrayList.add(new Entry(5, 36f, "6"));
             entryArrayList.add(new Entry(6, 30f, "7"));
             entryArrayList.add(new Entry(7, 40f, "8"));
             entryArrayList.add(new Entry(8, 45f, "9"));
             entryArrayList.add(new Entry(9, 60f, "10"));
             entryArrayList.add(new Entry(10, 45f, "10"));
             entryArrayList.add(new Entry(11, 20f, "10"));


             //LineDataSet is the line on the graph
             LineDataSet lineDataSet = new LineDataSet(entryArrayList, "This is y bill");

             lineDataSet.setLineWidth(5f);
             lineDataSet.setColor(Color.GRAY);
             lineDataSet.setCircleColorHole(Color.GREEN);
             lineDataSet.setCircleColor(R.color.colorWhite);
             lineDataSet.setHighLightColor(Color.RED);
             lineDataSet.setDrawValues(false);
             lineDataSet.setCircleRadius(10f);
             lineDataSet.setCircleColor(Color.YELLOW);

             //to make the smooth line as the graph is adrapt change so smooth curve
             lineDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
             //to enable the cubic density : if 1 then it will be sharp curve
             lineDataSet.setCubicIntensity(0.2f);

             //to fill the below of smooth line in graph
             lineDataSet.setDrawFilled(true);
             lineDataSet.setFillColor(Color.BLACK);
             //set the transparency
             lineDataSet.setFillAlpha(80);

             //set the gradiant then the above draw fill color will be replace
             Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradiant);
             lineDataSet.setFillDrawable(drawable);

             //set legend disable or enable to hide {the left down corner name of graph}
             Legend legend = lineChartDownFill.getLegend();
             legend.setEnabled(false);

             //to remove the cricle from the graph
             lineDataSet.setDrawCircles(false);

             //lineDataSet.setColor(ColorTemplate.COLORFUL_COLORS);


             ArrayList<ILineDataSet> iLineDataSetArrayList = new ArrayList<>();
             iLineDataSetArrayList.add(lineDataSet);

             //LineData is the data accord
             LineData lineData = new LineData(iLineDataSetArrayList);
             lineData.setValueTextSize(13f);
             lineData.setValueTextColor(Color.BLACK);
             */
    }



    /*
    private void setUpBarChart(List<VaccineReport> vaccineReports){

        List<BarEntry> entries = new ArrayList<>();
        for (VaccineReport data : vaccineReports) {
            // turn your data into Entry objects
            entries.add(new BarEntry(data.getxAxisValue(), data.getCoverageImmunisedDelayed()));
        }

        //DataSet objects hold data which belongs together, and allow individual styling of that data.
        BarDataSet dataSet = new BarDataSet(entries,"Vaccinations");
        dataSet.setStackLabels(new String[]{"Immunised", "Not Immunised"});

        //change color of stack labels
        reportsFragmentBinding.chart.getLegend().setTextColor(getResources().getColor(R.color.white));

        // add entries to dataset
        int barColors[] = new int[2];
        barColors[0] = bar_color1;//cpt0
        barColors[1] = bar_color2;//cpd

        dataSet.setColors(barColors);
        dataSet.setValueTextColor(xaxis_value_text_color); // styling, value colorss


        //ad labels horizontally

        ArrayList<String> labels = new ArrayList<>();

        labels.add("Jan");
        labels.add("Feb");
        labels.add("Mar");
        labels.add("Apr");
        labels.add("May");
        labels.add("Jun");
        labels.add("Jul");
        labels.add("Aug");
        labels.add("Sep");
        labels.add("Oct");
        labels.add("Nov");
        labels.add("Dec");



        YAxis yAxis1 = reportsFragmentBinding.chart.getAxisLeft();
        YAxis yAxis2 = reportsFragmentBinding.chart.getAxisRight();


        yAxis1.setDrawAxisLine(false);
        yAxis2.setDrawAxisLine(false);

        yAxis1.setDrawGridLines(false);
        // yAxis2.setDrawGridLines(false);


        yAxis1.setTextColor(yaxis1_scale_text_Color);
        yAxis2.setTextColor(yaxis2_scale_text_Color);

        yAxis1.setGridColor(yaxis1_grid_Color);
        yAxis2.setGridColor(yaxis2_grid_Color);



        XAxis xAxis = reportsFragmentBinding.chart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setTextColor(xaxis_value_text_color);
        xAxis.setCenterAxisLabels(true);
        xAxis.setDrawGridLines(false);


        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGridColor(xaxis_grid_color);
        xAxis.setTextSize(14);
        xAxis.setTypeface(MyFonts.getTypeFace(getContext()));
        //xAxis.setLabelRotationAngle(-90);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                if(value >= 0){
                    if (value <= labels.size() - 1) {
                        return labels.get((int) value);
                    }
                    return "";

                }
                return "";

            }
        });


        // ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        // dataSets.add(dataSet);




        //This object holds all data that is represented by a Chart
        //LineData lineData = new LineData(dataSet);
        BarData barData = new BarData(dataSet);
        barData.setBarWidth(0.5f); // set custom bar width

        barData.setDrawValues(true);


        reportsFragmentBinding.chart.setFitBars(true); // make the x-axis fit exactly all bars
        reportsFragmentBinding.chart.setData(barData);
        reportsFragmentBinding.chart.setHighlightFullBarEnabled(false);
        reportsFragmentBinding.chart.getDescription().setEnabled(false);
        reportsFragmentBinding.chart.animateY(1500);
        reportsFragmentBinding.chart.invalidate(); // refresh
        reportsFragmentBinding.chart.setNoDataText("Loading..");
        reportsFragmentBinding.chart.setNoDataTextColor(getResources().getColor(R.color.white));
        reportsFragmentBinding.chart.setNoDataTextTypeface(MyFonts.getTypeFace(getContext()));
        reportsFragmentBinding.chart.setNoDataTextTypeface(MyFonts.getBoldTypeFace(getContext()));

        reportsFragmentBinding.linechart.setVisibility(View.INVISIBLE);
        reportsFragmentBinding.chart.setVisibility(View.VISIBLE);


    }
     */

    /**
     *
     *   private void setUpGraphData(String vaccineName, int year){
     //plot a graph of coverage percentage with months
     GenerateReport generateReport = Injection.provideGenerateReport(getActivity().getApplication());

     String searchDate = DateCalendarConverter.dateToStringWithoutTimeAndDay(new Date());
     //searchDate used only to get months
     GenerateReport.ReportType reportType = GenerateReport.ReportType.VACCINE_SPECIFIC;

     GenerateReport.RequestValues requestValues = new GenerateReport.RequestValues(reportType,searchDate);
     requestValues.setTypeOfVaccine(vaccineName);
     requestValues.setYear(year);

     generateReport.execute(requestValues, new UseCase.OutputBoundary<GenerateReport.ResponseValue>() {
    @Override
    public void onSuccess(GenerateReport.ResponseValue result) {




    }

    @Override
    public void onError(String errorMessage) {


    reportsFragmentBinding.progressbarReports.setVisibility(View.INVISIBLE);

    }
    });

     }

     */

}
