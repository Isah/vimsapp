package com.vims.vimsapp.view.patients.detail;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.patients.addchild.AddChildActivityWizard;
import com.vims.vimsapp.view.patients.list.ViewCaretakerChildrenListFragment;

import java.util.ArrayList;
import java.util.List;

import vimsapp.R;
import vimsapp.databinding.PatientsFragmentCaretakerDetailBinding;

public class ViewCaretakerDetailsFragment extends Fragment implements AppBarLayout.OnOffsetChangedListener{


    ImageView mProfileImage;
    private boolean mIsAvatarShown = true;
    private int mMaxScrollSize;
    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;


    ViewCaretakerDetailsActivityViewModel detailsActivityViewModel;

    PatientsFragmentCaretakerDetailBinding profileBinding;


   // private static final String ARG_PARAM1 = "param1";
    ///private static final String ARG_PARAM2 = "param2";


    //private String mParam1;
   // private String mParam2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        profileBinding  = DataBindingUtil.inflate(inflater, R.layout.patients_fragment_caretaker_detail, container, false);
        detailsActivityViewModel =  ViewModelProviders.of(this).get(ViewCaretakerDetailsActivityViewModel.class);


        setUpDynamicAppBar();
     //   Bundle b = getArguments();

        Bundle b =  getActivity().getIntent().getExtras();

        if(b!=null) {
            String caretakerString = b.getString("caretaker");
            Caretaker caretaker = MyDetailsTypeConverter.fromStringToCaretaker(caretakerString);

            String full_name = caretaker.getFirstName()+" "+caretaker.getLastName();

            profileBinding.firstnameProfileTv.setTypeface(MyFonts.getTypeFace(getContext()));
            profileBinding.firstnameProfileTv.setText(full_name);
            profileBinding.address.setTypeface(MyFonts.getTypeFace(getContext()));
            String address = caretaker.getDistrict()+", Uganda";
            profileBinding.address.setText(address);


            profileBinding.fabCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getContext(),AddChildActivityWizard.class);
                    intent.putExtra("caretakerId", caretaker.getCaretakerId());
                    startActivity(intent);

                }
            });






            setupViewPager(b);
        }




        //initialize your UI

        return profileBinding.getRoot();
    }


    public static ViewCaretakerDetailsFragment newInstance(Bundle args) {
        ViewCaretakerDetailsFragment fragment = new ViewCaretakerDetailsFragment();
        //Bundle args = new Bundle();
       // args.putString(ARG_PARAM1, MyDetailsTypeConverter.fromCatakerToString(caretaker));

        fragment.setArguments(args);
        return fragment;
    }





    public void setUpDynamicAppBar(){

        AppBarLayout appbarLayout = profileBinding.materialupAppbar;

       // Toolbar toolbar = profileBinding.materialupToolbar;
       // toolbar.setTitle("");
        /*
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                // onBackPressed();



            }
        });
         */
        //setSupportActionBar(toolbar);
        //getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();

    }



    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {



        mProfileImage = profileBinding.materialupProfileImage;

        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int percentage = (Math.abs(verticalOffset)) * 100 / mMaxScrollSize;

        if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
            mIsAvatarShown = false;

            mProfileImage.animate()
                    .scaleY(0).scaleX(0)
                    .setDuration(200)
                    .start();
        }

        if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
            mIsAvatarShown = true;

            mProfileImage.animate()
                    .scaleY(1).scaleX(1)
                    .start();
        }





    }






    //set up the fragments on the viewpager
    private void setupViewPager(Bundle bundle) {

        //setup the viewpager--- it contains the fragments to be swiped
        ViewPager viewPager = null;
       // ViewPager viewPager =  profileBinding.materialupViewpager;
        //assign viewpager to tablayout
        profileBinding.tabsCaretaker.setupWithViewPager(viewPager);


        ViewCaretakerDetailsFragment.ViewPagerAdapter adapter = new ViewCaretakerDetailsFragment.ViewPagerAdapter(getChildFragmentManager());//child


        adapter.addFragment(FragmentCaretakerDetailsProfile.newInstance(bundle), "Profile");
        adapter.addFragment(ViewCaretakerChildrenListFragment.newInstance(bundle), "Siblings");
        viewPager.setAdapter(adapter);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }


        public void removeItem(int position) {
            mFragmentList.remove(position);
        }


        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}

