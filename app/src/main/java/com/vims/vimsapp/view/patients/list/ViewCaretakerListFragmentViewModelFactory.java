package com.vims.vimsapp.view.patients.list;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.vims.vimsapp.searchchild.ViewCaretakers;


/**
 * Created by root on 6/3/18.
 */
public class ViewCaretakerListFragmentViewModelFactory implements ViewModelProvider.Factory{


    private final ViewCaretakers viewCaretakers;

    public ViewCaretakerListFragmentViewModelFactory(ViewCaretakers viewCaretakers) {
        this.viewCaretakers = viewCaretakers;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if(modelClass.isAssignableFrom(ViewCaretakerListFragmentViewModel.class)){

            return (T) new ViewCaretakerListFragmentViewModel(viewCaretakers);

        }
        else{

            throw new IllegalArgumentException("View Model not found");
        }

    }




}
