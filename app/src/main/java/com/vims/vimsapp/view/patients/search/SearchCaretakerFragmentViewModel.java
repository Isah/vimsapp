package com.vims.vimsapp.view.patients.search;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.vims.vimsapp.UseCase;

import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.searchchild.SearchPatient;

import java.util.List;

public class SearchCaretakerFragmentViewModel extends ViewModel{


    SearchPatient searchCaretaker;


    MutableLiveData<CharSequence> mSearchText;

    MutableLiveData<List<Caretaker>> mCaretakers;


    MutableLiveData<String> mNoDataError;


    public SearchCaretakerFragmentViewModel(SearchPatient searchCaretaker) {
        this.searchCaretaker = searchCaretaker;

    }


    public MutableLiveData<CharSequence> getmSearchText() {

        return mSearchText;
    }

    public void setmSearchText(CharSequence searchText) {
        if(mSearchText == null){
            mSearchText = new MutableLiveData<>();

        }
        this.mSearchText.postValue(searchText);
    }






    public MutableLiveData<String> getmNoDataError() {
        return mNoDataError;
    }

    public void setmNoDataError(String noDataError) {
        if(mNoDataError == null){
            mNoDataError = new MutableLiveData<>();
        }
        this.mNoDataError.postValue(noDataError);
    }


    public LiveData<List<Caretaker>> getSearchedCaretakers() {

        if(mCaretakers ==null){

            mCaretakers = new MutableLiveData<>();

        }

        return mCaretakers;
    }


    public void loadCaretakers(String query){

       // mCaretakers = Transformations.switchMap(mSearchText, s ->);
      //  if(getmSearchText().getValue() != null) {


            searchCaretaker.execute(new SearchPatient.RequestValues(query, SearchPatient.Patient.CARETAKER), new UseCase.OutputBoundary<SearchPatient.ResponseValue>() {
                @Override
                public void onSuccess(SearchPatient.ResponseValue result) {

                    mCaretakers.postValue(result.getCaretakers());


                }

                @Override
                public void onError(String e) {

                    setmNoDataError(e);

                }
            });

        }


   // }




}
