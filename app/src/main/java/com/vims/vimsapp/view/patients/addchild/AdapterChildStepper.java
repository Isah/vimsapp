package com.vims.vimsapp.view.patients.addchild;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;
import com.vims.vimsapp.view.patients.addchild.AddChildBirthDateFragment;
import com.vims.vimsapp.view.patients.addchild.AddChildGenderFragment;
import com.vims.vimsapp.view.patients.addchild.AddChildProfileFragment;
import com.vims.vimsapp.view.patients.addchild.AddChildReviewFragment;

/**
 * Created by Lincoln on 6/4/2018.
 */

public class AdapterChildStepper extends AbstractFragmentStepAdapter {
    public AdapterChildStepper(@NonNull FragmentManager fm, @NonNull Context context) {
        super(fm, context);
    }
    @Override
    public Step createStep(int position) {
        Bundle b = new Bundle();
        switch (position) {
            case 0:
                final AddChildGenderFragment step1 = AddChildGenderFragment.newInstance("","");
                b.putInt("Child", position);
                step1.setArguments(b);
                return step1;

            case 1:
                final AddChildProfileFragment step2 = AddChildProfileFragment.newInstance("","");
                b.putInt("Profile", position);
                step2.setArguments(b);
                return step2;



            case 2:
                final AddChildBirthDateFragment step3 = AddChildBirthDateFragment.newInstance("","");
                b.putInt("Date", position);
                step3.setArguments(b);
                return step3;


            case 3:
                final AddChildWeightFragment step4 = AddChildWeightFragment.newInstance("","");
                b.putInt("Weight", position);
                step4.setArguments(b);
                return step4;

            case 4:
                final AddChildReviewFragment step5 = new AddChildReviewFragment();
                b.putInt("Review", position);
                step5.setArguments(b);
                return step5;


            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 5;
    }


    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        //Override this method to set Step title for the Tabs, not necessary for other stepper types
        switch (position){
            case 0:
                return new StepViewModel.Builder(context)
                        .setTitle("Gender") //can be a CharSequence instead
                        .create();
            case 1:
                return new StepViewModel.Builder(context)
                        .setTitle("Names") //can be a CharSequence instead
                        .create();

            case 2:
                return new StepViewModel.Builder(context)
                        .setTitle("BirthDate") //can be a CharSequence instead
                        .create();


            case 3:
                return new StepViewModel.Builder(context)
                        .setTitle("Weight") //can be a CharSequence instead
                        .create();


            case 4:
                return new StepViewModel.Builder(context)
                        .setTitle("Review") //can be a CharSequence instead
                        .create();

        }
        return null;
    }

}
