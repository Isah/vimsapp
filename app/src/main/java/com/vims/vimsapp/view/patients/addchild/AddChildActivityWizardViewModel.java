package com.vims.vimsapp.view.patients.addchild;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;


import com.vims.vimsapp.addchild_controller.AddChildController;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.registerchild.RegisterChild;
import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.registerchild.GenerateChildSchedule;
import com.vims.vimsapp.utilities.AgeCalculator;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.view.patients.detail.MyDetailsTypeConverter;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by root on 5/31/18.
 */

public class AddChildActivityWizardViewModel extends ViewModel {



    private MutableLiveData<CharSequence> mfirstName;
    private MutableLiveData<CharSequence> mlastName;
    private MutableLiveData<CharSequence> mweight;
    private MutableLiveData<CharSequence> mCaretakerName;
    private MutableLiveData<String> mCaretakerId;
    private MutableLiveData<CharSequence> mdateOfBirth;
    private MutableLiveData<CharSequence> mGender;

    private MutableLiveData<CharSequence> mName;
    private MutableLiveData<CharSequence> mProfileDescription;



    private MutableLiveData<String> mRegisgrationResponse;
    private MutableLiveData<Boolean> mScheduleGenerateResponse;

    private MutableLiveData<String> mlastNameError;
    private MutableLiveData<String> mfirstNameError;
    private MutableLiveData<String> mBirthPlaceError;
    private MutableLiveData<String> mCaretakerError;
    private MutableLiveData<String> mRegisgrationResponseError;


    private RegisterChild registerChild;
    private GenerateChildSchedule generateChildSchedule;

    public AddChildActivityWizardViewModel(RegisterChild registerChild, GenerateChildSchedule generateChildSchedule) {

        this.registerChild = registerChild;
        this.generateChildSchedule = generateChildSchedule;

        mlastNameError = new MutableLiveData<>();
        mfirstNameError = new MutableLiveData<>();
        mBirthPlaceError= new MutableLiveData<>();
        mCaretakerError = new MutableLiveData<>();


        mfirstName = new MutableLiveData<>();
        mlastName = new MutableLiveData<>();
        mweight = new MutableLiveData<>();
        mCaretakerName = new MutableLiveData<>();
        mdateOfBirth = new MutableLiveData<>();
        mGender = new MutableLiveData<>();



        mRegisgrationResponse = new MutableLiveData<>();
        mScheduleGenerateResponse = new MutableLiveData<>();

        mRegisgrationResponseError = new MutableLiveData<>();

    }



    public MutableLiveData<CharSequence> getmProfileDescription() {

          if(mProfileDescription == null){
            mProfileDescription = new MutableLiveData<>();
          }




          //String dateValue = ""+getMdateOfBirth().getValue();

         // Date birthDate =  DateCalendarConverterForView.stringToDateWithoutTimeViewModel(dateValue);
          //String stringDate =  DateCalendarConverter.dateToStringWithoutTime(birthDate);
          String description = "Confirm  registration of this little baby "+getmGender().getValue()+" born on the "+getMdateOfBirth().getValue()+" with a birth weight of "+getmBirthWeight().getValue();

          mProfileDescription.postValue(description);


        return mProfileDescription;
    }



    public MutableLiveData<CharSequence> getmName() {

        if(mName == null){
            mName = new MutableLiveData<>();
        }

        String fullname = getFirstName().getValue()+" "+getLastName().getValue();

      //  String description = "little baby "+getmGender().getValue()+" "+fullname+" was born on "+getMdateOfBirth().getValue()+" from "+getmBirthPlace().getValue();

         mName.postValue(fullname);


        return mName;
    }




    public MutableLiveData<String> getmCaretakerId() {
        return mCaretakerId;
    }

    public void setmCaretakerId(String caretakerId) {
        if(mCaretakerId == null){

            mCaretakerId = new MutableLiveData<>();
        }
        mCaretakerId.postValue(caretakerId);
    }



    public LiveData<String> getMlastNameError() {
        if(mlastNameError == null){
            mlastNameError = new MutableLiveData<>();
        }
        return mlastNameError;
    }

    private void setMlastNameError(String lastNameError) {
        mlastNameError.postValue(lastNameError);
    }

    public LiveData<String> getMfirstNameError() {
        if(mfirstNameError == null){
            mfirstNameError = new MutableLiveData<>();
        }
        return mfirstNameError;

    }

    private void setMfirstNameError(String firstNameError) {
        mfirstNameError.postValue(firstNameError);
    }



    public LiveData<String> getRegistrationResponse() {
        return mRegisgrationResponse;
    }


    public MutableLiveData<Boolean> getmScheduleGenerateResponse() {

        if(mScheduleGenerateResponse == null){

            mScheduleGenerateResponse = new MutableLiveData<>();
        }
        return mScheduleGenerateResponse;
    }

    public void setmScheduleGenerateResponse(boolean scheduleGenerateResponse) {

        this.mScheduleGenerateResponse.postValue(scheduleGenerateResponse);

    }

    public void setRegistrationResponse(String res) {
        mRegisgrationResponse.postValue(res);
    }


    public LiveData<String> getRegistrationResponseError() {
        return mRegisgrationResponseError;
    }

    public void setRegistrationResponseError(String res) {
        mRegisgrationResponseError.postValue(res);
    }




    public LiveData<CharSequence> getFirstName() {

        return mfirstName;
    }

    public void setFirstName(CharSequence fName) {
        mfirstName.postValue(fName);
    }

    public LiveData<CharSequence> getLastName() {

        return mlastName;
    }

    public void setLastName(CharSequence lName) {
        mlastName.postValue(lName);
    }




    private MutableLiveData<CharSequence> total_weight_float;



    public void setFullWeight(){


        if(weight_float.getValue()!=null && mweight.getValue()!=null ){


            CharSequence w1  = mweight.getValue();
            CharSequence w2  = weight_float.getValue();

            String w3 = w1+"."+w2+"";


            total_weight_float.postValue(w3);


            if (total_weight_float == null) {
                total_weight_float = new MutableLiveData<>();
            }


            total_weight_float.postValue(w3);

        }




    }


    public LiveData<CharSequence> getmTotalWeight() {
        if (total_weight_float == null) {
            total_weight_float = new MutableLiveData<>();
        }
        return total_weight_float;
    }



    private MutableLiveData<CharSequence> weight_float;

    public LiveData<CharSequence> getmBirthWeightFloat() {
        if(weight_float == null){
            weight_float = new MutableLiveData<>();
        }
        return weight_float;
    }

    public void setmBirthWeightFloat(CharSequence birthWeight) {
        if(weight_float == null){
            weight_float = new MutableLiveData<>();
        }
        this.weight_float.postValue(birthWeight);
    }




    public MutableLiveData<CharSequence> getmBirthWeight() {
        return mweight;
    }

    public void setmBirthWeight(CharSequence birthWeight) {
        this.mweight.postValue(birthWeight);
    }






    public MutableLiveData<CharSequence> getmCaretakerName() {
        return mCaretakerName;
    }

    public void setmCaretakerName(CharSequence caretakerName) {
        this.mCaretakerName.postValue(caretakerName);
    }




    public MutableLiveData<CharSequence> getMdateOfBirth() {
        return mdateOfBirth;
    }

    public void setMdateOfBirth(CharSequence dateOfBirth) {
        this.mdateOfBirth.postValue(dateOfBirth);
    }




    public MutableLiveData<CharSequence> getmGender() {
        return mGender;
    }

    public void setmGender(CharSequence gender) {
        this.mGender.postValue(gender);
    }






    //----ERROR -----------------------------------------

    public MutableLiveData<String> getmBirthPlaceError() {
        return mBirthPlaceError;
    }

    public void setmBirthPlaceError(String birthPlaceError) {
        this.mBirthPlaceError.postValue(birthPlaceError);
    }

    public MutableLiveData<String> getmCaretakerError() {
        return mCaretakerError;
    }

    public void setmCaretakerError(String caretakerError) {
        this.mCaretakerError.postValue(caretakerError);
    }






    private MutableLiveData<Boolean> isName1Valid;
    public MutableLiveData<Boolean> getIsAddress1Valid() {

        return isName1Valid;
    }
    public void setIsName1Valid(boolean isaddressValid) {
        if(isName1Valid == null){
            isName1Valid = new MutableLiveData<>();
        }
        this.isName1Valid.postValue(isaddressValid);
    }



    private MutableLiveData<Boolean> isName2Valid;
    public MutableLiveData<Boolean> getIsName2Valid() {
        return isName2Valid;
    }
    public void setIsName2Valid(boolean isaddressValid) {
        if(isName2Valid == null){
            isName2Valid = new MutableLiveData<>();
        }
        this.isName2Valid.postValue(isaddressValid);
    }




    public boolean checkNameValid(){
        if(isName2Valid == null){
            isName2Valid = new MutableLiveData<>();
        }
        if(isName1Valid == null){
            isName1Valid = new MutableLiveData<>();
        }

        if(isName1Valid.getValue() != null && isName2Valid.getValue() != null){

            if(isName1Valid.getValue() && isName2Valid.getValue()){
                return true;
            }
        }
        return false;
    }





    boolean isBirthValid;
    public boolean checkBirth(){

        String errorMessage = "cannot be empty!";

        if(TextUtils.isEmpty(getMdateOfBirth().getValue())){

            isBirthValid = false;

        }


        else {

            isBirthValid = true;
        }


        return isBirthValid;

    }



    boolean isNamesValid;
    public boolean checkChildProfileNames(){


        String errorMessage = "cannot be empty!";

        if(TextUtils.isEmpty(getFirstName().getValue())){

            isNamesValid = false;
            setMfirstNameError("FirstName "+errorMessage);

        }
        else if(TextUtils.isEmpty(getLastName().getValue())){

            isNamesValid = false;
            setMlastNameError("LastName "+errorMessage);


        }
        else {

            isNamesValid = true;
        }


        return isNamesValid;

    }


    boolean isGenderValid;
    public boolean checkGender(){


        String errorMessage = "cannot be empty!";

        if(TextUtils.isEmpty(getmGender().getValue())){

            isGenderValid = false;


        }

        else{

            isGenderValid = true;
        }


        return isGenderValid;

    }



    boolean isIdValid;
    public boolean checkCaretakerId(){


        String errorMessage = "cannot be empty!";

        if(TextUtils.isEmpty(getmCaretakerId().getValue())){

            isIdValid = false;

            setmBirthPlaceError(" cId "+errorMessage);

        }

        else{

            isIdValid = true;
        }


        return isIdValid;

    }



    private void generateChildSchedule(Child child){

        Log.i("VModel", "generate child schedule called");

        //generate the child's vaccination schedule (habiba)

        GenerateChildSchedule.RequestValues requestValues = new GenerateChildSchedule.RequestValues(child);

        Log.i("VModel Request Birth", ""+child.getDateOfBirth());

        generateChildSchedule.execute(requestValues, new UseCase.OutputBoundary<GenerateChildSchedule.ResponseValue>() {
            @Override
            public void onSuccess(GenerateChildSchedule.ResponseValue result) {
                Log.i("VModel Schedule created", " date "+child.getRegistrationDate());

            //   String childName = result.getVaccinationCard().getVaccinationEvents().get(0).getChildName();
             //  String date = DateCalendarConverter.dateToStringWithoutTime(result.getVaccinationCard().getDateCreated());

              //  String a = "Thank you, "+childName+" registered successfully";
              //  String b = a+" on "+date;
               // child.setVaccinationEvents(result.getVaccinationCard());

                String childString =  MyDetailsTypeConverter.fromChildToString(child);
              //  setmScheduleGenerateResponse(child);

            }

            @Override
            public void onError(String e) {
                Log.i("VModel Sche not created", " date "+e);


               // setmScheduleGenerateResponse("error generating card "+e);



            }
        });


    }




    public void registerChild(Context context, AddChildController registerChildController){

        PreferenceHandler preferenceHandler = PreferenceHandler.getInstance(context);


        if(getmGender().getValue()!= null
                && getFirstName().getValue()!=null
                && getLastName().getValue()!=null
                && getmBirthWeight().getValue()!=null
                && getmBirthWeightFloat().getValue()!=null
                && getMdateOfBirth().getValue()!=null
                && getmGender().getValue() !=null
                && getmCaretakerId().getValue() != null
                ) {



            //NEW CODE
            //AddChildPresenterImpl presenter = new AddChildPresenterImpl(null);
            //GenerateChildSchedule generateChildSchedule = Injection.provideGenerateChildScheduleUseCase(Injection.provideApplication());

            registerChildController.registerChild(
                    getFirstName().getValue().toString(),
                    getLastName().getValue().toString(),
                    getmGender().getValue().toString(),
                    getmBirthWeight().getValue().toString(),
                    getmBirthWeightFloat().getValue().toString(),
                    getMdateOfBirth().getValue().toString(),
                    getmCaretakerId().getValue(),
                    preferenceHandler.getPref(PreferenceHandler.USER_HOSPITAL_ID)
            );
            //NEW CODE


            /*
            Child child = new Child();


            child.setFirstName(getFirstName().getValue().toString());
            child.setLastName(getLastName().getValue().toString());



            //10 21, 2018

            String datetoconvert = getMdateOfBirth().getValue().toString();
            String vals[] = datetoconvert.split("/");//month//day//year

            int month = Integer.parseInt(vals[0]);
            int day = Integer.parseInt(vals[1]);
            int year = Integer.parseInt(vals[2]);

            Calendar calendar = Calendar.getInstance();
            calendar.set(year,month,day);

            child.setDateOfBirth(DateCalendarConverter.dateToStringWithoutTime((calendar.getTime())));


            AgeCalculator.Age myAge = new AgeCalculator().calculateAge(DateCalendarConverter.stringToDateWithoutTime(child.getDateOfBirth()));
            String stringAge;
            if(myAge.getYears()==0 && myAge.getMonths() == 0){

              int days = myAge.getDays();

               stringAge = "" + days+" days";
            }else {

                stringAge = "" + Integer.toString(myAge.getYears()) + "-" + Integer.toString(myAge.getMonths());

            }

            child.setAge(stringAge);
            child.setHospitalId(preferenceHandler.getPref(PreferenceHandler.USER_HOSPITAL_ID));

            child.setGender(getmGender().getValue().toString());

            String w1 = getmBirthWeight().getValue().toString();
            String w2 = getmBirthWeightFloat().getValue().toString();
            String w = ""+w1+"."+w2;
            float myweight = Float.parseFloat(w);
            child.setWeight(myweight);
            child.setCaretakerId(getmCaretakerId().getValue());
            child.setRegistrationDate(DateCalendarConverter.dateToStringWithoutTime(new Date()));

            Log.i("Live data name", "" + getFirstName().getValue());

            Log.i("Live data obj birth", "" + getMdateOfBirth().getValue().toString());

            Log.i("child obj birth", "" + child.getDateOfBirth());


            RegisterChild.RequestValues requestValues = new RegisterChild.RequestValues(child);
            Log.i("child req obj birth", "" + requestValues.getmChild().getDateOfBirth());


            generateChildSchedule(child);
            */




        }
    }








}
