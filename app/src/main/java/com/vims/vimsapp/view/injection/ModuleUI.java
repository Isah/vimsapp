package com.vims.vimsapp.view.injection;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.LifecycleActivity;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.vims.vimsapp.addchild_controller.AddChildPresenter;
import com.vims.vimsapp.manage_order_controller.ManageOrderPresenter;
import com.vims.vimsapp.manage_order_controller.ViewOrderHistoryPresenter;
import com.vims.vimsapp.presenters.addchild_presenter.AddChildPresenterImpl;
import com.vims.vimsapp.presenters.addchild_presenter.AddChildViewModel;
import com.vims.vimsapp.presenters.makeorder_presenter.ManageOrderPresenterImpl;
import com.vims.vimsapp.presenters.makeorder_presenter.ManageOrderView;
import com.vims.vimsapp.presenters.makeorder_presenter.ManageOrderViewModel;
import com.vims.vimsapp.presenters.makeorder_presenter.ViewOrderHistoryPresenterImpl;
import com.vims.vimsapp.presenters.makeorder_presenter.ViewOrderHistoryViewModel;
import com.vims.vimsapp.view.patients.addchild.AddChildActivityWizardViewModel;
import com.vims.vimsapp.view.patients.addchild.AddChildActivityWizardViewModelFactory;


public class ModuleUI {


    /// PRESENTERS //////////////////////////////////////////////////////////////////
    public static AddChildPresenter provideAddChildPresenter(Fragment fragment) {
        return new AddChildPresenterImpl(providePreviewViewModel(fragment));
    }


    private static AddChildActivityWizardViewModelFactory provideViewModelFactory(Application application){

        return new AddChildActivityWizardViewModelFactory(Injection.provideRegisterChildUseCase(application), Injection.provideGenerateChildScheduleUseCase(application));
    }


    public static AddChildActivityWizardViewModel provideInteractionViewModel(FragmentActivity fragment){

        return  ViewModelProviders.of(fragment,provideViewModelFactory(fragment.getApplication())).get(AddChildActivityWizardViewModel.class);

    }

    public static AddChildViewModel providePreviewViewModel(Fragment fragment){
        //Im not injecting anything in the view model so no need of the factory
        return  ViewModelProviders.of(fragment).get(AddChildViewModel.class);

    }


    /////////// manage order /////////////////


    public static ManageOrderPresenter provideManageOrderPresenter(ManageOrderView manageOrderView){

        return new ManageOrderPresenterImpl(manageOrderView);
    }



    public static ViewOrderHistoryPresenter provideViewOrderHistoryPresenter(ViewOrderHistoryViewModel viewModel){

        return new ViewOrderHistoryPresenterImpl(viewModel);
    }







}
