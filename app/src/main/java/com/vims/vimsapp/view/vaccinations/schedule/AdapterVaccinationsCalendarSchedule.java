package com.vims.vimsapp.view.vaccinations.schedule;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vims.vimsapp.UseCase;

import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationStatus;
import com.vims.vimsapp.searchchild.ViewChildren;
import com.vims.vimsapp.recordvaccinations.RecordChildVaccination;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.vaccinations.CardHeader;

import java.util.Date;
import java.util.List;

import vimsapp.R;

public class AdapterVaccinationsCalendarSchedule extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    //the adapter needs its viewmodel

    private static volatile AdapterVaccinationsCalendarSchedule INSTANCE;

    private List<VaccinationEvent> vaccinationEvents;
    private RecordChildVaccination recordChildVaccination;


    private Application context;
    private Activity activity;
    private CardHeader cardHeader;



    private AdapterVaccinationsCalendarSchedule(CardHeader header , List<VaccinationEvent> vaccinationEvents, Application context, RecordChildVaccination recordChildVaccination) {
        this.context = context;
        this.cardHeader = header;
        this.vaccinationEvents = vaccinationEvents;
        this.recordChildVaccination = recordChildVaccination;
        //1. Animations bug
        setHasStableIds(true); //in constructor

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }




    public static void clearInstance() {
        INSTANCE = null;
    }

    public static AdapterVaccinationsCalendarSchedule getINSTANCE(CardHeader cardHeader, List<VaccinationEvent> vaccinationEvents, Application context, RecordChildVaccination recordChildVaccination) {
        if(INSTANCE == null){

            INSTANCE = new AdapterVaccinationsCalendarSchedule(cardHeader, vaccinationEvents, context, recordChildVaccination);
        }


        return INSTANCE;
    }

    public void replaceItems(List<VaccinationEvent> vaccinationEvents){
        this.vaccinationEvents = vaccinationEvents;

        //1. Animations bug
//        setHasStableIds(true); //in constructor

    }




    public Context getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }


    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        /*
        if(viewType == TYPE_HEADER) {
            View   v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vaccinations_card_header, parent, false);

            return  new VHHeader(v);


        }
        */


        if(viewType == TYPE_ITEM){


            View   v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vaccinations_card_horizontal_alt, parent, false);

            return new MyViewHolder(v);


        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");


    }


    private class VHHeader extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView txtTitle;
        TextView txtSubTitle;

        public VHHeader(View itemView) {
            super(itemView);
            this.txtTitle = itemView.findViewById(R.id.tvtitleEvents);
            this.txtSubTitle =  itemView.findViewById(R.id.tvSubtitleEvents);

            txtTitle.setTypeface(MyFonts.getTypeFace(context));
            txtSubTitle.setTypeface(MyFonts.getTypeFace(context));

        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition(); // gets item position

            // We can access the data within the views
            //  Toast.makeText(mContext, "you clicked me", Toast.LENGTH_SHORT).show();
            //  Intent intent = new Intent(v.getContext(), LoginActivity.class);
            // v.getContext().startActivity(intent);

            //  myBtnclickListener.onSignUpBtnClick(v, position); // call the onClick in the OnCardItemClickListener Interface i created
         //   dismissClickListner.onBtnDismiss(v,position);
           // onHeaderClickListener.onHeaderClick(v,position);


        }




    }



    //(2) implements the click listner
    public class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {
        public TextView title;
        public TextView subtitle;
        public TextView vaccineDescription;

        public TextView supposed_vaccination_date;
        //public CheckBox add_vaccination_checkbox;
        public TextView add_vaccination_checkbox;
        public TextView tvEventDay;
        public TextView tvEventMonth;
        public ImageView ivDropDown;



        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.tv_vaccination_name);
            subtitle = view.findViewById(R.id.tv_child_name);
            vaccineDescription =view.findViewById(R.id.vaccineDescriptionHowGiven);

            supposed_vaccination_date = view.findViewById(R.id.tv_time);
            add_vaccination_checkbox = view.findViewById(R.id.checkbox_action_add_vaccination);

            tvEventDay = view.findViewById(R.id.tv_vaccination_day);
            tvEventMonth = view.findViewById(R.id.tv_vaccination_month);

            ivDropDown = view.findViewById(R.id.iv_drop_down);


            title.setTypeface(MyFonts.getTypeFace(context));
            subtitle.setTypeface(MyFonts.getTypeFace(context));
            supposed_vaccination_date.setTypeface(MyFonts.getTypeFace(context));
            add_vaccination_checkbox.setTypeface(MyFonts.getTypeFace(context));

            tvEventMonth.setTypeface(MyFonts.getTypeFace(context));
            tvEventDay.setTypeface(MyFonts.getTypeFace(context));




            //(3) Bind the click listener
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition(); // gets item position

            VaccinationEvent vaccinationEvent = vaccinationEvents.get(position);


        //    myclickListener.onCaretakerClicked(v, position, vaccinationEvent); // call the onClick in the OnCardItemClickListener Interface i created




        }

    }


    int mExpandedPosition = -1;
    int previousExpandedPosition = -1;

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder general_holder, int position) {

        /*
        if(general_holder instanceof VHHeader)
        {

            VHHeader VHheader = (VHHeader)general_holder;

            VHheader.txtTitle.setText(cardHeader.getTitle());
            VHheader.txtSubTitle.setText(cardHeader.getSubTitle());

        }
        */
        if(general_holder instanceof AdapterVaccinationsCalendarSchedule.MyViewHolder ) {

            final VaccinationEvent vaccinationEvent = getVaccinationEvent(position);
            final AdapterVaccinationsCalendarSchedule.MyViewHolder holder = (AdapterVaccinationsCalendarSchedule.MyViewHolder ) general_holder;


            //String month = DateCalendarConverter.dateToMonth(mdy);
            // April 4, 2018

            String eDate = DateCalendarConverter.dateToStringWithoutTime(new Date(vaccinationEvent.getScheduledDate()));

            String[] dateArray = eDate.split(" ");

            String m = dateArray[0];
            String d = dateArray[1];
            //   String y = dateArray[2];

            if(m.length()> 3){

                m =  m.substring(0,3);
            }

            holder.title.setText(vaccinationEvent.getVaccineName());
            holder.subtitle.setText(vaccinationEvent.getChildName());
            holder.tvEventDay.setText(d);
            holder.tvEventMonth.setText(m);


            if(vaccinationEvent.getVaccinationStatus() == VaccinationStatus.NOT_DONE){

                holder.add_vaccination_checkbox.setSelected(false);
                //holder.add_vaccination_checkbox.setChecked(false);

                holder.add_vaccination_checkbox.setTextColor(context.getResources().getColor(R.color.colorAccent));
            }else if(vaccinationEvent.getVaccinationStatus() == VaccinationStatus.DONE){


                holder.add_vaccination_checkbox.setSelected(true);
               // holder.add_vaccination_checkbox.setChecked(true);
                holder.add_vaccination_checkbox.setEnabled(false);
                String complete = "COMPLETE";
                holder.add_vaccination_checkbox.setText(complete);


            }

            if(vaccinationEvent.getVaccinationDate() != null) {
                holder.supposed_vaccination_date.setText(vaccinationEvent.getVaccinationDate());
            }
            else{
                String vaccination ="Pending";
                holder.supposed_vaccination_date.setText(vaccination);
            }

            //solution 1
            boolean isExpanded = position == mExpandedPosition;

            holder.vaccineDescription.setVisibility(isExpanded?View.VISIBLE:View.GONE);
            holder.itemView.setActivated(isExpanded);
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);


            holder.ivDropDown.setImageDrawable(isExpanded?context.getResources().getDrawable(R.drawable.ic_expand_less_black_18dp):context.getResources().getDrawable(R.drawable.ic_expand_more_black_18dp));
            //  holder.ivDropDown.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_expand_more_black_24dp));
            //  holder.ivDropDown.setBackground(isExpanded?context.getResources().getDrawable(R.drawable.ic_expand_less_black_24dp):context.getResources().getDrawable(R.drawable.ic_expand_more_black_24dp));

            holder.ivDropDown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    //   onRecordVaccinationClicked.onCardExpandCLicked(v,position,mExpandedPosition,isExpanded);

                    mExpandedPosition = isExpanded ? -1:position;
                    //  TransitionManager.beginDelayedTransition(theView);
                    notifyItemChanged(position);

                }
            });


            //Drawable img = getContext().getResources().getDrawable( R.drawable.ic_perm_identity_black_24dp);
            //caretakerAddNameFragmentBinding.tvFirstnameCaretaker.setCompoundDrawablesWithIntrinsicBounds( getResources().getDrawable( R.drawable.ic_perm_identity_black_24dp), null, null, null);


            holder.add_vaccination_checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                 onRecordVaccinationClicked.onRecordVaccination(v,position,vaccinationEvent);



                }//end of onclick
            });



        }





        // holder.phoneno.setText(caretaker.getPhoneno1());
       // holder.district.setText(caretaker.getDistrict());
       // holder.noChildren.setText(noOfKids);

        // Picasso.with(mContext).load(album.getNoticeUrl()).fit().placeholder(R.drawable.challenge).into(holder.thumbnail);



        // int descLength = album.getDescription().length();
        // holder.itemView.setTag(descLength);


        /*

        //ONCLICK LISTNER FOR SHARE BUTTON

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(mContext, "Sharing.. "+album.getTitle(), Toast.LENGTH_SHORT).show();

            }
        });
       */

    }



    @Override
    public int getItemCount() {

        return vaccinationEvents.size();
    }


    //2. Animations bug
    @Override
    public long getItemId(int position) {
        return vaccinationEvents.get(position).hashCode(); //Any unique id
    }



    private VaccinationEvent getVaccinationEvent(int position)
    {
        return vaccinationEvents.get(position);
    }

    //    need to override this method
    @Override
    public int getItemViewType(int position) {

        return TYPE_ITEM;
    }


    private boolean isPositionHeader(int position)
    {
        return position == 0;
    }




    //interfaces
    private OnRecordVaccinationClicked onRecordVaccinationClicked;

    //someione who implements this interface ill send him a click trigger
    public interface OnRecordVaccinationClicked{

        void onRecordVaccination(View view, int position, VaccinationEvent vaccinationEvent);

    }

    public void setClickListner(OnRecordVaccinationClicked onRecordVaccinationClicked){
        this.onRecordVaccinationClicked = onRecordVaccinationClicked;
    }


    public void confirmVaccinationDialog(String vaccineName, String childName) {


       // Activity activity = (Activity) this.activity;

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.MyDialogTheme);

        String dialogTitle = "Confirm Recording Vaccination ?";
        String dialogDesc =  ""+vaccineName+" administered to "+childName +", the operation can not be undone";

        builder.setTitle(dialogTitle);
        builder.setMessage(dialogDesc);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {



                    }

                });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                    }
                });

        AlertDialog dialog = builder.create();


          //if using api >= 19 use TypeToast not type_sytem_alert

          //  dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);

            dialog.show();




    }


    /*
    int mStackLevel = 0;
    void showDialog() {
        mStackLevel++;

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        Activity activity = (Activity) this.activity;

        FragmentManager fragmentMgr = activity.getFragmentManager();
        FragmentTransaction ft = fragmentMgr.beginTransaction();
        Fragment prev = fragmentMgr.findFragmentByTag("dialog");



        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        ConfirmVaccinationDialogFrgment newFragment = ConfirmVaccinationDialogFrgment.newInstance(mStackLevel);
        newFragment.show(ft, "dialog");

    }
    */


    public static class GetChildAsyncTask extends AsyncTask<Void,Void,Child>{


        AdapterVaccinationsCalendarSchedule.MyViewHolder holder;
        VaccinationEvent vaccinationEvent;
        Child child;
        Application mContext;

       public GetChildAsyncTask(Application context, AdapterVaccinationsCalendarSchedule.MyViewHolder holder, VaccinationEvent vaccinationEvent) {
           super();
           this.holder = holder;
           this.vaccinationEvent = vaccinationEvent;
           mContext = context;
       }

       @Override
       protected void onPreExecute() {
           super.onPreExecute();
       }

       @Override
       protected Child doInBackground(Void... voids) {




           ViewChildren.RequestValues requestValues = new ViewChildren.RequestValues(vaccinationEvent.getChildId(), ViewChildren.RequestType.CHILD);


           Injection.provideReadChildrenUseCase(mContext).execute(requestValues, new UseCase.OutputBoundary<ViewChildren.ResponseValue>() {
               @Override
               public void onSuccess(ViewChildren.ResponseValue result) {


                 child = result.getChild();

               }

               @Override
               public void onError(String errorMessage) {

                   String er = "N/A";

                   holder.supposed_vaccination_date.setText(er);

               }
           });



           return child;


       }

       @Override
       protected void onPostExecute(Child child) {
           super.onPostExecute(child);

           if(child!=null) {

               holder.supposed_vaccination_date.setText(child.getDateOfBirth());
           }

       }

       @Override
       protected void onProgressUpdate(Void... values) {
           super.onProgressUpdate(values);
       }

       @Override
       protected void onCancelled(Child child) {
           super.onCancelled(child);
       }

       @Override
       protected void onCancelled() {
           super.onCancelled();
       }


   }







}