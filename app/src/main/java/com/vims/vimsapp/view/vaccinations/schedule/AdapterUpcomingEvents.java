package com.vims.vimsapp.view.vaccinations.schedule;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;


import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationStatus;
import com.vims.vimsapp.recordvaccinations.RecordChildVaccination;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsDataSource;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsRepository;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.ThemeManager;
import com.vims.vimsapp.utilities.calendar.MyCalendarDate;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.injection.ModuleRepository;
import com.vims.vimsapp.view.vaccinations.CardHeader;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import vimsapp.R;


public class AdapterUpcomingEvents extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable{


    //the adapter needs its viewmodel

    private static volatile AdapterUpcomingEvents INSTANCE;

    private List<VaccinationEvent> vaccinationEvents;
    private RecordChildVaccination recordChildVaccination;


    private Application context;
    private Activity activity;
    private CardHeader cardHeader;


    int sizeOfItems;


    public Map<String, VaccinationEvent> mappedEvents;

    private AdapterUpcomingEvents(CardHeader header , List<VaccinationEvent> vaccinationEvents, Application context, RecordChildVaccination recordChildVaccination) {
        this.context = context;
        this.cardHeader = header;

        ThemeManager.setContext(context);

        // Do in memory cache update to keep the app UI up to date
        if (mappedEvents== null) {
           mappedEvents = new LinkedHashMap<>();
        }

        /*
        String id = UUID.randomUUID().toString();
        for(VaccinationEvent vaccinationEvent: vaccinationEvents) {
            mappedEvents.put(id, vaccinationEvent);
        }
        */

        this.vaccinationEvents = vaccinationEvents;
        this.recordChildVaccination = recordChildVaccination;
        //1. Animations bug
       // setHasStableIds(true); //in constructor
        sizeOfItems = vaccinationEvents.size();


        setHasStableIds(true);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }




    private List<VaccinationEvent> vaccinationEventsListFiltered;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                Log.d("AdapterCaretakers","search filter"+charString);


                if (charString.isEmpty()) {
                    vaccinationEventsListFiltered = vaccinationEvents;
                } else {
                    List<VaccinationEvent> filteredList = new ArrayList<>();
                    for (VaccinationEvent row : vaccinationEvents) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getVaccineName().toLowerCase().contains(charString.toLowerCase()) || row.getChildName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                            Log.d("AdapterCaretakers","search found"+charString.toLowerCase());

                        }
                    }

                    vaccinationEventsListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = vaccinationEventsListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                Log.d("AdapterCaretakers","search filter results: "+filterResults.count);


                if(vaccinationEventsListFiltered.size() > 0) {

                    vaccinationEvents = (ArrayList<VaccinationEvent>) filterResults.values;
                    //replaceItems(caretakersListFiltered);
                    notifyDataSetChanged();
                }else{

                   searchInDatabase(charSequence);

                }

            }
        };
    }



    private void  searchInDatabase(CharSequence charSequence){

        SearchVaccinationsRepository patientsRepository = ModuleRepository.provideSearchVaccinationRepository(context);
        patientsRepository.searchVaccinationEvents(charSequence.toString(), new SearchVaccinationsDataSource.SearchEventCallback() {
            @Override
            public void onSearchEvents(List<VaccinationEvent> events) {

                vaccinationEvents = events;
                notifyDataSetChanged();
            }

            @Override
            public void onFailed(String error) {

                Toast.makeText(context,""+error,Toast.LENGTH_LONG).show();

            }
        });

    }



    /*

    public void updateVaccinationListItems(List<VaccinationEvent> employees) {
        final VaccinationsRecyclerViewDiffCallBack diffCallback = new VaccinationsRecyclerViewDiffCallBack(this.vaccinationEvents, employees);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        Log.d("AUE", "updated items: "+employees.size());


        this.vaccinationEvents.clear();
        this.vaccinationEvents.addAll(employees);
        sizeOfItems = employees.size();
        diffResult.dispatchUpdatesTo(this);


    }
    */


    public static void clearInstance() {
        INSTANCE = null;
    }

    public static AdapterUpcomingEvents getINSTANCE(CardHeader cardHeader, List<VaccinationEvent> vaccinationEvents, Application context, RecordChildVaccination recordChildVaccination) {
        if(INSTANCE == null){

            INSTANCE = new AdapterUpcomingEvents(cardHeader, vaccinationEvents, context, recordChildVaccination);
        }
        return INSTANCE;
    }

    public void replaceItems(List<VaccinationEvent> vaccinationEvents){
        this.vaccinationEvents = vaccinationEvents;

        //1. Animations bug
//        setHasStableIds(true); //in constructor
    }

    public void clearItems(){
        if(vaccinationEvents!=null)
        vaccinationEvents.clear();
        //1. Animations bug
//        setHasStableIds(true); //in constructor
    }



    //5 - 6
    public void removeItem(int position){

        if(!vaccinationEvents.isEmpty()) {

            vaccinationEvents.remove(position-1);

        }

    }


    public Context getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }




    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if(viewType == TYPE_HEADER) {
            View   v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vaccinations_card_header, parent, false);

            return  new VHHeader(v);


        }else if(viewType == TYPE_ITEM){


            View   v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vaccinations_card_horizontal_timeline, parent, false);

            return new MyViewHolder(v);


        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");


    }


    private class VHHeader extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView txtTitle;
        TextView txtSubTitle;

        TextView txtTitleImmunisationMonth;
        TextView txtSubtitleImmunisationCoverage;

        ProgressBar progressBarCircular;
        TextView tvImmunisationPercentage;
        BarChart barChart;
        Spinner monthSpinner;

        public VHHeader(View itemView) {
            super(itemView);
            this.txtTitle = itemView.findViewById(R.id.tvtitleEvents);
            this.txtSubTitle =  itemView.findViewById(R.id.tvSubtitleEvents);
           // this.txtTitleImmunisationMonth =  itemView.findViewById(R.id.tvTitleOverviewUpcomingVacs);
           // this.txtSubtitleImmunisationCoverage =  itemView.findViewById(R.id.tvSubtitleImmunisationCoverage);
            this.progressBarCircular = itemView.findViewById(R.id.cardCircularProgressbar);
            this.tvImmunisationPercentage = itemView.findViewById(R.id.tvImmunisationPercentage);
            this.barChart = itemView.findViewById(R.id.chart);
            txtTitle.setTypeface(MyFonts.getTypeFace(context));
            txtSubTitle.setTypeface(MyFonts.getTypeFace(context));
           // txtTitleImmunisationMonth.setTypeface(MyFonts.getTypeFace(context));
           // txtSubtitleImmunisationCoverage.setTypeface(MyFonts.getTypeFace(context));

         //  monthSpinner = itemView.findViewById(R.id.upcomingEventsSpinner);

        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition(); // gets item position

            //  We can access the data within the views
            //  Toast.makeText(mContext, "you clicked me", Toast.LENGTH_SHORT).show();
            //  Intent intent = new Intent(v.getContext(), LoginActivity.class);
            //  v.getContext().startActivity(intent);

            //  myBtnclickListener.onSignUpBtnClick(v, position); // call the onClick in the OnCardItemClickListener Interface i created
            //  dismissClickListner.onBtnDismiss(v,position);
            //  onHeaderClickListener.onHeaderClick(v,position);


        }




    }

    //(2) implements the click listner
    public class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {
        private TextView title;
        private TextView subtitle;
        private TextView tvEventYear;
        private ConstraintLayout vaccineDescription;

        private TextView vaccineDescProtects;
        private TextView vaccineDescMode;
        private TextView vaccineDescSite;

        private TextView vaccineDescProtectsLabel;
        private TextView vaccineDescModeLabel;
        private TextView vaccineDescSiteLabel;

        private TextView vaccination_date_days_togo;
       // private CheckBox add_vaccination_checkbox;
        private TextView add_vaccination_checkbox;
        private TextView tvEventDay;
        private TextView tvEventMonth;
        private ImageView ivDropDown;
        private ImageView ivProfile;


        private CardView cardViewItem;

        //public ConstraintLayout calendarLayout;

        public AppCompatImageView calendarLayout;


        public MyViewHolder(View view) {
            super(view);
            //(3) Bind the click listener
            view.setOnClickListener(this);
        }



        public void setData(VaccinationEvent vaccinationEvent, int position){

            //place data from the object onto the view
            //THE VIEW
            title = itemView.findViewById(R.id.tv_vaccination_name);
            subtitle = itemView.findViewById(R.id.tv_child_name);
            tvEventYear = itemView.findViewById(R.id.tv_vaccination_year);

            vaccineDescription = itemView.findViewById(R.id.vaccineDescription);
            vaccineDescSite = itemView.findViewById(R.id.vaccineSite);
            vaccineDescMode = itemView.findViewById(R.id.vaccineMode);
            vaccineDescProtects = itemView.findViewById(R.id.vaccineProtects);

            vaccineDescSiteLabel = itemView.findViewById(R.id.vaccineSiteLabel);
            vaccineDescModeLabel = itemView.findViewById(R.id.vaccineModeLabel);
            vaccineDescProtectsLabel = itemView.findViewById(R.id.vaccineProtectsLabel);

            vaccination_date_days_togo = itemView.findViewById(R.id.tv_time);
            add_vaccination_checkbox = itemView.findViewById(R.id.checkbox_action_add_vaccination);
            tvEventDay = itemView.findViewById(R.id.tv_vaccination_day);
            tvEventMonth = itemView.findViewById(R.id.tv_vaccination_month);
            ivDropDown = itemView.findViewById(R.id.iv_drop_down);

            cardViewItem = itemView.findViewById(R.id.card_view);
            calendarLayout = itemView.findViewById(R.id.calendarDayView);

            ivProfile = itemView.findViewById(R.id.imageTransit);



            cardViewItem.setCardBackgroundColor(context.getResources().getColor(R.color.grey_pallete_200c));


            //THE TYPEFACE
            title.setTypeface(MyFonts.getTypeFace(context));
            subtitle.setTypeface(MyFonts.getTypeFace(context));

            vaccineDescSite.setTypeface(MyFonts.getTypeFace(context));
            vaccineDescMode.setTypeface(MyFonts.getTypeFace(context));
            vaccineDescProtects.setTypeface(MyFonts.getTypeFace(context));

            vaccineDescSiteLabel.setTypeface(MyFonts.getTypeFace(context));
            vaccineDescModeLabel.setTypeface(MyFonts.getTypeFace(context));
            vaccineDescProtectsLabel.setTypeface(MyFonts.getTypeFace(context));

            vaccination_date_days_togo.setTypeface(MyFonts.getTypeFace(context));

            tvEventMonth.setTypeface(MyFonts.getTypeFace(context));
            tvEventDay.setTypeface(MyFonts.getTypeFace(context));
            tvEventYear.setTypeface(MyFonts.getTypeFace(context));



            //DRAWBLES
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
            if(AppCompatDelegate.isCompatVectorFromResourcesEnabled()) {
               // add_vaccination_checkbox.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.ic_add_vaccination_darktheme), null, null, null);
            }

            /*
            Bitmap myBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.baby_boy);
            RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(context.getResources(), myBitmap);
            drawable.setCircular(true);
            */

            //THE BINDING
            if(vaccinationEvent.getVaccinationStatus() == VaccinationStatus.NOT_DONE){

                vaccineDescProtects.setText(VaccineSpecHandler.getVaccineDesc(VaccineSpecHandler.SPEC_TYPE_PROTECTS,vaccinationEvent.getVaccineName()));
                vaccineDescMode.setText(VaccineSpecHandler.getVaccineDesc(VaccineSpecHandler.SPEC_TYPE_MODE,vaccinationEvent.getVaccineName()));
                vaccineDescSite.setText(VaccineSpecHandler.getVaccineDesc(VaccineSpecHandler.SPEC_TYPE_SITE,vaccinationEvent.getVaccineName()));


                Date dateScheduled = new Date(vaccinationEvent.getScheduledDate());


                long daysToGo =  DateCalendarConverter.calculateDifferenceInDates(dateScheduled, MyCalendarDate.getCurrentCalendarDate());

                String  daysToGoToString = ""+Long.toString(daysToGo)+"d";
                vaccination_date_days_togo.setText(daysToGoToString);
                //vaccination_date_days_togo.setTextColor(context.getResources().getColor(R.color.colorAccent));


                //String month = DateCalendarConverter.dateToMonth(mdy);
                // April 4, 2018
                String vDate = DateCalendarConverter.dateToStringWithoutTime(new Date(vaccinationEvent.getScheduledDate()));
                String[] dateArray = vDate.split(" ");

                String m = dateArray[0];
                String d = dateArray[1];
                String y = dateArray[2];

                if (m.length() > 3) {
                    m = m.substring(0, 3);
                }

                y = y.substring(2,4);

                String eventTitle = "Record "+vaccinationEvent.getVaccineName();
                String eventSubTile = "administer "+vaccinationEvent.getVaccineName();
                String eventDate = "Due on "+vaccinationEvent.getScheduledDate();
                String childName = "to "+vaccinationEvent.getChildName();


                title.setText(eventTitle);
                subtitle.setText(childName);
                tvEventDay.setText(d);
                tvEventMonth.setText(m);
                tvEventYear.setText(y);


                title.setEnabled(true);

                boolean isEventMissed = vaccinationEvent.getVaccinationStatus() == VaccinationStatus.NOT_DONE && dateScheduled.before(new Date());
                boolean isEventDue = vaccinationEvent.getVaccinationStatus() == VaccinationStatus.NOT_DONE && dateScheduled.after(new Date()) || dateScheduled.equals(new Date());
                boolean isEventDayReached =  daysToGo == 0;

                ///////////// calculated days due  /////////////////////////

                ////////////////////////////////////////////////////////////
                /////-linearLayout.setBackground(context.getResources().getDrawable(ThemeManager.getBackgroundDrawableUpcoming()));
                //EVENT DAY TODAY///////////////////////////////////////////

                if(isEventDayReached){
                    daysToGoToString = "Due Today";
                    vaccination_date_days_togo.setText(daysToGoToString);

                   vaccination_date_days_togo.setTextColor(context.getResources().getColor(R.color.brand_green_50));

                   ivProfile.setBackground(ThemeManager.getBackgroundDrawableEvents(VaccinationStatus.DUE));

                    /*
                    tvEventDay.setTextColor(context.getResources().getColor(R.color.brand_green_30));
                    tvEventMonth.setTextColor(context.getResources().getColor(R.color.brand_green_30));
                     */
                    calendarLayout.setBackground(ThemeManager.getBackgroundDrawable(VaccinationStatus.DUE));



                }
                else if(isEventMissed){

                    ivProfile.setBackground(ThemeManager.getBackgroundDrawableEvents(VaccinationStatus.FAILED));


                    ///////////// calculated days overdue  ///////////////////////
                    long missedByNo = DateCalendarConverter.calculateDifferenceInDates(dateScheduled, MyCalendarDate.getCurrentCalendarDate());
                    String missedDays[] = Long.toString(missedByNo).split("-");

                    String  missedNo = "";
                    if(missedByNo < 0) {
                        missedNo = missedDays[1] + " d overdue";
                    }

                    vaccination_date_days_togo.setText(missedNo);
                    vaccination_date_days_togo.setTextColor(context.getResources().getColor(R.color.errorColorDarkThemeLight));

                    calendarLayout.setBackground(ThemeManager.getBackgroundDrawable(VaccinationStatus.FAILED));

                    ////////////////////////////////////////////////////////////
                }
                else {
                   // add_vaccination_checkbox.setEnabled(false);
                   // linearLayout.setEnabled(false);
                  //  title.setTextColor(context.getResources().getColor(ThemeManager.getTextColorAdministerBtn()));
                   // subtitle.setTextColor(context.getResources().getColor(ThemeManager.getTextColorAdministerBtn()));
                  //  administerBtn.setEnabled(false);

                    calendarLayout.setBackground(ThemeManager.getBackgroundDrawable());
                   // calendarLayout.setBackground(ThemeManager.getBackgroundDrawable());

                   ivProfile.setBackground(ThemeManager.getBackgroundDrawableEvents(VaccinationStatus.NOT_DONE));
                   // tvEventDay.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    //tvEventMonth.setTextColor(context.getResources().getColor(R.color.colorPrimary));

                }

                //* TO BE REMOVED AFTER PROTOTYPE
                /*
                if(isEventMissed){
                    ///////////// calculated days overdue  ///////////////////////
                    Calendar calScheduled  = DateCalendarConverter.dateToCalendar(dateScheduled);
                    Calendar calToday  = DateCalendarConverter.dateToCalendar(new Date());

                    int missedByNo = calToday.get(Calendar.DAY_OF_YEAR) - calScheduled.get(Calendar.DAY_OF_YEAR);

                    String  missedNo = Integer.toString(missedByNo)+" days overdue";
                    supposed_vaccination_date.setText(missedNo);
                    supposed_vaccination_date.setTextColor(context.getResources().getColor(R.color.textColorSecondary));
                    //add_vaccination_checkbox.setTextColor(context.getResources().getColor(R.color.colorAccent));
                    ////////////////////////////////////////////////////////////

                    //linearLayout.setBackgroundColor(context.getResources().getColor(R.color.errorColor));
                    linearLayout.setBackground(context.getResources().getDrawable(R.drawable.shape_roundeded_rect_cardevents_missed));
                }else if(isEventDue){

                    ///////////// calculated days due  ///////////////////////

                    int daysToGo1 = calculatedDaysToGo(dateScheduled);

                    String  daysToGoToString1 = Integer.toString(daysToGo1)+" days to go";
                    supposed_vaccination_date.setText(daysToGoToString1);
                    ////////////////////////////////////////////////////////////

                   // linearLayout.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));

                    linearLayout.setBackground(context.getResources().getDrawable(R.drawable.shape_roundeded_rect_cardevents_upcoming));
                }
                */
                //*/  ///////////////////////////////////////////////////////////////////////////////////




                /*
                if (!scheduledDate.equals(dateToDay)) {
                    add_vaccination_checkbox.setEnabled(false);
                    //add_vaccination_checkbox.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                   // add_vaccination_checkbox.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.ic_check_circle_pending_24dp),null,null,null);
                    float a = 0.2f;
                    add_vaccination_checkbox.setAlpha(a);
                    String text = "vaccinate";
                    add_vaccination_checkbox.setText(text);
                }
                */

                //solution 1
                boolean isExpanded = position == mExpandedPosition;

                vaccineDescription.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
                itemView.setActivated(isExpanded);
                AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

                ivDropDown.setImageDrawable(isExpanded ? context.getResources().getDrawable(R.drawable.ic_expand_less_black_18dp) : context.getResources().getDrawable(R.drawable.ic_expand_more_black_18dp));

                //holder.ivDropDown.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_expand_more_black_24dp));
                //holder.ivDropDown.setBackground(isExpanded?context.getResources().getDrawable(R.drawable.ic_expand_less_black_24dp):context.getResources().getDrawable(R.drawable.ic_expand_more_black_24dp));

                ivDropDown.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //   onCardExpandClickedListner.onCardExpandCLicked(v,position,mExpandedPosition,isExpanded);
                        mExpandedPosition = isExpanded ? -1 : position;
                        //  TransitionManager.beginDelayedTransition(theView);
                        notifyItemChanged(position);
                    }
                });


                //Drawable img = getContext().getResources().getDrawable( R.drawable.ic_perm_identity_black_24dp);
                //caretakerAddNameFragmentBinding.tvFirstnameCaretaker.setCompoundDrawablesWithIntrinsicBounds( getResources().getDrawable( R.drawable.ic_perm_identity_black_24dp), null, null, null);
                cardViewItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        //title.setEnabled(false);
                       // add_vaccination_checkbox.setChecked(false);

                        int position = getAdapterPosition(); // gets item position

                        myclickListener.onVaccinationClicked(calendarLayout, position, vaccinationEvent);

                    }//end of onclick
                });


            }

        }




        int daysToGo = 0;
        private int calculatedDaysToGo(Date scheduledDate){

            Calendar calScheduled  = DateCalendarConverter.dateToCalendar(scheduledDate);
            Calendar calToday  = DateCalendarConverter.dateToCalendar(new Date());

            daysToGo =  calScheduled.get(Calendar.DAY_OF_YEAR) - calToday.get(Calendar.DAY_OF_YEAR);

            return daysToGo;
        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition(); // gets item position

           // VaccinationEvent vaccinationEvent = vaccinationEvents.get(position);
           // myclickListener.onCaretakerClicked(v, position, vaccinationEvent); // call the onClick in the OnCardItemClickListener Interface i created
        }

    }





    int mExpandedPosition = -1;
    int previousExpandedPosition = -1;




    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder general_holder, int position) {

        if(general_holder instanceof VHHeader)
        {

           // String cardSubTitle = "upcoming "+(getItemCount()-1);

            VHHeader VHheader = (VHHeader) general_holder;

      //      startCircularProgress(VHheader,cardHeader.getImmunisationCoveragePercentage());


            //////////////setup header///////////////////////////////////////////

            String year = DateCalendarConverter.dateToStringGetYear(new Date());
            int upcoming = getItemCount()-1;

            String day = "OnHold";
            if(getItemCount()>1) {

              //  Date dateScheduled = DateCalendarConverter.stringToDateWithoutTime(vaccinationEvents.get(0).getScheduledDate());

              // day = DateCalendarConverter.dateToStringWithoutTimeAndDay(dateScheduled);
            }
            String title  = day;
            String subTitle  = "total: "+upcoming;

            VHheader.txtTitle.setText(title);
            VHheader.txtSubTitle.setText(subTitle);

        }
        else if(general_holder instanceof AdapterUpcomingEvents.MyViewHolder ) {

            final AdapterUpcomingEvents.MyViewHolder holder = (AdapterUpcomingEvents.MyViewHolder) general_holder;
            final VaccinationEvent vaccinationEvent = getVaccinationEvent(position);


            holder.setData(vaccinationEvent,position);



        }


        // holder.phoneno.setText(caretaker.getPhoneno1());
        // holder.district.setText(caretaker.getDistrict());
        // holder.noChildren.setText(noOfKids);
        // Picasso.with(mContext).load(album.getNoticeUrl()).fit().placeholder(R.drawable.challenge).into(holder.thumbnail);

        // int descLength = album.getDescription().length();
        // holder.itemView.setTag(descLength);

        /*

        //ONCLICK LISTNER FOR SHARE BUTTON

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText
                (mContext, "Sharing.. "+album.getTitle(), Toast.LENGTH_SHORT).show();

            }
        });
       */

    }



    @Override
    public int getItemCount() {
      //  vaccinationEvents.size()+1;
        return vaccinationEvents.size()+1;
    }

    /*
    //2. Animations bug
    @Override
    public long getItemId(int position) {
        return vaccinationEvents.get(position+1).hashCode(); //Any unique id
    }
    */


    private VaccinationEvent getVaccinationEvent(int position)
    {
        return vaccinationEvents.get(position-1);
    }

    //    need to override this method
    @Override
    public int getItemViewType(int position) {
        if(isPositionHeader(position))
            return TYPE_HEADER;
        return TYPE_ITEM;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private boolean isPositionHeader(int position)
    {
        return position == 0;
    }




    //(1) Declare Interface
    protected AdapterUpcomingEvents.OnVaccinationCLickLIstner myclickListener;  //our interace
    // private WebView webView;


    //interfaces
    public void setClickListener(AdapterUpcomingEvents.OnVaccinationCLickLIstner onCardItemClickListener) {
        this.myclickListener = onCardItemClickListener;
    }


    public interface OnVaccinationCLickLIstner {

        void onVaccinationClicked(View view, int position, VaccinationEvent caretaker);
    }


    ////////////////// SPINNER CLICK EVENTS /////////////////////////////////////

    //Interface definition
    private OnSpinnerClickedListner onSpinnerClickedListner;
    public interface OnSpinnerClickedListner{

        void onSpinnerClicked(String item);

    }

    //Set methhod
    public void setSpinnerClickListner(OnSpinnerClickedListner onSpinnerClickedListner){
        this.onSpinnerClickedListner = onSpinnerClickedListner;
    }

    /////////////////////////////////////////////////////////////////////////////////





    private OnCardExpandClickedListner onCardExpandClickedListner;

    //someione who implements this interface ill send him a click trigger
    public interface OnCardExpandClickedListner{

        void onCardExpandCLicked(View view, int position, int mExpandedPostn, boolean isExpanded);

    }

    public void setClickListner(OnCardExpandClickedListner onCardExpandClickedListner){
        this.onCardExpandClickedListner = onCardExpandClickedListner;
    }


    public void confirmVaccinationDialog(String vaccineName, String childName) {


       // Activity activity = (Activity) this.activity;

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.MyDialogTheme);

        String dialogTitle = "Confirm Recording Vaccination ?";
        String dialogDesc =  ""+vaccineName+" administered to "+childName +", the operation can not be undone";

        builder.setTitle(dialogTitle);
        builder.setMessage(dialogDesc);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {



                    }

                });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                    }
                });

        AlertDialog dialog = builder.create();


          //if using api >= 19 use TypeToast not type_sytem_alert

          //  dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);

            dialog.show();




    }


    /*
    int mStackLevel = 0;
    void showDialog() {
        mStackLevel++;

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        Activity activity = (Activity) this.activity;

        FragmentManager fragmentMgr = activity.getFragmentManager();
        FragmentTransaction ft = fragmentMgr.beginTransaction();
        Fragment prev = fragmentMgr.findFragmentByTag("dialog");



        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        ConfirmVaccinationDialogFrgment newFragment = ConfirmVaccinationDialogFrgment.newInstance(mStackLevel);
        newFragment.show(ft, "dialog");

    }
   */







}