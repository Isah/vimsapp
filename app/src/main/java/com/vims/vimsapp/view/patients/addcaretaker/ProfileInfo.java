package com.vims.vimsapp.view.patients.addcaretaker;

/**
 * Created by root on 6/25/18.
 */

public class ProfileInfo {

    private String Title;
    private String subtitle;
    private int avator;

    public int getAvator() {
        return avator;
    }

    public void setAvator(int avator) {
        this.avator = avator;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }
}
