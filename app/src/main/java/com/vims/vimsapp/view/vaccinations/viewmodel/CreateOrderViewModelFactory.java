package com.vims.vimsapp.view.vaccinations.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.vims.vimsapp.manageorders.RequestOrder;


/**
 * Created by root on 6/3/18.
 */
public class CreateOrderViewModelFactory implements ViewModelProvider.Factory{


    private final RequestOrder requestOrder;


    public CreateOrderViewModelFactory(RequestOrder requestOrder) {
      this.requestOrder = requestOrder;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if(modelClass.isAssignableFrom(ViewScheduleUpcomingFragmentViewModel.class)){

            return (T) new CreateOrderViewModel(requestOrder);

        }
        else{

            throw new IllegalArgumentException("View Model not found");
        }

    }




}
