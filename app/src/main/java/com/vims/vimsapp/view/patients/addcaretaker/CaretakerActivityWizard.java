package com.vims.vimsapp.view.patients.addcaretaker;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.utilities.ThemeManager;
import com.vims.vimsapp.view.injection.Injection;

import vimsapp.R;
import vimsapp.databinding.PatientsActivityAddCaretakerBinding;

import static com.vims.vimsapp.utilities.ScreenManager.setScreenOrientation;


public class CaretakerActivityWizard extends AppCompatActivity implements StepperLayout.StepperListener{


   // FragmentAddCaretakerBioViewModel addChildActivityWizardViewModel;
    //binding name with view
    PatientsActivityAddCaretakerBinding registerformBinding;
    CaretakerActivityViewModelFactory caretakerActivityViewModelFactory;
    CaretakerActivityWizardViewModel caretakerActivityWizardViewModel;

    private StepperLayout mStepperLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        registerformBinding =   DataBindingUtil.setContentView(this, R.layout.patients_activity_add_caretaker);


        caretakerActivityViewModelFactory = new CaretakerActivityViewModelFactory(Injection.provideRegisterCaretakerUseCase(getApplication()));

        caretakerActivityWizardViewModel =  ViewModelProviders.of(this, caretakerActivityViewModelFactory).get(CaretakerActivityWizardViewModel.class);
        //setUpActionBar();

        setScreenOrientation(this);

        //setContentView(R.layout.patients_activity_add_caretaker);
       // Toolbar toolbar = registerformBinding.toolbar;
       // setSupportActionBar(toolbar);


      //addChildActivityWizardViewModel =  ViewModelProviders.of(this).get(FragmentAddCaretakerBioViewModel.class);

       setUpActionBar();

        mStepperLayout = findViewById(R.id.stepperLayout);
        mStepperLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mStepperLayout.setTabNavigationEnabled(true);
        mStepperLayout.setAdapter(new AdapterCaretakerStepper(getSupportFragmentManager(), this));


        registerformBinding.stepperLayout.setNextButtonColor(getResources().getColor(R.color.colorPrimary));
        registerformBinding.stepperLayout.setBackButtonColor(getResources().getColor(R.color.colorPrimary));
        registerformBinding.stepperLayout.setCompleteButtonColor(getResources().getColor(R.color.colorPrimary));


        caretakerActivityWizardViewModel.setmIsNextButtonEnabled(true);
        //observerNextButton();

        observeSaving();


    }

    Toolbar toolbar;

    private void setUpActionBar(){

        toolbar = registerformBinding.toolbar;
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Add Caretaker");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        }
        // setActionBar(toolbar);
          //we replace our actionbar with our toolbar
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);

    
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }




    @Override
    public void onCompleted(View completeButton) {




    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {



    }

    @Override
    public void onReturn() {
    finish();
    }



    public void observeSaving(){

        Observer<String> responseObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {


                registerformBinding.stepperLayout.setTabNavigationEnabled(false);




                registerformBinding.stepperLayout.setBackButtonEnabled(false);
                registerformBinding.stepperLayout.setCompleteButtonEnabled(false);

                registerformBinding.stepperLayout.setBackButtonColor(getResources().getColor(ThemeManager.getWizardButtonColor()));
                registerformBinding.stepperLayout.setCompleteButtonColor(getResources().getColor(ThemeManager.getWizardButtonColor()));

            }};

        caretakerActivityWizardViewModel.getResponse().observe(this, responseObserver);


    }


    public void observerNextButton(){

        Observer<Boolean> nextButtonObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null) {
                    registerformBinding.stepperLayout.setNextButtonEnabled(aBoolean);
                    if(aBoolean){

                        registerformBinding.stepperLayout.setNextButtonColor(getResources().getColor(R.color.colorPrimary));
                        registerformBinding.stepperLayout.setBackButtonColor(getResources().getColor(R.color.textColorPrimary));

                    }else{

                        registerformBinding.stepperLayout.setNextButtonColor(getResources().getColor(R.color.grey_500));
                    }

                }

            }
        };


         caretakerActivityWizardViewModel.getmIsNextButtonEnabled().observe(this,nextButtonObserver);

    }
}
