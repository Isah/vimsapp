package com.vims.vimsapp.view.vaccinations.schedule;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;

import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationStatus;
import com.vims.vimsapp.recordvaccinations.RecordChildVaccination;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.ThemeManager;
import com.vims.vimsapp.utilities.calendar.MyCalendarDate;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.vaccinations.CardHeader;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import vimsapp.R;

public class AdapterMissedEvents extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    //the adapter needs its viewmodel
    private static volatile AdapterMissedEvents INSTANCE;

    private List<VaccinationEvent> vaccinationEvents;
    private RecordChildVaccination recordChildVaccination;


    private Application context;
    private Activity activity;
    private CardHeader cardHeader;


    int sizeOfItems;


    public Map<String, VaccinationEvent> mappedEvents;


    private AdapterMissedEvents(List<VaccinationEvent> vaccinationEvents, Application context, RecordChildVaccination recordChildVaccination) {
        this.context = context;

        // Do in memory cache update to keep the app UI up to date
        if (mappedEvents== null) {
           mappedEvents = new LinkedHashMap<>();
        }

        /*
        String id = UUID.randomUUID().toString();
        for(VaccinationEvent vaccinationEvent: vaccinationEvents) {
            mappedEvents.put(id, vaccinationEvent);
        }
        */


        this.vaccinationEvents = vaccinationEvents;
        this.recordChildVaccination = recordChildVaccination;
        //1. Animations bug
       // setHasStableIds(true); //in constructor
        sizeOfItems = vaccinationEvents.size();


        //setHasStableIds(true);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }



    public static void clearInstance() {
        INSTANCE = null;
    }

    public static AdapterMissedEvents getINSTANCE(List<VaccinationEvent> vaccinationEvents, Application context, RecordChildVaccination recordChildVaccination) {
        if(INSTANCE == null){

            INSTANCE = new AdapterMissedEvents(vaccinationEvents, context, recordChildVaccination);
        }
        return INSTANCE;
    }

    public void replaceItems(List<VaccinationEvent> vaccinationEvents){
        this.vaccinationEvents = vaccinationEvents;

        //1. Animations bug
        //setHasStableIds(true); //in constructor

    }


    public void removeItem(int position){

        Log.d("AdapterMissed",""+position);

        if(!vaccinationEvents.isEmpty()) {
            vaccinationEvents.remove(position);

        }

    }




    public Context getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }




    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*
        if(viewType == TYPE_HEADER) {
            View   v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vaccinations_card_header, parent, false);
            return  new VHHeader(v);
        }
        */
      if(viewType == TYPE_ITEM){


            View   v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vaccinations_card_horizontal_timeline, parent, false);

            return new MyViewHolder(v);


        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");


    }


    private class VHHeader extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView txtTitle;
        TextView txtSubTitle;
        TextView txtTitleImmunisationMonth;
        TextView txtSubtitleImmunisationCoverage;
        ProgressBar progressBarCircular;
        TextView tvImmunisationPercentage;
        BarChart barChart;

        public VHHeader(View itemView) {
            super(itemView);
            this.txtTitle = itemView.findViewById(R.id.tvtitleEvents);
            this.txtSubTitle =  itemView.findViewById(R.id.tvSubtitleEvents);
           // this.txtTitleImmunisationMonth =  itemView.findViewById(R.id.tvTitleOverviewUpcomingVacs);
           // this.txtSubtitleImmunisationCoverage =  itemView.findViewById(R.id.tvSubtitleImmunisationCoverage);
            this.progressBarCircular = itemView.findViewById(R.id.cardCircularProgressbar);
            this.tvImmunisationPercentage = itemView.findViewById(R.id.tvImmunisationPercentage);
            this.barChart = itemView.findViewById(R.id.chart);
            txtTitle.setTypeface(MyFonts.getTypeFace(context));
            txtSubTitle.setTypeface(MyFonts.getTypeFace(context));
          //  txtTitleImmunisationMonth.setTypeface(MyFonts.getTypeFace(context));
           // txtSubtitleImmunisationCoverage.setTypeface(MyFonts.getTypeFace(context));

        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition(); // gets item position

            // We can access the data within the views
            //  Toast.makeText(mContext, "you clicked me", Toast.LENGTH_SHORT).show();
            //  Intent intent = new Intent(v.getContext(), LoginActivity.class);
            // v.getContext().startActivity(intent);

            //  myBtnclickListener.onSignUpBtnClick(v, position); // call the onClick in the OnCardItemClickListener Interface i created
            //   dismissClickListner.onBtnDismiss(v,position);
            // onHeaderClickListener.onHeaderClick(v,position);


        }




    }

    //(2) implements the click listner
    public class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {
        private TextView title;
        private TextView subtitle;

        private ConstraintLayout vaccineDescription;
        private TextView vaccineDescProtects;
        private TextView vaccineDescMode;
        private TextView vaccineDescSite;

        private TextView vaccineDescProtectsLabel;
        private TextView vaccineDescModeLabel;
        private TextView vaccineDescSiteLabel;


        private TextView daystoGoLabel;

        private TextView supposed_vaccination_date;
        //private CheckBox add_vaccination_checkbox;
        private TextView add_vaccination_checkbox;
        private TextView tvEventDay;
        private TextView tvEventMonth;
        private TextView tvEventYear;
        private ImageView ivDropDown;



        private CardView cardViewItem;

        //private ConstraintLayout calendarLayout;
        private AppCompatImageView calendarLayout;


        public MyViewHolder(View view) {
            super(view);
            //(3) Bind the click listener
            view.setOnClickListener(this);
        }


        public void setData(VaccinationEvent vaccinationEvent, int position){

            //place data from the object onto the view

            //THE VIEW
            title = itemView.findViewById(R.id.tv_vaccination_name);
            subtitle = itemView.findViewById(R.id.tv_child_name);

            vaccineDescription = itemView.findViewById(R.id.vaccineDescription);
            vaccineDescSite = itemView.findViewById(R.id.vaccineSite);
            vaccineDescMode = itemView.findViewById(R.id.vaccineMode);
            vaccineDescProtects = itemView.findViewById(R.id.vaccineProtects);

            vaccineDescSiteLabel = itemView.findViewById(R.id.vaccineSiteLabel);
            vaccineDescModeLabel = itemView.findViewById(R.id.vaccineModeLabel);
            vaccineDescProtectsLabel = itemView.findViewById(R.id.vaccineProtectsLabel);



            supposed_vaccination_date = itemView.findViewById(R.id.tv_time);

            tvEventDay = itemView.findViewById(R.id.tv_vaccination_day);
            tvEventMonth = itemView.findViewById(R.id.tv_vaccination_month);
            tvEventYear = itemView.findViewById(R.id.tv_vaccination_year);
            ivDropDown = itemView.findViewById(R.id.iv_drop_down);

            calendarLayout = itemView.findViewById(R.id.calendarDayView);
            cardViewItem = itemView.findViewById(R.id.card_view);

         //   daystoGoLabel = itemView.findViewById(R.id.dateLable);

            cardViewItem.setCardBackgroundColor(context.getResources().getColor(R.color.grey_pallete_200c));




            //THE TYPEFACE
            title.setTypeface(MyFonts.getTypeFace(context));
            subtitle.setTypeface(MyFonts.getTypeFace(context));
            supposed_vaccination_date.setTypeface(MyFonts.getTypeFace(context));


            vaccineDescSite.setTypeface(MyFonts.getTypeFace(context));
            vaccineDescMode.setTypeface(MyFonts.getTypeFace(context));
            vaccineDescProtects.setTypeface(MyFonts.getTypeFace(context));


            vaccineDescSiteLabel.setTypeface(MyFonts.getTypeFace(context));
            vaccineDescModeLabel.setTypeface(MyFonts.getTypeFace(context));
            vaccineDescProtectsLabel.setTypeface(MyFonts.getTypeFace(context));



            tvEventMonth.setTypeface(MyFonts.getTypeFace(context));
            tvEventDay.setTypeface(MyFonts.getTypeFace(context));
            tvEventYear.setTypeface(MyFonts.getTypeFace(context));


            //THE BINDING

            if(vaccinationEvent.getVaccinationStatus() == VaccinationStatus.NOT_DONE){


                vaccineDescProtects.setText(VaccineSpecHandler.getVaccineDesc(VaccineSpecHandler.SPEC_TYPE_PROTECTS,vaccinationEvent.getVaccineName()));
                vaccineDescMode.setText(VaccineSpecHandler.getVaccineDesc(VaccineSpecHandler.SPEC_TYPE_MODE,vaccinationEvent.getVaccineName()));
                vaccineDescSite.setText(VaccineSpecHandler.getVaccineDesc(VaccineSpecHandler.SPEC_TYPE_SITE,vaccinationEvent.getVaccineName()));




                //String month = DateCalendarConverter.dateToMonth(mdy);
                // April 4, 2018

                /*
                String[] dateArray = vaccinationEvent.getScheduledDate().split(" ");

                String m = dateArray[0];
                String d = dateArray[1];
                String y = dateArray[2];

                if (m.length() > 3) {

                    m = m.substring(0, 3);
                }
                */

                String vDate = DateCalendarConverter.dateToStringWithoutTime(new Date(vaccinationEvent.getScheduledDate()));
                String[] dateArray = vDate.split(" ");

                String m = dateArray[0];
                String d = dateArray[1];
                String y = dateArray[2];

                if (m.length() > 3) {

                    m = m.substring(0, 3);
                }

                String eventTitle = "record "+vaccinationEvent.getVaccineName();
                String eventSubTile = "administer "+vaccinationEvent.getVaccineName();
                String eventDate = "Due on "+vaccinationEvent.getScheduledDate();


                title.setText(eventTitle);
                subtitle.setText(vaccinationEvent.getChildName());

               //  tvEventDay.setTextColor(context.getResources().getColor(R.color.errorColorDarkThemeLight));
               //  tvEventMonth.setTextColor(context.getResources().getColor(R.color.errorColorDarkThemeLight));

                tvEventDay.setText(d);
                tvEventMonth.setText(m);
                tvEventYear.setText(y);


                    ///////////// calculated days overdue  ///////////////////////
                    Date dateScheduled = new Date(vaccinationEvent.getScheduledDate());


                    long missedByNo = DateCalendarConverter.calculateDifferenceInDates(dateScheduled, MyCalendarDate.getCurrentCalendarDate());
                    String missedDays[] = Long.toString(missedByNo).split("-");

                    String  missedNo = "";
                    if(missedByNo < 0) {
                        missedNo = missedDays[1] + " d overdue";
                    }


                    supposed_vaccination_date.setText(missedNo);
                    supposed_vaccination_date.setTextColor(context.getResources().getColor(R.color.errorColorDarkThemeLight));


                calendarLayout.setBackground(ThemeManager.getBackgroundDrawable(VaccinationStatus.FAILED));

                    ////////////////////////////////////////////////////////////



                /*
                if (!scheduledDate.equals(dateToDay)) {
                    add_vaccination_checkbox.setEnabled(false);
                    //add_vaccination_checkbox.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                   // add_vaccination_checkbox.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.ic_check_circle_pending_24dp),null,null,null);

                    float a = 0.2f;
                    add_vaccination_checkbox.setAlpha(a);
                    String text = "vaccinate";
                    add_vaccination_checkbox.setText(text);
                }
                */

                //solution 1
                boolean isExpanded = position == mExpandedPosition;

                vaccineDescription.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
                itemView.setActivated(isExpanded);
                AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);


                ivDropDown.setImageDrawable(isExpanded ? context.getResources().getDrawable(R.drawable.ic_expand_less_black_18dp) : context.getResources().getDrawable(R.drawable.ic_expand_more_black_18dp));
                //  holder.ivDropDown.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_expand_more_black_24dp));
                //  holder.ivDropDown.setBackground(isExpanded?context.getResources().getDrawable(R.drawable.ic_expand_less_black_24dp):context.getResources().getDrawable(R.drawable.ic_expand_more_black_24dp));


                ivDropDown.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //   onCardExpandClickedListner.onCardExpandCLicked(v,position,mExpandedPosition,isExpanded);

                        mExpandedPosition = isExpanded ? -1 : position;
                        //  TransitionManager.beginDelayedTransition(theView);
                        notifyItemChanged(position);
                    }
                });



                //Drawable img = getContext().getResources().getDrawable( R.drawable.ic_perm_identity_black_24dp);
                //caretakerAddNameFragmentBinding.tvFirstnameCaretaker.setCompoundDrawablesWithIntrinsicBounds( getResources().getDrawable( R.drawable.ic_perm_identity_black_24dp), null, null, null);


                cardViewItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int position = getAdapterPosition(); // gets item position

                        myclickListener.onVaccinationClicked(calendarLayout, position, vaccinationEvent);


                    }//end of onclick
                });


            }




        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition(); // gets item position

           // VaccinationEvent vaccinationEvent = vaccinationEvents.get(position);
           // myclickListener.onCaretakerClicked(v, position, vaccinationEvent); // call the onClick in the OnCardItemClickListener Interface i created
        }

    }


    int mExpandedPosition = -1;
    int previousExpandedPosition = -1;
    int pStatus = 0;
    String percentage = "";


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder general_holder, int position) {

        if(general_holder instanceof VHHeader)
        {

            String cardSubTitle = Integer.toString(getItemCount()-1);
            VHHeader VHheader = (VHHeader) general_holder;

            VHheader.txtTitle.setText(cardHeader.getTitle());
            VHheader.txtSubTitle.setText(cardSubTitle);


        }
        else if(general_holder instanceof AdapterMissedEvents.MyViewHolder ) {

            final AdapterMissedEvents.MyViewHolder holder = (AdapterMissedEvents.MyViewHolder) general_holder;
            final VaccinationEvent vaccinationEvent = getVaccinationEvent(position);


            holder.setData(vaccinationEvent,position);



        }


    }



    @Override
    public int getItemCount() {

        return vaccinationEvents.size();
    }

    /*

    //2. Animations bug
    @Override
    public long getItemId(int position) {
        return vaccinationEvents.get(position+1).hashCode(); //Any unique id
    }
    */


    private VaccinationEvent getVaccinationEvent(int position)
    {
        return vaccinationEvents.get(position);
    }

    //    need to override this method
    @Override
    public int getItemViewType(int position) {
      /*
        if(isPositionHeader(position))
            return TYPE_HEADER;
            */
        return TYPE_ITEM;
    }

    /*
    @Override
    public long getItemId(int position) {
        return position;
    }

    */
    private boolean isPositionHeader(int position)
    {
        return position == 0;
    }




    //(1) Declare Interface
    protected AdapterMissedEvents.OnVaccinationCLickLIstner myclickListener;  //our interace
    // private WebView webView;


    //interfaces
    public void setClickListener(AdapterMissedEvents.OnVaccinationCLickLIstner onCardItemClickListener) {
        this.myclickListener = onCardItemClickListener;
    }


    public interface OnVaccinationCLickLIstner {

        void onVaccinationClicked(View view, int position, VaccinationEvent caretaker);
    }





    private OnCardExpandClickedListner onCardExpandClickedListner;

    //someione who implements this interface ill send him a click trigger
    public interface OnCardExpandClickedListner{

        void onCardExpandCLicked(View view, int position, int mExpandedPostn, boolean isExpanded);

    }

    public void setClickListner(OnCardExpandClickedListner onCardExpandClickedListner){
        this.onCardExpandClickedListner = onCardExpandClickedListner;
    }


    public void confirmVaccinationDialog(String vaccineName, String childName) {


       // Activity activity = (Activity) this.activity;

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.MyDialogTheme);

        String dialogTitle = "Confirm Recording Vaccination ?";
        String dialogDesc =  ""+vaccineName+" administered to "+childName +", the operation can not be undone";

        builder.setTitle(dialogTitle);
        builder.setMessage(dialogDesc);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {



                    }

                });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                    }
                });

        AlertDialog dialog = builder.create();


          //if using api >= 19 use TypeToast not type_sytem_alert

          //  dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);

            dialog.show();




    }



    /*
    int mStackLevel = 0;

    void showDialog() {
        mStackLevel++;

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        Activity activity = (Activity) this.activity;

        FragmentManager fragmentMgr = activity.getFragmentManager();
        FragmentTransaction ft = fragmentMgr.beginTransaction();
        Fragment prev = fragmentMgr.findFragmentByTag("dialog");



        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        ConfirmVaccinationDialogFrgment newFragment = ConfirmVaccinationDialogFrgment.newInstance(mStackLevel);
        newFragment.show(ft, "dialog");

    }
    */









}