package com.vims.vimsapp.view.patients.list;


import android.app.ActionBar;
import android.app.SearchManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ListView;
import android.widget.TextView;


import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.patients.addcaretaker.AddCaretakerWelcomeActivity;

import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import vimsapp.R;

import vimsapp.databinding.PatientsChildListFragmentBinding;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;
import static com.vims.vimsapp.view.patients.addchild.AddChildWelcomeActivity.REQUEST_CODE;


public class ViewChildrenListFragment extends Fragment {


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;

    PatientsChildListFragmentBinding detailChildrenBinding;
    ViewCaretakerChildrenListFragmentViewModel viewCaretakerChildrenListFragmentViewModel;


    Bundle bundle;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    PreferenceHandler preferenceHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        detailChildrenBinding  = DataBindingUtil.inflate(inflater, R.layout.patients_child_list_fragment, container, false);

       viewCaretakerChildrenListFragmentViewModel = ViewModelProviders.of(this).get(ViewCaretakerChildrenListFragmentViewModel.class);

        bundle = getArguments();

        preferenceHandler = PreferenceHandler.getInstance(getContext());

        //viewCaretakerChildrenListFragmentViewModel.setIsStartProgress(true);
        viewCaretakerChildrenListFragmentViewModel.loadChildren();

        setUpRecyclerView();
        observeProgressBar();
        observeChildren();

        detailChildrenBinding.recyclerviewContent.errorMsgTv.setTypeface(MyFonts.getTypeFace(getContext()));
        detailChildrenBinding.recyclerviewContent.errorMsgTvSubtitle.setTypeface(MyFonts.getTypeFace(getContext()));



        return detailChildrenBinding.getRoot();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_add_caretaker:


                Intent detailsIntent = new Intent(getContext(), AddCaretakerWelcomeActivity.class);
                startActivityForResult(detailsIntent, REQUEST_CODE);


                return true;

        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onPrepareOptionsMenu(Menu menu) {

       // MenuItem itemSearch = menu.findItem(R.id.action_add_caretaker);
       // itemSearch.setVisible(false);

        MenuItem item = menu.findItem(R.id.action_settings);
        item.setVisible(false);

        MenuItem itemSchedule = menu.findItem(R.id.action_hospital_schedule);
        itemSchedule.setVisible(false);

        MenuItem itemSchedule1 = menu.findItem(R.id.action_sync);
        itemSchedule1.setVisible(false);



    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        setUpSearch(menu);

    }





    private void setUpSearch(Menu menu){

        /*
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchViewItem.getActionView();
        searchView.setIconifiedByDefault(false);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        searchView.setLayoutParams(params);
        searchViewItem.expandActionView();
        */


        // Inflate the menu_home; this adds items to the action bar if it is present.

        getActivity().getMenuInflater().inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);



        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchViewItem.getActionView();
//        searchView.setIconifiedByDefault(false);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        searchView.setLayoutParams(params);
        // searchViewItem.expandActionView();



        //Android guides explicitly says:
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        // Which is in your case NOT TRUE. Because you don't want to noticemgt in MainActivity but in SearchableActivity.
        // That means you should use this ComponentName instead of getComponentName() method:

        //----  ComponentName cn = new ComponentName(this, ActivitySearch.class);

        //---- searchView.setSearchableInfo(searchManager.getSearchableInfo(cn));
        //   searchView.setBackgroundColor(Color.WHITE);
        // Do not iconify the widget;expand it by default
        //searchView.setIconifiedByDefault(false);
        // searchView.setSubmitButtonEnabled(true);



        searchView.setIconifiedByDefault(true);
        // searchView.setIconified(false);




        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {


                if(adapterChildren!=null) {
                    adapterChildren.getFilter().filter(query);
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                return false;
            }
        });


        searchView.setQueryHint("Search children");


        /*
       searchView.setOnCloseListener(new SearchView.OnCloseListener() {
           @Override
           public boolean onClose() {

             //  viewCaretakerListFragmentViewModel.loadCaretakers();

             //  Toast.makeText(getContext(),"closed", Toast.LENGTH_LONG).show();
               viewCaretakerListFragmentViewModel.loadCaretakers();


               return false;
           }
       });
       */



        searchView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {

            @Override
            public void onViewDetachedFromWindow(View arg0) {
                // search was detached/closed
                //  viewCaretakerListFragmentViewModel.loadCaretakers();

                //  Toast.makeText(getContext(),"closed", Toast.LENGTH_LONG).show();
                viewCaretakerChildrenListFragmentViewModel.loadChildren();


            }

            @Override
            public void onViewAttachedToWindow(View arg0) {
                // search was opened
            }
        });



    }





    @Override
    public void onResume() {
        super.onResume();

        int isRecorded = preferenceHandler.getIntValue(PreferenceHandler.IS_CHILD_REGISTERED_INT);
        int isCaretakerRecorded = preferenceHandler.getIntValue(PreferenceHandler.IS_CHILD_REGISTERED_INT);
        if(isRecorded == 1 || isCaretakerRecorded == 1){
            viewCaretakerChildrenListFragmentViewModel.loadChildren();
            preferenceHandler.setIntValue(PreferenceHandler.IS_CHILD_REGISTERED_INT,0);
        }


    }




    private void observeProgressBar(){

        Observer<Boolean> progressObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {

                if(aBoolean!=null) {

                    if (aBoolean) {
                        detailChildrenBinding.recyclerviewContent.progressbarCaretakers.setVisibility(View.VISIBLE);

                    }else{
                        detailChildrenBinding.recyclerviewContent.progressbarCaretakers.setVisibility(View.INVISIBLE);

                    }

                }

            }
        };

        viewCaretakerChildrenListFragmentViewModel.getIsStartProgress().observe(this,progressObserver);

    }


    private void observeChildren(){

        Observer<List<Child>> childrenObserver = new Observer<List<Child>>() {
            @Override
            public void onChanged(@Nullable List<Child> children) {


                if(children!=null) {
                    detailChildrenBinding.recyclerviewContent.recyclerview.setVisibility(View.VISIBLE);
                    detailChildrenBinding.recyclerviewContent.errorMsgTv.setVisibility(View.INVISIBLE);
                    detailChildrenBinding.recyclerviewContent.errorMsgTvSubtitle.setVisibility(View.INVISIBLE);
                    detailChildrenBinding.recyclerviewContent.errorMsgNoChildren.setVisibility(View.INVISIBLE);


                    updateBasicInfo(children);
                }else{

                    detailChildrenBinding.recyclerviewContent.progressbarCaretakers.setVisibility(View.INVISIBLE);
                    detailChildrenBinding.recyclerviewContent.errorMsgTv.setVisibility(View.VISIBLE);
                    detailChildrenBinding.recyclerviewContent.errorMsgTvSubtitle.setVisibility(View.VISIBLE);
                    detailChildrenBinding.recyclerviewContent.errorMsgNoChildren.setVisibility(View.VISIBLE);


                    detailChildrenBinding.recyclerviewContent.recyclerview.setVisibility(View.INVISIBLE);

                }

            }
        };

         viewCaretakerChildrenListFragmentViewModel.getAllChildren().observe(this, childrenObserver);
    }



    public static ViewChildrenListFragment newInstance(Bundle args) {
        ViewChildrenListFragment fragment = new ViewChildrenListFragment();
     //   Bundle args = new Bundle();
       // args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }



    //AdapterChildrenArray adapterChildren;
    ListView childlistview;
    TextView tvHeader;


    public void setUpChildListView(int caretakerId){

        LayoutInflater inflater = getLayoutInflater();
       // childlistview = detailChildrenBinding.lvChildren;

        ViewGroup header = (ViewGroup)inflater.inflate(R.layout.patients_header_childrenlistinfo, childlistview, false);
        childlistview.addHeaderView(header, null, false);

        tvHeader =  header.findViewById(R.id.sizeOfChildren);

        /*
        ImageView imageView = header.findViewById(R.id.imageview_profile_card);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getContext(),AddChildProfileFragment.class);
                intent.putExtra("caretakerId", caretakerId);
                startActivity(intent);

            }
        });
        */

    }


    int no_columns = 0;
    public void setUpRecyclerView(){

        Interpolator overshoot  = new OvershootInterpolator(1f);
        Interpolator accelerateDecelerate  = new AccelerateDecelerateInterpolator();
        Interpolator bounce  = new BounceInterpolator();


        //setup the recycler view basic info
        SlideInUpAnimator animator = new SlideInUpAnimator(accelerateDecelerate);
        detailChildrenBinding.recyclerviewContent.recyclerview.setItemAnimator(animator);
        detailChildrenBinding.recyclerviewContent.recyclerview.getItemAnimator().setAddDuration(getResources().getInteger(R.integer.duration_recycler_view));
        //setting animation on scrolling

        DividerItemDecoration decoration = new DividerItemDecoration(getContext(), VERTICAL);
        //decoration.setDrawable(getResources().getDrawable(R.drawable.shape_borderline_ultra_thin_darktheme));
       // detailChildrenBinding.recyclerviewContent.recyclerview.addItemDecoration(decoration);



        detailChildrenBinding.recyclerviewContent.recyclerview.setHasFixedSize(true); //enhance recycler view scroll
        detailChildrenBinding.recyclerviewContent.recyclerview.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset_large);
        detailChildrenBinding.recyclerviewContent.recyclerview.addItemDecoration(itemDecoration);

        RecyclerView.LayoutManager mLayoutManager ;


        boolean isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);
        boolean isTablet = getResources().getBoolean(R.bool.tablet);



        if(isTablet){



             no_columns = 3;
             mLayoutManager = new GridLayoutManager(getActivity(), no_columns,GridLayoutManager.VERTICAL, false);
           // mLayoutManager = new LinearLayoutManager(getContext());
            detailChildrenBinding.recyclerviewContent.recyclerview.setLayoutManager(mLayoutManager);


             /*
            if(isPotrait) {

                no_columns = 2;

              //  mLayoutManager = new GridLayoutManager(getActivity(), no_columns,GridLayoutManager.VERTICAL, false);

                mLayoutManager = new LinearLayoutManager(getContext());
                detailChildrenBinding.recyclerviewContent.recyclerview.setLayoutManager(mLayoutManager);



            }
            else{

               // no_columns = 5;
                //mLayoutManager = new GridLayoutManager(getActivity(), no_columns,GridLayoutManager.VERTICAL, false);

                mLayoutManager = new LinearLayoutManager(getContext());
                detailChildrenBinding.recyclerviewContent.recyclerview.setLayoutManager(mLayoutManager);

            }
            */

        }else{

            mLayoutManager = new LinearLayoutManager(getContext());
            detailChildrenBinding.recyclerviewContent.recyclerview.setLayoutManager(mLayoutManager);

            /*

            if(isPotrait) {

                no_columns = 2;
                // setRecyclerViewLayoutManager(LINEAR_LAYOUT_MANAGER, no_columns);
                mLayoutManager = new LinearLayoutManager(getContext());
                detailChildrenBinding.recyclerviewContent.recyclerview.setLayoutManager(mLayoutManager);

            }
            else{

                no_columns = 4;
                mLayoutManager = new GridLayoutManager(getActivity(), no_columns,GridLayoutManager.VERTICAL, false);
                detailChildrenBinding.recyclerviewContent.recyclerview.setLayoutManager(mLayoutManager);

            }
            */



        }









    }


    AdapterChildren adapterChildren;
    public void updateBasicInfo(List<Child> children){


        // adapterProfileInfoArray = new AdapterProfileInfoArray(getContext(), children);
        // adapterProfileInfoArray.notifyDataSetChanged();
        // profilelistview.setAdapter(adapterProfileInfoArray);
        adapterChildren = new AdapterChildren(children, getContext());
        // adapterChildren.replaceItems(careTakers);
        adapterChildren.setHasStableIds(true);
        adapterChildren.notifyItemInserted(0);
        detailChildrenBinding.recyclerviewContent.recyclerview.setAdapter(adapterChildren);

    }



    }




