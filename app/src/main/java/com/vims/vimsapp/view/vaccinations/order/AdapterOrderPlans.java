package com.vims.vimsapp.view.vaccinations.order;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.manageorders.VaccinePlan;
import com.vims.vimsapp.view.MyFonts;

import java.util.List;
import java.util.Map;

import vimsapp.R;

public class AdapterOrderPlans extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    //the adapter needs its viewmodel
    private static volatile AdapterOrderPlans INSTANCE;

    private List<VaccinePlan> vaccinePlans;


    private Application context;

    int sizeOfItems;

    public Map<String, VaccinationEvent> mappedEvents;
    boolean isAnOrder;


    public AdapterOrderPlans(List<VaccinePlan> vaccinePlans, Application context, boolean isAnOrder) {
        this.context = context;
        this.isAnOrder = isAnOrder;

        /*
        String id = UUID.randomUUID().toString();
        for(VaccinationEvent vaccinationEvent: vaccinationEvents) {
            mappedEvents.put(id, vaccinationEvent);
        }
        */

        this.vaccinePlans = vaccinePlans;

        //1. Animations bug
       // setHasStableIds(true); //in constructor
        sizeOfItems = vaccinePlans.size();


        setHasStableIds(true);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }



    public static void clearInstance() {
        INSTANCE = null;
    }





    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

      if(viewType == TYPE_ITEM){

          int layout1 = R.layout.vaccinations_order_card;
          int layout2 = R.layout.vaccinations_ordered_card;
          int layout;

          if(isAnOrder) {
              layout = layout1;
          }else{
              layout = layout2;
          }
          View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);

          return new MyViewHolder(v);


        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");


    }

    //(2) implements the click listner
    public class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {
        private TextView vaccineBrand;
        private TextView quantityAtHand;
        private TextView quantityRequired;
        private TextView tvDosageUsed;

        private ImageView addButton;
        private ImageView minusButton;



        public MyViewHolder(View view) {
            super(view);
            //(3) Bind the click listener
            view.setOnClickListener(this);
        }


        public void setData(VaccinePlan vaccinePlan, int position){

            //place data from the object onto the view

            //THE VIEW
            vaccineBrand = itemView.findViewById(R.id.tvVaccineBrand);
            quantityAtHand = itemView.findViewById(R.id.tvQuantityAtHand);
            quantityRequired = itemView.findViewById(R.id.tvQuantityRequired);
            addButton = itemView.findViewById(R.id.imageButtonAdd);
            minusButton = itemView.findViewById(R.id.imageButtonMin);
            tvDosageUsed = itemView.findViewById(R.id.tvQuantityUsed);

            //THE TYPEFACE
            vaccineBrand.setTypeface(MyFonts.getTypeFace(context));
            quantityAtHand.setTypeface(MyFonts.getTypeFace(context));
            quantityRequired.setTypeface(MyFonts.getTypeFace(context));
            tvDosageUsed.setTypeface(MyFonts.getTypeFace(context));


            //THE BINDING
            String qRequired = Integer.toString(vaccinePlan.getQuantityRequired());
            String qAtHand = Integer.toString(vaccinePlan.getQuantityAtHand());
            String dosageUsed = Integer.toString(vaccinePlan.getQuantityUsed());

            vaccineBrand.setText(vaccinePlan.getVaccineName());
            quantityRequired.setText(qRequired);
            quantityAtHand.setText(qAtHand);
            tvDosageUsed.setText(dosageUsed);

            if(!isAnOrder){
                quantityRequired.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                 quantityAtHand.setTextColor(context.getResources().getColor(R.color.colorAccent));
            }


            addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    myclickListener.onAddClicked(quantityAtHand,quantityRequired, position, vaccinePlan); // call the onClick in the OnCardItemClickListener Interface i created

                }
            });
            minusButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myclickListener.onMinusClicked(quantityAtHand,quantityRequired,position,vaccinePlan);


                }
            });


        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition(); // gets item position

           // VaccinationEvent vaccinationEvent = vaccinationEvents.get(position);

        }

    }


    int mExpandedPosition = -1;
    int previousExpandedPosition = -1;
    int pStatus = 0;
    String percentage = "";


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder general_holder, int position) {


      if(general_holder instanceof AdapterOrderPlans.MyViewHolder ) {

            final AdapterOrderPlans.MyViewHolder holder = (AdapterOrderPlans.MyViewHolder) general_holder;
            final VaccinePlan vaccinePlan = getVaccinePlan(position);


            holder.setData(vaccinePlan,position);



        }


    }



    @Override
    public int getItemCount() {

        return vaccinePlans.size();
    }

    /*

    //2. Animations bug
    @Override
    public long getItemId(int position) {
        return vaccinationEvents.get(position+1).hashCode(); //Any unique id
    }
    */


    public VaccinePlan getVaccinePlan(int position)
    {
        return vaccinePlans.get(position);
    }

    public void upDateVaccinePlan(int position, VaccinePlan vaccinePlan)
    {
        vaccinePlans.set(position,vaccinePlan);

    }

    public List<VaccinePlan> getVaccinePlans()
    {
        return vaccinePlans;
    }


    //    need to override this method
    @Override
    public int getItemViewType(int position) {
      /*
        if(isPositionHeader(position))
            return TYPE_HEADER;
            */
        return TYPE_ITEM;
    }

    /*
    @Override
    public long getItemId(int position) {
        return position;
    }

    */
    private boolean isPositionHeader(int position)
    {
        return position == 0;
    }




    //(1) Declare Interface
    protected AdapterOrderPlans.OnVaccinationCLickLIstner myclickListener;  //our interace
    // private WebView webView;


    //interfaces
    public void setClickListener(AdapterOrderPlans.OnVaccinationCLickLIstner onCardItemClickListener) {
        this.myclickListener = onCardItemClickListener;
    }


    public interface OnVaccinationCLickLIstner {

        void onAddClicked(TextView qtyAtHand,TextView qtyRequired, int position, VaccinePlan vaccinePlan);
        void onMinusClicked(TextView qtyAtHand,TextView qtyRequired, int position, VaccinePlan vaccinePlan);
    }





    private OnCardExpandClickedListner onCardExpandClickedListner;

    //someione who implements this interface ill send him a click trigger
    public interface OnCardExpandClickedListner{

        void onCardExpandCLicked(View view, int position, int mExpandedPostn, boolean isExpanded);

    }

    public void setClickListner(OnCardExpandClickedListner onCardExpandClickedListner){
        this.onCardExpandClickedListner = onCardExpandClickedListner;
    }










}