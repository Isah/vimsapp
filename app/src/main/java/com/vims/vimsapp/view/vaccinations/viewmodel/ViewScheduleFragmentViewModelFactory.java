package com.vims.vimsapp.view.vaccinations.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.vims.vimsapp.managehospitalschedule.GenerateHospitalSchedule;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsSchedule;


/**
 * Created by root on 6/3/18.
 */
public class ViewScheduleFragmentViewModelFactory implements ViewModelProvider.Factory{


    private SearchVaccinationsSchedule readChildSchedule;
    private GenerateHospitalSchedule generateHospitalSchedule;

    public ViewScheduleFragmentViewModelFactory(SearchVaccinationsSchedule readChildSchedule) {
        this.readChildSchedule = readChildSchedule;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if(modelClass.isAssignableFrom(ViewScheduleFragmentViewModel.class)){

            return (T) new ViewScheduleFragmentViewModel(readChildSchedule);

        }
        else{

            throw new IllegalArgumentException("View Model not found");
        }

    }




}
