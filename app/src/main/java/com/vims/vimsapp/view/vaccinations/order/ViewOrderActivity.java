package com.vims.vimsapp.view.vaccinations.order;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleActivity;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.manage_order_controller.ManageOrderController;
import com.vims.vimsapp.manageorders.ViewOrders;
import com.vims.vimsapp.manageorders.Dosage;
import com.vims.vimsapp.manageorders.VaccineOrder;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.manageorders.VaccinePlan;
import com.vims.vimsapp.presenters.makeorder_presenter.ManageOrderView;
import com.vims.vimsapp.presenters.makeorder_presenter.ManageOrderViewModel;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsDataSource;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsRepository;
import com.vims.vimsapp.manageorders.RequestOrder;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsSchedule;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.MainThreadExecutor;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.utilities.ScreenManager;
import com.vims.vimsapp.utilities.alarm.UIAlert;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.injection.ModuleRepository;
import com.vims.vimsapp.view.injection.ModuleUI;
import com.vims.vimsapp.view.patients.list.ItemOffsetDecoration;
import com.vims.vimsapp.view.vaccinations.viewmodel.CreateOrderViewModelFactory;
import vimsapp.databinding.VaccinationsOrderViewActivityBinding;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import vimsapp.R;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;

public class ViewOrderActivity extends AppCompatActivity {


    boolean isSubmitOrder = false;

    Bundle bundle;

    String orderId;

    VaccinationsOrderViewActivityBinding activityBinding;


    @Override
    public void onStart() {
        super.onStart();
       // lifecycleRegistry.markState(Lifecycle.State.STARTED);
    }


    /*
    @Override
    public Lifecycle getLifecycle() {
        return lifecycleRegistry;
    }
    */


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityBinding = DataBindingUtil.setContentView(this,R.layout.vaccinations_order_view_activity);

        // manageOrderViewModel = ViewModelProviders.of(this).get(ManageOrderViewModel.class);
        //lifecycleRegistry = new LifecycleRegistry(this);
       // lifecycleRegistry.markState(Lifecycle.State.CREATED);

        ScreenManager.setScreenOrientation(this);
        setupToolbar();
        setUpRecyclerView();

        bundle = getIntent().getExtras();
        if(bundle!=null){
           // int orderMonth = bundle.getInt("order_month");

            //global_order_month = orderMonth;
            orderId = bundle.getString("order_id");
            loadDosage(orderId);


        }


    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    private void setupToolbar(){

        activityBinding.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        activityBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // manageOrderViewModel.setmNewOrderItemList(new ArrayList<>());

                finish();
                // onBackPressed();

            }
        });

        setSupportActionBar(activityBinding.toolbar);

    }

    RecyclerView.LayoutManager mLayoutManager;
    public void setUpRecyclerView(){
        boolean isTablet = getResources().getBoolean(R.bool.tablet);
        boolean isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);

        // DividerItemDecoration decoration = new DividerItemDecoration(getApplicationContext(), VERTICAL);
        DividerItemDecoration decoration = new DividerItemDecoration(getApplicationContext(), VERTICAL);
        // decoration.setDrawable(getResources().getDrawable(R.drawable.shape_borderline_ultra_thin_darktheme));
        activityBinding.recyclerview.addItemDecoration(decoration);

        //setup the recycler view basic info
        SlideInUpAnimator animator = new SlideInUpAnimator(new OvershootInterpolator(1f));
        activityBinding.recyclerview.setItemAnimator(animator);
        activityBinding.recyclerview.getItemAnimator().setAddDuration(500);
        //setting animation on scrolling

        // scheduleHomeBinding.recyclerview.setHasFixedSize(true); //enhance recycler view scroll
        // scheduleHomeBinding.recyclerview.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getApplicationContext(), R.dimen.item_offset_small);
        activityBinding.recyclerview.addItemDecoration(itemDecoration);
        //  RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL, false);



        if(isTablet){

            //if(!isPotrait){
            //mLayoutManager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL, false);
            mLayoutManager  = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);

            // }


        }else {
            // if(!isPotrait){
            mLayoutManager  = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);

            // }

        }

        activityBinding.recyclerview.setLayoutManager(mLayoutManager);


    }



    private void loadDosage(String orderId) {

       // Calendar calendar = Calendar.getInstance();
       // calendar.set(Calendar.MONTH, month);
       // calendar.set(Calendar.DAY_OF_MONTH, dayOfOrder);

        ViewOrders viewOrders = Injection.provideViewOrders(getApplication());
        ViewOrders.RequestValues requestValues = new ViewOrders.RequestValues(false);

            requestValues.setSubmitOrder(false);
            requestValues.setOrderId(orderId);

            viewOrders.execute(requestValues, new UseCase.OutputBoundary<ViewOrders.ResponseValue>() {
                @Override
                public void onSuccess(ViewOrders.ResponseValue result) {

                    activityBinding.tvOrderDate.setText(result.getVaccineOrder().getOrderDate());

                    displayOrderId();

                    VaccineOrder vaccineOrder = result.getVaccineOrder();

                    activityBinding.tvOrderSubmitted.setVisibility(View.VISIBLE);
                    String ordering = "Delivered on " + vaccineOrder.getOrderDate();
                    activityBinding.tvOrderSubmitted.setText(ordering);

                    updateUpcomingEvents(vaccineOrder.getVaccinePlans(), vaccineOrder.getVaccinePlans().size(), false);

                }

                @Override
                public void onError(String errorMessage) {



                }
            });




    }
    private void displayOrderId(){

        String orderID = bundle.getString("order_id");

        if(orderID!= null) {

            String[] orderSplit = orderID.split("-");

            String firstString = orderSplit[1];
            String lastString = orderSplit[4];

            char orderid_char1 = lastString.charAt(0);

            //db7b91e0-14b5-4a87-8312-1b695457bf97
            //bd0f5a79-7908-49b7-b081-c5af2165755f

            //THE BINDING
            String orderId = firstString+" - "+orderid_char1;
            String orderTitle = "Order #" + orderId;

            activityBinding. tvDateTiming.setText(orderTitle);
        }


    }



    AdapterOrderPlans adapterOrders;

    public void updateUpcomingEvents(List<VaccinePlan> vaccinePlanList, int total, boolean isAnOrder){

        adapterOrders = new AdapterOrderPlans(vaccinePlanList,getApplication(),isAnOrder);
        //adapterOrders.setClickListener(new OrderListner());


        //adapterVaccinationEvents.setHasStableIds(true);ooncomingEvents.notifyItemInserted(0);
        int prevSize = vaccinePlanList.size();
        adapterOrders.notifyItemRangeInserted(prevSize, vaccinePlanList.size() -prevSize);

        activityBinding.recyclerview.setNestedScrollingEnabled(false);

        activityBinding.recyclerview.postDelayed(new Runnable() {
            @Override
            public void run() {
                activityBinding.recyclerview.setAdapter(adapterOrders);

                //   adapterUpcomingEvents.getFilter().filter("January");

            }
        },100);

        String totalQuantity = "Req. "+Integer.toString(total);
        //tvQuantityRequired.setText(totalQuantity);

    }





}
