package com.vims.vimsapp.view.vaccinations.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.vims.vimsapp.UseCase;

import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsSchedule;

import java.util.Date;
import java.util.List;

/**
 * Created by root on 5/30/18.
 */

public class ViewScheduleFragmentViewModel extends ViewModel {


     private  MutableLiveData<List<VaccinationEvent>>  mCalendarDayVaccinationEvents;
     private  MutableLiveData<List<VaccinationEvent>>  mVaccinationEventsForDate;
     private  MutableLiveData<String>  errorMessage;


     private SearchVaccinationsSchedule readChildSchedule;

    public ViewScheduleFragmentViewModel(SearchVaccinationsSchedule readChildSchedule) {

        this.readChildSchedule = readChildSchedule;

        mCalendarDayVaccinationEvents = new MutableLiveData<>();
        mVaccinationEventsForDate = new MutableLiveData<>();

        errorMessage = new MutableLiveData<>();


    }



    public MutableLiveData<String> getErrorMessage() {
        return errorMessage;
    }



    public LiveData<List<VaccinationEvent>> getCalendarDayVaccinationEvents() {
        if(mCalendarDayVaccinationEvents == null){
            mCalendarDayVaccinationEvents = new MutableLiveData<>();

        }
        return mCalendarDayVaccinationEvents;
    }




    public LiveData<List<VaccinationEvent>> getVaccinationEventsOfGivenDate() {
        if(mVaccinationEventsForDate == null){
            mVaccinationEventsForDate = new MutableLiveData<>();

        }
        return mVaccinationEventsForDate;
    }



    private MutableLiveData<List<VaccinationEvent>> mUpcomingVaccinations;
    public LiveData<List<VaccinationEvent>> getUpcomingVaccinations() {
        if(mUpcomingVaccinations == null){
            mUpcomingVaccinations = new MutableLiveData<>();

        }
        return mUpcomingVaccinations;
    }


    private MutableLiveData<List<VaccinationEvent>> mCompletedVaccinations;
    public LiveData<List<VaccinationEvent>> getCompletedVaccinations() {
        if(mCompletedVaccinations == null){
            mCompletedVaccinations = new MutableLiveData<>();

        }
        return mCompletedVaccinations;
    }


    private MutableLiveData<List<VaccinationEvent>> mMissedVaccinations;
    public LiveData<List<VaccinationEvent>> getMissedVaccinations() {
        if(mMissedVaccinations == null){
            mMissedVaccinations = new MutableLiveData<>();

        }
        return mMissedVaccinations;
    }




    public void loadVaccinationCards(Date cardCreationDate) {

        SearchVaccinationsSchedule.RequestValues requestValues = new SearchVaccinationsSchedule.RequestValues(SearchVaccinationsSchedule.VaccinationsRequestType.DATE);
        requestValues.setDate(cardCreationDate);


        readChildSchedule.execute(requestValues, new UseCase.OutputBoundary<SearchVaccinationsSchedule.ResponseValue>() {
            @Override
            public void onSuccess(SearchVaccinationsSchedule.ResponseValue result) {

                    mVaccinationEventsForDate.postValue(result.getVaccinationEvents());

            }

            @Override
            public void onError(String e) {

                errorMessage.postValue(e);

            }
        });


        }

    }



