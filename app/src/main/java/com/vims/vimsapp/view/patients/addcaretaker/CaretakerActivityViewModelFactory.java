package com.vims.vimsapp.view.patients.addcaretaker;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.vims.vimsapp.registerchild.RegisterCaretaker;


/**
     * Created by root on 6/3/18.
     */
public class CaretakerActivityViewModelFactory implements ViewModelProvider.Factory{


        private final RegisterCaretaker registerCaretaker;

        public CaretakerActivityViewModelFactory(RegisterCaretaker registerCaretaker) {
            this.registerCaretaker = registerCaretaker;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            if(modelClass.isAssignableFrom(CaretakerActivityWizardViewModel.class)){

                return (T) new CaretakerActivityWizardViewModel(registerCaretaker);

            }
            else{

                throw new IllegalArgumentException("View Model not found");
            }

        }




}
