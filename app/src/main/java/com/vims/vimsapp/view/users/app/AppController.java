package com.vims.vimsapp.view.users.app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.vims.vimsapp.utilities.AppExecutors;
import com.vims.vimsapp.utilities.PreferenceHandler;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Lincoln on 5/29/2018.
 */

public class AppController extends Application {
    public static final String TAG = AppController.class.getSimpleName();

    private RequestQueue mRequestQueue;

    private static AppController mInstance;

    PreferenceHandler preferenceHandler;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        preferenceHandler = PreferenceHandler.getInstance(getApplicationContext());

       // Log.d("APPTIME", "something went wrong when trying to initialize TrueTime");

      //  initTrueTime();


    }




    /*
    private void initTrueTime(){

        AppExecutors appExecutors = new AppExecutors();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                Log.d("APPTIME", "started runnable");

                   // TrueTime.build().initialize();
                    try {
                        TrueTime.build()
                                //.withSharedPreferences(SampleActivity.this)
                                .withNtpHost("time.google.com")
                                .withLoggingEnabled(false)
                                .withConnectionTimeout(3_1428)
                                .initialize();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "something went wrong when trying to initialize TrueTime", e);
                        Log.d("APPTIME", ""+e.getMessage());
                    }

            }
        };

        appExecutors.networkIO().execute(runnable);
    }
      */


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}

