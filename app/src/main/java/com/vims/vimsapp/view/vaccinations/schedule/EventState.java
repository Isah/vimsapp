package com.vims.vimsapp.view.vaccinations.schedule;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class EventState {


    Context mContext;

    public EventState(Context context) {
        mContext = context;
    }

    public void putPref(String key, String value) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = prefs.edit();


        editor.putString(key, value);


        editor.apply();
    }

    public String getPref(String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return preferences.getString(key, null);
    }


}
