package com.vims.vimsapp.view.vaccinations.viewmodel;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.vims.vimsapp.searchvaccinations.SearchVaccinationsRepository;
import com.vims.vimsapp.managehospitalschedule.GenerateHospitalSchedule;
import com.vims.vimsapp.viewcoverage.GenerateReport;

import com.vims.vimsapp.searchvaccinations.SearchVaccinationsSchedule;
import com.vims.vimsapp.recordvaccinations.RecordChildVaccination;


/**
 * Created by root on 6/3/18.
 */
public class ViewScheduleUpcomingFragmentViewModelFactory implements ViewModelProvider.Factory{


    private final SearchVaccinationsSchedule readChildSchedule;
    private final GenerateHospitalSchedule generateHospitalSchedule;
    private final RecordChildVaccination recordChildVaccination;
    private final GenerateReport generateReport;
    private final SearchVaccinationsRepository vaccinationsRepository;
    private final Application mApplication;


    public ViewScheduleUpcomingFragmentViewModelFactory(SearchVaccinationsSchedule readChildSchedule, RecordChildVaccination recordChildVaccination, GenerateReport generateReport, SearchVaccinationsRepository vaccinationsRepository, GenerateHospitalSchedule generateHospitalSchedule, Application application) {
        this.readChildSchedule = readChildSchedule;
        this.recordChildVaccination = recordChildVaccination;
        this.generateReport = generateReport;
        this.vaccinationsRepository = vaccinationsRepository;
        this.mApplication = application;
        this.generateHospitalSchedule = generateHospitalSchedule;

    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if(modelClass.isAssignableFrom(ViewScheduleUpcomingFragmentViewModel.class)){

            return (T) new ViewScheduleUpcomingFragmentViewModel(readChildSchedule, recordChildVaccination, generateReport, vaccinationsRepository,generateHospitalSchedule, mApplication);

        }
        else{

            throw new IllegalArgumentException("View Model not found");
        }

    }




}
