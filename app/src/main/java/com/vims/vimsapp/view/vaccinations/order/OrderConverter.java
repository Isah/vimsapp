package com.vims.vimsapp.view.vaccinations.order;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vims.vimsapp.entities.vaccinations.VaccinationCard;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;


import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class OrderConverter {

    static Gson gson = new Gson();
    @TypeConverter
    public static   List<VaccinationEvent> stringToSomeObjectList(String data) {


        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<VaccinationEvent>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(List<VaccinationEvent> someObjects) {


        return gson.toJson(someObjects);
    }





    @TypeConverter
    public static VaccinationCard fromStringToVaccinationCard(String value) {

        Type listType = new TypeToken<VaccinationCard>() {}.getType();

        return new Gson().fromJson(value, listType);

    }

    @TypeConverter
    public static String fromVaccinationCardToString(VaccinationCard coverageReport) {

        Gson gson = new Gson();

        String json = gson.toJson(coverageReport);

        return json;

    }







}
