package com.vims.vimsapp.view.vaccinations.reference;

import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;


import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;

import java.util.List;

public class VaccinationsRecyclerViewDiffCallBack extends DiffUtil.Callback  {


        private final List<VaccinationEvent> mOldEmployeeList;
        private final List<VaccinationEvent> mNewEmployeeList;

        public VaccinationsRecyclerViewDiffCallBack(List<VaccinationEvent> oldEmployeeList, List<VaccinationEvent> newEmployeeList) {
            this.mOldEmployeeList = oldEmployeeList;
            this.mNewEmployeeList = newEmployeeList;
        }

        @Override
        public int getOldListSize() {
            return mOldEmployeeList.size();
        }

        @Override
        public int getNewListSize() {
            return mNewEmployeeList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return mOldEmployeeList.get(oldItemPosition).getEventId() == mNewEmployeeList.get(
                    newItemPosition).getEventId();
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            final VaccinationEvent oldEmployee = mOldEmployeeList.get(oldItemPosition);
            final VaccinationEvent newEmployee = mNewEmployeeList.get(newItemPosition);

            return oldEmployee.getVaccineName().equals(newEmployee.getVaccineName());
        }

        @Nullable
        @Override
        public Object getChangePayload(int oldItemPosition, int newItemPosition) {
            // Implement method if you're going to use ItemAnimator
            return super.getChangePayload(oldItemPosition, newItemPosition);
        }
    }

