package com.vims.vimsapp.view.reports;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.view.injection.ModuleRepository;
import com.vims.vimsapp.viewcoverage.CoverageReport;
import com.vims.vimsapp.viewcoverage.GenerateReport;
import com.vims.vimsapp.viewcoverage.VaccineReport;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.patients.list.ItemOffsetDecoration;
import com.vims.vimsapp.utilities.PreferenceHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import vimsapp.R;
import vimsapp.databinding.ReportsFragmentVaccineCoverageBinding;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentReportsVaccineCoverage.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentReportsVaccineCoverage#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentReportsVaccineCoverage extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    ReportsFragmentVaccineCoverageBinding reportsFragmentBinding;



    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentReportsVaccineCoverage() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentReports.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentReportsVaccineCoverage newInstance(String param1, String param2) {
        FragmentReportsVaccineCoverage fragment = new FragmentReportsVaccineCoverage();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    FragmentReportsViewModelFactory fragmentReportsViewModelFactory;
    FragmentReportsViewModel fragmentReportsViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        reportsFragmentBinding = DataBindingUtil.inflate(inflater,R.layout.reports_fragment_vaccine_coverage,container,false);

        fragmentReportsViewModelFactory = new FragmentReportsViewModelFactory(Injection.provideGenerateReport(getActivity().getApplication()), ModuleRepository.provideViewChildRepository(getActivity().getApplication()),getActivity().getApplication());
        fragmentReportsViewModel=  ViewModelProviders.of(this, fragmentReportsViewModelFactory).get(FragmentReportsViewModel.class);


        //reportsFragmentBinding.headerGraph.tvtitleEvents.setTypeface(MyFonts.getBoldTypeFace(getContext()));
        //reportsFragmentBinding.headerGraph.tvSubtitleEvents.setTypeface(MyFonts.getTypeFace(getContext()));

        //((AppCompatActivity)getActivity()).getSupportActionBar().setLogo(R.drawable.notice_48);
        final CollapsingToolbarLayout collapsingToolbarLayout = reportsFragmentBinding.collapsingToolbar;
        collapsingToolbarLayout.setTitleEnabled(false);


        Toolbar toolbar = reportsFragmentBinding.toolbar;
        SpannableStringBuilder str = new SpannableStringBuilder("Stats");
        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        toolbar.setTitle(str);
        toolbar.setTitleMarginStart(72);


        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar); //we replace our actionbar with our toolbar
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if(actionBar!=null) {
          //  actionBar.setTitle("Reports");
        }


        setUpRecyclerView();
       // setUpBarGraph();

        //item sinner sets the spinner text color
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getContext(), R.array.graph_array, R.layout.item_spinner);
        adapter.setDropDownViewResource(R.layout.item_spinner_dropdown);
        reportsFragmentBinding.graphSpinner.setAdapter(adapter);


        // setUpYearlyGraph();
        // vaccineReports();

       // fragmentReportsViewModel.generateReports();
        //fragmentReportsViewModel.generateReportsForEachVaccine();

        //observeGraphReports();
      //  observeVaccineCardReports();

      //  observeGraphReportErrors();
      //  observeVaccineReportCardErrors();
      //  observeProgress();


        reportsFragmentBinding.recyclerViewReports.setVisibility(View.INVISIBLE);

        //executeVaccineReports();

        setupDrawables();

        vaccineSpinner();

        setUpColorTheme();


        return reportsFragmentBinding.getRoot();

    }

    private void setupDrawables(){

        reportsFragmentBinding.reportsNoDataViewTitle.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_chart_blue_24dp),null,null,null);

    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /**
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


   /*
    private void observeGraphReports(){

        Observer<CoverageReport> coverageReportObserver = new Observer<CoverageReport>() {
            @Override
            public void onChanged(@Nullable CoverageReport coverageReport) {

                if (coverageReport!=null && coverageReport.getVaccineReportList() != null){

                List<VaccineReport> vaccineReports = coverageReport.getVaccineReportList();

                    setUpBarChart(vaccineReports);


                reportsFragmentBinding.recyclerViewReports.setVisibility(View.VISIBLE);


                reportsFragmentBinding.graphSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        //String item = reportsFragmentBinding.headerGraph.graphSpinner.getSelectedItem().toString();

                        String item = parent.getSelectedItem().toString();

                        if (item.equals("TRENDS")) {


                          //  setUpLineChart(vaccineReports);


                        } else {//LINE

                           // setUpBarChart(vaccineReports);


                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                //UPDATE THE HEADER
                String statReport = "" + coverageReport.getTotalImmunized() + "/" + coverageReport.getTotalVaccinations();
                String title = "Dosage";



                String dateOfVaccination = DateCalendarConverter.dateToStringWithoutTime(new Date());
                String[] dateofvac = dateOfVaccination.split(" ");
                String vm = dateofvac[0];
                String vd = dateofvac[1];
                String vy = dateofvac[2];

                if (vm.length() > 3) {
                        vm = vm.substring(0, 3);
                 }

                String dateofvac_strimed = ""+vm+" "+vd+" "+vy;


               // reportsFragmentBinding.headerGraph.tvSubtitleEvents.setText(vy);
               // reportsFragmentBinding.headerGraph.tvtitleEvents.setText(title);
                startCircularProgress(coverageReport.getCoveragePercentage());


            }



            }
        };

        //declare viewmodel observation
        fragmentReportsViewModel.getCoverageReportMutableLiveData().observe(this,coverageReportObserver);


    }
   */




    private void observeGraphReportErrors(){

        Observer<String> graphError = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                reportsFragmentBinding.chart.setNoDataText(s);

            }
        };

        //declare observation
        fragmentReportsViewModel.getError().observe(this,graphError);


    }


    ProgressDialog progressDialog;

    public void setProgressDialog(){

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        new Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed

                        // onLoginFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);

    }


    private void observeProgress(){

        Observer<FragmentReportsViewModel.ReportProgress> progressObserver = new Observer<FragmentReportsViewModel.ReportProgress>() {
            @Override
            public void onChanged(@Nullable FragmentReportsViewModel.ReportProgress reportProgress) {

                if(reportProgress == FragmentReportsViewModel.ReportProgress.IN_PROGRESS){

                   // reportsFragmentBinding.headerGraph.reportsHeader.setVisibility(View.INVISIBLE);
                    reportsFragmentBinding.cardViewHomeWelcome.setVisibility(View.INVISIBLE);
                    reportsFragmentBinding.errorViewReports.setVisibility(View.INVISIBLE);
                    reportsFragmentBinding.progressbarReports.setVisibility(View.VISIBLE);

                }else if(reportProgress == FragmentReportsViewModel.ReportProgress.LOAD_SUCCESS){

                   // reportsFragmentBinding.headerGraph.reportsHeader.setVisibility(View.VISIBLE);
                    reportsFragmentBinding.cardViewHomeWelcome.setVisibility(View.VISIBLE);
                    reportsFragmentBinding.progressbarReports.setVisibility(View.INVISIBLE);
                    reportsFragmentBinding.errorViewReports.setVisibility(View.INVISIBLE);

                }
                else {//failed

                   // reportsFragmentBinding.headerGraph.reportsHeader.setVisibility(View.INVISIBLE);
                    reportsFragmentBinding.cardViewHomeWelcome.setVisibility(View.INVISIBLE);
                    reportsFragmentBinding.progressbarReports.setVisibility(View.INVISIBLE);
                    reportsFragmentBinding.errorViewReports.setVisibility(View.VISIBLE);

                }

            }
        };

        fragmentReportsViewModel.getProgressMutableLiveData().observe(this,progressObserver);

    }




    int  bar_labels_color = 0;
    int bar_color1 = 0;//colorPrimary / colorPrimaryTint0
    int bar_color2 = 0;//blue grey 700 /  colorPrimaryDark

    int yaxis1_scale_text_Color = 0; // colorPrimary //textColorSecondary)
    int yaxis2_scale_text_Color = 0;

    int yaxis2_grid_Color = 0;//blue grey 800 / colorPrimaryDark
    int yaxis1_grid_Color = 0;//blue grey 800 / colorPrimaryDark


    int xaxis_grid_color = 0;
    int xaxis_label_text_color = 0;//text color secondary / white
    int xaxis_value_text_color = 0;//text color secondary / white

    int DEFAULT_THEME_ID = 0;
    int LIGHT_THEME_ID = 0;

    int theme_id = 0;


    PreferenceHandler preferenceHandler;

    public void setUpColorTheme(){

        DEFAULT_THEME_ID = R.style.AppTheme_Dark;
        LIGHT_THEME_ID = R.style.AppTheme_Light;

        preferenceHandler = PreferenceHandler.getInstance(getContext());
        theme_id = preferenceHandler.getIntValue("THEME_ID");


        if(theme_id == DEFAULT_THEME_ID){

            bar_labels_color = getResources().getColor(R.color.white);
            bar_color1 = getResources().getColor(R.color.colorPrimary);//colorPrimary / colorPrimaryTint0
            bar_color2 = getResources().getColor(R.color.blue_grey_700);//blue grey 700 /  colorPrimaryDark

            yaxis1_scale_text_Color = getResources().getColor(R.color.colorPrimaryTint0); // colorPrimary //textColorSecondary)
            yaxis2_scale_text_Color = getResources().getColor(R.color.rowBackgroundDarkTheme);

            yaxis2_grid_Color = getResources().getColor(R.color.blue_grey_800);//blue grey 800 / colorPrimaryDark
            yaxis1_grid_Color = getResources().getColor(R.color.blue_grey_800);//blue grey 800 / colorPrimaryDark


            xaxis_grid_color = getResources().getColor(R.color.colorPrimaryDark);
            xaxis_label_text_color = getResources().getColor(R.color.colorPrimaryTint0);//text color secondary / white
            xaxis_value_text_color = getResources().getColor(R.color.colorPrimaryTint0);//text color secondary / white


        }else  if(theme_id == LIGHT_THEME_ID){

            bar_labels_color = getResources().getColor(R.color.textColorPrimary);//tcp   / white
            bar_color1 = getResources().getColor(R.color.colorPrimary);   // colorPrimaryLight
            bar_color2 = getResources().getColor(R.color.grey_pallete_100);    // colorPrimaryDark

            yaxis1_scale_text_Color = getResources().getColor(R.color.textColorPrimary);// tcp / cp
            yaxis2_scale_text_Color = getResources().getColor(R.color.textColorPrimary);//tcp / cp

            yaxis2_grid_Color = getResources().getColor(R.color.grey_pallete_300);
            yaxis1_grid_Color = getResources().getColor(R.color.grey_pallete_300);//blue grey 800 / colorPrimaryDark



            xaxis_grid_color = getResources().getColor(R.color.textColorPrimary);
            xaxis_label_text_color = getResources().getColor(R.color.textColorPrimary);//tcp // tcpd
            xaxis_value_text_color = getResources().getColor(R.color.textColorPrimary);//tcp // tcpd

        }


    }




    private void vaccineSpinner(){

        setUpGraphData("OPV0");

        ArrayAdapter adapter = ArrayAdapter.createFromResource(getContext(), R.array.vaccine_array, R.layout.item_spinner);
        adapter.setDropDownViewResource(R.layout.item_spinner_dropdown);
        reportsFragmentBinding.graphSpinner.setAdapter(adapter);

        reportsFragmentBinding.graphSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getSelectedItem().toString();

                if (item.equals("OPV0")) {

                    setUpGraphData("OPV0");

                }if (item.equals("BCG")) {

                    setUpGraphData("BCG");

                }if (item.equals("OPV1")) {

                    setUpGraphData("OPV1");


                }if (item.equals("Hib1")) {

                    setUpGraphData("Hib1");

                }if (item.equals("PCV1")) {

                    setUpGraphData("PCV1");

                }if (item.equals("Rota1")) {

                    setUpGraphData("Rota1");

                }if (item.equals("OPV2")) {

                    setUpGraphData("OPV2");

                }if (item.equals("Hib2")) {

                    setUpGraphData("Hib2");


                }if (item.equals("PCV2")) {

                    setUpGraphData("PCV2");

                }if (item.equals("Rota2")) {

                    setUpGraphData("Rota2");

                }if (item.equals("OPV3")) {

                    setUpGraphData("OPV3");

                }if (item.equals("Hib3")) {

                    setUpGraphData("Hib3");

                }if (item.equals("PCV3")) {

                    setUpGraphData("PCV3");

                }if (item.equals("Rota3")) {

                    setUpGraphData("Rota3");

                }if (item.equals("IPV")) {

                    setUpGraphData("IPV");

                }if (item.equals("Measles")) {

                    setUpGraphData("Measles");

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void setUpGraphData(String vaccineName){
        Date today = new Date();
        String year =  DateCalendarConverter.dateToStringGetYear(today);
        int year1 = Integer.parseInt(year);
        int year2 = year1 - 1;
        int year3 = year2 - 1;
        int year4 = year3 - 1;
        Log.d("SpinnerGraph",""+year);

        //plot a graph of coverage percentage with months
        GenerateReport generateReport = Injection.provideGenerateReport(getActivity().getApplication());

        String searchDate = DateCalendarConverter.dateToStringWithoutTimeAndDay(new Date());
        GenerateReport.ReportType reportType = GenerateReport.ReportType.VACCINE_SPECIFIC_HOME;

        GenerateReport.RequestValues requestValues = new GenerateReport.RequestValues(reportType,searchDate);
        requestValues.setTypeOfVaccine(vaccineName);

         generateReport.execute(requestValues, new UseCase.OutputBoundary<GenerateReport.ResponseValue>() {
             @Override
             public void onSuccess(GenerateReport.ResponseValue result) {

                 reportsFragmentBinding.recyclerViewReports.setVisibility(View.VISIBLE);
                 reportsFragmentBinding.cardViewHomeWelcome.setVisibility(View.VISIBLE);


                 CoverageReport coverageReport = result.getCoverageReport();
                 List<VaccineReport> vaccineReports =  coverageReport.getVaccineReportsCard().getVaccineReports();

                 int upcoming = coverageReport.getUpcomingVaccinations();
                 int missed  = coverageReport.getMissedVaccinations();
                 int done = coverageReport.getTotalImmunized();
                 int coverage = coverageReport.getCoveragePercentage();
                 int totalVaccinations = missed + upcoming + done;

                 int percentageUpcoming = (int) (((double) upcoming / (double) totalVaccinations * 100));
                 int percentageMissed = (int) (((double) missed / (double) totalVaccinations * 100));
                 int percentageDone = (int) (((double) done / (double) totalVaccinations * 100));

                 Log.d("CoverageNow", " COV: "+coverage+" vac:"+vaccineName);
                 setUpBarChart(vaccineReports);

                 String pStatus = ""+coverage+"%";

                 //reportsFragmentBinding.circularProgressbar.setProgress(pStatus);
                 reportsFragmentBinding.tv.setText(pStatus);

                 //startCircularProgress(coverage,pStatus);


             }

             @Override
             public void onError(String errorMessage) {

             }
         });

        }

    private void setUpBarChart(List<VaccineReport> vaccineReports){

        List<BarEntry> entries = new ArrayList<>();

        for (VaccineReport data : vaccineReports) {
            // turn your data into Entry objects
            entries.add(new BarEntry(data.getxAxisValue(), data.getCoverageImmunisedDelayed()));
        }

        //DataSet objects hold data which belongs together, and allow individual styling of that data.
        BarDataSet dataSet = new BarDataSet(entries,"Vaccinations");
        dataSet.setStackLabels(new String[]{"Immunised", "Not Immunised"});

        //change color of dataset stack labels
        reportsFragmentBinding.chart.getLegend().setTextColor(xaxis_label_text_color);

        // add entries to dataset
        int barColors[] = new int[2];
        barColors[0] = bar_color1;//cpt0
        barColors[1] = bar_color2;//cpd

        dataSet.setColors(barColors);
        dataSet.setValueTextColor(xaxis_value_text_color); // styling, value colorss


        //ad labels horizontally
        ArrayList<String> labels = new ArrayList<>();

        labels.add("Jan");
        labels.add("Feb");
        labels.add("Mar");
        labels.add("Apr");
        labels.add("May");
        labels.add("Jun");
        labels.add("Jul");
        labels.add("Aug");
        labels.add("Sep");
        labels.add("Oct");
        labels.add("Nov");
        labels.add("Dec");



        YAxis yAxis1 = reportsFragmentBinding.chart.getAxisLeft();
        YAxis yAxis2 = reportsFragmentBinding.chart.getAxisRight();


        yAxis1.setDrawAxisLine(false);
        yAxis2.setDrawAxisLine(false);

        yAxis1.setDrawGridLines(false);
        // yAxis2.setDrawGridLines(false);


        yAxis1.setTextColor(yaxis1_scale_text_Color);
        yAxis2.setTextColor(yaxis2_scale_text_Color);

        yAxis1.setGridColor(yaxis1_grid_Color);
        yAxis2.setGridColor(yaxis2_grid_Color);



        XAxis xAxis = reportsFragmentBinding.chart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setTextColor(xaxis_value_text_color);
        xAxis.setCenterAxisLabels(true);
        xAxis.setDrawGridLines(false);


        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGridColor(xaxis_grid_color);
        xAxis.setTextSize(14);
        xAxis.setTypeface(MyFonts.getTypeFace(getContext()));
        //xAxis.setLabelRotationAngle(-90);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                if(value >= 0){
                    if (value <= labels.size() - 1) {
                        return labels.get((int) value);
                    }
                    return "";

                }
                return "";

            }
        });


        // ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        // dataSets.add(dataSet);




        //This object holds all data that is represented by a Chart
        //LineData lineData = new LineData(dataSet);
        BarData barData = new BarData(dataSet);
        barData.setBarWidth(0.7f); // set custom bar width

        barData.setDrawValues(true);


        reportsFragmentBinding.chart.setFitBars(true); // make the x-axis fit exactly all bars
        reportsFragmentBinding.chart.setData(barData);
        reportsFragmentBinding.chart.setHighlightFullBarEnabled(false);
        reportsFragmentBinding.chart.getDescription().setEnabled(false);
        reportsFragmentBinding.chart.animateY(1500);
        reportsFragmentBinding.chart.invalidate(); // refresh
        reportsFragmentBinding.chart.setNoDataText("Loading..");
        reportsFragmentBinding.chart.setNoDataTextColor(getResources().getColor(R.color.white));
        reportsFragmentBinding.chart.setNoDataTextTypeface(MyFonts.getTypeFace(getContext()));
        reportsFragmentBinding.chart.setNoDataTextTypeface(MyFonts.getTypeFace(getContext()));

       // reportsFragmentBinding.linechart.setVisibility(View.INVISIBLE);
        reportsFragmentBinding.chart.setVisibility(View.VISIBLE);


    }



    private void startCircularProgress(int progress,final int pStatus) {
        Handler handler = new Handler();

        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (pStatus < progress) {
                   // pStatus += 1;

                    String percentage = Integer.toString(progress)+"%";

                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub

                            reportsFragmentBinding.circularProgressbar.setProgress(pStatus);
                            reportsFragmentBinding.tv.setText(percentage);


                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        // Just to display the progress slowly
                        Thread.sleep(5); //thread will take approx 3 seconds to finish
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }




    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);

      //  item.setVisible(false);

    }





    DividerItemDecoration decoration;
    public void setUpRecyclerView(){

        decoration = new DividerItemDecoration(getContext(), VERTICAL);
        //ReportsFragmentBinding.recyclerViewReports.addItemDecoration(decoration);
        //setup the recycler view basic info
        SlideInUpAnimator animator = new SlideInUpAnimator(new OvershootInterpolator(1f));

        reportsFragmentBinding.recyclerViewReports.setItemAnimator(animator);
        reportsFragmentBinding.recyclerViewReports.getItemAnimator().setAddDuration(1000);
        //setting animation on scrolling

        reportsFragmentBinding.recyclerViewReports.setHasFixedSize(true); //enhance recycler view scroll
        reportsFragmentBinding.recyclerViewReports.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset_schedule);
        reportsFragmentBinding.recyclerViewReports.addItemDecoration(itemDecoration);

        RecyclerView.LayoutManager mLayoutManager;

        boolean isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);
        boolean isTablet = getResources().getBoolean(R.bool.tablet);

       // mLayoutManager = new LinearLayoutManager(getContext());
        //mLayoutManager  = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

        int spanCount;

        if(isTablet) {
        spanCount = 4;
        }else{
         spanCount = 3;
        }
         mLayoutManager = new GridLayoutManager(getActivity(), spanCount, GridLayoutManager.VERTICAL, false);
         reportsFragmentBinding.recyclerViewReports.setLayoutManager(mLayoutManager);


    }

    /*
    public void executeVaccineReports(){

        GenerateReport generateReport = Injection.provideGenerateReport(getActivity().getApplication());

        GenerateReport.ReportType reportType = GenerateReport.ReportType.MONTHLY;
        String a = DateCalendarConverter.dateToStringWithoutTimeAndDay(new Date());

        GenerateReport.RequestValues requestValues = new GenerateReport.RequestValues(reportType, a);

        generateReport.execute(requestValues, new UseCase.OutputBoundary<GenerateReport.ResponseValue>() {
            @Override
            public void onSuccess(GenerateReport.ResponseValue result) {

                List<VaccineReport> vaccineReports = result.getCoverageReport().getVaccineReportList();

                updateBasicInfo(vaccineReports);

            }

            @Override
            public void onError(String errorMessage) {

            }
        });





    }
      */

    AdapterReportsAlt adapterReports;
    public void updateBasicInfo(List<VaccineReport> reportList){
        Collections.sort(reportList);

        List<VaccineReport>  unDuplicatesList = new ArrayList<>(new LinkedHashSet<>(reportList));


        adapterReports = new AdapterReportsAlt(unDuplicatesList, getContext());
        // adapterReports.replaceItems(careTakers);
        adapterReports.setHasStableIds(true);
        adapterReports.notifyItemInserted(0);
        reportsFragmentBinding.recyclerViewReports.setAdapter(adapterReports);

    }







    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

      //  inflater.inflate(R.menu.menu_home, menu);

      //  MenuItem itemSearch = menu.findItem(R.id.action_hospital_schedule);
      //  itemSearch.setVisible(false);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        /*

        int id = item.getItemId();
        switch (id) {

            case android.R.id.home:
                //supportFinishAfterTransition();

                return true;

            case R.id.action_search:
                return true;
        }
        */

        return super.onOptionsItemSelected(item);
    }








}
