package com.vims.vimsapp.view.patients.addchild;

import android.app.ActivityOptions;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.presenters.addchild_presenter.AddChildViewModel;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.MyDateMonth;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.injection.ModuleUI;
import com.vims.vimsapp.view.patients.detail.MyDetailsTypeConverter;
import com.vims.vimsapp.view.patients.detail.ViewChildDetailsActivity;

import vimsapp.R;
import vimsapp.databinding.PatientsChildReviewFragmentBinding;


public class AddChildReviewFragment extends Fragment implements BlockingStep {

    AddChildActivityWizardViewModel addChildActivityWizardViewModel;
    AddChildActivityWizardViewModelFactory viewModelFactory;
    PatientsChildReviewFragmentBinding reviewBinding;


    AddChildViewModel addChildViewModel;


    PreferenceHandler preferenceHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        reviewBinding  = DataBindingUtil.inflate(inflater, R.layout.patients_child_review_fragment, container, false);

        //setSupportActionBar(toolbar);
        //itemView =  inflater.inflate(R.layout.patients_activity_add_caretaker, container, false);

       //viewModelFactory = new AddChildActivityWizardViewModelFactory(Injection.provideRegisterChildUseCase(getActivity().getApplication()), Injection.provideGenerateChildScheduleUseCase(getActivity().getApplication()));
       //addChildActivityWizardViewModel =  ViewModelProviders.of(getActivity(),viewModelFactory).get(AddChildActivityWizardViewModel.class);
        addChildActivityWizardViewModel = ModuleUI.provideInteractionViewModel(getActivity());
        addChildViewModel = ModuleUI.providePreviewViewModel(this);


        //setUpActionBar();
        preferenceHandler = PreferenceHandler.getInstance(getContext());

        //Initialize your UI
        observeName();
        observeDateOfBirth();
        observeGender();
        observeChildWeight();
        observeNullId();


        //When UI is updated
        observeClearDetail();
        observeShowSuccess();




        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        //  setUpTickAnimation();
            tickCross = reviewBinding.tickCross;
            tickToCross = (AnimatedVectorDrawable) getContext().getResources().getDrawable(R.drawable.avd_tick_to_add);
            crossToTick = (AnimatedVectorDrawable) getContext().getResources().getDrawable(R.drawable.avd_add_to_tick);

        }


        /*
        if(isAboveKitkat()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //    animateSuccess();
                AnimatedVectorDrawable drawable = tick ? crossToTick : tickToCross;
                tickCross.setImageDrawable(drawable);
                drawable.start();
                tick = !tick;
            }
        }
        */

            // setUpTickAnimation();
        return reviewBinding.getRoot();

    }





    //MORPHING ANIMATION
    private ImageView tickCross;
    private AnimatedVectorDrawable tickToCross;
    private AnimatedVectorDrawable crossToTick;
    private boolean tick = true;




    private void setUpTickAnimation(){

    }


    private void animateSuccess(){

    }



    public void observeName(){

        Observer<CharSequence> nameObserver = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence s) {

                String names = ""+s;
                reviewBinding.tvPreviewChildFirstname.setTypeface(MyFonts.getTypeFace(getContext()));

                reviewBinding.tvPreviewChildFirstname.setText(names);

            }};

        addChildActivityWizardViewModel.getmName().observe(this,nameObserver);

    }
    public void observeDateOfBirth(){

        Observer<CharSequence> ageObserver = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence s) {

                String[] dateArrray = new String[3];

                if(!TextUtils.isEmpty(s)) {
                    //11/10/2018

                    dateArrray = s.toString().split("/");  //MMMM d, yyyy

                    MyDateMonth myDateMonth =  DateCalendarConverter.dateToMyMonth(Integer.parseInt(dateArrray[0]));
                    String dateFinal = ""+ myDateMonth.getMonthName()+" "+dateArrray[1]+", "+dateArrray[2];

                    reviewBinding.tvPreviewChildBirthdate.setText(dateFinal);

                }


            }};

        addChildActivityWizardViewModel.getMdateOfBirth().observe(this,ageObserver);

    }
    public void observeGender(){

        Observer<CharSequence> phone2Observer = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence s) {


                if(s!=null) {
                    String gender = "baby " + s;

                    reviewBinding.tvPreviewChildGender.setText(gender);

                    if (s.equals("girl")) {

                       // reviewBinding.imageView4.setImageDrawable(getResources().getDrawable(R.drawable.baby_girl));

                    } else {
                       // reviewBinding.imageView4.setImageDrawable(getResources().getDrawable(R.drawable.baby_boy));


                    }
                }

            }};

        addChildActivityWizardViewModel.getmGender().observe(this,phone2Observer);

    }
    public void observeChildWeight(){

        Observer<CharSequence> districtObserver = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence s) {

                String w = s+" kg";

                reviewBinding.tvPreviewChildWeight.setText(w);

            }};

        addChildActivityWizardViewModel.getmTotalWeight().observe(this,districtObserver);

    }
    public void observeNullId(){

        Observer<String> idObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(getContext(),s, Toast.LENGTH_LONG).show();

            }};

        addChildActivityWizardViewModel.getmBirthPlaceError().observe(this,idObserver);

    }


    private void notifySuccessToRemoveWizardButtons(){
        addChildActivityWizardViewModel.setmScheduleGenerateResponse(true);
    }

    //OBSERVATION OF SUCCESS
    private void observeClearDetail(){

        Observer<Boolean> progress = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null && !aBoolean){
                    reviewBinding.registrationProgress.setVisibility(View.INVISIBLE);
                    notifySuccessToRemoveWizardButtons();
                }
            }
        };
        addChildViewModel.getmIsShowProgressBar().observe(this,progress);


        Observer<Boolean> detailGrid = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null && !aBoolean){
                    reviewBinding.grid.setVisibility(View.INVISIBLE);
                }
            }
        };
        addChildViewModel.getmIsShowChildDetailGrid().observe(this,detailGrid);


        Observer<Boolean> dash = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null && !aBoolean){

                    reviewBinding.view.setVisibility(View.INVISIBLE);
                }
            }
        };
        addChildViewModel.getmIsShowTitleDashLine().observe(this,dash);


    }
    private void observeShowSuccess(){

        Observer<Boolean> colors = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null && aBoolean){
                    //Colors
                    reviewBinding.tvPreviewChildFirstname.setTextColor(getResources().getColor(R.color.colorPrimaryTint3));
                    reviewBinding.tvPreviewChildFirstnameLabel.setTextColor(getResources().getColor(R.color.colorPrimary));

                }
            }
        };
        addChildViewModel.getmIsShowColorsOfDescTitleSubtitle().observe(this,colors);



        Observer<Boolean> wrapper = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null && aBoolean){

                    reviewBinding.wrapChild.setBackground(getResources().getDrawable(R.drawable.shape_borderline_default_cp_review));
                    reviewBinding.viewDetailsNow.setVisibility(View.GONE);

                }
            }
        };
        addChildViewModel.getmIsShowChildSuccessWrapper().observe(this,wrapper);


        Observer<Boolean> animation = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null && aBoolean){

                    reviewBinding.tickCross.setImageDrawable(getResources().getDrawable(R.drawable.ic_animation_done));

                }
            }
        };
        addChildViewModel.getmIsShowSuccessAnimation().observe(this,animation);


        Observer<Boolean> caretaketReg = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null && aBoolean){

                    //To mark current caretaker reg to use when deciding to refresh list/not
                    preferenceHandler.setIntValue(PreferenceHandler.IS_CHILD_REGISTERED_INT,1);

                }
            }
        };
        addChildViewModel.getmIsMarkCaretakerAsRegistered().observe(this,caretaketReg);



        Observer<String> title = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                reviewBinding.tvPreviewChildFirstname.setText(s);

            }
        };
        addChildViewModel.getmSuccessDescTitle().observe(this,title);


        Observer<String> subtitle = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                reviewBinding.tvPreviewChildFirstnameLabel.setVisibility(View.VISIBLE);
                reviewBinding.tvPreviewChildFirstnameLabel.setText(s);

            }
        };
        addChildViewModel.getmSuccessDescSubtitle().observe(this,subtitle);



    }



    private void viewChildDetails(Child child){

        String childString = MyDetailsTypeConverter.fromChildToString(child);


        Intent intent = new Intent(getContext(), ViewChildDetailsActivity.class);

        Bundle bundle = new Bundle();
        //bundle.putString("child",childString);
        bundle.putString("child_id", child.getChildId());
        // bundle.putInt("child_done_vacs",getDoneEvents(child.getVaccinationEvents()));
        intent.putExtras(bundle);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            //shared Element Transitions
            //ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, p1, p2, p3);
            //    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, view, transition_name);
            // view.getContext().startActivity(intent, options.toBundle());

            //Exit Transitions
            Bundle optionsBundle = ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle();
            getContext().startActivity(intent, optionsBundle);


        } else {
            getContext().startActivity(intent);
        }

    }



    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {

    }



    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

        if(addChildActivityWizardViewModel.checkCaretakerId()) {

            reviewBinding.registrationProgress.setVisibility(View.VISIBLE);


            //At this point after gathering the user data
            //call the controller and pass it your unformated data
            //which  is text from the view
            addChildActivityWizardViewModel.registerChild(getContext(), Injection.provideController(this,getActivity().getApplication()));

            /*
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    getActivity().finish();
                }
            };
            new MainThreadExecutor(5000).execute(runnable);
            */

        }

    }



    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {

        callback.goToPrevStep();


    }


    @Override
    public VerificationError verifyStep() {
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        return null;
    }

    @Override
    public void onSelected() {
        //update UI when selected
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        //handle error inside of the fragment, e.g. show error on EditText
    }







    //////////////////////////////NOT IN USE //////////////////////////////////////////////
    /*
    public void observeScheduleGeneration(){

        Observer<Child> responseObserver = new Observer<Child>() {
            @Override
            public void onChanged(@Nullable Child child) {

                if(child!=null){
                    String success = "registered successfully";
                    String fullname = ""+child.getFirstName()+" "+child.getLastName()+" "+success;
                    String advice = "You can now view and vaccinate this child";


                    reviewBinding.tvPreviewChildFirstname.setText(advice);
                    reviewBinding.tvPreviewChildFirstnameLabel.setText(fullname);
                    reviewBinding.wrapChild.setBackground(getResources().getDrawable(R.drawable.shape_borderline_default_cp_review));


                    reviewBinding.tvPreviewChildFirstnameLabel.setVisibility(View.VISIBLE);
                    reviewBinding.registrationProgress.setVisibility(View.INVISIBLE);
                    reviewBinding.grid.setVisibility(View.INVISIBLE);
                    reviewBinding.view.setVisibility(View.INVISIBLE);
                    reviewBinding.viewDetailsNow.setVisibility(View.VISIBLE);

                    reviewBinding.tickCross.setImageDrawable(getResources().getDrawable(R.drawable.ic_animation_done));

                    //Colors
                    reviewBinding.tvPreviewChildFirstname.setTextColor(getResources().getColor(R.color.colorPrimaryTint3));
                    reviewBinding.tvPreviewChildFirstnameLabel.setTextColor(getResources().getColor(R.color.colorPrimary));


                    //To mark current caretaker reg to use when deciding to refresh list/not
                    preferenceHandler.setIntValue(PreferenceHandler.IS_CHILD_REGISTERED_INT,1);



                    //reviewBinding.patientsChildReviewContent.setVisibility(View.INVISIBLE);


                    reviewBinding.viewDetailsNow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            getActivity().finish();
                            viewChildDetails(child);


                        }
                    });

                }






            }};

        addChildActivityWizardViewModel.getmScheduleGenerateResponse().observe(this, responseObserver);


    }



      */







}
