package com.vims.vimsapp.view.vaccinations;

public class CardHeader {

    private String title;
    private String subTitle;
    private String immunisationMonth;
    private String immunisationCoverage;
    private int immunisationCoveragePercentage;

    public int getImmunisationCoveragePercentage() {
        return immunisationCoveragePercentage;
    }

    public void setImmunisationCoveragePercentage(int immunisationCoveragePercentage) {
        this.immunisationCoveragePercentage = immunisationCoveragePercentage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }


    public String getImmunisationMonth() {
        return immunisationMonth;
    }

    public void setImmunisationMonth(String immunisationMonth) {
        this.immunisationMonth = immunisationMonth;
    }

    public String getImmunisationCoverage() {
        return immunisationCoverage;
    }

    public void setImmunisationCoverage(String immunisationCoverage) {
        this.immunisationCoverage = immunisationCoverage;
    }
}
