package com.vims.vimsapp.view.patients.list;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;


import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.searchchild.ViewCaretakers;

import java.util.List;

/**
 * Created by root on 5/30/18.
 */

public class ViewCaretakerListFragmentViewModel extends ViewModel {


     private  MutableLiveData<List<Caretaker>>  caretakers;
     private  MutableLiveData<String>  errorMessage;
     private  MutableLiveData<Boolean> mIsStartProgress;


     private ViewCaretakers viewCaretakers;

    public ViewCaretakerListFragmentViewModel(ViewCaretakers viewCaretakers) {

        this.viewCaretakers = viewCaretakers;

        caretakers = new MutableLiveData<>();
        errorMessage = new MutableLiveData<>();



    }



    public MutableLiveData<String> getErrorMessage() {
        return errorMessage;
    }

    public LiveData<List<Caretaker>> getCaretakers() {
        if(caretakers == null){
            caretakers = new MutableLiveData<>();

        }
        return caretakers;
    }


    public MutableLiveData<Boolean> getIsStartProgress() {

        return mIsStartProgress;
    }

    private void setIsStartProgress(boolean isStartProgress) {
        if(mIsStartProgress == null){
            mIsStartProgress = new MutableLiveData<>();
        }
        this.mIsStartProgress.postValue(isStartProgress);
    }

    public void loadCaretakers() {

        setIsStartProgress(true);

       // patientsRepository = PatientsRepository.getInstance(null,null,this.getApplication());
        ViewCaretakers.RequestValues requestValues = new ViewCaretakers.RequestValues(false);

       viewCaretakers.execute(requestValues, new UseCase.OutputBoundary<ViewCaretakers.ResponseValue>() {
           @Override
           public void onSuccess(ViewCaretakers.ResponseValue result) {

               setIsStartProgress(false);
               caretakers.postValue(result.getmCaretakers());

           }

           @Override
           public void onError(String e) {
               setIsStartProgress(false);

           }
       });


        /*
        List<Caretaker> children = patientsRepository.getCaretakers();
        if (children != null) {

            for (Caretaker caretaker : children) {
                Log.i("caretaker ",""+children.get(0).getLastName());

                String caretakerId = Integer.toString(caretaker.getId());

                List<Child> children = patientsRepository.getChildrenByCaretakerId(caretakerId);
                if(!children.isEmpty()) {
                    for (Child child: children) {
                        Log.i("care children", "" + child.getFirstName());

                    }
                    Log.i("care children size", "" + children.size());

                    caretaker.setChildren(children);
                }
                Log.i("care children size", "" + children.size());


            }

            setCaretakersWithChildren(children);

        }
        */
    }



       // ViewCaretakers viewCaretakerInteractor;
       // viewCaretakerInteractor = new ViewCaretakers(new PatientsRepository(this.getApplication()));
       // viewCaretakerInteractor.execute(this);





}
