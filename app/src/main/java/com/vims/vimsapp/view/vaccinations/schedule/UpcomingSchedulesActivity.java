package com.vims.vimsapp.view.vaccinations.schedule;

import android.app.ActivityOptions;
import android.app.SearchManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsSchedule;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.utilities.alarm.UIAlert;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.injection.ModuleRepository;
import com.vims.vimsapp.view.patients.detail.MyDetailsTypeConverter;
import com.vims.vimsapp.view.patients.list.ItemOffsetDecoration;
import com.vims.vimsapp.view.vaccinations.CardHeader;
import com.vims.vimsapp.view.vaccinations.order.ViewOrderActivity;
import com.vims.vimsapp.view.vaccinations.viewmodel.ViewScheduleUpcomingFragmentViewModel;
import com.vims.vimsapp.view.vaccinations.viewmodel.ViewScheduleUpcomingFragmentViewModelFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import vimsapp.R;
import vimsapp.databinding.VaccinationFragmentScheduleDoneBinding;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UpcomingSchedulesActivity.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class UpcomingSchedulesActivity extends AppCompatActivity {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    VaccinationFragmentScheduleDoneBinding vaccinationScheduleBinding;

    ViewScheduleUpcomingFragmentViewModel viewScheduleFragmentViewModel;
    ViewScheduleUpcomingFragmentViewModelFactory scheduleUpcomingFragmentViewModelFactory;



    public UpcomingSchedulesActivity() {
        // Required empty public constructor
    }



    // TODO: Rename and change types and number of parameters


    EventState eventState;
    Button requestOrderButton;
    String orderevents = "";
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


       // vaccinationHomeBinding = DataBindingUtil.inflate(inflater, R.layout.vaccination_fragment_home, container, false);

        vaccinationScheduleBinding = DataBindingUtil.setContentView(this,R.layout.vaccination_fragment_schedule_done);


       toolbar = vaccinationScheduleBinding.toolbar;
       toolbar.setTitle("");



       toolbar.setNavigationOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               //finish();
               Toast.makeText(getApplicationContext(),"close pressed",Toast.LENGTH_LONG).show();
           }
       });



       setSupportActionBar(toolbar); //we replace our actionbar with our toolbar

       // ScreenManager.setScreenOrientation(this);




        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.setTitle("");
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        scheduleUpcomingFragmentViewModelFactory = new ViewScheduleUpcomingFragmentViewModelFactory(Injection.provideReadChildSchedule(getApplication()), Injection.provideRecordVaccinationUseCase(getApplication()), Injection.provideGenerateReport(getApplication()), ModuleRepository.provideSearchVaccinationRepository(getApplication()),Injection.provideHospitalSchedule(getApplication()), getApplication() );
        viewScheduleFragmentViewModel = ViewModelProviders.of(this, scheduleUpcomingFragmentViewModelFactory).get(ViewScheduleUpcomingFragmentViewModel.class);


        setUpRecyclerView();
        fonts();
       // vaccinationScheduleBinding.setViewModel(viewScheduleFragmentViewModel);



        observeUpcomingSchedule();
        observeErrorMessage();
        observeProgressBar();
        //observeRecordedVaccinaton();

        setupDrawables();

        //item sinner sets the spinner text color
        /*
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.months_array, R.layout.item_spinner);
        adapter.setDropDownViewResource(R.layout.item_spinner_dropdown);
        vaccinationScheduleBinding.monthSpinner.setAdapter(adapter);
        */

        //  setupViewPager(savedInstanceState);
        //  setupTabIcons();
        //  spinnerListner();


        requestOrderButton = vaccinationScheduleBinding.requestOrder;


        requestOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        FloatingActionButton fab = vaccinationScheduleBinding.btnOrder;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ViewOrderActivity.class);
                //  intent.putExtra("orderevents",OrderConverter.someObjectListToString(vaccinationEvents));
                startActivity(intent);
            }
        });


        Bundle bundle = getIntent().getExtras();
        if(bundle!=null) {
            int month = bundle.getInt("nextScheduledMonth");
            int year = bundle.getInt("nextScheduledYear");
            setUpYearSpinner(year);
            setUpSpinner(month);

        }

        eventState = new EventState(getApplicationContext());


        setUpSearch();
        observeRecord();
    }



    private void observeRecord(){

        Observer<String> ob = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                UIAlert.showAlert("Vaccination",s,UpcomingSchedulesActivity.this,UIAlert.AlertType.INFO);
                //Toast.makeText(getApplicationContext(),""+s,Toast.LENGTH_LONG).show();
                finish();
            }
        };
        viewScheduleFragmentViewModel.getRecordResponse().observe(this,ob);

    }


    private void fonts(){

        vaccinationScheduleBinding.tvMonthlyPlanDate.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
        vaccinationScheduleBinding.textViewMonthPlan.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
    }

    private void toastMsg(String msg){
        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
    }

    private void setUpSearch(){

      //  this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        /*
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchViewItem.getActionView();
        searchView.setIconifiedByDefault(false);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        searchView.setLayoutParams(params);
        searchViewItem.expandActionView();
        */

        //Inflate the menu_home; this adds items to the action bar if it is present.
        //Get the search view

        /*
         getMenuInflater().inflate(R.menu.menu_search, menu);
         MenuItem searchViewItem = menu.findItem(R.id.action_search);
         SearchView searchView = (SearchView) searchViewItem.getActionView();
        */

        SearchView searchView = findViewById(R.id.searchbox);
        //searchView.setFocusable(true);// searchView is null
        //searchView.setFocusableInTouchMode(true);
        searchView.setIconifiedByDefault(false);
        searchView.clearFocus();//do not need keyboard openned
        //searchView.onActionViewExpanded();
        //searchView.requestFocusFromTouch();
        searchView.setIconified(false);

        /*
        search.setIconifiedByDefault(true);
        search.setFocusable(true);
        search.setIconified(false);
        search.clearFocus();
        search.requestFocusFromTouch();
        */

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);


        /**
        android.app.ActionBar.LayoutParams params = new android.app.ActionBar.LayoutParams(android.app.ActionBar.LayoutParams.MATCH_PARENT, android.app.ActionBar.LayoutParams.MATCH_PARENT);
        searchView.setLayoutParams(params);
         **/

        //Android guides explicitly says:
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        // Which is in your case NOT TRUE. Because you don't want to noticemgt in MainActivity but in SearchableActivity.
        // That means you should use this ComponentName instead of getComponentName() method:

        //----  ComponentName cn = new ComponentName(this, ActivitySearch.class);

        //---- searchView.setSearchableInfo(searchManager.getSearchableInfo(cn));
        //   searchView.setBackgroundColor(Color.WHITE);
        // Do not iconify the widget;expand it by default
        //searchView.setIconifiedByDefault(false);
        // searchView.setSubmitButtonEnabled(true);


        /*
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
                vaccinationScheduleBinding.searchText.setVisibility(View.INVISIBLE);

            }
        });
        */


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

               // Toast.makeText(getApplicationContext(),""+query, Toast.LENGTH_LONG).show();
               // vaccinationScheduleBinding.progressbarUpcomingEvents.setVisibility(View.VISIBLE);

                if(adapterUpcomingEvents!=null) {
                    adapterUpcomingEvents.getFilter().filter(query);
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //adapterCaretakers.getFilter().filter(newText);

                if (TextUtils.isEmpty(newText)){
                    //Text is cleared, do your thing
                    //searchView.setIconified(true);
                    int month_of_spinner = PreferenceHandler.getInstance(getApplicationContext()).getIntValue(PreferenceHandler.ORDER_MONTH_KEY);
                    String month = DateCalendarConverter.monthHashMap().get(month_of_spinner);


                    Calendar calendar = Calendar.getInstance();
                    int thisYear  = calendar.get(Calendar.YEAR);
                    viewScheduleFragmentViewModel.loadUpcomingVaccinationEvents(month,thisYear, SearchVaccinationsSchedule.VaccinationsRequestType.DUE);
                   // vaccinationScheduleBinding.searchText.setVisibility(View.VISIBLE);
                }


                return false;
            }
        });

        searchView.setQueryHint("Search child, vaccine");


        //searchViewItem.expandActionView();
        //this makes back button work with search view
        //searchView.setFocusableInTouchMode(true);


        /*
        searchViewItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do whatever you need
                Toast.makeText(getApplicationContext(),"item expanded",Toast.LENGTH_LONG).show();

                return true; // KEEP IT TO TRUE OR IT DOESN'T OPEN !!
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do whatever you need
                Toast.makeText(getApplicationContext(),"item collapsed",Toast.LENGTH_LONG).show();

                return true; // OR FALSE IF YOU DIDN'T WANT IT TO CLOSE!
            }
        });
        */



        searchView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {

            @Override
            public void onViewDetachedFromWindow(View arg0) {
                // search was detached/closed
                //  viewCaretakerListFragmentViewModel.loadCaretakers();

                //  Toast.makeText(getContext(),"closed", Toast.LENGTH_LONG).show();
                Calendar calendar = Calendar.getInstance();
                int thisMonth  = calendar.get(Calendar.MONTH);
                int thisYear  = calendar.get(Calendar.YEAR);

                String month = DateCalendarConverter.monthHashMap().get(thisMonth);
                viewScheduleFragmentViewModel.loadUpcomingVaccinationEvents(month,thisYear,SearchVaccinationsSchedule.VaccinationsRequestType.DUE);


            }

            @Override
            public void onViewAttachedToWindow(View arg0) {
                // search was opened
            }
        });



    }

    private void setupDrawables(){

        vaccinationScheduleBinding.tvErrorRegister.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_group_add_cs_16dp),null,null,null);
        vaccinationScheduleBinding.errorMsgTv.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
        vaccinationScheduleBinding.errorMsgTvTitle.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
        vaccinationScheduleBinding.tvErrorRegister.setTypeface(MyFonts.getTypeFace(getApplicationContext()));

    }


    String month = "February";


    @Override
    public void onResume() {
        super.onResume();


        if(getApplication()!=null) {
            if (adapterUpcomingEvents != null) {

                String vaccinationRecorded = eventState.getPref("recorded");
                if (vaccinationRecorded.equals("1")) {

                    eventState.putPref("recorded", "0");

                    String index = eventState.getPref("recordedIndex");
                    int indexRecorded = Integer.parseInt(index);

                    adapterUpcomingEvents.removeItem(indexRecorded);
                    adapterUpcomingEvents.notifyItemRemoved(indexRecorded);
                    // scheduleHomeBinding.recyclerview.setAdapter(adapterUpcomingEvents);
                 //   viewScheduleFragmentViewModel.loadUpcomingVaccinationEvents(month);



                } else if (vaccinationRecorded.equals("0")) {

                    //scheduleHomeBinding.recyclerview.setAdapter(adapterUpcomingEvents);
                    //scheduleHomeBinding.recyclerview.setAdapter(adapterUpcomingEvents);
                    //adapterUpcomingEvents.notifyDataSetChanged();

                   // viewScheduleFragmentViewModel.loadUpcomingVaccinationEvents(month);
                }
            }

        }


    }


    private void setOrderMonth(int month){

        PreferenceHandler preferenceHandler = PreferenceHandler.getInstance(getApplicationContext());
        preferenceHandler.setIntValue(PreferenceHandler.ORDER_MONTH_KEY,month);


        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        preferenceHandler.setIntValue(PreferenceHandler.ORDER_DAY_KEY,day);


    }


    Spinner yearSpinner;
    private void setUpYearSpinner(int year){
        yearSpinner =  vaccinationScheduleBinding.spinnerUpcomingYear;
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int nextYear = currentYear+1;
        ArrayList<Integer> years = new ArrayList<>();
        years.add(currentYear);
        years.add(nextYear);
        ArrayAdapter<Integer> yearadapter = new ArrayAdapter<>(this,
                R.layout.item_spinner,
                years
        );


        int selectionIndex;
        if(year == 2019){
            selectionIndex = 0;
        }else{
            selectionIndex = 1;
        }


        yearadapter.setDropDownViewResource(R.layout.item_spinner_dropdown);
        yearSpinner.setAdapter(yearadapter);

        yearSpinner.setSelection(selectionIndex);


        yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                 int selectedYear = (Integer)parent.getSelectedItem();


                //get the year from the other year spinner
                String selectedMonth =  eventsSpinner.getSelectedItem().toString();
                int month = DateCalendarConverter.monthStringToInt(selectedMonth);
                setOrderMonth(month);


                String s ="y:"+selectedYear+" m"+selectedMonth;

              //  Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();

                //using viewmodel
                viewScheduleFragmentViewModel.loadUpcomingVaccinationEvents(selectedMonth,selectedYear,SearchVaccinationsSchedule.VaccinationsRequestType.DUE);



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    Spinner eventsSpinner;
    private void setUpSpinner(int scheduledMonth){

        //Set order month
        //First disable the order button
        /*
        boolean isEnd = DateCalendarConverter.isEligiblePlaceOrder(scheduledMonth);
        if(isEnd){
            requestOrderButton.setEnabled(true);
        }else{
            requestOrderButton.setEnabled(false);
            requestOrderButton.setTextColor(getResources().getColor(R.color.grey_pallete_300));
        }
        */
        eventsSpinner =  vaccinationScheduleBinding.spinnerUpcomingEvents;
        eventsSpinner.setSelection(scheduledMonth);


        List<String> theMonths = getSpinnerMonths(scheduledMonth);

        // (3) create an adapter from the list
        ArrayAdapter<String> myadapter = new ArrayAdapter<>(this,
                R.layout.item_spinner,
                theMonths
        );


        //item sinner sets the spinner text color
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.month_array, R.layout.item_spinner_light);
        myadapter.setDropDownViewResource(R.layout.item_spinner_dropdown);
        //  vaccinationScheduleBinding.spinnerUpcomingEvents.setAdapter(adapter);
        //eventsSpinner.setBackground(getResources().getDrawable(ThemeManager.getSpinnerDrawable()));
        eventsSpinner.setAdapter(myadapter);

        eventsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getSelectedItem().toString();

                int month = DateCalendarConverter.monthStringToInt(item);

                setOrderMonth(month);

                //get the year from the other year spinner
                int selectedYear = (Integer) yearSpinner.getSelectedItem();


                String s ="y:"+selectedYear+" m"+item;

              //  Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();


                //using viewmodel
               viewScheduleFragmentViewModel.loadUpcomingVaccinationEvents(item,selectedYear,SearchVaccinationsSchedule.VaccinationsRequestType.DUE);

                //without viewmodel
              //  loadUpcomingVaccinationEvents(item);
              //  Toast.makeText(getApplicationContext(),""+item, Toast.LENGTH_LONG).show();


                String date = item+" "+DateCalendarConverter.dateToStringGetYear(new Date());
               // vaccinationScheduleBinding.tvMonthDate.setText(date);

                final CollapsingToolbarLayout collapsingToolbarLayout = vaccinationScheduleBinding.collapsingToolbar;
                collapsingToolbarLayout.setTitleEnabled(false);
              //  collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
                //collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
                collapsingToolbarLayout.setTitle(date);

                vaccinationScheduleBinding.tvMonthlyPlanDate.setText(date);



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }



    List<String> monthArray = new ArrayList<>();
    private List<String> getSpinnerMonths(int month){


        monthArray.clear();
        Map<Integer,String> map = DateCalendarConverter.monthHashMap();


        for(Map.Entry<Integer, String> entry: map.entrySet()){
            if(entry.getKey() >= month){

            monthArray.add(entry.getValue());
            }
        }

        return monthArray;
    }





    SearchVaccinationsSchedule readChildSchedule;
    public void loadUpcomingVaccinationEvents(String month){
        readChildSchedule = Injection.provideReadChildSchedule(getApplication());


        viewScheduleFragmentViewModel.setLoadProgress(ViewScheduleUpcomingFragmentViewModel.ProgressStatus.LOADING);

        SearchVaccinationsSchedule.RequestValues requestValues = new SearchVaccinationsSchedule.RequestValues(SearchVaccinationsSchedule.VaccinationsRequestType.DUE);
        requestValues.setMonth(month);


        readChildSchedule.execute(requestValues, new UseCase.OutputBoundary<SearchVaccinationsSchedule.ResponseValue>() {
            @Override
            public void onSuccess(SearchVaccinationsSchedule.ResponseValue result) {


                //setNextVaccinationDate(result.getNextVaccinationDate());
                viewScheduleFragmentViewModel.setLoadProgress(ViewScheduleUpcomingFragmentViewModel.ProgressStatus.SUCCESS);

                //get events
                List<VaccinationEvent> vaccinationEvents = result.getVaccinationEvents();

              //  Toast.makeText(getApplicationContext(),""+vaccinationEvents.get(0).getScheduledDate(),Toast.LENGTH_LONG).show();

                Collections.sort(vaccinationEvents);

                updateUpcomingEvents(vaccinationEvents);

                vaccinationScheduleBinding.layoutError.setVisibility(View.INVISIBLE);
                vaccinationScheduleBinding.recyclerview.setVisibility(View.VISIBLE);


            }

            @Override
            public void onError(String theErrorMessage) {

                viewScheduleFragmentViewModel.setErrorMessage(theErrorMessage);
                viewScheduleFragmentViewModel.setLoadProgress(ViewScheduleUpcomingFragmentViewModel.ProgressStatus.FAILED);


            }
        });




    }


    RecyclerView.LayoutManager mLayoutManager;
    DividerItemDecoration decoration;
    public void setUpRecyclerView(){
        boolean isTablet = getResources().getBoolean(R.bool.tablet);
        boolean isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);

        decoration = new DividerItemDecoration(getApplicationContext(), VERTICAL);
      //  vaccinationScheduleBinding.recyclerview.addItemDecoration(decoration);

        //setup the recycler view basic info
        SlideInUpAnimator animator = new SlideInUpAnimator(new OvershootInterpolator(1f));
        vaccinationScheduleBinding.recyclerview.setItemAnimator(animator);
        vaccinationScheduleBinding.recyclerview.getItemAnimator().setAddDuration(500);
        //setting animation on scrolling

        // scheduleHomeBinding.recyclerview.setHasFixedSize(true); //enhance recycler view scroll
        // scheduleHomeBinding.recyclerview.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getApplicationContext(), R.dimen.item_offset_schedule);
        vaccinationScheduleBinding.recyclerview.addItemDecoration(itemDecoration);
        //  RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL, false);



        if(isTablet){

            //if(!isPotrait){
                //mLayoutManager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL, false);
                mLayoutManager  = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);

           // }


        }else {
            // if(!isPotrait){
            mLayoutManager  = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);

            // }

        }

        vaccinationScheduleBinding.recyclerview.setLayoutManager(mLayoutManager);


    }


    public class AdapterSpinnerListner implements AdapterUpcomingEvents.OnSpinnerClickedListner{

        @Override
        public void onSpinnerClicked(String item) {
         if(adapterUpcomingEvents!=null){
            // adapterUpcomingEvents.getFilter().filter(item);
             //viewScheduleFragmentViewModel.loadUpcomingVaccinationEvents(item);
             month = item;


         }
        }
    }


    public class AdapterVaccinationEventListner implements AdapterUpcomingEvents.OnVaccinationCLickLIstner {

        @Override
        public void onVaccinationClicked(View view, int position, VaccinationEvent vaccinationEvent) {

            Bundle bundle = new Bundle();
            bundle.putString("vevent", MyDetailsTypeConverter.fromVaccinationEventToString(vaccinationEvent));
            bundle.putInt("position",position);


            String transitionName  = getResources().getString(R.string.transitionNameEvent);
            Intent intent = new Intent(getApplicationContext(),ComfirmVaccinationActivity.class);
            intent.putExtras(bundle);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                //shared Element Transitions
                // ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, p1, p2, p3);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(UpcomingSchedulesActivity.this, view, transitionName);
                startActivity(intent, options.toBundle());
                //Exit Transitions
                //Bundle bundle = ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle();
                //context.startActivity(intent, bundle);

            } else {
                startActivity(intent);
            }

           // toggleBottomSheet(vaccinationEvent);


        }
    }


    AdapterUpcomingEvents adapterUpcomingEvents;

    List<VaccinationEvent> observedEvents;



    public void updateUpcomingEvents(List<VaccinationEvent> vaccinationEvents){

        AdapterUpcomingEvents.clearInstance();
        observedEvents = vaccinationEvents;



       // String stringcard = OrderConverter.someObjectListToString(vaccinationEvents);
       // int month = vaccinationEvents.get(0).getMonthOfEvent();
        //PreferenceHandler preferenceHandler = new PreferenceHandler(getApplicationContext());
        //preferenceHandler.putPref("orderlist",stringcard);

      //  int month = vaccinationEvents.get(0).getMonthOfEvent();




        //VaccinationStatistic vstat =  viewScheduleFragmentViewModel.getStatistic(vaccinationEvents.size());
        /// String cardTitle = "vaccinations:"+vstat.getTotalNoOfFinishedVaccinations()+"/"+vstat.getTotalNoOfVaccinations();
        String date =  DateCalendarConverter.dateToStringWithoutTimeAndDay(new Date());

        String card = "";

        CardHeader cardHeader = new CardHeader();
        cardHeader.setTitle(date);
        cardHeader.setSubTitle(card);
        //cardHeader.setImmunisationCoveragePercentage(vstat.getTotalVaccinationPercentage());

        adapterUpcomingEvents = AdapterUpcomingEvents.getINSTANCE(cardHeader,vaccinationEvents, getApplication(), Injection.provideRecordVaccinationUseCase(getApplication()));
        adapterUpcomingEvents.setActivity(getParent());
        adapterUpcomingEvents.setClickListener(new AdapterVaccinationEventListner());
        //adapterUpcomingEvents.setSpinnerClickListner(new AdapterSpinnerListner());

        if (adapterUpcomingEvents.getItemCount()<1) {
            Log.d("FUS","count < 1 resumed");
            vaccinationScheduleBinding.layoutError.setVisibility(View.VISIBLE);
            vaccinationScheduleBinding.recyclerview.setVisibility(View.INVISIBLE);
        }

 
        //adapterVaccinationEvents.setHasStableIds(true);ooncomingEvents.notifyItemInserted(0);
        int prevSize = vaccinationEvents.size();
        adapterUpcomingEvents.notifyItemRangeInserted(prevSize, vaccinationEvents.size() -prevSize);

        vaccinationScheduleBinding.recyclerview.setNestedScrollingEnabled(false);

        vaccinationScheduleBinding.recyclerview.postDelayed(new Runnable() {
            @Override
            public void run() {
                vaccinationScheduleBinding.recyclerview.setAdapter(adapterUpcomingEvents);

             //   adapterUpcomingEvents.getFilter().filter("January");

            }
        },100);


    }




    TextView tvMonthPlan;

    private void observeUpcomingSchedule(){
        tvMonthPlan = vaccinationScheduleBinding.textViewMonthPlan;

        Observer<List<VaccinationEvent>> vcardsObserver = new Observer<List<VaccinationEvent>>() {
            @Override
            public void onChanged(@Nullable List<VaccinationEvent> vaccinationEvents) {

                if(vaccinationEvents != null) {
                    vaccinationScheduleBinding.progressbarUpcomingEvents.setVisibility(View.INVISIBLE);

                    Collections.sort(vaccinationEvents);

                    List<VaccinationEvent>  unDuplicatesList = new ArrayList<>(new LinkedHashSet<>(vaccinationEvents));

                    String itemsSize = "No. doses: "+unDuplicatesList.size()+"";
                    tvMonthPlan.setText(itemsSize);

                    if(!vaccinationEvents.isEmpty()) {
                        updateUpcomingEvents(unDuplicatesList);
                    }else{
                       // setUpSpinner();
                    }

                }
            }
        };

        viewScheduleFragmentViewModel.upcomingVaccinations().observe(this, vcardsObserver);

    }

    private void observeErrorMessage(){

        Observer<String> ellapsedObs = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();

               vaccinationScheduleBinding.recyclerview.setVisibility(View.GONE);
               vaccinationScheduleBinding.layoutError.setVisibility(View.VISIBLE);

            }
        };
        //decalare observer
        viewScheduleFragmentViewModel.getErrorMessage().observe(this,ellapsedObs);
    }


   /*
    private void spinnerListner(){


        vaccinationScheduleBinding.monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                String monthSelected = parent.getSelectedItem().toString();

                // August 2, 2018

                ArrayList<VaccinationEvent> monthEvents = new ArrayList<>();

                for(VaccinationEvent vaccinationEvent: monthVaccinationEvents){

                    String mdy = vaccinationEvent.getScheduledDate();
                    String[] spliter = mdy.split(" ");

                    String eventMonth = spliter[0];

                    Log.d("SPINNER","EventMonth "+eventMonth+" Selected"+monthSelected);

                    if(monthSelected.equals(eventMonth)){

                     monthEvents.add(vaccinationEvent);

                    }

                }


                updateDoneEvents(monthEvents);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


       });
    }


   */






    public void observeProgressBar(){

        Observer<ViewScheduleUpcomingFragmentViewModel.ProgressStatus> progressObserver = new Observer<ViewScheduleUpcomingFragmentViewModel.ProgressStatus>() {
            @Override
            public void onChanged(@Nullable ViewScheduleUpcomingFragmentViewModel.ProgressStatus progressStatus) {

                if(progressStatus!=null) {
                    if (progressStatus == ViewScheduleUpcomingFragmentViewModel.ProgressStatus.LOADING) {

                        vaccinationScheduleBinding.progressbarUpcomingEvents.setVisibility(View.VISIBLE);



                    }else if(progressStatus == ViewScheduleUpcomingFragmentViewModel.ProgressStatus.FAILED){

                        vaccinationScheduleBinding.progressbarUpcomingEvents.setVisibility(View.INVISIBLE);
                        vaccinationScheduleBinding.layoutError.setVisibility(View.VISIBLE);

                        vaccinationScheduleBinding.recyclerview.setVisibility(View.INVISIBLE);


                        String doses = "0 doses";
                        tvMonthPlan.setText(doses);




                    }else if(progressStatus == ViewScheduleUpcomingFragmentViewModel.ProgressStatus.SUCCESS){

                        vaccinationScheduleBinding.progressbarUpcomingEvents.setVisibility(View.INVISIBLE);
                        vaccinationScheduleBinding.layoutError.setVisibility(View.INVISIBLE);
                        vaccinationScheduleBinding.recyclerview.setVisibility(View.VISIBLE);




                    }
                }

            }
        };

        viewScheduleFragmentViewModel.getLoadProgress().observe(this,progressObserver);

    }








    ///these icons go on tabs
    private int[] tabIcons = {
         //   R.drawable.ic_battery_full_black_24dp,
          //  R.drawable.ic_battery_20_black_24dp,
           // R.drawable.ic_battery_alert_black_24dp
    };



    /*
    //setup tab icons
    private void setupTabIcons() {

        TabLayout.Tab tab1 = vaccinationScheduleBinding.tabsCaretaker.getTabAt(0);
        if(tab1!=null) {
            tab1.setIcon(tabIcons[0]);
        }

        TabLayout.Tab tab2 = vaccinationScheduleBinding.tabsCaretaker.getTabAt(1);
        if(tab2!=null) {
            tab2.setIcon(tabIcons[1]);
        }

        TabLayout.Tab tab3 = vaccinationScheduleBinding.tabsCaretaker.getTabAt(2);
        if(tab3!=null) {
            tab3.setIcon(tabIcons[2]);
        }


    }
     */

    /*
    //set up the fragments on the viewpager
    private void setupViewPager(Bundle bundle) {


        //setup the viewpager--- it contains the fragments to be swiped
        ViewPager viewPager =  vaccinationScheduleBinding.viewpagerHome;
        //assign viewpager to tablayout
        vaccinationScheduleBinding.tabsCaretaker.setupWithViewPager(viewPager);



        AdapterViewPager adapter = new AdapterViewPager(getSupportFragmentManager());//child


       // adapter.addFragment(FragmentUpcomingSchedule.newInstance("due"), "Overview");
        adapter.addFragment(FragmentDoneSchedule.newInstance("done"), "Completed");
        adapter.addFragment(FragmentMissedSchedule.newInstance("overdue"), "Overdue");



        viewPager.setAdapter(adapter);



    }

      */

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    /**
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    **/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
