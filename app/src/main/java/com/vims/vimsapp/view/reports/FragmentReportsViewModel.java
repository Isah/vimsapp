package com.vims.vimsapp.view.reports;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.searchchild.ViewChildDataSource;
import com.vims.vimsapp.searchchild.ViewChildRepository;

import com.vims.vimsapp.viewcoverage.CoverageReport;
import com.vims.vimsapp.viewcoverage.GenerateReport;
import com.vims.vimsapp.viewcoverage.VaccineReportsCard;
import com.vims.vimsapp.utilities.DateCalendarConverter;

import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.view.patients.detail.MyDetailsTypeConverter;

import java.util.Date;
import java.util.List;

public class FragmentReportsViewModel extends ViewModel {


    private GenerateReport generateReport;
    private MutableLiveData<CoverageReport> coverageReportMutableLiveData;

    private MutableLiveData<String> errorMutable;

    private MutableLiveData<String> errorEachVaccineReportMutable;

    private PreferenceHandler preferenceHandler;
    private Application mContext;



    private ViewChildRepository vaccinationsRepository;

    public FragmentReportsViewModel(GenerateReport generateReport, ViewChildRepository  vaccinationsRepository, Application application) {
        this.generateReport = generateReport;
        this.vaccinationsRepository = vaccinationsRepository;
        this.mContext = application;
        preferenceHandler = PreferenceHandler.getInstance(mContext);

    }



    public enum ReportProgress{
         IN_PROGRESS,LOAD_SUCCESS,LOAD_FAIL

    }


    private MutableLiveData<ReportProgress> progressMutableLiveData;

    public LiveData<ReportProgress> getProgressMutableLiveData() {
        if(progressMutableLiveData == null){
            progressMutableLiveData = new MutableLiveData<>();
        }
        return progressMutableLiveData;
    }

    public void setProgressMutableLiveData(ReportProgress progress) {
        if(progressMutableLiveData == null){
            progressMutableLiveData = new MutableLiveData<>();
        }
        this.progressMutableLiveData.postValue(progress);
    }

    public LiveData<String> getError() {
        if(errorMutable==null){
            errorMutable = new MutableLiveData<>();
        }
        return errorMutable;
    }

    public void setError(String error) {
        if(errorMutable==null){
            errorMutable = new MutableLiveData<>();
        }
        this.errorMutable.postValue(error);
    }


    public LiveData<String> getErrorEachVaccineReportMutable() {
        if(errorEachVaccineReportMutable==null){
            errorEachVaccineReportMutable = new MutableLiveData<>();
        }
        return errorEachVaccineReportMutable;
    }

    public void setErrorEachVaccineReportMutable(String errorEachVaccineReport) {
        if(errorEachVaccineReportMutable==null){
            errorEachVaccineReportMutable = new MutableLiveData<>();
        }
        this.errorEachVaccineReportMutable.postValue(errorEachVaccineReport);
    }

    //reports
    public LiveData<CoverageReport> getCoverageReportMutableLiveData() {
        if(coverageReportMutableLiveData == null){
            coverageReportMutableLiveData = new MutableLiveData<>();
        }
        return coverageReportMutableLiveData;
    }

    public void setCoverageReportMutableLiveData(CoverageReport coverageReport) {
        coverageReportMutableLiveData.postValue(coverageReport);
    }


    private MutableLiveData<Boolean> isDataChanged;

    private Integer childrenCardSizeCompare;


    private int childrenCardSize = 0;

    public void loadReports(boolean isRefresh, String vaccine, int year){
        if(coverageReportMutableLiveData==null){
            coverageReportMutableLiveData = new MutableLiveData<>();
            childrenCardSizeCompare = 0;
        }
        if(isDataChanged==null){
            isDataChanged = new MutableLiveData<>();
        }


    vaccinationsRepository.checkChildCardsSize(new ViewChildDataSource.LoadChildCardsSizeCallback() {
        @Override
        public void onCardsSizeLoaded(int size) {

            childrenCardSize = size; //5

            int x = preferenceHandler.getIntValue("size");

            Log.d("CardSize","Size: "+childrenCardSize+" ComapreSize: "+x);

                    if (childrenCardSize > x) { //5 > 4

                        //means data has changed
                        preferenceHandler.setIntValue("size",size);

                        loadGraphData(isRefresh,true,vaccine,year);

                    } else if(childrenCardSize == 0){//less or equal no data has changed

                          setError("No Data E1 v");

                    }else { // there's data but no change
                       // loadGraphData(true,vaccine,year);

                        String key = "report-"+vaccine;
                        String savedReport = preferenceHandler.getPref(key);
                        //CoverageReport coverageReport = MyDetailsTypeConverter.fromStringToCoverageReport(savedReport);
                      //  setCoverageReportMutableLiveData(coverageReport);


                       loadGraphData(isRefresh,false,vaccine,year);


                    }

            }



        @Override
        public void onCardNotAvailable() {

            setError("Cards UnAvailable");

        }

       });


    }



    private MutableLiveData<String> mEllapsedTime;

    public LiveData<String> getEllapsedTime() {
        if(mEllapsedTime == null){
            mEllapsedTime = new MutableLiveData<>();
        }
        return mEllapsedTime;
    }

    private void setEllapsedTime(String ellapsedTime) {
        if(mEllapsedTime == null){
            mEllapsedTime = new MutableLiveData<>();
        }
        mEllapsedTime.postValue(ellapsedTime);
    }



    private void loadGraphData(boolean isResfresh, boolean isDataChanged, String vaccineName, int year){
        //plot a graph of coverage percentage with months

        long startTime = System.currentTimeMillis();
        setProgressMutableLiveData(ReportProgress.IN_PROGRESS);


        String searchDate = DateCalendarConverter.dateToStringWithoutTimeAndDay(new Date());
        //searchDate used only to get months
        GenerateReport.ReportType reportType = GenerateReport.ReportType.VACCINE_SPECIFIC_STATS;

       // GenerateReport.ReportType reportType = GenerateReport.ReportType.API;


        GenerateReport.RequestValues requestValues = new GenerateReport.RequestValues(reportType,searchDate);
        requestValues.setTypeOfVaccine(vaccineName);
        requestValues.setYear(year);
        requestValues.setDataChanged(isDataChanged);
        requestValues.setRefresh(isResfresh);


        generateReport.execute(requestValues, new UseCase.OutputBoundary<GenerateReport.ResponseValue>() {
            @Override
            public void onSuccess(GenerateReport.ResponseValue result) {

                long endTime = System.currentTimeMillis();
                long ellapsedTime = endTime - startTime;
                setEllapsedTime(Long.toString(ellapsedTime));


                CoverageReport coverageReport = result.getCoverageReport();
              //  List<VaccineReport> vaccineReports =  coverageReport.getVaccineReportsCard().getVaccineReports();

                String report = MyDetailsTypeConverter.fromCoverageReportToString(coverageReport);
                String key = "report-"+vaccineName;
                preferenceHandler.putPref(key,report);

                setCoverageReportMutableLiveData(coverageReport);

                setProgressMutableLiveData(ReportProgress.LOAD_SUCCESS);



            }

            @Override
            public void onError(String errorMessage) {


                setProgressMutableLiveData(ReportProgress.LOAD_FAIL);
                setError(errorMessage);

            }
        });

    }




    private MutableLiveData<List<VaccineReportsCard>> vaccineReportsMutableLiveData;

    public LiveData<List<VaccineReportsCard>> getVaccineReportsMutableLiveData() {
        return vaccineReportsMutableLiveData;
    }

    public void setVaccineReportsMutableLiveData(List<VaccineReportsCard> vaccineReports) {
        this.vaccineReportsMutableLiveData.postValue(vaccineReports);
    }


    public void generateReportsForEachVaccine(){
        if(vaccineReportsMutableLiveData==null){
            vaccineReportsMutableLiveData = new MutableLiveData<>();
        }


        GenerateReport.ReportType reportType = GenerateReport.ReportType.VACCINE_COVERAGE_YEARLY;

        String yearOfReport =  DateCalendarConverter.dateToStringGetYear(new Date());

        //String yearOfReport = "2018";

        GenerateReport.RequestValues requestValues = new GenerateReport.RequestValues(reportType,yearOfReport);


        generateReport.execute(requestValues, new UseCase.OutputBoundary<GenerateReport.ResponseValue>() {
            @Override
            public void onSuccess(GenerateReport.ResponseValue result) {

                CoverageReport coverageReport = result.getCoverageReport();

               // List<VaccineReportsCard> vaccineReportsCards = coverageReport.getVaccineReportsCards();

              //  setVaccineReportsMutableLiveData(vaccineReportsCards);

            }

            @Override
            public void onError(String errorMessage) {

               setErrorEachVaccineReportMutable(errorMessage);

            }
        });



    }












}
