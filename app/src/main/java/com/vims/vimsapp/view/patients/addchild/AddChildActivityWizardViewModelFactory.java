package com.vims.vimsapp.view.patients.addchild;


import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.vims.vimsapp.registerchild.RegisterChild;
import com.vims.vimsapp.registerchild.GenerateChildSchedule;


/**
 * Created by root on 6/3/18.
 */
public class AddChildActivityWizardViewModelFactory implements ViewModelProvider.Factory{

    private final RegisterChild registerChild;
    private final GenerateChildSchedule generateChildSchedule;

    public AddChildActivityWizardViewModelFactory(RegisterChild registerChild, GenerateChildSchedule generateChildSchedule) {
        this.registerChild = registerChild;
        this.generateChildSchedule = generateChildSchedule;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if(modelClass.isAssignableFrom(AddChildActivityWizardViewModel.class)){

            return (T) new AddChildActivityWizardViewModel(registerChild, generateChildSchedule);

        }
        else{

            throw new IllegalArgumentException("View Model not found");
        }

    }




}
