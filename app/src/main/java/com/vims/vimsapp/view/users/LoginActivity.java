package com.vims.vimsapp.view.users;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.vims.vimsapp.MainActivity;

import com.vims.vimsapp.entities.schedule.Hospital;
import com.vims.vimsapp.managehospitalschedule.ManageHospitalScheduleDataSource;
import com.vims.vimsapp.managehospitalschedule.ManageHospitalScheduleRepository;
import com.vims.vimsapp.managehospitalschedule.ReadHospitals;
import com.vims.vimsapp.manageorders.VaccinePlan;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsRepository;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsSchedule;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.MainThreadExecutor;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.utilities.ThemeManager;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.injection.ModuleRepository;
import com.vims.vimsapp.view.injection.ModuleUI;
import com.vims.vimsapp.view.users.app.AppConfig;
import com.vims.vimsapp.view.users.app.AppController;
import com.tapadoo.alerter.Alerter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vims.vimsapp.utilities.Sentinel;
import vimsapp.R;

import static com.vims.vimsapp.utilities.ScreenManager.setScreenOrientation;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    private static final String TAG = com.vims.vimsapp.view.users.RegisterActivity.class.getSimpleName();
    private Button btnLogin;
    private Button btnLinkToRegister;


    private TextInputLayout inputLayoutEmail;
    private TextInputLayout inputLayoutPasscode;

    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;






    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setScreenOrientation(this);

        inputEmail = findViewById(R.id.email);
        inputPassword = findViewById(R.id.password);
        btnLogin = findViewById(R.id.btnLogin);

        inputLayoutEmail = findViewById(R.id.textInputLayoutEmail);
        inputLayoutPasscode = findViewById(R.id.textInputLayoutPasscode);

        btnLogin.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
        btnLinkToRegister = findViewById(R.id.btnLinkToRegisterScreen);

        TextView textView = findViewById(R.id.textViewSignUptitle);
        textView.setTypeface(MyFonts.getTypeFace(getApplicationContext()));


       setupDrawables();

      // spinner();
        spinnerHospital();


        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Session manager
        session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }


    }


    private void setupDrawables(){

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
      //  inputEmail.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_vims_add_email),null,null,null);
       // inputPassword.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_lock_black_24dp),null,null,null);

    }


    private final String USER_DEFAULT = "Default";
    private final String USER_DOCTOR = "Doctor";


    private void loginSetUp(String userType){

        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Sentinel sentinel;

                String password = "";
                String email = "";


                if(userType.equals("Default")){
                    password = "Default";
                    email = "vims@gmail.com";
                    checkLogin(USER_DEFAULT,email, password);

                }else{
                    password = inputPassword.getText().toString().trim();
                    email = inputEmail.getText().toString().trim();
                    // /check whether email is valid
                    if (!Sentinel.isValid(email)) {
                        //showAlert("Login Error", "Please check your email!", R.color.errorColor);

                        Snackbar snackbar = Snackbar.make(btnLogin, "Please check your email!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(4000);
                        snackbar.show();
                        // showSnackBarView(snackbar);

                    }
                    else{
                        // Check for empty data in the form
                        if (!TextUtils.isEmpty(email) && !password.isEmpty()) {
                            //   login user
                            //  //bypasses login
                            // loginTemporary("6936",email,password,"petr","123456789");
                            checkLogin(USER_DOCTOR,email, password);

                        } else {
                            // Prompt user to enter credentials


                            Snackbar snackbar = Snackbar.make(btnLogin, "Please enter your credentials!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(4000);
                            snackbar.show();
                            //showSnackBarView(snackbar);
                            //showAlert("Login Error", "Please enter your credentials!", R.color.errorColor);

                        }

                    }

                }


                //Get Selected Item
                //check whether its default user


            }

        });

        // Link to Register Screen
        btnLinkToRegister.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                /**Intent i = new Intent(getApplicationContext(),
                 RegisterActivity.class);
                 startActivity(i); **/
                showAlert("Notice", "Contact admin for account details", R.color.colorPrimary);



                //makeOrderUseMail(plans,3);



            }
        });




    }




    TextView tvHospitalName;
    TextView tvLoginTitle;

    boolean isResetHospital;

    private void spinnerHospital(){

        tvHospitalName = findViewById(R.id.tv_existing_hospital);
        tvLoginTitle = findViewById(R.id.textViewSignUptitle);

        Spinner eventsSpinner =  findViewById(R.id.spinnerHospital);

        PreferenceHandler pref = PreferenceHandler.getInstance(getApplicationContext());

        String hospital = pref.getPref(PreferenceHandler.USER_HOSPITAL_NAME);
        if(!TextUtils.isEmpty(hospital)){

            String welcome = "Tap to continue";
            tvLoginTitle.setText(welcome);
            tvHospitalName.setText(hospital);
            tvHospitalName.setVisibility(View.VISIBLE);
           eventsSpinner.setVisibility(View.INVISIBLE);


        }else{

            eventsSpinner.setVisibility(View.VISIBLE);
            tvHospitalName.setVisibility(View.GONE);

        }


        eventsSpinner.setSelection(1);

        //item sinner sets the spinner text color
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.hospital_array, R.layout.item_spinner_blue);
        adapter.setDropDownViewResource(R.layout.item_spinner_dropdown_blue);
        //  vaccinationScheduleBinding.spinnerUpcomingEvents.setAdapter(adapter);
       // eventsSpinner.setBackground(getResources().getDrawable(ThemeManager.getSpinnerDrawable()));
        eventsSpinner.setAdapter(adapter);

        eventsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                inputLayoutEmail.setVisibility(View.GONE);
                inputLayoutPasscode.setVisibility(View.GONE);


                String item = parent.getSelectedItem().toString();

                btnLogin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(!TextUtils.isEmpty(hospital)){


                            session.setLogin(true);

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);

                            new MainThreadExecutor(2000).execute(new Runnable() {
                                @Override
                                public void run() {
                                    hideDialog();
                                }
                            });



                        }else{



                            for(Hospital hospital: ReadHospitals.getHardCodedHospitals()) {

                                if (hospital.getHospitalName().toLowerCase().equals(item.toLowerCase())) {

                                    //lets use this hospital

                                    pref.putPref(PreferenceHandler.USER_NAME, "vimsuganda");
                                    //pref.putPref(PreferenceHandler.USER_PASSWORD,password);
                                    pref.putPref(PreferenceHandler.USER_ID, "vimsuganda@gmail.com");
                                    //pref.putPref(PreferenceHandler.USER_TOKEN,user_token);
                                    pref.putPref(PreferenceHandler.USER_EMAIL,"vimsuganda@gmail.com");


                                    // Inserting row in users table
                                    // Launch main activity
                                    pref.putPref(PreferenceHandler.USER_HOSPITAL_ID,hospital.getHospitalId());
                                    pref.putPref(PreferenceHandler.USER_HOSPITAL_NAME,hospital.getHospitalName());
                                    pref.putPref(PreferenceHandler.USER_HOSPITAL_ADDRESS,hospital.getDistrict());


                                    //Add a token for communication
                                    //pref.putPref(PreferenceHandler.USER_TOKEN,""+hospitalCode+"-"+hospital.getVimServerId());
                                    session.setLogin(true);

                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(intent);

                                    new MainThreadExecutor(2000).execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            hideDialog();
                                        }
                                    });

                                }
                            }



                        }





                    }
                });


                    String text = "Proceed";
                    btnLogin.setText(text);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }



    private void spinner(){


        Spinner eventsSpinner =  findViewById(R.id.spinnerLogin);
        eventsSpinner.setSelection(1);


        List<String> theMonths = new ArrayList<>();
        theMonths.add(USER_DEFAULT);
        theMonths.add(USER_DOCTOR);

        // (3) create an adapter from the list
        ArrayAdapter<String> myadapter = new ArrayAdapter<>(this,
                R.layout.item_spinner,
                theMonths
        );

        //item sinner sets the spinner text color
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.login_user_array, R.layout.item_spinner_light);
        myadapter.setDropDownViewResource(R.layout.item_spinner_dropdown);
        //  vaccinationScheduleBinding.spinnerUpcomingEvents.setAdapter(adapter);
        //eventsSpinner.setBackground(getResources().getDrawable(ThemeManager.getSpinnerDrawable()));
       // eventsSpinner.setAdapter(myadapter);

        eventsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getSelectedItem().toString();
                if(item.equals(USER_DEFAULT)){

                    inputLayoutEmail.setVisibility(View.GONE);
                    inputLayoutPasscode.setVisibility(View.GONE);


                    String text = "Proceed";
                    btnLogin.setText(text);


                    loginSetUp(USER_DEFAULT);


                }else{

                    String text = "Login";
                    btnLogin.setText(text);

                    inputLayoutEmail.setVisibility(View.VISIBLE);
                   // inputLayoutPasscode.setVisibility(View.VISIBLE);

                    loginSetUp(USER_DOCTOR);

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }





    private void isHospitalExist(String hospitalCode){
        //at this momment the user has loggedin
        //first save the user Id, and user Token to be used by the apis , e,g
        //checking and getting hospital details
        String login_title = "Login Failed";

      ManageHospitalScheduleRepository repository = ModuleRepository.provideManageHospitalScheduleRepository(getApplication());
      repository.getRegisteredHospital(hospitalCode, new ManageHospitalScheduleDataSource.isHospitalExistCallback() {
          @Override
          public void onAvailable(Hospital hospital) {
              //Log.d("HospitalDetail", hospital.getHospitalName());
              session.setLogin(true);
              // Inserting row in users table
              // Launch main activity
              PreferenceHandler pref = PreferenceHandler.getInstance(getApplicationContext());
              pref.putPref(PreferenceHandler.USER_HOSPITAL_ID,hospital.getHospitalId());
              pref.putPref(PreferenceHandler.USER_HOSPITAL_NAME,hospital.getHospitalName());
              pref.putPref(PreferenceHandler.USER_HOSPITAL_ADDRESS,hospital.getDistrict());


              //Add a token for communication
              pref.putPref(PreferenceHandler.USER_TOKEN,""+hospitalCode+"-"+hospital.getVimServerId());

              Intent intent = new Intent(LoginActivity.this, MainActivity.class);
              startActivity(intent);


              new MainThreadExecutor(2000).execute(new Runnable() {
                  @Override
                  public void run() {
                    hideDialog();
                  }
              });


          }

          @Override
          public void onNotAvailable(String error) {
             // showAlert(login_title, ""+error, R.color.errorColor);
              hideDialog();

              Snackbar snackbar = Snackbar.make(btnLogin, error, Snackbar.LENGTH_LONG);
              snackbar.setDuration(4000);
              snackbar.show();
              //showSnackBarView(snackbar);


          }
      });



    }



    private void loginTemporary(String hospitalId, String username, String password, String userId, String userToken){
        // db.addUser("petr", "petr@gmail.com", "12345", "x");
        //  session.setLogin(true);
        // Launch main activity
        //  Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        //  startActivity(intent);

        //set user login details
        PreferenceHandler pref = PreferenceHandler.getInstance(getApplicationContext());
        pref.putPref(PreferenceHandler.USER_NAME,username);
        pref.putPref(PreferenceHandler.USER_PASSWORD,password);
        pref.putPref(PreferenceHandler.USER_ID, userId);
        pref.putPref(PreferenceHandler.USER_TOKEN,userToken);

        isHospitalExist(hospitalId);


        /*
        PreferenceHandler pref = PreferenceHandler.getInstance(getApplicationContext());
        pref.putPref(PreferenceHandler.USER_NAME,username);
        pref.putPref(PreferenceHandler.USER_PASSWORD,password);
        pref.putPref(PreferenceHandler.USER_ID, UUID.randomUUID().toString());
        pref.putPref(PreferenceHandler.USER_HOSPITAL_ID,"5ceda2846c44052e9bb59d0f");// 5c25221f6c4405162f987b05
        pref.putPref(PreferenceHandler.USER_HOSPITAL_NAME,"Kyeibanga HC II");
        pref.putPref(PreferenceHandler.USER_TOKEN,"123456789asdfghjkl");
        */

    }







    /**
     * function to verify login details in mysql db
     */
    private void checkLogin(final String userType, final String email, final String hospital_code) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        String login_title = "Login Failed";

        pDialog.setMessage("Logging in ...");
        showDialog();

        PreferenceHandler pref = PreferenceHandler.getInstance(getApplicationContext());

        if (userType.equals("Default")) {
            pref.putPref(PreferenceHandler.USER_NAME, "Default");
            //pref.putPref(PreferenceHandler.USER_PASSWORD,password);
            pref.putPref(PreferenceHandler.USER_ID, "Default-1");
            //pref.putPref(PreferenceHandler.USER_TOKEN,user_token);
            pref.putPref(PreferenceHandler.USER_EMAIL, email);

            isHospitalExist(hospital_code);//hospital-code

        }else{
        StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response.toString());

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session

                        // Now store the user in SQLite
                        String uid = jObj.getString("uid");

                        JSONObject user = jObj.getJSONObject("user");
                        String name = user.getString("name");
                        String user_id = user.getString("id");
                        String email = user.getString("email");
                        String user_token = user.getString("token");
                        String hospital_id = user.getString("facility_id");
                        //String created_at = user.getString("created_at");

                        //  Log.d("HospitalDetail", hospital_id);

                        //user has passesd test 1

                        pref.putPref(PreferenceHandler.USER_NAME, name);
                        //pref.putPref(PreferenceHandler.USER_PASSWORD,password);
                        pref.putPref(PreferenceHandler.USER_ID, user_id);
                        //pref.putPref(PreferenceHandler.USER_TOKEN,user_token);
                        pref.putPref(PreferenceHandler.USER_EMAIL, email);

                        db.addUser(name, email, user_id, "2018");

                        // bob@gmail.com
                        // User myuser = new User(name,password,email,user_id,user_token);
                        //http://70.32.24.226:8080/vim/webapi/scheduleAPI/mobile/getDefaultEvents/5cfa4d770a975a04fc8638a5
                        //isHospitalExist(hospital_id);

                        isHospitalExist(hospital_code);//hospital-code


                    } else {
                        hideDialog();
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        ///showAlert(login_title, errorMsg, R.color.errorColor);

                        Snackbar snackbar = Snackbar.make(btnLogin, errorMsg, Snackbar.LENGTH_LONG);
                        snackbar.setDuration(4000);
                        snackbar.show();
                        //showSnackBarView(snackbar);


                    }
                } catch (JSONException e) {
                    // JSON error
                    hideDialog();
                    e.printStackTrace();
                    //showAlert(login_title, e.getMessage(), R.color.errorColor);

                    Snackbar snackbar = Snackbar.make(btnLogin, e.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.setDuration(4000);
                    snackbar.show();
                    // showSnackBarView(snackbar);

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e(TAG, "Login Error: " + error.getMessage());
                // showAlert(login_title, "Check your internet connection", R.color.errorColor);
                hideDialog();
                Snackbar snackbar = Snackbar.make(btnLogin, "Can't connect to internet!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(4000);
                snackbar.show();
                // showSnackBarView(snackbar);


                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);

                String pass = "12345";

                // params.put("password", pass);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }




    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void showAlert(String Title, String content, int colorInt) {
        Alerter.create(this)
                .setTitle(Title)
                .setText(content)
                .setBackgroundColorRes(colorInt) // or setBackgroundColorInt(Color.CYAN)
                .show();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}

