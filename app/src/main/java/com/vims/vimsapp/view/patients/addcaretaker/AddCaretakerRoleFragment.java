package com.vims.vimsapp.view.patients.addcaretaker;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;


import vimsapp.R;
import vimsapp.databinding.PatientsCaretakerAddRoleFragmentBinding;


///**
// * A simple {@link Fragment} subclass.
// * Use the {@link AddCaretakerNameFragment#newInstance} factory method to
// * create an instance of this fragment.
// */
public class AddCaretakerRoleFragment extends Fragment implements BlockingStep {
    // TODO: Rename parameter arguments, choose names that match
   //  the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    CaretakerActivityWizardViewModel caretakerActivityWizardViewModel;
   // binding name with view
    PatientsCaretakerAddRoleFragmentBinding caretakerAddRoleFragmentBinding;




    public AddCaretakerRoleFragment() {
      //   Required empty public constructor
    }



    public static AddCaretakerRoleFragment newInstance(String param1, String param2) {
        AddCaretakerRoleFragment fragment = new AddCaretakerRoleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

       setHasOptionsMenu(true); //to enable or display actions
    }

   @Override
   public void onPrepareOptionsMenu(Menu menu) {

        MenuItem itemSearch = menu.findItem(R.id.action_search);
      // itemSearch.setVisible(false);

       MenuItem itemSettings = menu.findItem(R.id.action_settings);
      // itemSettings.setVisible(false);
   }



   CaretakerActivityViewModelFactory caretakerActivityViewModelFactory;

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       caretakerAddRoleFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.patients_caretaker_add_role_fragment, container, false);

        //setSupportActionBar(toolbar);


       caretakerActivityViewModelFactory = new CaretakerActivityViewModelFactory(Injection.provideRegisterCaretakerUseCase(getActivity().getApplication()));

        caretakerActivityWizardViewModel =  ViewModelProviders.of(getActivity(), caretakerActivityViewModelFactory).get(CaretakerActivityWizardViewModel.class);
       //setUpActionBar();



        onButtonClicked();
        observeCaretakerRole();
      //  phoneNumber1TextChanged();
       // phoneNumber2TextChanged();
       // ageTextChanged();
       // observeBioErrors();

        boolean isTablet = getResources().getBoolean(R.bool.tablet);
        boolean isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);

        if(isTablet) {
            CardView.LayoutParams layoutParams = (CardView.LayoutParams) caretakerAddRoleFragmentBinding.cardView.getLayoutParams();

            if(isPotrait) {


                layoutParams.setMargins(80,8,80,8);

                //  caretakerAddRoleFragmentBinding.cardView.setLayoutParams(layoutParams);
            }else{

                layoutParams.setMargins(200,8,200,8);


            }
        }


        //set variables in Binding
        return caretakerAddRoleFragmentBinding.getRoot();
    }




    public void onButtonClicked(){


        caretakerAddRoleFragmentBinding.btnPreviewRoleFather.setCompoundDrawablesWithIntrinsicBounds(null,getResources().getDrawable(R.drawable.ic_vims_person_father_select), null,null);
        caretakerAddRoleFragmentBinding.btnPreviewRoleMother.setCompoundDrawablesWithIntrinsicBounds(null,getResources().getDrawable(R.drawable.ic_vims_person_mother_select), null,null);

        caretakerAddRoleFragmentBinding.btnPreviewRoleFather.setCompoundDrawablePadding(30);
        caretakerAddRoleFragmentBinding.btnPreviewRoleMother.setCompoundDrawablePadding(30);

        // caretakerAddRoleFragmentBinding.btnPreviewRoleFather.setBackground(getResources().getDrawable(R.drawable.shape_borderline_thick));
        caretakerAddRoleFragmentBinding.tvWizardDatebirth.setTypeface(MyFonts.getTypeFace(getContext()));
        caretakerAddRoleFragmentBinding.btnPreviewRoleFather.setTypeface(MyFonts.getTypeFace(getContext()));
        caretakerAddRoleFragmentBinding.btnPreviewRoleMother.setTypeface(MyFonts.getTypeFace(getContext()));


        caretakerActivityWizardViewModel.setlRole("Father");
        caretakerAddRoleFragmentBinding.btnPreviewRoleFather.setSelected(true);
        caretakerAddRoleFragmentBinding.btnPreviewRoleMother.setSelected(false);


        caretakerAddRoleFragmentBinding.btnPreviewRoleFather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // caretakerAddRoleFragmentBinding.btnPreviewRoleMother.setPadding(32,32,32,32);
             //   caretakerAddRoleFragmentBinding.btnPreviewRoleFather.setPadding(32,32,32,32);



                caretakerActivityWizardViewModel.setlRole("Father");

                caretakerAddRoleFragmentBinding.btnPreviewRoleFather.setCompoundDrawablesWithIntrinsicBounds(null,getResources().getDrawable(R.drawable.ic_vims_person_father_select), null,null);



                //  caretakerAddRoleFragmentBinding.btnPreviewRoleMother.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
               // caretakerAddRoleFragmentBinding.btnPreviewRoleFather.setBackgroundColor(getResources().getColor(R.color.blue_grey_300));


                caretakerAddRoleFragmentBinding.btnPreviewRoleFather.setTypeface(MyFonts.getTypeFace(getContext()));
                caretakerAddRoleFragmentBinding.btnPreviewRoleMother.setTypeface(MyFonts.getTypeFace(getContext()));


                caretakerAddRoleFragmentBinding.btnPreviewRoleFather.setSelected(true);
                caretakerAddRoleFragmentBinding.btnPreviewRoleMother.setSelected(false);
                        // normal click action here


                ColorFilter colorFilter = new PorterDuffColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

             //   caretakerAddRoleFragmentBinding.btnPreviewRoleFather.getDrawable().setColorFilter(colorFilter);


                 caretakerActivityWizardViewModel.setmIsNextButtonEnabled(true);





            }
        });




        caretakerAddRoleFragmentBinding.btnPreviewRoleMother.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




            //    caretakerAddRoleFragmentBinding.btnPreviewRoleMother.setBackground(getResources().getDrawable(R.drawable.shape_borderline_thick));
               // caretakerAddRoleFragmentBinding.btnPreviewRoleGurdian.setBackground(getResources().getDrawable(R.drawable.shape_background_grey_secondary));
              //  caretakerAddRoleFragmentBinding.btnPreviewRoleFather.setBackground(getResources().getDrawable(R.drawable.shape_borderline_zero));

                caretakerAddRoleFragmentBinding.btnPreviewRoleMother.setTypeface(MyFonts.getTypeFace(getContext()));
                caretakerAddRoleFragmentBinding.btnPreviewRoleFather.setTypeface(MyFonts.getTypeFace(getContext()));


                caretakerAddRoleFragmentBinding.btnPreviewRoleMother.setCompoundDrawablesWithIntrinsicBounds(null,getResources().getDrawable(R.drawable.ic_vims_person_mother_select), null,null);


                caretakerActivityWizardViewModel.setlRole("Mother");
                caretakerAddRoleFragmentBinding.btnPreviewRoleFather.setSelected(false);
                caretakerAddRoleFragmentBinding.btnPreviewRoleMother.setSelected(true);

                caretakerActivityWizardViewModel.setmIsNextButtonEnabled(true);


            }
        });








    }




    public void observeCaretakerRole(){


        Observer<CharSequence> nameObserver = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence charSequence) {


                String title = ""+charSequence+"'s name ?";
              //  caretakerAddRoleFragmentBinding.tvWizardCaretakerNameTitle.setText(title);

            }
        };




    }




    public void observeBioErrors(){

        Observer<String> fnameErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();

            }};

        caretakerActivityWizardViewModel.getMfirstNameError().observe(this,fnameErrorObserver);


        Observer<String> lnameErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();

            }};
        caretakerActivityWizardViewModel.getMlastNameError().observe(this,lnameErrorObserver);


        Observer<String> ageErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();
            }};

        caretakerActivityWizardViewModel.getMBirthDateError().observe(this,ageErrorObserver);


        Observer<String> phone1ErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();

            }};

        caretakerActivityWizardViewModel.getmPhoneError().observe(this,phone1ErrorObserver);



    }



    private void goNext(StepperLayout.OnNextClickedCallback callback){

     callback.goToNextStep();

    }



    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        //if(caretakerActivityWizardViewModel.checkCaretakerBio()) {
            callback.goToNextStep();
       // }
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
           getActivity().finish();
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}
