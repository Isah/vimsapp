package com.vims.vimsapp.view.patients.list;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.support.constraint.ConstraintLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;


import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.searchchild.SearchPatient;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.patients.addchild.AddChildActivityWizard;
import com.vims.vimsapp.view.patients.addchild.AddChildWelcomeActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import vimsapp.R;

/**
 * Created by root on 5/31/18.
 */

public class AdapterCaretakers extends RecyclerView.Adapter<AdapterCaretakers.MyViewHolder> implements Filterable {


    List<Caretaker> caretakers;
    Context context;

    public AdapterCaretakers(List<Caretaker> caretakers, Context context) {
        this.context = context;
        this.caretakers = caretakers;
        //1. Animations bug
        setHasStableIds(true); //in constructor


        Caretaker caretakerAdd = new Caretaker();
        caretakerAdd.setFirstName("Add");
      //  caretakers.add(0,caretakerAdd);


    }



    private List<Caretaker> caretakersListFiltered;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                Log.d("AdapterCaretakers","search filter"+charString);

                if (charString.isEmpty()) {
                    caretakersListFiltered = caretakers;
                } else {
                    List<Caretaker> filteredList = new ArrayList<>();
                    for (Caretaker row : caretakers) {
                        Log.d("AdapterCaretakers","search item"+row.getFirstName());

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getFirstName().toLowerCase().contains(charString.toLowerCase()) || row.getLastName().toLowerCase().contains(charString.toLowerCase()) || row.getDistrict().contains(charSequence)) {
                            filteredList.add(row);
                            Log.d("AdapterCaretakers","search found"+charString.toLowerCase());

                        }
                    }

                    caretakersListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.count = caretakersListFiltered.size();
                filterResults.values = caretakersListFiltered;

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                Log.d("AdapterCaretakers","publish count: "+filterResults.count);

                if(filterResults.count>0) {
                    caretakers =   (ArrayList<Caretaker>) filterResults.values;
                  //caretakers.clear();
                 // caretakers = mycares;
                   // caretakers = (ArrayList<Caretaker>) filterResults.values;

                    //replaceItems(caretakersListFiltered);
                    notifyDataSetChanged();
                }else{

                    searchInDatabase(charSequence);

                }

            }
        };
    }



    private void searchInDatabase(CharSequence searchText){

        Activity activity = (Activity) context;

        SearchPatient searchCaretaker = Injection.provideSearchPatientsUseCase(activity.getApplication());
        SearchPatient.RequestValues requestValues = new SearchPatient.RequestValues(searchText, SearchPatient.Patient.CARETAKER);

        searchCaretaker.execute(requestValues, new UseCase.OutputBoundary<SearchPatient.ResponseValue>() {
            @Override
            public void onSuccess(SearchPatient.ResponseValue result) {
                caretakers = result.getCaretakers();
                //replaceItems(caretakersListFiltered);
                notifyDataSetChanged();
            }

            @Override
            public void onError(String errorMessage) {



            }
        });


    }


    public void replaceItems(List<Caretaker> caretakers){
        this.caretakers = caretakers;

        //1. Animations bug
//        setHasStableIds(true); //in constructor

    }



    //(1) Declare Interface
    protected OnCaretakerClickListener myclickListener;  //our interace
    // private WebView webView;



    boolean isTablet;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout_id  = 0;
         isTablet = context.getResources().getBoolean(R.bool.tablet);
        boolean isPotriat = context.getResources().getBoolean(R.bool.layout_orientation_potrait);
        if(isTablet){
               layout_id  = R.layout.patients_card_caretaker_vertical;
        }else {
            layout_id  = R.layout.patients_card_caretaker_horizontal;
           // layout_id  = R.layout.patients_card_caretaker_vertical;
        }


        View itemView = LayoutInflater.from(parent.getContext()).inflate(layout_id, parent, false);

        return new MyViewHolder(itemView);
    }


    //(2) implements the click listner
    public class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{
        public TextView fullname;
        public TextView district;
        public TextView phoneno;
        public TextView noChildren;
        public ImageView profilePic;
        public ImageView profilePicAdd;
        public ImageView addChild;
        public TextView addCaretakerLabel;
        public ConstraintLayout lBottom;
        public CardView cardViewCaretaker;


        public MyViewHolder(View view) {
            super(view);
            fullname = view.findViewById(R.id.firstname_tv);
            district = view.findViewById(R.id.tv_district);
            //phoneno =   view.findViewById(R.id.tv_phone);
            noChildren = view.findViewById(R.id.textViewNoChildren);
            profilePic = view.findViewById(R.id.iv_card_caretaker_profile);
            profilePicAdd = view.findViewById(R.id.iv_card_caretaker_profile_add);
            addChild = view.findViewById(R.id.imageViewAddChildMenu);
            lBottom = view.findViewById(R.id.lCaretakerCardBottom);
            addCaretakerLabel = view.findViewById(R.id.tvAddCaretakerLabel);
            cardViewCaretaker =view.findViewById(R.id.card_view_caretaker);


            //setFontface
            fullname.setTypeface(MyFonts.getTypeFace(view.getContext()));
            district.setTypeface(MyFonts.getTypeFace(view.getContext()));
            //phoneno.setTypeface(MyFonts.getTypeFace(view.getContext()));
            noChildren.setTypeface(MyFonts.getTypeFace(view.getContext()));
            addCaretakerLabel.setTypeface(MyFonts.getTypeFace(view.getContext()));


            //(3) Bind the click listener
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition(); // gets item position

            Caretaker caretaker = caretakers.get(position);

            myclickListener.onCaretakerClicked(v, position, caretaker, profilePic); // call the onClick in the OnCardItemClickListener Interface i created


        }

    }


    private void setProfilePic(MyViewHolder holder ,Caretaker caretaker){

        if(!TextUtils.isEmpty(caretaker.getProfileImagePath())){
            File imageFile = new File(caretaker.getProfileImagePath());
            Bitmap bProfilePic = BitmapFactory.decodeFile(imageFile.getPath());
            Log.i("Path Values 1", imageFile.getPath());
            Log.i("Path Values 2", imageFile.getAbsolutePath());

            //1. picasso
            //Picasso.with(context).load(imageFile).into(holder.profilePic);
            //2  decode
            //holder.profilePic.setImageBitmap(decodeSampledBitmapFromResource(imageFile.getPath(), 300, 300));
            //3. roundbitmap
            // Bitmap myBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.baby_girl);

            RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(context.getResources(), bProfilePic);
            drawable.setCircular(true);
            holder.profilePic.setImageDrawable(drawable);
        }else {

            Bitmap myBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.g_caretaker_sad_grey);
            RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(context.getResources(), myBitmap);
            drawable.setCircular(true);
            // holder.profilePic.setImageDrawable(drawable);


            // holder.profilePic.setImageDrawable(context.getResources().getDrawable(R.drawable.g_new_caretaker_cp));

            // holder.profilePic.setBackground(context.getResources().getDrawable(R.drawable.g_new_caretaker_cp));


        }
    }


    private String resizeName(String fullname, Caretaker caretaker){


        int fn_size = caretaker.getFirstName().length();
        int ln_size = caretaker.getLastName().length();


        if(fullname.length() >= 15){
            if(fn_size > ln_size) {

                char cut_fn = caretaker.getFirstName().charAt(0);
                String fn = ""+cut_fn;
                String fn_upper = fn.toUpperCase();

                char ln_fc = caretaker.getLastName().charAt(0);
                String ln_fc_string = ""+ln_fc;
                String ln_fc_upper = ln_fc_string.toUpperCase();


                fullname = fn_upper+". "+caretaker.getLastName();

              //  Log.d("CaretakerLengthN","fn: "+fullname);


            }else if(fn_size < ln_size){
                char cut_ln = caretaker.getLastName().charAt(0);
                fullname = cut_ln+" "+caretaker.getFirstName();

                Log.d("CaretakerLengthN","fn: "+fullname);

            }else{

                // String cut_ln = caretaker.getLastName().substring(3);
                // fullname = cut_ln+" "+caretaker.getLastName();

            }
        }
        return  fullname;

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Caretaker caretaker = getCaretakers(position);


        if(position==-5){ //it will never be


           // holder.cardViewCaretaker.setCardBackgroundColor(context.getResources().getColor(R.color.grey_pallete_100));

            holder.profilePic.setVisibility(View.INVISIBLE);
            holder.profilePicAdd.setVisibility(View.VISIBLE);

            holder.lBottom.setVisibility(View.INVISIBLE);
            holder.addCaretakerLabel.setVisibility(View.VISIBLE);



        }else{

            if(isTablet) {
             //   holder.cardViewCaretaker.setCardBackgroundColor(context.getResources().getColor(R.color.grey_pallete_200));
            }

            holder.profilePic.setVisibility(View.VISIBLE);
            holder.profilePicAdd.setVisibility(View.INVISIBLE);

            holder.lBottom.setVisibility(View.VISIBLE);
            holder.addCaretakerLabel.setVisibility(View.INVISIBLE);



            String full = caretaker.getFirstName()+" "+caretaker.getLastName();

            String fullname = full;
            if(isTablet) {
                fullname  = resizeName(full, caretaker);
            }


            String noOfKids = "0";
            if(null != caretaker.getChildren()) {
                noOfKids = Integer.toString(caretaker.getChildren().size());
            }


            holder.fullname.setText(fullname);
            // holder.phoneno.setText(caretaker.getPhoneno1());
            holder.district.setText(caretaker.getDistrict());
            holder.noChildren.setText(noOfKids);


        }







        //setProfilePic(holder,caretaker);



        /*
        //circular image
        //Bitmap myBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_doctor_male);
        RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(context.getResources(), bProfilePic);
        drawable.setCircular(true);
        holder.profilePic.setImageDrawable(drawable);
        */


        /*
        Bitmap myPictureBitmap = BitmapFactory.decodeFile(imageFile.getPath());
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            myPictureBitmap = Bitmap.createScaledBitmap(myPictureBitmap, holder.profilePic.getWidth(),holder.profilePic.getHeight(),true);
        }
        holder.profilePic.setImageBitmap(myPictureBitmap);

        */

       // String description = ""+caretaker.getFirstName()+" is a "+caretaker.getAge()+" year old <Mother> with "+noOfKids+" from "+caretaker.getDistrict();





      //  holder.profilePic.setImageBitmap(profilePic);

       // Picasso.with(mContext).load(album.getNoticeUrl()).fit().placeholder(R.drawable.challenge).into(holder.thumbnail);



       // int descLength = album.getDescription().length();
       // holder.itemView.setTag(descLength);


        //ONCLICK LISTENER FOR SHARE BUTTON

        holder.addChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //creating a popup menu
                PopupMenu popup = new PopupMenu(context, holder.addChild);
                //inflating menu from xml resource
                popup.inflate(R.menu.menu_card_item);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_add_child:
                                //handle menu1 click
                                Intent intent = new Intent(context, AddChildActivityWizard.class);
                                intent.putExtra("caretakerId", caretaker.getCaretakerId());
                                context.startActivity(intent);

                                return true;

                            default:
                                return false;
                        }
                    }
                });
                //displaying the popup
                popup.show();





            }
        });


    }



    @Override
    public int getItemCount() {

        return caretakers.size();
    }


    //2. Animations bug
    @Override
    public long getItemId(int position) {
        return caretakers.get(position).hashCode(); //Any unique id
    }



    private Caretaker getCaretakers(int position)
    {
        return caretakers.get(position);
    }




    //interfaces
    public void setClickListener(OnCaretakerClickListener onCardItemClickListener) {
        this.myclickListener = onCardItemClickListener;
    }


    public interface OnCaretakerClickListener {

        void onCaretakerClicked(View view, int position, Caretaker caretaker, ImageView profilePic);
    }




    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    public static Bitmap decodeSampledBitmapFromResource(String path,
                                                         //1. Get the Bitmap
            /* These methods attempt to allocate memory for the constructed bitmap and
            therefore can easily result in an OutOfMemory exception. use injustDecodeBounds*/

                                                         //2.Get the dimensions coz image may be large


                                                         //3. Decide if full image can be loaded into memory
             /* To tell the decoder to subsample the image, loading a smaller version into
             memory, set inSampleSize to true

             For example, an image with resolution 2048x1536 that is decoded with an
             inSampleSize of 4 produces a bitmap of approximately 512x384. Loading this into
             memory uses 0.75MB rather than 12MB for
             the full image (assuming a bitmap configuration of ARGB_8888).

             */



                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);


        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;
        String imageType = options.outMimeType;

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }




}
