package com.vims.vimsapp.view.patients.addchild;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.vims.vimsapp.MainActivity;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.utilities.alarm.UIAlert;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;

import kotlin.jvm.internal.PackageReference;
import vimsapp.R;
import vimsapp.databinding.PatientsChildAddWelcomeFragmentBinding;

import static com.vims.vimsapp.utilities.ScreenManager.setScreenOrientation;


public class AddChildWelcomeActivity extends AppCompatActivity {


    PatientsChildAddWelcomeFragmentBinding welcomeFragmentBinding;
    AddChildActivityWizardViewModel addChildActivityWizardViewModel;
    AddChildActivityWizardViewModelFactory viewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        welcomeFragmentBinding = DataBindingUtil.setContentView(this, R.layout.patients_child_add_welcome_fragment);


        viewModelFactory = new AddChildActivityWizardViewModelFactory(Injection.provideRegisterChildUseCase(getApplication()), Injection.provideGenerateChildScheduleUseCase(getApplication()));
        addChildActivityWizardViewModel =  ViewModelProviders.of(this,viewModelFactory).get(AddChildActivityWizardViewModel.class);


        setScreenOrientation(this);



        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            String careId = bundle.getString("caretakerId");


            welcomeFragmentBinding.getStartedBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PreferenceHandler pref =PreferenceHandler.getInstance(getApplicationContext());
                    int isLoaded = pref.getIntValue(PreferenceHandler.SCHEDULE_LOADED_INT);
                    if(isLoaded == 1){
                        Intent intent = new Intent(getApplicationContext(), AddChildActivityWizard.class);
                        intent.putExtra("caretakerId", careId);
                        startActivityForResult(intent, REQUEST_CODE);
                        finish();
                    }else{

                        String content = "You need to first load a hospital schedule to continue";
                        UIAlert.showAlert("Info",""+content,AddChildWelcomeActivity.this,UIAlert.AlertType.INFO);

                    }

                }
            });

            welcomeFragmentBinding.getStartedCancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    finish();

                }
            });


        }


        observeRegistration();

        welcomeFragmentBinding.tvTitleWelcomeAddchild.setTypeface(MyFonts.getTypeFace(this));
        welcomeFragmentBinding.tvSubtitleWelcomeAddchild.setTypeface(MyFonts.getTypeFace(this));
        welcomeFragmentBinding.getStartedBtn.setTypeface(MyFonts.getTypeFace(this));


    }


    @Override
    protected void onResume() {
        super.onResume();

        String action = "Continue Registration";

       // welcomeFragmentBinding.tvTitleWelcomeAddchild.setText(title);
       // welcomeFragmentBinding.tvSubtitleWelcomeAddchild.setText(subtitle);
       //  welcomeFragmentBinding.getStartedBtn.setText(action);

    }

    public static final int REQUEST_CODE = 2;


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                Bundle bundle = data.getExtras();
                if (bundle != null) {

                    // welcomeFragmentBinding.tvTitleWelcomeAddchild.setText();
                    //       welcomeFragmentBinding.tvSubtitleWelcomeAddchild.setTypeface(MyFonts.getTypeFace(this));
                    //welcomeFragmentBinding.getStartedBtn.setTypeface(MyFonts.getTypeFace(this));


                }
            }

        }


    }


    private void observeRegistration(){

        Observer<String> regObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                String title = "Thankyou";
                String subtitle = s+", you can go a head, generate and issue the immunisation schedule to the parent";
                String action = "continue registration";

                welcomeFragmentBinding.tvTitleWelcomeAddchild.setText(title);
                welcomeFragmentBinding.tvSubtitleWelcomeAddchild.setText(subtitle);

                welcomeFragmentBinding.getStartedBtn.setText(action);


            }
        };

        addChildActivityWizardViewModel.getRegistrationResponse().observe(this,regObserver);

    }





}
