package com.vims.vimsapp.view.reports;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.vims.vimsapp.viewcoverage.VaccineReport;
import com.vims.vimsapp.utilities.PreferenceHandler;

import java.util.List;

import vimsapp.R;

/**
 * Created by root on 5/31/18.
 */

public class AdapterReportsAlt extends RecyclerView.Adapter<AdapterReportsAlt.MyViewHolder> {


    List<VaccineReport> vaccineReportsCards;
    Context context;

    public AdapterReportsAlt(List<VaccineReport> vaccineReportsCards, Context context) {
        this.context = context;
        this.vaccineReportsCards = vaccineReportsCards;
        //  this.children.add(addChild);
        //1. Animations bug
        setHasStableIds(true); //in constructor

    }




    //(1) Declare Interface
    protected OnCardItemClickListener myclickListener;  //our interace
    // private WebView webView;



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View  itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.reports_card_horizontal_shift, parent, false);

        return new MyViewHolder(itemView);
    }


    //(2) implements the click listner
    public class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{
        //the lookup cache
        TextView title;
        TextView tvVaccinePercentageCoverage;
        TextView tvVaccinesImmunized;
        TextView tvVaccinesNotImmunized;
        TextView tvVaccinesInPlan;
        TextView tvVaccinesRatings;
        LineChart lineChart;
        ProgressBar progressBar;

        CardView cardView;




        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.tvReportsHeaderVaccineName);
            tvVaccinesImmunized = view.findViewById(R.id.tvReportsHeaderImmunized);
            tvVaccinesNotImmunized = view.findViewById(R.id.tvReportsHeaderNotImmunized);
            tvVaccinePercentageCoverage = view.findViewById(R.id.tvReportsHeaderVaccineCoverage);
            tvVaccinesInPlan = view.findViewById(R.id.tvReportsHeaderVaccinesInplan);
            tvVaccinesRatings = view.findViewById(R.id.tvReportsHeaderVaccineNo);
            progressBar = view.findViewById(R.id.circularProgressbar);
            lineChart = view.findViewById(R.id.linechart);
            cardView = view.findViewById(R.id.card_view_report);

           // tvVaccinesInPlan.setTypeface(MyFonts.getBoldTypeFace(context));
           // tvVaccinesImmunized.setTypeface(MyFonts.getBoldTypeFace(context));

           // horizontalBarChart = view.findViewById(R.id.chartHorizontal);

            //setFontface
            //(3) Bind the click listener
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition(); // gets item position

              VaccineReport report = vaccineReportsCards.get(position);
//              myclickListener.onClick(v, position); // call the onClick in the OnCardItemClickListener Interface i created

            //  ProfileInfo caretaker = children.get(position);

            /*
              Intent intent = new Intent(context, ViewChildDetailsActivity.class);

              Bundle bundle = new Bundle();
              bundle.putString("child", MyDetailsTypeConverter.fromChildToString(child));
              intent.putExtras(bundle);
              loadDetailsActivity(intent, v, "transition");
              */


        }

    }





    public void loadDetailsActivity(Intent intent, View view, String transition_name){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            //shared Element Transitions
            //ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, p1, p2, p3);
            //    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, view, transition_name);
            // view.getContext().startActivity(intent, options.toBundle());

            //Exit Transitions
            Bundle bundle = ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle();
            context.startActivity(intent, bundle);


        } else {
            context.startActivity(intent);
        }


    }




    @Override
    public void onBindViewHolder(final MyViewHolder holder,final int position) {
        final VaccineReport vaccineReport = getVaccinationReport(position);

       // int missed = vaccinationReport.getNoOfPlannedImmunisations() - vaccinationReport.getNoOfImmunizedChildren();
        String coverage = Integer.toString(vaccineReport.getCoveragePercentage())+"%";

        int coverageInt = vaccineReport.getCoveragePercentage();

        String rating = Integer.toString(position+1);

        String immunized = Integer.toString(vaccineReport.getNoOfImmunizedChildren())+"/"+Integer.toString(vaccineReport.getNoOfPlannedImmunisations());
        String inplan = Integer.toString(vaccineReport.getNoOfPlannedImmunisations());


        Log.d("ARAlternative","I-VaccineName "+vaccineReport.getVaccineName());
        Log.d("ARAlternative","I-Inplan "+vaccineReport.getNoOfPlannedImmunisations());
        Log.d("ARAlternative","I-Immunized"+immunized);
        Log.d("ARAlternative","I-Delayed"+vaccineReport.getNoOfDelayedImmunisations());
        Log.d("ARAlternative","I-Coverage"+coverage+" "+coverageInt);


        holder.tvVaccinesRatings.setText(rating);
        holder.title.setText(vaccineReport.getVaccineName());
        //holder.tvVaccinePercentageCoverage.setText(coverage);

        String immunizedTotal = "";

      //  holder.tvVaccinesInPlan.setText(inplan);
        holder.tvVaccinesImmunized.setText(immunized);


        setUpCircularProgressDrawable(coverageInt,holder);


        startCircularProgress(coverageInt,holder.progressBar,holder.tvVaccinePercentageCoverage);



      //  holder.title.setTypeface(MyFonts.getBoldTypeFace(context));
        //holder.tvVaccinesRatings.setTypeface(MyFonts.getTypeFace(context));
      //  holder.tvVaccinePercentageCoverage.setTypeface(MyFonts.getBoldTypeFace(context));

       //setUpBarGraph(holder,vaccinationReport.getRecordedVaccination(),missed,vaccinationReport.getTotalVaccination());


       // setUpLineChart(holder, vaccinationReportCard);

    }



    @Override
    public int getItemCount() {

        return vaccineReportsCards.size();
    }


    //2. Animations bug
    @Override
    public long getItemId(int position) {
        return vaccineReportsCards.get(position).hashCode(); //Any unique id
    }




    private VaccineReport getVaccinationReport(int position)
    {
        return vaccineReportsCards.get(position);
    }




    //interfaces
    public void setClickListener(OnCardItemClickListener onCardItemClickListener) {
        this.myclickListener = onCardItemClickListener;
    }


    public interface OnCardItemClickListener {

        void onClick(View view, int position);
    }




    int pStatus;
    private void startCircularProgress(int progress, ProgressBar progressBar, TextView textView) {
          pStatus = 0;
        Log.d("ARAltProgress","I-Progress"+progress);
       // Log.d("ARAltProgress","I-Progress"+pStatus);
        Handler handler = new Handler();

        new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub

                while (pStatus < progress) {
                    pStatus += 1;
                    String percentage = Integer.toString(progress)+"%";

                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            progressBar.setProgress(pStatus);
                            textView.setText(percentage);

                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        // Just to display the progress slowly
                        Thread.sleep(100); //thread will take approx 3 seconds to finish
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void setUpCircularProgressDrawable(int coverage, MyViewHolder holder){
      int  DEFAULT_THEME_ID = R.style.AppTheme_Dark;
      int  LIGHT_THEME_ID = R.style.AppTheme_Light;

        int high_coverage = 75;
        int low_coverage = 40;

        PreferenceHandler preferenceHandler = PreferenceHandler.getInstance(context);
        int  theme_id = preferenceHandler.getIntValue("THEME_ID");


        if(theme_id == DEFAULT_THEME_ID){

            if(coverage > high_coverage){
                // holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                holder.progressBar.setProgressDrawable(context.getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_dark_high));
            }else if(coverage <= low_coverage){
                //holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.errorColor));
                holder.progressBar.setProgressDrawable(context.getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_dark_low));
            }
            else {
                //holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorAccent));
                holder.progressBar.setProgressDrawable(context.getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_dark_meduim));
            }


        }else{
            if(coverage > high_coverage){
                // holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                holder.progressBar.setProgressDrawable(context.getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_light_high));
            }else if(coverage <= low_coverage){
                //holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.errorColor));
                holder.progressBar.setProgressDrawable(context.getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_light_low));
            }
            else {
                //holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorAccent));
                holder.progressBar.setProgressDrawable(context.getResources().getDrawable(R.drawable.shape_progress_indicator_circular_thin_light_meduim));
            }

        }





    }




}
