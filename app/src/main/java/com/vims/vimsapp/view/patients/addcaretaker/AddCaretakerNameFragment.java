package com.vims.vimsapp.view.patients.addcaretaker;


import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.vims.vimsapp.utilities.imager.GetBitmapPlaceHolder;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Calendar;

import vimsapp.R;
import vimsapp.databinding.PatientsCaretakerAddNameFragmentBinding;


///**
// * A simple {@link Fragment} subclass.
// * Use the {@link AddCaretakerNameFragment#newInstance} factory method to
// * create an instance of this fragment.
// */
public class AddCaretakerNameFragment extends Fragment implements BlockingStep {
    // TODO: Rename parameter arguments, choose names that match
   //  the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    CaretakerActivityWizardViewModel caretakerActivityWizardViewModel;
   // binding name with view
   PatientsCaretakerAddNameFragmentBinding caretakerAddNameFragmentBinding;




    public AddCaretakerNameFragment() {
      //   Required empty public constructor
    }



    public static AddCaretakerNameFragment newInstance(String param1, String param2) {
        AddCaretakerNameFragment fragment = new AddCaretakerNameFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


        setHasOptionsMenu(true); //to enable or display actions
    }

   @Override
   public void onPrepareOptionsMenu(Menu menu) {

        MenuItem itemSearch = menu.findItem(R.id.action_search);
      // itemSearch.setVisible(false);

       MenuItem itemSettings = menu.findItem(R.id.action_settings);
      // itemSettings.setVisible(false);
   }



   CaretakerActivityViewModelFactory caretakerActivityViewModelFactory;

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       caretakerAddNameFragmentBinding  = DataBindingUtil.inflate(inflater, R.layout.patients_caretaker_add_name_fragment, container, false);

        //setSupportActionBar(toolbar);


       caretakerActivityViewModelFactory = new CaretakerActivityViewModelFactory(Injection.provideRegisterCaretakerUseCase(getActivity().getApplication()));

        caretakerActivityWizardViewModel =  ViewModelProviders.of(getActivity(), caretakerActivityViewModelFactory).get(CaretakerActivityWizardViewModel.class);
       //setUpActionBar();



       setupDrawables();
       setupAdaptiveCardMargins();


         profileImageClicked();
        firstNameTextChanged();
        lastNameTextChanged();
        observeCaretakerRole();

      // registerDateClickListner();

      //  phoneNumber1TextChanged();
       // phoneNumber2TextChanged();
       // ageTextChanged();
        observeBioErrors();




        //set variables in Binding
        return caretakerAddNameFragmentBinding.getRoot();
    }

    Toolbar toolbar;



   private  void setupDrawables(){
       caretakerAddNameFragmentBinding.tvFirstnameCaretaker.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(getContext(),R.drawable.ic_person_cp_24dp), null, null, null);
       caretakerAddNameFragmentBinding.tvLastnameCaretaker.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(getContext(),R.drawable.ic_person_cp_24dp), null, null, null);
       caretakerAddNameFragmentBinding.tvBirthdateCaretaker.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(getContext(),R.drawable.ic_menu_calendar_birthdate_black_24dp), null, null, null);
   }

   private void setupAdaptiveCardMargins(){

       boolean isTablet = getResources().getBoolean(R.bool.tablet);
       boolean isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);



       if(isTablet) {
           CardView.LayoutParams layoutParams = (CardView.LayoutParams) caretakerAddNameFragmentBinding.cardView.getLayoutParams();

           if(isPotrait) {


               layoutParams.setMargins(80,8,80,8);

               //  caretakerAddRoleFragmentBinding.cardView.setLayoutParams(layoutParams);
           }else{

               layoutParams.setMargins(200,8,200,8);


           }
       }


   }



    //Pick From Camera and Galeery
    private ImageView imageview;
    private Button btnSelectImage;
    private Bitmap bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;


    // Select image from camera and gallery
    private void selectImage() {

        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;
        if (requestCode == PICK_IMAGE_GALLERY && data != null) {
            Uri selectedImage = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bytes);

                Log.e("Activity", "Pick from Gallery::>>> ");

                imgPath = getRealPathFromURI(selectedImage);

                 //  -  /storage/sdcard0/DCIM/Camera/IMG_20180526_130542.jpg

                destination = new File(imgPath);

                Log.d("Image Path", "Pick from Gallery::>>> "+destination);

                caretakerActivityWizardViewModel.setProfileImageBitmap(bitmap);

                caretakerAddNameFragmentBinding.ivCaretakerAvator.setImageBitmap(bitmap);

                caretakerActivityWizardViewModel.setProfileImagePath(imgPath);




            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }




    public static final int RESULT_LOAD_IMAGE = 10;

    public void profileImageClicked(){

        caretakerAddNameFragmentBinding.ivCaretakerAvator.setVisibility(View.INVISIBLE);
        caretakerAddNameFragmentBinding.ivCaretakerAvator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                //startActivityForResult(i, RESULT_LOAD_IMAGE);
                 selectImage();
                //startGallery();
            }
        });


    }



    int formAcceptanceCounter = 0;//if 3 then ok


    public void firstNameTextChanged(){


       // Drawable img = getContext().getResources().getDrawable( R.drawable.ic_perm_identity_black_24dp);
//        caretakerAddNameFragmentBinding.tvFirstnameCaretaker.setCompoundDrawablesWithIntrinsicBounds( getResources().getDrawable( R.drawable.ic_perm_identity_black_24dp), null, null, null);

        caretakerAddNameFragmentBinding.tvFirstnameCaretaker.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                if(charSequence.toString().trim().isEmpty() && charSequence.length() > 1){
                    caretakerAddNameFragmentBinding.tvFirstnameCaretaker.setError("first name Required!");


                }else{
                    caretakerActivityWizardViewModel.setFirstName(charSequence);
                }






            }

            @Override
            public void afterTextChanged(Editable editable) {





            }
        });
    }
    public void lastNameTextChanged(){

//        Drawable img = getContext().getResources().getDrawable( R.drawable.ic_perm_identity_black_24dp);
  //      caretakerAddNameFragmentBinding.tvLastnameCaretaker.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);

        caretakerActivityWizardViewModel.setmIsNextButtonEnabled(false);

        caretakerAddNameFragmentBinding.tvLastnameCaretaker.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                if(charSequence.toString().trim().isEmpty() && charSequence.length() > 1){
                    caretakerAddNameFragmentBinding.tvLastnameCaretaker.setError("last name required!");

                }
                else{
                    caretakerActivityWizardViewModel.setLastName(charSequence);
                }




            }

            @Override
            public void afterTextChanged(Editable editable) {



            }
        });
    }



    DatePickerDialog.OnDateSetListener dateSetListener;

    public void registerDateClickListner(){

        TextView tvAge = caretakerAddNameFragmentBinding.tvCaretakerAge;
        String defaultAge = "0";
        tvAge.setText(defaultAge);

     caretakerAddNameFragmentBinding.imageButtonAdd.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {

            int defaultValue = Integer.parseInt(tvAge.getText().toString());
            int newValue = defaultValue + 1;
            String newValueString = Integer.toString(newValue);
            tvAge.setText(newValueString);

            caretakerActivityWizardViewModel.setlBirthDate(newValueString);



         }
     });
     caretakerAddNameFragmentBinding.imageButtonMin.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {

             int defaultValue = Integer.parseInt(tvAge.getText().toString());

             if(defaultValue > 0) {
                 int newValue = defaultValue - 1;
                 String newValueString = Integer.toString(newValue);
                 tvAge.setText(newValueString);
                 caretakerActivityWizardViewModel.setlBirthDate(newValueString);

             }


         }
     });




       /*

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                String date = ""+dayOfMonth+"/"+monthOfYear+"/"+year;

                caretakerAddNameFragmentBinding.tvBirthdateCaretaker.setText(date);
                caretakerActivityWizardViewModel.setlBirthDate(date);


            }
        };

        //on date clicked open the calendar view and reister listner to it

        caretakerAddNameFragmentBinding.tvBirthdateCaretaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar now = Calendar.getInstance();

                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        dateSetListener,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getActivity().getFragmentManager(), "");


            }
        });
           */

    }

    public void observeCaretakerRole(){


        Observer<CharSequence> nameObserver = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence charSequence) {


                String title = ""+charSequence+"'s basic info ?";
                caretakerAddNameFragmentBinding.tvWizardCaretakerNameTitle.setTypeface(MyFonts.getTypeFace(getContext()));
                caretakerAddNameFragmentBinding.tvWizardCaretakerNameTitle.setText(title);
                caretakerAddNameFragmentBinding.tvWizardCaretakerNameTitle.setTypeface(MyFonts.getTypeFace(getContext()));

            }
        };


        caretakerActivityWizardViewModel.getlRole().observe(this,nameObserver);



    }


    public void observeBioErrors(){

        Observer<String> fnameErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                caretakerAddNameFragmentBinding.tvFirstnameCaretaker.setError(s);

            }};

        caretakerActivityWizardViewModel.getMfirstNameError().observe(this,fnameErrorObserver);


        Observer<String> lnameErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                caretakerAddNameFragmentBinding.tvLastnameCaretaker.setError(s);

            }};
        caretakerActivityWizardViewModel.getMlastNameError().observe(this,lnameErrorObserver);


        Observer<String> ageErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();

                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();

                caretakerAddNameFragmentBinding.tlBirth.setError(s);
                caretakerAddNameFragmentBinding.tvBirthdateCaretaker.setError(s);

            }};

        caretakerActivityWizardViewModel.getMBirthDateError().observe(this,ageErrorObserver);


        Observer<String> phone1ErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();

            }};

        caretakerActivityWizardViewModel.getmPhoneError().observe(this,phone1ErrorObserver);



    }






    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        if(caretakerActivityWizardViewModel.checkCaretakerBasicinfo()) {
            caretakerActivityWizardViewModel.setFullName();

            if(imgPath ==null){
                String path = "drawable/icons8_doctor_female_50.png";
                caretakerActivityWizardViewModel.setProfileImageBitmap(GetBitmapPlaceHolder.getBitmapPlaceholder(getContext()));
                caretakerActivityWizardViewModel.setProfileImagePath(path);

            }

            callback.goToNextStep();
        }
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
          callback.goToPrevStep();
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }


       /*

    private void setUpActionBar(){

        toolbar = registerformBinding.toolbar;
        toolbar.setTitle("Caretaker Form");

         //((AppCompatActivity)getActivity()).setSupportActionBar(toolbar); //we replace our actionbar with our toolbar
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();

            }
        });
    }
    */






    /*

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == RESULT_LOAD_IMAGE) {
            Bitmap bitmap = getPath(data.getData());

            caretakerAddNameFragmentBinding.ivCaretakerAvator.setImageBitmap(bitmap);

        }


    }
    */
    /*

    private Bitmap getPath(Uri uri) {

       // String[] projection = {MediaStore.Images.Media.DATA};

        //Cursor cursor = managedQuery(uri, projection, null, null, null);
       // int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        //cursor.moveToFirst();

        //String filePath = cursor.getString(column_index);
        // cursor.close();
        // Convert file path into bitmap image using below line.
        //Bitmap bitmap = BitmapFactory.decodeFile(filePath);


       // Bitmap bitmapImage = MediaStore.Images.Media.getBitmap(getContentResolver(), returnUri);
        Bitmap bitmap = null;

        try {
          bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
            // Log.d(TAG, String.valueOf(bitmap));


        } catch (IOException e) {
            e.printStackTrace();
        }



        return bitmap;
    }



    private void startGallery() {
        Intent cameraIntent = new Intent(Intent.ACTION_GET_CONTENT);
        cameraIntent.setType("image/*");

        if (cameraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(cameraIntent, RESULT_LOAD_IMAGE);
        }
    }

*/

}
