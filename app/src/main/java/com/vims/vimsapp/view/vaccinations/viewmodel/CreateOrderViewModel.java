package com.vims.vimsapp.view.vaccinations.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.manageorders.VaccinePlan;
import com.vims.vimsapp.manageorders.RequestOrder;

import java.util.List;

public class CreateOrderViewModel extends ViewModel {



    private MutableLiveData<List<VaccinePlan>> mutableVaccinePlanList;
    private MutableLiveData<String> userName;
    private MutableLiveData<String> hospitalId;


    private MutableLiveData<Boolean> isSuccess;
    private MutableLiveData<String> responseMessage;

    private RequestOrder requestOrder;



    public CreateOrderViewModel(RequestOrder requestOrder) {
        this.requestOrder = requestOrder;
    }


    public MutableLiveData<String> getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String myresponseMessage) {
        if(responseMessage == null){
            responseMessage = new MutableLiveData<>();
        }
        this.responseMessage.postValue(myresponseMessage);
    }

    public MutableLiveData<Boolean> getIsSuccess() {
        return isSuccess;
    }

    public void setiSSuccess(boolean myshowSuccess) {
        if(isSuccess == null){
            isSuccess = new MutableLiveData<>();
        }
        this.isSuccess.postValue(myshowSuccess);
    }



    public LiveData<List<VaccinePlan>> getMutableVaccinePlanList() {
        return mutableVaccinePlanList;
    }

    public void setMutableVaccinePlanList(List<VaccinePlan> vaccinePlanList) {
        if(mutableVaccinePlanList == null){
            mutableVaccinePlanList = new MutableLiveData<>();
        }
        this.mutableVaccinePlanList.postValue(vaccinePlanList);
    }

    public MutableLiveData<String> getUserName() {
        return userName;
    }

    public void setUserName(String myuserName) {
        if (userName == null) {
            userName = new MutableLiveData<>();
        }
        this.userName.postValue(myuserName);
    }

    public MutableLiveData<String> getHospitalId() {
        if (hospitalId == null) {
            hospitalId = new MutableLiveData<>();
        }
        return hospitalId;
    }

    public void setHospitalId(String myhospitalId) {
        if (hospitalId == null) {
            hospitalId = new MutableLiveData<>();
        }
        this.hospitalId.postValue(myhospitalId);
    }


    public void executeOrderRequest(int ordermonth){
        String hospitalName = "";
        String username = "";
        List<VaccinePlan> vaccinePlans = null;

        if(hospitalId.getValue()!= null && mutableVaccinePlanList.getValue() != null & userName.getValue() != null) {
            hospitalName = hospitalId.getValue();
            vaccinePlans = mutableVaccinePlanList.getValue();
            username = userName.getValue();
        }

        RequestOrder.RequestValues requestValues = new RequestOrder.RequestValues(hospitalName,username,vaccinePlans,ordermonth);

        requestOrder.execute(requestValues, new UseCase.OutputBoundary<RequestOrder.ResponseValue>() {
            @Override
            public void onSuccess(RequestOrder.ResponseValue result) {

                setiSSuccess(true);
                setResponseMessage(result.getVaccineOrder().getUserName());

            }

            @Override
            public void onError(String errorMessage) {

                setiSSuccess(false);
                setResponseMessage(errorMessage);

            }
        });



    }



}
