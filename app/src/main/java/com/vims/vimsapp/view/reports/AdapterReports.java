package com.vims.vimsapp.view.reports;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.vims.vimsapp.viewcoverage.VaccineReport;

import com.vims.vimsapp.viewcoverage.VaccineReportsCard;
import com.vims.vimsapp.view.MyFonts;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import vimsapp.R;

/**
 * Created by root on 5/31/18.
 */

public class AdapterReports extends RecyclerView.Adapter<AdapterReports.MyViewHolder> {


    List<VaccineReportsCard> vaccineReportsCards;
    Context context;

    public AdapterReports(List<VaccineReportsCard> vaccineReportsCards, Context context) {
        this.context = context;

        this.vaccineReportsCards = vaccineReportsCards;


        //  this.children.add(addChild);
        //1. Animations bug
        setHasStableIds(true); //in constructor

    }




    //(1) Declare Interface
    protected OnCardItemClickListener myclickListener;  //our interace
    // private WebView webView;



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;

        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.reports_card_horizontal_shift, parent, false);

        return new MyViewHolder(itemView);
    }


    //(2) implements the click listner
    public class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{
        //the lookup cache
        TextView title;
        TextView tvVaccinePercentageCoverage;
        TextView tvVaccinesImmunized;
        TextView tvVaccinesNotImmunized;
        TextView tvVaccinesInPlan;
        TextView tvVaccinesRatings;
        LineChart lineChart;
        ProgressBar progressBar;

        CardView cardView;




        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.tvReportsHeaderVaccineName);
            tvVaccinesImmunized = view.findViewById(R.id.tvReportsHeaderImmunized);
            tvVaccinesNotImmunized = view.findViewById(R.id.tvReportsHeaderNotImmunized);
            tvVaccinePercentageCoverage = view.findViewById(R.id.tvReportsHeaderVaccineCoverage);
            tvVaccinesInPlan = view.findViewById(R.id.tvReportsHeaderVaccinesInplan);
            tvVaccinesRatings = view.findViewById(R.id.tvReportsHeaderVaccineNo);
            progressBar = view.findViewById(R.id.circularProgressbar);
            lineChart = view.findViewById(R.id.linechart);
            cardView = view.findViewById(R.id.card_view_report);

            tvVaccinesInPlan.setTypeface(MyFonts.getTypeFace(context));
            tvVaccinesImmunized.setTypeface(MyFonts.getTypeFace(context));

           // horizontalBarChart = view.findViewById(R.id.chartHorizontal);

            //setFontface
            //(3) Bind the click listener
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition(); // gets item position

              VaccineReportsCard report = vaccineReportsCards.get(position);
//              myclickListener.onClick(v, position); // call the onClick in the OnCardItemClickListener Interface i created

            //  ProfileInfo caretaker = children.get(position);

            /*
              Intent intent = new Intent(context, ViewChildDetailsActivity.class);

              Bundle bundle = new Bundle();
              bundle.putString("child", MyDetailsTypeConverter.fromChildToString(child));
              intent.putExtras(bundle);
              loadDetailsActivity(intent, v, "transition");
              */


        }

    }





    public void loadDetailsActivity(Intent intent, View view, String transition_name){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            //shared Element Transitions
            //ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, p1, p2, p3);
            //    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, view, transition_name);
            // view.getContext().startActivity(intent, options.toBundle());

            //Exit Transitions
            Bundle bundle = ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle();
            context.startActivity(intent, bundle);


        } else {
            context.startActivity(intent);
        }


    }


    int pStatus = 0;
    private void startCircularProgress(int progress, ProgressBar progressBar, TextView textView) {

        Handler handler = new Handler();

        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (pStatus < progress) {
                    pStatus += 1;
                    String percentage = Integer.toString(progress)+"%";

                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub

                            progressBar.setProgress(pStatus);
                            textView.setText(percentage);


                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        // Just to display the progress slowly
                        Thread.sleep(50); //thread will take approx 3 seconds to finish
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }




    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final VaccineReportsCard vaccinationReportCard = getVaccinationReport(position);


       // int missed = vaccinationReport.getNoOfPlannedImmunisations() - vaccinationReport.getNoOfImmunizedChildren();
        String coverage = Integer.toString(vaccinationReportCard.getCoveragePercentage())+"%";

        String rating = Integer.toString(position+1);

        String immunized = Integer.toString(vaccinationReportCard.getNoImmunized())+"/";
        String inplan = Integer.toString(vaccinationReportCard.getNoInplan());

        holder.tvVaccinesRatings.setText(rating);
        holder.title.setText(vaccinationReportCard.getVaccineName());
     //   holder.tvVaccinePercentageCoverage.setText(coverage);
        holder.tvVaccinesInPlan.setText(inplan);
        holder.tvVaccinesImmunized.setText(immunized);

        if(vaccinationReportCard.getCoveragePercentage() > 30){
       //     holder.cardView.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        }





        startCircularProgress(vaccinationReportCard.getCoveragePercentage(),holder.progressBar,holder.tvVaccinePercentageCoverage);



        holder.title.setTypeface(MyFonts.getTypeFace(context));
        //holder.tvVaccinesRatings.setTypeface(MyFonts.getTypeFace(context));
      //  holder.tvVaccinePercentageCoverage.setTypeface(MyFonts.getBoldTypeFace(context));

       //setUpBarGraph(holder,vaccinationReport.getRecordedVaccination(),missed,vaccinationReport.getTotalVaccination());


       // setUpLineChart(holder, vaccinationReportCard);

    }



    @Override
    public int getItemCount() {

        return vaccineReportsCards.size();
    }


    //2. Animations bug
    @Override
    public long getItemId(int position) {
        return vaccineReportsCards.get(position).hashCode(); //Any unique id
    }



    private VaccineReportsCard getVaccinationReport(int position)
    {
        return vaccineReportsCards.get(position);
    }




    //interfaces
    public void setClickListener(OnCardItemClickListener onCardItemClickListener) {
        this.myclickListener = onCardItemClickListener;
    }


    public interface OnCardItemClickListener {

        void onClick(View view, int position);
    }




    int a=0;
    private void setUpLineChart(MyViewHolder myViewHolder,VaccineReportsCard vaccineReportsCard){

        List<Entry> entries = new ArrayList<>();

        List<VaccineReport>  unDuplicateReports = new ArrayList<>(new LinkedHashSet<>(vaccineReportsCard.getVaccineReports()));

        //for each report in the 10 cards
        for (VaccineReport vaccineReport : unDuplicateReports) {
            // turn your data into Entry objects
            // there are 12 reports this loop shoulg go 12 time
            Log.d("AdapterReport",vaccineReport.getVaccineName()+" count:"+unDuplicateReports.size());

            float xvalue = Float.parseFloat(Integer.toString(vaccineReport.getxAxisValue()));
                entries.add(new Entry(xvalue, vaccineReport.getCoveragePercentage()));

        }



        LineDataSet dataSet = new LineDataSet(entries, "Customized values");
        dataSet.setColor(ContextCompat.getColor(context, R.color.colorPrimaryTint0));
        dataSet.setValueTextColor(ContextCompat.getColor(context, R.color.white));
        dataSet.setCubicIntensity(0.6f);
        dataSet.setFillColor(context.getResources().getColor(R.color.colorPrimaryTint0));
        dataSet.setLineWidth(1f);
        dataSet.setCircleRadius(3f);
        dataSet.setDrawCircleHole(false);
        dataSet.setValueTextSize(9f);
        dataSet.setDrawFilled(true);
        dataSet.setCircleRadius(3f);

        dataSet.enableDashedLine(10f, 5f, 0f);






        /***********************************************************************/
        // Controlling X axis
        XAxis xAxis = myViewHolder.lineChart.getXAxis();
        xAxis.setEnabled(false);
        xAxis.setDrawLabels(false);
        xAxis.setGranularityEnabled(false);


        xAxis.setDrawAxisLine(false);
        // Set the xAxis position to bottom. Default is top
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        //Customizing x axis value
        final String[] months = new String[]{"J", "F", "M", "A","M","J","J","A","S","O","N","D"};

        IAxisValueFormatter formatter = new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return months[(int) value-1];
            }
        };


        xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
        xAxis.setValueFormatter(formatter);
        //do not draw the xaxis lines that move up
        xAxis.setDrawGridLines(false);
        //color text
        xAxis.setTextColor(context.getResources().getColor(R.color.white));

        /*** Controlling right side of y axis **/
        YAxis yAxisRight = myViewHolder.lineChart.getAxisRight();
        yAxisRight.setEnabled(false);

        //***
        // Controlling left side of y axis
        YAxis yAxisLeft = myViewHolder.lineChart.getAxisLeft();
        yAxisLeft.setEnabled(false);
        yAxisLeft.setDrawGridLines(false);
        yAxisLeft.setGranularity(1f);
        yAxisLeft.setAxisMaximum(100f);
        yAxisLeft.setAxisMinimum(0f);
        //color of the text values on the left y axis
        yAxisLeft.setTextColor(context.getResources().getColor(R.color.white));
        //color of the y axis lines
        // yAxisLeft.setGridColor(getResources().getColor(R.color.colorPrimaryLight));


        // Setting Data
        LineData data = new LineData(dataSet);
        myViewHolder.lineChart.setData(data);
        myViewHolder.lineChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);


        Description description = new Description();
        description.setText("There's no data");

        myViewHolder.lineChart.setDescription(description);
        myViewHolder.lineChart.setNoDataText("");
        //refresh
        myViewHolder.lineChart.invalidate();

        myViewHolder.lineChart.setVisibility(View.VISIBLE);
       // myViewHolder.lineChart.setVisibility(View.INVISIBLE);

        /*********************************************************************/

    }





}
