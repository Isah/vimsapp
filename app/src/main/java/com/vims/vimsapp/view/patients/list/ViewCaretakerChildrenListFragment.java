package com.vims.vimsapp.view.patients.list;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ListView;
import android.widget.TextView;

import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.view.patients.detail.MyDetailsTypeConverter;
import com.vims.vimsapp.utilities.PreferenceHandler;

import java.util.List;



import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import vimsapp.R;
import vimsapp.databinding.PatientsFragmentCaretakerDetailChildrenBinding;


public class ViewCaretakerChildrenListFragment extends Fragment {


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;

    PatientsFragmentCaretakerDetailChildrenBinding detailChildrenBinding;
    ViewCaretakerChildrenListFragmentViewModel viewCaretakerChildrenListFragmentViewModel;

    private Caretaker caretaker;
    Bundle bundle;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        detailChildrenBinding  = DataBindingUtil.inflate(inflater, R.layout.patients_fragment_caretaker_detail_children, container, false);

       viewCaretakerChildrenListFragmentViewModel = ViewModelProviders.of(this).get(ViewCaretakerChildrenListFragmentViewModel.class);

        bundle = getArguments();

        if (bundle != null) {

            String caretakerString = getArguments().getString("caretaker");
            caretaker = MyDetailsTypeConverter.fromStringToCaretaker(caretakerString);

            Log.i("caretaker name",""+caretaker.getLastName());

            viewCaretakerChildrenListFragmentViewModel.setCaretakerId(caretaker.getCaretakerId());
            viewCaretakerChildrenListFragmentViewModel.loadChildren(caretaker.getCaretakerId());


        }
        setUpRecyclerView();
        observeChildren();




        return detailChildrenBinding.getRoot();
    }


    @Override
    public void onResume() {
        super.onResume();
        if(bundle!=null) {
            viewCaretakerChildrenListFragmentViewModel.setCaretakerId(caretaker.getCaretakerId());
            viewCaretakerChildrenListFragmentViewModel.loadChildren(caretaker.getCaretakerId());
        }

    }



    public void observeChildren(){

        Observer<List<Child>> childrenObserver = new Observer<List<Child>>() {
            @Override
            public void onChanged(@Nullable List<Child> children) {

                if(children!=null) {
                    if (!children.isEmpty()) {
                        detailChildrenBinding.recyclerviewChildreninfo.setVisibility(View.VISIBLE);
                        updateBasicInfo(children);

                    } else {

                        detailChildrenBinding.recyclerviewChildreninfo.setVisibility(View.INVISIBLE);
                    }
                }

            }
        };

         viewCaretakerChildrenListFragmentViewModel.getChildren().observe(this, childrenObserver);
    }



    public static ViewCaretakerChildrenListFragment newInstance(Bundle args) {
        ViewCaretakerChildrenListFragment fragment = new ViewCaretakerChildrenListFragment();
     //   Bundle args = new Bundle();
       // args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }



    //AdapterChildrenArray adapterChildren;
    ListView childlistview;
    TextView tvHeader;


    public void setUpChildListView(int caretakerId){

        LayoutInflater inflater = getLayoutInflater();
       // childlistview = detailChildrenBinding.lvChildren;

        ViewGroup header = (ViewGroup)inflater.inflate(R.layout.patients_header_childrenlistinfo, childlistview, false);
        childlistview.addHeaderView(header, null, false);

        tvHeader =  header.findViewById(R.id.sizeOfChildren);

        /*
        ImageView imageView = header.findViewById(R.id.imageview_profile_card);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getContext(),AddChildProfileFragment.class);
                intent.putExtra("caretakerId", caretakerId);
                startActivity(intent);

            }
        });
        */

    }



    int DEFAULT_THEME_ID =0;
    int LIGHT_THEME_ID=0;
    int recycler_view_drawable =0;



    int no_columns = 0;
    public void setUpRecyclerView(){
        DEFAULT_THEME_ID = R.style.AppTheme_Dark;
        LIGHT_THEME_ID = R.style.AppTheme_Light;
        PreferenceHandler preferenceHandler =PreferenceHandler.getInstance(getContext());

        int theme_id = preferenceHandler.getIntValue("THEME_ID");

        if(theme_id == DEFAULT_THEME_ID) {
            recycler_view_drawable =  R.drawable.divider_horizontal;
        }else{
            recycler_view_drawable =  R.drawable.divider_horizontal_light;
        }



        //setup the recycler view basic info
        SlideInUpAnimator animator = new SlideInUpAnimator(new OvershootInterpolator(1f));
        detailChildrenBinding.recyclerviewChildreninfo.setItemAnimator(animator);
        detailChildrenBinding.recyclerviewChildreninfo.getItemAnimator().setAddDuration(500);
        //setting animation on scrolling

        detailChildrenBinding.recyclerviewChildreninfo.setHasFixedSize(true); //enhance recycler view scroll
        detailChildrenBinding.recyclerviewChildreninfo.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset_profile);
        detailChildrenBinding.recyclerviewChildreninfo.addItemDecoration(itemDecoration);
        detailChildrenBinding.recyclerviewChildreninfo.setVisibility(View.INVISIBLE);



        RecyclerView.LayoutManager mLayoutManager ;




        boolean isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);
        boolean isTablet = getResources().getBoolean(R.bool.tablet);



        if(isTablet){

            if(isPotrait) {

                no_columns = 2;

               // mLayoutManager = new GridLayoutManager(getActivity(), no_columns,GridLayoutManager.VERTICAL, false);
                mLayoutManager = new LinearLayoutManager(getContext());
                detailChildrenBinding.recyclerviewChildreninfo.setLayoutManager(mLayoutManager);



            }
            else{

               // no_columns = 5;
                //mLayoutManager = new GridLayoutManager(getActivity(), no_columns,GridLayoutManager.VERTICAL, false);
                mLayoutManager = new LinearLayoutManager(getContext());
                detailChildrenBinding.recyclerviewChildreninfo.setLayoutManager(mLayoutManager);

            }

        }else{


            if(isPotrait) {

                no_columns = 2;
                // setRecyclerViewLayoutManager(LINEAR_LAYOUT_MANAGER, no_columns);
                mLayoutManager = new LinearLayoutManager(getContext());
                detailChildrenBinding.recyclerviewChildreninfo.setLayoutManager(mLayoutManager);



            }
            else{

                no_columns = 4;
                mLayoutManager = new GridLayoutManager(getActivity(), no_columns,GridLayoutManager.VERTICAL, false);
                detailChildrenBinding.recyclerviewChildreninfo.setLayoutManager(mLayoutManager);

            }



        }









    }


    AdapterChildren adapterChildren;
    public void updateBasicInfo(List<Child> children){


        // adapterProfileInfoArray = new AdapterProfileInfoArray(getContext(), children);
        // adapterProfileInfoArray.notifyDataSetChanged();
        // profilelistview.setAdapter(adapterProfileInfoArray);


        adapterChildren = new AdapterChildren(children, getContext());
        // adapterChildren.replaceItems(careTakers);
        adapterChildren.setHasStableIds(true);
        adapterChildren.notifyItemInserted(0);
        detailChildrenBinding.recyclerviewChildreninfo.setAdapter(adapterChildren);

    }



    }




