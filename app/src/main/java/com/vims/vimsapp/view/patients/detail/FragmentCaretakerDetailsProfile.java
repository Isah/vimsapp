package com.vims.vimsapp.view.patients.detail;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ListView;
import android.widget.TextView;

import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.view.patients.list.ItemOffsetDecoration;
import com.vims.vimsapp.view.patients.addcaretaker.ProfileInfo;
import com.vims.vimsapp.view.patients.list.AdapterProfileInfo;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import vimsapp.R;
import vimsapp.databinding.PatientsFragmentCaretakerDetailProfileBinding;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;

public class FragmentCaretakerDetailsProfile extends Fragment {


    PatientsFragmentCaretakerDetailProfileBinding profileBinding;

    // private static final String ARG_PARAM1 = "param1";
    ///private static final String ARG_PARAM2 = "param2";


    //private String mParam1;
    // private String mParam2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        profileBinding  = DataBindingUtil.inflate(inflater, R.layout.patients_fragment_caretaker_detail_profile, container, false);


        if (getArguments() != null) {
            String caretakerString = getArguments().getString("caretaker");
            Caretaker caretaker = MyDetailsTypeConverter.fromStringToCaretaker(caretakerString);

            ProfileInfo gender = new ProfileInfo();
            gender.setTitle(caretaker.getGenderRole());
            gender.setSubtitle("gender");
            gender.setAvator(R.drawable.ic_perm_identity_black_24dp);

            ProfileInfo dateOfBirth = new ProfileInfo();
            dateOfBirth.setTitle(caretaker.getBirthDate());
            dateOfBirth.setSubtitle("birthday");
            dateOfBirth.setAvator(R.drawable.ic_cake_black_24dp);

            ProfileInfo phoneInfo1 = new ProfileInfo();
            phoneInfo1.setTitle(caretaker.getPhoneNo());
            phoneInfo1.setSubtitle("phone l");
            phoneInfo1.setAvator(R.drawable.ic_phone_black_24dp);

            ProfileInfo phoneInfo2 = new ProfileInfo();
            phoneInfo2.setTitle(caretaker.getPhoneNo());
            phoneInfo2.setSubtitle("phone ll");
            phoneInfo2.setAvator(R.drawable.ic_phone_black_24dp);

            ProfileInfo infoLocation1 = new ProfileInfo();
            infoLocation1.setTitle(caretaker.getDistrict());
            infoLocation1.setSubtitle("district");
            infoLocation1.setAvator(R.drawable.ic_place_cp_24dp);

            ProfileInfo infoLocation2 = new ProfileInfo();
            infoLocation2.setTitle(caretaker.getParish());
            infoLocation2.setSubtitle("parish");
            infoLocation2.setAvator(R.drawable.ic_place_cp_24dp);

            ProfileInfo infoLocation3 = new ProfileInfo();
            infoLocation3.setTitle(caretaker.getVillage());
            infoLocation3.setSubtitle("village");
            infoLocation3.setAvator(R.drawable.ic_place_cp_24dp);



            ArrayList<ProfileInfo> basicInfos = new ArrayList<>();
            basicInfos.add(gender);
            basicInfos.add(dateOfBirth);


            ArrayList<ProfileInfo> contactInfos = new ArrayList<>();
            contactInfos.add(phoneInfo1);
            contactInfos.add(phoneInfo2);

            ArrayList<ProfileInfo> locationInfos = new ArrayList<>();
            locationInfos.add(infoLocation1);
            locationInfos.add(infoLocation2);
            locationInfos.add(infoLocation3);


           // setUpRecyclerViews();


            //Animator for recycler views
            SlideInUpAnimator animator = new SlideInUpAnimator(new OvershootInterpolator(1f));

            recyclerViewSetUp(profileBinding.recyclerviewBasicinfo, animator);
            recyclerViewSetUp(profileBinding.recyclerviewContactinfo, animator);
            recyclerViewSetUp(profileBinding.recyclerviewLocationinfo, animator);


            updateBasicInfo(basicInfos);
            updateContactInfo(contactInfos);
            updateLocationInfo(locationInfos);


        }
        //initialize your UI

        return profileBinding.getRoot();
    }


    public static FragmentCaretakerDetailsProfile newInstance(Bundle args) {
        FragmentCaretakerDetailsProfile fragment = new FragmentCaretakerDetailsProfile();
        //Bundle args = new Bundle();
        // args.putString(ARG_PARAM1, MyDetailsTypeConverter.fromCatakerToString(caretaker));

        fragment.setArguments(args);
        return fragment;
    }


    ListView profilelistview;

    TextView tvHeader;





    public void setUpRecyclerViews(){

        //setup the recycler view basic info
        SlideInUpAnimator animator = new SlideInUpAnimator(new OvershootInterpolator(1f));



        profileBinding.recyclerviewBasicinfo.setItemAnimator(animator);
        profileBinding.recyclerviewBasicinfo.getItemAnimator().setAddDuration(2000);
        //setting animation on scrolling

        profileBinding.recyclerviewBasicinfo.setHasFixedSize(true); //enhance recycler view scroll
        profileBinding.recyclerviewBasicinfo.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset_profile);
        profileBinding.recyclerviewBasicinfo.addItemDecoration(itemDecoration);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        profileBinding.recyclerviewBasicinfo.setLayoutManager(mLayoutManager);


        //setup the recycler view contact info
        profileBinding.recyclerviewContactinfo.setItemAnimator(animator);
        profileBinding.recyclerviewContactinfo.getItemAnimator().setAddDuration(2000);
        //setting animation on scrolling

        profileBinding.recyclerviewContactinfo.setHasFixedSize(true); //enhance recycler view scroll
        profileBinding.recyclerviewContactinfo.setNestedScrollingEnabled(false);// enable smooth scrooll


        profileBinding.recyclerviewContactinfo.addItemDecoration(itemDecoration);
        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(getActivity());
        profileBinding.recyclerviewContactinfo.setLayoutManager(mLayoutManager2);



        //setup the recycler view location info
        profileBinding.recyclerviewLocationinfo.setItemAnimator(animator);
        profileBinding.recyclerviewLocationinfo.getItemAnimator().setAddDuration(2000);
        //setting animation on scrolling

        profileBinding.recyclerviewLocationinfo.setHasFixedSize(true); //enhance recycler view scroll
        profileBinding.recyclerviewLocationinfo.setNestedScrollingEnabled(false);// enable smooth scrooll


        profileBinding.recyclerviewLocationinfo.addItemDecoration(itemDecoration);
        RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(getActivity());
        profileBinding.recyclerviewLocationinfo.setLayoutManager(mLayoutManager3);


    }



    public void recyclerViewSetUp(RecyclerView recyclerView, SlideInUpAnimator animator){


        DividerItemDecoration decoration = new DividerItemDecoration(getContext(), VERTICAL);
        //decoration.setDrawable(getResources().getDrawable(R.drawable.shape_borderline_ultra_thin_darktheme));
        recyclerView.addItemDecoration(decoration);


        recyclerView.setItemAnimator(animator);
        recyclerView.getItemAnimator().setAddDuration(2000);
        //setting animation on scrolling

        recyclerView.setHasFixedSize(true); //enhance recycler view scroll
        recyclerView.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset_profile);
        recyclerView.addItemDecoration(itemDecoration);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);


    }





    public void setUpLocationInfoListView(){

        /*
        LayoutInflater inflater = getLayoutInflater();
        profilelocationlistview = profileBinding.lvLocationinfo;
        ViewGroup header = (ViewGroup)inflater.inflate(R.layout.patients_header_basicinfo, profilelistview, false);
        profilelocationlistview.addHeaderView(header, null, false);
        //tvHeader =  header.findViewById(R.id.sizeOfChildren);
        */



    }


    AdapterProfileInfo adapterProfileInfo;
    public void updateBasicInfo(List<ProfileInfo> profileInfos){


        // adapterProfileInfoArray = new AdapterProfileInfoArray(getContext(), children);
        // adapterProfileInfoArray.notifyDataSetChanged();
        // profilelistview.setAdapter(adapterProfileInfoArray);


        adapterProfileInfo = new AdapterProfileInfo(profileInfos, getContext());
        // adapterChildren.replaceItems(careTakers);
        adapterProfileInfo.setHasStableIds(true);
        adapterProfileInfo.notifyItemInserted(0);
        profileBinding.recyclerviewBasicinfo.setAdapter(adapterProfileInfo);

    }


    public void updateContactInfo(List<ProfileInfo> contactInfos){


        // adapterProfileInfoArray = new AdapterProfileInfoArray(getContext(), children);
        // adapterProfileInfoArray.notifyDataSetChanged();
        // profilelistview.setAdapter(adapterProfileInfoArray);


        adapterProfileInfo = new AdapterProfileInfo(contactInfos, getContext());
        // adapterChildren.replaceItems(careTakers);
        adapterProfileInfo.setHasStableIds(true);
        adapterProfileInfo.notifyItemInserted(0);
        profileBinding.recyclerviewContactinfo.setAdapter(adapterProfileInfo);


    }



    public void updateLocationInfo(List<ProfileInfo> profileInfos){


        adapterProfileInfo = new AdapterProfileInfo(profileInfos, getContext());
        // adapterChildren.replaceItems(careTakers);
        adapterProfileInfo.setHasStableIds(true);
        adapterProfileInfo.notifyItemInserted(0);
        profileBinding.recyclerviewLocationinfo.setAdapter(adapterProfileInfo);

    }

}

/*
    public void initCaretaker(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            String caretakerJsonString = bundle.getString("caretaker");
            Caretaker caretaker = MyDetailsTypeConverter.fromString(caretakerJsonString);
            Log.i("caretakerid-> "," "+caretaker.getId());
            detailsActivityViewModel.setCaretaker(caretaker);
            //setUpRecyclerView(LINEAR_LAYOUT_MANAGER);
            // detailsActivityViewModel.searchChildren(caretaker.getId());
            // detailsActivityViewModel.loadChildren();
            //adapterChildrenRecycler = new AdapterChildren(children, getApplicationContext());
            //adapterChildrenRecycler.notifyItemInserted(0);
            //recyclerView.setAdapter(adapterChildrenRecycler);
        }
    }
    public void updateDetailsUI(Caretaker caretaker) {
        String full_name = caretaker.getFirstName() + " " + caretaker.getLastName();
        caretakerFragmentDetailBinding.firstnameProfileTv.setText(full_name);
        caretakerFragmentDetailBinding.firstnameProfileTv.setTypeface(MyFonts.getBoldTypeFace(this));
        // lastname_tv.setText(ln);
        caretakerFragmentDetailBinding.caretakerDetails.mobilenoProfileTv.setText(caretaker.getPhoneno1());
        caretakerFragmentDetailBinding.caretakerDetails.mobilenoProfileTv.setTypeface(MyFonts.getTypeFace(this));
    }
    private void observeDetails(){
        Observer<Caretaker> caretakerObserver = new Observer<Caretaker>() {
            @Override
            public void onChanged(@Nullable Caretaker caretaker) {
                updateDetailsUI(caretaker);
            }
        };
        detailsActivityViewModel.getCaretaker().observe(this,caretakerObserver);
    }
*/
