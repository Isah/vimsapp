package com.vims.vimsapp.view.reports;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;


import com.vims.vimsapp.searchchild.ViewChildRepository;
import com.vims.vimsapp.viewcoverage.GenerateReport;

public class FragmentReportsViewModelFactory implements ViewModelProvider.Factory{

    private GenerateReport generateReport;
    private ViewChildRepository vaccinationsRepository;
    private Application application;

    public FragmentReportsViewModelFactory(GenerateReport generateReport, ViewChildRepository  vaccinationsRepository, Application application) {
        this.generateReport = generateReport;
        this.vaccinationsRepository = vaccinationsRepository;
        this.application = application;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if(modelClass.isAssignableFrom(FragmentReportsViewModel.class)){

            return (T) new FragmentReportsViewModel(generateReport,vaccinationsRepository,application);

        }
        else{

            throw new IllegalArgumentException("View Model not found");
        }
    }
}
