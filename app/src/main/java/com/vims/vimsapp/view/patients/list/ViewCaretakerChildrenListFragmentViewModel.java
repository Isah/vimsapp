package com.vims.vimsapp.view.patients.list;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.vims.vimsapp.UseCase;

import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.searchchild.ViewChildDataSource;
import com.vims.vimsapp.searchchild.ViewChildRepository;
import com.vims.vimsapp.searchchild.ViewChildren;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.injection.ModuleRepository;

import java.util.List;

/**
 * Created by root on 6/30/18.
 */

public class ViewCaretakerChildrenListFragmentViewModel extends AndroidViewModel{



    private MutableLiveData<List<Child>> children;
    private MutableLiveData<List<Child>> all_children;
    private MutableLiveData<String> caretakerId;

    private  MutableLiveData<String>  errorMessage;



    public ViewCaretakerChildrenListFragmentViewModel(Application application) {
        super(application);

        children = new MutableLiveData<>();
        errorMessage = new MutableLiveData<>();

    }





    public MutableLiveData<String> getErrorMessage() {
        return errorMessage;
    }

    public LiveData<List<Child>> getChildren() {

        return children;
    }

    public LiveData<List<Child>> getAllChildren() {
        if(all_children==null){
            all_children =new MutableLiveData<>();
        }

        return all_children;
    }

    public void setCaretakerId(String mcaretakerId) {
        if(caretakerId==null){
            caretakerId =new MutableLiveData<>();
        }
        caretakerId.postValue(mcaretakerId);
    }




    MutableLiveData<Boolean> mIsStartProgress;
    public MutableLiveData<Boolean> getIsStartProgress() {

        return mIsStartProgress;
    }

    public void setIsStartProgress(Boolean isStartProgress) {
        if(mIsStartProgress == null){
            mIsStartProgress = new MutableLiveData<>();
        }
        this.mIsStartProgress.postValue(isStartProgress);
    }



    public void loadChildren(){
        setIsStartProgress(true);

        ViewChildren viewChildren = Injection.provideReadChildrenUseCase(getApplication());


        ViewChildren.RequestValues requestValues = new ViewChildren.RequestValues(null, ViewChildren.RequestType.CHILDREN);

        viewChildren.execute(requestValues, new UseCase.OutputBoundary<ViewChildren.ResponseValue>() {
            @Override
            public void onSuccess(ViewChildren.ResponseValue result) {

                setIsStartProgress(false);

                if(all_children == null){

                    all_children = new MutableLiveData<>();
                }

                all_children.postValue(result.getmChildren());

            }

            @Override
            public void onError(String mErrorMessage) {

                setIsStartProgress(false);

                if(errorMessage == null) {
                    errorMessage = new MutableLiveData<>();
                }

                errorMessage.postValue(mErrorMessage);


            }
        });



    }





    public void loadChildren(String caretakerId) {

     ViewChildRepository p = ModuleRepository.provideViewChildRepository(getApplication());

        p.getChildrenByCaretakerId(caretakerId, new ViewChildDataSource.LoadChildCallback() {
            @Override
            public void onChildrenLoaded(List<Child> childrenlist) {


                children.postValue(childrenlist);


            }

            @Override
            public void onDataNotAvailable() {

                if(errorMessage == null) {
                    errorMessage = new MutableLiveData<>();
                }

                errorMessage.postValue("No children Available!");

            }
        });



    }


    /*

    @Override
    public void onSuccess(List<Child> result) {
        // setCaretakersWithChildren(result);

        children.postValue(result);

    }

    @Override
    public void onError(String throwable) {

    }

   */
}
