package com.vims.vimsapp.view.patients.list;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;


import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationStatus;
import com.vims.vimsapp.searchchild.SearchPatient;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.patients.detail.MyDetailsTypeConverter;
import com.vims.vimsapp.view.patients.detail.ViewChildDetailsActivity;


import java.util.ArrayList;
import java.util.List;

import vimsapp.R;

/**
 * Created by root on 5/31/18.
 */

public class AdapterChildren extends RecyclerView.Adapter<AdapterChildren.MyViewHolder> implements Filterable {


    List<Child> children;
    Context context;

    public AdapterChildren(List<Child> children, Context context) {
        this.context = context;


        this.children = children;
      //  this.children.add(addChild);
        //1. Animations bug
        setHasStableIds(true); //in constructor


    }



    private List<Child> childrenListFiltered;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                Log.d("AdapterCaretakers","search filter"+charString);


                if (charString.isEmpty()) {
                    childrenListFiltered = children;
                } else {
                    List<Child> filteredList = new ArrayList<>();
                    for (Child row : children) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getFirstName().toLowerCase().contains(charString.toLowerCase()) || row.getLastName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                            Log.d("AdapterCaretakers","search found"+charString.toLowerCase());

                        }
                    }

                    childrenListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.count = childrenListFiltered.size();
                filterResults.values = childrenListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                if(filterResults.count > 0) {
                    children = (ArrayList<Child>) filterResults.values;
                    //replaceItems(caretakersListFiltered);
                    notifyDataSetChanged();
                }else{

                  searchInDatabase(charSequence);

                }

            }
        };
    }


    private void searchInDatabase(CharSequence searchText){

        Activity activity = (Activity) context;

        SearchPatient searchCaretaker = Injection.provideSearchPatientsUseCase(activity.getApplication());
        SearchPatient.RequestValues requestValues = new SearchPatient.RequestValues(searchText, SearchPatient.Patient.CHILD);

        searchCaretaker.execute(requestValues, new UseCase.OutputBoundary<SearchPatient.ResponseValue>() {
            @Override
            public void onSuccess(SearchPatient.ResponseValue result) {
                children = result.getChildren();
                //replaceItems(caretakersListFiltered);
                notifyDataSetChanged();
            }

            @Override
            public void onError(String errorMessage) {

            }
        });


    }






    //(1) Declare Interface
    protected OnCardItemClickListener myclickListener;  //our interace
    // private WebView webView;


    int layout_id  = 0;
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        boolean isTablet = context.getResources().getBoolean(R.bool.tablet);
        if(isTablet){
            layout_id  = R.layout.patients_card_child_vertical;
        }else {
            layout_id  = R.layout.patients_card_child_horizontal;
        }


        View itemView = LayoutInflater.from(parent.getContext()).inflate(layout_id, parent, false);

        return new MyViewHolder(itemView);
    }


    //(2) implements the click listner
    public class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{
        //the lookup cache
        TextView title;
        TextView subtitle;
        ImageView avator;
        ImageView profilePic;
        ImageView profilePicBackground;
        TextView childDosagePercentage;
        CardView cardView;


        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.tv_card_child_fullname);
            subtitle =   view.findViewById(R.id.tv_card_child_birthdate);
            avator = view.findViewById(R.id.babyGender);
            profilePic = view.findViewById(R.id.profile_image);
            //profilePicBackground = view.findViewById(R.id.profile_image_background);
            childDosagePercentage = view.findViewById(R.id.tv_card_child_precentage);
            //cardView = view.findViewById(R.id.card_view_kids);


            //typeface
            title.setTypeface(MyFonts.getTypeFace(context));
            subtitle.setTypeface(MyFonts.getTypeFace(context));

            //setFontface
            //(3) Bind the click listener
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();

            Child child = getChild(position);
            Intent intent = new Intent(context, ViewChildDetailsActivity.class);
            Bundle bundle = new Bundle();

            String childToString = MyDetailsTypeConverter.fromChildToString(child);

            bundle.putString("child_id", child.getChildId());
            //bundle.putString("child", childToString);
           // bundle.putInt("child_done_vacs",getDoneEvents(child.getVaccinationEvents()));
            intent.putExtras(bundle);

            String transition = context.getResources().getString(R.string.transitionNameImage);
            loadDetailsActivity(intent, avator, transition);



        }

    }




    public void loadDetailsActivity(Intent intent, View view, String transition_name){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            //shared Element Transitions
            //ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, p1, p2, p3);
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) context, view, transition_name);
            view.getContext().startActivity(intent, options.toBundle());


        } else {
            context.startActivity(intent);
        }


    }


    private int getDoneEvents(List<VaccinationEvent> vaccinationEvents){

        int vacs = 0;
        for(VaccinationEvent vaccinationEvent: vaccinationEvents) {
            if(vaccinationEvent.getVaccinationStatus() == VaccinationStatus.DONE){
                vacs++;
            }
        }

        return vacs;

    }

    private String resizeName(String fullname, Child child){


        int fn_size = child.getFirstName().length();
        int ln_size = child.getLastName().length();


        if(fullname.length() > 15){
            if(fn_size > ln_size) {

                char cut_fn = child.getFirstName().charAt(0);
                fullname = cut_fn+" "+child.getLastName();

                Log.d("CaretakerLengthN","fn: "+fullname);


            }else if(fn_size < ln_size){
                char cut_ln = child.getLastName().charAt(0);
                fullname = cut_ln+" "+child.getFirstName();

                Log.d("CaretakerLengthN","fn: "+fullname);

            }else{

                // String cut_ln = caretaker.getLastName().substring(3);
                // fullname = cut_ln+" "+caretaker.getLastName();

            }
        }
        return  fullname;

    }



    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Child child = getChild(position);


        if(child.getChildId() != null && !child.getChildId().isEmpty()) {

            String full = child.getFirstName()+" "+child.getLastName();
            String fullname = resizeName(full,child);




            holder.title.setText(fullname);
            holder.subtitle.setText(child.getDateOfBirth());

            /*
            Picasso.with(context)
                    .load(drawable.)
                    .fit()
                    .into(holder.profilePic);
                   */

            Log.d("gender: ",child.getGender());




          //  Log.i("child name", child.getFirstName());
        }
        else {


           // Log.i("child name", "No Children");
        }

        // int descLength = album.getDescription().length();
        // holder.itemView.setTag(descLength);


        //ONCLICK LISTNER FOR SHARE BUTTON

        /*
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // int position = getAdapterPosition(); // gets item position

                //myclickListener.onClick(v, position); // call the onClick in the OnCardItemClickListener Interface i created


            }
        });
        */


    }



    @Override
    public int getItemCount() {

        return children.size();
    }


    //2. Animations bug
    @Override
    public long getItemId(int position) {
        return children.get(position).hashCode(); //Any unique id
    }



    private Child getChild(int position)
    {
        return children.get(position);
    }




    //interfaces
    public void setClickListener(OnCardItemClickListener onCardItemClickListener) {
        this.myclickListener = onCardItemClickListener;
    }


    public interface OnCardItemClickListener {

        void onClick(View view, int position);
    }






}
