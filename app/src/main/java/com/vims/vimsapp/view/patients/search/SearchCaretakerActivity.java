package com.vims.vimsapp.view.patients.search;

import android.app.SearchManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.OvershootInterpolator;


import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.patients.list.AdapterCaretakers;
import com.vims.vimsapp.view.patients.list.ItemOffsetDecoration;

import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import vimsapp.R;
import vimsapp.databinding.ActivitySearchBinding;

public class SearchCaretakerActivity extends AppCompatActivity{


    SearchCaretakerFragmentViewModel searchCaretakerViewModel;
    SearchCaretakerFragmentViewModelFactory searchCaretakerViewModelFactory;

    ActivitySearchBinding activitySearchBinding;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activitySearchBinding = DataBindingUtil.setContentView(this, R.layout.activity_search);

        searchCaretakerViewModelFactory = new SearchCaretakerFragmentViewModelFactory(Injection.provideSearchPatientsUseCase(getApplication()));
        searchCaretakerViewModel = getMyViewModelProvider(this,searchCaretakerViewModelFactory).get(SearchCaretakerFragmentViewModel.class);


      //  Toolbar toolbar = findViewById(R.id.toolbar_main);
       // setSupportActionBar(toolbar);








        setUpRecyclerView();
        observeSearchedCaretaker();

        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {


            String query = intent.getStringExtra(SearchManager.QUERY);

            if(query!=null) {
                searchCaretakerViewModel.setmSearchText(query);
                searchCaretakerViewModel.loadCaretakers(query);
            }

        }



    }

    private ViewModelProvider viewModelProvider = null;


    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public void setViewModelProvider(ViewModelProvider viewModelProvider) {
        this.viewModelProvider = viewModelProvider;
    }

    private ViewModelProvider getMyViewModelProvider(SearchCaretakerActivity fragment, SearchCaretakerFragmentViewModelFactory searchCaretakerFragmentViewModelFactory) {
        if (viewModelProvider == null) {
            viewModelProvider = ViewModelProviders.of(fragment, searchCaretakerFragmentViewModelFactory);
        }
        return viewModelProvider;
    }




    RecyclerView.LayoutManager mLayoutManager;
    private void setUpRecyclerView(){
        boolean isTablet = getResources().getBoolean(R.bool.tablet);
        boolean isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);

        activitySearchBinding.errorMsgTv.setCompoundDrawablesWithIntrinsicBounds(null,getResources().getDrawable(R.drawable.ic_event_black_128dp),null,null);


        //setup the recycler view basic info
        SlideInLeftAnimator animator = new SlideInLeftAnimator(new OvershootInterpolator(1f));
        activitySearchBinding.recyclerview.setItemAnimator(animator);
        activitySearchBinding.recyclerview.getItemAnimator().setAddDuration(2000);
        //setting animation on scrolling

        activitySearchBinding.recyclerview.setHasFixedSize(true); //enhance recycler view scroll
        activitySearchBinding.recyclerview.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(this, R.dimen.item_offset_schedule);
        activitySearchBinding.recyclerview.addItemDecoration(itemDecoration);



        if(isTablet){

            if(!isPotrait){

                //    mLayoutManager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL, false);
                mLayoutManager  = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);



            }


        }else {

            // if(!isPotrait){

            mLayoutManager  = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);


            // }


        }

        activitySearchBinding.recyclerview.setLayoutManager(mLayoutManager);


    }



    public void observeSearchedCaretaker() {
        Observer<List<Caretaker>> observer = new Observer<List<Caretaker>>() {
            @Override
            public void onChanged(@Nullable List<Caretaker> caretakers) {

                if(caretakers!=null){

                    updateSearch(caretakers);

                }else{


                    Log.d("SCA","No Data");


                }

            }
        };

        searchCaretakerViewModel.getSearchedCaretakers().observe(this,observer);

    }

    AdapterCaretakers adapterVaccinationEvents;
    public void updateSearch(List<Caretaker> caretakers){

        adapterVaccinationEvents = new AdapterCaretakers(caretakers,this);

        //adapterChildren.replaceItems(careTakers);
        //adapterVaccinationEvents.setHasStableIds(true);
        adapterVaccinationEvents.notifyItemInserted(0);
        activitySearchBinding.recyclerview.setVisibility(View.VISIBLE);
        activitySearchBinding.progressbarCaretakers.setVisibility(View.INVISIBLE);
        activitySearchBinding.recyclerview.setAdapter(adapterVaccinationEvents);

    }



}
