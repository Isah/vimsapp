package com.vims.vimsapp.view.patients.search;


import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.vims.vimsapp.searchchild.SearchPatient;

/**
 * Created by root on 6/3/18.
 */
public class SearchCaretakerFragmentViewModelFactory implements ViewModelProvider.Factory{


    private final SearchPatient searchCaretaker;

    public SearchCaretakerFragmentViewModelFactory(SearchPatient searchCaretaker) {
        this.searchCaretaker = searchCaretaker;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if(modelClass.isAssignableFrom(SearchCaretakerFragmentViewModel.class)){

            return (T) new SearchCaretakerFragmentViewModel(searchCaretaker);

        }
        else{

            throw new IllegalArgumentException("View Model not found");
        }

    }




}
