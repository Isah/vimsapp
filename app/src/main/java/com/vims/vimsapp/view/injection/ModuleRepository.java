package com.vims.vimsapp.view.injection;

import android.app.Application;
import android.content.Context;

import com.vims.vimsapp.APIClient;
import com.vims.vimsapp.PatientsDatabase;
import com.vims.vimsapp.managehospitalschedule.ManageHospitalApi;
import com.vims.vimsapp.managehospitalschedule.ManageHospitalRemoteDataSource;
import com.vims.vimsapp.managehospitalschedule.ManageHospitalScheduleLocalDataSource;
import com.vims.vimsapp.managehospitalschedule.ManageHospitalScheduleRepository;
import com.vims.vimsapp.manageorders.ManageOrderApi;
import com.vims.vimsapp.manageorders.OrdersDatabase;
import com.vims.vimsapp.manageorders.OrdersLocalDataSource;
import com.vims.vimsapp.manageorders.OrdersRemoteDataSource;
import com.vims.vimsapp.manageorders.OrdersRepository;
import com.vims.vimsapp.manageorders.VaccineOrderDao;
import com.vims.vimsapp.recordvaccinations.AddVaccinationsLocalDataSource;
import com.vims.vimsapp.recordvaccinations.AddVaccinationsRepository;
import com.vims.vimsapp.registerchild.AddChildApi;
import com.vims.vimsapp.registerchild.AddChildLocalDataSource;
import com.vims.vimsapp.registerchild.AddChildRemoteRetrofitDataSource;
import com.vims.vimsapp.registerchild.AddChildRepository;
import com.vims.vimsapp.registerchild.local.CaretakerDao;
import com.vims.vimsapp.registerchild.local.ChildDao;
import com.vims.vimsapp.registerchild.local.PatientsSyncTimeDao;
import com.vims.vimsapp.registerchild.local.VaccinationEventDao;
import com.vims.vimsapp.searchchild.ViewChildApi;
import com.vims.vimsapp.searchchild.ViewChildLocalDataSource;
import com.vims.vimsapp.searchchild.ViewChildRemoteRetrofitDataSource;
import com.vims.vimsapp.searchchild.ViewChildRepository;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsLocalDataSource;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsRepository;
import com.vims.vimsapp.sync.SyncManager;
import com.vims.vimsapp.utilities.AppExecutors;

public class ModuleRepository {



    private static PatientsDatabase patientsDatabase;
    private static OrdersDatabase ordersDatabase;

    //// REPOSITORY  /////////////////////////////////////////////////////////


    //ADD CHILD REPOSItORY

    public static AddChildRepository provideAddChildRepository(Application application) {
        return AddChildRepository.getInstance(provideAddChildRemoteRetrofitRepository(application),provideAddChildLocalDataSource(application));
    }

    public static AddChildRemoteRetrofitDataSource provideAddChildRemoteRetrofitRepository(Application application) {
        return AddChildRemoteRetrofitDataSource.getInstance(provideAddChildApi(), application);
    }

    private static AddChildApi provideAddChildApi() {
        return APIClient.getRetrofitInstance().create(AddChildApi.class);
    }

    public static AddChildLocalDataSource provideAddChildLocalDataSource(Application application) {

        return AddChildLocalDataSource.getInstance(provideAppExecutors(), provideCaretakerDao(application),provideChildDao(application), providePatientsSyncTimeDao(application),  provideVaccinationEventDao(application), application);

    }




    //SEARCH CHILD REPOSItORY
    public static ViewChildRepository provideViewChildRepository(Application application) {
        return ViewChildRepository.getInstance(provideViewChildRemoteRetrofitRepository(application),provideViewChildLocalDataSource(application));
    }

    public static ViewChildRemoteRetrofitDataSource provideViewChildRemoteRetrofitRepository(Application application) {
        return ViewChildRemoteRetrofitDataSource.getInstance(provideViewChildApi(), application);
    }

    private static ViewChildApi provideViewChildApi() {
        return APIClient.getRetrofitInstance().create(ViewChildApi.class);
    }

    private static ViewChildLocalDataSource provideViewChildLocalDataSource(Application application) {

        return ViewChildLocalDataSource.getInstance(provideAppExecutors(), provideCaretakerDao(application),provideChildDao(application), providePatientsSyncTimeDao(application),  provideVaccinationEventDao(application), application);

    }





    //VACCINATIONS

    //RECORD VACCINATIONS

    //VACCINATIONS REPOSItORY needs local and remote, the application ontext moves from up to down to dao///////////////
    public static AddVaccinationsRepository provideAddVaccinationRepository(Application application) {
        return AddVaccinationsRepository.getInstance(provideAddVaccinationsLocalRepository(application));
    }


    public static AddVaccinationsLocalDataSource provideAddVaccinationsLocalRepository(Application application) {
        return AddVaccinationsLocalDataSource.getInstance(provideAppExecutors(), provideChildDao(application), providePatientsSyncTimeDao(application),  provideVaccinationEventDao(application), application);
    }

    ///////////////////////////////////////////////////



    // SEARCH VACCINATIONS
    public static SearchVaccinationsRepository provideSearchVaccinationRepository(Application application) {
        return SearchVaccinationsRepository.getInstance(provideSearchVaccinationsLocalRepository(application));
    }


    public static SearchVaccinationsLocalDataSource provideSearchVaccinationsLocalRepository(Application application) {
        return SearchVaccinationsLocalDataSource.getInstance(provideAppExecutors(), provideChildDao(application), providePatientsSyncTimeDao(application),  provideVaccinationEventDao(application), application);
    }




    //SCHEDULE

    public static ManageHospitalScheduleRepository provideManageHospitalScheduleRepository(Application application) {
        return ManageHospitalScheduleRepository.getInstance(provideManageHospitalScheduleRemoteDataSource(application),provideManageHospitalScheduleLocalDataSource(application));
    }

    private static ManageHospitalRemoteDataSource provideManageHospitalScheduleRemoteDataSource(Application application) {

        return ManageHospitalRemoteDataSource.getInstance(provideManageHospitalApi(),application);
    }


    private static ManageHospitalScheduleLocalDataSource provideManageHospitalScheduleLocalDataSource(Application application) {

        return ManageHospitalScheduleLocalDataSource.getInstance(provideAppExecutors(),provideVaccinationEventDao(application));
    }

    private static ManageHospitalApi  provideManageHospitalApi() {
        return APIClient.getRetrofitInstance().create(ManageHospitalApi.class);
    }







    //ORDERS REPOSItORY

    public static OrdersRepository provideOrdersRepository(Application application) {
        return OrdersRepository.getInstance(provideOrdersRemoteRepository(application),provideReportsLocalRepository(application));
    }

    //REPOSItORY REMOTE
    private static OrdersRemoteDataSource provideOrdersRemoteRepository(Application application) {
        return OrdersRemoteDataSource.getInstance(provideManageOrderApiApi(),application);
    }
    //REPOSItORY LOCAL
    private static OrdersLocalDataSource provideReportsLocalRepository(Application application) {
        return OrdersLocalDataSource.getInstance(provideAppExecutors(), provideVaccineOrderDao(application));
    }

    private static ManageOrderApi provideManageOrderApiApi() {
        return APIClient.getRetrofitInstance().create(ManageOrderApi.class);
    }









    public static AppExecutors provideAppExecutors(){

        return new AppExecutors();

    }


    public static SyncManager provideSyncManager(Context context){

        return new SyncManager(context);
    }



    /// DAO
    public static CaretakerDao provideCaretakerDao(Application application) {
        patientsDatabase = PatientsDatabase.getInstance(application);
        return  patientsDatabase.getCaretakerDao();

    }

    public static ChildDao provideChildDao(Application application) {
        patientsDatabase = PatientsDatabase.getInstance(application);
        return  patientsDatabase.getChildDao();

    }

    public static PatientsSyncTimeDao providePatientsSyncTimeDao(Application application) {
        patientsDatabase = PatientsDatabase.getInstance(application);
        return  patientsDatabase.getPatientsSyncTimeDao();

    }



    //API




    //vaccinations DAO///////////////////////////////////////////////////////////////////

    /// DAO

    public static VaccinationEventDao provideVaccinationEventDao(Application application) {
        patientsDatabase = PatientsDatabase.getInstance(application);

        return  patientsDatabase.getVaccinationEventDao();

    }



    public static VaccineOrderDao provideVaccineOrderDao(Application application) {
        ordersDatabase = OrdersDatabase.getInstance(application);
        return  ordersDatabase.getVaccineOrderDao();

    }

    //end of DAO vaccinations ///////////////////////////////////////////////////////




}
