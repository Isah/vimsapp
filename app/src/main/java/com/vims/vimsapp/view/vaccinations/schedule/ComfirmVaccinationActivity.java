package com.vims.vimsapp.view.vaccinations.schedule;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationLocationState;
import com.vims.vimsapp.entities.vaccinations.VaccinationStatus;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.utilities.ThemeManager;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.injection.ModuleRepository;
import com.vims.vimsapp.view.patients.detail.MyDetailsTypeConverter;
import com.vims.vimsapp.view.vaccinations.viewmodel.ViewScheduleUpcomingFragmentViewModel;
import com.vims.vimsapp.view.vaccinations.viewmodel.ViewScheduleUpcomingFragmentViewModelFactory;

import java.util.Date;

import vimsapp.R;
import vimsapp.databinding.VaccinationsComfirmVaccinationActivityBinding;


public class ComfirmVaccinationActivity extends AppCompatActivity {


    ViewScheduleUpcomingFragmentViewModel viewScheduleFragmentViewModel;
    ViewScheduleUpcomingFragmentViewModelFactory scheduleUpcomingFragmentViewModelFactory;

    VaccinationsComfirmVaccinationActivityBinding vaccinationActivityBinding;


    Bundle bundle;
    VaccinationEvent vaccinationEvent;
    int position;
    EventState eventState;

    ProgressDialog progressDialog;


    VaccinationLocationState vaccinationLocationState;



    /*
    private void anim(){
        LottieAnimationView animationView;

        animationView =  vaccinationActivityBinding.animationView;


        ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f);

        //  animationView.setProgress(0);
        animationView.pauseAnimation();
        // animationView.playAnimation();

                        //
                        //animationView.addAnimatorUpdateListener((animation) -> {
                            // Do something.
                          //  animationView.setProgress(animation.getAnimatedValue());

                       // });
                       //

        animator.addUpdateListener(animation -> {
            animationView.setProgress(animation.getAnimatedFraction());
        });
        animator.start();


        // vaccinationActivityBinding.animationView.cancelAnimation();

        if (animationView.isAnimating()) {
            // Do something.
        }


    }
     */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        scheduleUpcomingFragmentViewModelFactory = new ViewScheduleUpcomingFragmentViewModelFactory(Injection.provideReadChildSchedule(getApplication()), Injection.provideRecordVaccinationUseCase(getApplication()), Injection.provideGenerateReport(getApplication()), ModuleRepository.provideSearchVaccinationRepository(getApplication()),Injection.provideHospitalSchedule(getApplication()), getApplication() );
        viewScheduleFragmentViewModel = ViewModelProviders.of(this, scheduleUpcomingFragmentViewModelFactory).get(ViewScheduleUpcomingFragmentViewModel.class);

        vaccinationActivityBinding = DataBindingUtil.setContentView(this, R.layout.vaccinations_comfirm_vaccination_activity);


        eventState = new EventState(this);

        vaccinationActivityBinding.eventStatic.setChecked(true);
        vaccinationLocationState = VaccinationLocationState.STATIC;







        bundle = getIntent().getExtras();
        if(bundle!=null) {

            vaccinationEvent = MyDetailsTypeConverter.fromStringToVaccinationEvent(bundle.getString("vevent"));

            setUpDynamicAppBar(vaccinationEvent.getVaccineName());
            loadContent(vaccinationEvent);

             position = bundle.getInt("position");

             progressDialog = new ProgressDialog(ComfirmVaccinationActivity.this);

        }





        vaccinationActivityBinding.comfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String text = "Recording...";
                vaccinationActivityBinding.comfirmButton.setText(text);

                //vaccinationActivityBinding.progressBar.setVisibility(View.VISIBLE);
               // String comp = "OK...";
               // vaccinationActivityBinding.comfirmButton.setText(comp);


               // AnimatedVectorDrawableCompat animatedVectorDrawable = (AnimatedVectorDrawableCompat) getResources().getDrawable(R.drawable.avd_add_to_tick);
               // vaccinationActivityBinding.comfirmButton.setCompoundDrawablesWithIntrinsicBounds(animatedVectorDrawable,null,null,null);

               // animatedVectorDrawable.start();

                // Looper.prepare();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {


                       //progressDialog.setIndeterminate(true);
                       // progressDialog.setMessage("Please wait, completing vaccination...");


                        //Toast.makeText(getApplicationContext(),""+vaccinationLocationState,Toast.LENGTH_LONG).show();
                        viewScheduleFragmentViewModel.recordChildVaccination(vaccinationLocationState,vaccinationEvent);

                        if(position!=-1) {

                         String state =  bundle.getString("state");
                         if(state!= null && state.equals("missed")){  // missed vaccinations

                             eventState.putPref("recordedMissed", "1");
                             eventState.putPref("recordedIndexMissed", "" + position);

                         }else{   //upcoming vaccinations

                             eventState.putPref("recorded", "1");
                             eventState.putPref("recordedIndex", "" + position);

                         }


                        }

                        //update the widget after an attempted vaccination
                       // ImmunisationScheduleService.startImmunisationService(getApplicationContext());

                        Looper myLooper = Looper.myLooper();
                        if (myLooper!=null) {
                           // myLooper.quit();
                        }

                    }
                }, 1000);
                Looper.loop();


            }
        });


        observeRecord();


    }





    private void observeRecord(){

        Observer<String> ob = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

              // UIAlert.showAlert("Vaccination",s,ComfirmVaccinationActivity.this,R.color.colorPrimary);
              // Toast.makeText(getApplicationContext(),""+s,Toast.LENGTH_LONG).show();

                PreferenceHandler preferenceHandler = PreferenceHandler.getInstance(getApplicationContext());
                preferenceHandler.setIntValue(PreferenceHandler.IS_VACCINE_EVENT_RECORDED,1);



                finish();


            }
        };
        viewScheduleFragmentViewModel.getRecordResponse().observe(this,ob);


    }


    public void onRadioButtonClicked(View view){

        int checkedId = vaccinationActivityBinding.eventRadioGroup.getCheckedRadioButtonId();
        switch (checkedId){
            case R.id.event_outreach:
               vaccinationLocationState = VaccinationLocationState.OUTREACH;



                break;
            case R.id.event_static:

                vaccinationLocationState = VaccinationLocationState.OUTREACH;

                break;

        }


    }

    private void recordVaccination(){

        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait, completing vaccination...");
        progressDialog.show();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed

                      //  viewScheduleFragmentViewModel.recordChildVaccination(vaccinationEvent);

                        if(position!=-1) {
                            eventState.putPref("recorded", "1");
                            eventState.putPref("recordedIndex", "" + position);
                        }
                        finish();

                        // onLoginFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);

    }



    CollapsingToolbarLayout collapsingToolbarLayout;
    public void setUpDynamicAppBar(String vaccine){


       collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);

        ///collapsingToolbarLayout.setExpandedTitleGravity(Gravity.BOTTOM);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout.setCollapsedTitleGravity(Gravity.START);
       // collapsingToolbarLayout.setTitleEnabled(false);
        collapsingToolbarLayout.setTitle(vaccine);





        vaccinationActivityBinding.toolbarMain.setTitle("");
        vaccinationActivityBinding.toolbarMain.setNavigationIcon(getResources().getDrawable(R.drawable.ic_clear_white_24dp));
        //vaccinationActivityBinding.toolbarMain.set
        setSupportActionBar(vaccinationActivityBinding.toolbarMain);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setTitle("Confirm Vaccination ?");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            //getSupportActionBar().setIcon(getResources().getDrawable(R.drawable.custom_checkbox_cancel));
        }


        vaccinationActivityBinding.toolbarMain.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                 onBackPressed();
               // finish();


            }
        });




    }


    private void loadContent(VaccinationEvent vEvent){

        String vaccine = "Record "+vEvent.getVaccineName();
        String buttonText = "Record";
        vaccinationActivityBinding.comfirmButton.setText(buttonText);
        String toChild = ""+vEvent.getChildName();

        String dateScheduled = ""+DateCalendarConverter.dateToStringWithoutTime(new Date(vEvent.getScheduledDate()));
        Date dateScheduledDate = new Date(vEvent.getScheduledDate());
        long days = DateCalendarConverter.calculateDifferenceInDates(dateScheduledDate,new Date());

        Log.d("ComfirmDiff", "days: "+days+" status: "+vEvent.getVaccinationStatus());


        boolean isDateMissed = dateScheduledDate.before(new Date()) && vEvent.getVaccinationStatus()==VaccinationStatus.NOT_DONE;
        boolean isDateDue = days == 0 && vEvent.getVaccinationStatus()==VaccinationStatus.NOT_DONE;
        boolean isDateDone = vEvent.getVaccinationStatus()==VaccinationStatus.DONE;
        boolean isDateUpcoming = days > 0 && vEvent.getVaccinationStatus()==VaccinationStatus.NOT_DONE;



        //BIND
        vaccinationActivityBinding.tvSubtitleDateOfVaccination.setText(dateScheduled);
        vaccinationActivityBinding.tvSubtitleComfirmVaccineAdminister.setText(toChild);
        vaccinationActivityBinding.comfirmButton.setBackground(ThemeManager.getBackgroundDrawable());


        //TYPE FACE
        vaccinationActivityBinding.collapsingToolbar.setCollapsedTitleTypeface(MyFonts.getTypeFace(getApplicationContext()));
        vaccinationActivityBinding.collapsingToolbar.setExpandedTitleTypeface(MyFonts.getTypeFace(getApplicationContext()));
        vaccinationActivityBinding.comfirmButton.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
        vaccinationActivityBinding.tvSubtitleDateOfVaccinationLabel.setTypeface(MyFonts.getTypeFace(getApplicationContext()));

        String daysLeft = "";

        if(days > 0){
            daysLeft = ""+days+"d to go";
            vaccinationActivityBinding.calendarDayViewBg.setBackground(ThemeManager.getBackgroundDrawable());//brand green 20
            vaccinationActivityBinding.tvSubtitleDateOfVaccinationLabel.setTextColor(getResources().getColor(R.color.colorPrimaryTint2));
            collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(R.color.grey_pallete_600));
            vaccinationActivityBinding.comfirmButton.setEnabled(false);
            vaccinationActivityBinding.comfirmButton.setTextColor(getResources().getColor(R.color.grey_pallete_400));
            vaccinationActivityBinding.comfirmButton.setBackground(getResources().getDrawable(R.drawable.shape_roundeded_rectangle_upcoming_light_ultra));

        }
        else if(isDateDue){

            daysLeft = "Due Today";
            vaccinationActivityBinding.calendarDayViewBg.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            vaccinationActivityBinding.tvSubtitleDateOfVaccinationLabel.setTextColor(getResources().getColor(R.color.colorAccent));

            vaccinationActivityBinding.comfirmButton.setEnabled(true);

            vaccinationActivityBinding.comfirmButton.setTextColor(getResources().getColor(R.color.grey_pallete_100));
            vaccinationActivityBinding.comfirmButton.setBackgroundColor(getResources().getColor(R.color.colorAccent));



        }
        else if(isDateMissed){

            String daysStr = Long.toString(days);
            String missedDays[] = daysStr.split("-");
            String  missedNo = missedDays[1]+"d overdue";

            daysLeft = ""+missedNo;

            vaccinationActivityBinding.tvSubtitleDateOfVaccinationLabel.setTextColor(getResources().getColor(R.color.errorColorDarkThemeLight));
            vaccinationActivityBinding.calendarDayViewBg.setBackgroundColor(getResources().getColor(R.color.errorColor));

            vaccinationActivityBinding.comfirmButton.setEnabled(true);
            vaccinationActivityBinding.comfirmButton.setTextColor(getResources().getColor(R.color.grey_pallete_100));
            vaccinationActivityBinding.comfirmButton.setBackgroundColor(getResources().getColor(R.color.errorColor));


        }
        else if(isDateDone){

            daysLeft = "recorded on "+vEvent.getVaccinationDate();

            vaccinationActivityBinding.tvSubtitleDateOfVaccinationLabel.setTextColor(getResources().getColor(R.color.colorPrimary));
            vaccinationActivityBinding.calendarDayViewBg.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            vaccinationActivityBinding.comfirmButton.setEnabled(false);
            vaccinationActivityBinding.comfirmButton.setVisibility(View.GONE);
            vaccinationActivityBinding.cancelButton.setVisibility(View.VISIBLE);
            vaccinationActivityBinding.comfirmButton.setTextColor(getResources().getColor(R.color.grey_pallete_400));
            vaccinationActivityBinding.comfirmButton.setBackground(getResources().getDrawable(R.drawable.shape_roundeded_rectangle_upcoming_light_ultra));

        }


        vaccinationActivityBinding.tvSubtitleDateOfVaccinationLabel.setText(daysLeft);

    }








    @Override
    public void onResume() {
        super.onResume();

        loadContent(vaccinationEvent);

    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_comfirm_vaccination, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        return super.onOptionsItemSelected(item);
    }




}
