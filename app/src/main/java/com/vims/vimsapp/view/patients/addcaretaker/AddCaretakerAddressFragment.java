package com.vims.vimsapp.view.patients.addcaretaker;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
/*
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
*/
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;

import vimsapp.R;
import vimsapp.databinding.PatientsCaretakerAddContactsFragmentBinding;


///**
// * A simple {@link Fragment} subclass.
// * Use the {@link AddCaretakerNameFragment#newInstance} factory method to
// * create an instance of this fragment.
// */
public class AddCaretakerAddressFragment extends Fragment implements BlockingStep {
    // TODO: Rename parameter arguments, choose names that match
   //  the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    CaretakerActivityWizardViewModel caretakerActivityWizardViewModel;
   // binding name with view
   PatientsCaretakerAddContactsFragmentBinding caretakerAddContactsFragmentBinding;




    public AddCaretakerAddressFragment() {
      //   Required empty public constructor
    }



    public static AddCaretakerAddressFragment newInstance(String param1, String param2) {
        AddCaretakerAddressFragment fragment = new AddCaretakerAddressFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

       setHasOptionsMenu(true); //to enable or display actions
    }

   @Override
   public void onPrepareOptionsMenu(Menu menu) {

        MenuItem itemSearch = menu.findItem(R.id.action_search);
      // itemSearch.setVisible(false);

       MenuItem itemSettings = menu.findItem(R.id.action_settings);
      // itemSettings.setVisible(false);
   }



   CaretakerActivityViewModelFactory caretakerActivityViewModelFactory;

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       caretakerAddContactsFragmentBinding  = DataBindingUtil.inflate(inflater, R.layout.patients_caretaker_add_contacts_fragment, container, false);

        //setSupportActionBar(toolbar);


       caretakerActivityViewModelFactory = new CaretakerActivityViewModelFactory(Injection.provideRegisterCaretakerUseCase(getActivity().getApplication()));

        caretakerActivityWizardViewModel =  ViewModelProviders.of(getActivity(), caretakerActivityViewModelFactory).get(CaretakerActivityWizardViewModel.class);
       //setUpActionBar();



        phone1TextChanged();
        phone2TextChanged();
        parishTextChanged();
        districtTextChanged();
        //villageTextChanged();
        observeCaretakerName();

        observeGender();
        observeAdressErrors();
      //  phoneNumber1TextChanged();
       // phoneNumber2TextChanged();
       // ageTextChanged();
     //   observeBioErrors();


       boolean isTablet = getResources().getBoolean(R.bool.tablet);
       boolean isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);



       if(isTablet) {
           CardView.LayoutParams layoutParams = (CardView.LayoutParams) caretakerAddContactsFragmentBinding.cardView.getLayoutParams();

           if(isPotrait) {


               layoutParams.setMargins(80,8,80,8);

               //  caretakerAddRoleFragmentBinding.cardView.setLayoutParams(layoutParams);
           }else{

               layoutParams.setMargins(200,8,200,8);


           }
       }



       setupDrawables();

       //set variables in Binding
        return caretakerAddContactsFragmentBinding.getRoot();
    }


    private  void setupDrawables(){
        caretakerAddContactsFragmentBinding.tvAddressDistrictCaretaker.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(getContext(),R.drawable.ic_place_cp_24dp), null, null, null);
        caretakerAddContactsFragmentBinding.tvAddressParishCaretaker.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(getContext(),R.drawable.ic_place_cp_24dp), null, null, null);
        caretakerAddContactsFragmentBinding.tvAddressVillageCaretaker.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(getContext(),R.drawable.ic_place_cp_24dp), null, null, null);
        caretakerAddContactsFragmentBinding.tvAddressPhone1Caretaker.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(getContext(),R.drawable.ic_phone_cp_24dp), null, null, null);
    }




    private boolean isPhoneNumberValid(CharSequence phoneNumber, String countryCode)
    {

        if(phoneNumber.toString().startsWith("0") && phoneNumber.length() == 10){

            return  true;
        }

        /*
        //NOTE: This should probably be a member variable.
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

        try {
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber.toString(), countryCode);
            return phoneUtil.isValidNumber(numberProto);
        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }
        */
        return false;
    }
    public void phone1TextChanged(){

       caretakerAddContactsFragmentBinding.tvAddressPhone1Caretaker.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.toString().trim().isEmpty()){
                    caretakerAddContactsFragmentBinding.tvAddressPhone1Caretaker.setError("Field cannot be left blank.");
                    caretakerActivityWizardViewModel.setIsAddress1Valid(false);

                }else if(!isPhoneNumberValid(charSequence,"+256")){
                    caretakerActivityWizardViewModel.setIsAddress1Valid(false);
                    caretakerAddContactsFragmentBinding.tvAddressPhone1Caretaker.setError("Invalid phone number .");

                }else{
                    caretakerActivityWizardViewModel.setlPhone1(charSequence);
                    caretakerActivityWizardViewModel.setIsAddress1Valid(true);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    public void phone2TextChanged(){

        caretakerAddContactsFragmentBinding.tvAddressPhone2Caretaker.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() != 0){
                    caretakerActivityWizardViewModel.setlPhone2(charSequence);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }








    CharSequence genderIdentity = "";

    public void observeGender(){


        Observer<CharSequence> genderObserver = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence charSequence) {

                if(charSequence != null)
                    if(charSequence.equals("Father")){

                        genderIdentity = "Mr";

                    }else if(charSequence.equals("Mother")){

                        genderIdentity = "Mrs";


                    }

                else{

                    genderIdentity = "Caretaker";


                }


            }
        };

        caretakerActivityWizardViewModel.getlRole().observe(this, genderObserver);

    }



    public void observeCaretakerName(){


        Observer<CharSequence> nameObserver = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence charSequence) {


                String title = ""+genderIdentity+" "+charSequence+"'s address ?";
                caretakerAddContactsFragmentBinding.tvWizardCaretakerAddressTitle.setText(title);
                caretakerAddContactsFragmentBinding.tvWizardCaretakerAddressTitle.setTypeface(MyFonts.getTypeFace(getContext()));

            }
        };


         caretakerActivityWizardViewModel.getFirstName().observe(this, nameObserver);



    }





    public void villageTextChanged(){

        caretakerAddContactsFragmentBinding.tvAddressVillageCaretaker.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(charSequence.toString().trim().isEmpty()){
                    caretakerAddContactsFragmentBinding.tvAddressVillageCaretaker.setError("Field cannot be left blank.");
                    caretakerActivityWizardViewModel.setIsAddressVValid(false);

                }else{
                    caretakerActivityWizardViewModel.setlVillage(charSequence);
                    caretakerActivityWizardViewModel.setIsAddressVValid(true);
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    public void parishTextChanged(){
        caretakerAddContactsFragmentBinding.tvAddressParishCaretaker.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(charSequence.toString().trim().isEmpty()){
                    caretakerAddContactsFragmentBinding.tvAddressParishCaretaker.setError("Field cannot be left blank.");
                    caretakerActivityWizardViewModel.setIsAddressParValid(false);

                }else{
                    caretakerActivityWizardViewModel.setlParish(charSequence);
                    caretakerActivityWizardViewModel.setIsAddressParValid(true);
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
    public void districtTextChanged(){
        caretakerAddContactsFragmentBinding.tvAddressDistrictCaretaker.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(charSequence.toString().trim().isEmpty()){
                    caretakerAddContactsFragmentBinding.tvAddressDistrictCaretaker.setError("Field cannot be left blank.");
                    caretakerActivityWizardViewModel.setIsAddressDValid(false);

                }else{
                    caretakerActivityWizardViewModel.setlDistrict(charSequence);
                    caretakerActivityWizardViewModel.setIsAddressDValid(true);
                }





            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }




    public void observeAdressErrors(){

        Observer<String> districtErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();

            }};

        caretakerActivityWizardViewModel.getmDistrictError().observe(this,districtErrorObserver);


        Observer<String> parishErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();

            }};

        caretakerActivityWizardViewModel.getmParishError().observe(this,parishErrorObserver);



        /*
        Observer<String> villageErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();

            }};

        caretakerActivityWizardViewModel.getmVillageError().observe(this,villageErrorObserver);
          */

        Observer<String> pErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();

            }};

        caretakerActivityWizardViewModel.getmPhoneError().observe(this,pErrorObserver);



    }















    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {


        boolean check = caretakerActivityWizardViewModel.checkAddressValid();

        if(check) {

            caretakerActivityWizardViewModel.setFullAddress();
            callback.goToNextStep();
        }

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
          callback.goToPrevStep();
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}
