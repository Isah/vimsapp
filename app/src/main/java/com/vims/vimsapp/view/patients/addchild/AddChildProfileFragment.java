package com.vims.vimsapp.view.patients.addchild;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.vims.vimsapp.utilities.Sentinel;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.injection.ModuleUI;

import vimsapp.R;

import vimsapp.databinding.PatientsChildAddProfileFragmentBinding;


///**
// * A simple {@link Fragment} subclass.
// * Use the {@link AddCaretakerNameFragment#newInstance} factory method to
// * create an instance of this fragment.
// */
public class AddChildProfileFragment extends Fragment implements BlockingStep {
    // TODO: Rename parameter arguments, choose names that match
    //  the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    AddChildActivityWizardViewModel addChildActivityWizardViewModel;
    AddChildActivityWizardViewModelFactory viewModelFactory;

    // binding name with view
    PatientsChildAddProfileFragmentBinding profileFragmentBinding;



    public AddChildProfileFragment() {
        //   Required empty public constructor
    }



    public static AddChildProfileFragment newInstance(String param1, String param2) {
        AddChildProfileFragment fragment = new AddChildProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        setHasOptionsMenu(true); //to enable or display actions
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem itemSearch = menu.findItem(R.id.action_search);
        // itemSearch.setVisible(false);

        MenuItem itemSettings = menu.findItem(R.id.action_settings);
        // itemSettings.setVisible(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        profileFragmentBinding  = DataBindingUtil.inflate(inflater, R.layout.patients_child_add_profile_fragment, container, false);

        //setSupportActionBar(toolbar);
        //itemView =  inflater.inflate(R.layout.patients_activity_add_caretaker, container, false);

        //viewModelFactory = new AddChildActivityWizardViewModelFactory(Injection.provideRegisterChildUseCase(getActivity().getApplication()), Injection.provideGenerateChildScheduleUseCase(getActivity().getApplication()));
       // addChildActivityWizardViewModel =  ViewModelProviders.of(getActivity(), viewModelFactory).get(AddChildActivityWizardViewModel.class);

        addChildActivityWizardViewModel = ModuleUI.provideInteractionViewModel(getActivity());
        //setUpActionBar();



        firstNameTextChanged();
        lastNameTextChanged();
        genderObserverOnTitle();
               //birthPlaceTextChanged();
               //caretakerNameTextChanged();
               //genderNameTextChanged();
               //birthDateTextChanged();
               // observeBioErrors();
               setupDrawables();


       // set variables in Binding

        return profileFragmentBinding.getRoot();
    }


    /*
    private void setUpActionBar(){
        toolbar = registerformBinding.toolbar;
        toolbar.setTitle("Caretaker Form");
         //((AppCompatActivity)getActivity()).setSupportActionBar(toolbar); //we replace our actionbar with our toolbar
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();

            }
        });
    }
    */


    private  void setupDrawables(){

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        profileFragmentBinding.tvFirstnameChild.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_person_cp_24dp),null,null,null);
        profileFragmentBinding.tvLastnameChild.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_person_cp_24dp),null,null,null);

    }

    public void genderObserverOnTitle(){

        Observer<CharSequence> genderObserver = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence charSequence) {

                String text = "little baby "+charSequence+"'s name ?";
                profileFragmentBinding.tvWizardNameTitle.setTypeface(MyFonts.getTypeFace(getContext()));

                //profileFragmentBinding.tvWizardNameTitle.setTypeface(MyFonts.getBoldTypeFace(getContext()));
                profileFragmentBinding.tvWizardNameTitle.setText(text);

            }
        };

      addChildActivityWizardViewModel.getmGender().observe(this, genderObserver);

    }

    public void firstNameTextChanged(){

        profileFragmentBinding.tvFirstnameChild.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                if(charSequence.toString().trim().isEmpty()){
                    profileFragmentBinding.tvFirstnameChild.setError("Field cannot be left blank.");
                    addChildActivityWizardViewModel.setIsName1Valid(false);

                }
                /*
                else if(!Sentinel.isCharacterValid(charSequence.toString()) ){
                    profileFragmentBinding.tvFirstnameChild.setError("Invalid Input!.");
                    addChildActivityWizardViewModel.setIsName1Valid(false);

                }
                */
                else{
                    addChildActivityWizardViewModel.setFirstName(charSequence);
                    addChildActivityWizardViewModel.setIsName1Valid(true);
                }



            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void lastNameTextChanged(){
        profileFragmentBinding.tvLastnameChild.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                if(charSequence.toString().trim().isEmpty()){
                    profileFragmentBinding.tvLastnameChild.setError("Field cannot be left blank.");
                    addChildActivityWizardViewModel.setIsName2Valid(false);

                }else{
                    addChildActivityWizardViewModel.setLastName(charSequence);
                    addChildActivityWizardViewModel.setIsName2Valid(true);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }





    /*
    public void birthDateTextChanged(){
        profileFragmentBinding.tvDateofbirthChild.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() != 0){
                    addChildActivityWizardViewModel.setMdateOfBirth(charSequence);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }


    public void caretakerNameTextChanged(){
        profileFragmentBinding.tvCaretakerName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() != 0){
                    addChildActivityWizardViewModel.setmCaretakerName(charSequence);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }


    public void birthPlaceTextChanged(){
        profileFragmentBinding.tvBirthPlace.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() != 0){
                    addChildActivityWizardViewModel.setmBirthPlace(charSequence);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }
    */


    public void observeBioErrors(){

        Observer<String> fnameErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();

            }};

        addChildActivityWizardViewModel.getMfirstNameError().observe(this,fnameErrorObserver);


        Observer<String> lnameErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();

            }};
        addChildActivityWizardViewModel.getMlastNameError().observe(this,lnameErrorObserver);


        Observer<String> caretakerNameError = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();
            }};

        addChildActivityWizardViewModel.getmCaretakerError().observe(this,caretakerNameError);


    }


    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        if(addChildActivityWizardViewModel.checkNameValid()) {
            callback.goToNextStep();
        }
    }



    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
       callback.goToPrevStep();
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}
