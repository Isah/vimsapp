package com.vims.vimsapp.view.patients.addcaretaker;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;
import com.vims.vimsapp.view.patients.addcaretaker.AddCaretakerAddressFragment;
import com.vims.vimsapp.view.patients.addcaretaker.AddCaretakerNameFragment;
import com.vims.vimsapp.view.patients.addcaretaker.AddCaretakerReviewFragment;
import com.vims.vimsapp.view.patients.addcaretaker.AddCaretakerRoleFragment;

/**
 * Created by Lincoln on 6/4/2018.
 */

public class AdapterCaretakerStepper extends AbstractFragmentStepAdapter {
    public AdapterCaretakerStepper(@NonNull FragmentManager fm, @NonNull Context context) {
        super(fm, context);
    }
    @Override
    public Step createStep(int position) {
        Bundle b = new Bundle();
        switch (position) {
            case 0:
                final AddCaretakerRoleFragment step1 = new AddCaretakerRoleFragment();
                b.putInt("Child", position);
                step1.setArguments(b);
                return step1;

            case 1:
                final AddCaretakerNameFragment step2 = new AddCaretakerNameFragment();
                b.putInt("Child", position);
                step2.setArguments(b);
                return step2;

            case 2:
                final AddCaretakerAddressFragment step4 = new AddCaretakerAddressFragment();
                b.putInt("Child", position);
                step4.setArguments(b);
                return step4;

            case 3:
                final AddCaretakerReviewFragment step6 = new AddCaretakerReviewFragment();
                b.putInt("Mother", position);
                step6.setArguments(b);
                return step6;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }


    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        //Override this method to set Step title for the Tabs, not necessary for other stepper types
        switch (position){
            case 0:
                return new StepViewModel.Builder(context)
                        .setTitle("Identity Role") //can be a CharSequence instead
                        .create();
            case 1:
                return new StepViewModel.Builder(context)
                        .setTitle("Basic Info") //can be a CharSequence instead
                        .create();

            case 2:
                return new StepViewModel.Builder(context)
                        .setTitle("Address Info") //can be a CharSequence instead
                        .create();

            case 3:
                return new StepViewModel.Builder(context)
                        .setTitle("Review") //can be a CharSequence instead
                        .create();
        }
        return null;
    }

}
