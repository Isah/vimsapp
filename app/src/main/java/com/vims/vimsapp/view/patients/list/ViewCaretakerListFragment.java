package com.vims.vimsapp.view.patients.list;


import android.app.ActionBar;
import android.app.ActivityOptions;
import android.app.SearchManager;
import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.manageorders.VaccinePlan;
import com.vims.vimsapp.utilities.MainThreadExecutor;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.patients.addcaretaker.AddCaretakerWelcomeActivity;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.view.patients.detail.MyDetailsTypeConverter;
import com.vims.vimsapp.view.patients.detail.ViewCaretakerDetailsActivity;


import java.util.Collections;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import vimsapp.R;
import vimsapp.databinding.PatientsFragmentCaretakerListBinding;


import static android.app.Activity.RESULT_OK;
import static android.support.v7.widget.DividerItemDecoration.VERTICAL;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ViewCaretakerListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ViewCaretakerListFragment extends LifecycleFragment implements AdapterCaretakers.OnCaretakerClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private static final int REQUEST_CODE = 2;

    // TODO: Rename and change types of parameters
    private String mSearchQuery;
    private String mParam2;


    PatientsFragmentCaretakerListBinding listBinding;
    ViewCaretakerListFragmentViewModelFactory viewModelFactory;
    ViewCaretakerListFragmentViewModel viewCaretakerListFragmentViewModel;

    //CustomViewModelFactory customViewModelFactory;


    public ViewCaretakerListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param SearchQuery Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ViewCaretakerListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ViewCaretakerListFragment newInstance(String SearchQuery, String param2) {
        ViewCaretakerListFragment fragment = new ViewCaretakerListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, SearchQuery);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSearchQuery = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

            //filter the list here
            if(adapterCaretakers!=null) {
              //  adapterCaretakers.getFilter().filter(mSearchQuery);
            }

        }

        setHasOptionsMenu(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_add_caretaker:


                Intent detailsIntent = new Intent(getContext(), AddCaretakerWelcomeActivity.class);
                startActivityForResult(detailsIntent, REQUEST_CODE);


                return true;

        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem itemSearch = menu.findItem(R.id.action_settings);
        itemSearch.setVisible(false);
        MenuItem itemSchedule = menu.findItem(R.id.action_hospital_schedule);
        itemSchedule.setVisible(false);
        MenuItem itemSchedule1 = menu.findItem(R.id.action_sync);
        itemSchedule1.setVisible(false);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        setUpSearch(menu);

    }




    private void setUpSearch(Menu menu){

        /*
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchViewItem.getActionView();
        searchView.setIconifiedByDefault(false);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        searchView.setLayoutParams(params);
        searchViewItem.expandActionView();
        */
        // Inflate the menu_home; this adds items to the action bar if it is present.

        getActivity().getMenuInflater().inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);



        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchViewItem.getActionView();
//        searchView.setIconifiedByDefault(false);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        searchView.setLayoutParams(params);
        // searchViewItem.expandActionView();



        //Android guides explicitly says:
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        // Which is in your case NOT TRUE. Because you don't want to noticemgt in MainActivity but in SearchableActivity.
        // That means you should use this ComponentName instead of getComponentName() method:

        //----  ComponentName cn = new ComponentName(this, ActivitySearch.class);

        //---- searchView.setSearchableInfo(searchManager.getSearchableInfo(cn));
        //   searchView.setBackgroundColor(Color.WHITE);
        // Do not iconify the widget;expand it by default
        //searchView.setIconifiedByDefault(false);
        // searchView.setSubmitButtonEnabled(true);



        searchView.setIconifiedByDefault(true);
        // searchView.setIconified(false);




        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {


                Toast.makeText(getContext(),""+query, Toast.LENGTH_LONG).show();


                if(adapterCaretakers!=null) {
                    adapterCaretakers.getFilter().filter(query);
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // adapterCaretakers.getFilter().filter(newText);


                return false;
            }
        });


        searchView.setQueryHint("Search caretakers");


        /*
       searchView.setOnCloseListener(new SearchView.OnCloseListener() {
           @Override
           public boolean onClose() {

             //  viewCaretakerListFragmentViewModel.loadCaretakers();

             //  Toast.makeText(getContext(),"closed", Toast.LENGTH_LONG).show();
               viewCaretakerListFragmentViewModel.loadCaretakers();


               return false;
           }
       });
       */



        searchView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {

            @Override
            public void onViewDetachedFromWindow(View arg0) {
                // search was detached/closed
                //  viewCaretakerListFragmentViewModel.loadCaretakers();

                //  Toast.makeText(getContext(),"closed", Toast.LENGTH_LONG).show();
                viewCaretakerListFragmentViewModel.loadCaretakers();


            }

            @Override
            public void onViewAttachedToWindow(View arg0) {
                // search was opened
            }
        });



    }


    View itemView;
    TextView errorTv;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        listBinding  = DataBindingUtil.inflate(inflater, R.layout.patients_fragment_caretaker_list, container, false);

        //ViewCaretakers viewCaretakerUseCase = new ViewCaretakers();
        //CustomViewModelFactory = new CustomViewModelFactory(viewCaretakerUseCase);

          viewModelFactory = new ViewCaretakerListFragmentViewModelFactory(Injection.provideViewCaretakersUseCase(getActivity().getApplication()));

          viewCaretakerListFragmentViewModel =  ViewModelProviders.of(this, viewModelFactory).get(ViewCaretakerListFragmentViewModel.class);

          preferenceHandler = PreferenceHandler.getInstance(getContext());

        //setUpActionBar();
         addCaretakerButton();
         setUpRecyclerView();
         viewCaretakerListFragmentViewModel.loadCaretakers();
         observeCareTakers();
         observerErrors();
         observeProgressBar();

         fonts();

        // errorTv.setVisibility(View.VISIBLE);
         return listBinding.getRoot();

    }



    Toolbar toolbar;

    private void setUpActionBar(){

        toolbar = itemView.findViewById(R.id.toolbar);

        toolbar.setTitle("Caretakers");

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar); //we replace our actionbar with our toolbar
       // ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
       // ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);


    }


    //gone for now
    public void addCaretakerButton(){
         FloatingActionButton fab = listBinding.btnSaveCaretaker;
         //fab.setVisibility(View.GONE);

         fab.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                /*
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        */
               // Intent intent = new Intent(getContext(),CaretakerActivityWizard.class);
               // startActivityForResult(intent,REQUEST_CODE);


                // Intent detailsIntent = new Intent(getContext(), AddCaretakerWelcomeActivity.class);
                // startActivityForResult(detailsIntent,REQUEST_CODE);

                 /*
                 // Use TaskStackBuilder to build the back stack and get the PendingIntent
                 PendingIntent pendingIntent =
                         TaskStackBuilder.create(getContext())
                                 // add all of DetailsActivity's parents to the stack,
                                 // followed by DetailsActivity itself
                                 .addNextIntentWithParentStack(detailsIntent)
                                 .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                 NotificationCompat.Builder builder = new NotificationCompat.Builder(getContext());
                 builder.setContentIntent(pendingIntent);
                 */

                // startActivityForResult(detailsIntent,REQUEST_CODE);


             }
         });
     }


    int no_columns = 0;
    int DEFAULT_THEME_ID =0;
    int LIGHT_THEME_ID=0;
    int recycler_view_drawable =0;

    public void setUpRecyclerView() {

        DEFAULT_THEME_ID = R.style.AppTheme_Dark;
        LIGHT_THEME_ID = R.style.AppTheme_Light;

        /* ///////////////////////////////////////////////////////////////
        DisplayMetrics metrics = new DisplayMetrics();
        itemView.setTranslationY(metrics.heightPixels);

        Interpolator interpolator = new BounceInterpolator();
        itemView.animate().setInterpolator(interpolator)
                .setDuration(300)
                .setStartDelay(500)
                .translationXBy(-metrics.heightPixels)
                .start();
        */ //////////////////////////////////////////////////////////////

        PreferenceHandler preferenceHandler = PreferenceHandler.getInstance(getContext());

        int theme_id = preferenceHandler.getIntValue("THEME_ID");

        if (theme_id == DEFAULT_THEME_ID) {
            recycler_view_drawable = R.drawable.divider_horizontal;
        } else {
            recycler_view_drawable = R.drawable.divider_horizontal_light;
        }

        DividerItemDecoration decoration = new DividerItemDecoration(getContext(), VERTICAL);
        Drawable verticalDivider = ContextCompat.getDrawable(getActivity(), recycler_view_drawable);
        decoration.setDrawable(verticalDivider);
        //setup the recycler view
        recyclerView = listBinding.recyclerviewContent.recyclerview;
        //recyclerView.addItemDecoration(decoration);

        Interpolator overshoot = new OvershootInterpolator(1f);
        Interpolator accelerateDecelerate = new AccelerateDecelerateInterpolator();
        Interpolator bounce = new BounceInterpolator();

        SlideInUpAnimator animator = new SlideInUpAnimator(overshoot);
        recyclerView.setItemAnimator(animator);
        recyclerView.getItemAnimator().setAddDuration(getResources().getInteger(R.integer.duration_recycler_view));
        //setting animation on scrolling

        recyclerView.setHasFixedSize(true); //enhance recycler view scroll
        recyclerView.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset_large);
        recyclerView.addItemDecoration(itemDecoration);


        //mAdapter.setClickListener(new FragmentHome()); //cardviews

        viewCaretakerListFragmentViewModel.loadCaretakers();

        //mAdapter.notifyItemInserted(0);

        boolean isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);
        boolean isTablet = getResources().getBoolean(R.bool.tablet);


        if (isTablet) {

            //  if(isPotrait) {

             no_columns = 3;
             setRecyclerViewLayoutManager(GRID_LAYOUT_MANAGER, no_columns);

            //  setRecyclerViewLayoutManager(LINEAR_LAYOUT_MANAGER, no_columns);


            //   }
            // else {

            //no_columns = 4;
           // setRecyclerViewLayoutManager(GRID_LAYOUT_MANAGER, no_columns);
           // setRecyclerViewLayoutManager(LINEAR_LAYOUT_MANAGER, no_columns);

            // }

        } else {


            no_columns = 2;
            setRecyclerViewLayoutManager(LINEAR_LAYOUT_MANAGER, no_columns);



          //  no_columns = 2;
            //  setRecyclerViewLayoutManager(LINEAR_LAYOUT_MANAGER, no_columns);
           // setRecyclerViewLayoutManager(GRID_LAYOUT_MANAGER, no_columns);


            /*

            if (isPotrait) {

                no_columns = 2;
                setRecyclerViewLayoutManager(LINEAR_LAYOUT_MANAGER, no_columns);
                // setRecyclerViewLayoutManager(GRID_LAYOUT_MANAGER, no_columns);

            } else {

                no_columns = 4;
                //  setRecyclerViewLayoutManager(LINEAR_LAYOUT_MANAGER, no_columns);
                setRecyclerViewLayoutManager(GRID_LAYOUT_MANAGER, no_columns);

            }
            */


        }





    }



    private void fonts(){

        listBinding.recyclerviewContent.errorMsgTv.setTypeface(MyFonts.getTypeFace(getContext()));
        listBinding.recyclerviewContent.errorMsgTvSubtitle.setTypeface(MyFonts.getTypeFace(getContext()));

    }

    public void observeProgressBar(){

        Observer<Boolean> progressObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {

                if(aBoolean!=null) {
                    if (aBoolean) {
                        listBinding.recyclerviewContent.progressbarCaretakers.setVisibility(View.VISIBLE);
                    }else{
                        listBinding.recyclerviewContent.progressbarCaretakers.setVisibility(View.INVISIBLE);
                        listBinding.recyclerviewContent.errorMsgNoCaretakers.setVisibility(View.VISIBLE);

                        listBinding.recyclerviewContent.errorView.setVisibility(View.VISIBLE);
                        listBinding.recyclerviewContent.errorView.setVisibility(View.VISIBLE);



                    }
                }

            }
        };

       viewCaretakerListFragmentViewModel.getIsStartProgress().observe(this,progressObserver);

    }

    public void observeCareTakers(){
        Observer<List<Caretaker>> caretakerObserver = new Observer<List<Caretaker>>() {
            @Override
            public void onChanged(@Nullable List<Caretaker> careTakers) {

                if (careTakers != null) {
                    recyclerView.setVisibility(View.VISIBLE);

                    listBinding.recyclerviewContent.errorView.setVisibility(View.INVISIBLE);


                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {

                            updateUI(careTakers);
                        }
                    };
                    new MainThreadExecutor(1000).execute(runnable);



                }else{

                    //listBinding.recyclerviewContent.errorMsgNoCaretakers.setVisibility(View.VISIBLE);

                }

            }};

        viewCaretakerListFragmentViewModel.getCaretakers().observe(this, caretakerObserver);
    }


    public void observerErrors(){
        final Observer<String> errorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String error) {

                if (error != null)
                    recyclerView.setVisibility(View.INVISIBLE);
                  //  listBinding.errorView.setVisibility(View.VISIBLE);
                  //  errorTv.setVisibility(View.VISIBLE);
                    errorTv.setText(error);

            }};

        viewCaretakerListFragmentViewModel.getErrorMessage().observe(this, errorObserver);

    }



    PreferenceHandler preferenceHandler;

    @Override
    public void onResume() {
        super.onResume();

        int isRecorded = preferenceHandler.getIntValue(PreferenceHandler.IS_CARETAKER_REGISTERED_INT);
        int isChildRecorded = preferenceHandler.getIntValue(PreferenceHandler.IS_CHILD_REGISTERED_INT);
        if(isRecorded == 1){
            preferenceHandler.setIntValue(PreferenceHandler.IS_CARETAKER_REGISTERED_INT,0);
            viewCaretakerListFragmentViewModel.loadCaretakers();
        }else if(isChildRecorded == 1){
            viewCaretakerListFragmentViewModel.loadCaretakers();
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                //viewCaretakerListFragmentViewModel.loadCaretakers();
                /*
                String fn = data.getStringExtra("first_name");
                String ln = data.getStringExtra("last_name");
                String pn = data.getStringExtra("phone");

                Caretaker careTaker = new Caretaker();
                careTaker.setMobile(pn);
                careTaker.setFirstName(fn);
                careTaker.setLastName(ln);


                //viewmodel.add to database the view should be updated automatically
                List<Caretaker> children = new ArrayList<>();
                children.add(careTaker);


                adapterChildren = new AdapterCaretakers(children);

                //mAdapter.setClickListener(new FragmentHome()); //cardviews
                adapterChildren.setHasStableIds(true);


                recyclerView.setAdapter(adapterChildren);
                //mAdapter.notifyItemInserted(0);
                adapterChildren.notifyItemInserted(0);
                */

            }
        }

    }

    int GRID_LAYOUT_MANAGER = 0;
    int STAGGERED_GRID_LAYOUT_MANAGER = 2;
    int LINEAR_LAYOUT_MANAGER = 1;
    int mCurrentLayoutManagerType = 0;


    RecyclerView recyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    AdapterCaretakers adapterCaretakers;


    public void setRecyclerViewLayoutManager(int layoutManagerType, int no_grid_columns) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (recyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case 0:

                mLayoutManager = new GridLayoutManager(getActivity(), no_grid_columns,GridLayoutManager.VERTICAL, false);
                mCurrentLayoutManagerType = 0;
                break;
            case 1:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = 1;
                break;
            case 2:
                int no_sgrid_columns = getResources().getInteger(R.integer.staggered_grid_columns);
                mLayoutManager = new StaggeredGridLayoutManager(no_sgrid_columns,GridLayoutManager.VERTICAL);
                mCurrentLayoutManagerType = 2;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = 1;
        }

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.scrollToPosition(scrollPosition);
    }


    public void updateUI(List<Caretaker> caretakers) {

        Collections.sort(caretakers);
        adapterCaretakers = new AdapterCaretakers(caretakers,getContext());
        adapterCaretakers.setClickListener(this);


        //adapterChildren.replaceItems(careTakers);
        adapterCaretakers.setHasStableIds(true);
        adapterCaretakers.notifyItemInserted(0);
        recyclerView.setAdapter(adapterCaretakers);

    }



    @Override
    public void onCaretakerClicked(View view, int position, Caretaker caretaker, ImageView profilePic) {


        //transfer the click to main actvity that will implement the fragmentlistner
      //  fragmentListener.onFragmentCardInteraction(view,caretaker,profilePic);

        if(position == -5){

            Intent detailsIntent = new Intent(getContext(), AddCaretakerWelcomeActivity.class);
            startActivityForResult(detailsIntent,REQUEST_CODE);

        }else{

            Bundle bundle = new Bundle();
            bundle.putString("caretaker", MyDetailsTypeConverter.fromCatakerToString(caretaker));

            // if(isPotrait){
            // Intent for the activity to open when user selects the notification
            Intent detailsIntent = new Intent(getContext(), ViewCaretakerDetailsActivity.class);
            detailsIntent.setAction(Intent.ACTION_VIEW);
            detailsIntent.putExtras(bundle);

        /*
        // Use TaskStackBuilder to build the back stack and get the PendingIntent
        PendingIntent pendingIntent = TaskStackBuilder.create(this)
                        // add all of DetailsActivity's parents to the stack,
                        // followed by DetailsActivity itself
                        .addNextIntentWithParentStack(detailsIntent)
                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentIntent(pendingIntent);
        */


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                //shared Element Transitions
                // ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, p1, p2, p3);


                String trans = getResources().getString(R.string.transitionNameImageCaretaker);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), profilePic, trans);
                startActivity(detailsIntent, options.toBundle());

                //Exit Transitions
                // Bundle mbundle = ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle();
                // startActivity(intent, mbundle);

            } else {
                startActivity(detailsIntent);
            }


        }




    }



     /*
    OnFragmentInteractionListener fragmentListener;

    //overide to make sure container actvity has implemented the listner

     @Override
     public void onAttach(Context context) {
     super.onAttach(context);

     if (context instanceof OnFragmentInteractionListener) {

     fragmentListener = (OnFragmentInteractionListener) context;

     } else {

     throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");

     }

     }

     @Override
     public void onDetach() {
     super.onDetach();
     fragmentListener = null;
     }



    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentCardInteraction(View view, Caretaker caretaker, ImageView profilePic);
    }
    */

     /**
      * This interface must be implemented by activities that contain this
      * fragment to allow an interaction in this fragment to be communicated
      * to the activity and potentially other fragments contained in that
      * activity.
      * <p>
      * See the Android Training lesson <a href=
      * "http://developer.android.com/training/basics/fragments/communicating.html"
      * >Communicating with Other Fragments</a> for more information.
     */






}
