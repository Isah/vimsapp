package com.vims.vimsapp.view.patients.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;
import com.vims.vimsapp.view.patients.addcaretaker.ProfileInfo;

import java.util.List;

import vimsapp.R;

/**
 * Created by root on 6/25/18.
 */

public class AdapterProfileInfoArray extends ArrayAdapter<ProfileInfo> {

    public AdapterProfileInfoArray(Context context, List<ProfileInfo> profileInfos) {
        super(context, 0, profileInfos);

    }


    /**
     @Override
     public View getView(int position, View convertView, ViewGroup parent) {
     // Get the data item for this position
     Child user = getItem(position);
     // Check if an existing view is being reused, otherwise inflate the view
     if (convertView == null) {
     convertView = LayoutInflater.from(getContext()).inflate(R.layout.patients_card_child, parent, false);
     }
     // Lookup view for data population
     TextView tvName = (TextView) convertView.findViewById(R.id.tv_child_firstname);
     TextView tvHome = (TextView) convertView.findViewById(R.id.tv_child_lastname);
     // Populate the data into the template view using the data object
     tvName.setText(user.getFirstName());
     tvHome.setText(user.getLastName());
     // Return the completed view to render on screen
     return convertView;
     }
     */

    // View lookup cache
    private static class ViewHolder {
        TextView title;
        TextView subtitle;
        ImageView avator;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ProfileInfo profileInfo = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.patients_card_caretaker_profile, parent, false);
            viewHolder.title = convertView.findViewById(R.id.tv_profile_title);
            viewHolder.subtitle = convertView.findViewById(R.id.tv_profile_subtitle);
           // viewHolder.avator = convertView.findViewById(R.id.imageview_profile_card);
            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);
        } else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data from the data object via the viewHolder object
        // into the template view.
        if(profileInfo!=null) {
            viewHolder.title.setText(profileInfo.getTitle());
            viewHolder.subtitle.setText(profileInfo.getSubtitle());

            Picasso.with(getContext())
                    .load(profileInfo.getAvator())
                    .fit()
                    .placeholder(R.drawable.logo_icon_google_guideline_final_fill_128)
                    .into(viewHolder.avator);
        }
        // Return the completed view to render on screen
        return convertView;
    }
}