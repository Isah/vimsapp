package com.vims.vimsapp.view.patients.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.vims.vimsapp.entities.child.Child;

import java.util.List;

import vimsapp.R;

/**
 * Created by root on 6/10/18.
 */

public class AdapterChildrenArray extends ArrayAdapter<Child> {



    public AdapterChildrenArray(Context context, List<Child> users) {
        super(context, 0, users);

    }


    /**
     @Override
     public View getView(int position, View convertView, ViewGroup parent) {
     // Get the data item for this position
     Child user = getItem(position);
     // Check if an existing view is being reused, otherwise inflate the view
     if (convertView == null) {
     convertView = LayoutInflater.from(getContext()).inflate(R.layout.patients_card_child, parent, false);
     }
     // Lookup view for data population
     TextView tvName = (TextView) convertView.findViewById(R.id.tv_child_firstname);
     TextView tvHome = (TextView) convertView.findViewById(R.id.tv_child_lastname);
     // Populate the data into the template view using the data object
     tvName.setText(user.getFirstName());
     tvHome.setText(user.getLastName());
     // Return the completed view to render on screen
     return convertView;
     }
     */

    // View lookup cache
    private static class ViewHolder {
        TextView name;
        TextView home;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Child user = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.patients_card_child, parent, false);
          //  viewHolder.name = convertView.findViewById(R.id.tv_child_firstname);
            //viewHolder.home = convertView.findViewById(R.id.tv_child_lastname);
            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);
        } else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data from the data object via the viewHolder object
        // into the template view.
        if(user!=null) {
            viewHolder.name.setText(user.getFirstName());
            viewHolder.home.setText(user.getLastName());
        }
        // Return the completed view to render on screen
        return convertView;
    }
}