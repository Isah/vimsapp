package com.vims.vimsapp.view.patients.addchild;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import vimsapp.R;
import vimsapp.databinding.PatientsChildAddWeightFragmentBinding;


///**
// * A simple {@link Fragment} subclass.
// * Use the {@link AddCaretakerNameFragment#newInstance} factory method to
// * create an instance of this fragment.
// */
public class AddChildWeightFragment extends Fragment implements BlockingStep{
    // TODO: Rename parameter arguments, choose names that match
    //  the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    AddChildActivityWizardViewModel addChildActivityWizardViewModel;
    AddChildActivityWizardViewModelFactory viewModelFactory;

    // binding name with view
    //PatientsChildAddDateofbirthFragmentBinding dateofbirthFragmentBinding;
    PatientsChildAddWeightFragmentBinding dateofbirthFragmentBinding;




    public AddChildWeightFragment() {
        //   Required empty public constructor
    }



    public static AddChildWeightFragment newInstance(String param1, String param2) {
        AddChildWeightFragment fragment = new AddChildWeightFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        setHasOptionsMenu(true); //to enable or display actions
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem itemSearch = menu.findItem(R.id.action_search);
        // itemSearch.setVisible(false);

        MenuItem itemSettings = menu.findItem(R.id.action_settings);
        // itemSettings.setVisible(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        dateofbirthFragmentBinding  = DataBindingUtil.inflate(inflater, R.layout.patients_child_add_weight_fragment, container, false);


        //setSupportActionBar(toolbar);
        //itemView =  inflater.inflate(R.layout.patients_activity_add_caretaker, container, false);

        viewModelFactory = new AddChildActivityWizardViewModelFactory(Injection.provideRegisterChildUseCase(getActivity().getApplication()), Injection.provideGenerateChildScheduleUseCase(getActivity().getApplication()));

        addChildActivityWizardViewModel =  ViewModelProviders.of(getActivity(), viewModelFactory).get(AddChildActivityWizardViewModel.class);
        //setUpActionBar();



        weightTextChanged();
        observeWeight1();
        observeWeight2();
        //lastNameTextChanged();
        //birthPlaceTextChanged();
        //caretakerNameTextChanged();
        //genderNameTextChanged();
        //birthDateTextChanged();
        observeGender();
        observeFName();
        observeGenderErrors();


        //set variables in Binding
        return dateofbirthFragmentBinding.getRoot();
    }





    private void observeWeight1(){

        Observer<CharSequence> weighObr = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence charSequence) {


                if(charSequence!=null) {
                    String stringValue = charSequence.toString();

                   // String[] strValues = String.valueOf(stringValue).split("\\.");
                    float f1 = Float.parseFloat(stringValue);
                   // float f2 = Float.parseFloat(strValues[1]);
                   // numberPicker.setValue(Float.floatToIntBits(f1));
                   // numberPickerFloat.setValue(Float.floatToIntBits(f2));
                }

            }
        };

        addChildActivityWizardViewModel.getmBirthWeight().observe(this,weighObr);


    }


    private void observeWeight2(){

        Observer<CharSequence> weighObr = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence charSequence) {


                if(charSequence!=null) {
                    String stringValue = charSequence.toString();

                   // String[] strValues = String.valueOf(stringValue).split("\\.");
                  //  float f1 = Float.parseFloat(strValues[0]);
                    float f2 = Float.parseFloat(stringValue);
                    //numberPicker.setValue(Float.floatToIntBits(f1));
                   // numberPickerFloat.setValue(Float.floatToIntBits(f2));

                }


            }
        };



        addChildActivityWizardViewModel.getmBirthWeightFloat().observe(this,weighObr);


    }



    NumberPicker numberPicker;
    NumberPicker numberPickerFloat;
    private void weightTextChanged() {


        numberPickerFloat = dateofbirthFragmentBinding.numberPickerChildWeightFloat;
        numberPickerFloat.setMinValue(0);
        numberPickerFloat.setMaxValue(10);
       // numberPickerFloat.setValue(0);
        numberPickerFloat.setWrapSelectorWheel(true);

        CharSequence cs1 = "0";
        addChildActivityWizardViewModel.setmBirthWeightFloat(cs1);




        numberPicker = dateofbirthFragmentBinding.numberPickerChildWeight;
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(10);

       // numberPicker.setValue(3);
        CharSequence cs = "0";
        addChildActivityWizardViewModel.setmBirthWeight(cs);

        numberPicker.setWrapSelectorWheel(true);


        NumberPicker.OnValueChangeListener valueChangeListener1 = new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

               // Toast.makeText(getContext(), "selected number " + numberPicker.getValue(), Toast.LENGTH_SHORT).show();
                CharSequence nm = Integer.toString(numberPicker.getValue());

                addChildActivityWizardViewModel.setmBirthWeight(nm);

            }
        };

        NumberPicker.OnValueChangeListener valueChangeListener2 = new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

                // Toast.makeText(getContext(), "selected number " + numberPicker.getValue(), Toast.LENGTH_SHORT).show();

                CharSequence value = Integer.toString(numberPickerFloat.getValue());

                addChildActivityWizardViewModel.setmBirthWeightFloat(value);

            }
        };


        NumberFormat formatter1 = new DecimalFormat("#0.00 kg");

        NumberPicker.Formatter formatter = new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                //return String.format("%02d kg", value);
                //String.format("%.2f", 4.52135);
                //formatter1.format(value);
                return  String.format("%2d", value);
            }

        };


        numberPicker.setFormatter(formatter);
        numberPicker.setOnValueChangedListener(valueChangeListener1);

        numberPickerFloat.setFormatter(formatter);
        numberPickerFloat.setOnValueChangedListener(valueChangeListener2);

    }




    public void buildPickerDialog(){


        final NumberPicker numberPicker = new NumberPicker(getActivity());
        numberPicker.setMinValue(20);
        numberPicker.setMaxValue(60);


        NumberPicker.OnValueChangeListener valueChangeListener = new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

            }
        };


        NumberPicker.Formatter formatter = new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return String.format("%02d kg", value);
            }
        };


        numberPicker.setFormatter(formatter);
        numberPicker.setOnValueChangedListener(valueChangeListener);


        new AlertDialog.Builder(getContext())
                .setTitle("Child Weight")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {



                    }

                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {



                    }
                })
                .setView(numberPicker)
                .show();



    }

    CharSequence genderIdentity = "";

    public void observeGender(){


        Observer<CharSequence> genderObserver = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence charSequence) {

                if(charSequence != null)
                if(charSequence.equals("boy")){

                    genderIdentity = "his";

                }else{

                    genderIdentity = "her";


                }


            }
        };

        addChildActivityWizardViewModel.getmGender().observe(this, genderObserver);

    }



    private void observeFName(){

        Observer<CharSequence> nameObserver = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence charSequence) {


                String text = "Baby "+charSequence+" weight in kg?";


                dateofbirthFragmentBinding.tvWizardDatebirth.setTypeface(MyFonts.getTypeFace(getContext()));

                dateofbirthFragmentBinding.tvWizardDatebirth.setText(text);


            }
        };

        addChildActivityWizardViewModel.getFirstName().observe(this, nameObserver);

    }




    public void observeGenderErrors(){

        Observer<String> fnameErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();

            }};

       // addChildActivityWizardViewModel.getmGender().observe(this,fnameErrorObserver);


    }






    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {

        if(addChildActivityWizardViewModel.checkBirth()) {
            addChildActivityWizardViewModel.setFullWeight();
            callback.goToNextStep();
        }

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
       callback.goToPrevStep();
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}
