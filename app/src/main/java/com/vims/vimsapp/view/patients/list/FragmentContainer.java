package com.vims.vimsapp.view.patients.list;


import com.vims.vimsapp.entities.child.Caretaker;

public interface FragmentContainer {

    void showItems(Caretaker caretaker);


    boolean onBackPressed();



}
