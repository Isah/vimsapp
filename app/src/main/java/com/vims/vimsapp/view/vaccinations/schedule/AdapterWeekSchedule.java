package com.vims.vimsapp.view.vaccinations.schedule;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;


import com.vims.vimsapp.entities.schedule.WeekRepeat;
import com.vims.vimsapp.entities.schedule.WeekSchedule;


import java.util.ArrayList;
import java.util.List;

import vimsapp.R;

/**
 * Created by root on 5/31/18.
 */

public class AdapterWeekSchedule extends RecyclerView.Adapter<AdapterWeekSchedule.MyViewHolder> {


    List<WeekSchedule> weekSchedules;
    Context context;

    public AdapterWeekSchedule(List<WeekSchedule> children, Context context) {
        this.context = context;
        this.weekSchedules = children;
        //  this.children.add(addChild);
        //1. Animations bug
        setHasStableIds(true); //in constructor


    }


    public List<WeekSchedule> getWeekSchedules() {
        return weekSchedules;
    }

    //(1) Declare Interface
    protected OnCardItemClickListener myclickListener;  //our interace
    // private WebView webView;


    int layout_id  = 0;
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


     layout_id  = R.layout.vaccinations_card_bottom_sheet;



        View itemView = LayoutInflater.from(parent.getContext()).inflate(layout_id, parent, false);

        return new MyViewHolder(itemView);
    }


    //(2) implements the click listner
    public class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{
        //the lookup cache
        CheckBox dayOfWeekCheckBox;
        RadioGroup weekOfMonthRadioGroup;

        CheckBox week1CheckBox;
        CheckBox week2CheckBox;
        CheckBox weekLastCheckBox;



        public MyViewHolder(View view) {
            super(view);
            dayOfWeekCheckBox = view.findViewById(R.id.chkDayWeek);
            week1CheckBox = view.findViewById(R.id.chkDayWeek1);
            week2CheckBox = view.findViewById(R.id.chkDayWeek2);
            weekLastCheckBox = view.findViewById(R.id.chkDayWeek3);
            weekOfMonthRadioGroup =   view.findViewById(R.id.week_radio_group);

            //setFontface
            //(3) Bind the click listener
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();
        }

    }



    @Override
    public void onBindViewHolder(final MyViewHolder holder, int pos) {

         int position = holder.getAdapterPosition();

         WeekSchedule ws = getWeekSchedule(position);

         List<WeekRepeat> weekRepeats = new ArrayList<>();
         ws.setWeekRepeats(weekRepeats);


         int dayOfWeek = position +2; //mon = 2

        holder.dayOfWeekCheckBox.setChecked(false);
        holder.dayOfWeekCheckBox.setText(ws.getDayOfWeekName());

        holder.dayOfWeekCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){

                    ws.setSelected(true);
                    ws.setDayOfWeek(dayOfWeek);

                   // weekSchedules.remove(position);
                   // weekSchedules.set(position,ws);

                    //holder.weekOfMonthRadioGroup.setVisibility(View.VISIBLE);

                    holder.week1CheckBox.setVisibility(View.VISIBLE);
                    holder.week2CheckBox.setVisibility(View.VISIBLE);
                    holder.weekLastCheckBox.setVisibility(View.VISIBLE);


                }else{


                    ws.setSelected(false);

                   // weekSchedules.remove(position);
                   // weekSchedules.set(position,ws);

                    //holder.weekOfMonthRadioGroup.setVisibility(View.INVISIBLE);

                    holder.week1CheckBox.setVisibility(View.INVISIBLE);
                    holder.week2CheckBox.setVisibility(View.INVISIBLE);
                    holder.weekLastCheckBox.setVisibility(View.INVISIBLE);

                }

            }
        });


        holder.week1CheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if(isChecked){

                   WeekRepeat weekRepeat = new WeekRepeat();
                   weekRepeat.setDayOfWeekName(ws.getDayOfWeekName());
                   weekRepeat.setRepeatDay(1);
                   weekRepeat.setSelected(true);
                    //weekSchedules.remove(position);

                    //weekRepeats.add(weekRepeat);
                    ws.getWeekRepeats().add(weekRepeat);


                }else{

                  //  ws.getWeekRepeats().get(0).setSelected(false);

                    for(WeekRepeat weekRepeat: ws.getWeekRepeats()){
                        if(weekRepeat.getRepeatDay()==1){

                            weekRepeat.setSelected(false);
                        }
                    }

                   // ws.setWeekOfMonth(1);
                   // weekSchedules.remove(position);
                   // weekSchedules.add(ws);

                }


            }
        });

        holder.week2CheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if(isChecked){


                    WeekRepeat weekRepeat = new WeekRepeat();
                    weekRepeat.setDayOfWeekName(ws.getDayOfWeekName());
                    weekRepeat.setRepeatDay(2);
                    weekRepeat.setSelected(true);
                    //weekSchedules.remove(position);
                    ws.getWeekRepeats().add(weekRepeat);


                }else{

                  //  ws.getWeekRepeats().get(1).setSelected(false);

                    for(WeekRepeat weekRepeat: ws.getWeekRepeats()){
                        if(weekRepeat.getRepeatDay()==2){

                            weekRepeat.setSelected(false);
                        }
                    }


                    // ws.setWeekOfMonth(1);
                    // weekSchedules.remove(position);
                    // weekSchedules.add(ws);

                }


            }
        });

        holder.weekLastCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if(isChecked){


                    WeekRepeat weekRepeat = new WeekRepeat();
                    weekRepeat.setDayOfWeekName(ws.getDayOfWeekName());
                    weekRepeat.setRepeatDay(5);
                    weekRepeat.setSelected(true);
                    //weekSchedules.remove(position);
                    ws.getWeekRepeats().add(weekRepeat);


                }else{

                    for(WeekRepeat weekRepeat: ws.getWeekRepeats()){
                        if(weekRepeat.getRepeatDay()==5){

                            weekRepeat.setSelected(false);
                        }
                    }


                    // ws.setWeekOfMonth(1);
                    // weekSchedules.remove(position);
                    // weekSchedules.add(ws);

                }


            }
        });



        /*
        holder.weekOfMonthRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton radio = group.findViewById(group.getCheckedRadioButtonId());



                // Score selected = (Score) getItem((Integer) radioGroup.getTag());
                // boolean showMOT = false;
                radio.setChecked(true);
                radio.setSelected(true);
                switch (checkedId) {
                    case R.id.wks1:

                        ws.setWeekOfMonth(1);
                        weekSchedules.remove(position);
                        weekSchedules.add(ws);

                        break;
                    case R.id.wks2:

                        ws.setWeekOfMonth(2);
                        weekSchedules.remove(position);
                        weekSchedules.add(ws);

                        break;
                    case R.id.wks3:

                        ws.setWeekOfMonth(3);
                        weekSchedules.remove(position);
                        weekSchedules.add(ws);

                        break;
                    case R.id.wks4:

                        ws.setWeekOfMonth(4);
                        weekSchedules.remove(position);
                        weekSchedules.add(ws);
                        break;

                    case R.id.wks6:

                        for (int w = 1; w < 6; w++) {
                            WeekSchedule weekSchedule = new WeekSchedule();

                            weekSchedule.setDayOfWeek(ws.getDayOfWeek());
                            weekSchedule.setSelected(ws.isSelected());
                            weekSchedule.setWeekOfMonth(w);

                            if(!isContain(weekSchedules,weekSchedule)){
                             weekSchedules.add(weekSchedule);
                            }else {
                                weekSchedules.remove(weekSchedule);
                            }
                        }

                        //remove a schedule with weekschedule of 0
                        isContain0(weekSchedules);

                }


            }
        });

         */


        // int descLength = album.getDescription().length();
        // holder.itemView.setTag(descLength);


        //ONCLICK LISTNER FOR SHARE BUTTON

        /*
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // int position = getAdapterPosition(); // gets item position

                //myclickListener.onClick(v, position); // call the onClick in the OnCardItemClickListener Interface i created


            }
        });
        */


    }



    private boolean isContain(List<WeekSchedule> weekSchedules, WeekSchedule ws){

        for(WeekSchedule weekSchedule: weekSchedules) {
            if(weekSchedule.getWeekOfMonth() == ws.getWeekOfMonth() && weekSchedule.getDayOfWeek() == ws.getWeekOfMonth()){
                return true;
            }
        }
        return false;
    }

    private void isContain0(List<WeekSchedule> weekSchedules) {
        ArrayList<WeekSchedule> list = new ArrayList<>(weekSchedules);

        for (WeekSchedule wks : list) {
            if (wks.getWeekOfMonth() == 0) {
                weekSchedules.remove(wks);
            }
        }
    }




    @Override
    public int getItemCount() {

        return weekSchedules.size();
    }


    //2. Animations bug
    @Override
    public long getItemId(int position) {
        return weekSchedules.get(position).hashCode(); //Any unique id
    }



    private WeekSchedule getWeekSchedule(int position)
    {
        return weekSchedules.get(position);
    }




    //interfaces
    public void setClickListener(OnCardItemClickListener onCardItemClickListener) {
        this.myclickListener = onCardItemClickListener;
    }


    public interface OnCardItemClickListener {

        void onClick(View view, int position);
    }






}
