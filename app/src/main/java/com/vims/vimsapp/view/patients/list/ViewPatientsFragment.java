package com.vims.vimsapp.view.patients.list;

import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

//import com.vims.vimsapp.view.vaccinations.AdapterViewPager;

import vimsapp.R;
import vimsapp.databinding.PatientsListFragmentBinding;




/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ViewPatientsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ViewPatientsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ViewPatientsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    private PatientsListFragmentBinding patientsListFragmentBinding;


    public ViewPatientsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentHome.
     */
    // TODO: Rename and change types and number of parameters
    public static ViewPatientsFragment newInstance(String param1, String param2) {
        ViewPatientsFragment fragment = new ViewPatientsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        patientsListFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.patients_list_fragment, container, false);

        setupViewPager(savedInstanceState);
        // setupTabIcons();
        //((AppCompatActivity)getActivity()).getSupportActionBar().setLogo(R.drawable.notice_48);
        // final CollapsingToolbarLayout collapsingToolbarLayout = patientsListFragmentBinding.collapsingToolbar;
        /// collapsingToolbarLayout.setTitleEnabled(false);


        Toolbar toolbar = patientsListFragmentBinding.toolbar;
        SpannableStringBuilder str = new SpannableStringBuilder("VACCINATED");
        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 10, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        toolbar.setTitle(str);

        toolbar.setTitleMarginStart(getResources().getInteger(R.integer.fragment_title_margin));

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar); //we replace our actionbar with our toolbar
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if(actionBar!=null) {
         //   actionBar.setTitle("Patients");

        }


        return patientsListFragmentBinding.getRoot();

    }


    ///these icons go on tabs
    private int[] tabIcons = {
          //  R.drawable.ic_battery_full_black_24dp,
           // R.drawable.ic_battery_20_black_24dp,
           // R.drawable.ic_battery_alert_black_24dp
    };

    //setup tab icons
    private void setupTabIcons() {

        /*
        TabLayout.Tab tab1 = vaccinationHomeBinding.tabsCaretaker.getTabAt(0);
        if(tab1!=null) {
            tab1.setIcon(tabIcons[0]);
        }

        TabLayout.Tab tab2 = vaccinationHomeBinding.tabsCaretaker.getTabAt(1);
        if(tab2!=null) {
            tab2.setIcon(tabIcons[1]);
        }

        TabLayout.Tab tab3 = vaccinationHomeBinding.tabsCaretaker.getTabAt(2);
        if(tab3!=null) {
            tab3.setIcon(tabIcons[2]);
        }

        */

    }




    //set up the fragments on the viewpager
    private void setupViewPager(Bundle bundle) {

        //setup the viewpager--- it contains the fragments to be swiped
        ViewPager viewPager =  patientsListFragmentBinding.viewpagerHome;
        //assign viewpager to tablayout
        patientsListFragmentBinding.tabsCaretaker.setupWithViewPager(viewPager);


        AdapterViewPagerPatients adapter = new AdapterViewPagerPatients(getChildFragmentManager());//child

        adapter.addFragment(ViewCaretakerListFragment.newInstance(null,null), "CARETAKERS");
        adapter.addFragment(ViewChildrenListFragment.newInstance(null), "CHILDREN");


        viewPager.setAdapter(adapter);


    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    /**
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    **/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
