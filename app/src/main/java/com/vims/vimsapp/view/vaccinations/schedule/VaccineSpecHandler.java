package com.vims.vimsapp.view.vaccinations.schedule;


import com.vims.vimsapp.entities.vaccinations.VaccineSpecification;
import com.vims.vimsapp.viewcoverage.ReadVaccines;

import java.util.List;

public class VaccineSpecHandler {

    public static int SPEC_TYPE_MODE = 2;
    public static int SPEC_TYPE_SITE = 1;
    public static int SPEC_TYPE_PROTECTS = 0;

    public static String getVaccineDesc(int spect_type, String vaccineName) {

        String vaccine = "";

        ReadVaccines readVaccines = ReadVaccines.getInstance();
        List<VaccineSpecification> specs = readVaccines.getVaccineSpecificationList();
        for (VaccineSpecification spec : specs) {
            if (spec.getVaccineName().equals(vaccineName)) {
                if (spect_type == SPEC_TYPE_MODE) {

                    vaccine = spec.getModeOfAdministration();

                    break;

                } else if (spect_type == SPEC_TYPE_SITE) {

                    vaccine = spec.getSiteOfAdministration();

                    break;

                } else {

                    vaccine = spec.getProtectsAgainst();

                    break;
                }

            }


        }

        return vaccine;

    }
}

