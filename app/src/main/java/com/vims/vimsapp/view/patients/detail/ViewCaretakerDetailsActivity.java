package com.vims.vimsapp.view.patients.detail;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.Toast;



import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.searchchild.ViewChildDataSource;
import com.vims.vimsapp.searchchild.ViewChildRepository;
import com.vims.vimsapp.utilities.DrawableManager;
import com.vims.vimsapp.utilities.ScreenManager;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.injection.ModuleRepository;
import com.vims.vimsapp.view.patients.addchild.AddChildActivityWizard;
import com.vims.vimsapp.view.patients.addchild.AddChildWelcomeActivity;
import com.vims.vimsapp.view.patients.list.AdapterChildren;
import com.vims.vimsapp.view.patients.list.ItemOffsetDecoration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import vimsapp.R;
import vimsapp.databinding.PatientsFragmentCaretakerDetailBinding;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;

public class ViewCaretakerDetailsActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener{


    ImageView mProfileImage;
    private boolean mIsAvatarShown = true;
    private int mMaxScrollSize;
    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;

    ViewCaretakerDetailsActivityViewModel detailsActivityViewModel;
    PatientsFragmentCaretakerDetailBinding detailBinding;

    int layout1 = R.layout.patients_fragment_caretaker_detail;
    //int layout2 = R.layout.patients_fragment_caretaker_detail_tablet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        detailBinding = DataBindingUtil.setContentView(this, layout1);
        detailsActivityViewModel =  ViewModelProviders.of(this).get(ViewCaretakerDetailsActivityViewModel.class);


        ScreenManager.setScreenOrientation(this);

        Bundle b = getIntent().getExtras();
        if(b!=null) {

            String caretakerString = b.getString("caretaker");
            Caretaker caretaker = MyDetailsTypeConverter.fromStringToCaretaker(caretakerString);


           /** Caretaker Details Display**/
            SlideInUpAnimator animator = new SlideInUpAnimator(new OvershootInterpolator(1f));
            recyclerViewSetUp(detailBinding.recyclerviewDetailChildren, animator);
            setCaretakerDetail(caretaker);
            /** End of Caretaker Details Display**/


            setUpDynamicAppBar(0);



            drawables();


            detailBinding.contentChildrenInfo.buttonAddChild.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(),AddChildActivityWizard.class);
                    intent.putExtra("caretakerId", caretaker.getCaretakerId());
                    startActivity(intent);

                }
            });


            //  Picasso.with(getApplicationContext()).load(imageFile).resize(100,100).into(detailBinding.materialupProfileImage);
            // detailBinding.materialupProfileImage.setImageBitmap(profilePic);

            detailBinding.fabCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Uri u = Uri.parse("tel:" + detailBinding.tvPreviewCaretakerPhone1.getText().toString());
                    Intent i = new Intent(Intent.ACTION_DIAL, u);
                    try
                    {
                        // Launch the Phone app's dialer with a phone
                        // number to dial a call.
                        startActivity(i);
                    }
                    catch (SecurityException s)
                    {
                        // show() method display the toast with
                        // exception message.
                        Toast.makeText(getApplicationContext(), s.getMessage(), Toast.LENGTH_LONG).show();
                    }

                }
            });


            //setupViewPager(b);
        }



    }




    private void drawables(){

        boolean isTab = getResources().getBoolean(R.bool.tablet);

        int phone24 = R.drawable.ic_phone_cp_24dp;
        int phone16 = R.drawable.ic_phone_cp_16dp;

        int place24 = R.drawable.ic_place_cp_24dp;
        int place16 = R.drawable.ic_place_cp_16dp;

        int phone;
        int place;

        if(isTab){
            phone = phone24;
            place = place24;
        }else{
            phone = phone16;
            place = place16;
        }


        Drawable drawableIcon = AppCompatResources.getDrawable(getApplication(),phone);
        DrawableManager.setUpDrawableStart(detailBinding.tvPreviewCaretakerPhone1,drawableIcon);

        Drawable drawableIcon2 = AppCompatResources.getDrawable(getApplication(),place);
        DrawableManager.setUpDrawableStart(detailBinding.address,drawableIcon2);


    }



    private void setUpProfileImage(Caretaker caretaker){
        if(!TextUtils.isEmpty(caretaker.getProfileImagePath())){

            File imageFile = new File(caretaker.getProfileImagePath());
            Bitmap profilePic = BitmapFactory.decodeFile(imageFile.getPath());
            //  detailBinding.materialupProfileImage.setImageBitmap(decodeSampledBitmapFromResource(imageFile.getPath(), 50, 50));

            // Bitmap myBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icons8_doctor_female_50);
            RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(getResources(), profilePic);
            drawable.setCircular(true);
            detailBinding.materialupProfileImage.setImageDrawable(drawable);

        } else {

            Bitmap myBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_person_light_white_24dp);
            RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(getResources(), myBitmap);
            drawable.setCircular(true);
            // detailBinding.materialupProfileImage.setImageDrawable(drawable);

        }

    }

    private void setPalleteProfile(String profilePath){
        //1. Get the Bitmap
            /* These methods attempt to allocate memory for the constructed bitmap and
            therefore can easily result in an OutOfMemory exception. use injustDecodeBounds*/

        File imageFile = new File(profilePath);
        Bitmap profilePic = BitmapFactory.decodeFile(imageFile.getPath());


        //2.Get the dimensions coz image may be large


        //3. Decide if full image can be loaded into memory
             /* To tell the decoder to subsample the image, loading a smaller version into
             memory, set inSampleSize to true

             For example, an image with resolution 2048x1536 that is decoded with an
             inSampleSize of 4 produces a bitmap of approximately 512x384. Loading this into
             memory uses 0.75MB rather than 12MB for
             the full image (assuming a bitmap configuration of ARGB_8888).

             */
        //  Bitmap myBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_doctor_male);
        if (profilePic != null ) {

            Log.i("swatch","to be generated");
            //do processing on background thread
            Palette.from(profilePic).clearFilters().generate(new Palette.PaletteAsyncListener() {
                @Override
                public void onGenerated(Palette palette) {

                    Log.i("swatch","generated");
                    Palette.Swatch vibrantSwatch = palette.getDominantSwatch();
                    Palette.Swatch vibrantSwatchDark = palette.getDarkMutedSwatch();

                    if(vibrantSwatch != null && vibrantSwatchDark !=null){

                        // int backgroundColor = vibrantSwatch.getRgb();
                        int backgroundColor = vibrantSwatch.getPopulation();

                        ColorStateList.valueOf(backgroundColor);

                        int dark =   vibrantSwatchDark.getPopulation();

                        int transparentRGBInt = getColorWithAplha(backgroundColor, 0.5f);

                        // .setBackgroundColor(backgroundColor);
                        //detailBinding.materialupProfileBackdrop.setBackgroundColor(backgroundColor);
                       // detailBinding.materialupProfileImage.setBorderColor(backgroundColor);

                        Log.i("swatch",""+backgroundColor);
                        //setUpDynamicAppBar(transparentRGBInt);

                    }

                }
            });

        }else{

            Log.i("swatch","pic is null ");

        }

    }





    private void setCaretakerDetail(Caretaker caretakerDetail){

        detailBinding.firstnameProfileTv.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
        detailBinding.address.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
       // detailBinding.tvPreviewCaretakerRole.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
        detailBinding.tvPreviewCaretakerPhone1.setTypeface(MyFonts.getTypeFace(getApplicationContext()));
        detailBinding.contentChildrenInfo.sizeOfChildren.setTypeface(MyFonts.getTypeFace(getApplicationContext()));


        DrawableManager.setUpDrawableStart(detailBinding.tvPreviewCaretakerPhone1,getResources().getDrawable(R.drawable.ic_phone_cp_24dp));
        DrawableManager.setUpDrawableStart(detailBinding.address,getResources().getDrawable(R.drawable.ic_place_cp_24dp));

        detailBinding.tvPreviewCaretakerPhone1.setPaintFlags(detailBinding.tvPreviewCaretakerPhone1.getPaintFlags() | Paint.HINTING_ON);

       // detailBinding.tvPreviewCaretakerRole.setText(caretakerDetail.getGenderRole());
       //detailBinding.tvPreviewCaretakerAddress.setText(caretakerDetail.getDistrict());

        String gender;
        if(caretakerDetail.getGenderRole().equals("Male")){
        gender = "Mr.";
        }else{
            gender = "Mrs.";
        }
        String full_name = ""+gender+" "+caretakerDetail.getFirstName()+" "+caretakerDetail.getLastName();


        detailBinding.firstnameProfileTv.setText(full_name);

        String address = caretakerDetail.getDistrict()+", "+caretakerDetail.getParish();
        detailBinding.address.setText(address);


        //  AgeCalculator calculator = new AgeCalculator();
       // AgeCalculator.Age age = calculator.calculateAge(DateCalendarConverter.stringToDateWithoutTime(caretakerDetail.getBirthDate()));
       // String myage = Integer.toString(age.getYears());

     //detailBinding.tvPreviewCaretakerBirthdate.setText(myage);
     detailBinding.tvPreviewCaretakerPhone1.setText(caretakerDetail.getPhoneNo());



     loadChildren(caretakerDetail.getCaretakerId());

    }


    private void loadChildren(String id){

        ViewChildRepository p = ModuleRepository.provideViewChildRepository(getApplication());

        p.getChildrenByCaretakerId(id, new ViewChildDataSource.LoadChildCallback() {
            @Override
            public void onChildrenLoaded(List<Child> childrenlist) {


                updateBasicInfo(childrenlist);

            }

            @Override
            public void onDataNotAvailable() {


            }
        });

    }


    AdapterChildren adapterChildren;

    public void updateBasicInfo(List<Child> profileInfos){

        adapterChildren = new AdapterChildren(profileInfos,this);
        // adapterChildren.replaceItems(careTakers);
        adapterChildren.setHasStableIds(true);
        adapterChildren.notifyItemInserted(0);
        detailBinding.recyclerviewDetailChildren.setAdapter(adapterChildren);

    }



    public void recyclerViewSetUp(RecyclerView recyclerView, SlideInUpAnimator animator){

        DividerItemDecoration decoration = new DividerItemDecoration(getApplicationContext(), VERTICAL);
        //decoration.setDrawable(getResources().getDrawable(R.drawable.shape_borderline_ultra_thin_darktheme));
      //  recyclerView.addItemDecoration(decoration);


        recyclerView.setItemAnimator(animator);
        recyclerView.getItemAnimator().setAddDuration(2000);
        //setting animation on scrolling

        recyclerView.setHasFixedSize(true); //enhance recycler view scroll
        recyclerView.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getApplicationContext(), R.dimen.item_offset_profile);
        recyclerView.addItemDecoration(itemDecoration);

        RecyclerView.LayoutManager mLayoutManager;

        boolean isTab = getResources().getBoolean(R.bool.tablet);
        if(isTab){
            mLayoutManager = new GridLayoutManager(getApplication(), 3,GridLayoutManager.VERTICAL, false);


        }else{
            mLayoutManager = new LinearLayoutManager(getApplicationContext());


        }



        recyclerView.setLayoutManager(mLayoutManager);


    }







    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    public static Bitmap decodeSampledBitmapFromResource(String path,
                                                         //1. Get the Bitmap
            /* These methods attempt to allocate memory for the constructed bitmap and
            therefore can easily result in an OutOfMemory exception. use injustDecodeBounds*/

                                                         //2.Get the dimensions coz image may be large


                                                         //3. Decide if full image can be loaded into memory
             /* To tell the decoder to subsample the image, loading a smaller version into
             memory, set inSampleSize to true

             For example, an image with resolution 2048x1536 that is decoded with an
             inSampleSize of 4 produces a bitmap of approximately 512x384. Loading this into
             memory uses 0.75MB rather than 12MB for
             the full image (assuming a bitmap configuration of ARGB_8888).

             */



                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);


        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;
        String imageType = options.outMimeType;

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }




    private int getColorWithAplha(int color, float ratio)
    {
        int transColor = 0;
        int alpha = Math.round(Color.alpha(color) * ratio);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);
        transColor = Color.argb(alpha, r, g, b);
        return transColor ;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

    }




    //MENUS
    public void setUpDynamicAppBar(int darkcolor){
        // windowActionBarOverlay

        Toolbar toolbar = detailBinding.toolbarCaretakerDetail;
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null) {
            //getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                 onBackPressed();
                // finish();
                //support transitions
                // NavUtils.navigateUpFromSameTask(getParent());
            }
        });



        final CollapsingToolbarLayout collapsingToolbarLayout = detailBinding.mainCollapsing;
        //collapsingToolbarLayout.setTitleEnabled(false);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout.setCollapsedTitleGravity(Gravity.BOTTOM);
          collapsingToolbarLayout.setTitleEnabled(false);
        collapsingToolbarLayout.setTitle("care");




        AppBarLayout appbarLayout = detailBinding.materialupAppbar;
        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();

    }



    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reports, menu);




        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

            long caretaker_id = 0;
            if(detailsActivityViewModel.getCaretaker().getValue() != null) {
                Caretaker caretaker = detailsActivityViewModel.getCaretaker().getValue();
              //  caretaker_id = caretaker.getId();
            }


           // Intent intent = new Intent(getApplicationContext(),AddChildProfileFragment.class);
            //intent.putExtra("caretaker_id", caretaker_id);
            //startActivity(intent);


            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */




    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {


        mProfileImage = detailBinding.materialupProfileImage;
        FloatingActionButton fab = detailBinding.fabCall;


        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int percentage = (Math.abs(verticalOffset)) * 100 / mMaxScrollSize;

        if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
            mIsAvatarShown = false;

            mProfileImage.animate()
                    .scaleY(0).scaleX(0)
                    .setDuration(200)
                    .start();

            //////////////////////
            fab.animate()
                    .scaleY(0).scaleX(0)
                    .setDuration(200)
                    .start();

        }

        else if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
            mIsAvatarShown = true;

            mProfileImage.animate()
                    .scaleY(1).scaleX(1)
                    .start();

            ///////////////////////////
            fab.animate()
                    .scaleY(1).scaleX(1)
                    .start();


        }

    }

    private static final int REQUEST_CODE = 2;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

             Bundle bundle =   data.getExtras();
             if(bundle!=null) {
               String child_string =  bundle.getString("child_saved");
               //Child child = (Child) MyDetailsTypeConverter.fromStringTo(child_string);
                 /*
                 initCaretaker();
                 if(detailsActivityViewModel.getCaretaker().getValue()!=null) {
                     Toast.makeText(this, child_string + " isBack" + " " + detailsActivityViewModel.getCaretaker().getValue().getLastName(), Toast.LENGTH_LONG).show();

                     adapterChildren.addAll(detailsActivityViewModel.getCaretaker().getValue().getChildren());
                     adapterChildren.notifyDataSetChanged();
                     childlistview.setAdapter(adapterChildren);
                 }
                */
             }



            }
        }

    }

    //set up the fragments on the viewpager
    /*
    private void setupViewPager(Bundle bundle) {


        //setup the viewpager--- it contains the fragments to be swiped
        ViewPager viewPager =  detailBinding.materialupViewpager;
        //assign viewpager to tablayout
        detailBinding.tabsCaretaker.setupWithViewPager(viewPager);


         ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());//child

         adapter.addFragment(FragmentCaretakerDetailsProfile.newInstance(bundle), getResources().getString(R.string.title_tab1_caretaker_details));
         adapter.addFragment(ViewCaretakerChildrenListFragment.newInstance(bundle), getResources().getString(R.string.title_tab2_caretaker_details));
         viewPager.setAdapter(adapter);



    }
    */

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }


        public void removeItem(int position) {
            mFragmentList.remove(position);
        }


        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
