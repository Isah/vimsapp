package com.vims.vimsapp.view.patients.list;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.patients.addcaretaker.ProfileInfo;


import java.util.List;

import vimsapp.R;

/**
 * Created by root on 5/31/18.
 */

public class AdapterProfileInfo extends RecyclerView.Adapter<AdapterProfileInfo.MyViewHolder> {


    List<ProfileInfo> profileInfos;
    Context context;

    public AdapterProfileInfo(List<ProfileInfo> profileInfos, Context context) {
        this.context = context;
        this.profileInfos = profileInfos;
        //1. Animations bug
        setHasStableIds(true); //in constructor


    }



    public void replaceItems(List<ProfileInfo> caretakers){
        this.profileInfos = caretakers;

        //1. Animations bug
        setHasStableIds(true); //in constructor

    }



    //(1) Declare Interface
    protected OnCardItemClickListener myclickListener;  //our interace
    // private WebView webView;



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;

        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.patients_card_caretaker_profile, parent, false);

        return new MyViewHolder(itemView);
    }


    //(2) implements the click listner
    public class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{
        //the lookup cache
        TextView title;
        TextView subtitle;
        ImageView avator;


        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.tv_profile_title);
            subtitle =   view.findViewById(R.id.tv_profile_subtitle);
           // avator = view.findViewById(R.id.imageview_profile_card);

            title.setTypeface(MyFonts.getTypeFace(context));
            subtitle.setTypeface(MyFonts.getTypeFace(context));

            //setFontface
            //(3) Bind the click listener
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition(); // gets item position

//              myclickListener.onClick(v, position); // call the onClick in the OnCardItemClickListener Interface i created

          //  ProfileInfo caretaker = children.get(position);

          //  Intent intent = new Intent(context, ViewCaretakerDetailsActivity.class);

          //  Bundle bundle = new Bundle();
          //  bundle.putString("caretaker", MyDetailsTypeConverter.fromCatakerToString(caretaker));

           // intent.putExtras(bundle);
          //  loadDetailsActivity(intent, v, "transition");


        }

    }




    public void loadDetailsActivity(Intent intent, View view, String transition_name){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            //shared Element Transitions
            //ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, p1, p2, p3);
            //    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, view, transition_name);
            // view.getContext().startActivity(intent, options.toBundle());

            //Exit Transitions
            Bundle bundle = ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle();
            context.startActivity(intent, bundle);


        } else {
            context.startActivity(intent);
        }


    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final ProfileInfo profileInfo = getProfileInfos(position);



        holder.title.setText(profileInfo.getTitle());
        holder.subtitle.setText(profileInfo.getSubtitle());
       // holder.avator.setImageDrawable(context.getResources().getDrawable(profileInfo.getAvator()));
       // Picasso.with(context).load(profileInfo.getAvator()).fit().placeholder(R.drawable.vimslogo_small).into(holder.avator);



        // int descLength = album.getDescription().length();
        // holder.itemView.setTag(descLength);


        /*

        //ONCLICK LISTNER FOR SHARE BUTTON

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(mContext, "Sharing.. "+album.getTitle(), Toast.LENGTH_SHORT).show();

            }
        });
       */

    }



    @Override
    public int getItemCount() {

        return profileInfos.size();
    }


    //2. Animations bug
    @Override
    public long getItemId(int position) {
        return profileInfos.get(position).hashCode(); //Any unique id
    }



    private ProfileInfo getProfileInfos(int position)
    {
        return profileInfos.get(position);
    }




    //interfaces
    public void setClickListener(OnCardItemClickListener onCardItemClickListener) {
        this.myclickListener = onCardItemClickListener;
    }


    public interface OnCardItemClickListener {

        void onClick(View view, int position);
    }






}
