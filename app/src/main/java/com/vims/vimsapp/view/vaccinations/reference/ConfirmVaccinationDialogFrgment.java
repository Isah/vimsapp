package com.vims.vimsapp.view.vaccinations.reference;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.view.patients.detail.MyDetailsTypeConverter;
import com.vims.vimsapp.view.vaccinations.viewmodel.ViewScheduleUpcomingFragmentViewModel;
import com.vims.vimsapp.view.vaccinations.viewmodel.ViewScheduleUpcomingFragmentViewModelFactory;

import vimsapp.R;


public class ConfirmVaccinationDialogFrgment extends DialogFragment {
        int mNum;

        /**
         * Create a new instance of MyDialogFragment, providing "num"
         * as an argument.
         */
        static ConfirmVaccinationDialogFrgment newInstance(int num) {
            ConfirmVaccinationDialogFrgment f = new ConfirmVaccinationDialogFrgment();

            // Supply num input as an argument.
            Bundle args = new Bundle();
            args.putInt("num", num);
            f.setArguments(args);

            return f;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mNum = getArguments().getInt("num");

            // Pick a style based on the num.
            int style = DialogFragment.STYLE_NORMAL, theme = 0;
            switch ((mNum-1)%6) {
                case 1: style = DialogFragment.STYLE_NO_TITLE; break;
                case 2: style = DialogFragment.STYLE_NO_FRAME; break;
                case 3: style = DialogFragment.STYLE_NO_INPUT; break;
                case 4: style = DialogFragment.STYLE_NORMAL; break;
                case 5: style = DialogFragment.STYLE_NORMAL; break;
                case 6: style = DialogFragment.STYLE_NO_TITLE; break;
                case 7: style = DialogFragment.STYLE_NO_FRAME; break;
                case 8: style = DialogFragment.STYLE_NORMAL; break;
            }
            switch ((mNum-1)%6) {
                case 4: theme = android.R.style.Theme_Holo; break;
                case 5: theme = android.R.style.Theme_Holo_Light_Dialog; break;
                case 6: theme = android.R.style.Theme_Holo_Light; break;
                case 7: theme = android.R.style.Theme_Holo_Light_Panel; break;
                case 8: theme = android.R.style.Theme_Holo_Light; break;
            }
            setStyle(style, theme);
        }



    ViewScheduleUpcomingFragmentViewModel viewScheduleFragmentViewModel;
    ViewScheduleUpcomingFragmentViewModelFactory scheduleUpcomingFragmentViewModelFactory;


    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {




            View v = inflater.inflate(R.layout.vaccination_comfirm_vaccination_dialog_fragment, container, false);
            View tvTitle = v.findViewById(R.id.tv_title_dialog);
            View tvDesc = v.findViewById(R.id.description);



            //  String dialogString = "Dialog #" + mNum + ": using style " + getNameForNum(mNum);
            String dialogTitle = "Confirm Recording Vaccination ?";
            String dialogDesc =  "Are you sure about the operation, once confirmed its final!";

            ((TextView)tvTitle).setText(dialogTitle);
            ((TextView)tvDesc).setText(dialogDesc);

            // Watch for button clicks.
            Button button = v.findViewById(R.id.comfirmButton);

            Bundle bundle = getArguments();
            int position = bundle.getInt("position");
            VaccinationEvent vaccinationEvent = MyDetailsTypeConverter.fromStringToVaccinationEvent(bundle.getString("vevent"));


            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // When button is clicked, call up to owning activity.
                    //(getActivity()).showDialog(0);
                   // (getActivity()).finish();

                    onVaccinationChoiceListner.onConfirmation(vaccinationEvent,position);

                    android.app.FragmentManager fragmentMgr = getActivity().getFragmentManager();
                    fragmentMgr.popBackStack();

                   // FragmentTransaction ft = fragmentMgr.beginTransaction();

                }
            });

            return v;
        }







        public interface OnVaccinationChoiceListner{

            void onConfirmation(VaccinationEvent vaccinationEvent, int position);
            void onCancel();

        }


        public void setOnVaccinationChoiceListner(OnVaccinationChoiceListner onVaccinationChoiceListner){
            this.onVaccinationChoiceListner = onVaccinationChoiceListner;
        }

        private OnVaccinationChoiceListner onVaccinationChoiceListner;

    }



