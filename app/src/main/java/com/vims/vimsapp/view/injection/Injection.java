package com.vims.vimsapp.view.injection;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.LifecycleActivity;
import android.arch.lifecycle.LifecycleOwner;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.vims.vimsapp.addchild_controller.AddChildController;
import com.vims.vimsapp.manage_order_controller.ManageOrderController;
import com.vims.vimsapp.manage_order_controller.ManageOrderPresenter;
import com.vims.vimsapp.manage_order_controller.ViewOrderHistoryController;
import com.vims.vimsapp.manageorders.RequestOrder;
import com.vims.vimsapp.manageorders.ViewOrders;
import com.vims.vimsapp.presenters.makeorder_presenter.ManageOrderPresenterImpl;
import com.vims.vimsapp.presenters.makeorder_presenter.ManageOrderView;
import com.vims.vimsapp.presenters.makeorder_presenter.ManageOrderViewModel;
import com.vims.vimsapp.presenters.makeorder_presenter.ViewOrderHistoryViewModel;
import com.vims.vimsapp.recordvaccinations.RecordChildVaccination;
import com.vims.vimsapp.registerchild.GenerateChildSchedule;
import com.vims.vimsapp.managehospitalschedule.GenerateHospitalSchedule;
import com.vims.vimsapp.registerchild.RegisterCaretaker;
import com.vims.vimsapp.registerchild.RegisterChild;
import com.vims.vimsapp.searchchild.SearchPatient;
import com.vims.vimsapp.searchchild.ViewCaretakers;
import com.vims.vimsapp.searchchild.ViewChildren;
import com.vims.vimsapp.searchvaccinations.GenerateVaccinationStats;
import com.vims.vimsapp.searchvaccinations.SearchVaccinationsSchedule;
import com.vims.vimsapp.viewcoverage.GenerateReport;

public class Injection {



    // The main app will know about the controller
    public static AddChildController provideController(Fragment fragment, Application application){


        return new AddChildController(ModuleUI.provideAddChildPresenter(fragment), Injection.provideGenerateChildScheduleUseCase(application));
    }


    ///// USE CASES ///////////////////////////////////////////////////////////////////////////

    public static RegisterCaretaker provideRegisterCaretakerUseCase(Application application) {
        return new RegisterCaretaker(ModuleRepository.provideAddChildRepository(application));
    }


    public static RegisterChild provideRegisterChildUseCase(Application application) {
        return new RegisterChild(ModuleRepository.provideAddChildRepository(application));
    }

    public static ViewChildren provideReadChildrenUseCase(Application application) {
        return new ViewChildren(ModuleRepository.provideViewChildRepository(application));
    }


    public static ViewCaretakers provideViewCaretakersUseCase(Application app) {
        return new ViewCaretakers(ModuleRepository.provideViewChildRepository(app));
    }

    public static SearchPatient provideSearchPatientsUseCase(Application application) {
        return SearchPatient.getInstance(ModuleRepository.provideViewChildRepository(application));
    }

    public static GenerateChildSchedule provideGenerateChildScheduleUseCase(Application application) {
        return GenerateChildSchedule.getInstance(ModuleRepository.provideManageHospitalScheduleRepository(application),ModuleRepository.provideAddChildRepository(application));
    }

    public static SearchVaccinationsSchedule provideSearchVaccinationsUseCase(Application application){

        return SearchVaccinationsSchedule.getInstance(ModuleRepository.provideSearchVaccinationRepository(application));
    }






    public static RecordChildVaccination provideRecordVaccinationUseCase(Application application) {
        return RecordChildVaccination.getInstance(ModuleRepository.provideAddVaccinationRepository(application));
    }


    public static SearchVaccinationsSchedule provideReadChildSchedule(Application application) {
        return SearchVaccinationsSchedule.getInstance(ModuleRepository.provideSearchVaccinationRepository(application));
    }

    private static GenerateVaccinationStats provideGenerateVaccinationStats(Application application) {
        return new GenerateVaccinationStats(ModuleRepository.provideSearchVaccinationRepository(application));
    }



    public static GenerateHospitalSchedule provideHospitalSchedule(Application application){
        return GenerateHospitalSchedule.getInstance(ModuleRepository.provideManageHospitalScheduleRepository(application));

    }







    public static GenerateReport provideGenerateReport(Application application) {
        return new GenerateReport(provideGenerateVaccinationStats(application));
    }






    ///////////////   ManageOrders ////////////////////////////////////

    public static ManageOrderController provideManageOrderController( ManageOrderView manageOrderView, Application application){

        return ManageOrderController.getInstance(ModuleUI.provideManageOrderPresenter(manageOrderView), provideRequestOrder(application), provideViewOrders(application));

    }


    public static ViewOrderHistoryController provideViewOrderHistoryController(ViewOrderHistoryViewModel historyViewModel, Application application){

        return ViewOrderHistoryController.getInstance(ModuleUI.provideViewOrderHistoryPresenter(historyViewModel), provideViewOrders(application));

    }





    public static RequestOrder provideRequestOrder(Application application){
        return RequestOrder.getInstance(ModuleRepository.provideOrdersRepository(application));

    }


    public static ViewOrders provideViewOrders(Application application){
        return ViewOrders.getInstance(ModuleRepository.provideOrdersRepository(application),provideSearchVaccinationsUseCase(application));

    }




}
