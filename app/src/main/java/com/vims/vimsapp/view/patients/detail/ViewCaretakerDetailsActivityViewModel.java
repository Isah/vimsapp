package com.vims.vimsapp.view.patients.detail;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;


import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.searchchild.ViewChildRepository;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.injection.ModuleRepository;

import java.util.List;



/**
 * Created by root on 6/10/18.
 */

public class ViewCaretakerDetailsActivityViewModel extends AndroidViewModel{


    private MutableLiveData<Caretaker> caretaker;
    private MutableLiveData<String> caretakerLiveId;
    private LiveData<List<Child>> children;


    ViewChildRepository patientsRepository;

    public ViewCaretakerDetailsActivityViewModel(Application application) {
        super(application);

          caretaker = new MutableLiveData<>();

          patientsRepository = ModuleRepository.provideViewChildRepository(getApplication());

          caretakerLiveId = new MutableLiveData<>();


               }




    public MutableLiveData<String> getCaretaker_id() {
        return caretakerLiveId;
    }

    public void searchChildren(int newCaretakerId) {
            caretakerLiveId.postValue(Integer.toString(newCaretakerId));

    }

    public MutableLiveData<Caretaker> getCaretaker() {
        return caretaker;
    }

    public void setCaretaker(Caretaker caretaker) {
        this.caretaker.postValue(caretaker);
    }


    /*
    public LiveData<List<Child>> getChildren() {

        if(children ==null){

            children = new MutableLiveData<>();
            loadChildren();
        }
        return children;
    }


    public void loadChildren(){
        children = Transformations.switchMap(caretakerLiveId, s ->
                patientsRepository.getChildrenByCaretakerId(s));

    }
    */



}
