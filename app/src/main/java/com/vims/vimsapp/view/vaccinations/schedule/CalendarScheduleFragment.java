package com.vims.vimsapp.view.vaccinations.schedule;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.calendar.CurrentDayDecorator;
import com.vims.vimsapp.utilities.calendar.EventsDecorator;
import com.vims.vimsapp.utilities.calendar.WeekendsDecorator;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.patients.detail.MyDetailsTypeConverter;
import com.vims.vimsapp.view.patients.list.ItemOffsetDecoration;
import com.vims.vimsapp.view.vaccinations.CardHeader;
import com.vims.vimsapp.view.vaccinations.viewmodel.ViewScheduleFragmentViewModel;
import com.vims.vimsapp.view.vaccinations.viewmodel.ViewScheduleFragmentViewModelFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import vimsapp.R;
import vimsapp.databinding.VaccinationCalendarScheduleFragmentBinding;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CalendarScheduleFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CalendarScheduleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CalendarScheduleFragment extends Fragment implements OnDateSelectedListener, OnMonthChangedListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "schedule_type";

    // TODO: Rename and change types of parameters
    private String mScheduleType;


    private OnFragmentInteractionListener mListener;

    public CalendarScheduleFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param scheduleType Parameter 1.

     * @return A new instance of fragment FragmentSchedule.
     */
    // TODO: Rename and change types and number of parameters
    public static CalendarScheduleFragment newInstance(String scheduleType) {
        CalendarScheduleFragment fragment = new CalendarScheduleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, scheduleType);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mScheduleType = getArguments().getString(ARG_PARAM1);

        }
    }


    VaccinationCalendarScheduleFragmentBinding vaccinationFragmentScheduleBinding;
    ViewScheduleFragmentViewModel viewScheduleFragmentViewModel;
    ViewScheduleFragmentViewModelFactory viewScheduleFragmentViewModelFactory;


    private List<CalendarDay> calevents = new ArrayList<>();

    boolean isPotrait;
    boolean isTablet;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {



        vaccinationFragmentScheduleBinding = DataBindingUtil.inflate(inflater, R.layout.vaccination_calendar_schedule_fragment, container, false);
        viewScheduleFragmentViewModelFactory = new ViewScheduleFragmentViewModelFactory(Injection.provideReadChildSchedule(getActivity().getApplication()));
        viewScheduleFragmentViewModel = ViewModelProviders.of(this,viewScheduleFragmentViewModelFactory).get(ViewScheduleFragmentViewModel.class);


        vaccinationFragmentScheduleBinding.calendarView.setOnDateChangedListener(this);
        vaccinationFragmentScheduleBinding.calendarView.setOnMonthChangedListener(this);


        isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);
        isTablet = getResources().getBoolean(R.bool.tablet);

        /*
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if(actionBar!=null) {
            actionBar.setTitle("Calendar");
        }
        */

        //((AppCompatActivity)getActivity()).getSupportActionBar().setLogo(R.drawable.notice_48);
        vaccinationFragmentScheduleBinding.collapsingToolbar.setTitleEnabled(false);
       // collapsingToolbarLayout.setTitleEnabled(false);


        Toolbar toolbar = vaccinationFragmentScheduleBinding.toolbar;
        SpannableStringBuilder str = new SpannableStringBuilder("Calendar");
        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 8, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        toolbar.setTitle(str);

        toolbar.setTitleMarginStart(48);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar); //we replace our actionbar with our toolbar
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if(actionBar!=null) {
          //  actionBar.setTitle("Schedule");

        }


        CalendarMode calendarMode;

        if(isTablet){

            calendarMode = CalendarMode.MONTHS;

        }else {

            calendarMode = CalendarMode.WEEKS;

        }

        setUpCalendarView(calendarMode);
        setUpRecyclerView();


        //observeChildScheduleData();

        viewScheduleFragmentViewModel.loadVaccinationCards(DateCalendarConverter.dateWithTimeToDateWithoutTime(new Date()));
        observeChildScheduleToDecorateCalendar();


       // vaccinationFragmentScheduleBinding.calendarView.setDateSelected(calendar.getTime(), true);
      //  vaccinationFragmentScheduleBinding.calendarView.setDateSelected(CalendarDay.today(), false);
      //  vaccinationFragmentScheduleBinding.calendarView

        return  vaccinationFragmentScheduleBinding.getRoot();

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        super.onResume();
        viewScheduleFragmentViewModel.loadVaccinationCards(null);
      //  observeChildScheduleToDecorateCalendar();


    }





    public void setUpCalendarView(CalendarMode calendarMode){

        //vaccinationFragmentScheduleBinding.progressbarEvents.setVisibility(View.INVISIBLE);

        vaccinationFragmentScheduleBinding.calendarView.setTopbarVisible(true);

        vaccinationFragmentScheduleBinding.calendarView.state().edit()
                .setFirstDayOfWeek(Calendar.MONDAY)
                .setMinimumDate(CalendarDay.from(2016, 4, 3))
                .setMaximumDate(CalendarDay.from(2120, 5, 12))
                .setCalendarDisplayMode(calendarMode)
                                .commit();


        //vaccinationFragmentScheduleBinding.calendarView.setDateSelected(new Date(), true);
        vaccinationFragmentScheduleBinding.calendarView.setSelected(true);

        setEventsOfTheDay(new Date());



        /////////////////////////////////////////////////////////////////




        if(!isPotrait){

         //   vaccinationFragmentScheduleBinding.calendarView.setLeftArrowMask( null );
          //  vaccinationFragmentScheduleBinding.calendarView.setRightArrowMask( null );
          //  vaccinationFragmentScheduleBinding.calendarView.setTopbarVisible( false );


        }


        /*

        vaccinationFragmentScheduleBinding.calendarView.addDecorators(
                new CurrentDayDecorator(getActivity()),
                new HighlightWeekendsDecorator()
                new EventDecorator(getResources().getColor(R.color.colorAccent), calevents)

        );
        */


        addDecoratorsToCalendar(new CurrentDayDecorator(getActivity(),new Date()));
        addDecoratorsToCalendar(new WeekendsDecorator());

    }


    public void addDecoratorsToCalendar(DayViewDecorator dayViewDecorator){


        vaccinationFragmentScheduleBinding.calendarView.addDecorators(
               dayViewDecorator
        );


    }



    RecyclerView.LayoutManager mLayoutManager;
    public void setUpRecyclerView(){

        //setup the recycler view basic info
        SlideInLeftAnimator leftAnimator = new SlideInLeftAnimator(new OvershootInterpolator(1f));
        SlideInUpAnimator upAnimator = new SlideInUpAnimator(new OvershootInterpolator(1f));


        vaccinationFragmentScheduleBinding.recyclerviewBasicinfo.setHasFixedSize(true); //enhance recycler view scroll
        vaccinationFragmentScheduleBinding.recyclerviewBasicinfo.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset_schedule);
        vaccinationFragmentScheduleBinding.recyclerviewBasicinfo.addItemDecoration(itemDecoration);
        //  RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL, false);



        if(isTablet){

          //  if(!isPotrait){

              //  mLayoutManager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL, false);
                mLayoutManager  = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                vaccinationFragmentScheduleBinding.recyclerviewBasicinfo.setItemAnimator(upAnimator);
                vaccinationFragmentScheduleBinding.recyclerviewBasicinfo.getItemAnimator().setAddDuration(1000);
                //setting animation on scrolling


           // }


        }else {

           // if(!isPotrait){
              //  mLayoutManager  = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

                mLayoutManager  = new LinearLayoutManager(getContext());
                vaccinationFragmentScheduleBinding.recyclerviewBasicinfo.setItemAnimator(leftAnimator);
            vaccinationFragmentScheduleBinding.recyclerviewBasicinfo.getItemAnimator().setAddDuration(1000);
            //setting animation on scrolling


            // }


        }

        vaccinationFragmentScheduleBinding.recyclerviewBasicinfo.setLayoutManager(mLayoutManager);


    }




    /**
     * Created by Navruz on 17.06.2016.
     */



    private static final DateFormat FORMATTER = SimpleDateFormat.getDateInstance();


    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {


//            vaccinationFragmentScheduleBinding.progressbarEvents.setProgress(1000);
        // textView.setText(selected ? FORMATTER.format(date.getDate()) : "No Selection");
        // Toast.makeText(getContext(), FORMATTER.format(date.getDate()), Toast.LENGTH_SHORT).show();



       // vaccinationFragmentScheduleBinding.tvTitleCalendar.setTypeface(MyFonts.getBoldTypeFace(getContext()));
        vaccinationFragmentScheduleBinding.tvSubtitleCalendar.setTypeface(MyFonts.getTypeFace(getContext()));

        vaccinationFragmentScheduleBinding.tvTitleCalendar.setText(FORMATTER.format(date.getDate()));
        String noEvents = "No Events";
        vaccinationFragmentScheduleBinding.tvErrorNoEvents.setText(noEvents);


        String dayofweek = DateCalendarConverter.dateToDayOfWeek(date.getDate());

        vaccinationFragmentScheduleBinding.tvSubtitleCalendar.setText(dayofweek);



        //Get a string of the selected date////////////////////////////////////
        String mcv_date =  DateCalendarConverter.dateToStringWithoutTime(date.getDate());
        Date searchDateItem = DateCalendarConverter.stringToDateWithoutTime(mcv_date);

        String dateSelected = FORMATTER.format(date.getDate());

        /////////////////////////////////////////////// //////////////////

        setEventsOfTheDay(searchDateItem);




    }

    @Override
    public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {

        //noinspection ConstantConditions
        //getSupportActionBar().setTitle(FORMATTER.format(date.getDate()));

    }




    private void setEventsOfTheDay(Date dateSelectedSearchItem){


        List<VaccinationEvent> evenDayVaccinations = getVaccinationEventsByDate(dateSelectedSearchItem,observedVaccinationEvents);

        if(!evenDayVaccinations.isEmpty()) {

            vaccinationFragmentScheduleBinding.progressbarEvents.setVisibility(View.INVISIBLE);
            vaccinationFragmentScheduleBinding.tvErrorNoEvents.setVisibility(View.INVISIBLE);
            vaccinationFragmentScheduleBinding.recyclerviewBasicinfo.setVisibility(View.VISIBLE);


            updateEventDay(evenDayVaccinations);

        }else{


            //vaccinationFragmentScheduleBinding.progressbarEvents.setVisibility(View.INVISIBLE);
            vaccinationFragmentScheduleBinding.tvErrorNoEvents.setVisibility(View.VISIBLE);
            vaccinationFragmentScheduleBinding.recyclerviewBasicinfo.setVisibility(View.INVISIBLE);

        }





    }

    private List<VaccinationEvent> getVaccinationEventsByDate(Date searchDateItem, List<VaccinationEvent> vaccinationEvents){

        List<VaccinationEvent> evenDayVacs = new ArrayList<>();
        evenDayVacs.clear();

        for(VaccinationEvent vaccinationEvent: vaccinationEvents){
            // Log.i("FRAGMENT ", "Vaccination Event: "+vaccinationEvents.get(0).getScheduledDate()+" evenDay: "+searchDateItem);

            Date dateScheduled = DateCalendarConverter.dateWithTimeToDateWithoutTime(new Date(vaccinationEvent.getScheduledDate()));

            if(dateScheduled.equals(searchDateItem)){

                Log.d("VCSF:","Date1:"+vaccinationEvent.getScheduledDate()+" Date2:"+searchDateItem);

                evenDayVacs.add(vaccinationEvent);

            }

        }

        return evenDayVacs;

    }



    AdapterVaccinationsCalendarSchedule adapterVaccinationEvents;

    public void updateEventDay(List<VaccinationEvent> vaccinationEvents){

        CardHeader cardHeader = new CardHeader();
       // cardHeader.setTitle(dateSelected);
       // cardHeader.setSubTitle(dayofweek);

        adapterVaccinationEvents = AdapterVaccinationsCalendarSchedule.getINSTANCE(cardHeader,vaccinationEvents, getActivity().getApplication(), Injection.provideRecordVaccinationUseCase(getActivity().getApplication()));
        if(adapterVaccinationEvents!=null){
            AdapterVaccinationsCalendarSchedule.clearInstance();
        }
        adapterVaccinationEvents = AdapterVaccinationsCalendarSchedule.getINSTANCE(cardHeader,vaccinationEvents, getActivity().getApplication(), Injection.provideRecordVaccinationUseCase(getActivity().getApplication()));
        adapterVaccinationEvents.setClickListner(new AdapterCalendarVaccinationListner());



        // adapterChildren.replaceItems(careTakers);
       //  adapterVaccinationEvents.setHasStableIds(true);
        adapterVaccinationEvents.notifyItemInserted(0);
        vaccinationFragmentScheduleBinding.recyclerviewBasicinfo.setAdapter(adapterVaccinationEvents);

    }




    ArrayList<VaccinationEvent> observedVaccinationEvents = new ArrayList<>();


    public  void observeChildScheduleToDecorateCalendar(){

        Observer<List<VaccinationEvent>> vcardsObserver = new Observer<List<VaccinationEvent>>() {
            @Override
            public void onChanged(@Nullable List<VaccinationEvent> vaccinationEvents) {

                if(vaccinationEvents != null) {

                        for (VaccinationEvent vevent : vaccinationEvents) {

                            Date dateScheduled =  DateCalendarConverter.dateWithTimeToDateWithoutTime(new Date(vevent.getScheduledDate()));

                            CalendarDay calendarDay = CalendarDay.from(DateCalendarConverter.dateToCalendar(dateScheduled));
                            calevents.add(calendarDay);


                        }

                        addDecoratorsToCalendar(new EventsDecorator(getResources().getColor(R.color.colorAccent), calevents));

                        observedVaccinationEvents.addAll(vaccinationEvents);


                }
            }
        };

        viewScheduleFragmentViewModel.getVaccinationEventsOfGivenDate().observe(this, vcardsObserver);


    }


    public class AdapterCalendarVaccinationListner implements AdapterVaccinationsCalendarSchedule.OnRecordVaccinationClicked{
        @Override
        public void onRecordVaccination(View view, int position, VaccinationEvent vaccinationEvent) {


            Bundle bundle = new Bundle();
            bundle.putString("vevent", MyDetailsTypeConverter.fromVaccinationEventToString(vaccinationEvent));
            bundle.putInt("position",position);


            Intent intent = new Intent(getContext(),ComfirmVaccinationActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);

        }
    }





}
