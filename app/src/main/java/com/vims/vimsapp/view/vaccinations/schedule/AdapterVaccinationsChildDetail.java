package com.vims.vimsapp.view.vaccinations.schedule;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.entities.vaccinations.VaccinationStatus;
import com.vims.vimsapp.recordvaccinations.RecordChildVaccination;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.ThemeManager;
import com.vims.vimsapp.utilities.calendar.MyCalendarDate;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.vaccinations.CardHeader;

import java.util.Date;
import java.util.List;

import vimsapp.R;

public class AdapterVaccinationsChildDetail extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    //the adapter needs its viewmodel

    private static volatile AdapterVaccinationsChildDetail INSTANCE;

    private List<VaccinationEvent> vaccinationEvents;
    private RecordChildVaccination recordChildVaccination;


    private Application context;
    private Activity activity;
    private CardHeader cardHeader;



    public AdapterVaccinationsChildDetail(CardHeader header , List<VaccinationEvent> vaccinationEvents, Application context, RecordChildVaccination recordChildVaccination) {
        this.context = context;
        this.cardHeader = header;
        this.vaccinationEvents = vaccinationEvents;
        this.recordChildVaccination = recordChildVaccination;
        //1. Animations bug
        setHasStableIds(true); //in constructor

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        ThemeManager.setContext(context);

    }


    /*
    public static AdapterVaccinationsChildDetail getINSTANCE(CardHeader cardHeader, List<VaccinationEvent> vaccinationEvents, Application context, RecordChildVaccination recordChildVaccination) {
        if(INSTANCE == null){

            INSTANCE = new AdapterVaccinationsChildDetail(cardHeader, vaccinationEvents, context, recordChildVaccination);
        }
        return INSTANCE;
    }
    */

    public void replaceItems(List<VaccinationEvent> vaccinationEvents){
        this.vaccinationEvents = vaccinationEvents;

        //1. Animations bug
//        setHasStableIds(true); //in constructor

    }

    public Context getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }


    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if(viewType == TYPE_HEADER) {
            View   v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vaccinations_card_header, parent, false);

            return  new VHHeader(v);


        }else if(viewType == TYPE_ITEM){


            View   v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vaccinations_card_horizontal_timeline, parent, false);

            return new MyViewHolder(v);


        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");


    }


    private class VHHeader extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView txtTitle;
        TextView txtSubTitle;

        public VHHeader(View itemView) {
            super(itemView);
            this.txtTitle = itemView.findViewById(R.id.tvtitleEvents);
            this.txtSubTitle =  itemView.findViewById(R.id.tvSubtitleEvents);

            txtTitle.setTypeface(MyFonts.getTypeFace(context));
            txtSubTitle.setTypeface(MyFonts.getTypeFace(context));

        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition(); // gets item position

            // We can access the data within the views
            //  Toast.makeText(mContext, "you clicked me", Toast.LENGTH_SHORT).show();
            //  Intent intent = new Intent(v.getContext(), LoginActivity.class);
            // v.getContext().startActivity(intent);

            //  myBtnclickListener.onSignUpBtnClick(v, position); // call the onClick in the OnCardItemClickListener Interface i created
         //   dismissClickListner.onBtnDismiss(v,position);
           // onHeaderClickListener.onHeaderClick(v,position);


        }




    }



    //(2) implements the click listner
    public class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {
        public TextView title;
        public TextView subtitle;

        private ConstraintLayout vaccineDescription;
        private TextView vaccineDescProtects;
        private TextView vaccineDescMode;
        private TextView vaccineDescSite;

        private TextView vaccineDescProtectsLabel;
        private TextView vaccineDescModeLabel;
        private TextView vaccineDescSiteLabel;


        public TextView daystoGoLabel;

        public TextView supposed_vaccination_date;

        public TextView tvEventDay;
        public TextView tvEventMonth;
        public TextView tvEventYear;
        public ImageView ivDropDown;

        public CardView cardViewItem;
        public ImageView ivTimer;
        public ImageView ivTimerDone;
        public ImageView imageTransit;



       // public ConstraintLayout calendarLayout;
       public AppCompatImageView calendarLayout;



        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.tv_vaccination_name);
            subtitle = view.findViewById(R.id.tv_child_name);


            vaccineDescription = itemView.findViewById(R.id.vaccineDescription);
            vaccineDescSite = itemView.findViewById(R.id.vaccineSite);
            vaccineDescMode = itemView.findViewById(R.id.vaccineMode);
            vaccineDescProtects = itemView.findViewById(R.id.vaccineProtects);


            vaccineDescSiteLabel = itemView.findViewById(R.id.vaccineSiteLabel);
            vaccineDescModeLabel = itemView.findViewById(R.id.vaccineModeLabel);
            vaccineDescProtectsLabel = itemView.findViewById(R.id.vaccineProtectsLabel);




            supposed_vaccination_date = view.findViewById(R.id.tv_time);

            ///daystoGoLabel = view.findViewById(R.id.dateLable);

            tvEventDay = view.findViewById(R.id.tv_vaccination_day);
            tvEventMonth = view.findViewById(R.id.tv_vaccination_month);
            tvEventYear = view.findViewById(R.id.tv_vaccination_year);

            ivDropDown = view.findViewById(R.id.iv_drop_down);
            ivTimer = view.findViewById(R.id.iv_icon_location);
            ivTimerDone = view.findViewById(R.id.iv_timer_done);
            imageTransit = view.findViewById(R.id.imageTransit);

            cardViewItem = view.findViewById(R.id.card_view);
            calendarLayout = view.findViewById(R.id.calendarDayView);



            ///FACE
            title.setTypeface(MyFonts.getTypeFace(context));
            subtitle.setTypeface(MyFonts.getTypeFace(context));

            vaccineDescSite.setTypeface(MyFonts.getTypeFace(context));
            vaccineDescMode.setTypeface(MyFonts.getTypeFace(context));
            vaccineDescProtects.setTypeface(MyFonts.getTypeFace(context));

            vaccineDescSiteLabel.setTypeface(MyFonts.getTypeFace(context));
            vaccineDescModeLabel.setTypeface(MyFonts.getTypeFace(context));
            vaccineDescProtectsLabel.setTypeface(MyFonts.getTypeFace(context));



            supposed_vaccination_date.setTypeface(MyFonts.getTypeFace(context));

            tvEventMonth.setTypeface(MyFonts.getTypeFace(context));
            tvEventDay.setTypeface(MyFonts.getTypeFace(context));

            //DRAWBLES
            if(AppCompatDelegate.isCompatVectorFromResourcesEnabled()) {
                // add_vaccination_checkbox.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.ic_add_vaccination_darktheme), null, null, null);
            }


            /*
            Bitmap myBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.baby_boy);
            RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(context.getResources(), myBitmap);
            drawable.setCircular(true);
            */
            // ivProfile.setImageDrawable(drawable);
            //(3) Bind the click listener
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition(); // gets item position

            VaccinationEvent vaccinationEvent = vaccinationEvents.get(position);


           //myclickListener.onCaretakerClicked(v, position, vaccinationEvent); // call the onClick in the OnCardItemClickListener Interface i created

        }

    }


    int mExpandedPosition = -1;
    int previousExpandedPosition = -1;


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder general_holder, int position) {
        final VaccinationEvent vaccinationEvent = getVaccinationEvent(position);


        if(general_holder instanceof VHHeader)
        {

            VHHeader VHheader = (VHHeader)general_holder;

            VHheader.txtTitle.setText(cardHeader.getTitle());
            VHheader.txtSubTitle.setText(cardHeader.getSubTitle());

        }
        else if(general_holder instanceof MyViewHolder ) {

             MyViewHolder holder = (MyViewHolder ) general_holder;
             String m = "";
             String d = "";
             String y = "";

            String vDate = DateCalendarConverter.dateToStringWithoutTime(new Date(vaccinationEvent.getScheduledDate()));
             if(!TextUtils.isEmpty(vDate)){
                String[] dateArray = vDate.split(" ");
                m = dateArray[0];
                d = dateArray[1];
                y = dateArray[2];
                if(m.length()> 3){
                    m =  m.substring(0,3);
                }
                y = y.substring(2,4);
             }

            String eventTitle = "record "+vaccinationEvent.getVaccineName();


            holder.title.setText(eventTitle);
            holder.subtitle.setText(vaccinationEvent.getChildName());
            holder.tvEventDay.setText(d);
            holder.tvEventMonth.setText(m);
            holder.tvEventYear.setText(y);


            holder.vaccineDescProtects.setText(VaccineSpecHandler.getVaccineDesc(VaccineSpecHandler.SPEC_TYPE_PROTECTS,vaccinationEvent.getVaccineName()));
            holder.vaccineDescMode.setText(VaccineSpecHandler.getVaccineDesc(VaccineSpecHandler.SPEC_TYPE_MODE,vaccinationEvent.getVaccineName()));
            holder.vaccineDescSite.setText(VaccineSpecHandler.getVaccineDesc(VaccineSpecHandler.SPEC_TYPE_SITE,vaccinationEvent.getVaccineName()));



            if(vaccinationEvent.getVaccinationStatus() == VaccinationStatus.NOT_DONE){
                Date dateScheduled  = new Date(vaccinationEvent.getScheduledDate());
                boolean isEventMissed = vaccinationEvent.getVaccinationStatus() == VaccinationStatus.NOT_DONE && dateScheduled.before(new Date());
                long daysToGo =  DateCalendarConverter.calculateDifferenceInDates(dateScheduled, MyCalendarDate.getCurrentCalendarDate());

                //MISSED
                if(daysToGo < 0){

                    ///////////// calculated days overdue  ///////////////////////
                    String missedDays[] = Long.toString(daysToGo).split("-");
                    String da = Long.toString(daysToGo);
                    String  missedNo = missedDays[1]+"d overdue";
                    holder.supposed_vaccination_date.setText(missedNo);
                    holder.supposed_vaccination_date.setTextColor(context.getResources().getColor(R.color.errorColorDarkThemeLight));

                   // holder.tvEventDay.setTextColor(context.getResources().getColor(R.color.errorColorDarkThemeLight));
                   // holder.tvEventMonth.setTextColor(context.getResources().getColor(R.color.errorColorDarkThemeLight));

                     /////- holder.linearLayout.setBackground(context.getResources().getDrawable(ThemeManager.getBackgroundDrawableUpcoming()));
                    ////////////////////////////////////////////////////////////
                    holder.calendarLayout.setBackground(ThemeManager.getBackgroundDrawable(VaccinationStatus.FAILED));
                    holder.imageTransit.setBackground(ThemeManager.getBackgroundDrawableEvents(VaccinationStatus.FAILED));



                    // UPCOMING
                }else{



                    String  daysToGoToString = Long.toString(daysToGo)+"d";
                    holder.supposed_vaccination_date.setText(daysToGoToString);
                    ///////////// calculated days due  ///////////////////////
                    boolean isEventDayReached =  daysToGo == 0;
                    if(isEventDayReached){

                        daysToGoToString = "Due Today";
                        holder.supposed_vaccination_date.setTextColor(context.getResources().getColor(R.color.brand_green_300));
                        holder.supposed_vaccination_date.setText(daysToGoToString);

                       // holder.tvEventDay.setTextColor(context.getResources().getColor(R.color.brand_green_30));
                       // holder.tvEventMonth.setTextColor(context.getResources().getColor(R.color.brand_green_30));
                        holder.calendarLayout.setBackground(ThemeManager.getBackgroundDrawable(VaccinationStatus.DUE));
                        holder.imageTransit.setBackground(ThemeManager.getBackgroundDrawableEvents(VaccinationStatus.DUE));


                    }else {

                        ////////////////////////////////////////////////////////////
                        holder.supposed_vaccination_date.setTextColor(context.getResources().getColor(R.color.blue_grey_500));
                       // holder.title.setTextColor(context.getResources().getColor(ThemeManager.getTextColorAdministerBtn()));
                       // holder.subtitle.setTextColor(context.getResources().getColor(ThemeManager.getTextColorAdministerBtn()));
                        holder.calendarLayout.setBackground(ThemeManager.getBackgroundDrawable());
                        holder.imageTransit.setBackground(ThemeManager.getBackgroundDrawableEvents(VaccinationStatus.NOT_DONE));

                       // holder.tvEventDay.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                        //holder.tvEventMonth.setTextColor(context.getResources().getColor(R.color.colorPrimary));

                        /////----- holder.linearLayout.setBackground(context.getResources().getDrawable(ThemeManager.getBackgroundDrawableUpcoming()));
                       // holder.cardViewItem.setEnabled(false);
                    }


                }


             //   holder.add_vaccination_checkbox.setChecked(false);

                // holder.add_vaccination.setTextColor(context.getResources().getColor(R.color.red));
            }else if(vaccinationEvent.getVaccinationStatus() == VaccinationStatus.DONE){

              //  holder.supposed_vaccination_date.setTextColor(context.getResources().getColor(R.color.navy_blue_300));

               // holder.cardViewItem.setEnabled(false);
                holder.ivTimer.setVisibility(View.INVISIBLE);
                holder.ivTimerDone.setVisibility(View.VISIBLE);

                holder.calendarLayout.setBackground(ThemeManager.getBackgroundDrawable(VaccinationStatus.DONE));
                holder.imageTransit.setBackground(ThemeManager.getBackgroundDrawableEvents(VaccinationStatus.DONE));

               // holder.administerButton.setCardElevation(elevation);
                //holder.title.setTextColor(context.getResources().getColor(R.color.navy_blue_200));

                holder.title.setTextColor(context.getResources().getColor(ThemeManager.getTextColorAdministerBtn()));
                holder.subtitle.setTextColor(context.getResources().getColor(ThemeManager.getTextColorAdministerBtn()));


                //holder.tvEventDay.setTextColor(context.getResources().getColor(R.color.navy_blue_40));
                //holder.tvEventMonth.setTextColor(context.getResources().getColor(R.color.navy_blue_40));


                //SPLIT VACCINATION DATE INTO MONTH , DAY,YEAR
                //String[] dateValuesMdy = vaccinationEvent.getScheduledDate().split(" ");
                String[] dateValuesMdy = vaccinationEvent.getVaccinationDate().split(" ");

                String month = dateValuesMdy[0];
                String day = dateValuesMdy[1];
                String year = dateValuesMdy[2];

                if(month.length()> 3){
                    month =  month.substring(0,3);
                }

                String vaccinationDate = ""+month+" "+day+" "+year;
                holder.supposed_vaccination_date.setText(vaccinationDate);
                /////-holder.linearLayout.setBackground(context.getResources().getDrawable(ThemeManager.getBackgroundDrawableDone()));

            }


            //solution 1
            boolean isExpanded = position == mExpandedPosition;

            holder.vaccineDescription.setVisibility(isExpanded?View.VISIBLE:View.GONE);
            holder.itemView.setActivated(isExpanded);
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);


            holder.ivDropDown.setImageDrawable(isExpanded?context.getResources().getDrawable(R.drawable.ic_expand_less_black_18dp):context.getResources().getDrawable(R.drawable.ic_expand_more_black_18dp));
            //  holder.ivDropDown.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_expand_more_black_24dp));
            //  holder.ivDropDown.setBackground(isExpanded?context.getResources().getDrawable(R.drawable.ic_expand_less_black_24dp):context.getResources().getDrawable(R.drawable.ic_expand_more_black_24dp));


            holder.ivDropDown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    //   onRecordVaccinationClicked.onCardExpandCLicked(v,position,mExpandedPosition,isExpanded);

                    mExpandedPosition = isExpanded ? -1:position;
                   // TransitionManager.beginDelayedTransition(holder.itemView);
                    notifyItemChanged(position);

                }
            });



            //String dateofvaccination =  DateCalendarConverter.dateToStringWithoutTime(vaccinationEvent.getScheduledDate());
            //Drawable img = getContext().getResources().getDrawable( R.drawable.ic_perm_identity_black_24dp);
            //caretakerAddNameFragmentBinding.tvFirstnameCaretaker.setCompoundDrawablesWithIntrinsicBounds( getResources().getDrawable( R.drawable.ic_perm_identity_black_24dp), null, null, null);


            holder.cardViewItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                 onRecordVaccinationClicked.onRecordVaccination(holder.calendarLayout,position,vaccinationEvent);

                }//end of onclick
            });



        }



        /*

        //ONCLICK LISTNER FOR SHARE BUTTON

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(mContext, "Sharing.. "+album.getTitle(), Toast.LENGTH_SHORT).show();

            }
        });
       */

    }


    @Override
    public int getItemCount() {

        return vaccinationEvents.size();
    }


    //2. Animations bug
    @Override
    public long getItemId(int position) {
        return vaccinationEvents.get(position).hashCode(); //Any unique id
    }



    private VaccinationEvent getVaccinationEvent(int position)
    {
        return vaccinationEvents.get(position);
    }

    //    need to override this method
    @Override
    public int getItemViewType(int position) {
       /*
        if(isPositionHeader(position))
            return TYPE_HEADER;
            */
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position)
    {
        return position == 0;
    }




    //interfaces
    private OnRecordVaccinationClicked onRecordVaccinationClicked;

    //someione who implements this interface ill send him a click trigger
    public interface OnRecordVaccinationClicked{

        void onRecordVaccination(View view, int position, VaccinationEvent vaccinationEvent);

    }

    public void setClickListner(OnRecordVaccinationClicked onRecordVaccinationClicked){
        this.onRecordVaccinationClicked = onRecordVaccinationClicked;
    }


    public void confirmVaccinationDialog(String vaccineName, String childName) {


       // Activity activity = (Activity) this.activity;

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.MyDialogTheme);

        String dialogTitle = "Confirm Recording Vaccination ?";
        String dialogDesc =  ""+vaccineName+" administered to "+childName +", the operation can not be undone";

        builder.setTitle(dialogTitle);
        builder.setMessage(dialogDesc);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {



                    }

                });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                    }
                });

        AlertDialog dialog = builder.create();


          //if using api >= 19 use TypeToast not type_sytem_alert

          //  dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);

            dialog.show();




    }

    /*
    int mStackLevel = 0;
    void showDialog() {
        mStackLevel++;

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        Activity activity = (Activity) this.activity;

        FragmentManager fragmentMgr = activity.getFragmentManager();
        FragmentTransaction ft = fragmentMgr.beginTransaction();
        Fragment prev = fragmentMgr.findFragmentByTag("dialog");



        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        ConfirmVaccinationDialogFrgment newFragment = ConfirmVaccinationDialogFrgment.newInstance(mStackLevel);
        newFragment.show(ft, "dialog");

    }
    */






}