package com.vims.vimsapp.view.patients.addchild;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.utilities.MainThreadExecutor;
import com.vims.vimsapp.utilities.ThemeManager;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.injection.ModuleUI;

import vimsapp.R;

import vimsapp.databinding.PatientsActivityAddChildBinding;

import static com.vims.vimsapp.utilities.ScreenManager.setScreenOrientation;


public class AddChildActivityWizard extends AppCompatActivity implements StepperLayout.StepperListener{


    // FragmentAddCaretakerBioViewModel addChildActivityWizardViewModel;
    //binding name with view
    PatientsActivityAddChildBinding addChildBinding;
    AddChildActivityWizardViewModel addChildActivityWizardViewModel;
    AddChildActivityWizardViewModelFactory wizardViewModelFactory;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addChildBinding =   DataBindingUtil.setContentView(this, R.layout.patients_activity_add_child);

       // wizardViewModelFactory = new AddChildActivityWizardViewModelFactory(Injection.provideRegisterChildUseCase(getApplication()), Injection.provideGenerateChildScheduleUseCase(getApplication()));

      //  addChildActivityWizardViewModel = ViewModelProviders.of(this, wizardViewModelFactory).get(AddChildActivityWizardViewModel.class);

        addChildActivityWizardViewModel = ModuleUI.provideInteractionViewModel(this);

        setScreenOrientation(this);

        setUpActionBar();

        observeSaving();


          addChildBinding.stepperLayoutChild.setNextButtonColor(getResources().getColor(R.color.colorPrimary));
          addChildBinding.stepperLayoutChild.setBackButtonColor(getResources().getColor(R.color.colorPrimary));
          addChildBinding.stepperLayoutChild.setCompleteButtonColor(getResources().getColor(R.color.colorPrimary));


        addChildBinding.stepperLayoutChild.setAdapter(new AdapterChildStepper(getSupportFragmentManager(), this));

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){

            String careId = bundle.getString("caretakerId");
            addChildActivityWizardViewModel.setmCaretakerId(careId);
        }

    }

    Toolbar toolbar;

    private void setUpActionBar(){

        toolbar = addChildBinding.toolbar;
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Add Child");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        }
        // setActionBar(toolbar);
        //we replace our actionbar with our toolbar
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);


    }







    public void observeSaving(){


        Observer<Boolean> childObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean child) {


                addChildBinding.stepperLayoutChild.setTabNavigationEnabled(false);

                addChildBinding.stepperLayoutChild.setBackButtonEnabled(false);
                addChildBinding.stepperLayoutChild.setCompleteButtonEnabled(false);

                addChildBinding.stepperLayoutChild.setBackButtonColor(getResources().getColor(ThemeManager.getWizardButtonColor()));
                addChildBinding.stepperLayoutChild.setCompleteButtonColor(getResources().getColor(ThemeManager.getWizardButtonColor()));

                new MainThreadExecutor(1500).execute(new Runnable() {
                    @Override
                    public void run() {

                        finish();
                    }
                });


            }
        };

        addChildActivityWizardViewModel.getmScheduleGenerateResponse().observe(this, childObserver);




    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }




    @Override
    public void onCompleted(View completeButton) {




    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {

    }

    @Override
    public void onReturn() {
        finish();
    }
}
