package com.vims.vimsapp.view.patients.addcaretaker;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import com.vims.vimsapp.entities.child.Caretaker;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.view.MyFonts;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.patients.addchild.AddChildActivityWizard;
import com.vims.vimsapp.view.patients.addchild.AddChildWelcomeActivity;
import com.vims.vimsapp.view.patients.detail.MyDetailsTypeConverter;
import com.vims.vimsapp.view.patients.detail.ViewCaretakerDetailsActivity;

import vimsapp.R;
import vimsapp.databinding.PatientsCaretakerAddPreviewFragmentBinding;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class AddCaretakerReviewFragment extends Fragment implements BlockingStep {

    CaretakerActivityWizardViewModel caretakerActivityWizardViewModel;
    CaretakerActivityViewModelFactory activityViewModelFactory;

    PatientsCaretakerAddPreviewFragmentBinding reviewBinding;

    PreferenceHandler preferenceHandler;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        reviewBinding  = DataBindingUtil.inflate(inflater, R.layout.patients_caretaker_add_preview_fragment, container, false);

        //setSupportActionBar(toolbar);
        //itemView =  inflater.inflate(R.layout.patients_activity_add_caretaker, container, false);

        activityViewModelFactory = new CaretakerActivityViewModelFactory(Injection.provideRegisterCaretakerUseCase(getActivity().getApplication()));
        caretakerActivityWizardViewModel =  ViewModelProviders.of(getActivity(), activityViewModelFactory).get(CaretakerActivityWizardViewModel.class);
        //setUpActionBar();

        preferenceHandler = PreferenceHandler.getInstance(getContext());

        //initialize your UI
        observeNames();
        observeRole();
        observeBirthDate();
        observePhone();
        observeAddress();
        observeProfileImageBitmap();

       // animate();


        observeSaving();



        boolean isTablet = getResources().getBoolean(R.bool.tablet);
        boolean isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);



        if(isTablet) {
            CardView.LayoutParams layoutParams = (CardView.LayoutParams) reviewBinding.cardView.getLayoutParams();

            if(isPotrait) {


                layoutParams.setMargins(80,8,80,8);

                //  caretakerAddRoleFragmentBinding.cardView.setLayoutParams(layoutParams);
            }else{

                layoutParams.setMargins(150,8,150,8);


            }
        }


        return reviewBinding.getRoot();
    }






    /*

    private void animate(){

            LottieAnimationView animationView;

            animationView =  reviewBinding.animationView;


            ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f);

            //  animationView.setProgress(0);
            animationView.pauseAnimation();
            // animationView.playAnimation();

            //
            //animationView.addAnimatorUpdateListener((animation) -> {
            // Do something.
            //  animationView.setProgress(animation.getAnimatedValue());

            // });
            //

            animator.addUpdateListener(animation -> {
                animationView.setProgress(animation.getAnimatedFraction());
            });
            animator.start();


            // vaccinationActivityBinding.animationView.cancelAnimation();

            if (animationView.isAnimating()) {
                // Do something.
            }





    }

   */



    public void observeProfileImageBitmap(){

        Observer<Bitmap> profileObserver = new Observer<Bitmap>() {
            @Override
            public void onChanged(Bitmap bitmap) {
              //  Resources res = getContext().getResources();
              //  int id = R.drawable.icon_doctor_female;
               //// Bitmap bitmap = BitmapFactory.decodeResource(res,id);
                if (bitmap!=null) {
                    reviewBinding.ivCaretakerAvatorPreview.setImageBitmap(bitmap);
                }
            }
        };

        caretakerActivityWizardViewModel.getProfileImageBitmap().observe(this,profileObserver);

    }

    private void registerChildPrompt(String caretaker){

       Caretaker my_caretaker =  MyDetailsTypeConverter.fromStringToCaretaker(caretaker);

        reviewBinding.addChildNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().finish();


                Intent intent = new Intent(getContext(),AddChildActivityWizard.class);
                intent.putExtra("caretakerId", my_caretaker.getCaretakerId());
                startActivity(intent);


            }
        });

        reviewBinding.addChildNotNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().finish();


                Bundle bundle = new Bundle();
                bundle.putString("caretaker", caretaker);

                // if(isPotrait){
                // Intent for the activity to open when user selects the notification
                Intent detailsIntent = new Intent(getContext(), ViewCaretakerDetailsActivity.class);
               // detailsIntent.setAction(Intent.ACTION_VIEW);
                detailsIntent.putExtras(bundle);

                startActivity(detailsIntent);



            }
        });

    }

    public void observeSaving(){

        Observer<String> responseObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                 //Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();
                 Caretaker caretaker =  MyDetailsTypeConverter.fromStringToCaretaker(s);
                 String fullname = caretaker.getFirstName()+" "+caretaker.getLastName();

                 String message = " Registered successfully";
                 reviewBinding.tvPreviewCaretakerFirstname.setText(fullname);
                 reviewBinding.tvPreviewCaretakerFirstname.setTextColor(getResources().getColor(R.color.colorPrimaryTint3));

                 reviewBinding.textView13.setVisibility(VISIBLE);
                 reviewBinding.textView13.setTextColor(getResources().getColor(R.color.colorPrimary));

                 reviewBinding.textView13.setText(message);
                 reviewBinding.tvPreviewCaretakerAddress.setVisibility(View.GONE);

                 reviewBinding.ivCaretakerAvatorPreview.setImageDrawable(getResources().getDrawable(R.drawable.ic_animation_done));


                 reviewBinding.layoutCaretakerTitle.setVisibility(VISIBLE);
                 reviewBinding.registrationProgress.setVisibility(View.GONE);
                 reviewBinding.layoutCaretakerTitle.setBackground(getResources().getDrawable(R.drawable.shape_borderline_default_cp_review));

                // 0703000478
                // Gods Love Records
                // reviewBinding.registrationProgress.setVisibility(INVISIBLE);
                // reviewBinding.ivCaretakerAvatorPreview.setBackground(getResources().getDrawable(R.drawable.ic_animation_done));

                 reviewBinding.caretakerDetailInfo.setVisibility(INVISIBLE);
                 reviewBinding.addChildNotNow.setVisibility(VISIBLE);
                 reviewBinding.addChildNow.setVisibility(VISIBLE);

                 //reviewBinding.tvPreviewCaretakerFirstname.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
                 //reviewBinding.textView15.setTypeface(MyFonts.getBoldTypeFace(getContext()));

                 //To mark current caretaker reg to use when deciding to refresh list/not
                 preferenceHandler.setIntValue(PreferenceHandler.IS_CARETAKER_REGISTERED_INT,1);



                 registerChildPrompt(s);

            }};

        caretakerActivityWizardViewModel.getResponse().observe(this, responseObserver);


    }

    public void observeNames(){

        Observer<CharSequence> fnameObserver = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence s) {

                String names = ""+s;

                reviewBinding.tvPreviewCaretakerFirstname.setText(names);
                reviewBinding.tvPreviewCaretakerFirstname.setTypeface(MyFonts.getTypeFace(getContext()));

            }};

        caretakerActivityWizardViewModel.getFullName().observe(this,fnameObserver);

    }

    public void observeRole(){

        Observer<CharSequence> fnameObserver = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence s) {

                String role = ""+s;
                reviewBinding.tvPreviewCaretakerRole.setText(role);
                reviewBinding.tvPreviewCaretakerRole.setTypeface(MyFonts.getTypeFace(getContext()));

            }};

        caretakerActivityWizardViewModel.getlRole().observe(this,fnameObserver);

    }

    private void observeBirthDate(){

        Observer<String> ageObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
               // reviewBinding.tvPreviewCaretakerBirthdate.setText(s);
               // reviewBinding.tvPreviewCaretakerBirthdate.setTypeface(MyFonts.getTypeFace(getContext()));

            }};

        caretakerActivityWizardViewModel.getlBirthDate().observe(this,ageObserver);

    }

    public void observePhone(){

        Observer<CharSequence> phone1Observer = new Observer<CharSequence>() {
            @Override
            public void onChanged(@Nullable CharSequence s) {
                reviewBinding.tvPreviewCaretakerPhone1.setText(s);
                reviewBinding.tvPreviewCaretakerPhone1.setTypeface(MyFonts.getTypeFace(getContext()));



            }};

        caretakerActivityWizardViewModel.getlPhone1().observe(this,phone1Observer);

    }

    public void observeAddress(){

        Observer<CharSequence> addressObserver = new Observer<CharSequence>() {
            @Override
            public void onChanged(CharSequence s) {


                    reviewBinding.tvPreviewCaretakerAddress.setText(s);
                    reviewBinding.tvPreviewCaretakerAddress.setTypeface(MyFonts.getTypeFace(getContext()));


            }};

        caretakerActivityWizardViewModel.getFullAddress().observe(this,addressObserver);

    }



    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {



    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

        reviewBinding.registrationProgress.setVisibility(VISIBLE);
        reviewBinding.layoutCaretakerTitle.setVisibility(INVISIBLE);
        String hid =   PreferenceHandler.getInstance(getContext()).getPref(PreferenceHandler.USER_HOSPITAL_ID);
        caretakerActivityWizardViewModel.addCaretaker(hid);


    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {

        callback.goToPrevStep();


    }


    @Override
    public VerificationError verifyStep() {
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        return null;
    }

    @Override
    public void onSelected() {
        //update UI when selected
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        //handle error inside of the fragment, e.g. show error on EditText
    }
}
