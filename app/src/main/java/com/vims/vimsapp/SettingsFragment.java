package com.vims.vimsapp;


import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;


import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.preference.PreferenceScreen;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

//import com.vansuita.materialabout.builder.AboutBuilder;
//import com.vansuita.materialabout.views.AboutView;
import com.vims.vimsapp.view.users.LoginActivity;
import com.vims.vimsapp.view.users.SQLiteHandler;
import com.vims.vimsapp.view.users.SessionManager;

import vimsapp.R;
/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener{


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //register the listner
        log();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
     //   setSharePreferences();
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

        addPreferencesFromResource(R.xml.pref_settings);
        //bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_sync)));
       // bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_theme)));
       // setSharePreferences();


//         setSummary();



        Preference myPref = (Preference) findPreference(getResources().getString(R.string.pref_key_libraries));
        myPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                //open browser or intent here

                //Toast.makeText(getContext(),"libs loaded",Toast.LENGTH_LONG).show();
                displayLicensesAlertDialog();


                return true;
            }
        });



        Preference creditsPref = (Preference) findPreference(getResources().getString(R.string.pref_key_credits));
        creditsPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                //open browser or intent here

                //Toast.makeText(getContext(),"libs loaded",Toast.LENGTH_LONG).show();
                displayCreditsAlertDialog();


                return true;
            }
        });


        Preference myPrefLogout = (Preference) findPreference(getResources().getString(R.string.pref_key_logout));
        myPrefLogout.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                //open browser or intent here

                //Toast.makeText(getContext(),"libs loaded",Toast.LENGTH_LONG).show();
                logoutUser();

                return true;
            }
        });


    }


    private SQLiteHandler db;
    private SessionManager session;

    private void log(){
        // SqLite database handler
        db = new SQLiteHandler(getContext());

        // session manager
        session = new SessionManager(getContext());

    }

    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }




    private void displayLicensesAlertDialog() {
        WebView view = (WebView) LayoutInflater.from(getContext()).inflate(R.layout.licence_dialog, null);
        view.loadUrl("file:///android_asset/open_source_licenses.html");
       AlertDialog mAlertDialog = new AlertDialog.Builder(getContext(), R.style.Theme_AppCompat_Light_Dialog_Alert)
                .setTitle(getString(R.string.pref_title_libraries))
                .setView(view)
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }




    private void displayCreditsAlertDialog() {
        WebView view = (WebView) LayoutInflater.from(getContext()).inflate(R.layout.licence_dialog, null);
        view.loadUrl("file:///android_asset/credits.html");
        AlertDialog mAlertDialog = new AlertDialog.Builder(getContext(), R.style.Theme_AppCompat_Light_Dialog_Alert)
                .setTitle(getString(R.string.pref_title_credits))
                .setView(view)
                .setPositiveButton(android.R.string.ok, null)
                .show();

    }

    /*
    private void displayAboutView(){

        AboutView view = AboutBuilder.with(getActivity())
                .setPhoto(R.mipmap.profile_picture)
                .setCover(R.mipmap.profile_cover)
                .setName("Your Full Name")
                .setSubTitle("Mobile Developer")
                .setBrief("I'm warmed of mobile technologies. Ideas maker, curious and nature lover.")
                .setAppIcon(R.mipmap.ic_launcher)
                .setAppName(R.string.app_name)
                .addGooglePlayStoreLink("8002078663318221363")
                .addGitHubLink("user")
                .addFacebookLink("user")
                .addFiveStarsAction()
                .setVersionNameAsAppSubTitle()
                .addShareAction(R.string.app_name)
                .setWrapScrollView(true)
                .setLinksAnimated(true)
                .setShowAsCard(true)
                .build();
    }
       */


    private String getCategoryKey(String prefKey) {

        String[] catKeys = {getResources().getString(R.string.pref_key_category_general),getResources().getString(R.string.pref_key_category_about)};

        PreferenceCategory curCat;
        Preference test;
        for(String catKey : catKeys)
        {
            curCat = (PreferenceCategory)findPreference(catKey);
            if(curCat == null)
                continue;
            test = curCat.findPreference(prefKey);
            if(test != null)
                return catKey;
        }
        return null;
    }

    private void setSharePreferences(){
        Log.d("PreferenceSummary","started");
        //Instance to share preferences
        String[] catKeys = {getResources().getString(R.string.pref_key_category_general),getResources().getString(R.string.pref_key_category_about)};


        SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();
        //to get the total no of preferences to get the preference we want
        PreferenceScreen preferenceScreen = getPreferenceScreen();
        int count = preferenceScreen.getPreferenceCount();

        PreferenceCategory category = (PreferenceCategory)findPreference(catKeys[0]);



        for(int i=0; i<count; i++){
            Preference preference = preferenceScreen.getPreference(i);
            if (!(preference instanceof CheckBoxPreference)) {

                //if its not checkbox (coz checkbox auto fills in)
                String value = sharedPreferences.getString(preference.getKey(),"");
                Log.d("PreferenceSummary",""+value);
                setPreferenceSummary(preference,value);
            }

        }

    }


    private void setProfileSummary(){

        Preference preference  = findPreference(getResources().getString(R.string.pref_key_version));

        if(null != preference) {
            if (!(preference instanceof CheckBoxPreference)) {

               // String value = sharedPreferences.getString(preference.getKey(), "");
               // setPreferenceSummary(preference, "4.6");
                preference.setSummary("0.5");

            }
        }

    }





    private void setSharePreferences1(){
        Log.d("PreferenceSummary","started");
        //Instance to share preferences
        String[] catKeys = {getResources().getString(R.string.pref_key_category_general),getResources().getString(R.string.pref_key_category_about)};


        SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();
        //to get the total no of preferences to get the preference we want
        PreferenceScreen preferenceScreen = getPreferenceScreen();
        int count = preferenceScreen.getPreferenceCount();

      //  PreferenceCategory category = (PreferenceCategory)findPreference(catKeys[0]);

        for(int i=0; i<count; i++){
            Preference preference = preferenceScreen.getPreference(i);
            if (!(preference instanceof CheckBoxPreference)) {

                //if its not checkbox (coz checkbox auto fills in)
                String value = sharedPreferences.getString(getString(R.string.pref_key_version),"2.5");
                Log.d("PreferenceSummary",""+value);

                preference.setSummary(value);

               // setPreferenceSummaryUser(preference,value);
            }

        }

    }






    private void setPreferenceSummary(Preference preference, String value){
        //To set correct preference summary
        //check if the preference  is the instance of the List Preference
        //If it is cast it and get the index of that list preference
        //By using findIndexOf value then check if index is valid
        //PreferenceCategory cannot be cast to android.support.v7.preference.ListPreference

        // preference comes as a prerefence category
        if (!(preference instanceof CheckBoxPreference)) {
            // For list preferences, look up the correct display value in
            // the preference's 'entries' list.
            ListPreference listPreference = (ListPreference) preference;
            int index = listPreference.findIndexOfValue(value);

            if(index>=0){
                preference.setSummary(listPreference.getEntries()[index]);
            }
            // if valid look through the entries array and get entry label at that exact index
            //preference.setSummary(index >= 0 ? listPreference.getEntries()[index] : null);
        }

        }



    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        //used to update a new preference label

      Preference preference  = findPreference(key);

      if(null != preference) {
          if (!(preference instanceof CheckBoxPreference)) {

              String value = sharedPreferences.getString(preference.getKey(), "");
              setPreferenceSummary(preference, value);


          }
      }


    }




    /*
     else if (preference instanceof RingtonePreference) {
                // For ringtone preferences, look up the correct display value
                // using RingtoneManager.
                if (TextUtils.isEmpty(stringValue)) {
                    // Empty values correspond to 'silent' (no ringtone).
                    preference.setSummary(R.string.pref_ringtone_silent);

                } else {
                    Ringtone ringtone = RingtoneManager.getRingtone(
                            preference.getContext(), Uri.parse(stringValue));

                    if (ringtone == null) {
                        // Clear the summary if there was a lookup error.
                        preference.setSummary(R.string.summary_choose_ringtone);
                    } else {
                        // Set the summary to reflect the new ringtone display
                        // name.
                        String name = ringtone.getTitle(preference.getContext());
                        preference.setSummary(name);
                    }
                }

            } else if (preference instanceof EditTextPreference) {
                if (preference.getKey().equals("key_gallery_name")) {
                    // update the changed gallery name to summary filed
                    preference.setSummary(stringValue);
                }
            }
     */



}
