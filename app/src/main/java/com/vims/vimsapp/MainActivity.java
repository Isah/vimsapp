package com.vims.vimsapp;

import android.app.SearchManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.elconfidencial.bubbleshowcase.BubbleShowCase;
import com.elconfidencial.bubbleshowcase.BubbleShowCaseBuilder;
import com.elconfidencial.bubbleshowcase.BubbleShowCaseListener;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.mancj.materialsearchbar.SimpleOnSearchActionListener;

import com.vims.vimsapp.entities.schedule.WeekRepeat;
import com.vims.vimsapp.entities.schedule.WeekSchedule;
import com.vims.vimsapp.registerchild.AddChildDataSource;
import com.vims.vimsapp.sync.NetworkReceiver;
import com.vims.vimsapp.sync.SyncManager;
import com.vims.vimsapp.sync.SyncNetworkStatusManager;
import com.vims.vimsapp.utilities.BottomNavigationViewHelper;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.MainThreadExecutor;
import com.vims.vimsapp.utilities.MyRecyclerViewManager;
import com.vims.vimsapp.utilities.alarm.UIAlert;
import com.vims.vimsapp.view.injection.Injection;
import com.vims.vimsapp.view.reports.FragmentReportsGlobalCoverage;
import com.vims.vimsapp.view.users.SQLiteHandler;
import com.vims.vimsapp.view.users.SessionManager;
import com.vims.vimsapp.view.patients.list.ViewCaretakerListFragment;
import com.vims.vimsapp.view.patients.list.ViewPatientsFragment;
import com.vims.vimsapp.view.patients.search.SearchCaretakerActivity;
import com.vims.vimsapp.view.users.LoginActivity;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.view.vaccinations.order.OrderListFragment;
import com.vims.vimsapp.view.vaccinations.schedule.AdapterWeekSchedule;
import com.vims.vimsapp.view.vaccinations.schedule.HomeScheduleFragment;
import com.vims.vimsapp.view.vaccinations.schedule.CalendarScheduleFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import vimsapp.R;

import static com.vims.vimsapp.utilities.ScreenManager.setScreenOrientation;
import static com.vims.vimsapp.utilities.alarm.UIAlert.showSnackBarView;

public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener{

    private SQLiteHandler db;
    private SessionManager session;
    private List<String> lastSearches;
    private MaterialSearchBar searchBar;

    Toolbar toolbar;
    private BottomNavigationView bottomNavigationView;
    DrawerLayout drawer;

    MainActivityViewModel mainActivityViewModel;
    MainActivityViewModelFactory mainActivityViewModelFactory;

    PreferenceHandler preferenceHandler;

    boolean isSyncFromLogin = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        preferenceHandler = PreferenceHandler.getInstance(getApplicationContext());
        preferenceHandler.putPref(getResources().getString(R.string.pref_key_theme), getResources().getString(R.string.pref_array_theme_value_light));
        preferenceHandler.getSharedPreferencesInstance().registerOnSharedPreferenceChangeListener(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.appbar_main_activity);
        toolbar = findViewById(R.id.toolbar_main);
        //toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);



        setSupportActionBar(toolbar);

        //AppIcon As Navigation
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        mainActivityViewModelFactory = new MainActivityViewModelFactory(Injection.provideHospitalSchedule(getApplication()));
        mainActivityViewModel = ViewModelProviders.of(this,mainActivityViewModelFactory).get(MainActivityViewModel.class);

        //toolbar.setTitle("VimsApp");
        //toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        //((AppCompatActivity)getActivity()).getSupportActionBar().setLogo(R.drawable.notice_48);
        final CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);

        ///collapsingToolbarLayout.setExpandedTitleGravity(Gravity.BOTTOM);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout.setCollapsedTitleGravity(Gravity.START);
        collapsingToolbarLayout.setTitleEnabled(false);
        collapsingToolbarLayout.setTitle("VimsApp");

        textViewSync = findViewById(R.id.textView11);
        tvSyncDate = findViewById(R.id.tvLastSyncDate);


        isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());


        if (!session.isLoggedIn()) {
            logoutUser();
        }

     //   setUpDrawerNavigation();
        //setUpSearchBar();
        setUpBottomNavigation();
        setScreenOrientation(this);
        //setUpHiddenStatusBar();

        ///sync ////////////////////
        setUpBottomSheet();
        buttonloadSyncDataListner();
        observeHospitalSchedule();
        observeHospitalScheduleError();
        //////////////////////


        setUpHospitalSpinner();


        weekSchedules = new ArrayList<>();
       // setUpWeekSchedules();

        for (int w = 2; w <= 6; w++) {
            WeekSchedule weekSchedule = new WeekSchedule();

            weekSchedule.setDayOfWeekName(getWeekName(w));
            weekSchedule.setDayOfWeek(0);
            weekSchedule.setWeekOfMonth(0);
            weekSchedules.add(weekSchedule);
        }

        bnLoadSchedule();
        setUpScheduleBottomSheet();



        Button btn = findViewById(R.id.buttonSync);
        btn.setVisibility(View.GONE);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //syncDataPackets();
            }
        });

        //check whether firsttime
        int scheduleFirstTime = preferenceHandler.getIntValue("schedule_first_time");
        if(scheduleFirstTime!=1){ //1 means is old time user
            preferenceHandler.setIntValue("schedule_first_time",1);
            String USER_HOSPITAL_ID = preferenceHandler.getPref(PreferenceHandler.USER_HOSPITAL_ID);


            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    toggleBottomSheetShedule();
                }
            };
            new MainThreadExecutor(2000).execute(runnable);


        }



        displaySyncReminder();


        //   NETWORKING  ////////////////////////////////
        //1. Registers BroadcastReceiver to track network connection changes.
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkReceiver();
        this.registerReceiver(receiver, filter);



    }




////////////////    NETWORKING    ///////////////////////////////////////////////////////////////////

    public static final String WIFI = "Wi-Fi";
    public static final String ANY = "Any";

    private static boolean wifiConnected = false;
    private static boolean mobileConnected = false;
    // The user's current network preference setting.
    public static String sPref = null;
    // Whether the display should be refreshed.
    public static boolean refreshDisplay = false;
    // The BroadcastReceiver that tracks network connectivity changes.
    private NetworkReceiver receiver = new NetworkReceiver();



    public void performSyncing() {
       /* if (((sPref.equals("ANY")) && (wifiConnected || mobileConnected))
                || ((sPref.equals("WIFI")) && (wifiConnected))) {
                */
        if ((sPref.equals(WIFI)) && (wifiConnected)) {

          //  Toast.makeText(getApplicationContext()," now syncing", Toast.LENGTH_LONG).show();

           // syncDataPackets();
        } else {


          //  Toast.makeText(getApplicationContext()," wifi off", Toast.LENGTH_LONG).show();


        }
    }


    // Refreshes the display if the network connection and the
    // pref settings allow it.

    @Override
    public void onStart () {
        super.onStart();

        // Gets the user's network preference settings
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        // Retrieves a string value for the preferences. The second parameter
        // is the default value to use if a preference value is not found.
        sPref = sharedPrefs.getString(getResources().getString(R.string.pref_key_sync), "Wi-Fi");

        updateConnectedFlags();

        /* when there's no network con as per NetworkReceiver Broadcaster
           refresh is set to false otherwise true and if true sync performs
         */
        if(refreshDisplay){
            performSyncing();
        }
    }


    private void updateConnectedFlags(){

        SyncNetworkStatusManager syncNetworkStatusManager = SyncNetworkStatusManager.getInstance(getApplicationContext());
        wifiConnected = syncNetworkStatusManager.isWifiOnline();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Unregisters BroadcastReceiver when app is destroyed.
        if (receiver != null) {
            this.unregisterReceiver(receiver);
        }
        if(preferenceHandler!=null) {
            preferenceHandler.getSharedPreferencesInstance().unregisterOnSharedPreferenceChangeListener(this);
        }
    }


    TextView textViewSync;
    TextView tvSyncDate;



    private void displaySyncReminder(){

        long date_long =  PreferenceHandler.getInstance(getApplicationContext()).getLongPref(PreferenceHandler.SYNC_TIME_KEY);
        Date syncDate = new Date(date_long);

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(syncDate);

        int sync_day_month = calendar1.get(Calendar.DAY_OF_MONTH);
        int sync_month = calendar1.get(Calendar.MONTH);

        Calendar calendar = Calendar.getInstance();
        int current_day_month = calendar.get(Calendar.DAY_OF_MONTH);
        int current_month = calendar.get(Calendar.MONTH);

        int lastDayOfMonth = calendar.getActualMaximum(Calendar.DATE);

        Log.d("CURRENT_MONTH","month = "+current_month);

        boolean isMonthEnd = current_day_month == lastDayOfMonth;
        boolean isLastSyncUpdated = sync_month == current_month;

        //set whether its sync time
        if(isMonthEnd && !isLastSyncUpdated){ //its time to sync  isMonthEnd && !isLastSyncUpdated
           // tvSyncDate.setTextColor(getResources().getColor(R.color.errorColor));

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    toggleBottomSheet();
                }
            };
            new MainThreadExecutor(2000).execute(runnable);

        }else if(!isLastSyncUpdated){
          //  tvSyncDate.setTextColor(getResources().getColor(R.color.errorColor));
        }
        else{
          //  tvSyncDate.setTextColor(getResources().getColor(R.color.colorPrimary));
        }

    }

    private void syncDataPackets(){
       // tvSyncDate.setVisibility(View.INVISIBLE);

        SyncManager syncManager = new SyncManager(getApplicationContext());

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                syncManager.syncData(false,new AddChildDataSource.SyncPatientsCallback() {
                    @Override
                    public void onSynced(String msg) {

                        Runnable run = new Runnable() {
                            @Override
                            public void run() {

                               long date_long =  PreferenceHandler.getInstance(getApplicationContext()).getLongPref(PreferenceHandler.SYNC_TIME_KEY);
                               Date syncDate = new Date(date_long);
                               String syncDateString = "last sync: "+DateCalendarConverter.dateToStringWithoutTime(syncDate);

                              //  String msg = "Data has been synced!";
                                //Toast.makeText(getApplicationContext(),""+msg,Toast.LENGTH_LONG).show();
                                //textViewSync.setText(msg);
                                //textViewSync.setVisibility(View.INVISIBLE);
                               // tvSyncDate.setTextColor(getResources().getColor(R.color.colorPrimary));
                                tvSyncDate.setVisibility(View.VISIBLE);
                                tvSyncDate.setText(syncDateString);
                                //tvSyncDate.setBackground(getResources().getDrawable(R.drawable.shape_borderline_default_cp_review));
                                progressBar.setVisibility(View.INVISIBLE);


                               //
                                // toggleBottomSheet();
                                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);


                                Snackbar snackbar = Snackbar.make(recyclerView, msg, Snackbar.LENGTH_LONG);
                                snackbar.setDuration(4000);
                                showSnackBarView(snackbar);


                                mainActivityViewModel.saveApplicationTime(getApplicationContext());


                            }
                        };

                        new MainThreadExecutor(1000).execute(run);

                    }

                    @Override
                    public void onSyncFailed(String errorMsg) {

                     //   Toast.makeText(getApplicationContext(),"Synced"+errorMsg,Toast.LENGTH_LONG).show();
                      //  Log.d("MainActivitySync","onSync: "+errorMsg);

                       // toggleBottomSheet();
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);


                        Runnable run = new Runnable() {
                            @Override
                            public void run() {

                                String msg = "Never!";

                                //textViewSync.setText(msg);
                                //textViewSync.setVisibility(View.INVISIBLE);
                              //  tvSyncDate.setTextColor(getResources().getColor(R.color.errorColor));
                                tvSyncDate.setText(msg);
                                tvSyncDate.setVisibility(View.VISIBLE);
                                //tvSyncDate.setBackground(getResources().getDrawable(R.drawable.shape_borderline_default_cp_review));
                                progressBar.setVisibility(View.INVISIBLE);




                                if(errorMsg.contains("Data up to date!")){//from loading caretakers

                                    Snackbar snackbar = Snackbar.make(recyclerView, errorMsg, Snackbar.LENGTH_LONG);
                                    snackbar.setDuration(4000);
                                    snackbar.show();
                                    showSnackBarView(snackbar);


                                   // Toast.makeText(getApplicationContext(),""+errorMsg,Toast.LENGTH_LONG).show();
                                }
                                else if(errorMsg.contains("404")){

                                    Snackbar snackbar = Snackbar.make(recyclerView, "Couldn't complete sync", Snackbar.LENGTH_LONG);
                                    snackbar.setDuration(6000);
                                    snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
                                    snackbar.setAction("Retry", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            toggleBottomSheet();

                                        }
                                    }).show();

                                    showSnackBarView(snackbar);


                                }
                                else{

                                    Snackbar snackbar = Snackbar.make(recyclerView, ""+ errorMsg, Snackbar.LENGTH_LONG);
                                    snackbar.setDuration(6000);
                                    snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
                                    snackbar.setAction("Retry", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            toggleBottomSheet();

                                        }
                                    }).show();

                                    showSnackBarView(snackbar);


                                }
                            }
                        };

                        new MainThreadExecutor(1000).execute(run);

                    }
                });

            }
        };

        new MainThreadExecutor(6000).execute(runnable);

    }


    //////////////////////   END OF NETWORKING   ///////////////////////////////////////////////////////
    //////////////////////   SCREEN SET UP       /////////////////////////////////////////




    SearchView searchView;
    Fragment patientsFragment;
    FragmentManager patientsDetailsFragmentMgr = getSupportFragmentManager();


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu_home; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        toolbar.inflateMenu(R.menu.main);

         MenuItem menuItem = menu.findItem(R.id.action_hospital_schedule);


        if(menuItem!=null) {
          //  discoverNewSchedule(toolbar);
        }


        String primaryText = "Load Hospital Schedule";
        String secondaryText = "Tap the menu icon to get access to the hospital schedule";
        String preferenceText = "schedule_first_time";

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

              //  FeatureDiscovery.tapTargetKeepSafeOverFlowMenu(MainActivity.this,toolbar,menuItemId,primaryText,secondaryText);
                //////////////////////////////

            }
        };


        return true;
    }


    private void discoverNewSchedule(Toolbar menuItem){

        //int id = menuItem.getItemId();

       // ViewTarget target = new ViewTarget(toolbar.findViewById(R.id.menu_id_launcher));

       // MenuItem menuItem = menu.findItem(R.id.action_hospital_schedule);


        String primaryText = "Load New Hospital Schedule";
        String secondaryText = "Tap the menu icon and load a new hospital schedule when available";

        //  BubbleShowCase bubbleShowCase = new BubbleShowCase(new BubbleShowCaseBuilder(getActivity()));
        BubbleShowCaseBuilder builder = new BubbleShowCaseBuilder(MainActivity.this);
        builder.title(primaryText);
        builder.targetView(toolbar.findViewById(R.id.action_hospital_schedule));
        builder .description(secondaryText); //More detailed description
        builder.arrowPosition(BubbleShowCase.ArrowPosition.TOP); //You can force the position of the arrow to change the location of the bubble.
        builder.backgroundColor(getResources().getColor(R.color.colorPrimary)); //Bubble background color
        builder.textColor(Color.WHITE); //Bubble Text color
        builder.titleTextSize(17); //Title text size in SP (default value 16sp)
        builder.descriptionTextSize(15); //Subtitle text size in SP (default value 14sp)

        builder .showOnce("BUBBLE_SHOW_CASE_ID") ;//Id to show only once the BubbleShowCase

        builder.listener(new BubbleShowCaseListener() {
            @Override
            public void onTargetClick(BubbleShowCase bubbleShowCase) {

                bubbleShowCase.finishSequence();
            }

            @Override
            public void onCloseActionImageClick(BubbleShowCase bubbleShowCase) {
                bubbleShowCase.finishSequence();
            }

            @Override
            public void onBackgroundDimClick(BubbleShowCase bubbleShowCase) {

                bubbleShowCase.finishSequence();
            }

            @Override
            public void onBubbleClick(BubbleShowCase bubbleShowCase) {

                bubbleShowCase.finishSequence();
            }
        });

        builder.show();



    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                //drawer.openDrawer(GravityCompat.START);
                return true;

            case R.id.action_sync:
                toggleBottomSheet();


                return true;

            case R.id.action_hospital_schedule:

                toggleBottomSheetShedule();

                return true;



            case R.id.action_settings:
                Intent intent = new Intent(this,ActivitySettings.class);
                startActivity(intent);

                 return true;
        }

        return super.onOptionsItemSelected(item);
    }



    private void loadScheduleMenu(){

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();

               toggleBottomSheet();

            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();



    }



    private void setUpSearch(Menu menu){
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        //Android guides explicitly says:
        // Assumes current activity is the searchable activity
        // searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        // Which is in your case NOT TRUE. Because you don't want to noticemgt in MainActivity but in SearchableActivity.
        // That means you should use this ComponentName instead of getComponentName() method:

        // ComponentName cn = new ComponentName(this, SearchCaretakerActivity.class);
        ComponentName cn = getComponentName();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(cn));
        //   searchView.setBackgroundColor(Color.WHITE);
        // Do not iconify the widget;expand it by default
        // searchView.setIconifiedByDefault(false);
        // searchView.setSubmitButtonEnabled(true);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {


                Bundle bundle = new Bundle();
                bundle.putString("searchQuery", query);


                patientsFragment = ViewCaretakerListFragment.newInstance(query,"");

                patientsDetailsFragmentMgr.beginTransaction()
                        .add(R.id.fragmentholder_search, patientsFragment)
                        .commit();


                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });



        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                patientsDetailsFragmentMgr.beginTransaction()
                        .remove(patientsFragment)
                        .commit();

                return false;
            }
        });

    }




    //App RATINGS ////////////////////////////////////////////////




    /////////////////// BOTTOM SHEET //////////////////////////////////////////////

    BottomSheetBehavior sheetBehavior;
    ConstraintLayout layoutBottomSheet;

    private void setUpBottomSheet(){

        layoutBottomSheet = findViewById(R.id.bottom_sheet_sync);
        //layoutBottomSheet = .hospitalScheduleBottomSheet.bottomSheetCalendar;

        //Provides callbacks and make the BottomSheet work with CoordinatorLayout
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);

        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:


                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        // btnBottomSheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        //  btnBottomSheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });


    }

    private void toggleBottomSheet(){

        long date_long =  PreferenceHandler.getInstance(getApplicationContext()).getLongPref(PreferenceHandler.SYNC_TIME_KEY);
        String syncDateString;
        if(date_long>0) {
            Date syncDate = new Date(date_long);

            Calendar c = Calendar.getInstance();
            c.set(Calendar.MONTH,8);

           long difference = DateCalendarConverter.calculateDifferenceInDates(syncDate,new Date());

           //Log.d("DateDiff"," d: "+difference);
           if(difference > 31){
               tvSyncDate.setTextColor(getResources().getColor(R.color.errorColor));
           }

            syncDateString = "last sync: "+DateCalendarConverter.dateToStringWithoutTime(syncDate);

            tvSyncDate.setText(syncDateString);
        }else{
            syncDateString = " Never";
            tvSyncDate.setText(syncDateString);
        }


        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            //btnBottomSheet.setText("Close sheet");
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            // btnBottomSheet.setText("Expand sheet");
        }


    }



    private BottomSheetBehavior sheetBehaviorSchedule;
    ConstraintLayout layoutBottomSheetSchedule;
    TextView tvDateScheduled;




    private void isShowCreatSchedule(boolean isShow){

        if(isShow){
            layoutCreateSchedule.setVisibility(View.VISIBLE);
            layoutLoadSchedule.setVisibility(View.INVISIBLE);
            buttonCreateSchedule.setVisibility(View.VISIBLE);
        }else{
            layoutCreateSchedule.setVisibility(View.INVISIBLE);
            layoutLoadSchedule.setVisibility(View.VISIBLE);
            buttonCreateSchedule.setVisibility(View.INVISIBLE);

        }


    }


    private void showScheduleDateCreated(){

        long savedTimeStamp = preferenceHandler.getLongPref(PreferenceHandler.HOSPITAL_SCHEDULE_CREATION_DATE);

        String date;
        if(savedTimeStamp < 1){
            date = "N/A";
            tvDateScheduled.setText(date);
        }else {
            String dateConverted = ""+DateCalendarConverter.dateToStringWithoutTime(new Date(savedTimeStamp));


            date = "created: "+dateConverted;
            tvDateScheduled.setText(date);
        }


    }

    private void setUpScheduleBottomSheet(){

        layoutBottomSheetSchedule = findViewById(R.id.bottom_sheet_hospital_schedule);
        // Provides callbacks and make the BottomSheet work with CoordinatorLayout
        sheetBehaviorSchedule = BottomSheetBehavior.from(layoutBottomSheetSchedule);

        tvDateScheduled = findViewById(R.id.textViewScheduleSubtitle);


        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
        sheetBehaviorSchedule.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:

                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        // btnBottomSheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        //  btnBottomSheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });






    }
    private void toggleBottomSheetShedule(){
        showScheduleDateCreated();

        isShowCreatSchedule(false);

        if (sheetBehaviorSchedule.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehaviorSchedule.setState(BottomSheetBehavior.STATE_EXPANDED);
            //btnBottomSheet.setText("Close sheet");
        } else {

            sheetBehaviorSchedule.setState(BottomSheetBehavior.STATE_COLLAPSED);

        }


    }


    private void observeHospitalScheduleError(){

        Observer<String> errorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                Snackbar snackbar = Snackbar.make(recyclerView, s, Snackbar.LENGTH_LONG);
                snackbar.setDuration(6000);
                snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
                showSnackBarView(snackbar);
                toggleBottomSheetShedule();
                snackbar.setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        toggleBottomSheetShedule();

                        //  mainActivityViewModel.loadHospitalSchedule(isLoadOnline, preferenceHandler.getPref(PreferenceHandler.USER_HOSPITAL_NAME), null);
                    }
                }).show();


                progressBarHospitalSchedule.setVisibility(View.INVISIBLE);



            }
        };

        mainActivityViewModel.getHospitalScheduleError().observe(this,errorObserver);


    }
    private void observeHospitalSchedule(){

        Observer<String> stringObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                if(s!=null){

                    //Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
                    //String content = "loaded Hospital Schedule";
                    String title = "The "+s+" has been loaded successfully";
                    //UIAlert.showAlert(title,""+s,MainActivity.this,UIAlert.AlertType.SUCCESS);
                    // Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
                    preferenceHandler.setIntValue(PreferenceHandler.SCHEDULE_LOADED_INT,1);

                    progressBar.setVisibility(View.INVISIBLE);
                    progressBarHospitalSchedule.setVisibility(View.INVISIBLE);
                    toggleBottomSheetShedule();

                    Snackbar snackbar = Snackbar.make(recyclerView, title, Snackbar.LENGTH_LONG);
                    snackbar.setDuration(4000);
                    showSnackBarView(snackbar);

                    mainActivityViewModel.saveApplicationTime(getApplicationContext());

                    preferenceHandler.putLongPref(PreferenceHandler.HOSPITAL_SCHEDULE_CREATION_DATE,new Date().getTime());
                    //String timeStampDate = "date: "+new Date(savedTimeStamp);
                    //tvDateScheduled.setText(timeStampDate);

                    adapterWeekSchedule.getWeekSchedules().clear();

                    isShowCreatSchedule(false);


                }

            }
        };

        mainActivityViewModel.getHospitalScheduleLoadFeedback().observe(this,stringObserver);

    }





    Spinner spinnerHospital;
    private void setUpHospitalSpinner(){

        spinnerHospital =  findViewById(R.id.hospitalSpinner);

        //item sinner sets the spinner text color
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.hospital_array, R.layout.item_spinner_blue);
        adapter.setDropDownViewResource(R.layout.item_spinner_dropdown_blue);


        //eventsSpinner.setBackground(getResources().getDrawable(ThemeManager.getSpinnerDrawable()));
        spinnerHospital.setAdapter(adapter);

        spinnerHospital.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

               // String item = parent.getSelectedItem().toString();
               // preferenceHandler.putPref(PreferenceHandler.USER_HOSPITAL_NAME,item);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }
    private ProgressBar progressBarHospitalSchedule;


    AdapterWeekSchedule adapterWeekSchedule;
    private void updateInfo(List<WeekSchedule> weekSchedules){

        adapterWeekSchedule = new AdapterWeekSchedule(weekSchedules,getApplicationContext());

        adapterWeekSchedule.setHasStableIds(true);
        adapterWeekSchedule.notifyDataSetChanged();
        recyclerView.setAdapter(adapterWeekSchedule);

    }

    RecyclerView recyclerView;
    Switch alarmToggle;
    ArrayList<WeekSchedule> weekSchedules;

    ConstraintLayout layoutCreateSchedule;
    ConstraintLayout layoutLoadSchedule;

    Button buttonCreateSchedule;
    Button buttonShowAddSchedule;

    private void bnLoadSchedule(){

        //Animator for recycler view
        Interpolator overshoot  = new OvershootInterpolator(1f);
        Interpolator accelerateDecelerate  = new AccelerateDecelerateInterpolator();
        Interpolator bounce  = new BounceInterpolator();

        SlideInUpAnimator animator = new SlideInUpAnimator(accelerateDecelerate);

        recyclerView = findViewById(R.id.recycler_view_week);
        MyRecyclerViewManager.recyclerViewSetUp(getApplicationContext(),recyclerView,animator);

        updateInfo(weekSchedules);


        buttonCreateSchedule = findViewById(R.id.buttonHospitalSchedule);
        buttonShowAddSchedule = findViewById(R.id.buttonShowCreateSchedule);

        TextView tvScheduleTitle = findViewById(R.id.textViewScheduleTitle);
        TextView tvScheduleDesc = findViewById(R.id.textViewScheduleDescription);


        layoutCreateSchedule = findViewById(R.id.custom_schedule);
        layoutLoadSchedule = findViewById(R.id.loadScheduleLayout);
        //ImageView ivSchedule = findViewById(R.id.ivSchedule);

        progressBarHospitalSchedule = findViewById(R.id.progressBarHospitalSchedule);

        String buttonTextCreate = "CREATE LOCAL SCHEDULE";
        String buttonTextLoad = "LOAD ONLINE SCHEDULE";

        String title1 = "CREATE SCHEDULE";
        String title2 = "HOSPITAL SCHEDULE";

        buttonCreateSchedule.setText(buttonTextCreate);
        tvScheduleTitle.setText(title2);


        alarmToggle = findViewById(R.id.switch_schedule);
        alarmToggle.setVisibility(View.GONE);
        isLoadOnline = false;


         alarmToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){

                    buttonCreateSchedule.setText(buttonTextLoad);
                    tvScheduleTitle.setText(title2);



                    tvScheduleDesc.setVisibility(View.VISIBLE);
                    layoutCreateSchedule.setVisibility(View.GONE);
                    layoutLoadSchedule.setVisibility(View.VISIBLE);

                    //spinnerHospital.setVisibility(View.INVISIBLE);

                    isLoadOnline = true;

                }else{

                    tvScheduleTitle.setText(title1);
                    buttonCreateSchedule.setText(buttonTextCreate);
                    tvScheduleDesc.setVisibility(View.GONE);


                    layoutCreateSchedule.setVisibility(View.VISIBLE);
                    layoutLoadSchedule.setVisibility(View.INVISIBLE);
                    //spinnerHospital.setVisibility(View.VISIBLE);


                    isLoadOnline = false;

                }

            }
        });

       // alarmToggle.setChecked(true);
        buttonCreateSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressBarHospitalSchedule.setVisibility(View.VISIBLE);

                ArrayList<WeekSchedule> myschedules = new ArrayList<>(finalCopyWeeks(adapterWeekSchedule.getWeekSchedules()));


                if(isLoadOnline){

                    mainActivityViewModel.loadHospitalSchedule(isLoadOnline, preferenceHandler.getPref(PreferenceHandler.USER_HOSPITAL_NAME), myschedules);

                }else{


                    if(!myschedules.isEmpty() && checkIfContainsAnyWeekOfMonth(myschedules)) {

                        mainActivityViewModel.loadHospitalSchedule(isLoadOnline, preferenceHandler.getPref(PreferenceHandler.USER_HOSPITAL_NAME), myschedules);

                    }else{

                        progressBarHospitalSchedule.setVisibility(View.INVISIBLE);
                        UIAlert.showAlert("Info","Please select Day and Repeat weeks!",MainActivity.this,UIAlert.AlertType.INFO);

                    }

                }




            }
        });




        buttonShowAddSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isShowCreatSchedule(true);

            }
        });

    }




    private boolean checkIfContainsAnyWeekOfMonth(List<WeekSchedule> schedules){

        for(WeekSchedule ws: schedules){

            for(WeekRepeat weekRepeat: ws.getWeekRepeats()) {
                if (weekRepeat.getRepeatDay() < 1) {

                    return false;
                }
            }

        }

        return true;
    }


    private List<WeekSchedule> finalCopyWeeks(List<WeekSchedule> weekSchedules){

        // Create a new ArrayList
        ArrayList<WeekSchedule> newList = new ArrayList<>();

        // Traverse through the first list
        for (WeekSchedule element : weekSchedules) {
            // If this element is not present in newList
            // then add it
            if (!newList.contains(element) && element.isSelected()) {

                newList.add(element);

            }

        }

        // return the new list
        return newList;
    }
    LinearLayout radioGroupLayout;
    private void showWeekOfMonth(boolean isDisplay){
        if(isDisplay){
            radioGroupLayout.setVisibility(View.VISIBLE);
        }else{
            radioGroupLayout.setVisibility(View.INVISIBLE);
        }

    }


    String day = "";

    private String getWeekName(int i){

        //Remember Sunday is first day of week = 1
        if(i == 2){
            day = "M";
        }else if(i == 3){
            day = "T";
        }
        else if(i == 4){
            day = "W";
        }
        else if(i == 5){
            day = "T";
        }
        else if(i == 6){
            day = "F";
        }
        return day;
    }





    ProgressBar progressBar;
    boolean isLoadOnline = false;




    private void buttonloadSyncDataListner(){
        Button button = findViewById(R.id.buttonSchedule);

        progressBar = findViewById(R.id.progressBarSchedule);

        String buttonText1 = "SYNC DATA";
        String buttonText2 = "LOAD ONLINE SCHEDULE";

        button.setText(buttonText1);

        /*
        Switch alarmToggle = findViewById(R.id.switch2);
        alarmToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){

                    button.setText(buttonText2);
                    isLoadOnline = true;

                }else{

                    button.setText(buttonText1);
                    isLoadOnline = false;

                }

            }
        });
         */

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressBar.setVisibility(View.VISIBLE);

               // isSyncFromLogin = false;
                syncDataPackets();

            }
        });




    }

    ///////////////////////////////////////////////////////////////////////////////






    //-------------------SEARCH BAR  -------------------------------------------------------/
    private void setUpSearchBar(){
        // Search bar
        searchBar = findViewById(R.id.searchBar);
        //enable searchbar callbacks
        //searchBar.setOnSearchActionListener(this);
        //restore last queries from disk
        lastSearches = searchBar.getLastSuggestions();
        searchBar.setLastSuggestions(lastSearches);
        //Inflate menu and setup OnMenuItemClickListener
        searchBar.inflateMenu(R.menu.main);
        searchBar.setSearchIcon(R.drawable.ic_search_black_24dp);
        searchBar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        searchBar.setCardViewElevation(10);
        searchBar.setDividerColor(R.color.colorAccent);
        //searchBar.setPlaceHolderColor(getResources().getColor(R.color.light_blue));
          searchBar.setNavButtonEnabled(true);
        // SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        ComponentName cn = new ComponentName(this, SearchCaretakerActivity.class);

        searchBar.setOnSearchActionListener(new SimpleOnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {
                String s = enabled ? "enabled" : "disabled";
                Toast.makeText(MainActivity.this, "Search " + s, Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
              //  startSearch(text.toString(), true, null, true);
              //   setSearchableInfo(searchManager.getSearchableInfo(cn));
            }

            @Override
            public void onButtonClicked(int buttonCode) {
                switch (buttonCode) {
                    case MaterialSearchBar.BUTTON_NAVIGATION:
                        drawer.openDrawer(Gravity.LEFT);
                        break;
                    case MaterialSearchBar.BUTTON_SPEECH:
                        Toast.makeText(MainActivity.this, "Speech ", Toast.LENGTH_SHORT).show();

                        break;
                    case MaterialSearchBar.BUTTON_BACK:
                        searchBar.disableSearch();
                        break;
                }
            }
        });


        searchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("LOG_TAG", getClass().getSimpleName() + " text changed " + searchBar.getText());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }

        });

    }


    private void selectNavItem(MenuItem item){
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            // Handle the camera action
        }
        else if (id == R.id.nav_settings) {
            Intent intent = new Intent(getApplicationContext(), ActivitySettings.class);
            startActivity(intent);

        } else if (id == R.id.nav_help) {

        } else if (id == R.id.nav_logout) {
            logoutUser();
        }

    }

    //---------------------DRAWER NAVIGATION -----------------------------------------------/
    private void  setUpDrawerNavigation(){

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new DrawerNavigationListner());



        View headerView = navigationView.getHeaderView(0);
        TextView txtName = headerView.findViewById(R.id.profile_name);
        TextView txtEmail = headerView.findViewById(R.id.profile_email);


        //Fetching user details from sqlite
        HashMap<String, String> user = db.getUserDetails();

        String name = user.get("name");
        String email = user.get("email");

        // Displaying the user details on the screen
        txtName.setText(name);
        txtEmail.setText(email);

    }

    public class DrawerNavigationListner implements NavigationView.OnNavigationItemSelectedListener{

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            // Handle navigation view item clicks here.

            selectNavItem(item);

            return true;
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        finishAffinity();

        // int pid = android.os.Process.myPid();
        // android.os.Process.killProcess(pid);
        /*
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            finishAffinity();
        }
        */

        /*
        finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
          */
    }


    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     */
    private void logoutUser() {
        session.setLogin(false);

        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }


    //---------------BOTTOM NAVIGATION NAVIGATION-----------------------------/






    @Override
    protected void onPostResume() {
        super.onPostResume();
       // mainActivityViewModel.setIsmHomeBottomNavSelected(true);
    }

    boolean isPotrait = false;

    private class MyBottomItemsListner implements BottomNavigationView.OnNavigationItemSelectedListener{

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            item.setChecked(true);

            selectBottomNavItem(item);

            return true;

            }
    }



    void setUpBottomNavigation(){

        bottomNavigationView = findViewById(R.id.bottom_navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        NavigationView navigationView = findViewById(R.id.nav_view_bottom_items);
        mainActivityViewModel.setIsmHomeBottomNavSelected(true);

        observePatientsNaviagation();
        observeHomeNavigation();
        observeScheduleNavigation();
        observeReportsNavigation();
        observeOrdersNavigation();

        bottomNavigationView.setOnNavigationItemSelectedListener(new MyBottomItemsListner());

        /*
        navigationView.setPadding(0,140,0,0);
        CoordinatorLayout.LayoutParams layoutParams = ( CoordinatorLayout.LayoutParams) navigationView.getLayoutParams();
        layoutParams.setMargins(0,144,0,0);
        navigationView.setLayoutParams(layoutParams);
        navigationView.setNavigationItemSelectedListener(new MyVerticalBottomViewItemsListner());
        */



    }

    public void selectBottomNavItem(MenuItem item){


        switch (item.getItemId()) {
            case R.id.menu_home:


                  mainActivityViewModel.setIsmHomeBottomNavSelected(true);
                  mainActivityViewModel.setIsPatientsBottomNavSelected(false);
                  mainActivityViewModel.setIsmScheduleBottomNavSelected(false);
                  mainActivityViewModel.setIsmReportsBottomNavSelected(false);
                  mainActivityViewModel.setIsOrdersBottomNavSelected(false);

                break;


            case R.id.menu_orders:


                mainActivityViewModel.setIsOrdersBottomNavSelected(true);
                mainActivityViewModel.setIsmReportsBottomNavSelected(false);
                mainActivityViewModel.setIsmHomeBottomNavSelected(false);
                mainActivityViewModel.setIsPatientsBottomNavSelected(false);
                mainActivityViewModel.setIsmScheduleBottomNavSelected(false);


                break;
           /*
            case R.id.menu_schedule:

                //FragmentTransaction saved_transaction = getSupportFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                //saved_transaction.replace(R.id.fragmentholder_botton, fragment);
                //  saved_transaction.addToBackStack(null);
                // Commit the transaction
                //saved_transaction.commit();


                mainActivityViewModel.setIsmScheduleBottomNavSelected(true);
                mainActivityViewModel.setIsmReportsBottomNavSelected(false);
                mainActivityViewModel.setIsPatientsBottomNavSelected(false);
                mainActivityViewModel.setIsmHomeBottomNavSelected(false);


                break;
              */


            case R.id.menu_reports:


                mainActivityViewModel.setIsmReportsBottomNavSelected(true);
                mainActivityViewModel.setIsmHomeBottomNavSelected(false);
                mainActivityViewModel.setIsPatientsBottomNavSelected(false);
                mainActivityViewModel.setIsmScheduleBottomNavSelected(false);
                mainActivityViewModel.setIsOrdersBottomNavSelected(false);


                break;

            case R.id.menu_patients:
                isPaientsTaped = true;


                isPotrait = getResources().getBoolean(R.bool.layout_orientation_potrait);

                mainActivityViewModel.setIsPatientsBottomNavSelected(true);

                break;



        }


    }


    boolean isPaientsTaped = false;
    public void observePatientsNaviagation(){

        Observer<Boolean> patientsObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {

                if(aBoolean) {

                    FragmentManager patientsFragmentMgr = getSupportFragmentManager();
                    patientsFragmentMgr.beginTransaction()
                            .replace(R.id.fragmentholder_botton, ViewPatientsFragment.newInstance("User", "Profiles"))
                            .commit();


                }else{


                  //  mainActivityViewModel.setIsmReportsBottomNavSelected(false);
                   // mainActivityViewModel.setIsmScheduleBottomNavSelected(false);
                   // mainActivityViewModel.setIsmHomeBottomNavSelected(false);

                }


            }
        };

        mainActivityViewModel.getIsPatientsBottomNavSelected().observe(this,patientsObserver);


    }


    public void observeHomeNavigation(){

        Observer<Boolean> homeObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {

                if(aBoolean) {
                    FragmentManager hfragmentManager = getSupportFragmentManager();
                    hfragmentManager.beginTransaction()
                            .replace(R.id.fragmentholder_botton, HomeScheduleFragment.newInstance("upcoming"))
                            .commit();

                }else{


                 //   mainActivityViewModel.setIsmReportsBottomNavSelected(false);
                  //  mainActivityViewModel.setIsmScheduleBottomNavSelected(false);
                   // mainActivityViewModel.setIsPatientsBottomNavSelected(false);


                }


            }
        };

        mainActivityViewModel.getIsmHomeBottomNavSelected().observe(this,homeObserver);


    }


    public void observeScheduleNavigation(){

        Observer<Boolean> observerSchedule = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {


                if(aBoolean){

                    //Toast.makeText(getApplicationContext(),"Schedule Not Available",Toast.LENGTH_LONG).show();

                    FragmentManager scheduleFragmentMgr = getSupportFragmentManager();
                    scheduleFragmentMgr.beginTransaction()
                            .replace(R.id.fragmentholder_botton, CalendarScheduleFragment.newInstance("Profiles"))
                            .commit();

                }else{

                  //  mainActivityViewModel.setIsmReportsBottomNavSelected(false);
                   // mainActivityViewModel.setIsPatientsBottomNavSelected(false);
                   // mainActivityViewModel.setIsmHomeBottomNavSelected(false);




                }


            }
        };


        mainActivityViewModel.getIsmScheduleBottomNavSelected().observe(this, observerSchedule);





    }


    public void observeReportsNavigation(){


        Observer<Boolean> reportsObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {


                if(aBoolean){


                 //   Toast.makeText(getApplicationContext(),"Reports Not Available",Toast.LENGTH_LONG).show();

                    FragmentManager reportsFragmentMgr = getSupportFragmentManager();
                    reportsFragmentMgr.beginTransaction()
                            .replace(R.id.fragmentholder_botton, FragmentReportsGlobalCoverage.newInstance("User", "Profiles"))
                            .commit();



                }else{


                  //  mainActivityViewModel.setIsPatientsBottomNavSelected(false);
                   // mainActivityViewModel.setIsmScheduleBottomNavSelected(false);
                   // mainActivityViewModel.setIsmHomeBottomNavSelected(false);


                }



            }
        };



        mainActivityViewModel.getIsmReportsBottomNavSelected().observe(this,reportsObserver);


    }

    public void observeOrdersNavigation(){


        Observer<Boolean> ordersObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {

                if(aBoolean){


                     //  Toast.makeText(getApplicationContext(),"Orders Available",Toast.LENGTH_LONG).show();


                    FragmentManager ordersFragmentMgr = getSupportFragmentManager();
                    ordersFragmentMgr.beginTransaction()
                            .replace(R.id.fragmentholder_botton, new OrderListFragment())
                            .commit();



                }

            }
        };



        mainActivityViewModel.getIsOrdersBottomNavSelected().observe(this,ordersObserver);


    }





    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        //check what preference changed
        if(key.equals(getString(R.string.pref_key_theme))){
           String themeId =  sharedPreferences.getString(key,"");
           if(themeId.equals("R.style.AppTheme_Light")){

               Toast.makeText(getApplicationContext(),"Light Theme Selected",Toast.LENGTH_LONG).show();
               setTheme(R.style.AppTheme_Light);
           }else{//Dark

               Toast.makeText(getApplicationContext(),"Dark Theme Selected",Toast.LENGTH_LONG).show();
               setTheme(R.style.AppTheme_Dark);
           }
            //preferenceHandler.putPref(sharedPreferences.getString(key,""));
        }


    }
}
