package com.vims.vimsapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.ParseException;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.webkit.WebView;

import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.MainThreadExecutor;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.view.users.LoginActivity;

import java.text.SimpleDateFormat;
import java.util.Date;

import vimsapp.R;

import static android.net.sip.SipErrorCode.TIME_OUT;

public class SplashActivity  extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                    if(isDateAccurate()) {
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(intent);
                        //  Intent intent = new Intent(this, ActivityLauncher.class);
                        // startActivity(intent);
                        finish();
                    }else{
                        String title = "Date";
                        String dateNow = DateCalendarConverter.dateToStringWithoutTime(new Date());
                        String msg = "Your phone date "+dateNow+" is in accurate! adjust your clock and try again";
                        displayError(title,msg);

                    }
            }
        };
        new MainThreadExecutor(300).execute(runnable);


    }


    private boolean isDateAccurate(){

        PreferenceHandler preferenceHandler = PreferenceHandler.getInstance(getApplicationContext());

        Date save_timestamp = new Date(preferenceHandler.getLongPref(PreferenceHandler.DATE_ACCURATE_TIME_KEY));
        //Log.d("DateTimeAccurate", "splash; "+save_timestamp +" "+new Date());

        if(new Date().before(save_timestamp)){

            return false;
        }

        return true;
    }


    private void displayError(String title, String msg) {

        AlertDialog mAlertDialog = new AlertDialog.Builder(SplashActivity.this, R.style.Theme_AppCompat_Light_Dialog_Alert)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton("Adjust Date", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        startActivityForResult(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS), 0);
                        finish();

                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .show();
    }



}
