package com.vims.vimsapp;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.net.ParseException;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.vims.vimsapp.entities.schedule.WeekSchedule;
import com.vims.vimsapp.managehospitalschedule.GenerateHospitalSchedule;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.utilities.PreferenceHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivityViewModel extends ViewModel{


    private MutableLiveData<Boolean> ismPatientsBottomNavSelected;

    private MutableLiveData<Boolean> ismHomeBottomNavSelected;

    private MutableLiveData<Boolean> ismScheduleBottomNavSelected;

    private MutableLiveData<Boolean> ismReportsBottomNavSelected;

    private MutableLiveData<Boolean> ismOrdersBottomNavSelected;




    public MainActivityViewModel(GenerateHospitalSchedule generateHospitalSchedule) {
        this.generateHospitalSchedule = generateHospitalSchedule;
        ismHomeBottomNavSelected = new MutableLiveData<>();

    }


    public MutableLiveData<Boolean> getIsPatientsBottomNavSelected() {

        if(ismPatientsBottomNavSelected== null){

            ismPatientsBottomNavSelected = new MutableLiveData<>();
        }
        return ismPatientsBottomNavSelected;
    }

    public void setIsPatientsBottomNavSelected(Boolean isPatientsBottomNavSelected) {

        this.ismPatientsBottomNavSelected.postValue(isPatientsBottomNavSelected);
    }




    public MutableLiveData<Boolean> getIsOrdersBottomNavSelected() {

        if(ismOrdersBottomNavSelected== null){

            ismOrdersBottomNavSelected = new MutableLiveData<>();
        }
        return ismOrdersBottomNavSelected;
    }

    public void setIsOrdersBottomNavSelected(Boolean isOrdersBottomNavSelected) {
        if(ismOrdersBottomNavSelected== null){

            ismOrdersBottomNavSelected = new MutableLiveData<>();
        }
        this.ismOrdersBottomNavSelected.postValue(isOrdersBottomNavSelected);
    }








    public MutableLiveData<Boolean> getIsmHomeBottomNavSelected() {
        if(ismHomeBottomNavSelected == null){

            ismHomeBottomNavSelected = new MutableLiveData<>();
        }
        return ismHomeBottomNavSelected;
    }


    public void setIsmHomeBottomNavSelected(Boolean isHomeBottomNavSelected) {
        if(ismHomeBottomNavSelected == null){
            ismHomeBottomNavSelected = new MutableLiveData<>();
        }
        this.ismHomeBottomNavSelected.postValue(isHomeBottomNavSelected);
    }




    public MutableLiveData<Boolean> getIsmScheduleBottomNavSelected() {
        if(ismScheduleBottomNavSelected == null){
            ismScheduleBottomNavSelected = new MutableLiveData<>();
        }

        return ismScheduleBottomNavSelected;
    }

    public void setIsmScheduleBottomNavSelected(Boolean isScheduleBottomNavSelected) {
        this.ismScheduleBottomNavSelected.postValue(isScheduleBottomNavSelected);
    }




    public MutableLiveData<Boolean> getIsmReportsBottomNavSelected() {

        if(ismReportsBottomNavSelected == null){

            ismReportsBottomNavSelected = new MutableLiveData<>();
        }

        return ismReportsBottomNavSelected;
    }

    public void setIsmReportsBottomNavSelected(Boolean isReportsBottomNavSelected) {
        this.ismReportsBottomNavSelected.postValue(isReportsBottomNavSelected);
    }




    private MutableLiveData<String> hospitalScheduleError;
    public MutableLiveData<String> getHospitalScheduleError() {
        if(hospitalScheduleError == null){
            hospitalScheduleError = new MutableLiveData<>();
        }
        return hospitalScheduleError;
    }

    public void setHospitalScheduleError(String hospital_schedule_error) {
        if(hospitalScheduleError == null){
            hospitalScheduleError = new MutableLiveData<>();
        }
        this.hospitalScheduleError.postValue(hospital_schedule_error);
    }




    private MutableLiveData<String> hospitalScheduleLoadFeedback;
    private GenerateHospitalSchedule generateHospitalSchedule;


    public MutableLiveData<String> getHospitalScheduleLoadFeedback() {
        if(hospitalScheduleLoadFeedback==null){
            hospitalScheduleLoadFeedback = new MutableLiveData<>();
        }

        return hospitalScheduleLoadFeedback;
    }

    public void loadHospitalSchedule(boolean isLoadOnline, String hospitalId, List<WeekSchedule> weekSchedules){

        if(hospitalScheduleLoadFeedback==null){
            hospitalScheduleLoadFeedback = new MutableLiveData<>();
        }

        GenerateHospitalSchedule.RequestType requestType;

        if(!isLoadOnline){//offline request you create a new schedule
           requestType = GenerateHospitalSchedule.RequestType.CREATE_SCHEDULE;
        }else{
            requestType = GenerateHospitalSchedule.RequestType.LOAD_SCHEDULE;
        }

        Log.d("ScheduleLoad","ISV: "+isLoadOnline+" type: "+requestType);


        GenerateHospitalSchedule.RequestValues requestValues = new GenerateHospitalSchedule.RequestValues(isLoadOnline,hospitalId,requestType);
        requestValues.setWeekSchedules(weekSchedules);


        generateHospitalSchedule.execute(requestValues, new UseCase.OutputBoundary<GenerateHospitalSchedule.ResponseValue>() {
            @Override
            public void onSuccess(GenerateHospitalSchedule.ResponseValue result) {

                hospitalScheduleLoadFeedback.postValue(result.getResponse());

            }

            @Override
            public void onError(String errorMessage) {

                setHospitalScheduleError(errorMessage);

            }
        });



    }






    public void  saveApplicationTime(Context context){

        RequestQueue queue = Volley.newRequestQueue(context);
        String url ="http://worldclockapi.com/api/json/utc/now";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            try {

                                JSONObject jObj = new JSONObject(response);
                                String  timeOffset = jObj.getString("currentDateTime");

                                String[] time_array = timeOffset.split("T");
                                String extractedDate = time_array[0];
                                //2019-06-10
                                String[] extractedDateArray = extractedDate.split("-");

                                Log.d("DateTimeAccurate","time ext: "+extractedDate);


                                int y = Integer.parseInt(extractedDateArray[0]);
                                int m = Integer.parseInt(extractedDateArray[1]);
                                int d = Integer.parseInt(extractedDateArray[2]);

                               // Log.d("DateTimeAccurate","time ext: "+extractedDate);

                                Calendar calendar = Calendar.getInstance();
                                calendar.set(Calendar.YEAR, y);
                                calendar.set(Calendar.MONTH, --m);
                                calendar.set(Calendar.DAY_OF_MONTH, d);
                                Date formatedDateOffset = DateCalendarConverter.dateWithTimeToDateWithoutTime(calendar.getTime());

                                Log.d("DateTimeAccurate","time: "+formatedDateOffset);

                                PreferenceHandler preferenceHandler = PreferenceHandler.getInstance(context);
                                //Date date = simpleDateFormat.parse(new Date(timeOffset));
                                preferenceHandler.putLongPref(PreferenceHandler.DATE_ACCURATE_TIME_KEY,formatedDateOffset.getTime());

                            }catch (JSONException e){

                                Log.d("DateTimeAccurate","parse: "+e.getMessage());

                            }


                            //Log.d(TAG, "onResponse: " + result.toString());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  Log.w(TAG, "onErrorResponse: "+ error.getMessage());
                Log.d("DateTimeAccurate","volley: "+error);
            }
        });
        queue.add(stringRequest);

    }




}
