package com.vims.vimsapp.utilities;

import java.util.regex.Pattern;

/**
 * Created by Lincoln on 7/19/2018.
 */

public class Sentinel {

    public static boolean isValid(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }


    public static boolean isCharacterValid(String character) {
        String emailRegex = "[^\\s]*";//[^\s]*

        Pattern pat = Pattern.compile(emailRegex);
        if (character == null)
            return false;
        return pat.matcher(character).matches();
    }


}
