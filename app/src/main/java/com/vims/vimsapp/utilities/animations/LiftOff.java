package com.vims.vimsapp.utilities.animations;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.transition.TransitionValues;
import android.view.View;
import android.view.ViewGroup;

public class LiftOff {



    @TargetApi(21)
    public Animator createAnimator(ViewGroup sceneRoot, TransitionValues startValues, TransitionValues endValues){

        float lift = 4f;

        return ObjectAnimator.ofFloat(endValues.view, View.TRANSLATION_Z,lift,0f);


    }


}
