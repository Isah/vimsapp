package com.vims.vimsapp.utilities;


import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.vims.vimsapp.view.patients.list.ItemOffsetDecoration;

import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import vimsapp.R;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;

public class MyRecyclerViewManager{



    public static void recyclerViewSetUp(Context context, RecyclerView recyclerView, SlideInUpAnimator animator){

        DividerItemDecoration decoration = new DividerItemDecoration(context, VERTICAL);
       // recyclerView.addItemDecoration(decoration);
        recyclerView.setItemAnimator(animator);
        recyclerView.getItemAnimator().setAddDuration(100);
        //setting animation on scrolling

        recyclerView.setHasFixedSize(true); //enhance recycler view scroll
        recyclerView.setNestedScrollingEnabled(false);// enable smooth scrooll

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(context, R.dimen.item_offset_large);
        recyclerView.addItemDecoration(itemDecoration);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);


    }




}
