package com.vims.vimsapp.utilities.calendar;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.Date;

import vimsapp.R;


    public class CurrentDayDecorator implements DayViewDecorator {


        CalendarDay currentDay;

        private Drawable drawable;



        public CurrentDayDecorator(Activity context, Date date) {

            drawable = ContextCompat.getDrawable(context, R.drawable.vims_shape_calendar_selector);
            currentDay  = CalendarDay.from(date);

        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return day.equals(currentDay);
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.setSelectionDrawable(drawable);
        }
    }

