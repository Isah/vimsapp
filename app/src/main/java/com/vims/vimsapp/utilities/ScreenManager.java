package com.vims.vimsapp.utilities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.View;

import vimsapp.R;

public class ScreenManager {



    public static void setScreenOrientation(Activity activity){


       // boolean isTablet = activity.getResources().getBoolean(R.bool.tablet);


        // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

       // if(!isTablet){
           activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
       // }


    }


    private void windowFullScreen(){

        /*
        if(Build.VERSION.SDK_INT < 16) {
           getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        */
    }


    public static void setUpHiddenStatusBar(Activity activity){

       ActionBar actionBar = activity.getActionBar();

        View decorView = activity.getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        // Remember that you should never show the action bar if the
        // status bar is hidden, so hide that too if necessary.

        if(actionBar!=null) {
            actionBar.hide();
        }
    }



}
