package com.vims.vimsapp.utilities.animations;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.ContentProvider;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.format.DateUtils;
import android.transition.Slide;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import vimsapp.R;

import static android.provider.Settings.System.DATE_FORMAT;

public class AnimatorManager {



    public static boolean onStartNestedScroll(View child, View target, int nestedScrollAxes){

        boolean isScroll  = (nestedScrollAxes & View.SCROLL_AXIS_VERTICAL) != 0;

        return isScroll;

    }


    @TargetApi(21)
    public static void detailEnterTransition(Activity context){

        Slide slide = new Slide(Gravity.BOTTOM);
        slide.setInterpolator(AnimationUtils.loadInterpolator(context,android.R.interpolator.linear_out_slow_in));
        context.getWindow().setEnterTransition(slide);

    }


    public static void animateScrollForView(View view) {

       final int startScrollPos = view.getContext().getResources().getDimensionPixelOffset(R.dimen.init_scroll_up_distance);

        Animator animator = ObjectAnimator.ofInt(view, "scrollY", startScrollPos).setDuration(1500);
         animator.start();


    }


    public static void animateTextView(int initialValue, int finalValue, final TextView textview) {
        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(1500);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                textview.setText(valueAnimator.getAnimatedValue().toString());
            }
        });
        valueAnimator.start();

    }

    public static void animateCountDown(TextView tvDaysTogo, int days){

        long initialTime = DateUtils.DAY_IN_MILLIS * days + DateUtils.HOUR_IN_MILLIS * 9 +
                DateUtils.MINUTE_IN_MILLIS * 3 +
                DateUtils.SECOND_IN_MILLIS * 42;

        CountDownTimer countDownTimer = new CountDownTimer(initialTime, 1000) {
            StringBuilder time = new StringBuilder();

            @Override
            public void onTick(long millisUntilFinished) {
                time.setLength(0);
                if(millisUntilFinished > DateUtils.DAY_IN_MILLIS){
                    long count = millisUntilFinished /DateUtils.DAY_IN_MILLIS;
                    if(count >1){
                        time.append(count).append(" days ");
                    }else{
                        time.append(count).append("day");
                    }
                    millisUntilFinished %= DateUtils.DAY_IN_MILLIS;
                }

                time.append(DateUtils.formatElapsedTime(Math.round(millisUntilFinished /1000d)));
                tvDaysTogo.setText(time.toString());

            }

            @Override
            public void onFinish() {

                tvDaysTogo.setText(DateUtils.formatElapsedTime(0));


            }

        }.start();
    }




    public static void makeSceneTransition(Activity activity, View sharedView, String transitionName, Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            //shared Element Transitions
            // ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, p1, p2, p3);
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(activity, sharedView, transitionName);
            activity.startActivity(intent, options.toBundle());

            //Exit Transitions
            //Bundle bundle = ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle();
            //context.startActivity(intent, bundle);

        } else {
            activity.startActivity(intent);
        }
    }



    static Runnable  runnable;
    private static Handler handler = new Handler();
    public static  void countDownStart(String EVENT_DATE_TIME, TextView tv) {
      runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    handler.postDelayed(this, 1000);
                    SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
                    Date event_date = dateFormat.parse(EVENT_DATE_TIME);
                    Date current_date = new Date();

                    if (!current_date.after(event_date)) {
                        long diff = event_date.getTime() - current_date.getTime();
                        long Days = diff / (24 * 60 * 60 * 1000);
                        long Hours = diff / (60 * 60 * 1000) % 24;
                        long Minutes = diff / (60 * 1000) % 60;
                        long Seconds = diff / 1000 % 60;
                        //

                        String value = ""+String.format("%02d", Days)+":"+String.format("%02d", Hours)+":"+String.format("%02d", Minutes)+":"+String.format("%02d", Seconds);
                        tv.setText(value);

                        //tv_days.setText(String.format("%02d", Days));
                        //tv_hour.setText(String.format("%02d", Hours));
                        //tv_minute.setText(String.format("%02d", Minutes));
                        //tv_second.setText(String.format("%02d", Seconds));

                    } else {
                       // linear_layout_1.setVisibility(View.VISIBLE);
                        //linear_layout_2.setVisibility(View.GONE);
                        handler.removeCallbacks(runnable);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 0);
    }

    protected void onStop() {
        handler.removeCallbacks(runnable);
    }


}
