package com.vims.vimsapp.utilities.imager;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import vimsapp.R;

public class GetBitmapPlaceHolder {


    static Bitmap bitmap;
    public static Bitmap getBitmapPlaceholder(Context context){

          Resources res = context.getResources();
          int id = R.drawable.ic_person_light_white_24dp;
          bitmap = BitmapFactory.decodeResource(res,id);

          return bitmap;
    }


}
