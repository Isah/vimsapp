package com.vims.vimsapp.utilities.imager;

import android.arch.persistence.room.TypeConverter;
import android.graphics.Bitmap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class BitmapConverter {

    static Gson gson = new Gson();

        @TypeConverter
        public static Bitmap toBitmap(String bitmapString) {

            Type bitmapType = new TypeToken<Bitmap>() {}.getType();

            return bitmapString == null ? null : gson.fromJson(bitmapString,bitmapType);
        }

        @TypeConverter
        public static String toStringBitmap(Bitmap bitmap) {
            return bitmap == null ? null : gson.toJson(bitmap);
        }



}
