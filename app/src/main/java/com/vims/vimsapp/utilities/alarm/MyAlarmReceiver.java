package com.vims.vimsapp.utilities.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.vims.vimsapp.utilities.DateCalendarConverter;

import java.util.Date;

import vimsapp.R;




/**
 * Create a broadcast receiver that receives the broadcast intents from the
 * AlarmManager and reacts appropriately:
 *
 * A broadcast receiver receives the broadcast intent from the alarm manager
 * and delivers the notification.
 * **/
public class MyAlarmReceiver extends BroadcastReceiver {


    private static final int NOTIFICATION_ID = 0;
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        //throw new UnsupportedOperationException("Not yet implemented");
       showNotification(context);

    }


    Notification notification;
    private void showNotification(Context context) {

            String title = "Immunisation Due";
            String content = "Tap to record vaccinations";
            notification = new Notification(context, title, content, R.drawable.ic_vims_new_logo);
           // notification.createNotificationChannel();
            notification.deliverNotification(context);

    }

}
