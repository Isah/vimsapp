package com.vims.vimsapp.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import vimsapp.R;

public class DrawableManager {


    /** sets up drawables for appCompat low api devices*/
    public static void setUpDrawableStart(TextView textView,Drawable drawableResource) {

            textView.setCompoundDrawablesWithIntrinsicBounds(drawableResource,null,null,null);

    }

    public static void setUpDrawableStart(Button btnView, Drawable drawableResource) {

        btnView.setCompoundDrawablesWithIntrinsicBounds(drawableResource,null,null,null);

    }

    public static void setRoundedDrawable(Context context, int drawableResource, ImageView imageView) {

        Bitmap myBitmap = BitmapFactory.decodeResource(context.getResources(), drawableResource);
        RoundedBitmapDrawable rounded_drawable = RoundedBitmapDrawableFactory.create(context.getResources(), myBitmap);
        rounded_drawable.setCircular(true);
        imageView.setImageDrawable(rounded_drawable);

    }





}
