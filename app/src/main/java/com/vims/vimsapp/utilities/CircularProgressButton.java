package com.vims.vimsapp.utilities;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.widget.Button;

import vimsapp.R;

public class CircularProgressButton extends AppCompatButton {

    public CircularProgressButton(Context context) {
        super(context);
    }

    public enum ProgressState {
        PROGRESS, IDLE
    }

    public class LoadingButton extends AppCompatButton {
        public LoadingButton(Context context) {
            super(context);
            init(context);
        }

        public LoadingButton(Context context, AttributeSet attrs) {
            super(context, attrs);
            init(context);
        }

        public LoadingButton(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            init(context);
        }

        public LoadingButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
            super(context, attrs, defStyleAttr);
            init(context);
        }

        GradientDrawable mGradientDrawable;
        private void init(Context context) {

            mGradientDrawable = (GradientDrawable)
                    ContextCompat.getDrawable(context, R.drawable.morph_shape_default);

            setBackground(mGradientDrawable);

        }

    }
}