package com.vims.vimsapp.utilities.alarm;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;

import com.tapadoo.alerter.Alerter;

import vimsapp.R;

public class UIAlert {


    public enum AlertType{
      SUCCESS,ERROR,INFO

    }

    public static void showAlert(String Title, String content, Activity activity, AlertType alertType ) {


        Alerter.create(activity)
                .setTitle(Title)
                .setText(content)
                .setBackgroundColorRes(getColor(alertType)) // or setBackgroundColorInt(Color.CYAN)
                .show();
    }



    private static int getColor(AlertType alertType){

        int drawable;

        if(alertType == AlertType.ERROR){

            drawable = R.color.errorColor;
        }
        else if(alertType == AlertType.INFO){

            drawable = R.color.colorAccent;
        }else{

            drawable = R.color.colorPrimary;

        }

        return drawable;

    }


    private static int getDrawable(AlertType alertType){

        int drawable;

      if(alertType == AlertType.ERROR){

          drawable = R.drawable.ic_notifications_error_24dp;
      }
       else if(alertType == AlertType.INFO){

            drawable = R.drawable.ic_notifications_accent_24dp;
        }else{

          drawable = R.drawable.ic_notifications_primary_24dp;

        }

        return drawable;

    }






    public static void showSnackBarView(Snackbar snackbar){


        ////findViewById(R.id.cordinator_layout_main
        final View snackBarView = snackbar.getView();
       // final CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) snackBarView.getLayoutParams();
         final ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) snackBarView.getLayoutParams();
        int sideMargin = 16;
        int marginBottom = 72;

        params.setMargins(params.leftMargin + sideMargin, params.topMargin,
                params.rightMargin + sideMargin,
                params.bottomMargin + marginBottom);

        snackBarView.setLayoutParams(params);
        snackbar.show();


    }

}
