package com.vims.vimsapp.utilities;

import android.content.Context;
import android.os.Build;
import android.view.View;

import vimsapp.R;

public class SDKBuildManager {


    static boolean isKitkat = false;
    public static boolean isKitKat(){
        //extended image backdrop lollip 5, kitkat 4
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            //detailBinding.materialupProfileBackdrop.setImageBitmap(profilePic);
            isKitkat = true;
        }else {
            isKitkat = false;
        }
        return isKitkat;
    }



    static boolean isBuildM = false;
    public static boolean isBuildM(){
        //extended image backdrop lollip 5, kitkat 4
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            //detailBinding.materialupProfileBackdrop.setImageBitmap(profilePic);
            isBuildM = true;
        }else {
            isBuildM = false;
        }
        return isBuildM;
    }


    public static void trasnparentStatusBar(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            //getWindow().setStatusBarColor(context.getResources().getColor(R.color.black_trans80, null));

        }
    }


}
