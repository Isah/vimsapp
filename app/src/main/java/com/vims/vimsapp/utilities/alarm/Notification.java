package com.vims.vimsapp.utilities.alarm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;

import android.graphics.Color;
import android.support.v4.app.NotificationCompat;

import com.vims.vimsapp.MainActivity;

import vimsapp.R;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by LINCOLN on 12/14/2018.
 */

public class Notification {

    private Context context;
    private String content;
    private String title;
    private int icon=R.drawable.logo_icon_google_guideline_final_fill_128;
    private int mId=1;

    public Notification(Context context,String title,String content,int icon) {
        this.context=context;
        this.title=title;
        this.content=content;
        this.icon=icon;

       // createNotificationChannel();
    }

    NotificationManager mNotificationManager;

    public void showNotification() {

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this.context)
                        .setSmallIcon(this.icon)
                        .setContentTitle(this.title)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setAutoCancel(true)
                        .setContentText(this.content);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivity.class);

// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);

        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);


        mBuilder.setContentIntent(resultPendingIntent);
       mNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.

        mNotificationManager.notify(mId, mBuilder.build());
    }




    public void cancelNotifications(){
        mNotificationManager.cancelAll();

    }



    private static final String PRIMARY_CHANNEL_ID =
            "primary_notification_channel";
    public void createNotificationChannel() {

        // Create a notification manager object.
        mNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        // Notification channels are only available in OREO and higher.
        // So, add a check on SDK version.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            // Create the NotificationChannel with all the parameters.
            NotificationChannel notificationChannel = new NotificationChannel(PRIMARY_CHANNEL_ID,
                            "Stand up notification",
                            NotificationManager.IMPORTANCE_HIGH);

            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setDescription
                    ("Notifies every 15 minutes to stand up and walk");
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
    }

    private static final int NOTIFICATION_ID = 0;

    public void deliverNotification(Context context) {

        /**
         * Note: PendingIntent flags tell the system how to handle the situation
         * when multiple instances of the same PendingIntent are created (meaning
         * that the instances contain the same Intent). The FLAG_UPDATE_CURRENT flag
         * tells the system to use the old Intent but replace the extras data. Because
         * you don't have any extras in this Intent,
         * you can use the same PendingIntent over and over.
         */

        Intent contentIntent = new Intent(context, MainActivity.class);
        PendingIntent contentPendingIntent = PendingIntent.getActivity(context, NOTIFICATION_ID, contentIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this.context)
                .setSmallIcon(this.icon)
                .setContentTitle(this.title)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(contentPendingIntent)
                .setAutoCancel(true)
                .setContentText(this.content);



        mNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            // Create the NotificationChannel with all the parameters.
            NotificationChannel notificationChannel = new NotificationChannel(PRIMARY_CHANNEL_ID,
                    "Stand up notification",
                    NotificationManager.IMPORTANCE_HIGH);

            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setDescription
                    ("Notifies every 15 minutes to stand up and walk");
            mNotificationManager.createNotificationChannel(notificationChannel);
            mBuilder.setChannelId(PRIMARY_CHANNEL_ID);
        }




        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

    }


}

