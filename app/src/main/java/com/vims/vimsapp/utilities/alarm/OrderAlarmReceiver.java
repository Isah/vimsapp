package com.vims.vimsapp.utilities.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import vimsapp.R;

public class OrderAlarmReceiver extends BroadcastReceiver {



    private static final int NOTIFICATION_ID = 0;
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        //throw new UnsupportedOperationException("Not yet implemented");
        showNotification(context);

    }


    Notification notification;
    private void showNotification(Context context) {

        String title = "Order submission active";
        String content = "Tap to submit an order";
        notification = new Notification(context, title, content, R.drawable.ic_vims_new_logo);
        // notification.createNotificationChannel();
        notification.deliverNotification(context);

    }


}
