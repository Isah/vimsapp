package com.vims.vimsapp.utilities.pdf;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.vims.vimsapp.entities.child.Child;
import com.vims.vimsapp.entities.vaccinations.VaccinationEvent;
import com.vims.vimsapp.utilities.DateCalendarConverter;
import com.vims.vimsapp.view.vaccinations.schedule.VaccineSpecHandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

public class MyPdfCreator {

        private Context context;
        final private int REQUEST_CODE_ASK_PERMISSIONS = 111;
        private static final String TAG = "PdfCreatorActivity";




        private Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
        private  Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
        private  Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
        private  Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);


        public MyPdfCreator(Context context) {
            this.context = context;
        }



        private File pdfFile;

    public File getPdfFile() {
        return pdfFile;
    }

    public void createPdf(Child child, String pdfname, String hospiitalName) throws FileNotFoundException, DocumentException {

            String pdf_name_extension = ""+pdfname+".pdf";

            File docsFolder = new File(Environment.getExternalStorageDirectory() + "/Documents");
            if (!docsFolder.exists()) {
                docsFolder.mkdir();
                Log.i(TAG, "Created a new directory for PDF");
            }

            pdfFile = new File(docsFolder.getAbsolutePath(),pdf_name_extension);
            OutputStream output = new FileOutputStream(pdfFile);
            Document document = new Document();
            PdfWriter.getInstance(document, output);
            document.open();
            document.addAuthor("Isah Petr");
            document.addTitle("CHILD HEALTH CARD");
            document.addSubject("Immunisation protects your child against serious diseases " +
                    "folllow and complete the immunisation schedule belows");

            addContent(document,child,hospiitalName);

            document.close();

        }



            /*
            paragraph1 = new Paragraph("Subcategory 2", subFont);
            section = chapter.addSection(paragraph1);
            section.add(new Paragraph("Paragraph 1"));
            section.add(new Paragraph("Paragraph 2"));
            section.add(new Paragraph("Paragraph 3"));

            // add a list
            //createList(section);


            section.add(paragraph);
            */



        //CREATE PDF CONTENT
        private void addContent(Document document, Child child, String hospitalName) throws DocumentException {


            Anchor anchor = new Anchor("", catFont);
            // anchor.setName("BUSHENYI MEDICAL SERVICES");
            // Second parameter is the number of the chapter
            // Chapter chapter = new Chapter(new Paragraph(anchor), -1);

            Paragraph title = new Paragraph(hospitalName, catFont);
            title.setAlignment(Element.ALIGN_CENTER);
            //Section section = chapter.addSection(title);


            Paragraph subTitle = new Paragraph("CHILD HEALTH CARD", subFont);
            subTitle.setAlignment(Element.ALIGN_CENTER);
            //section.addSection(titleChapter1);

            document.add(title);
            document.add(subTitle);


            addEmptyLine(document,1);


            createProfileTable(document,child,hospitalName);


            addEmptyLine(document,2);

            // Second parameter is the number of the chapter
            // chapter = new Chapter(new Paragraph(anchor),0);
            Paragraph scheduleHeader = new Paragraph("CHILD SCHEDULE", subFont);
            scheduleHeader.setAlignment(Element.ALIGN_CENTER);
            //Section section_schedule = chapter.addSection(scheduleHeader);


            Paragraph scheduleSubTitle = new Paragraph("Immunisation protects your child against serious diseases, even when the schedule date is missed ,follow and complete the immunisation schedule below");
            scheduleSubTitle.setAlignment(Element.ALIGN_CENTER);
            // section_schedule.add(scheduleSubTitle);
            // now add all this to the document
            document.add(scheduleHeader);
            document.add(scheduleSubTitle);


            addEmptyLine(document,1);

            // add a table
            createScheduleTable(document,child.getVaccinationEvents());


            // Next section
            /*
            anchor = new Anchor("Second Chapter", catFont);
            anchor.setName("Second Chapter");

            // Second parameter is the number of the chapter
            chapter = new Chapter(new Paragraph(anchor), 1);

            paragraph1 = new Paragraph("Subcategory", subFont);
            section = chapter.addSection(paragraph1);
            section.add(new Paragraph("This is a very important message"));

            // now add all this to the document
            document.add(chapter);
            */

        }


        private void createList(Section section) {
            com.itextpdf.text.List list = new com.itextpdf.text.List(true, false, 10);
            list.add(new ListItem("First point"));
            list.add(new ListItem("Second point"));
            list.add(new ListItem("Third point"));
            section.add(list);
        }

        private void addEmptyLine(Document document, int numberOfParagraphs) {

            Paragraph paragraph = new Paragraph("", subFont);

            for (int i = 0; i < numberOfParagraphs; i++) {
                paragraph.add(new Paragraph(" "));

                try {

                    document.add(paragraph);


                }catch (DocumentException e){


                }
            }
        }



        private void createProfileTable(Document document, Child child, String hospitalName) throws BadElementException {
            PdfPTable table = new PdfPTable(6);

            // t.setBorderColor(BaseColor.GRAY);
            // table.setPadding(4);
            // table.setSpacing(4);
            // table.setBorderWidth(1);

            // table.setSplitRows(true);
            table.setWidthPercentage(100);



            //CELLS

            PdfPCell c1 = new PdfPCell(new Phrase("REG NO"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setPadding(8);
            table.addCell(c1);


            c1 = new PdfPCell(new Phrase("NAME"));
            c1.setPadding(8);
            c1.setFixedHeight(20);
            // c1.setBorderColor(new BaseColor(Color.RED));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("BIRTH DATE"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setPadding(8);
            c1.setFixedHeight(20);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("BIRTH WEIGHT(Kg)"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setPadding(8);
            c1.setFixedHeight(20);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("GENDER"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setPadding(8);

            table.addCell(c1);




            c1 = new PdfPCell(new Phrase("HEALTH UNIT"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setPadding(8);
            table.addCell(c1);

            table.setHeaderRows(1);

            String childName = ""+child.getFirstName()+" "+child.getLastName();
            String regno[] = child.getChildId().split("-");


            PdfPCell cell1 = new PdfPCell(new Phrase(""+regno[0]));
            cell1.setPadding(8);
            PdfPCell cell2 = new PdfPCell(new Phrase(""+childName));
            cell2.setPadding(8);
            PdfPCell cell3 = new PdfPCell(new Phrase(""+ child.getDateOfBirth()));
            cell3.setPadding(8);
            PdfPCell cell4 = new PdfPCell(new Phrase(""+child.getWeight()));
            cell4.setPadding(8);
            PdfPCell cell5 = new PdfPCell(new Phrase(""+child.getGender()));
            cell5.setPadding(8);
            PdfPCell cell6 = new PdfPCell(new Phrase(hospitalName));
            cell6.setPadding(8);

            table.addCell(cell1);
            table.addCell(cell2);
            table.addCell(cell3);
            table.addCell(cell4);
            table.addCell(cell5);
            table.addCell(cell6);



            /*
            table.addCell(""+childName);
            table.addCell(""+DateCalendarConverter.dateToStringWithoutTime(child.getDateOfBirth()));
            table.addCell(""+child.getWeight());
            table.addCell(""+child.getGender());
            table.addCell(""+child.getId());
            table.addCell("Bushenyi Hospital");
            */

            try {

                document.add(table);

            }catch (DocumentException d) {

            }

        }

        private void createScheduleTable(Document document, List<VaccinationEvent> vaccinationCard) throws BadElementException {
            PdfPTable table = new PdfPTable(5);


            // t.setBorderColor(BaseColor.GRAY);
            // table.setPadding(4);
            // table.setSpacing(4);
            // table.setBorderWidth(1);

            table.setSplitRows(true);
            table.setWidthPercentage(100);

            //CELLS
            PdfPCell c1 = new PdfPCell(new Phrase("VACCINE"));
            c1.setPadding(8);
            // c1.setBorderColor(new BaseColor(Color.RED));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("PROTECTS AGAINST"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setPadding(8);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("MODE OF ADMINISTRATION"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setPadding(8);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("DATE SCHEDULED"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setPadding(8);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("DATE GIVEN"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setPadding(8);
            table.addCell(c1);
            table.setHeaderRows(1);




            for(VaccinationEvent vaccinationEvent: vaccinationCard){

                String dateSch = DateCalendarConverter.dateToStringWithoutTime(new Date(vaccinationEvent.getScheduledDate()));

                PdfPCell cell1 = new PdfPCell(new Phrase(""+vaccinationEvent.getVaccineName()));
                cell1.setPadding(8);
                PdfPCell cell2 = new PdfPCell(new Phrase(""+VaccineSpecHandler.getVaccineDesc(VaccineSpecHandler.SPEC_TYPE_PROTECTS,vaccinationEvent.getVaccineName())));
                cell2.setPadding(8);
                PdfPCell cell3 = new PdfPCell(new Phrase(""+ VaccineSpecHandler.getVaccineDesc(VaccineSpecHandler.SPEC_TYPE_MODE,vaccinationEvent.getVaccineName())));
                cell3.setPadding(8);
                PdfPCell cell4 = new PdfPCell(new Phrase(""+ dateSch));
                cell4.setPadding(8);
                PdfPCell cell5 = new PdfPCell(new Phrase(""));
                cell5.setPadding(8);


                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
                table.addCell(cell4);
                table.addCell(cell5);

            }

            try {

                document.add(table);

            }catch (DocumentException d) {

            }

        }










}
