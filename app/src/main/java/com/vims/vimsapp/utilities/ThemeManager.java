package com.vims.vimsapp.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;


import com.vims.vimsapp.entities.vaccinations.VaccinationStatus;

import vimsapp.R;

public class ThemeManager implements SharedPreferences.OnSharedPreferenceChangeListener{


    public static String  DEFAULT_THEME_ID = "R.style.AppTheme_Dark";
    public static String  LIGHT_THEME_ID = "R.style.AppTheme_Light";
    public static String  LIGHT_THEME_ID_BLUE = "blue_light";



    static String theme_id = "";

    //static int background_drawable = 0;

    static Drawable background_drawable;
    static Drawable background_drawable_default;

    static int background_drawable_done = 0;
    static int graphSpinnerDrawable = 0;

    static int drawable_gradient =0;

    static int textcolor = 0;

    static Context mycontext;


    public static void setContext(Context context){
        PreferenceHandler preferenceHandler = PreferenceHandler.getInstance(context);
        mycontext = context;
        theme_id = preferenceHandler.getPref(context.getResources().getString(R.string.pref_key_theme));
      //  DEFAULT_THEME_ID = "R.style.AppTheme_Dark";
      //  LIGHT_THEME_ID = "R.style.AppTheme_Light";

    }



    static int buttonColor  =0;

    public static int getWizardButtonColor(){

        if(theme_id.equals(DEFAULT_THEME_ID)){
            buttonColor = R.color.windowBackgroundDarkTheme;

        }else  if(theme_id.equals(LIGHT_THEME_ID)){
            buttonColor = R.color.rowForeground;

        }

        return buttonColor;

    }


    public static Drawable getBackgroundDrawable(VaccinationStatus vaccinationStatus){

        if(vaccinationStatus == VaccinationStatus.DONE){

            background_drawable = mycontext.getResources().getDrawable(R.drawable.shape_rectangle_rounded_shade_outline_inside_blue_light);

        }else  if(vaccinationStatus == VaccinationStatus.FAILED){

            background_drawable = mycontext.getResources().getDrawable(R.drawable.shape_rectangle_rounded_shade_outline_inside_red_light);
        }
        else {
           // background_drawable = mycontext.getResources().getDrawable(R.drawable.shape_roundeded_rectangle_due);
            background_drawable = mycontext.getResources().getDrawable(R.drawable.shape_rectangle_rounded_shade_outline_inside_green_light);

        }

        return background_drawable;

    }

   static Drawable background_drawable_events;
    public static Drawable getBackgroundDrawableEvents(VaccinationStatus vaccinationStatus){

        if(vaccinationStatus == VaccinationStatus.DUE){

            background_drawable_events = mycontext.getResources().getDrawable(R.drawable.shape_curve_semicirce_green);

        }else  if(vaccinationStatus == VaccinationStatus.FAILED){

            background_drawable_events = mycontext.getResources().getDrawable(R.drawable.shape_curve_semicirce_red);

        }
        else  if(vaccinationStatus == VaccinationStatus.DONE){

            background_drawable_events = mycontext.getResources().getDrawable(R.drawable.shape_curve_semicirce_blue);

        }
        else {
            // background_drawable = mycontext.getResources().getDrawable(R.drawable.shape_roundeded_rectangle_due);
            background_drawable_events = mycontext.getResources().getDrawable(R.drawable.shape_curve_semicirce_grey);


        }

        return background_drawable_events;

    }


    public static Drawable getBackgroundDrawable(){

        if(theme_id.equals(DEFAULT_THEME_ID)){

            background_drawable_default = mycontext.getResources().getDrawable(R.drawable.shape_roundeded_rectangle_upcoming_dark);


        }else{

            background_drawable_default = mycontext.getResources().getDrawable(R.drawable.shape_roundeded_rectangle_upcoming_light_ultra);

        }

        return background_drawable_default;

    }



    public static int getTextColorAdministerBtn(){

        if(theme_id.equals(DEFAULT_THEME_ID)){

            textcolor = R.color.blue_grey_300;

        }else{

            textcolor = R.color.grey_pallete_400;

        }

        return textcolor;

    }

    public static int getLinechartDrawableGradient(){

        if(theme_id.equals(DEFAULT_THEME_ID)){

            drawable_gradient = R.drawable.scrim_gradient_shape_dark;

        }else{
            drawable_gradient = R.drawable.scrim_gradient_shape_light;

        }

        return drawable_gradient;

    }



    public static int getSpinnerDrawable(){

        if(theme_id.equals(DEFAULT_THEME_ID)){

            graphSpinnerDrawable = R.drawable.shape_spinner_background_dark;

        }else  if(theme_id.equals(LIGHT_THEME_ID)){
            graphSpinnerDrawable  = R.drawable.shape_spinner_background_light;

        }

        return graphSpinnerDrawable;

    }


    public final static int VACCINATION_UPCOMING = 0;
    public final static int VACCINATION_MISSED = 1;
    public final static int VACCINATION_DONE = 2;

    static Drawable progressDrawable;


    public static Drawable getProgressBarDrawable(int vaccinationType){

        if(theme_id.equals(DEFAULT_THEME_ID)){

            if(vaccinationType == VACCINATION_UPCOMING){

                progressDrawable = mycontext.getResources().getDrawable(R.drawable.shape_progress_indicator_horizontal_thin_dark_meduim);

            }else if(vaccinationType == VACCINATION_MISSED){

                progressDrawable = mycontext.getResources().getDrawable(R.drawable.shape_progress_indicator_horizontal_thin_dark_low);
            }
            else if(vaccinationType == VACCINATION_DONE){

                progressDrawable = mycontext.getResources().getDrawable(R.drawable.shape_progress_indicator_horizontal_thin_dark_high);
            }


        }else{
            if(vaccinationType == VACCINATION_UPCOMING){

                progressDrawable = mycontext.getResources().getDrawable(R.drawable.shape_progress_indicator_horizontal_thin_light_meduim);
            }else if(vaccinationType == VACCINATION_MISSED){

                progressDrawable = mycontext.getResources().getDrawable(R.drawable.shape_progress_indicator_horizontal_thin_light_low);
            }
            else if(vaccinationType == VACCINATION_DONE){

                progressDrawable = mycontext.getResources().getDrawable(R.drawable.shape_progress_indicator_horizontal_thin_light_high);
            }


        }

        return progressDrawable;


    }





    public  static int bar_labels_color = 0;
    public  static int bar_color1 = 0;
    public  static int bar_color2 = 0;


    public  static int yaxis1_scale_text_Color = 0;
    public  static int yaxis2_scale_text_Color = 0;
    public  static int yaxis2_grid_Color = 0;
    public  static int yaxis1_grid_Color = 0;


    public  static int xaxis_grid_color = 0;
    public  static int xaxis_label_text_color = 0;
    public  static int xaxis_value_text_color = 0;

    public  static int drawale_gradient = 0;

    public  static int linechart_dataset_color = 0;


    public static void setUpGraphColorTheme(String my_theme_id){
        linechart_dataset_color = R.color.colorPrimaryTint3;

        if(my_theme_id.equals(DEFAULT_THEME_ID)){

            drawale_gradient = R.drawable.scrim_gradient_shape_dark;

            // scheduleHomeBinding.cardViewGraph.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
           //  bar_labels_color = mycontext.getResources().getColor(R.color.white);
           // bar_color1 = mycontext.getResources().getColor(R.color.colorPrimary);//colorPrimary / colorPrimaryTint0
           // bar_color2 = mycontext.getResources().getColor(R.color.blue_grey_800);//blue grey 700 /  colorPrimaryDark

           // yaxis1_scale_text_Color = mycontext.getResources().getColor(R.color.rowBackgroundDarkTheme); // colorPrimary //blue_grey_900)
           // yaxis2_scale_text_Color = mycontext.getResources().getColor(R.color.rowBackgroundDarkTheme);
           // yaxis2_grid_Color = mycontext.getResources().getColor(R.color.blue_grey_800);//blue grey 800 / colorPrimaryDark

           // xaxis_grid_color = mycontext.getResources().getColor(R.color.colorPrimaryDark);
           // xaxis_label_text_color = mycontext.getResources().getColor(R.color.grey_pallete_400);//grey_pallete_400 / white
           // xaxis_value_text_color = mycontext.getResources().getColor(R.color.grey_pallete_400);//grey_pallete_400 / white

        }

        else if(my_theme_id.equals(LIGHT_THEME_ID_BLUE)){

            drawale_gradient = R.drawable.scrim_gradient_shape_light_blue;

            //scheduleHomeBinding.cardViewGraph.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
            bar_labels_color = mycontext.getResources().getColor(R.color.white);
            bar_color1 = mycontext.getResources().getColor(R.color.white);//colorPrimary / colorPrimaryTint0
            bar_color2 = mycontext.getResources().getColor(R.color.colorPrimaryTint2);//blue grey 700 /  colorPrimaryDark

            yaxis1_scale_text_Color = mycontext.getResources().getColor(R.color.colorPrimaryTint1); // colorPrimary //blue_grey_900)
            yaxis2_scale_text_Color = mycontext.getResources().getColor(R.color.colorPrimaryTint1);
            yaxis2_grid_Color = mycontext.getResources().getColor(R.color.colorPrimaryTint3);//blue grey 800 / colorPrimaryDark

            xaxis_grid_color = mycontext.getResources().getColor(R.color.colorPrimaryTint3);
            xaxis_label_text_color = mycontext.getResources().getColor(R.color.colorPrimaryTint1);//grey_pallete_400 / white
            xaxis_value_text_color = mycontext.getResources().getColor(R.color.white);//grey_pallete_400 / white

        }
        else{
            drawale_gradient = R.drawable.scrim_gradient_shape_light;


          //  bar_labels_color = mycontext.getResources().getColor(R.color.textColorPrimary);//tcp   / white
          //  bar_color1 = mycontext.getResources().getColor(R.color.colorPrimary);   // colorPrimaryLight
          //  bar_color2 = mycontext.getResources().getColor(R.color.grey_pallete_200);    // colorPrimaryDark

          //  yaxis1_scale_text_Color = mycontext.getResources().getColor(R.color.textColorPrimary);// tcp / cp
          //  yaxis2_scale_text_Color = mycontext.getResources().getColor(R.color.textColorPrimary);//tcp / cp

          //  yaxis2_grid_Color = mycontext.getResources().getColor(R.color.grey_pallete_300);
          //  yaxis1_grid_Color = mycontext.getResources().getColor(R.color.grey_pallete_300);

          //  xaxis_grid_color = mycontext.getResources().getColor(R.color.textColorPrimary);
          //  xaxis_label_text_color = mycontext.getResources().getColor(R.color.ms_black);//tcp // tcpd
         //   xaxis_value_text_color = mycontext.getResources().getColor(R.color.ms_black);//tcp // tcpd

        }


    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        //check what preference changed
        if(key.equals(mycontext.getString(R.string.pref_key_theme))){
            String themeId =  sharedPreferences.getString(key,"");
            if(themeId.equals("R.style.AppTheme_Light")){

                theme_id = LIGHT_THEME_ID;

            }else{//Dark

                theme_id = DEFAULT_THEME_ID;
            }
            //preferenceHandler.putPref(sharedPreferences.getString(key,""));
        }




    }
}
