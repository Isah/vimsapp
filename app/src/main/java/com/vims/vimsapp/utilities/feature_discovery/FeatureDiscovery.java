package com.vims.vimsapp.utilities.feature_discovery;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.content.res.AppCompatResources;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.support.v7.widget.Toolbar;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.vims.vimsapp.utilities.MainThreadExecutor;
import com.vims.vimsapp.utilities.PreferenceHandler;
import com.vims.vimsapp.view.MyFonts;

import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;
import vimsapp.R;

import static java.security.AccessController.getContext;

public class FeatureDiscovery {


    static PreferenceHandler preferenceHandler;

    public static void tapTarget(Activity activity, View targetView, String prefReference, String primaryText, String secondaryText){
        //TAP TARGET
        //when to show it
        Runnable  runnable = new Runnable() {
            @Override
            public void run() {

                preferenceHandler = PreferenceHandler.getInstance(activity.getApplicationContext());
                int firsttime = preferenceHandler.getIntValue(prefReference);
                if(firsttime != 1) {
                    //findViewById(R.id.action_hospital_schedule)
                    new MaterialTapTargetPrompt.Builder(activity, R.style.AppTheme)
                            .setTarget(targetView)
                            .setPrimaryText(primaryText)
                            .setSecondaryText(secondaryText)
                            .setAnimationInterpolator(new FastOutSlowInInterpolator())
                            .setBackgroundColour(activity.getApplicationContext().getResources().getColor(R.color.colorPrimary))
                            .setOnHidePromptListener(new MaterialTapTargetPrompt.OnHidePromptListener() {
                                @Override
                                public void onHidePrompt(MotionEvent event, boolean tappedTarget) {
                                    //TODO: Store in SharedPrefs so you don't show this prompt again.
                                    if (tappedTarget)
                                    {
                                        //Do something such as storing a value so that this prompt is never shown again
                                        //preferenceHandler.setIntValue(prefReference, 1);
                                    }
                                }

                                @Override
                                public void onHidePromptComplete() {
                                }
                            })
                            .show();
                }



            }
        };
        new MainThreadExecutor(500).execute(runnable);


    }

    public static void tapTargetOverflowMenu(Activity activity, int targetView, String prefReference, String primaryText, String secondaryText){
        //TAP TARGET
        //when to show it
        Runnable  runnable = new Runnable() {
            @Override
            public void run() {

                preferenceHandler = PreferenceHandler.getInstance(activity.getApplicationContext());
                int firsttime = preferenceHandler.getIntValue(prefReference);
                if(firsttime != 1) {
                    //findViewById(R.id.action_hospital_schedule)
                    new MaterialTapTargetPrompt.Builder(activity, R.style.AppTheme)
                            .setTarget(targetView)
                            .setPrimaryText(primaryText)
                            .setSecondaryText(secondaryText)
                            .setAnimationInterpolator(new FastOutSlowInInterpolator())
                            .setBackgroundColour(activity.getApplicationContext().getResources().getColor(R.color.colorPrimary))
                            .setOnHidePromptListener(new MaterialTapTargetPrompt.OnHidePromptListener() {
                                @Override
                                public void onHidePrompt(MotionEvent event, boolean tappedTarget) {
                                    //TODO: Store in SharedPrefs so you don't show this prompt again.
                                    if (tappedTarget)
                                    {
                                        //Do something such as storing a value so that this prompt is never shown again
                                        preferenceHandler.setIntValue(prefReference, 1);
                                    }
                                }

                                @Override
                                public void onHidePromptComplete() {
                                }
                            })
                            .show();
                }



            }
        };
        new MainThreadExecutor(500).execute(runnable);


    }




    public static void tapTargetKeepSafe(Activity activity,View targetView,Drawable drawableIcon, String preferenceText, String title, String subTitle){



        Context context = activity.getApplicationContext();

        Runnable  runnable = new Runnable() {
            @Override
            public void run() {


        preferenceHandler = PreferenceHandler.getInstance(activity.getApplicationContext());

        preferenceHandler.setIntValue("hospital_schedule_first_time",0);

        int firsttime = preferenceHandler.getIntValue(preferenceText);
        if(firsttime != 1) {
            TapTargetView.showFor(activity, TapTarget.forView(targetView, title, subTitle)
                            //All options below are optional
                            .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                            .outerCircleAlpha(0.96f)            // Specify the alpha amount for the outer circle
                            .targetCircleColor(R.color.white)   // Specify a color for the target circle
                            .titleTextSize(24)                  // Specify the size (in sp) of the title text
                            .titleTextColor(R.color.white)      // Specify the color of the title text
                            .descriptionTextSize(16)            // Specify the size (in sp) of the description text
                            .descriptionTextColor(R.color.colorPrimaryTint0)  // Specify the color of the description text
                            .textColor(R.color.white)            // Specify a color for both the title and description text
                            .textTypeface(MyFonts.getTypeFace(activity.getApplicationContext()))  // Specify a typeface for the text
                            .dimColor(R.color.ms_black)            // If set, will dim behind the view with 30% opacity of the given color
                            .drawShadow(true)                   // Whether to draw a drop shadow or not
                            .cancelable(true)                  // Whether tapping outside the outer circle dismisses the view
                            .tintTarget(true)                   // Whether to tint the target view's color
                            .transparentTarget(false)           // Specify whether the target is transparent (displays the content underneath)
                            .icon(drawableIcon)// Specify a custom drawable to draw as the target
                            .targetRadius(60),                  // Specify the target radius (in dp)

                    new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels

                        @Override
                        public void onTargetDismissed(TapTargetView view, boolean userInitiated) {
                            super.onTargetDismissed(view, userInitiated);
                            preferenceHandler.setIntValue(preferenceText, 1);

                        }
                    });

        }

            }
        };
        new MainThreadExecutor(1000).execute(runnable);


    }


    public static void tapTargetKeepSafeOverFlowMenu(Activity activity, Toolbar targetToolBar, int menuItemId, String title, String subTitle){

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

        Context context = activity.getApplicationContext();
       // Drawable drawableIcon = context.getResources().getDrawable(R.drawable.ic_menu_schedule_black_24dp);


        TapTargetView.showFor(activity,
                TapTarget.forToolbarMenuItem(targetToolBar,menuItemId,title,subTitle)
                        //All options below are optional
                        .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                        .outerCircleAlpha(0.96f)            // Specify the alpha amount for the outer circle
                        .targetCircleColor(R.color.white)   // Specify a color for the target circle
                        .titleTextSize(24)                  // Specify the size (in sp) of the title text
                        .titleTextColor(R.color.white)      // Specify the color of the title text
                        .descriptionTextSize(16)            // Specify the size (in sp) of the description text
                        .descriptionTextColor(R.color.colorPrimaryTint0)  // Specify the color of the description text
                        .textColor(R.color.white)            // Specify a color for both the title and description text
                        .textTypeface(MyFonts.getTypeFace(activity.getApplicationContext()))  // Specify a typeface for the text
                        .dimColor(R.color.white)            // If set, will dim behind the view with 30% opacity of the given color
                        .drawShadow(true)                   // Whether to draw a drop shadow or not
                        .cancelable(true)                  // Whether tapping outside the outer circle dismisses the view
                        .tintTarget(true)                   // Whether to tint the target view's color
                        .transparentTarget(true)           // Specify whether the target is transparent (displays the content underneath)
                        //.icon(drawableIcon)// Specify a custom drawable to draw as the target
                        .targetRadius(50),                  // Specify the target radius (in dp)
                new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);      // This call is optional
                        // doSomething();
                    }

                });

            }
        };
        new MainThreadExecutor(1000).execute(runnable);


    }


}
