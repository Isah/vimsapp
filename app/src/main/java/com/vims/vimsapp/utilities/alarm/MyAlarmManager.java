package com.vims.vimsapp.utilities.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import static android.content.Context.ALARM_SERVICE;

public class MyAlarmManager {


    private static MyAlarmManager INSTANCE = null;



    public static int ALARM_TYPE_IMMUNISE = 1;
    public static int ALARM_TYPE_ORDER = 0;

    private int mAlarmType;


    private  Context mContext;

    private MyAlarmManager(Context context) {

        this.mContext = context;

    }

    public static MyAlarmManager getInstance(Context context) {

        if (INSTANCE == null) {
            INSTANCE = new MyAlarmManager(context);
        }
        return INSTANCE;
    }


    private AlarmManager alarmManager;
    private PendingIntent notifyPendingIntent;

    public void startAlarm(Date nextDate ,int alarmType) {

        Intent notifyIntent;

        if(alarmType == ALARM_TYPE_IMMUNISE) {
           notifyIntent = new Intent(mContext, MyAlarmReceiver.class);
        }else {
            notifyIntent = new Intent(mContext, OrderAlarmReceiver.class);
        }



        notifyPendingIntent = PendingIntent.getBroadcast(mContext, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager = (AlarmManager) mContext.getSystemService(ALARM_SERVICE);

        /**
         * a`n inexact, repeating alarm that uses elapsed time and wakes the device up if it is asleep.
         * The real-time clock is not relevant here,
         * because you want to deliver the notification every 15 minutes.
         * You use the setInexactRepeating() alarm because it is more resource-efficient to use
         * inexact
         * timing, which lets the system bundle alarms from different apps together
         */

        // long repeatInterval = AlarmManager.INTERVAL_FIFTEEN_MINUTES;
        // long triggerTime = SystemClock.elapsedRealtime() + repeatInterval;


        Calendar cal = Calendar.getInstance();
        cal.setTime(nextDate);


        int vm = cal.get(Calendar.MONTH);
        int vd = cal.get(Calendar.DAY_OF_MONTH);
        int vy = cal.get(Calendar.YEAR);

        cal.set(Calendar.HOUR_OF_DAY, 9);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.AM_PM, Calendar.AM);


        //If the Toggle is turned on, set the repeating alarm with a 15 minute interval
        if (alarmManager != null) {
            // alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, triggerTime, repeatInterval, notifyPendingIntent);
            alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), notifyPendingIntent);

        }else{

        }


        // ToggleButton alarmToggle =  scheduleHomeBinding.switch1;


    }


    public void stopAlarm() {

        if (alarmManager != null) {
            alarmManager.cancel(notifyPendingIntent);
            //new Notification().cancelNotifications();
        }

    }
}
