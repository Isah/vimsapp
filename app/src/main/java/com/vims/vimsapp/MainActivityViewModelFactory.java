package com.vims.vimsapp;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.vims.vimsapp.managehospitalschedule.GenerateHospitalSchedule;


/**
 * Created by root on 6/3/18.
 */
public class MainActivityViewModelFactory implements ViewModelProvider.Factory{


    private final GenerateHospitalSchedule generateHospitalSchedule;

    public MainActivityViewModelFactory(GenerateHospitalSchedule generateHospitalSchedule) {
        this.generateHospitalSchedule = generateHospitalSchedule;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if(modelClass.isAssignableFrom(MainActivityViewModel.class)){

            return (T) new MainActivityViewModel(generateHospitalSchedule);

        }
        else{

            throw new IllegalArgumentException("View Model not found");
        }

    }




}
