package com.vims.vimsapp.viewmodel_tests;


import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;

import com.vims.vimsapp.UseCase;
import com.vims.vimsapp.domain.patients.entity.Caretaker;
import com.vims.vimsapp.domain.patients.interactor.vaccinated.SearchPatient;
import com.vims.vimsapp.view.patients.search.SearchCaretakerActivity;
import com.vims.vimsapp.view.patients.search.SearchCaretakerFragmentViewModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SearchCaretakerViewModelTest {

    //msg limit = 256 chars
    //When Testing the UI you verify certain UI methosds were called apropriately
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();


    @Mock
    ViewModelProvider viewModelProvider;

    @Mock
    SearchPatient searchCaretaker;


    @Mock
    Observer<CharSequence> searchTextObserver;


    @Mock
    Observer<String> noDataObserver;

    @Mock
    Observer<List<Caretaker>> caretakerSearchedListObsrever;

    @Captor
    ArgumentCaptor<UseCase.OutputBoundary<SearchPatient.ResponseValue>> searchBoundaryArgumentCaptor;





    @InjectMocks
    SearchCaretakerFragmentViewModel caretakerFragmentViewModel;


    SearchCaretakerActivity searchCaretakerActivity;


    @Before
    public void setUp(){

        MockitoAnnotations.initMocks(this);

        searchCaretakerActivity = new SearchCaretakerActivity();

        searchCaretakerActivity.setViewModelProvider(viewModelProvider);

        when(viewModelProvider.get(any(Class.class))).thenReturn(caretakerFragmentViewModel);


    }



    @Test
    public void verify_search_with_valid_data_updates_ui(){


        caretakerFragmentViewModel.setmSearchText("Hello");
        caretakerFragmentViewModel.getmSearchText().observeForever(searchTextObserver);

        //1. when caretakers are requested
        caretakerFragmentViewModel.getSearchedCaretakers();

        //2. make sure the use case executed
        verify(searchCaretaker).execute(any(SearchPatient.RequestValues.class), searchBoundaryArgumentCaptor.capture());

        //3. capture and simulate the callback

        List<Caretaker> dummyCaretakers = new ArrayList<>();
        dummyCaretakers.add(new Caretaker());

        //4. the callback will trigger update of the UI
        searchBoundaryArgumentCaptor.getValue().onSuccess(new SearchPatient.ResponseValue(dummyCaretakers));


        //5. verify update of the UI
        caretakerFragmentViewModel.getSearchedCaretakers().observeForever(caretakerSearchedListObsrever);
        verify(caretakerSearchedListObsrever).onChanged(dummyCaretakers);


    }



    @Test
    public void verify_search_with_invalid_data_shows_error_ui(){


        caretakerFragmentViewModel.setmSearchText("");
        caretakerFragmentViewModel.getmSearchText().observeForever(searchTextObserver);

        //1. when caretakers are requested
        caretakerFragmentViewModel.getSearchedCaretakers();

        //2. make sure the use case executed
        verify(searchCaretaker).execute(any(SearchPatient.RequestValues.class), searchBoundaryArgumentCaptor.capture());

        //3. capture and simulate the callback

        List<Caretaker> dummyCaretakers = new ArrayList<>();
        dummyCaretakers.add(new Caretaker());

        //4. the callback will trigger update of the UI with error
        searchBoundaryArgumentCaptor.getValue().onError("No Caretakers");


        //5. verify update of the UI never
        caretakerFragmentViewModel.getSearchedCaretakers().observeForever(caretakerSearchedListObsrever);
        verify(caretakerSearchedListObsrever, never()).onChanged(dummyCaretakers);


        caretakerFragmentViewModel.getmNoDataError().observeForever(noDataObserver);
        verify(noDataObserver).onChanged(anyString());




    }








}
