package com.vims.vimsapp.viewmodel_tests;


import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;

import com.vims.vimsapp.UseCase;

import com.vims.vimsapp.manageorders.RequestOrder;
import com.vims.vimsapp.manageorders.VaccineOrder;
import com.vims.vimsapp.manageorders.VaccinePlan;
import com.vims.vimsapp.view.vaccinations.order.ViewOrderActivity;
import com.vims.vimsapp.view.vaccinations.viewmodel.CreateOrderViewModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CreateOrderViewModelTest {



    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();


    @Mock
    ViewModelProvider viewModelProvider;

    @Mock
    RequestOrder requestOrder;



    //CREATE MOCKS OF OBSERVERS - to check whether methods were executed from them
    @Mock
    Observer<String> userNameObserver;

    @Mock
    Observer<String> hospitalIdObserver;

    @Mock
    Observer<Boolean> isSuccessObserver;

    @Mock
    Observer<String> responseMessageObserver;

    @Mock
    Observer<List<VaccinePlan>> vaccinePlanListObserver;


    @Captor
    ArgumentCaptor<UseCase.OutputBoundary<RequestOrder.ResponseValue>> requestOrderArgumentCaptor;



    //INJECT ALL THE MOCKS IN THE VIEWMODEL
    @InjectMocks
    CreateOrderViewModel createOrderViewModel;


    ViewOrderActivity viewOrderActivity;




    @Before
    public void setUp(){

          MockitoAnnotations.initMocks(this);

         //  Looper.prepare();
         // viewOrderActivity = new ViewOrderActivity();
         // viewOrderActivity.setViewModelProvider(viewModelProvider);

        when(viewModelProvider.get(any(Class.class))).thenReturn(createOrderViewModel);


    }



    @Test
    public void verify_create_order_with_valid_data_shows_success(){


        createOrderViewModel.setUserName("Hello");
        createOrderViewModel.getUserName().observeForever(userNameObserver);

        createOrderViewModel.setHospitalId("hospital");
        createOrderViewModel.getHospitalId().observeForever(hospitalIdObserver);

        VaccineOrder dummyVaccineOrder = new VaccineOrder();
        VaccinePlan vaccinePlan = new VaccinePlan();
        vaccinePlan.setQuantityAtHand(5);
        vaccinePlan.setQuantityRequired(6);
        vaccinePlan.setVaccineName("Polio");

        ArrayList<VaccinePlan> plans = new ArrayList<>();
        plans.add(vaccinePlan);

        dummyVaccineOrder.setUserName("Peter");
        dummyVaccineOrder.setOrderStatus(VaccineOrder.OrderStatus.SENT);
        dummyVaccineOrder.setVaccinePlans(plans);

        createOrderViewModel.setMutableVaccinePlanList(plans);

        //1. when an order has been sent
        createOrderViewModel.executeOrderRequest(0);

        //2. make sure the use case executed
        verify(requestOrder).execute(any(RequestOrder.RequestValues.class), requestOrderArgumentCaptor.capture());
        //3. capture and simulate the callback   //we assume its success




        //4. the callback will trigger update of the UI show success message
        requestOrderArgumentCaptor.getValue().onSuccess(new RequestOrder.ResponseValue(dummyVaccineOrder));


        //5. verify update of the UI

        createOrderViewModel.getIsSuccess().observeForever(isSuccessObserver);
        verify(isSuccessObserver).onChanged(true);

        createOrderViewModel.getResponseMessage().observeForever(responseMessageObserver);
        verify(responseMessageObserver).onChanged(any());


    }






}
