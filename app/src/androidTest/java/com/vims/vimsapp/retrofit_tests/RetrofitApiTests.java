package com.vims.vimsapp.retrofit_tests;


import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.vims.vimsapp.domain.vaccinations.data.remote.VaccinationsApi;
import com.vims.vimsapp.domain.vaccinations.data.remote.VaccinationsApiClient;
import com.vims.vimsapp.domain.vaccinations.entity.HospitalImmunisationEvent;
import com.vims.vimsapp.domain.vaccinations.entity.VaccinationCard;
import com.vims.vimsapp.domain.vaccinations.entity.VaccinationEvent;
import com.vims.vimsapp.domain.vaccinations.entity.VaccinationStatus;
import com.vims.vimsapp.utilities.DateCalendarConverter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


@RunWith(AndroidJUnit4.class)
public class RetrofitApiTests {



    VaccinationsApi vaccinationsApi;

    @Before
    public void createDb(){

        Context context = InstrumentationRegistry.getTargetContext();

        vaccinationsApi = VaccinationsApiClient.getRetrofitInstance().create(VaccinationsApi.class);

    }


    String res = "Api";
    boolean isApiConnected = false;

    @Test
    public void sendsCardToApi(){

        final VaccinationCard vaccinationCard = new VaccinationCard();
        vaccinationCard.setDateCreated(new Date());
        vaccinationCard.setChildId(UUID.randomUUID().toString());


        VaccinationEvent vaccinationEvent = new VaccinationEvent();
        String dateVacc = DateCalendarConverter.dateToStringWithoutTime(new Date());

        vaccinationEvent.setVaccinationDate(dateVacc);
        vaccinationEvent.setVaccinationStatus(VaccinationStatus.DONE);
        vaccinationEvent.setVaccineName("Polio 0");

        ArrayList<VaccinationEvent> vaccinationEvents = new ArrayList<>();
        vaccinationEvents.add(vaccinationEvent);
        vaccinationCard.setVaccinationEvents(vaccinationEvents);

       //vaccinationsApi.saveChildVaccinationCard(vaccinationCard);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                Call<String> callSync = vaccinationsApi.saveChildVaccinationCard(vaccinationCard);

                Log.d("CARDRetrofitSend","API Started!");

                callSync.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                        Log.d("CARDRetrofitSend",response.message());

                        isApiConnected = true;
                        assertEquals("response executed",response.body());

                        Log.d("CARDRetrofitSend","API Tested!"+response.body());

                        res = response.body();
                    }

                    @Override
                    public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {

                        Log.d("CARDRetrofitFail",t.getMessage());
                        res = t.getMessage();
                        isApiConnected = false;

                        assertEquals("response executed error",t.getMessage());


                    }
                });


            }
        };

        new MainThreadExecutor().execute(runnable);

        Log.d("CARD ","API Ended!");

    }



    @Test
    public void getCardsFromApi(){
        //vaccinationsApi.saveChildVaccinationCard(vaccinationCard);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                 Call<List<VaccinationCard>> callSync = vaccinationsApi.getAllVaccinationCards();
                 Log.d("CARDRetrofit","API Started!");

                 callSync.enqueue(new Callback<List<VaccinationCard>>() {
                  @Override
                  public void onResponse(Call<List<VaccinationCard>> call, Response<List<VaccinationCard>> response) {
                 Log.d("CARDRetrofit","API Responded!");
                 assertEquals("ofcourse", response.message());
                 }

                 @Override
                public void onFailure(Call<List<VaccinationCard>> call, Throwable t) {
                 Log.d("CARDRetrofit","API Response Failure Messages!"+t.getCause());
                  }
                });

            }
        };

        new MainThreadExecutor().execute(runnable);

    }


    @Test
    public void getImmunisationEventsFromApi(){
        Log.d("CARDRetrofit","API GO!");

        //vaccinationsApi.saveChildVaccinationCard(vaccinationCard);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                Call<List<HospitalImmunisationEvent>> callSync = vaccinationsApi.getHospitalSchedule("pdpdpdpd");
                Log.d("CARDRetrofit","API Started!");

                callSync.enqueue(new Callback<List<HospitalImmunisationEvent>>() {
                    @Override
                    public void onResponse(Call<List<HospitalImmunisationEvent>> call, Response<List<HospitalImmunisationEvent>> response) {


                        Log.d("CARDRetrofit","API Responded!");

                        assertEquals("HospitalEvents", response.message());

                    }

                    @Override
                    public void onFailure(Call<List<HospitalImmunisationEvent>> call, Throwable t) {

                        Log.d("CARDRetrofit","API Response Failure Messages!"+t.getCause());

                    }
                });

            }
        };


        new MainThreadExecutor().execute(runnable);

        // assertTrue(callSync.isExecuted());
        // assertTrue(callSync.isCanceled());

    }






    public class MainThreadExecutor implements Executor {

        private Handler mainThreadHandler = new Handler(Looper.getMainLooper());


        @Override
        public void execute(@NonNull Runnable runnable) {

            mainThreadHandler.postDelayed(runnable, 5000);
        }
    }


}
